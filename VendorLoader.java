import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class VendorLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public VendorLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		Map vendorList = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		String[] row;
		int rowCount=0;
		String vendorCountSQL = "select count(*) count from enterprise_strategy where enterprise_strategy_name = ? and plan_type='3RD PARTY' and is_activity='N'";
		String vendorSQL = "select id,enterprise_strategy_name,extension from enterprise_strategy where enterprise_strategy_name like ? and plan_type='3RD PARTY' and is_activity='N' order by updated_timestamp asc";
		String vendorOlderSQL = "select id,enterprise_strategy_name,extension from enterprise_strategy where enterprise_strategy_name = ? and plan_type='3RD PARTY' and is_activity='N' order by updated_timestamp asc limit 1";
		String vendorDeleteSQL = "delete from enterprise_strategy where id = ?";
		String vendorInsertSQL = "insert into enterprise_strategy(id, enterprise_strategy_name, extension, is_activity, plan_type, company_code, created_by, updated_by) values(uuid(), ?,?, 'N', '3RD PARTY','BKS','support@smarterd.com','support@smarterd.com')";
		String vendorUpdateSQL = "update enterprise_strategy set extension = ? where id = ?";
		String id=null;

		// STEP - 1 CLEANUP DUPLICATES IN EXISTING TABLE
		LinkedHashSet vendorIdList = new LinkedHashSet<>();
		PreparedStatement pst = conn.prepareStatement(vendorSQL);
		pst.setString(1, "%");
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String vendorName = rs.getString("enterprise_strategy_name");
			vendorIdList.add(vendorName);
		}
		rs.close();
		pst.close();
		System.out.println(vendorIdList.toString());
		Iterator iter = vendorIdList.iterator();
		while(iter.hasNext()) {
			String vendorName = (String)iter.next();

			int count = 0;
			pst = conn.prepareStatement(vendorCountSQL);
			pst.setString(1, vendorName);
			rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			// get the older vendor and delete
			if(count > 1) {
				pst = conn.prepareStatement(vendorOlderSQL);
				pst.setString(1, vendorName);
				rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
				}
				rs.close();
				pst.close();
				System.out.println("Duplicate Older Vendor found:"+vendorName+":"+id);
				
				pst = conn.prepareStatement(vendorDeleteSQL);
				pst.setString(1, id);
				pst.executeUpdate();
				pst.close();
				System.out.println("Duplicate Vendor deleted:"+vendorName);
				
			}
		}

		/*
		// STEP-2 COMMENT STEP-1 AND RUN CANONICAL FILE DATA 
		// UNCOMMENT ONE CONDITION AT A TIME AND RUN 
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String vendorName=null, aliasName=null;
				String category=null, level=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						vendorName = row[i].trim();
					} else if(i==1) {
						aliasName = row[i].trim();
					}
				}
				LinkedHashSet aliasList = null;
				if(vendorList.containsKey(vendorName)) {
					aliasList = (LinkedHashSet)vendorList.get(vendorName);
				} else {
					aliasList = new LinkedHashSet<>();
				}
				aliasList.add(aliasName);
				vendorList.put(vendorName,aliasList);

				vendorName = "";
				aliasName = "";
			}
		}
		//System.out.println("Vendor:"+vendorList.toString()+":"+vendorList.size());
		
		Iterator iter = vendorList.keySet().iterator();
		while(iter.hasNext()) {
			String vendorName = (String)iter.next();
			LinkedHashSet aliasList = (LinkedHashSet)vendorList.get(vendorName);

			String id=null, extension=null;
			int count = 0;
			PreparedStatement pst = conn.prepareStatement(vendorCountSQL);
			pst.setString(1, vendorName);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			// get the older vendor and delete
			if(count > 1) {
				pst = conn.prepareStatement(vendorOlderSQL);
				pst.setString(1, vendorName);
				rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
				}
				rs.close();
				pst.close();
				System.out.println("Duplicate Older Vendor found:"+vendorName+":"+id);

				pst = conn.prepareStatement(vendorDeleteSQL);
				pst.setString(1, id);
				pst.executeUpdate();
				pst.close();
				System.out.println("Duplicate Vendor deleted:"+vendorName);
			} else if(count == 1) {
				System.out.println("Vendor found:"+vendorName+":"+id);
				String ext = null;
				pst = conn.prepareStatement(vendorOlderSQL);
				pst.setString(1, vendorName);
				rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();
				OrderedJSONObject extObj = null;
				JSONArray aliasArr = null;
				if(ext != null && ext.length() > 0) {
					extObj = new OrderedJSONObject(ext);
					if(extObj.has("vendor_alias")) {
						aliasArr = (JSONArray)extObj.get("vendor_alias");
						Iterator aliasListArr = aliasList.iterator();
						while(aliasListArr.hasNext()) {
							boolean found = false;
							String name = (String)aliasListArr.next();
							Iterator aliasArrIter = aliasArr.iterator();
							while(aliasArrIter.hasNext()) {
								OrderedJSONObject aliasObj = (OrderedJSONObject)aliasArrIter.next();
								String aliasName = (String)aliasObj.get("vendor_name");
								if(aliasName.equalsIgnoreCase(name)) {
									found = true;
									break;
								}
							}
							if(!found) {
								OrderedJSONObject aliasObj = new OrderedJSONObject();
								aliasObj.put("vendor_name", name);
								aliasArr.add(aliasObj);
							}
						}
						extObj.put("vendor_alias", aliasArr);
					} else {
						aliasArr = new JSONArray();
						Iterator aliasListIter = aliasList.iterator();
						while(aliasListIter.hasNext()) {
							String name = (String)aliasListIter.next();
							OrderedJSONObject aliasObj = new OrderedJSONObject();
							aliasObj.put("vendor_name", name);
							aliasArr.add(aliasObj);
						}
						extObj.put("vendor_alias", aliasArr);
					}
				} else {
					aliasArr = new JSONArray();
					extObj = new OrderedJSONObject();
					Iterator aliasListIter = aliasList.iterator();
					while(aliasListIter.hasNext()) {
						String name = (String)aliasListIter.next();
						OrderedJSONObject aliasObj = new OrderedJSONObject();
						aliasObj.put("vendor_name", name);
						aliasArr.add(aliasObj);
					}
					extObj.put("vendor_alias", aliasArr);
				}
				pst = conn.prepareStatement(vendorUpdateSQL);
				pst.setString(1, extObj.toString());
				pst.setString(2, id);;
				pst.executeUpdate();
				pst.close();
				System.out.println("Successfully updated vendor!!!");
			} else if(count == 0) {
				System.out.println("Vendor not found:"+vendorName);
				JSONArray aliasArr = new JSONArray();
				Iterator aliasIter = aliasList.iterator();
				while(aliasIter.hasNext()) {
					String name = (String)aliasIter.next();
					OrderedJSONObject aliasObj = new OrderedJSONObject();
					aliasObj.put("vendor_name", name);
					aliasArr.add(aliasObj);
				}
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("vendor_alias", aliasArr);

				pst = conn.prepareStatement(vendorInsertSQL);
				pst.setString(1, vendorName);
				pst.setString(2, extObj.toString());
				pst.executeUpdate();
				pst.close();
			}
		}
		*/
		conn.commit();
		conn.close();
	}

	private void updateModel() throws Exception {
		String sql = "update enterprise_model set model_name='Test Process',model_desc='Test Process',model_def=compress('{\"enableRtl\":false,\"locale\":\"en-US\",\"animationComplete\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"click\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"collectionChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"commandExecute\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"connectionChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"contextMenuBeforeItemRender\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"contextMenuClick\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"contextMenuOpen\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"created\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"dataLoaded\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"doubleClick\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"dragEnter\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"dragLeave\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"dragOver\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"drop\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"expandStateChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"fixedUserHandleClick\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"historyChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"historyStateChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"keyDown\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"keyUp\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"mouseEnter\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"mouseLeave\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"mouseOver\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"onImageLoad\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"onUserHandleMouseDown\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"onUserHandleMouseEnter\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"onUserHandleMouseLeave\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"onUserHandleMouseUp\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"positionChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"propertyChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"rotateChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"scrollChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"segmentCollectionChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"selectionChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"sizeChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"sourcePointChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"targetPointChange\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"textEdit\":{\"_isScalar\":false,\"closed\":false,\"isStopped\":false,\"hasError\":false,\"thrownError\":null,\"__isAsync\":false},\"getConnectorDefaults\":{},\"height\":\"700px\",\"snapSettings\":{\"horizontalGridlines\":{\"lineColor\":\"#e0e0e0\",\"lineIntervals\":[1,9,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75],\"snapIntervals\":[20],\"lineDashArray\":\"\"},\"verticalGridlines\":{\"lineColor\":\"#e0e0e0\",\"lineIntervals\":[1,9,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75,0.25,9.75],\"snapIntervals\":[20],\"lineDashArray\":\"\"},\"constraints\":31,\"gridType\":\"Lines\"},\"width\":\"100%\",\"enablePersistence\":false,\"scrollSettings\":{\"viewPortWidth\":1479,\"viewPortHeight\":700,\"currentZoom\":1,\"horizontalOffset\":0,\"verticalOffset\":0,\"padding\":{\"left\":0,\"right\":0,\"top\":0,\"bottom\":0},\"scrollLimit\":\"Diagram\",\"minZoom\":0.2,\"maxZoom\":30,\"canAutoScroll\":false},\"rulerSettings\":{\"showRulers\":false,\"horizontalRuler\":{\"orientation\":\"Horizontal\",\"arrangeTick\":null},\"verticalRuler\":{\"orientation\":\"Vertical\",\"arrangeTick\":null}},\"backgroundColor\":\"transparent\",\"constraints\":500,\"layout\":{\"type\":\"None\",\"enableAnimation\":true,\"connectionPointOrigin\":\"SamePoint\",\"arrangement\":\"Nonlinear\"},\"contextMenuSettings\":{},\"dataSourceSettings\":{\"dataManager\":null,\"dataSource\":null,\"crudAction\":{\"read\":\"\"},\"connectionDataSource\":{\"crudAction\":{\"read\":\"\"}}},\"mode\":\"SVG\",\"layers\":[{\"objects\":[\"TerminatordItBg\",\"ProcessKAO6h\",\"Link42TQEWw\"],\"id\":\"default_layer\",\"visible\":true,\"lock\":false,\"zIndex\":0,\"objectZIndex\":6}],\"nodes\":[{\"shape\":{\"type\":\"Flow\",\"shape\":\"Terminator\"},\"ports\":[],\"id\":\"TerminatordItBg\",\"width\":80,\"height\":40,\"style\":{\"fill\":\"white\",\"strokeColor\":\"#757575\",\"strokeWidth\":1,\"strokeDashArray\":\"\",\"opacity\":1,\"gradient\":{\"type\":\"None\"}},\"container\":null,\"offsetX\":451,\"offsetY\":252,\"visible\":true,\"horizontalAlignment\":\"Left\",\"verticalAlignment\":\"Top\",\"backgroundColor\":\"transparent\",\"borderColor\":\"none\",\"borderWidth\":0,\"rotateAngle\":0,\"pivot\":{\"x\":0.5,\"y\":0.5},\"margin\":{},\"flip\":\"None\",\"wrapper\":{\"actualSize\":{\"width\":80,\"height\":40},\"offsetX\":451,\"offsetY\":252},\"constraints\":5240814,\"previewSize\":{},\"dragSize\":{},\"zIndex\":4,\"annotations\":[],\"isExpanded\":true,\"expandIcon\":{\"shape\":\"None\"},\"fixedUserHandles\":[],\"tooltip\":{\"openOn\":\"Auto\"},\"inEdges\":[],\"outEdges\":[\"Link42TQEWw\"],\"parentId\":\"\",\"processId\":\"\",\"umlIndex\":-1,\"isPhase\":false,\"isLane\":false},{\"shape\":{\"type\":\"Flow\",\"shape\":\"Process\"},\"ports\":[],\"id\":\"ProcessKAO6h\",\"width\":80,\"height\":40,\"style\":{\"fill\":\"white\",\"strokeColor\":\"#757575\",\"strokeWidth\":1,\"strokeDashArray\":\"\",\"opacity\":1,\"gradient\":{\"type\":\"None\"}},\"container\":null,\"offsetX\":614,\"offsetY\":347,\"visible\":true,\"horizontalAlignment\":\"Left\",\"verticalAlignment\":\"Top\",\"backgroundColor\":\"transparent\",\"borderColor\":\"none\",\"borderWidth\":0,\"rotateAngle\":0,\"pivot\":{\"x\":0.5,\"y\":0.5},\"margin\":{},\"flip\":\"None\",\"wrapper\":{\"actualSize\":{\"width\":80,\"height\":40},\"offsetX\":614,\"offsetY\":347},\"constraints\":5240814,\"previewSize\":{},\"dragSize\":{},\"zIndex\":5,\"annotations\":[],\"isExpanded\":true,\"expandIcon\":{\"shape\":\"None\"},\"fixedUserHandles\":[],\"tooltip\":{\"openOn\":\"Auto\"},\"inEdges\":[\"Link42TQEWw\"],\"outEdges\":[],\"parentId\":\"\",\"processId\":\"\",\"umlIndex\":-1,\"isPhase\":false,\"isLane\":false}],\"connectors\":[{\"shape\":{\"type\":\"None\"},\"id\":\"Link42TQEWw\",\"type\":\"Orthogonal\",\"sourcePoint\":{\"x\":451,\"y\":272},\"targetPoint\":{\"x\":614,\"y\":327},\"targetDecorator\":{\"shape\":\"Arrow\",\"style\":{\"fill\":\"#797979\",\"strokeColor\":\"#797979\",\"strokeWidth\":1,\"strokeDashArray\":\"\",\"opacity\":1,\"gradient\":{\"type\":\"None\"}},\"width\":9,\"height\":5,\"pivot\":{\"x\":0,\"y\":0.5}},\"style\":{\"strokeWidth\":1,\"strokeColor\":\"#797979\",\"strokeDashArray\":\"4 4\",\"fill\":\"transparent\",\"opacity\":1,\"gradient\":{\"type\":\"None\"}},\"sourcePortID\":\"\",\"targetPortID\":\"\",\"sourceID\":\"TerminatordItBg\",\"targetID\":\"ProcessKAO6h\",\"flip\":\"None\",\"segments\":[{\"type\":\"Orthogonal\",\"length\":null,\"direction\":null}],\"sourceDecorator\":{\"shape\":\"None\",\"width\":10,\"height\":10,\"pivot\":{\"x\":0,\"y\":0.5},\"style\":{\"fill\":\"black\",\"strokeColor\":\"black\",\"strokeWidth\":1,\"strokeDashArray\":\"\",\"opacity\":1,\"gradient\":{\"type\":\"None\"}}},\"cornerRadius\":0,\"wrapper\":{\"actualSize\":{\"width\":163,\"height\":55},\"offsetX\":532.5,\"offsetY\":299.5},\"annotations\":[],\"fixedUserHandles\":[],\"previewSize\":{},\"zIndex\":6,\"visible\":true,\"constraints\":470590,\"connectionPadding\":0,\"hitPadding\":10,\"tooltip\":{\"openOn\":\"Auto\"},\"sourcePadding\":0,\"targetPadding\":0,\"parentId\":\"\"}],\"pageSettings\":{\"orientation\":\"Landscape\",\"height\":null,\"width\":null,\"background\":{\"source\":\"\",\"color\":\"transparent\"},\"showPageBreaks\":false,\"fitOptions\":{\"canFit\":false},\"boundaryConstraints\":\"Infinity\"},\"selectedItems\":{\"nodes\":[],\"connectors\":[],\"constraints\":16382,\"userHandles\":[],\"rotateAngle\":0,\"pivot\":{\"x\":0.5,\"y\":0.5},\"width\":0,\"height\":0,\"offsetX\":0,\"offsetY\":0,\"rubberBandSelectionMode\":\"CompleteIntersect\",\"wrapper\":null},\"basicElements\":[],\"tooltip\":{\"content\":\"\",\"relativeMode\":\"Mouse\"},\"commandManager\":{\"commands\":[]},\"tool\":3,\"bridgeDirection\":\"Top\",\"customCursor\":[],\"diagramSettings\":{\"inversedAlignment\":true},\"version\":17.1}'),extension='{}',updated_timestamp=current_timestamp,updated_by='support@smarterd.com' where id='eedbabc8-6bbd-11eb-9c5f-42010a8a0098'";
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.executeUpdate();
		pst.close();
		conn.commit();
	}

	private void getModel() throws Exception {
		String sql = "select uncompress(model_def) model_def from enterprise_model where id='eedbabc8-6bbd-11eb-9c5f-42010a8a0098'";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String txt = rs.getString("model_def");
			System.out.println(txt);
		}
		rs.close();
		pst.close();
	}
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		VendorLoader loader = new VendorLoader(filename, companyCode, env);
		//loader.process();
		loader.getModel();
	}
}