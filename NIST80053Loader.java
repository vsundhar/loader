import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

/**
This loader does 2 things:
1 - Loads NIST 800-53 Rev 5 Framework with Control Baseline and Privacy Baseline - process() (Source: MADS)
2 - Maps NIST CSF to NIST 800-53 - linkNISTCSFToNIST80053() (Source: JBL)
 */
public class NIST80053Loader {
	private String domain = "NIST 800-53";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
	OrderedJSONObject controlNFList = new OrderedJSONObject();
   	
	public NIST80053Loader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		Map controlList = new HashMap();

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String internalId=null, controlName=null, controlDesc=null, guidance=null, relatedControls=null, controlBaseline=null, privacy=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						internalId = row[i].trim();
					} else if(i==1) {
						controlName = row[i].trim();
					} else if(i==2) {
						controlDesc = row[i].trim();
					} else if(i==3) {
						guidance = row[i].trim();
					} else if(i==4) {
						relatedControls = row[i].trim();
					} else if(i==5) {
						controlBaseline = row[i].trim();
					} else if(i==6) {
						privacy = row[i].trim();
					}
				}

				controlDesc = controlDesc.replaceAll("'","");
				if(controlDesc.indexOf("Withdrawn:") == -1) {	// Control Withdrawn mentioned in Desc - DO NOT LOAD
					System.out.println(internalId+":"+controlName);
					
					// Check if internalId has parenthesis
					boolean parentControl = true;
					String parentId = null, parentName = null;
					if(internalId.indexOf("(") != -1) {
						parentControl = false;
						String parentInternalId = internalId.substring(0, internalId.indexOf("("));
						parentId = (String)controlList.get(parentInternalId);
						parentName = controlName.substring(0, controlName.indexOf("|")).trim();
						controlName = controlName.substring(controlName.indexOf("|")+1).trim();
					}

					if(privacy == null || privacy.trim().length() == 0) {
						privacy = "N";
					} else {
						privacy = "Y";
					}
					System.out.println(parentControl+":"+parentId+":"+parentName+":"+controlName+":"+controlBaseline+":"+privacy);

					// Check Capability
					String controlId = checkCapabilityByInternalId(internalId);
					
					if(controlId == null) {
						controlId = generateUUID();
						if(parentControl) {
							controlList.put(internalId, controlId);
							insertCapability(controlId, rootId, internalId, rootId, controlName, controlDesc, "1", controlBaseline, privacy, guidance, relatedControls);
						} else {
							insertCapability(controlId, parentId, internalId, parentName, controlName, controlDesc, "2", controlBaseline, privacy, guidance, relatedControls);
						}
						System.out.println("Successfully inserted Control!!!");
					}
				} else {
					System.out.println("Control Withdrawn["+internalId+"]!!!");
				}
			}
		}
		
		conn.commit();
		conn.close();
	}

	public void linkNISTCSFToNIST80053() throws Exception {
		String[] row;
		int rowCount=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String csfInternalId = null, nist80053InternalIds = null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						csfInternalId = row[i].trim();
					} else if(i==1) {
						nist80053InternalIds = row[i].trim();
					} 
				}

				JSONArray nist80053Arr = new JSONArray();
				csfInternalId = csfInternalId.substring(0,csfInternalId.indexOf(":")).trim();
				String[] nist80053List = nist80053InternalIds.split(",");
				for(int i=0; i<nist80053List.length; i++) {
					String nist80053InternalId = nist80053List[i].trim();
					System.out.println(csfInternalId+":"+nist80053InternalId); 
					nist80053Arr.add(nist80053InternalId);
				}

				// Select controlId
				String sql = "select id, business_capability_ext from enterprise_business_capability where internal_id = ?";
				String updatesql = "update enterprise_business_capability set business_capability_ext = ? where id = ?";
				PreparedStatement pst = conn.prepareStatement(sql);
				pst.setString(1, csfInternalId);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					String controlId = rs.getString("id");
					String ext = rs.getString("business_capability_ext");

					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.remove("NIST 800-53");
					
					OrderedJSONObject reqObj = new OrderedJSONObject();
					reqObj.put("standard", "NIST 800-53");
					reqObj.put("requirements", nist80053Arr);
					extObj.put("requirements", reqObj);
					PreparedStatement pst1 = conn.prepareStatement(updatesql);
					pst1.setString(1, extObj.toString());
					pst1.setString(2, controlId);
					pst1.executeUpdate();
					pst1.close();
				}
				rs.close();
				pst.close();
			}
		}
		conn.commit();
	}

	public void updateBaselineControlAndPrivacy() throws Exception {
		String[] row;
		int rowCount=0;

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String internalId=null, controlName=null, controlDesc=null, withdrawn="", privacy="N", low="", medium="", high="";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
					} else if(i==1) {
						internalId = row[i].trim();
					} else if(i==2) {
						controlName = row[i].trim();
					} else if(i==3) {
						withdrawn = row[i].trim();
					} else if(i==4) {
						privacy = row[i].trim();
					} else if(i==5) {
						low = row[i].trim();
					} else if(i==6) {
						medium = row[i].trim();
					} else if(i==7) {
						high = row[i].trim();
					}
				}

				if(withdrawn == null || withdrawn.length() == 0) {	// Control Withdrawn mentioned in Desc - DO NOT LOAD
					System.out.println(internalId+":"+controlName);
					
					String level = "";
					if(low != null && low.length() > 0) {
						level = "L";
					}
					if(medium != null && medium.length() > 0) {
						if(level.length() == 0) {
							level = "M";
						} else {
							level = level+",M";
						}
					}
					if(high != null && high.length() > 0) {
						if(level.length() == 0) {
							level = "H";
						} else {
							level = level+",H";
						}
					}

					if(privacy == null || privacy.trim().length() == 0) {
						privacy = "N";
					} else {
						privacy = "Y";
					}
					String updatesql = "update enterprise_business_capability set business_capability_ext=json_set(business_capability_ext, '$.custom.Level', ?), business_capability_ext=json_set(business_capability_ext, '$.custom.Privacy', ?) where internal_id=? and business_capability_domain='NIST 800-53'";
					PreparedStatement pst = conn.prepareStatement(updatesql);
					pst.setString(1, level);
					pst.setString(2, privacy);
					pst.setString(3, internalId);
					pst.executeUpdate();
					pst.close();
				} else {
					System.out.println("Control Withdrawn["+internalId+"]!!!");
				}
			}
		}
		
		conn.commit();
		conn.close();
	}

	// Family got missed when we first loaded NIST 800-53
	private void addFamilytoNIST80053Controls() throws Exception {
		OrderedJSONObject familyList = new OrderedJSONObject();
		familyList.put("AC","ACCESS CONTROL");
		familyList.put("AT","AWARENESS AND TRAINING");
		familyList.put("AU","AUDIT AND ACCOUNTABILITY");
		familyList.put("CA","ASSESSMENT, AUTHORIZATION, AND MONITORING");
		familyList.put("CM","CONFIGURATION MANAGEMENT");
		familyList.put("CP","CONTINGENCY PLANNING");
		familyList.put("IA","IDENTIFICATION AND AUTHENTICATION");
		familyList.put("IR","INCIDENT RESPONSE");
		familyList.put("MA","MAINTENANCE");
		familyList.put("MP","MEDIA PROTECTION");
		familyList.put("PE","PHYSICAL AND ENVIRONMENTAL PROTECTION");
		familyList.put("PL","PLANNING");
		familyList.put("PM","PROGRAM MANAGEMENT");
		familyList.put("PS","PERSONNEL SECURITY");
		familyList.put("RA","RISK ASSESSMENT");
		familyList.put("SA","SYSTEM AND SERVICES ACQUISITION");
		familyList.put("SC","SYSTEM AND COMMUNICATIONS PROTECTION");
		familyList.put("PT","SYSTEM AND INFORMATION INTEGRITY");
		familyList.put("SR","SUPPLY CHAIN RISK MANAGEMENT");
		familyList.put("SI","SYSTEM AND INFORMATION INTEGRITY");

		OrderedJSONObject familyIds = new OrderedJSONObject();
		Iterator iter = familyList.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			String name = (String)familyList.get(key);

			/*
			String id = generateUUID();
			insertCapability(id, "NIST 800-53", key, "NIST 800-53", name, "", "1", "", "", "", "");
			
			// Update existing child with this parent and change level to 2
			String updatesql = "update enterprise_business_capability set parent_capability_id=?, business_capability_level=? where substring(internal_id,1,2) = ? and instr(internal_id,'-') > 0 and instr(internal_id, '(') = 0 and business_capability_domain = 'NIST 800-53'";
			PreparedStatement pst = conn.prepareStatement(updatesql);
			pst.setString(1, id);
			pst.setString(2, "2");
			pst.setString(3, key);
			pst.executeUpdate();
			pst.close();

			// Update current 2 child level to 3
			updatesql = "update enterprise_business_capability set business_capability_level=? where substring(internal_id,1,2) = ? and instr(internal_id, '(') > 0 and business_capability_domain = 'NIST 800-53'";
			pst = conn.prepareStatement(updatesql);
			pst.setString(1, "3");
			pst.setString(2, key);
			pst.executeUpdate();
			pst.close();
			*/

			// Update Family Name
			String updatesql = "update enterprise_business_capability set business_capability_ext=json_set(business_capability_ext, '$.family', ?) where substring(internal_id,1,2) = ? and business_capability_domain = 'NIST 800-53'";
			PreparedStatement pst = conn.prepareStatement(updatesql);
			pst.setString(1, name);
			pst.setString(2, key);
			pst.executeUpdate();
			pst.close();
		}
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		String id = checkCapability("NIST 800-53", "NIST 800-53");
		//rootId = "d121ee96-b83d-4e3d-8b7d-3d90ba7c8c88";
		rootId = "NIST 800-53";

		if(id == null) {
			insertCapability(rootId, rootId, "", "NIST 800-53", "NIST 800-53", "NIST 800-53 Control Requirements","0","","","","");
		}
		capabilityList.put("NIST 800-53", rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id,parent_capability_id,business_capability_ext from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ? and business_capability_status='Y' and business_capability_domain=?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		pst.setString(3, domain);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}


	private String checkCapabilityByInternalId(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, String level, String controlBaseline, String privacy, String guidance, String relatedControls) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "5";
   		String createdBy = "support@smarterd.com";
		String owner = "", status = "Y";
		JSONArray inscopeclassArr = new JSONArray();
		JSONArray enablingclassArr = new JSONArray();
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("currentMaturity", "0.0");
		factorObject.put("targetMaturity", "0.0");
		factorObject.put("baselineMaturity", "0.0");
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");

		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		OrderedJSONObject customObj = new OrderedJSONObject();
		customObj.put("Level", controlBaseline);
		customObj.put("Privacy", privacy);

		JSONArray rcList = null;
		String[] rcArr = relatedControls.split(",");
		if(rcArr.length > 0) {
			rcList = new JSONArray();
			for(int i =0; i<rcArr.length; i++) {
				String rc = rcArr[i];
				rcList.add(rc.trim());
			}
		} else {
			rcList = new JSONArray();
		}

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", parent);
		extObject.put("assetClass", inscopeclassArr);
		extObject.put("ecosystem", enablingclassArr);
		extObject.put("securityFunction", new JSONArray());
		extObject.put("criticality", "");
		extObject.put("frequency", new JSONArray());
		extObject.put("evidence", "");
		extObject.put("external_guidance", guidance);
		extObject.put("system_risk", new Integer(0));
		extObject.put("user_risk", new Integer(0));
		extObject.put("related_controls", rcList);
		extObject.put("custom", customObj);

		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2, parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, 0);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void updateCapability(String id, String parentId, String childName, String parentName, String ext, String assetClass) throws Exception {
		String sql = "update enterprise_business_capability set id=?, parent_capability_id=?, business_capability_ext=? where child_capability_name = ? and parent_capability_name=?";
		JSONArray classArr = null;
		OrderedJSONObject extObj = new OrderedJSONObject(ext);
		if(extObj.has("assetClass")) {
			if(extObj.get("assetClass") instanceof String) {
				String capabilityAssetClassList = (String)extObj.get("assetClass");
				if(capabilityAssetClassList.length() > 0) {
					classArr = new JSONArray(capabilityAssetClassList);
				} else {
					classArr = new JSONArray();
				}
			} else if(extObj.get("assetClass") instanceof JSONArray) {
				classArr = (JSONArray)extObj.get("assetClass");
			} else {
				classArr = new JSONArray();
			}
			ArrayList list = new ArrayList();
			Iterator iter = classArr.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				list.add(s.trim());
				// Remove old assetclass with space in front
				iter.remove();
			}
			iter = list.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				classArr.add(s);
			}
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					if(!classArr.contains(str[i].trim())) {
						classArr.add(str[i].trim());
					}
				}
			}
		} else {
			classArr = new JSONArray();
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					classArr.add(str[i].trim());
				}
			}
		}
		extObj.put("assetClass", classArr);
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, id);
		pst.setString(2, parentId);
		pst.setString(3, extObj.toString());
		pst.setString(4, childName);
		pst.setString(5, parentName);
		System.out.println(pst.toString());
		pst.executeUpdate();
	}

	private void createComplianceRelationship(String rId, String aId, String type, JSONArray compArr) throws Exception {
		System.out.println("createComplianceRelationship:"+compArr+":"+type);
		String checkrelsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
        String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		String compsql = "select id from enterprise_business_capability where internal_id = ? and business_capability_domain like ?";
		PreparedStatement pst = conn.prepareStatement(insertrelsql);
		String updatedBy = "support@smarterd.com";

		if(compArr.size() > 0) {
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPLIANCE", type);
			Iterator iter = compArr.iterator();
			while(iter.hasNext()) {
				String str = (String)iter.next();

				String compId = null;
				PreparedStatement pst1 = conn.prepareStatement(compsql);
				pst1.setString(1, str);
				pst1.setString(2, type);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					compId = rs1.getString("id");
				}
				rs1.close();
				pst1.close();

				if(compId != null) {
					int count = 0;
					pst1 = conn.prepareStatement(checkrelsql);
					pst1.setString(1, aId);
					pst1.setString(2, compId);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						ctx.put("LINKED", "");
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, compId);
						pst.setString(2, aId);
						pst.setString(3, "COMPLIANCE");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "COMPLIANCE");
						pst.setString(1, aId);
						pst.setString(2, compId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
					}

					count = 0;
					pst1 = conn.prepareStatement(checkrelsql);
					pst1.setString(1, rId);
					pst1.setString(2, compId);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						ctx.put("LINKED", "Y");
						pst.setString(1, compId);
						pst.setString(2, rId);
						pst.setString(3, "COMPLIANCE");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "COMPLIANCE");
						pst.setString(1, rId);
						pst.setString(2, compId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
					}
				} else {
					if(!controlNFList.containsKey(str)) {
						controlNFList.put(str, type);
					}
				}
			}
		}
		pst.executeBatch();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "peapi", password = "smartEuser2022!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];

		NIST80053Loader loader = new NIST80053Loader(filename, companyCode, env);
		//loader.process();
		//loader.updateBaselineControlAndPrivacy();
		//loader.linkNISTCSFToNIST80053();
		loader.addFamilytoNIST80053Controls();
	}
}