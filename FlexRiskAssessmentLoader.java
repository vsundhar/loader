import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class FlexRiskAssessmentLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public FlexRiskAssessmentLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		String inserttcsql = "insert into enterprise_threat_class(_id,name,type,behavior,assetClass,extension,lifecycle,owner,prevention,threats,recommendation,csf,createdAt,updatedAt,createdBy,updatedBy) values(?,?,'','',?,?,'ACTIVE','','',json_array(),'',json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
		String inserttsql = "insert into enterprise_threat(_id,id,name,description,source,information,criticality,countermeasures,extension,lifecycle,applicable,owner,region,assetclass,threatClasses,vulnerabilities,createdAt,updatedAt,createdBy,updatedBy) values(?,?,?,?,'','','','',?,'ACTIVE','','','',?,json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
		String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		String insertassessmentsql = "insert into enterprise_risk_assessment_detail values(uuid(),?,?,?,?,?,?,?,?,?,?,'','',?,current_timestamp,current_timestamp,'support@smarterd.com','support@smaretrd.com')";
		PreparedStatement pst2 = conn.prepareStatement(inserttsql);
		PreparedStatement pst3 = conn.prepareStatement(insertrelsql);
		
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				//System.out.println("Processing row-" + rowCount + "...");
				
				String internalId=null,assetClass=null,threat=null,vulnerability=null;
				String rHarm=null,rProbability=null,rRisk=null;
				String desc=null,currentControlEffectiveness=null;
				String cHarm=null,cProbability=null,cRisk=null;
				String confidentiality=null,integrity=null,availability=null,impactRating=null;
				String riskTreatmentOption=null,selectedControl=null,relevantControl=null;
				String eHarm=null,eProbability=null,eRisk=null;
				String controlMaturity=null,newControlEffectiveness=null;

				for(int i=0; i<row.length; i++) {
					System.out.println(i+":"+row[i]);
					if(i==0) {
						internalId = row[i].trim();
					} else if(i==1) {
						assetClass = row[i].trim();
						assetClass = assetClass.substring(assetClass.indexOf("-")+1).trim();
					} else if(i==2) {			
						threat = row[i].trim();
					} else if(i==3) {
						vulnerability = row[i].trim();
					} else if(i==4) {
						rHarm = row[i].trim();
						if(rHarm.length() == 0) {
							rHarm = "0";
						}
					} else if(i==5) {
						rProbability = row[i].trim();
						if(rProbability.length() == 0) {
							rProbability = "0";
						}
					} else if(i==6) {
						rRisk = row[i].trim();
						if(rRisk.length() == 0) {
							rRisk = "0";
						}
					} else if(i==7) {
						relevantControl = row[i].trim();
					} else if(i==8) {
						desc = row[i].trim();
					} else if(i==9) {
						currentControlEffectiveness = row[i].trim();
					} else if(i==10) {
						cHarm = row[i].trim();
					} else if(i==11) {
						cProbability = row[i].trim();
					} else if(i==12) {
						cRisk = row[i].trim();
					} else if(i==13) {
						confidentiality = row[i].trim();
					} else if(i==14) {
						integrity = row[i].trim();
					} else if(i==15) {
						availability = row[i].trim();
					} else if(i==16) {
						impactRating = row[i].trim();
					} else if(i==17) {
						riskTreatmentOption = row[i].trim();
					} else if(i==18) {
						selectedControl = row[i].trim();
					} else if(i==19) {
						eHarm = row[i].trim();
					} else if(i==20) {
						eProbability = row[i].trim();
					} else if(i==21) {
						eRisk = row[i].trim();
					} else if(i==22) {
						controlMaturity = row[i].trim();
					} else if(i==23) {
						newControlEffectiveness = row[i].trim();
					}
				}

				JSONArray acArr = new JSONArray();
				acArr.add(assetClass);

				OrderedJSONObject rrObj = new OrderedJSONObject();
				rrObj.put("harm", Integer.parseInt(rHarm));
				rrObj.put("probability", Integer.parseInt(rProbability));
				rrObj.put("risk", Integer.parseInt(rRisk));

				OrderedJSONObject crObj = new OrderedJSONObject();
				crObj.put("harm", Integer.parseInt(cHarm));
				crObj.put("probability", Integer.parseInt(cProbability));
				crObj.put("risk", Integer.parseInt(cRisk));

				OrderedJSONObject ciaObj = new OrderedJSONObject();
				ciaObj.put("confidentiality", confidentiality);
				ciaObj.put("integrity", integrity);
				ciaObj.put("availability", availability);
				ciaObj.put("impact_rating", Integer.parseInt(impactRating));

				OrderedJSONObject erObj = new OrderedJSONObject();
				erObj.put("harm", Integer.parseInt(eHarm));
				erObj.put("probability", Integer.parseInt(eProbability));
				erObj.put("risk", Integer.parseInt(eRisk));

				JSONArray cList = new JSONArray();
				if(selectedControl.indexOf("No action") == -1) {
					String[] controlList = selectedControl.split("\n");
					for(int i=0; i<controlList.length; i++) {
						String control = controlList[i];

						// Need Activity level from the control for DEMO instance
						control = control.substring(0,control.lastIndexOf("."));
						//------------------------------------------------------
						if(!cList.contains(control)) {
							cList.add(control);
						}
					}
				}
				
				String id = null;
				String checksql = "select _id from enterprise_threat_class where name=?";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, threat);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("_id");
				}
				rs.close();
				pst.close();

				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("assessment", "Y");

				//String insertsql = "insert into enterprise_risk_register values(?,?,?,'',?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
				if(id == null) {
					id = generateUUID();

					/*
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, id);
					pst.setString(2, internalId);
					pst.setString(3, acArr.toString());
					pst.setString(4, threat);
					pst.setString(5, vulnerability);
					pst.setString(6, rrObj.toString());
					pst.setString(7, desc);
					pst.setString(8, currentControlEffectiveness);
					pst.setString(9, crObj.toString());
					pst.setString(10, ciaObj.toString());
					pst.setString(11, riskTreatmentOption);
					pst.setString(12, cList.toString());
					pst.setString(13, erObj.toString());
					pst.setString(14, controlMaturity);
					pst.setString(15, newControlEffectiveness);
					pst.executeUpdate();
					pst.close();
					*/
					
					OrderedJSONObject tcrObj = new OrderedJSONObject();
					tcrObj.put("harm", Integer.parseInt("0"));
					tcrObj.put("probability", Integer.parseInt("0"));
					tcrObj.put("risk", Integer.parseInt("0"));
					extObj.put("raw_risk", tcrObj);
					PreparedStatement pst1 = conn.prepareStatement(inserttcsql);
					pst1.setString(1, id);
					pst1.setString(2, threat);
					pst1.setString(3, acArr.toString());
					pst1.setString(4, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
					System.out.println(internalId+":"+id);
				}

				// Add Threat
				extObj.put("threat_class", threat);
				extObj.put("raw_risk", rrObj);
				String tId = generateUUID();
				pst2.setString(1, tId);
				pst2.setString(2, internalId);
				pst2.setString(3, vulnerability);
				pst2.setString(4, desc);
				pst2.setString(5, extObj.toString());
				pst2.setString(6, acArr.toString());
				pst2.addBatch();

				// Link Threat to Threat Class
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("LINKED", "N");
				ctx.put("COMPONENT TYPE", "THREAT CLASS");
				pst3.setString(1, tId);
				pst3.setString(2, id);
				pst3.setString(3, "THREAT");
				pst3.setString(4, ctx.toString());
				pst3.setString(5, "support@smarterd.com");
				pst3.setString(6, "support@smarterd.com");
				pst3.setString(7, companyCode);
				pst3.addBatch();
				ctx.put("COMPONENT TYPE", "THREAT");
				pst3.setString(1, id);
				pst3.setString(2, tId);
				pst3.setString(3, "THREAT CLASS");
				pst3.setString(4, ctx.toString());
				pst3.setString(5, "support@smarterd.com");
				pst3.setString(6, "support@smarterd.com");
				pst3.setString(7, companyCode);
				pst3.addBatch();

				// Get Collaboration 
				String controlsql = "select id from enterprise_business_capability where internal_id = ?";
				JSONArray relevantControlList = new JSONArray();
				if(relevantControl.indexOf("No action") == -1) {
					String[] relevantControlArr = relevantControl.split("\n");
					for(int i=0; i<relevantControlArr.length; i++) {
						String control = relevantControlArr[i];

						// Need Activity level from the control for DEMO instance
						control = control.substring(0,control.lastIndexOf("."));
						//------------------------------------------------------
						if(!relevantControlList.contains(control)) {
							relevantControlList.add(control);
						}
					}
				}

				/*
				JSONArray selectedcontrolList = new JSONArray();
				String collsql = "select a.id id, component_id, internal_id, extension from enterprise_collaboration a, enterprise_business_capability b where a.component_id=b.id and component_type = 'CAPABILITY' and json_extract(extension, '$.\"risk_register\"') = ?";
				PreparedStatement pst5 = conn.prepareStatement(collsql);
				pst5.setString(1, internalId);
				ResultSet rs5 = pst5.executeQuery();
				while(rs5.next()) {
				*/
				Iterator iter = relevantControlList.iterator();
				while(iter.hasNext()) {
					String controlInternalId = (String)iter.next();
					PreparedStatement pst5 = conn.prepareStatement(controlsql);
					pst5.setString(1, controlInternalId.trim());
					ResultSet rs5 = pst5.executeQuery();
					while(rs5.next()) {
						String controlId = rs5.getString("id");
						//String riskId = rs5.getString("id");
						//String controlId = rs5.getString("component_id");
						//String controlInternalId = rs5.getString("internal_id");
						System.out.println("Control Id:"+controlId+":"+controlInternalId);

						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst3.setString(1, tId);
						pst3.setString(2, controlId);
						pst3.setString(3, "THREAT");
						pst3.setString(4, ctx.toString());
						pst3.setString(5, "support@smarterd.com");
						pst3.setString(6, "support@smarterd.com");
						pst3.setString(7, companyCode);
						pst3.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst3.setString(1, controlId);
						pst3.setString(2, tId);
						pst3.setString(3, "CAPABILITY");
						pst3.setString(4, ctx.toString());
						pst3.setString(5, "support@smarterd.com");
						pst3.setString(6, "support@smarterd.com");
						pst3.setString(7, companyCode);
						pst3.addBatch();

						// Rollup Control Link to Threat
						ctx.put("LINKED", "Y");
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst3.setString(1, id);
						pst3.setString(2, controlId);
						pst3.setString(3, "THREAT CLASS");
						pst3.setString(4, ctx.toString());
						pst3.setString(5, "support@smarterd.com");
						pst3.setString(6, "support@smarterd.com");
						pst3.setString(7, companyCode);
						pst3.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst3.setString(1, controlId);
						pst3.setString(2, id);
						pst3.setString(3, "CAPABILITY");
						pst3.setString(4, ctx.toString());
						pst3.setString(5, "support@smarterd.com");
						pst3.setString(6, "support@smarterd.com");
						pst3.setString(7, companyCode);
						pst3.addBatch();

						//selectedcontrolList.add(controlInternalId);

						// Get Control Details
						/*
						String controlInternalId="",parentId="",controlFamily="",currentMaturity="",level="";
						String controlsql = "select internal_id, parent_capability_id, json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity, json_unquote(json_extract(business_capability_ext, '$.\"family\"')) control_family,business_capability_level from enterprise_business_capability where id = ?";
						PreparedStatement pst6 = conn.prepareStatement(controlsql);
						pst6.setString(1, controlId);
						ResultSet rs6 = pst6.executeQuery();
						while(rs6.next()) {
							controlInternalId = rs6.getString("internal_id");
							parentId = rs6.getString("parent_capability_id");
							controlFamily = rs6.getString("control_family");
							currentMaturity = rs6.getString("current_maturity");
							level = rs6.getString("business_capability_level");
							selectedcontrolList.add(controlInternalId);
						}
						rs6.close();
						pst6.close();
						*/

						/*
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						if(cr != null && cr.length() > 0) {
							OrderedJSONObject crObj = new OrderedJSONObject(cr);
							extObj.put("current_risk", crObj);
						}
						if(er != null && er.length() > 0) {
							OrderedJSONObject erObj = new OrderedJSONObject(er);
							extObj.put("estimated_risk", erObj);
						}
						if(cia != null && cia.length() > 0) {
							OrderedJSONObject ciaObj = new OrderedJSONObject(cia);
							extObj.put("cia", ciaObj);
						}
						if(controls != null && controls.length() > 0) {
							JSONArray controlsObj = new JSONArray(controls);
							extObj.put("selected_component", controlsObj);
						}
						pst2.setString(1, extObj.toString());
						pst2.setString(2, id);
						pst2.addBatch();
						*/
					}
					rs5.close();
					pst5.close();
				}

				if(controlMaturity.equalsIgnoreCase("Defined")) {
					controlMaturity = "3.0";
				} else if(controlMaturity.equalsIgnoreCase("Managed")) {
					controlMaturity = "4.0";
				} else if(controlMaturity.equalsIgnoreCase("Optimized")) {
					controlMaturity = "5.0";
				} else {
					controlMaturity = "0.0";
				}

				// Add Assessment Detail Entry
				PreparedStatement pst4 = conn.prepareStatement(insertassessmentsql);
				OrderedJSONObject raObj = new OrderedJSONObject();
				raObj.put("risk_id", internalId);
				raObj.put("threat_class", threat);
				raObj.put("threat", vulnerability);
				raObj.put("assetclass", acArr);
				raObj.put("description", desc);
				raObj.put("raw_risk", rrObj);
				pst4.setString(1, "d172aafd-38c8-11ec-973b-42010a8a0003");
				pst4.setString(2, tId);
				pst4.setString(3, relevantControlList.toString());
				pst4.setString(4, controlMaturity);
				pst4.setString(5, currentControlEffectiveness);
				pst4.setString(6, crObj.toString());
				pst4.setString(7, ciaObj.toString());
				pst4.setString(8, erObj.toString());
				pst4.setString(9, newControlEffectiveness);
				pst4.setString(10, riskTreatmentOption);
				pst4.setString(11, raObj.toString());
				pst4.executeUpdate();
				pst4.close();
			}
		}
		pst2.executeBatch();
		pst2.close();
		pst3.executeBatch();
		pst3.close();

		System.out.println("Total Assets:"+rowCount);
		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		FlexRiskAssessmentLoader loader = new FlexRiskAssessmentLoader(filename, companyCode, env);
		loader.process();
	}
}