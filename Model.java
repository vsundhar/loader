import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  

public class Model {
	private String companyCode = null;
    private Connection conn = null;
	private String hostname = "localhost";
   	
	public Model(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void processModel() throws Exception {
		String processId = "";

		String sql = "select uncompress(model_def) model_def from enterprise_model where id = ?";
        String assetsql = "select asset_name, replace(json_extract(extension,'$.\"Title\"'),'\"','') title, lifecycle_name, replace(json_extract(extension,'$.\"Organization\"'),'\"','') org, replace(json_extract(extension,'$.\"Reporting To\"'),'\"','') manager, replace(json_extract(extension,'$.\"Resource Type\"'),'\"','') type from enterprise_asset where id = ?";
        String modeldef = null;
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, processId);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            modeldef = (String)rs.getString("model_def");
        }
        rs.close();
        pst.close();

        if(modeldef != null && modeldef.length() > 0) {
            OrderedJSONObject modelObj = new OrderedJSONObject(modeldef);
            if(modelObj.has("nodes")) {
                JSONArray compList = new JSONArray();
                JSONArray nodesArr = (JSONArray)modelObj.get("nodes");
                Iterator iter = nodesArr.iterator();
                while(iter.hasNext()) {
                    OrderedJSONObject nodeObj = (OrderedJSONObject)iter.next();
                    if(nodeObj.has("data")) {
                        OrderedJSONObject dataObj = (OrderedJSONObject)nodeObj.get("data");
                        if(dataObj.containsKey("component_id")) {
                            String id = (String)dataObj.get("component_id");
                            pst = conn.prepareStatement(assetsql);
                            rs = pst.executeQuery();
                            while(rs.next()) {
                                String name = rs.getString("asset_name");
                                String title = rs.getString("title");
                                String lifecycle = rs.getString("lifecycle_name");
                                String org = rs.getString("org");
                                String manager = rs.getString("manager");
                                String type = rs.getString("type");

                                if(nodeObj.has("annotations")) {
                                    int count = 0;
                                    JSONArray annList = new JSONArray();
                                    JSONArray annArr = (JSONArray)nodeObj.get("annotations");
                                    Iterator annIter = annArr.iterator();
                                    while(annIter.hasNext()) {
                                        OrderedJSONObject annObj = (OrderedJSONObject)annIter.next();
                                        if(count == 0) {
                                            annObj.put("content", name);
                                        } else if(count == 1) {
                                            annObj.put("content", title);
                                        }
                                        count++;
                                        annList.add(annObj);
                                    }
                                    nodeObj.put("annotations", annList);
                                }
                                if(nodeObj.has("tooltip")) {
                                    OrderedJSONObject tooltip = (OrderedJSONObject)nodeObj.get("tooltip");
                                    String content = "Name:"+name+"<br>Title:"+title+"<br>Lifecycle:"+lifecycle+"<br>Organization:"+org+"<br>Manager:"+manager+"<br>Type:"+type;
                                    tooltip.put("content", content);
                                    nodeObj.put("tooltip", tooltip);
                                }
                            }
                            rs.close();
                            pst.close();
                        }
                    }
                }
            }
        }
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		Model model = new Model(filename, companyCode, env);
		model.processModel();
	}
}