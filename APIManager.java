import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  

public class APIManager {
	private String companyCode = null;
    private Connection conn = null;
	private String hostname = "localhost";
    private String apiUrl = null;
   	
	public APIManager(String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
            apiUrl = "https://api.smarterd.com/smarterd/api/smarte";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
            apiUrl = "https://papi.smarterd.com/smarterd/api/smarte";
		}
        
        // Connect Mysql
        //mysqlConnect(companyCode);
	}
	
	public void invoke(String command, String action, OrderedJSONObject dataObj) throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("COMMAND", command);
        parameters.put("ACTION", action);
        parameters.put("EMAIL", "support@smarterd.com");
        parameters.put("DATA", dataObj.toString());
        String param = getParamsString(parameters);

        URL url = new URL(apiUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Java client");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(param);
        out.flush();
        out.close();
        
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        System.out.println(content.toString());
	}

    public String getParamsString(Map<String, String> params) throws Exception {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
          result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
          result.append("=");
          result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
          result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String companyCode = args[0];
		String env = args[1];
        System.out.println(companyCode);
		APIManager mgr = new APIManager(companyCode, env);

        // Trigger Vulnerability Summary Job
        /*
        String command = "enterprise_job_scheduler";
        String action = "schedule";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "start");
        dataObj.put("2", "Vulnerability Summary");
        dataObj.put("3", companyCode);
        */

        // getAssetScope API
        /*
        String command = "enterprise_vulnerability";
        String action = "vulnerability";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        JSONArray filter = new JSONArray();
        filter.add("Apache");
        filter.add("RMS");
        dataObj.put("1", "getAssetScope");
        dataObj.put("2", filter);
        dataObj.put("3", companyCode);
        */
        /*
        String command = "enterprise_risk_query";
        String action = "risk_query";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "getRiskDetail");
        dataObj.put("2", "ASSET");
        dataObj.put("3", "%");
        dataObj.put("4", companyCode);
        */
        /*
        String command = "enterprise_report_query";
        String action = "report_query";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "getVulnerabilityDataCount");
        dataObj.put("2", "%");
        dataObj.put("3", "%");
        dataObj.put("4", "%");
        dataObj.put("5", "Medium");
        dataObj.put("6", "Risk Acceptance");
        dataObj.put("7", "%");
        dataObj.put("8", "%");
        dataObj.put("9", "%");
        dataObj.put("10", companyCode);
        mgr.invoke(command, action, dataObj);
        */
        /*
        String command = "enterprise_risk_query";
        String action = "risk_query";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "getVulnerabilityRemediationSummary");
        dataObj.put("2", companyCode);
        */
        /*
        String command = "enterprise_audit";
        String action = "audit";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "generatePOAMPlan");
        dataObj.put("2", "1c031f0a-8b4c-11ec-aa62-42010a8a0004");
        dataObj.put("3", "Ardel Test Assessment-1");
        dataObj.put("4", "Test Assessment");
        dataObj.put("5", "03/25/2022");
        dataObj.put("6", "05/31/2022");
        */
        String command = "enterprise_reminder";
        String action = "reminder";
        OrderedJSONObject dataObj = new OrderedJSONObject();
        dataObj.put("1", "processReminder");
        dataObj.put("2", "SE");

        mgr.invoke(command, action, dataObj);
	}
}