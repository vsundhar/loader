import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.util.Date;

public class NIST80030ThreatLoader {
	private CSVReader reader = null;
	private String companyCode = null;
	private Connection conn = null;
	private String hostname = "";
	
	public NIST80030ThreatLoader(String filename, String env, String companyCode) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);

		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
		mysqlConnect(companyCode);
	}
	
	public void loadThreatClass() throws Exception {
		String[] row;
		int rowCount=0;
		String insertsql = "insert into enterprise_threat_class(_id,name,behavior,assetClass,lifecycle,recommendation,csf,extension,createdAt,updatedAt) values(uuid(),?,?,json_array(),'Active',json_array(),json_array(),?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(insertsql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String internalId="", name="", desc="", tactic="", source="";
				for(int i=0; i<row.length; i++) {
					if(i==0) {
						internalId = row[i].trim();
					} else if(i==1) {			
						tactic = row[i].trim();
					} else if(i==2) {
						name = row[i].trim();
					} else if(i==3) {
						desc = row[i].trim();
					} else if(i==4) {
						source = row[i].trim();
					}
				}

				OrderedJSONObject rrObj = new OrderedJSONObject();
				rrObj.put("harm", new Integer(0));
				rrObj.put("level", "");
				rrObj.put("probability", new Integer(0));
				rrObj.put("risk", new Integer(0));

				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("internal_id", internalId);
				extObj.put("source", source);
				extObj.put("tactic", tactic);
				extObj.put("domain", "NIST800-30");
				extObj.put("priority", "");
				extObj.put("raw_risk", rrObj);
				extObj.put("assessment", "Y");
				extObj.put("threat_score", new Integer(0));

				// Insert into Threat Class
				pst.setString(1, name);
				pst.setString(2, desc);
				pst.setString(3, extObj.toString());
				pst.addBatch();
			}
		}

		pst.executeBatch();
		pst.close();
		conn.commit();
	}
	
    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "admin", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
	}
	
	private String camelCase(String str) throws Exception {
		return(str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase());
    }

	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String env = args[1];
		String companyCode = args[2];
		NIST80030ThreatLoader loader = new NIST80030ThreatLoader(filename, env, companyCode);
		loader.loadThreatClass();
	}
}