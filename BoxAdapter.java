import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxUser;

import java.io.FileInputStream;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class BoxAdapter {
	private String companyCode = null;
	//private String username = "vsundhar@smarterd.com";
	private String maurAccessToken = "0r3435tloqpgg49oa0wk613zv6g2qv0h";
	//private String ssUrl = "https://flexport.atlassian.net/";
	private BoxAPIConnection api = null;
	private static final int MAX_DEPTH = 1;
   	
	public BoxAdapter(String companyCode) throws Exception {
		this.companyCode = companyCode;
		api = new BoxAPIConnection(maurAccessToken);
		BoxUser.Info userInfo = BoxUser.getCurrentUser(api).getInfo();
        System.out.format("Welcome, %s <%s>!\n\n", userInfo.getName(), userInfo.getLogin());
		System.out.println("Successfully connected to Box!!!");
	}
	
    public OrderedJSONObject getFolderList() throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		BoxFolder rootFolder = BoxFolder.getRootFolder(api);
		listFolder(rootFolder, 0);

		return res;
	}

	private void listFolder(BoxFolder folder, int depth) throws Exception {
		for (BoxItem.Info itemInfo : folder) {
            String indent = "";
            for (int i = 0; i < depth; i++) {
                indent += "    ";
            }
            System.out.println(indent + itemInfo.getName());
            if(itemInfo instanceof BoxFolder.Info) {
                BoxFolder childFolder = (BoxFolder) itemInfo.getResource();
                if (depth < MAX_DEPTH) {
                    listFolder(childFolder, depth + 1);
                }
            }
        }
	}

	public OrderedJSONObject getFiles(long id) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		
		return res;
	}

	private String getDateFormat(String d) throws Exception {
		String isoFormat = "^((19|2[0-9])[0-9]{2})([- /.])(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
		String usFormat = "^(((0[13578]|(10|12))/(0[1-9]|[1-2][0-9]|3[0-1]))|(02/(0[1-9]|[1-2][0-9]))|((0[469]|11)/(0[1-9]|[1-2][0-9]|30)))/[0-9]{4}$";
		String format = "";

		if(d.matches(isoFormat)) {
			format = "iso";
		} else if(d.matches(usFormat)) {
			format = "us";
		} else if(d.indexOf("T") != -1) {
			format = "iso 8601";
		}

		return format;
	}
	
	public static void main(String[] args) throws Exception {
		String companyCode = args[0];
		BoxAdapter adapter = new BoxAdapter(companyCode);
		OrderedJSONObject res = adapter.getFolderList();
		//System.out.println(res.toString());
		//String id = "8070030597351300";
		//adapter.getMAURSheetDetails(Long.parseLong(id));
		/*
		int count = 0;
		Map names = adapter.getMappedNames(res);
		Iterator iter = res.keySet().iterator();
		while(iter.hasNext()) {
			String id = (String)iter.next();
			String name = (String)res.get(id);
			if(names.containsKey(name)) {
				name = (String)names.get(name);
			}
			adapter.getMAURSheetDetails(Long.parseLong(id), name);
			count ++;
		}
		*/
	}
}