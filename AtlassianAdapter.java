import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.HttpClient;

import java.net.URI;

import java.text.SimpleDateFormat;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Project;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Watches;
import net.rcarz.jiraclient.ProjectCategory;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.IssueLink;
import net.rcarz.jiraclient.IssueType;
import net.rcarz.jiraclient.Comment;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.agile.AgileClient;
import net.rcarz.jiraclient.agile.Sprint;
import net.rcarz.jiraclient.agile.TimeTracking;
import net.rcarz.jiraclient.agile.Resolution;
import net.sf.json.JSON;
import net.sf.json.JSONObject;

import java.util.Map;
import java.util.HashMap;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class AtlassianAdapter {
	private String companyCode = null;
	//private String username = "vsundhar@smarterd.com";
	private String username = "jira_integrations@flexport.com";
	//private String password = "oPCp7JvIjGhYL88AZJhw10B3";
	private String password = "ODAMzvN5lqiBsnXshtNx8F64";
	//private String jiraUrl = "https://smarterd.atlassian.net/";
	private String jiraUrl = "https://flexport.atlassian.net/";
	private JiraClient jira = null;
   	
	public AtlassianAdapter(String companyCode) throws Exception {
		this.companyCode = companyCode;
		this.jira = getJiraClient();
		System.out.println("Successfully connected to Jira!!!");
	}
	
    public String createIssue(String desc, String comment, String assignee, JSONArray watcherList) throws Exception {
		String projectKey = "SEC";
		String type = "EPIC";
		// Create Issue
		Issue newIssue = jira.createIssue(projectKey, type)
        .field(Field.SUMMARY, desc)
		.field(Field.DESCRIPTION, desc)
		.execute();

		// Add Comment
		newIssue.addComment(comment);

		// Assign 
		try {
			newIssue.update().field(Field.ASSIGNEE, assignee);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

		// Add Watcher
		Iterator iter = watcherList.iterator();
		while(iter.hasNext()) {
			String watcher = (String)iter.next();
			try {
				newIssue.addWatcher(watcher);
			} catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
		}

		return newIssue.toString();
	}

	public void createEpicByInitiative(String parentKey, String name, String desc, String type, String assignee, 
									   JSONArray watcherList, String startdate, String enddate, String priority, String comment) throws Exception {
		System.out.println("In createEpicByInitiative["+name+":"+desc+":"+assignee+":"+enddate+"]");
		String error = "";
		String assigneeError = "";
		JSONArray watcherError = new JSONArray();
		OrderedJSONObject res = new OrderedJSONObject();
		String projectCategory = "";
		String sql = "select json_extract(extension, '$.\"jiraid\"') jira_id from enterprise_asset where asset_name = ?";

		// Get Jira Client
		JiraClient jira = getJiraClient();

		AgileClient agile = new AgileClient(jira);
		net.rcarz.jiraclient.agile.Issue parentIssue = agile.getIssue(parentKey);
		String parentName = parentIssue.getName();
		String parentDesc = parentIssue.getDescription();
		if(parentDesc == null) {
			parentDesc = "";
		}

		Issue iIssue = jira.getIssue(parentKey);
		Project project = iIssue.getProject();
		String projectKey = project.getKey();

		ProjectCategory pc = project.getCategory();
		projectCategory = pc.getName();
		if(projectCategory == null) {
			projectCategory = "Security";
		}

		// Get Custom Field Id for Epic Name
		List issueTypes = project.getIssueTypes();
		System.out.println(issueTypes.size());
		Iterator iter = issueTypes.iterator();
		while(iter.hasNext()) {
			IssueType issueType = (IssueType)iter.next();
			String issueId = issueType.getId();
			String issueName = issueType.getName();
			System.out.println(issueName+":"+issueId);
		}

		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd"); // Jira Format
		DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");

		LocalDate sd = LocalDate.now();
		startdate = sd.format(df1);
		System.out.println("Startdate:"+startdate);

		// enddate format
		LocalDate ed = LocalDate.parse(enddate, df1);
		enddate = ed.format(df);
		System.out.println("Enddate:"+enddate);

		// Create Issue
		Issue newIssue = null;
				
		if(type.equalsIgnoreCase("Epic")) {
			newIssue = jira.createIssue(projectKey, type)
			.field("customfield_10107", name)
			.field(Field.SUMMARY, name)
			.field(Field.DESCRIPTION, desc)
			.field(Field.PRIORITY, priority)
			.field(Field.DUE_DATE, enddate)
			.execute();
		} else {
			newIssue = jira.createIssue(projectKey, type)
			.field(Field.SUMMARY, desc)
			.field(Field.DESCRIPTION, desc)
			.field(Field.PRIORITY, priority)
			.field(Field.DUE_DATE, enddate)
			.execute();
		}

		newIssue.transition().execute("In Progress");

		System.out.println("Successfully created Jira EPIC!!!");

		// Add Comment
		if(comment != null && comment.length() > 0) {
			newIssue.addComment(comment);
		}
		newIssue.update();
		String issueKey = newIssue.getKey();

		// Link Initiative
		if(type.equalsIgnoreCase("Epic")) {
			iIssue.link(issueKey, "Initiative Link");
		} else if(type.equalsIgnoreCase("Story")) {
			iIssue.link(issueKey, "Story");
		}
	}

	public String createSubTask(String key, String desc, String comment, String assignee, JSONArray watcherList) throws Exception {
		// Get Issue
		Issue issue = jira.getIssue(key);

		// Create Sub Task
		Issue subtask = issue.createSubtask()
        .field(Field.SUMMARY, desc)
		.field(Field.DESCRIPTION, desc)
		.field(Field.ASSIGNEE, assignee)
		.execute();

		// Add Comment
		subtask.addComment(comment);

		// Add Watcher
		Iterator iter = watcherList.iterator();
		while(iter.hasNext()) {
			String watcher = (String)iter.next();
			subtask.addWatcher(watcher);
		}

		return subtask.toString();
	}

	public JSONArray getProjectList(String pc) throws Exception {
		JSONArray projectList = new JSONArray();
		List list = jira.getProjects();
		Iterator iter = list.iterator();
		while(iter.hasNext()) {
			Project p = (Project)iter.next();
			ProjectCategory pCategory = p.getCategory();
			if(pCategory != null) {
				String category = pCategory.getName();
				if(category.equals(pc)) {
					OrderedJSONObject obj = new OrderedJSONObject();
					String desc = "", lead = "";
					desc = p.getDescription();
					User user = p.getLead();
					if(user != null) {
						System.out.println(user.toString());
						lead = user.getId();
					}
					if(desc == null) {
						desc = "";

					}
					if(lead == null) {
						lead = "";
					}
					obj.put("key", p.getKey());
					obj.put("name", p.getName());
					obj.put("desc", desc);
					obj.put("Owner", lead);
					projectList.add(obj);

					System.out.println(p.getKey()+":"+p.getName()+":"+desc+":"+lead);
				}
			}
		}

		return projectList;
	}

	public JSONArray getInitiativeList(String projectKey) throws Exception {
		System.out.println("In getInitiativeList..."+projectKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			AgileClient agile = new AgileClient(jira);
			//Issue.SearchResult sr = jira.searchIssues("project="+projectKey);
			Issue.SearchResult sr = jira.searchIssues("project = SEC"+" and parentEpic ="+projectKey);
			
			System.out.println("Total: " + sr.total);
			Iterator iter = sr.iterator();
			while (iter.hasNext()) {
				Issue jiraIssue = (Issue)iter.next();
				String id = jiraIssue.getId();
				String key = jiraIssue.getKey();
				String desc = jiraIssue.getDescription();
				Project project = jiraIssue.getProject();
				String projectName = project.getName();
				projectKey = project.getKey();
				String projectDesc = project.getDescription();
				ProjectCategory pc = project.getCategory();
				String projectCategory = "";
				if(pc != null) {
					projectCategory = pc.getName();
				}
				if(projectDesc == null) {
					projectDesc = "";
				}

				IssueType issueType = jiraIssue.getIssueType();
				if(issueType != null) {
					String type = issueType.getName();
					if(type.equalsIgnoreCase("initiative")) {
						String assignee = "";
						if(jiraIssue.getAssignee() != null) {
							assignee = jiraIssue.getAssignee().getDisplayName();
						}
						String status = jiraIssue.getStatus().getName();
						String priority = jiraIssue.getPriority().getName();
						int subtaskCount = jiraIssue.getSubtasks().size();
						String createDate = "";
						if(jiraIssue.getCreatedDate() != null) {
							createDate = sdf.format(jiraIssue.getCreatedDate());
						}
						String expiryDate = "";
						if(jiraIssue.getDueDate() != null) {
							expiryDate = sdf.format(jiraIssue.getDueDate());
						}
				
						net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
						String name = issue.getName();
						if(name.equalsIgnoreCase("Vulnerability Management")) {
							String sprintName="", sprintStartdate="", sprintEnddate="";
							int sprintIssueCount=0;
							if(issue.getSprint() != null) {
								Sprint sprint = issue.getSprint();
								sprintName = sprint.getName();
								if(sprint.getStartDate() != null) {
									sprintStartdate = sdf.format(sprint.getStartDate());
								}
								if(sprint.getEndDate() != null) {
									sprintEnddate = sdf.format(sprint.getEndDate());
								}
								sprintIssueCount = sprint.getIssues().size();
							}
							
							TimeTracking tt = null;
							String timeEstimate="", timeSpent="";
							if(issue.getTimeTracking() != null) {
								tt = issue.getTimeTracking();
								timeEstimate = tt.getOriginalEstimate();
								timeSpent = tt.getTimeSpent();
							}
					
							Resolution resolution = null;
							String resName = "", resDesc = "";
							if(issue.getResolution() != null) {
								resolution = issue.getResolution();
								resName = resolution.getName();
								resDesc = resolution.getDescription();
							}
							
							/*
							OrderedJSONObject ext = new OrderedJSONObject();
							ext.put("sprint_name", sprintName);
							ext.put("sprine_startdate", sprintStartdate);
							ext.put("sprint_enddate", sprintEnddate);
							ext.put("sprint_issue_count", new Integer(sprintIssueCount));
							ext.put("time_estimate", timeEstimate);
							ext.put("time_spent", timeSpent);
							ext.put("resolution_name", resName);
							ext.put("resolutiom_desc", resDesc);
							ext.put("issue_type", type);
					
							OrderedJSONObject issueObj = new OrderedJSONObject();
							issueObj.put("project_name", projectName);
							issueObj.put("project_key", projectKey);
							issueObj.put("project_category", projectCategory);
							issueObj.put("project_desc", projectDesc);
							issueObj.put("name", name);
							issueObj.put("key", key);
							issueObj.put("desc", desc);
							issueObj.put("type", type);
							issueObj.put("assignee", assignee);
							issueObj.put("status", status);
							issueObj.put("priority", priority);
							issueObj.put("subtask_count", new Integer(subtaskCount));
							issueObj.put("create_date", createDate);
							issueObj.put("expiry_date", expiryDate);
							issueObj.put("time_estimate", timeEstimate);
							issueObj.put("time_spent", timeSpent);
							issueObj.put("resolution_name", resName);
							issueObj.put("resolution_desc", resDesc);
							issueObj.put("sprint_name", sprintName);
							issueObj.put("sprint_startdate", sprintStartdate);
							issueObj.put("sprint_enddate", sprintEnddate);
							issueObj.put("sprint_issue_count", new Integer(sprintIssueCount));
					
							res.add(issueObj);
							*/
							
							System.out.println("Id:"+id+" Issue Name:"+name);
						}
					}
				}
			}
	
			System.out.println("getIssueList:"+res.size());
		}
  
		return res;
	}

	public JSONArray getRestIssueList(String epicKey) throws Exception {
		System.out.println("In getEpicList..."+epicKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			AgileClient agile = new AgileClient(jira);
		
			//Issue.SearchResult sr = jira.searchIssues("project="+projectKey+" and issueType = Epic", "key,name,description");
			RestClient rc = jira.getRestClient();
			URI uri = createSearchURI("rest/api/latest/search", rc, "project=SEC and 'Epic Link'="+epicKey, "key,summary,description,issuetype,priority,assignee,creator,status,updated,duedate");
			JSON result = rc.get(uri);
			JSONObject obj = (JSONObject)result;
			net.sf.json.JSONArray issueList = (net.sf.json.JSONArray)obj.get("issues");
			System.out.println("Total: " + issueList.size());
			Iterator iter = issueList.iterator();
			while (iter.hasNext()) {
				JSONObject issue = (JSONObject)iter.next();
				
				String id=null, issueKey=null, name=null, desc=null, issueType=null, priority=null, assignee=null, owner=null, status=null, updatedDate=null, dueDate=null;
				Iterator iter1 = issue.keySet().iterator();
				while(iter1.hasNext()) {
					String key = (String)iter1.next();
					if(key.equals("key")) {
						issueKey = (String)issue.get(key);
					} else if(key.equals("fields")) {
						JSONObject fieldList = (JSONObject)issue.get("fields");
						if(fieldList.containsKey("summary")) {
							name = (String)fieldList.get("summary");
						} 
						if(fieldList.containsKey("issuetype")) {
							JSONObject iObj = (JSONObject)fieldList.get("issuetype");
							issueType = (String)iObj.get("name");
						} 
						if(fieldList.containsKey("description")) {
							if(fieldList.get("description") instanceof String) {
								desc = (String)fieldList.get("description");
							}
						} 
						if(fieldList.containsKey("assignee")) {
							if(fieldList.get("assignee") instanceof JSONObject) {
								JSONObject aObj = (JSONObject)fieldList.get("assignee");
								assignee = (String)aObj.get("displayName");
							}
						} 
						if(fieldList.containsKey("creator")) {
							JSONObject cObj = (JSONObject)fieldList.get("creator");
							owner = (String)cObj.get("displayName");
						} 
						if(fieldList.containsKey("priority")) {
							JSONObject pObj = (JSONObject)fieldList.get("priority");
							priority = (String)pObj.get("name");
						} 
						if(fieldList.containsKey("status")) {
							JSONObject sObj = (JSONObject)fieldList.get("status");
							status = (String)sObj.get("name");
						} 
						if(fieldList.containsKey("updated")) {
							updatedDate = (String)fieldList.get("updated");
						} 
						if(fieldList.containsKey("duedate")) {
							if(fieldList.get("duedate") instanceof String) {
								dueDate = (String)fieldList.get("duedate");
							}
						} 
						System.out.println(issueKey+":"+name+":"+":"+issueType+":"+priority+":"+assignee+":"+updatedDate+":"+dueDate+":"+status);
					} 
				}
				OrderedJSONObject issueObj = new OrderedJSONObject();
				issueObj.put("project_key", "SEC");
				issueObj.put("name", name);
				issueObj.put("key", issueKey);
				issueObj.put("desc", desc);
				issueObj.put("type", "Epic");
				issueObj.put("assignee", assignee);
				issueObj.put("owner", owner);
				issueObj.put("status", status);
				issueObj.put("priority", priority);
				issueObj.put("create_date", updatedDate);
				issueObj.put("expiry_date", dueDate);
		
				res.add(issueObj);
			}

			System.out.println("getIssueList:"+res.size());
		}
  
		return res;
	}

	public JSONArray getIssueList(String epicKey) throws Exception {
		System.out.println("In getIssueList..."+epicKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			AgileClient agile = new AgileClient(jira);
			//Issue.SearchResult sr = jira.searchIssues("project="+projectKey);
			//Issue.SearchResult sr = jira.searchIssues("project = SEC"+" and parentEpic ="+epicKey);
			Issue.SearchResult sr = jira.searchIssues("project=SEC and 'Epic Link' ="+epicKey);
			//Issue.SearchResult sr = jira.searchIssues("project=SEC and parentEpic ="+epicKey);
			System.out.println("Total: " + sr.total);
			Iterator iter = sr.iterator();
			while (iter.hasNext()) {
				Issue jiraIssue = (Issue)iter.next();
				String id = jiraIssue.getId();
				String key = jiraIssue.getKey();
				String desc = jiraIssue.getDescription();
				Project project = jiraIssue.getProject();
				String projectName = project.getName();
				String projectKey = project.getKey();
				String projectDesc = project.getDescription();
				ProjectCategory pc = project.getCategory();
				String projectCategory = "";
				if(pc != null) {
					projectCategory = pc.getName();
				}
				if(projectDesc == null) {
					projectDesc = "";
				}

				IssueType issueType = jiraIssue.getIssueType();
				String type = issueType.getName();
				String assignee = "";
				if(jiraIssue.getAssignee() != null) {
					assignee = jiraIssue.getAssignee().getDisplayName();
				}
				String status = jiraIssue.getStatus().getName();
				String priority = jiraIssue.getPriority().getName();
				int subtaskCount = jiraIssue.getSubtasks().size();
				String createDate = "";
				if(jiraIssue.getCreatedDate() != null) {
					createDate = sdf.format(jiraIssue.getCreatedDate());
				}
				String expiryDate = "";
				if(jiraIssue.getDueDate() != null) {
					expiryDate = sdf.format(jiraIssue.getDueDate());
				}
		
				net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
				String name = issue.getName();

				String sprintName="", sprintStartdate="", sprintEnddate="";
				int sprintIssueCount=0;
				if(issue.getSprint() != null) {
					Sprint sprint = issue.getSprint();
					sprintName = sprint.getName();
					if(sprint.getStartDate() != null) {
						sprintStartdate = sdf.format(sprint.getStartDate());
					}
					if(sprint.getEndDate() != null) {
						sprintEnddate = sdf.format(sprint.getEndDate());
					}
					sprintIssueCount = sprint.getIssues().size();
				}
				
				TimeTracking tt = null;
				String timeEstimate="", timeSpent="";
				if(issue.getTimeTracking() != null) {
					tt = issue.getTimeTracking();
					timeEstimate = tt.getOriginalEstimate();
					timeSpent = tt.getTimeSpent();
				}
		
				Resolution resolution = null;
				String resName = "", resDesc = "";
				if(issue.getResolution() != null) {
					resolution = issue.getResolution();
					resName = resolution.getName();
					resDesc = resolution.getDescription();
				}
				
				/*
				OrderedJSONObject ext = new OrderedJSONObject();
				ext.put("sprint_name", sprintName);
				ext.put("sprine_startdate", sprintStartdate);
				ext.put("sprint_enddate", sprintEnddate);
				ext.put("sprint_issue_count", new Integer(sprintIssueCount));
				ext.put("time_estimate", timeEstimate);
				ext.put("time_spent", timeSpent);
				ext.put("resolution_name", resName);
				ext.put("resolutiom_desc", resDesc);
				ext.put("issue_type", type);
		
				OrderedJSONObject issueObj = new OrderedJSONObject();
				issueObj.put("project_name", projectName);
				issueObj.put("project_key", projectKey);
				issueObj.put("project_category", projectCategory);
				issueObj.put("project_desc", projectDesc);
				issueObj.put("name", name);
				issueObj.put("key", key);
				issueObj.put("desc", desc);
				issueObj.put("type", type);
				issueObj.put("assignee", assignee);
				issueObj.put("status", status);
				issueObj.put("priority", priority);
				issueObj.put("subtask_count", new Integer(subtaskCount));
				issueObj.put("create_date", createDate);
				issueObj.put("expiry_date", expiryDate);
				issueObj.put("time_estimate", timeEstimate);
				issueObj.put("time_spent", timeSpent);
				issueObj.put("resolution_name", resName);
				issueObj.put("resolution_desc", resDesc);
				issueObj.put("sprint_name", sprintName);
				issueObj.put("sprint_startdate", sprintStartdate);
				issueObj.put("sprint_enddate", sprintEnddate);
				issueObj.put("sprint_issue_count", new Integer(sprintIssueCount));
		
				res.add(issueObj);
				*/
				
				System.out.println("Key:"+key+" Type "+type+" Issue Name:"+name+" Status:"+status+" Start Date:"+createDate+" End Date:"+expiryDate);
			}
	
			System.out.println("getEpicList:"+res.size());
		}
  
		return res;
	}

	public JSONArray getEpicList(String issueKey) throws Exception {
		System.out.println("In getEpicList..."+issueKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			AgileClient agile = new AgileClient(jira);

			Issue initiative =  jira.getIssue(issueKey);
			Project project = initiative.getProject();
			String projectName = project.getName();
			String projectKey = project.getKey();
			String projectDesc = project.getDescription();
			ProjectCategory pc = project.getCategory();
			String projectCategory = "";
			if(pc != null) {
				projectCategory = pc.getName();
			}
			if(projectDesc == null) {
				projectDesc = "";
			}

			List linkedIssues = initiative.getIssueLinks();
			Iterator iter = linkedIssues.iterator();
			while(iter.hasNext()) {
				IssueLink issueLink = (IssueLink)iter.next();
				Issue jiraIssue = issueLink.getOutwardIssue();
				IssueType issueType = jiraIssue.getIssueType();
				if(issueType != null) {
					String type = issueType.getName();
					if(type.equalsIgnoreCase("epic")) {
						String id = jiraIssue.getId();
						String key = jiraIssue.getKey();
						String desc = jiraIssue.getSummary();

						String assignee = "";
						if(jiraIssue.getAssignee() != null) {
							assignee = jiraIssue.getAssignee().getDisplayName();
						}
						
						String status = jiraIssue.getStatus().getName();
						String priority = jiraIssue.getPriority().getName();
						int subtaskCount = jiraIssue.getSubtasks().size();
						String createDate = "";
						if(jiraIssue.getCreatedDate() != null) {
							createDate = sdf.format(jiraIssue.getCreatedDate());
						}
						String expiryDate = "";
						if(jiraIssue.getDueDate() != null) {
							expiryDate = sdf.format(jiraIssue.getDueDate());
						}
				
						net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
						String name = issue.getName();
						String sprintName="", sprintStartdate="", sprintEnddate="";
						int sprintIssueCount=0;
						if(issue.getSprint() != null) {
							Sprint sprint = issue.getSprint();
							sprintName = sprint.getName();
							if(sprint.getStartDate() != null) {
								sprintStartdate = sdf.format(sprint.getStartDate());
							}
							if(sprint.getEndDate() != null) {
								sprintEnddate = sdf.format(sprint.getEndDate());
							}
							sprintIssueCount = sprint.getIssues().size();
						}
						
						TimeTracking tt = null;
						String timeEstimate="", timeSpent="";
						if(issue.getTimeTracking() != null) {
							tt = issue.getTimeTracking();
							timeEstimate = tt.getOriginalEstimate();
							timeSpent = tt.getTimeSpent();
						}
				
						Resolution resolution = null;
						String resName = "", resDesc = "";
						if(issue.getResolution() != null) {
							resolution = issue.getResolution();
							resName = resolution.getName();
							resDesc = resolution.getDescription();
						}
				
						OrderedJSONObject ext = new OrderedJSONObject();
						ext.put("sprint_name", sprintName);
						ext.put("sprine_startdate", sprintStartdate);
						ext.put("sprint_enddate", sprintEnddate);
						ext.put("sprint_issue_count", new Integer(sprintIssueCount));
						ext.put("time_estimate", timeEstimate);
						ext.put("time_spent", timeSpent);
						ext.put("resolution_name", resName);
						ext.put("resolutiom_desc", resDesc);
						ext.put("issue_type", type);
				
						OrderedJSONObject issueObj = new OrderedJSONObject();
						issueObj.put("project_name", projectName);
						issueObj.put("project_key", projectKey);
						issueObj.put("project_category", projectCategory);
						issueObj.put("project_desc", projectDesc);
						issueObj.put("name", name);
						issueObj.put("key", key);
						issueObj.put("desc", desc);
						issueObj.put("type", type);
						issueObj.put("assignee", assignee);
						issueObj.put("status", status);
						issueObj.put("priority", priority);
						issueObj.put("subtask_count", new Integer(subtaskCount));
						issueObj.put("create_date", createDate);
						issueObj.put("expiry_date", expiryDate);
						issueObj.put("time_estimate", timeEstimate);
						issueObj.put("time_spent", timeSpent);
						issueObj.put("resolution_name", resName);
						issueObj.put("resolution_desc", resDesc);
						issueObj.put("sprint_name", sprintName);
						issueObj.put("sprint_startdate", sprintStartdate);
						issueObj.put("sprint_enddate", sprintEnddate);
						issueObj.put("sprint_issue_count", new Integer(sprintIssueCount));
				
						res.add(issueObj);
						
						System.out.println("Id:"+id+" Epic Name:"+name+" Start Date:"+createDate+" End Date:"+expiryDate);
					}
				}
			}
	
			System.out.println("getEpicList:"+res.size());
		}
  
		return res;
	}

	public String getIssueStatus(String key) throws Exception {
		AgileClient agile = new AgileClient(jira);
		RestClient rc = jira.getRestClient();
		net.rcarz.jiraclient.agile.Issue aissue = agile.getIssue(key);
		Issue issue = jira.getIssue(key);
		//String name = issue.getName();
		String desc = issue.getDescription();
		//JSON result = rc.get("rest/agile/latest/epic/SEC-3390/issue");

		JSON result = rc.get("rest/api/latest/jql/match");
		
		return issue.getStatus().toString();
	}

	public JSONArray getEList(String projectKey) throws Exception {
		System.out.println("In getEpicList..."+projectKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			AgileClient agile = new AgileClient(jira);
			RestClient rc = jira.getRestClient();

			//Issue.SearchResult sr = jira.searchIssues("project="+projectKey+" and issueType = Epic", "key,name,description");
			
			URI uri = createSearchURI("rest/api/latest/search", rc, "project="+projectKey+" and issueType = Epic", "key,summary,description,priority,assignee,creator,status,updated,due");
			JSON result = rc.get(uri);
			JSONObject obj = (JSONObject)result;
			net.sf.json.JSONArray issueList = (net.sf.json.JSONArray)obj.get("issues");
			System.out.println("Total: " + issueList.size());
			Iterator iter = issueList.iterator();
			while (iter.hasNext()) {
				JSONObject issue = (JSONObject)iter.next();
				
				String id=null, issueKey=null, name=null, desc=null, priority=null, assignee=null, owner=null, status=null, updatedDate=null, dueDate=null;
				Iterator iter1 = issue.keySet().iterator();
				while(iter1.hasNext()) {
					String key = (String)iter1.next();
					if(key.equals("key")) {
						issueKey = (String)issue.get(key);
					} else if(key.equals("fields")) {
						JSONObject fieldList = (JSONObject)issue.get("fields");
						if(fieldList.containsKey("summary")) {
							name = (String)fieldList.get("summary");
						} 
						if(fieldList.containsKey("description")) {
							if(fieldList.get("description") instanceof String) {
								desc = (String)fieldList.get("description");
							}
						} 
						if(fieldList.containsKey("assignee")) {
							JSONObject aObj = (JSONObject)fieldList.get("assignee");
							assignee = (String)aObj.get("displayName");
						} 
						if(fieldList.containsKey("creator")) {
							JSONObject cObj = (JSONObject)fieldList.get("creator");
							owner = (String)cObj.get("displayName");
						} 
						if(fieldList.containsKey("priority")) {
							JSONObject pObj = (JSONObject)fieldList.get("priority");
							priority = (String)pObj.get("name");
						} 
						if(fieldList.containsKey("status")) {
							JSONObject sObj = (JSONObject)fieldList.get("status");
							status = (String)sObj.get("name");
						} 
						if(fieldList.containsKey("updated")) {
							updatedDate = (String)fieldList.get("updated");
						} 
						if(fieldList.containsKey("due")) {
							dueDate = (String)fieldList.get("due");
						} 
						System.out.println(issueKey+":"+name+":"+priority+":"+assignee+":"+updatedDate+":"+dueDate+":"+status);
					} 
				}
			}

			System.out.println("getIssueList:"+res.size());
		}
  
		return res;
	}
	
	public void deleteIssue(String issueKey, boolean deleteSubtasks) throws Exception {
	}

	private JiraClient getJiraClient() throws Exception {
		BasicCredentials creds = new BasicCredentials(username, password);
        return(new JiraClient(jiraUrl, creds));
	}

	/**
     * Creates the URI to execute a jql search.
     * 
     * @param restclient
     * @param jql
     * @param includedFields
     * @param expandFields
     * @param maxResults
     * @param startAt
     * @return the URI to execute a jql search.
     * @throws URISyntaxException
     */
    private static URI createSearchURI(String baseUri, RestClient restclient, String jql, String includedFields) throws Exception {
        Map<String, String> queryParams = new HashMap<String, String>();
        queryParams.put("jql", jql);
        if (includedFields != null) {
            queryParams.put("fields", includedFields);
        }

        URI searchUri = restclient.buildURI(baseUri, queryParams);
        return searchUri;
    }
	
	public static void main(String[] args) throws Exception {
		String companyCode = args[0];
		//String assignee = "jmazaira@smarterd.com";
		String assignee = "jchung@flexport.com";
		JSONArray watcher = new JSONArray();
		//watcher.add("vsundhar@smarterd.com");
		//watcher.add("bdesai@smarterd.com");
		//watcher.add("jchung@flexport.com");
		AtlassianAdapter adapter = new AtlassianAdapter("FLEX");
		//String id = adapter.createIssue("Test Issue from SmarterD", "Test Comment", assignee, watcher);
		//System.out.println(id);

		//id = adapter.createSubTask(id, "SmarterD created Sub Task", "Sub Task Comment", assignee, watcher);
		//System.out.println(id);
		//List pc = adapter.getProjectList("Security");
		
		JSONArray list = adapter.getInitiativeList("SEC-3390");
		System.out.println(list.toString());

		//JSONArray list = adapter.getEpicList("SEC-63");
		//System.out.println(list.toString());

		//adapter.createEpicByInitiative("SEC-86", "Create Vulnerability Management Process", "Create Vulnerability Management Process", "Story", "bdesai@smarterd.com", new JSONArray(), "06/15/2021", "10/31/2021", "High", "");
		//adapter.getIssueStatus("SEC-3390");

		//adapter.getEList("SEC");

		//JSONArray list = adapter.getRestIssueList("SEC-3390");
		//System.out.println(list.toString());
		
	}
}