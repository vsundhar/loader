import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import java.text.DecimalFormat;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;

public class DHAPlanLoader {
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public DHAPlanLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = CSVParser.parse(new FileReader(new File(filename+"_data.csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String planId = null, subplanId = null, planDuration=null, subplanDuration=null;
		OrderedJSONObject planList = new OrderedJSONObject();
		OrderedJSONObject rDetailList = new OrderedJSONObject();
		DateTimeFormatter indf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter outdf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		DecimalFormat df = new DecimalFormat("0.00");
		String plansql = "select _id from enterprise_threat_plan where name = ?";
		String planinsertsql = "insert into enterprise_threat_plan(_id,name,planType,category,startDate,endDate,completion,status,implementation,extension,createdBy,updatedBy) values(?,?,?,'',?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com')";
		String subplaninsertsql = "insert into enterprise_threat_subplan(_id,name,planType,category,threatPlan,root,subplan,startDate,endDate,completion,status,implementation,extension,createdBy,updatedBy) values(?,?,?,'',?,?,?,?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com')";
		String subplansql = "select _id from enterprise_threat_subplan where name = ? and threatPlan = ? and root = true";
		for(CSVRecord dataRow : dataReader) {
			String internalId=null, mode=null, taskName=null, duration=null, sd=null, ed=null, completion=null, totalHrs=null, resources=null, budget=null;
			if(dataRow.isConsistent()) {
				
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						internalId = dataRow.get(i).trim();
					} else if(i==1) {
						mode = dataRow.get(i).trim();
					} else if(i==2) {
						taskName = dataRow.get(i).trim();
					} else if(i==3) {
						duration = dataRow.get(i).trim();
					} else if(i==4) {
						sd = dataRow.get(i).trim();
						LocalDate indt = LocalDate.parse(sd, indf);
						sd = outdf.format(indt);
					} else if(i==5) {
						ed = dataRow.get(i).trim();
						LocalDate indt = LocalDate.parse(ed, indf);
						ed = outdf.format(indt);
					} else if(i==6) {
						completion = dataRow.get(i).trim();
					} else if(i==7) {
						totalHrs = dataRow.get(i).trim();
						totalHrs = totalHrs.replaceAll(",","");
					} else if(i==8) {
						resources = dataRow.get(i).trim();
					} else if(i==9) {
						budget = dataRow.get(i).trim();
					}
				}
			}
			System.out.println(internalId+":"+taskName+":"+resources);
			String impl = "";
			if(completion.equals("0")) {
				impl = "New";
			} else if(completion.equals("100")) {
				impl = "Completed";
			} else {
				impl = "In-Flight";
			}

			String[] resourceList = resources.split(",");
			JSONArray rList = new JSONArray();
			for(int i=0; i<resourceList.length; i++) {
				String name = resourceList[i].trim();
				if(name.length() > 0) {
					rList.add(name);
				}
			}

			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("duration", new Double(duration));
			extObj.put("internal_id", internalId);
			extObj.put("roadmap", "N");
			extObj.put("system_risk", new Integer(0));
            extObj.put("system_priority", "");
			extObj.put("fiscal_year", "FY22");

			String planType = null, componentId = null;
			PreparedStatement pst = null;
			String[] ids = internalId.split("\\.");
			System.out.println(ids.length);
			if(ids.length == 1) {
				planType = "PLAN";
				planId = generateUUID();
				componentId = planId;
				int taskId = generateId(planId);
				extObj.put("level", new Integer(1));
				extObj.put("task_id", new Integer(taskId));
				pst = conn.prepareStatement(planinsertsql);
				pst.setString(1, planId);
				pst.setString(2, taskName);
				pst.setString(3, "IT");
				pst.setString(4, sd);
				pst.setString(5, ed);
				pst.setString(6, completion);
				pst.setString(7, "GREEN");
				pst.setString(8, impl);
				pst.setString(9, extObj.toString());
				pst.executeUpdate();
				pst.close();
				planDuration = duration;
			} else if(ids.length == 2) {
				planType = "SUBPLAN";
				subplanId = generateUUID();
				componentId = subplanId;
				int taskId = generateId(subplanId);
				extObj.put("level", new Integer(2));
				extObj.put("task_id", new Integer(taskId));
				pst = conn.prepareStatement(subplaninsertsql);
				pst.setString(1, subplanId);
				pst.setString(2, taskName);
				pst.setString(3, "IT");
				pst.setString(4, planId);
				pst.setBoolean(5, true);
				pst.setString(6, "");
				pst.setString(7, sd);
				pst.setString(8, ed);
				pst.setString(9, completion);
				pst.setString(10, "GREEN");
				pst.setString(11, impl);
				pst.setString(12, extObj.toString());
				pst.executeUpdate();
				pst.close();
				subplanDuration = duration;
			} else if(ids.length == 3) {
				planType = "ACTIVITY";
				String activityId = generateUUID();
				componentId = activityId;
				int taskId = generateId(activityId);
				extObj.put("level", new Integer(3));
				extObj.put("task_id", new Integer(taskId));
				pst = conn.prepareStatement(subplaninsertsql);
				pst.setString(1, activityId);
				pst.setString(2, taskName);
				pst.setString(3, "IT");
				pst.setString(4, planId);
				pst.setBoolean(5, false);
				pst.setString(6, subplanId);
				pst.setString(7, sd);
				pst.setString(8, ed);
				pst.setString(9, completion);
				pst.setString(10, "GREEN");
				pst.setString(11, impl);
				pst.setString(12, extObj.toString());
				pst.executeUpdate();
				pst.close();

				// Add Resources
				int rCount = rList.size();
				Iterator rIter = rList.iterator();
				while(rIter.hasNext()) {
					String name = (String)rIter.next();
					double d = rCount * Double.parseDouble(duration);
					double hrs = Double.parseDouble(totalHrs)/d;
					String hrsstr = df.format(hrs);
					String key = sd+":"+ed;
					OrderedJSONObject utilization = null;
					JSONArray arr = null;
					if(rDetailList.has(key)) {
						utilization = (OrderedJSONObject)rDetailList.get(key);
						if(utilization.has(name)) {
							arr = (JSONArray)utilization.get(name);
						} else {
							arr = new JSONArray();
						}
						OrderedJSONObject obj = new OrderedJSONObject();
						obj.put("id", activityId);
						obj.put("internal_id", internalId);
						obj.put("task_name", taskName);
						obj.put("hrs", hrsstr);
						arr.add(obj);
						utilization.put(name, arr);
						rDetailList.put(key, utilization);
					} else{
						utilization = new OrderedJSONObject();
						arr = new JSONArray();
						OrderedJSONObject obj = new OrderedJSONObject();
						obj.put("id", activityId);
						obj.put("internal_id", internalId);
						obj.put("task_name", taskName);
						obj.put("hrs", hrsstr);
						arr.add(obj);
						utilization.put(name, arr);
						rDetailList.put(key, utilization);
					}
				}
			}
		}
		System.out.println("Resource Detail:"+rDetailList.size());

		String sql = "select id from enterprise_asset where asset_name = ?";
		String peoplesql = "insert into enterprise_asset(id,asset_name,asset_category,extension,created_by,updated_by,company_code) values(?,?,'PEOPLE',json_object(),'support@smarterd.com','support@smarterd.com',?)";
		Iterator iter = rDetailList.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			OrderedJSONObject utilization = (OrderedJSONObject)rDetailList.get(key);
			int count = utilization.size();
			Iterator utilIter = utilization.keySet().iterator();
			while(utilIter.hasNext()) {
				String name = (String)utilIter.next();
				JSONArray arr = (JSONArray)utilization.get(name);
				Iterator arrIter = arr.iterator();
				while(arrIter.hasNext()) {
					OrderedJSONObject obj = (OrderedJSONObject)arrIter.next();
					String activityId = (String)obj.get("id");
					String internalId = (String)obj.get("internal_id");
					String taskName = (String)obj.get("task_name");
					String hrs = (String)obj.get("hrs");
					double d = Double.parseDouble(hrs);
					d = d/count;
					hrs = df.format(d);

					String[] dates = key.split(":");
					String sd = dates[0];
					String ed = dates[1];

					String rId = null;
					PreparedStatement pst = conn.prepareStatement(sql);
					pst.setString(1, name);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						rId = rs.getString("id");
					}
					rs.close();
					pst.close();

					if(rId == null) {
						rId = generateUUID();
						pst = conn.prepareStatement(peoplesql);
						pst.setString(1, rId);
						pst.setString(2, name);
						pst.setString(3, companyCode);
						pst.executeUpdate();
						pst.close();
					}

					OrderedJSONObject ctx = new OrderedJSONObject();
					ctx.put("plan_internal_id", internalId);
					ctx.put("plan_name", taskName);
					ctx.put("asset_id", rId);
					ctx.put("asset_name", name);
					ctx.put("startdate", sd);
					ctx.put("enddate", ed);
					ctx.put("taskDetail", "");
					ctx.put("totaleffort", "0.0");
					ctx.put("role", "");
					ctx.put("uom", "Days");
					ctx.put("hoursperday", new Double(Double.parseDouble(hrs)));
					ctx.put("status", "Active");
					ctx.put("LINKED", "N");

					int id = createRel(activityId, rId, "ACTIVITY", "PEOPLE", ctx, "support@smarterd.com");
					ctx.put("linked_id", new Integer(id));
					planRelRollup(activityId, "ACTIVITY", rId, "PEOPLE", ctx);
				}
			}
		}

		conn.commit();
		conn.close();
	}

	private void planRelRollup(String planId, String planType, String componentId, String componentType, OrderedJSONObject ctxObj) throws Exception {
        System.out.println("In planRelUp..."+planId+":"+planType+":"+componentId+":"+componentType);
        if(planType.equalsIgnoreCase("SUBPLAN")) {
            String sql = "select threatPlan from enterprise_threat_subplan where _id = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, planId);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                String id = rs.getString("threatPlan");
                createPlanRel(id, "PLAN", componentId, componentType, ctxObj); 
            }
            rs.close();
            pst.close();
        } else if(planType.equalsIgnoreCase("ACTIVITY")) {
            String sql = "select subplan from enterprise_threat_subplan where _id = ?";
            String rootsql = "select root from enterprise_threat_subplan where _id = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, planId);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                String id = rs.getString("subplan");

                // Get root flag for this parent
                PreparedStatement pst1 = conn.prepareStatement(rootsql);
                pst1.setString(1, id);
                ResultSet rs1 = pst1.executeQuery();
                while(rs1.next()) {
                    boolean root = rs1.getBoolean("root");
                    if(root) {
                        createPlanRel(id, "SUBPLAN", componentId, componentType, ctxObj);
                        planRelRollup(id, "SUBPLAN", componentId, componentType, ctxObj);
                    } else {
                        createPlanRel(id, "ACTIVITY", componentId, componentType, ctxObj);
                        planRelRollup(id, "ACTIVITY", componentId, componentType, ctxObj);
                    }
                }
                rs1.close();
                pst1.close();
            }
            rs.close();
            pst.close();
        }
        System.out.println("Successfully Rolled Up Relationship!!!");
    }

	 private void createPlanRel(String planId, String planType, String componentId, String componentType, OrderedJSONObject ctxObj) throws Exception {
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_timestamp,created_by,updated_timestamp,updated_by,company_code) values(null, ?, ?, ?, ?, now(),?,now(),?,?)";
        String enterpriseRelUpdateSQL = "update enterprise_rel set asset_rel_context = ? where component_id = ? and asset_id = ?";
        String checksql = "select id, asset_rel_context from enterprise_rel where asset_id=? and component_id=?";
        String checksubplansql = "select min(json_unquote(json_extract(b.asset_rel_context, '$.\"startdate\"'))) startdate, max(json_unquote(json_extract(b.asset_rel_context, '$.\"enddate\"'))) enddate,sum(json_unquote(json_extract(b.asset_rel_context, '$.\"hoursperday\"'))) hoursperday from enterprise_threat_subplan a, enterprise_rel b where a.subplan=? and a.root=false and a._id = b.component_id and b.asset_id = ?";
        String checkplansql = "select min(json_unquote(json_extract(b.asset_rel_context, '$.\"startdate\"'))) startdate, max(json_unquote(json_extract(b.asset_rel_context, '$.\"enddate\"'))) enddate,sum(json_unquote(json_extract(b.asset_rel_context, '$.\"hoursperday\"'))) hoursperday from enterprise_threat_subplan a, enterprise_rel b where a.threatPlan=? and a.root=true and a._id = b.component_id and b.asset_id = ?";

		ctxObj.put("COMPONENT TYPE", componentType);
		ctxObj.put("LINKED", "Y");
		
		PreparedStatement pst1 = conn.prepareStatement(enterpriseRelInsertSQL);
		pst1.setString(1, componentId);
		pst1.setString(2, planId);
		pst1.setString(3, planType);
		pst1.setString(4, ctxObj.toString());
		pst1.setString(5, "support@smarterd.com");
		pst1.setString(6, "support@smarterd.com");
		pst1.setString(7, companyCode);
		pst1.addBatch();

		// Insert Reverse
		ctxObj.put("COMPONENT TYPE", planType);
		pst1.setString(1, planId);
		pst1.setString(2, componentId);
		pst1.setString(3, componentType);
		pst1.setString(4, ctxObj.toString());
		pst1.setString(5, "support@smarterd.com");
		pst1.setString(6, "support@smarterd.com");
		pst1.setString(7, companyCode);
		pst1.addBatch();
		pst1.executeBatch();
		pst1.close();
    }

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private int createRel(String componentId, String assetId, String componentType, String assetType, OrderedJSONObject ctx, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";
		String sql = "select id from enterprise_rel where id=last_insert_id()";

		PreparedStatement pst = conn.prepareStatement(enterpriseRelInsertSQL);
		ctx.put("LINKED", "N");
		ctx.put("COMPONENT TYPE", componentType);
		pst.setString(1, assetId);
		pst.setString(2, componentId);
		pst.setString(3, assetType);
		pst.setString(4, ctx.toString());
		pst.setString(5, updatedBy);
		pst.setString(6, updatedBy);
		pst.setString(7, companyCode);
		pst.addBatch();
		ctx.put("COMPONENT TYPE", assetType);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		pst.setString(3, componentType);
		pst.setString(4, ctx.toString());
		pst.setString(5, updatedBy);
		pst.setString(6, updatedBy);
		pst.setString(7, companyCode);
		pst.addBatch();
		pst.executeBatch();
		pst.close();

		int id = 0;
		pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getInt("id");
		}
		rs.close();
		pst.close();
		return id;
    }


	private int generateId(String str) throws Exception {
        String base64encodedString = Base64.getEncoder().encodeToString(str.getBytes("utf-8"));
		BigInteger i = new BigInteger(base64encodedString.getBytes());
		int j = i.intValue();
        return j;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		DHAPlanLoader loader = new DHAPlanLoader(filename, companyCode, env);
		loader.load();
	}
}