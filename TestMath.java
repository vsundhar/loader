import java.time.LocalDate;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.regex.Matcher;
import java.util.HashMap;
import java.util.regex.Pattern;

public class TestMath {
    private int[] markers = new int[] {100,150,105,120,90,80,50,75,75,70,80,90,100};
    private static final HashMap<String,String> sqlTokens;
    private static Pattern sqlTokenPattern;
    static
    {           
        //MySQL escape sequences: http://dev.mysql.com/doc/refman/5.1/en/string-syntax.html
        String[][] search_regex_replacement = new String[][]
        {
                    //search string     search regex        sql replacement regex
                {   "\u0000"    ,       "\\x00"     ,       "\\\\0"     },
                {   "'"         ,       "'"         ,       "\\\\'"     },
                {   "\""        ,       "\""        ,       "\\\\\""    },
                {   "\b"        ,       "\\x08"     ,       "\\\\b"     },
                {   "\n"        ,       "\\n"       ,       "\\\\n"     },
                {   "\r"        ,       "\\r"       ,       "\\\\r"     },
                {   "\t"        ,       "\\t"       ,       "\\\\t"     },
                {   "\u001A"    ,       "\\x1A"     ,       "\\\\Z"     },
                {   "\\"        ,       "\\\\"      ,       "\\\\\\\\"  }
        };

        sqlTokens = new HashMap<String,String>();
        String patternStr = "";
        for (String[] srr : search_regex_replacement)
        {
            sqlTokens.put(srr[0], srr[2]);
            patternStr += (patternStr.isEmpty() ? "" : "|") + srr[1];            
        }
        sqlTokenPattern = Pattern.compile('(' + patternStr + ')');
    }

    public void TestWorkNotes(String id, String workNotes, String componentType) throws Exception {
        String name = "";
        String[] list = workNotes.split("\\n");
        String ts = "", commentBy = "", note = "";
        boolean complete = false;
        for(int i=0; i<list.length; i++) {
            String str = list[i].trim();
            System.out.println("str:"+str);
            if(str.length() > 0) {
                if(str.toLowerCase().endsWith("(work notes)") || (str.toLowerCase().endsWith("(additional comments)") && !str.toLowerCase().startsWith("text :"))) {
                    if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, componentType);
                    }
                    note = "";
                    str = str.replaceAll("\\(Additional comments\\)", "").replaceAll("\\(Additional Comments\\)","");;
                    str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                    ts = str.substring(0, 18).trim();
                    commentBy = str.substring(22).trim();
                } else if(str.toLowerCase().indexOf("(work notes)") != -1) {
                    if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, componentType);
                    }
                    note = str.substring(str.toLowerCase().indexOf("(work notes)")+1);
                    str = str.substring(0, str.toLowerCase().indexOf("(work notes)")).trim();
                    str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)", "");
                    ts = str.substring(0, 18).trim();
                    commentBy = str.substring(19).trim();
                    /*
                    if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, componentType);
                    }
                    note = "";
                    str = str.replaceAll("\\(Additional comments\\)", "").replaceAll("\\(Additional Comments\\)","");;
                    str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                    ts = str.substring(0, 18).trim();
                    commentBy = str.substring(22).trim();
                    */
                } else if(str.toLowerCase().indexOf("(additional comments)") != -1 && !str.toLowerCase().startsWith("text :")) {
                    if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, componentType);
                    }
                    note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                    str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                    str = str.replaceAll("\\(Additional comments\\)", "");
                    ts = str.substring(0, 18).trim();
                    commentBy = str.substring(19).trim();
                    /*
                    if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, componentType);
                    }
                    note = "";
                    str = str.replaceAll("\\(Additional comments\\)", "").replaceAll("\\(Additional Comments\\)","");;
                    str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                    ts = str.substring(0, 18).trim();
                    commentBy = str.substring(22).trim();
                    */
                } else {
                    if(note.length() == 0) {
                        note = str.trim();
                    } else {
                        note = note + "\n"+ str.trim();
                    }
                }
            }
        }
        if(note != null && note.length() > 0) {
            //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
            //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
            addWorkNotes(id, name, ts, commentBy, note, componentType);
        }
    }

    private void addWorkNotes(String id, String name, String ts, String commentBy, String workNotes, String componentType) throws Exception {
		System.out.println("In addWorkNotes..."+id+":"+commentBy+":"+ts);

        // Remove Timezone from Timestamp - for compatible with database
        ts = ts.replaceAll("PST", "").trim();
        /*
        String checksql = "select count(*) count from enterprise_comment where comp_id = ? and comment = ?";
        int count = 0;
        PreparedStatement pst = conn.prepareStatement(checksql);
        pst.setString(1, id);
        pst.setString(2, workNotes);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            count = rs.getInt("count");
        }
        rs.close();
        pst.close();
        
        if(count == 0) {
            System.out.println("Adding WorkNote...");
            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("component_name", name);
            extObj.put("component_type", componentType);
            String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,comment_address,created_by,updated_by,created_timestamp,updated_timestamp,comment_type,extension,company_code) values(uuid(),?,'Work Note',?,?,?,?,?,?,'WORK NOTE',?,?)";
            pst = conn.prepareStatement(insertsql);
            pst.setString(1, id);
            pst.setString(2, workNotes);
            pst.setString(3, commentBy);
            pst.setString(4, commentBy);
            pst.setString(5, commentBy);
            pst.setString(6, ts);
            pst.setString(7, ts);
            pst.setString(8, extObj.toString());
            pst.setString(9, companyCode);
            System.out.println(pst.toString());
            pst.executeUpdate();
            pst.close();
        } else {
            System.out.println("WorkNote exists!!!");
        }
        */
	}


    public static String escape(String s)
    {
        Matcher matcher = sqlTokenPattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        while(matcher.find())
        {
            matcher.appendReplacement(sb, sqlTokens.get(matcher.group(1)));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public TestMath() {}

    public static void main(String[] args) throws Exception {
        /*
        int cost=50, impact=50, efficacy=50; 
        int cieRisk = (int)((cost+impact)*1/efficacy);

        int percentComplete = Math.round((float)5 / 28 * 100);
        System.out.println(percentComplete);
        */

        String startdate = "12/15/2022";
        String fiscalyearstart = "01/01";
        String fiscalYear = "FY21";
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate firstDate = LocalDate.parse(startdate, df);
        LocalDate secondDate = LocalDate.parse(fiscalyearstart+"/20"+fiscalYear.substring(2), df);
        String startyear = Integer.toString(firstDate.getYear());
        long days = ChronoUnit.DAYS.between(secondDate, firstDate);
        long months = ChronoUnit.MONTHS.between(secondDate, firstDate);
        long years = ChronoUnit.YEARS.between(secondDate, firstDate);
        //System.out.println(days+":"+months+":"+years);

        LocalDate currentDate = LocalDate.now();
        String dueDate = "06/24/2023";
        long remainingDays = 0;
        LocalDate dd = null;
        if(dueDate != null && dueDate.length() > 0) {
            dd = LocalDate.parse(dueDate, df);
        } else {
            dd = LocalDate.now();
        }
        remainingDays = ChronoUnit.DAYS.between(currentDate, dd);
        System.out.println("Remaining Days:"+remainingDays);

        /*
        if(days < 0 && months < 0) {
            fiscalYear = "FY"+fiscalYear.substring(2);
        } else if(days >= 0 && months >= 0) {
            int y = Integer.parseInt(fiscalYear.substring(2))+(int)years+1;
            fiscalYear = "FY"+y;
        } else {
            fiscalYear = "FY"+startyear.substring(2);
        }
        */

        /*
        int month = firstDate.getMonthValue();
        int year = firstDate.getYear();
        int fiscalMonth = secondDate.getMonthValue();
        System.out.println(month+":"+fiscalMonth+":"+year);
        int fy = 0;
        if(fiscalMonth == 1) {
            fy = year;
        } else {
            fy = (month >= fiscalMonth) ? year+1 : year;
        }


        System.out.println(Integer.toString(fy).substring(2));
        */
    
        /*
        String roadmapDate = "Q42020";
        String y = roadmapDate.substring(2);
        String quarter = roadmapDate.substring(1, 2);
        System.out.println(y+":"+quarter);
        */

        /*
        // Variables to hold the regular price, the
        // amount of a discount, and the sale price.
        double regularPrice = 59.0;
        
        // Calculate the amount of a 20% discount.
        double discount = regularPrice * 0.2;
        
        // Calculate the sale price by subtracting
        // the discount from the regular price.
        double salePrice = regularPrice - discount;
        String str = "[\"1\",\"2\"]";
        System.out.println(str.indexOf("["));
        */
        
        // Display the results.
        /*
        System.out.println("Regular price: $" + regularPrice);
        System.out.println("Discount amount: $" + discount);
        System.out.println("Sale price: $" + salePrice); 
        */
        /*
        int totalMaturity = 2, count = 22;
        BigDecimal avgMaturity = new BigDecimal((double)totalMaturity/count).setScale(1, RoundingMode.HALF_DOWN);
        System.out.println(totalMaturity+":"+avgMaturity.doubleValue());
        //MathContext m = new MathContext(2);
        //avgMaturity = avgMaturity.round(m);

        System.out.println(totalMaturity+":"+avgMaturity.toString());
        */
        /*
        String str = "ddd", content = "$71.06";
        content = Matcher.quoteReplacement(content);
        String replaced = str.replaceFirst(".*", content);
        System.out.println(replaced);
        */

        /*
        String str = "-0082";
        System.out.println(str.matches("-?\\d+(\\.\\d+)?"));
        */

        /*
        String ports = "443";
        String[] str = ports.split(",");
        for(int j=0; j< str.length; j++) {
            System.out.println(str[j].trim());
        }
        */

        /*
        String str = "Test [a,b]";
        System.out.println(str.substring(str.length()-1));
        */
        //String str = "\ufffd\\tCombine inseams to cover the full range of Xshort to Xlong.  \\n     o\\tCurrently they are all broken out\\n\ufffd\\tAbility to make adjustments on the packsheet approval tab\\n     o\\tCurrently we can only break packs\\n\ufffd\\tMerch PO Creation tab\\n     o\\tAdd BBR # to the pack sheets\\n     o\\tBetter readability.  Merchants only need bulk and pack information.  \\n     o\\tAdjust the numeric order.\\n            _\\tCurrently 6, 7 ,8 ,9 ,10, 11, 12\\n            _\\tRequest is 10, 11, 12, 6, 7, 8, 9.  This is how it is reflect in the ordering system\\n     o\\tGray out unneeded cells or automatic hiding of unneeded information.\\n     o\\tDefault to show one month on the merch PO creation tab instead of both months. \\n\ufffd\\tColor theme, need to simplify";
        /*
        String str = "To replace our current Pack Sheet.   This this an excel tool that we use to provide Size and Pack information to Merchants to writing PO's.";
        if(str.indexOf("\'") != -1 && str.indexOf("\\'") != -1) {
            System.out.println(str);
            System.out.println("------");
            str = escape(str);
            System.out.println(str);
        }
        */
        /*
        String appCode = "EBS1";
        String ch = appCode.substring(appCode.length()-1);
        System.out.println(ch);
        if(onlyDigits(ch)) {
            appCode = appCode.substring(0,appCode.length()-1);
        }
        System.out.println(appCode);
        */
        /*
        String str = "dc2pech2esx6.mcorp.mauricesinc.com";
        //String regex = "^.*[a-zA-Z]+.*$";
        //Pattern p = Pattern.compile(regex);
        //Matcher m = p.matcher(str);
        System.out.println(str.matches("^.*[a-zA-Z]+.*$"));
        */
        //String scanId = "CVE-2019-18919-193.28.23.1-443";
        /*
        Hashids hashids = new Hashids("this is my salt");
        long[] hash = hashids.decode(scanId);
        */
        //System.out.println(Math.abs(scanId.hashCode()));
    }

    private static boolean onlyDigits(String str) {
        // Traverse the string from start to end
		int n = str.length();
        for (int i = 0; i < n; i++) {
            // Check if character is
            // not a digit between 0-9
            // then return false
            if (str.charAt(i) < '0' || str.charAt(i) > '9') {
                return false;
            }
        }
		// If we reach here, that means
		// all characters were digits.
        return true;
    }
}