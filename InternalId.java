
import java.util.regex.*;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import org.apache.wink.json4j.OrderedJSONObject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.time.DayOfWeek;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.sql.*;

public class InternalId {
	Connection conn;
	public InternalId() {
	}
	private void updateInternalId() throws Exception {
		int planCount = 0;
		mysqlConnect("MAUR", "prod");

		String plansql = "select _id, extension from enterprise_threat_plan";
		String updatesql = "update enterprise_threat_plan set extension=? where _id=?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(plansql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String ext = rs.getString("extension");
			planCount++;
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			extObj.put("internal_id", String.valueOf(planCount));
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.addBatch();
			System.out.println("-------------------------");
			System.out.println(planCount);
			processSubPlan(id, String.valueOf(planCount), true);
			System.out.println("-------------------------");
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
		conn.close();
	}

	private void processSubPlan(String planId, String planInternalId, boolean root) throws Exception {
		int planCount = 0;
		String plansql = "";
		if(root) {
			plansql = "select _id, extension from enterprise_threat_subplan where threatPlan=?";
		} else {
			plansql = "select _id, extension from enterprise_threat_subplan where subplan=?";
		}
		String updatesql = "update enterprise_threat_subplan set extension=? where _id=?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(plansql);
		pst.setString(1, planId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String ext = rs.getString("extension");
			planCount++;
			String internalId = planInternalId+"."+String.valueOf(planCount);
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			extObj.put("internal_id", internalId);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.addBatch();
			System.out.println(internalId);
			processSubPlan(id, internalId, false);
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();

	}

	private void mysqlConnect(String dbname, String env) throws Exception {
		String hostname = "35.197.106.183";
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	public static void main(String[] args) throws Exception {
		InternalId internalId = new InternalId();
		internalId.updateInternalId();
	}
}