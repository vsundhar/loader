import java.sql.*;
import java.util.*;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class SEAssetLoader {
    private String companyCode = "SE";
    private String hostname = "localhost";
    private Connection conn = null;
    private CSVReader reader = null;

    public SEAssetLoader(String filename, String env) throws Exception {
        reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);

        if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		} else {
            hostname = "localhost";
        }
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    private OrderedJSONObject loadAsset() throws Exception {
        System.out.println("Loading SE Asset ...");
        OrderedJSONObject res = new OrderedJSONObject();
        String insertSQL = "insert into enterprise_asset(id,asset_name,asset_desc,extension,asset_type,updated_by,company_code) value(uuid(), ?, ?, ?, 'ASSET', 'support@smarterd.com','SE')";
        String updateSQL = "update enterprise_asset set extension = ? where id = ?";
        String checkSQL = "select id, extension from enterprise_asset where asset_name like ? and asset_type = 'ASSET'";
        PreparedStatement pst1 = conn.prepareStatement(insertSQL);
        PreparedStatement pst2 = conn.prepareStatement(updateSQL);

        String[] row;
		int rowCount=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String assetName=null, assetDesc=null, category=null, mfg=null, licenseMethod=null, ecosystem=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						assetName = row[i].trim();
					} else if(i==1) {
						assetDesc = row[i].trim();
					} else if(i==2) {
						ecosystem = row[i].trim();
					} else if(i==3) {
						licenseMethod = row[i].trim();
					} else if(i==4) {
						mfg = row[i].trim();
					} else if(i==5) {
						category = row[i].trim();
					}
                }

                String id = null, ext=null;
                PreparedStatement pst = conn.prepareStatement(checkSQL);
                pst.setString(1, assetName);
                ResultSet rs = pst.executeQuery();
                while(rs.next()) {
                    id = rs.getString("id");
                    ext = rs.getString("extension");
                }
                rs.close();
                pst.close();

                OrderedJSONObject extObj = null;
                if(id == null) {
                    extObj = new OrderedJSONObject();
                    JSONArray arr = new JSONArray();
                    if(ecosystem != null && ecosystem.length() > 0) {
                        String[] str = ecosystem.split(",");
                        for(int i=0; i<str.length; i++) {
                            arr.add(str[i].trim());
                        }
                    }
                    JSONArray categoryArr = new JSONArray();
                    if(category != null && category.length() > 0) {
                        String[] str = category.split(",");
                        for(int i=0; i<str.length; i++) {
                            categoryArr.add(str[i].trim());
                        }
                    }
                    extObj.put("Ecosystem", arr);
                    extObj.put("OEM Vendor", mfg);
                    extObj.put("Category", categoryArr);
                    extObj.put("License Type", licenseMethod);
                    extObj.put("Sub-Type", "Cybersecurity");
                    
                    pst1.setString(1, assetName);
                    pst1.setString(2, assetDesc);
                    pst1.setString(3, extObj.toString());
                    pst1.addBatch();
                } else {
                    extObj = new OrderedJSONObject(ext);
                    JSONArray arr = null;
                    if(extObj.has("Ecosystem")) {
                        arr = (JSONArray)extObj.get("Ecosystem");
                    } else {
                        arr = new JSONArray();
                    }

                    if(ecosystem != null && ecosystem.length() > 0) {
                        String[] str = ecosystem.split(",");
                        for(int i=0; i<str.length; i++) {
                            if(!arr.contains(str[i])) {
                                arr.add(str[i].trim());
                            }
                        }
                    }
                    extObj.put("Ecosystem", arr);
                    
                    if(extObj.has("Category")) {
                        arr = (JSONArray)extObj.get("Category");
                    } else {
                        arr = new JSONArray();
                    }
                    if(category != null && category.length() > 0) {
                        String[] str = category.split(",");
                        for(int i=0; i<str.length; i++) {
                            if(!arr.contains(str[i])) {
                                arr.add(str[i].trim());
                            }
                        }
                    }
                    extObj.put("Category", arr);
                    
                    pst2.setString(1, extObj.toString());
                    pst2.setString(2, id);
                    pst2.addBatch();
                }
            }
        }

        pst1.executeBatch();
        pst2.executeBatch();
        conn.commit();

        pst1.close();
        pst2.close();

        res.put("STATUS", "Successfully loaded Asset!!!");
        System.out.println("Successfully loaded Asset["+rowCount+"]!!!");
        return res;
    }

    private void mysqlConnect(String dbname) throws Exception {
        String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String filename = args[0];
        String env = args[1];
        
        SEAssetLoader loader = new SEAssetLoader(filename, env);
        loader.loadAsset();
    } 

}