import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Base64;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.Project;
import net.rcarz.jiraclient.ProjectCategory;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.IssueType;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Watches;
import net.sf.json.JSON;
import net.sf.json.JSONObject;

import net.rcarz.jiraclient.Comment;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.agile.AgileClient;
import net.rcarz.jiraclient.agile.Sprint;
import net.rcarz.jiraclient.agile.TimeTracking;
import net.rcarz.jiraclient.agile.Resolution;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.ContentType;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
 
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.nio.file.Files;
import java.nio.file.*;

/*
   1 - SC Request Date format requires str_to_date conversion
   2 - Enabled Unicode Charset at the DB Table and hence not required to remove special chars from string
*/

public class SmarteServiceRequest {
   private String companyCode = null;
   private Connection conn = null;
   String hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
   String filename = "";
   FileWriter fw = null;
   
   public SmarteServiceRequest(String companyCode, String env, String filename) throws Exception {
      this.companyCode = companyCode;
      this.filename =filename;

      if(env.equalsIgnoreCase("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equalsIgnoreCase("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
      // Connect Mysql
      mysqlConnect(companyCode);
   }

   public void setFile(String filename) {
      this.filename = filename;
   }

   private void loadSolarwindsSR() throws Exception {
      String[] row;
		int count = 0, rowCount = 0;

		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
            System.out.println("Processing row..."+rowCount);
            String reqNo = "", name = "", desc = "", requestedBy = "", assignee = "", assignmentGroup="";
            String loc="", openedBy="", openedAt="",closedBy="",closedAt="",order="",duedate="",instruction="",requestedDate="";
            String priority="", requestedState="", state="", updatedBy="", updatedAt="", comment="", watchList="", requestDetails="", attachment=""; 
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
                  reqNo = row[i].trim();
					} else if(i==1) {		
                  name = row[i].trim();
					} else if(i==2) {		
                  desc = row[i].trim();
					} else if(i==3) {		
                  requestedBy = row[i].trim();
					} else if(i==4) {		
                  assignee = row[i].trim();
					} else if(i==5) {		
                  assignmentGroup = row[i].trim();
					} else if(i==6) {		
                  loc = row[i].trim();
					} else if(i==7) {		
                  order = row[i].trim();
					} else if(i==8) {		
                  duedate = row[i].trim();
					} else if(i==9) {		
                  instruction = row[i].trim();
					} else if(i==10) {		
                  priority = row[i].trim();
					} else if(i==11) {		
                  state = row[i].trim();
					} else if(i==12) {		
                  comment = row[i].trim();
					} else if(i==13) {		
                  watchList = row[i].trim();
					} else if(i==14) {		
                  openedAt = row[i].trim();
					} else if(i==15) {		
                  openedBy = row[i].trim();
					} else if(i==16) {		
                  updatedAt = row[i].trim();
					} else if(i==17) {		
                  updatedBy = row[i].trim();
					} else if(i==18) {		
                  closedAt = row[i].trim();
					} else if(i==19) {		
                  closedBy = row[i].trim();
					} else if(i==20) {		
                  requestDetails = row[i].trim();
					} else if(i==21) {		
                  attachment = row[i].trim();
					} 
				}
            // Get Assignment Group
            if(assignmentGroup.indexOf("~") != -1) {
               String[] group = assignmentGroup.split("~");
               assignmentGroup = group[1].replaceAll("\"", "").trim();
            }
            // Format Date
            openedAt = openedAt+":00";
            closedAt = closedAt+":00";
            updatedAt = updatedAt+":00";

            System.out.println(openedAt+":"+updatedAt+":"+closedAt);

            DateTimeFormatter df1 = DateTimeFormatter.ofPattern("M/d/yy HH:mm:ss"); // 15
            DateTimeFormatter df2 = DateTimeFormatter.ofPattern("M/d/yy H:mm:ss");  // 14
            DateTimeFormatter df3 = DateTimeFormatter.ofPattern("MM/d/yy H:mm:ss"); // 15
            DateTimeFormatter df4 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); // Output format
            LocalDateTime dt = null;
            if(openedAt.indexOf("/") != -1 && openedAt.length() == 15) {
               try {
                  dt = LocalDateTime.parse(openedAt, df1);
                  openedAt = dt.format(df);
               } catch(Exception e) {
                  dt = LocalDateTime.parse(openedAt, df3);
                  openedAt = dt.format(df);
               }
            } else if(openedAt.indexOf("/") != -1 && openedAt.length() == 14) {
               dt = LocalDateTime.parse(openedAt, df2);
               openedAt = dt.format(df);
            } else if(openedAt.indexOf("-") != -1) {
               dt = LocalDateTime.parse(openedAt, df4);
               openedAt = dt.format(df);
            }

            if(updatedAt.indexOf("/") != -1 && updatedAt.length() == 15) {
               try {
                  dt = LocalDateTime.parse(updatedAt, df1);
                  updatedAt = dt.format(df);
               } catch(Exception e) {
                  dt = LocalDateTime.parse(updatedAt, df3);
                  updatedAt = dt.format(df);
               }
            } else if(updatedAt.indexOf("/") != -1 && updatedAt.length() == 14) {
               dt = LocalDateTime.parse(updatedAt, df2);
               updatedAt = dt.format(df);
            } else if(updatedAt.indexOf("-") != -1) {
               dt = LocalDateTime.parse(updatedAt, df4);
               updatedAt = dt.format(df);
            }

            /*
            openedAt = openedAt.replaceAll("-", "/");
            closedAt = closedAt.replaceAll("-", "/");
            updatedAt = updatedAt.replaceAll("-", "/");
            */
           
            //name = name.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
            //name = name.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
            //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
            //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
            
            // Check if request exists
            String id = null, srPriority = null;
            String checksql = "select id,priority from enterprise_itsm_service_request where ticket_ref = ?";
            PreparedStatement pst = conn.prepareStatement(checksql);
            pst.setString(1, reqNo);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
               id = rs.getString("id");
               srPriority = rs.getString("priority");
            }
            rs.close();
            pst.close();
            
            if(priority.indexOf("Low") != -1) {
               priority = "Low";
            } else if(priority.indexOf("Medium") != -1) {
               priority = "Medium";
            } else if(priority.indexOf("High") != -1) {
               priority = "High";
            } else if(priority.indexOf("Critical") != -1) {
               priority = "Critical";
            }

            // Use existing priority if already present
            if(srPriority != null && srPriority.length() > 0) {
               priority = srPriority;
            }

            //System.out.println(desc);
            if(id == null) {
               System.out.println("Adding...");
               //String insertsql = "insert into enterprise_itsm_service_request(id,service_type,ticket_ref,component_id,component_name,location,description,details,requested_by,opened,opened_by,assignee,assignment_group,environment,qty,due_date,estimated_date,risk,priority,status,request_type,request_details,extension,created_by,updated_by,closed_by,created_timestamp,updated_timestamp,closed) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%m/%d/%Y %H:%i:%s'),str_to_date(?,'%m/%d/%Y %H:%i:%s'),?)";
               String insertsql = insertsql = "insert into enterprise_itsm_service_request(id,service_type,ticket_ref,component_id,component_name,location,name,description,requested_by,opened,opened_by,assignee,assignment_group,environment,qty,due_date,estimated_date,risk,priority,status,request_type,request_details,extension,created_by,updated_by,closed_by,created_timestamp,updated_timestamp,closed) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
               id = generateUUID();
               OrderedJSONObject requestObj = new OrderedJSONObject();
               requestObj.put("details", requestDetails);

               OrderedJSONObject extObj = new OrderedJSONObject();
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, id);
               pst.setString(2, "SERVICE REQUEST");
               pst.setString(3, reqNo);
               pst.setString(4, "");
               pst.setString(5, "");
               pst.setString(6, loc);
               pst.setString(7, name);
               pst.setString(8, desc);
               pst.setString(9, requestedBy);
               pst.setString(10, openedAt);
               pst.setString(11, openedBy);
               pst.setString(12, assignee);
               pst.setString(13, assignmentGroup);
               pst.setString(14, "");  // Environment
               pst.setString(15, order);
               pst.setString(16, duedate);
               pst.setString(17, "");  // Estimated Date
               pst.setString(18, "");  // Risk
               pst.setString(19, priority);
               pst.setString(20, state);  // Overall Requested Status
               pst.setString(21, "");  
               pst.setString(22, requestObj.toString());
               pst.setString(23, extObj.toString());
               pst.setString(24, openedBy);
               pst.setString(25, updatedBy);
               pst.setString(26, closedBy);
               pst.setString(27, openedAt);
               pst.setString(28, updatedAt);
               pst.setString(29, closedAt);
               pst.executeUpdate();
               pst.close();
            } else {
               System.out.println("Updating SR..."+id);
               OrderedJSONObject requestObj = new OrderedJSONObject();
               requestObj.put("details", requestDetails);
               //String updatesql = "update enterprise_itsm_service_request set description=?, details=?, priority=?, status=?, due_date=?, assignee=?, assignment_group=?, request_details=?, created_by=?, created_timestamp=str_to_date(?,'%m/%d/%Y %H:%i:%s'), updated_by=?, updated_timestamp=str_to_date(?,'%m/%d/%Y %H:%i:%s'), closed_by=?, closed=? where id = ?";
               String updatesql = "update enterprise_itsm_service_request set name=?, description=?, priority=?, status=?, due_date=?, request_details=?, created_by=?, created_timestamp=?, updated_by=?, updated_timestamp=?, closed_by=?, closed=? where id = ?";
               pst = conn.prepareStatement(updatesql);
               pst.setString(1, name);
               pst.setString(2, desc);
               pst.setString(3, priority);
               pst.setString(4, requestedState);
               pst.setString(5, duedate);
               pst.setString(6, requestObj.toString());
               pst.setString(7, openedBy);
               pst.setString(8, openedAt);
               pst.setString(9, updatedBy);
               pst.setString(10, updatedAt);
               pst.setString(11, closedBy);
               pst.setString(12, closedAt);
               pst.setString(13, id);
               
               pst.executeUpdate();
               pst.close();
            }

            //System.out.println("Comment:"+comment);
            if(comment != null && comment.length() > 0) {
               System.out.println("comment:"+comment);
               String[] list = comment.split("\";");
               for(int i=0; i<list.length; i++) {
                  String notes = list[i];
                  String[] noteDetails = notes.split("\",");
                  String ts = openedAt;
                  String commentBy = noteDetails[1].replaceAll("\"", "").trim();
                  addWorkNotes(id, name, ts, commentBy, notes, "SERVICE REQUEST");
               }
            }

            if(attachment != null && attachment.length() > 0) {
               addAttachment(id, attachment, "SERVICE REQUEST");
            }
         }
      }

      /*
      // Update Scan Period for all Vulnerability Related Service Request
      // Get Current Scan Period
      String scanPeriod = getCurrentVulnerabilityPeriod();
      String updatesql = "update enterprise_itsm_service_request set extension=json_set(extension,'$.scan_period', ?) where request_type like 'Vulner%' and str_to_date(date_format(opened, '%m/%d/%Y'),'%m/%d/%Y') >= str_to_date((select max(json_unquote(json_extract(extension,'$.THREAT.\"Last Discovered On\"'))) from enterprise_vulnerability_log), '%m/%d/%Y')";
      PreparedStatement pst = conn.prepareStatement(updatesql);
      pst.setString(1, scanPeriod);
      pst.executeUpdate();
      pst.close();
      */

      conn.commit();
   }

   private void loadSR() throws Exception {
      String[] row;
		int count = 0, rowCount = 0;

		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
            System.out.println("Processing row..."+rowCount);
            String reqNo = "", name = "", desc = "", requestedBy = "", assignee = "", assignmentGroup="";
            String loc="", openedBy="", openedAt="",closedBy="",closedAt="",order="",duedate="",instruction="",requestedDate="";
            String priority="", requestedState="", state="", updatedBy="", updatedAt="", comment="", watchList=""; 
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
                  reqNo = row[i].trim();
					} else if(i==1) {		
                  name = row[i].trim();
					} else if(i==2) {		
                  desc = row[i].trim();
					} else if(i==3) {		
                  requestedBy = row[i].trim();
					} else if(i==4) {		
                  assignee = row[i].trim();
					} else if(i==5) {		
                  assignmentGroup = row[i].trim();
					} else if(i==6) {		
                  loc = row[i].trim();
					} else if(i==7) {		
                  order = row[i].trim();
					} else if(i==8) {		
                  duedate = row[i].trim();
					} else if(i==9) {		
                  instruction = row[i].trim();
					} else if(i==10) {		
                  priority = row[i].trim();
					} else if(i==11) {		
                  requestedState = row[i].trim();
					} else if(i==12) {		
                  state = row[i].trim();
					} else if(i==13) {		
                  comment = row[i].trim();
					} else if(i==14) {		
                  watchList = row[i].trim();
					} else if(i==15) {		
                  openedAt = row[i].trim();
					} else if(i==16) {		
                  openedBy = row[i].trim();
					} else if(i==17) {		
                  updatedAt = row[i].trim();
					} else if(i==18) {		
                  updatedBy = row[i].trim();
					} else if(i==19) {		
                  closedAt = row[i].trim();
					} else if(i==20) {		
                  closedBy = row[i].trim();
					} 
				}
            System.out.println(openedAt+":"+updatedAt+":"+closedAt);
            //name = name.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
            //name = name.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
            //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
            //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
            
            // Check if request exists
            String id = null, srPriority = null;
            String checksql = "select id,priority from enterprise_itsm_service_request where ticket_ref = ?";
            PreparedStatement pst = conn.prepareStatement(checksql);
            pst.setString(1, reqNo);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
               id = rs.getString("id");
               srPriority = rs.getString("priority");
            }
            rs.close();
            pst.close();
            
            if(priority.indexOf("Low") != -1) {
               priority = "Low";
            } else if(priority.indexOf("Medium") != -1) {
               priority = "Medium";
            } else if(priority.indexOf("High") != -1) {
               priority = "High";
            } else if(priority.indexOf("Critical") != -1) {
               priority = "Critical";
            }

            // Use existing priority if already present
            if(srPriority != null && srPriority.length() > 0) {
               priority = srPriority;
            }

            //System.out.println(desc);
            if(id == null) {
               System.out.println("Adding...");
               //String insertsql = "insert into enterprise_itsm_service_request(id,service_type,ticket_ref,component_id,component_name,location,description,details,requested_by,opened,opened_by,assignee,assignment_group,environment,qty,due_date,estimated_date,risk,priority,status,request_type,request_details,extension,created_by,updated_by,closed_by,created_timestamp,updated_timestamp,closed) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,str_to_date(?,'%m/%d/%Y %H:%i:%s'),str_to_date(?,'%m/%d/%Y %H:%i:%s'),?)";
               String insertsql = "insert into enterprise_itsm_service_request(id,service_type,ticket_ref,component_id,component_name,location,name,description,requested_by,opened,opened_by,assignee,assignment_group,environment,qty,due_date,estimated_date,risk,priority,status,request_type,request_details,extension,created_by,updated_by,closed_by,created_timestamp,updated_timestamp,closed) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
               id = generateUUID();
               OrderedJSONObject requestObj = new OrderedJSONObject();
               requestObj.put("request_state", state);
               OrderedJSONObject extObj = new OrderedJSONObject();
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, id);
               pst.setString(2, "SERVICE REQUEST");
               pst.setString(3, reqNo);
               pst.setString(4, "");
               pst.setString(5, "");
               pst.setString(6, loc);
               pst.setString(7, name);
               pst.setString(8, desc);
               pst.setString(9, requestedBy);
               pst.setString(10, openedAt);
               pst.setString(11, openedBy);
               pst.setString(12, assignee);
               pst.setString(13, assignmentGroup);
               pst.setString(14, "");  // Environment
               pst.setString(15, order);
               pst.setString(16, duedate);
               pst.setString(17, "");  // Estimated Date
               pst.setString(18, "");  // Risk
               pst.setString(19, priority);
               pst.setString(20, requestedState);  // Overall Requested Status
               pst.setString(21, "");  
               pst.setString(22, requestObj.toString());
               pst.setString(23, extObj.toString());
               pst.setString(24, openedBy);
               pst.setString(25, updatedBy);
               pst.setString(26, closedBy);
               pst.setString(27, openedAt);
               pst.setString(28, updatedAt);
               pst.setString(29, closedAt);
               pst.executeUpdate();
               pst.close();
            } else {
               System.out.println("Updating SR..."+id);
               OrderedJSONObject requestObj = new OrderedJSONObject();
               requestObj.put("request_state", state);
               //String updatesql = "update enterprise_itsm_service_request set description=?, details=?, priority=?, status=?, due_date=?, assignee=?, assignment_group=?, request_details=?, created_by=?, created_timestamp=str_to_date(?,'%m/%d/%Y %H:%i:%s'), updated_by=?, updated_timestamp=str_to_date(?,'%m/%d/%Y %H:%i:%s'), closed_by=?, closed=? where id = ?";
               String updatesql = "update enterprise_itsm_service_request set name=?, description=?, priority=?, status=?, due_date=?, request_details=?, created_by=?, created_timestamp=?, updated_by=?, updated_timestamp=?, closed_by=?, closed=? where id = ?";
               pst = conn.prepareStatement(updatesql);
               pst.setString(1, name);
               pst.setString(2, desc);
               pst.setString(3, priority);
               pst.setString(4, requestedState);
               pst.setString(5, duedate);
               pst.setString(6, requestObj.toString());
               pst.setString(7, openedBy);
               pst.setString(8, openedAt);
               pst.setString(9, updatedBy);
               pst.setString(10, updatedAt);
               pst.setString(11, closedBy);
               pst.setString(12, closedAt);
               pst.setString(13, id);
               
               pst.executeUpdate();
               pst.close();
            }

            //System.out.println("Comment:"+comment);
            if(comment != null && comment.length() > 0) {
               System.out.println("comment:"+comment);
               String[] list = comment.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  //System.out.println(str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(additional comments)") || str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("additional comments") != -1) {
                           if(note.length() > 0) {
                              //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                              //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                              addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                           }
                           note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                           str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                           str = str.replaceAll("\\(Additional comments\\)", "");
                           ts = str.substring(0, 18).trim();
                           commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
               }
            }
         }
      }

      // Update Scan Period for all Vulnerability Related Service Request
      // Get Current Scan Period
      String scanPeriod = getCurrentVulnerabilityPeriod();
      String updatesql = "update enterprise_itsm_service_request set extension=json_set(extension,'$.scan_period', ?) where request_type like 'Vulner%' and str_to_date(date_format(opened, '%m/%d/%Y'),'%m/%d/%Y') >= str_to_date((select max(json_unquote(json_extract(extension,'$.THREAT.\"Last Discovered On\"'))) from enterprise_vulnerability_log), '%m/%d/%Y')";
      PreparedStatement pst = conn.prepareStatement(updatesql);
      pst.setString(1, scanPeriod);
      pst.executeUpdate();
      pst.close();

      conn.commit();
   }

   private void loadCR() throws Exception {
		int count = 0, rowCount = 1;

		//CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
      CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
      for(CSVRecord row : dataReader) {
         String reqNo = "", name = "", desc = "", type = "", env = "", priority="", risk = "", requestedBy = "", requestedDate = "", assignee = "", assignmentGroup="";
         String tags = "", ciClass="", ci="", createdBy="", createdAt="",openedBy="", openedAt="",approval="",startDate="",endDate="",workstartDate="",workendDate="",state="";
         String updatedBy="", updatedAt="",closedBy="", closedAt="",duration="",cabapprovalDate="",cabreviewDate="",justification="",impact="";
         String impl="",testplan="",outageRequired="",onHold="",onHoldReason="",closeCode="",closeNotes="", comment="",workNotes="",workNoteList="";
         for(int i=0; i<row.size(); i++) {
            if(i==0) {		
               reqNo = row.get(i).trim();
            } else if(i==1) {		
               name = row.get(i).trim();
            } else if(i==2) {		
               desc = row.get(i).trim();
            } else if(i==3) {		
               type = row.get(i).trim();
            } else if(i==4) {		
               env = row.get(i).trim();
            } else if(i==5) {		
               priority = row.get(i).trim();
            } else if(i==6) {		
               risk = row.get(i).trim();
            } else if(i==7) {		
               requestedBy = row.get(i).trim();
            } else if(i==8) {		
               requestedDate = row.get(i).trim();
            } else if(i==9) {		
               assignee = row.get(i).trim();
            } else if(i==10) {		
               assignmentGroup = row.get(i).trim();
            } else if(i==11) {		
               tags = row.get(i).trim();
            } else if(i==12) {		
               ciClass = row.get(i).trim();
            } else if(i==13) {		
               ci = row.get(i).trim();
            }  else if(i==14) {		
               createdAt = row.get(i).trim();
            } else if(i==15) {		
               createdBy = row.get(i).trim();
            } else if(i==16) {		
               openedAt = row.get(i).trim();
            } else if(i==17) {		
               openedBy = row.get(i).trim();
            } else if(i==18) {		
               approval = row.get(i).trim();
            } else if(i==19) {		
               startDate = row.get(i).trim();
            } else if(i==20) {		
               endDate = row.get(i).trim();
            } else if(i==21) {		
               workstartDate = row.get(i).trim();
            } else if(i==22) {		
               workendDate = row.get(i).trim();
            } else if(i==23) {		
               state = row.get(i).trim();
            } else if(i==24) {		
               updatedBy = row.get(i).trim();
            } else if(i==25) {		
               updatedAt = row.get(i).trim();
            } else if(i==26) {		
               closedBy = row.get(i).trim();
            } else if(i==27) {		
               closedAt = row.get(i).trim();
            } else if(i==28) {		
               duration = row.get(i).trim();
            } else if(i==29) {		
               cabapprovalDate = row.get(i).trim();
            } else if(i==30) {		
               cabreviewDate = row.get(i).trim();
            } else if(i==31) {		
               justification = row.get(i).trim();
            } else if(i==32) {		
               impact = row.get(i).trim();
            } else if(i==33) {		
               impl = row.get(i).trim();
            } else if(i==34) {		
               testplan = row.get(i).trim();
            } else if(i==35) {		
               outageRequired = row.get(i).trim();
            } else if(i==36) {		
               onHold = row.get(i).trim();
            } else if(i==37) {		
               onHoldReason = row.get(i).trim();
            } else if(i==38) {		
               closeCode = row.get(i).trim();
            } else if(i==39) {		
               closeNotes = row.get(i).trim();
            } else if(i==40) {		
               comment = row.get(i).trim();
            } else if(i==41) {		
               workNotes = row.get(i).trim();
            } else if(i==42) {		
               workNoteList = row.get(i).trim();
            }
         }

         System.out.println("Processing row..."+rowCount+":"+reqNo);

         if(priority.indexOf("1") != -1) {
            priority = "Critical";
         } else  if(priority.indexOf("2") != -1) {
            priority = "High";
         } else  if(priority.indexOf("3") != -1) {
            priority = "Medium";
         } else  if(priority.indexOf("4") != -1) {
            priority = "Low";
         }

         //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");

         // Check if request exists
         String id = null, ext = null;
         String checksql = "select id, extension from enterprise_itsm_service_request where ticket_ref = ?";
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, reqNo);
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
            id = rs.getString("id");
            ext = rs.getString("extension");
         }
         rs.close();
         pst.close();

         OrderedJSONObject extObj = null;
         if(id != null) {
            extObj = new OrderedJSONObject(ext);
         } else {
            extObj = new OrderedJSONObject();
         }
         extObj.put("tags", tags);
         extObj.put("justification", justification);
         extObj.put("implementation", impl);
         extObj.put("impact", impact);
         extObj.put("validation", testplan);
         extObj.put("outage_required", outageRequired);
         extObj.put("env", env);
         extObj.put("on_hold", onHold);
         extObj.put("on_hold_reason", onHoldReason);
         extObj.put("close_code", closeCode);
         extObj.put("close_notes", closeNotes);
         extObj.put("cab_review_date", cabreviewDate);
         extObj.put("cab_approval_date", cabapprovalDate);
         extObj.put("cab_attendees", new JSONArray());
         extObj.put("planned_start_date", startDate);
         extObj.put("planned_end_date", endDate);
         extObj.put("actual_start_date", workstartDate);
         extObj.put("actual_end_date", workendDate);
         extObj.put("duration", duration);
         extObj.put("requested_by", requestedBy);
         extObj.put("requested_date", requestedDate);

         //System.out.println(desc);
         //System.out.println(workNoteList);
         if(id == null) {
            String insertsql = "insert into enterprise_itsm_service_request(id,service_type,ticket_ref,component_id,component_name,location,name,description,requested_by,opened,opened_by,assignee,assignment_group,environment,qty,due_date,estimated_date,risk,priority,status,request_type,request_details,extension,created_by,updated_by,closed_by,created_timestamp,updated_timestamp,closed) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            id = generateUUID();
            OrderedJSONObject requestObj = new OrderedJSONObject();

            pst = conn.prepareStatement(insertsql);
            pst.setString(1, id);
            pst.setString(2, "CHANGE REQUEST");
            pst.setString(3, reqNo);
            pst.setString(4, "");
            pst.setString(5, ci);
            pst.setString(6, "");
            pst.setString(7, name);
            pst.setString(8, desc);
            pst.setString(9, requestedBy);
            pst.setString(10, openedAt);
            pst.setString(11, openedBy);
            pst.setString(12, assignee);
            pst.setString(13, assignmentGroup);
            pst.setString(14, env);  // Environment
            pst.setString(15, "");
            pst.setString(16, "");
            pst.setString(17, "");  // Estimated Date
            pst.setString(18, risk);  // Risk
            pst.setString(19, priority);
            pst.setString(20, state);
            pst.setString(21, type);  
            pst.setString(22, requestObj.toString());
            pst.setString(23, extObj.toString());
            pst.setString(24, openedBy);
            pst.setString(25, updatedBy);
            pst.setString(26, closedBy);
            pst.setString(27, updatedAt);
            pst.setString(28, openedAt);
            pst.setString(29, closedAt);
            pst.executeUpdate();
            pst.close();
         } else {
            String updatesql = "update enterprise_itsm_service_request set name=?, description=?,assignee=?, assignment_group=?,environment=?,risk=?,priority=?,status=?,request_type=?,extension=?,updated_by=?,updated_timestamp=?,closed_by=?,closed=?,service_type='CHANGE REQUEST' where id=?";
            pst = conn.prepareStatement(updatesql);
            pst.setString(1, name);
            pst.setString(2, desc);
            pst.setString(3, assignee);
            pst.setString(4, assignmentGroup);
            pst.setString(5, env);  // Environment
            pst.setString(6, risk);  // Risk
            pst.setString(7, priority);
            pst.setString(8, state);
            pst.setString(9, type);  
            pst.setString(10, extObj.toString());
            pst.setString(11, updatedBy);
            pst.setString(12, updatedAt);
            pst.setString(13, closedBy);
            pst.setString(14, closedAt);
            pst.setString(15, id);
            System.out.println(pst.toString());
            pst.executeUpdate();
            pst.close();
         }

         //System.out.println("Work Note:"+workNotes);
         if(workNotes != null && workNotes.length() > 0) {
            String[] list = workNotes.split("\\n");
            String ts = "", commentBy = "", note = "";
            boolean complete = false;
            for(int i=0; i<list.length; i++) {
               String str = list[i].trim();
               System.out.println("str:"+str);
               if(str.trim().length() > 0) {
                  if(str.toLowerCase().endsWith("(work notes)")) {
                     if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                     }
                     note = "";
                     str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                     ts = str.substring(0, 18).trim();
                     commentBy = str.substring(22).trim();
                  } else if(str.toLowerCase().indexOf("(work notes)") != -1) {
                     if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                     }
                     note = str.substring(str.toLowerCase().indexOf("(work notes)")+1);
                     str = str.substring(0, str.toLowerCase().indexOf("(work notes)")).trim();
                     str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)", "");
                     ts = str.substring(0, 18).trim();
                     commentBy = str.substring(19).trim();
                  } else {
                     if(note.length() == 0) {
                        note = str.trim();
                     } else {
                        note = note + "\n"+ str.trim();
                     }
                  }
               }
            }
            if(note != null && note.length() > 0) {
               //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
               //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
               addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
            }
         }

         if(comment != null && comment.length() > 0) {
            System.out.println("comment:"+comment);
            String[] list = comment.split("\\n");
            String ts = "", commentBy = "", note = "";
            boolean complete = false;
            for(int i=0; i<list.length; i++) {
               String str = list[i].trim();
               System.out.println("str:"+str);
               if(str.length() > 0) {
                   if(str.toLowerCase().endsWith("(additional comments)") || str.toLowerCase().endsWith("(work notes)")) {
                     if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                     }
                     note = "";
                     str = str.replaceAll("\\(Additional comments\\)", "");
                     str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                     ts = str.substring(0, 18).trim();
                     commentBy = str.substring(22).trim();
                  } else if(str.toLowerCase().indexOf("(additional comments)") != -1) {
                     if(note.length() > 0) {
                        //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                        //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                        addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                     }
                     note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                     str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                     str = str.replaceAll("\\(Additional comments\\)", "");
                     ts = str.substring(0, 18).trim();
                     commentBy = str.substring(19).trim();
                  } else {
                     if(note.length() == 0) {
                        note = str.trim();
                     } else {
                        note = note + "\n"+ str.trim();
                     }
                  }
               }
            }
            if(note != null && note.length() > 0) {
               //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
               //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
               addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
            }
         }
         
         rowCount++;
      }

      conn.commit();
   }

   // Do not update opened, updated and closed timestamp as we don't have concept of SC Item
   // ServiceNow SC Item will update assignee, assignment group 
   private void loadSRItem() throws Exception {
      int rowCount = 1;
      String checksql = "select id, description,assignee,assignment_group,request_details from enterprise_itsm_service_request where ticket_ref = ?";
      String updatesql = "update enterprise_itsm_service_request set name=?, description=?, extension=json_set(extension, '$.ref_no', ?), requested_by=?, opened_by=?, due_date=?, qty=?, assignee=?, assignment_group=?, request_details=? where ticket_ref=?";
		//CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
      CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
      for(CSVRecord row : dataReader) {
         System.out.println("Processing row..."+rowCount);
         String ritmNo = "", reqNo = "", name = "", desc = "", state = "", requestedBy = "", dueDate="", qty = "", assignee = "", assignmentGroup="";
         String openedBy="", openedAt="", updatedBy="", updatedAt="", closedBy="", closedAt="",comment="",workNotes="";
         for(int i=0; i<row.size(); i++) {
            if(i==0) {		
               ritmNo = row.get(i).trim();
            } else if(i==1) {		
               name = row.get(i).trim();
            } else if(i==2) {		
               desc = row.get(i).trim();
            } else if(i==3) {		
               state = row.get(i).trim();
            } else if(i==4) {		
               reqNo = row.get(i).trim();
            } else if(i==5) {		
               requestedBy = row.get(i).trim();
            } else if(i==6) {		
               openedBy = row.get(i).trim();
            } else if(i==7) {		
               dueDate = row.get(i).trim();
            } else if(i==8) {		
               qty = row.get(i).trim();
            } else if(i==9) {		
               assignmentGroup = row.get(i).trim();
            } else if(i==10) {		
               assignee = row.get(i).trim();
            } else if(i==11) {		
               openedAt = row.get(i).trim();
            } else if(i==12) {		
               openedBy = row.get(i).trim();
            } else if(i==13) {		
               updatedAt = row.get(i).trim();
            } else if(i==14) {		
               updatedBy = row.get(i).trim();
            } else if(i==15) {		
               closedAt = row.get(i).trim();
            } else if(i==16) {		
               closedBy = row.get(i).trim();
            } else if(i==17) {		
               workNotes = row.get(i).trim();
            } else if(i==18) {		
               comment = row.get(i).trim();
            } 
         }

         //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");

         /*
         if(state.equalsIgnoreCase("Waiting for Approval") || state.equalsIgnoreCase("approval")) {
            state = "Approval";
         } else if(state.equalsIgnoreCase("request approved") || state.equalsIgnoreCase("request_approved")) {
            state = "Approved";
         } else if(state.equalsIgnoreCase("Request Cancelled")) {
            state = "Cancelled";
         } else if(state.equalsIgnoreCase("complete")) {
            state = "Complete";
         }
         */

         String id=null, detail=null, rmAssignee=null, rmGroup=null, reqDetails=null;
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, reqNo);
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
            id = rs.getString("id");
            detail = rs.getString("description");
            rmAssignee = rs.getString("assignee");
            rmGroup = rs.getString("assignment_group");
            reqDetails = rs.getString("request_details");
         }
         rs.close();
         pst.close();

         if(detail != null && detail.length() > 0 && desc.length() == 0) {
            desc = detail;
         } else if(detail != null && detail.length() == 0 && desc.length() > 0) {
            desc = detail + "\n" + desc;
         }
         if((rmAssignee != null && rmAssignee.length() > 0)) {
            assignee = rmAssignee;
         }
         if((rmGroup != null && rmGroup.length() > 0)) {
            assignmentGroup = rmGroup;
         }

         if(id != null) {
            OrderedJSONObject reqDetailObj = null;
            if(reqDetails != null && reqDetails.length() > 0) {
               reqDetailObj = new OrderedJSONObject(reqDetails);
            } else {
               reqDetailObj = new OrderedJSONObject();
            }
            reqDetailObj.put("request_item_state", state);
            pst = conn.prepareStatement(updatesql);
            pst.setString(1, name);
            pst.setString(2, desc);
            pst.setString(3, ritmNo);
            pst.setString(4, requestedBy);
            pst.setString(5, openedBy);
            pst.setString(6, dueDate);
            pst.setString(7, qty);
            pst.setString(8, assignee);
            pst.setString(9, assignmentGroup);
            pst.setString(10, reqDetailObj.toString());
            pst.setString(11, reqNo);
            pst.executeUpdate();
            pst.close();

             //System.out.println("Work Note:"+workNotes);
            if(workNotes != null && workNotes.length() > 0) {
               String[] list = workNotes.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(work notes)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(work notes)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(work notes)")).trim();
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
               }
            }

            if(comment != null && comment.length() > 0) {
               System.out.println("comment:"+comment);
               String[] list = comment.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(additional comments)") || str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(additional comments)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(id, name, ts, commentBy, note, "SERVICE REQUEST");
               }
            }
         } else {
            System.out.println("Service Request not found:"+reqNo);
         }
         rowCount++;
      }
      conn.commit();
   }

   private void loadSRTask() throws Exception {
      int rowCount = 1;
      String checksql = "select id,name,assignee,assignment_group from enterprise_itsm_service_request where ticket_ref = ?";
      String checktasksql = "select id from enterprise_comment where json_extract(extension, '$.ref_no') = ?";
      String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,company_code,created_by,created_timestamp,comment_address,status,updated_by,updated_timestamp,comment_type,extension) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
      String updatetasksql = "update enterprise_comment set status=?,updated_by=?,updated_timestamp=?,extension=? where id = ?";
      String updatesql = "update enterprise_itsm_service_request set assignee=?, assignment_group=? where id = ?";
		
      CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
      for(CSVRecord row : dataReader) {
         System.out.println("Processing SC Task row..."+rowCount);
         String taskNo = "", taskType = "", name = "", priority = "", reqNo = "", requestType = "", desc = "", state = "", createdBy = "", createdAt = "", dueDate="", qty = "", assignee = "", assignmentGroup="", updatedBy="", updatedAt="", closedBy = "", closedAt = "", workstartDate = "", workendDate = "", workNotes = "", comment = "";
         for(int i=0; i<row.size(); i++) {
            if(i==0) {		
               taskNo = row.get(i).trim();
            } else if(i==1) {		
               taskType = row.get(i).trim();
            } else if(i==2) {		
               name = row.get(i).trim();
            } else if(i==3) {		
               desc = row.get(i).trim();
            } else if(i==4) {		
               priority = row.get(i).trim();
            } else if(i==5) {		
               state = row.get(i).trim();
            } else if(i==6) {		
               assignmentGroup = row.get(i).trim();
            } else if(i==7) {		
               assignee = row.get(i).trim();
            } else if(i==8) {		
               reqNo = row.get(i).trim();
            } else if(i==9) {		
               workstartDate = row.get(i).trim();
            } else if(i==10) {		
               workendDate = row.get(i).trim();
            } else if(i==11) {		
               createdBy = row.get(i).trim();
            } else if(i==12) {		
               createdAt = row.get(i).trim();
            } else if(i==13) {		
               updatedBy = row.get(i).trim();
            } else if(i==14) {		
               updatedAt = row.get(i).trim();
            } else if(i==15) {		
               closedBy = row.get(i).trim();
            } else if(i==16) {		
               closedAt = row.get(i).trim();
            } else if(i==17) {		
               workNotes = row.get(i).trim();
            } else if(i==18) {		
               comment = row.get(i).trim();
            }
         }

         //name = name.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //name = name.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
         //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");

         if(state.equalsIgnoreCase("Work In Progress")) {
            state = "In Progress";
         }
         if(workstartDate == null) {
            workstartDate = "";
         }
         if(workendDate == null) {
            workendDate = "";
         }

         String id = null, compName = null, scAssignee = null, scassignmentGroup = null;
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, reqNo);
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
            id = rs.getString("id");
            compName = rs.getString("name");
            scAssignee = rs.getString("assignee");
            scassignmentGroup = rs.getString("assignment_group");
         }
         rs.close();
         pst.close();

         if(id != null) {
            String taskId = null;
            pst = conn.prepareStatement(checktasksql);
            pst.setString(1, taskNo);
            rs = pst.executeQuery();
            while(rs.next()) {
               taskId = rs.getString("id");
            }
            rs.close();
            pst.close();

            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("ref_no", taskNo);
            extObj.put("assignee", assignee);
            extObj.put("dueDate", workendDate);
            extObj.put("type", taskType);
            extObj.put("component_type", "SERVICE REQUEST");
            extObj.put("component_name", compName);
            extObj.put("start_date", workstartDate);
            extObj.put("end_date", workendDate);
            extObj.put("closed_at", closedAt);
            extObj.put("closed_by", closedBy);

            if(taskId == null) {
               taskId = generateUUID();
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, taskId);
               pst.setString(2, id);
               pst.setString(3, name);
               pst.setString(4, desc);
               pst.setString(5, companyCode);
               pst.setString(6, createdBy);
               pst.setString(7, createdAt);
               pst.setString(8, "");
               pst.setString(9, state);
               pst.setString(10, updatedBy);
               pst.setString(11, updatedAt);
               pst.setString(12, "TASK");
               pst.setString(13, extObj.toString());
               pst.executeUpdate();
               pst.close();
            } else {
               pst = conn.prepareStatement(updatetasksql);
               pst.setString(1, state);
               pst.setString(2, updatedBy);
               pst.setString(3, updatedAt);
               pst.setString(4, extObj.toString());
               pst.setString(5, taskId);
               pst.executeUpdate();
               pst.close();
            }

            if(workNotes != null && workNotes.length() > 0) {
               String[] list = workNotes.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(taskId, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(work notes)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(taskId, name, ts, commentBy, note, "SERVICE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(work notes)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(work notes)")).trim();
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(taskId, name, ts, commentBy, note, "SERVICE REQUEST");
               }
            }

            if(comment != null && comment.length() > 0) {
               System.out.println("comment:"+comment);
               String[] list = comment.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(additional comments)") || str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(additional comments)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
               }
            }

            // Update Service Request
            if((scAssignee == null || scAssignee.length() == 0) && (assignee != null && assignee.length() > 0)) {
               scAssignee = assignee;
            }
            if((scassignmentGroup == null || scassignmentGroup.length() == 0) && (assignmentGroup != null && assignmentGroup.length() > 0)) {
               scassignmentGroup = assignmentGroup;
            }
            pst = conn.prepareStatement(updatesql);
            pst.setString(1, scAssignee);
            pst.setString(2, scassignmentGroup);
            pst.setString(3, id);
            pst.executeUpdate();
            pst.close();
         } else {
            System.out.println("Service Request not found:"+reqNo);
         }
         rowCount++;
      }
      conn.commit();
   }

   private void loadCRTask() throws Exception {
      int rowCount = 1;
      String checksql = "select id,name from enterprise_itsm_service_request where ticket_ref = ?";
      String checktasksql = "select id from enterprise_comment where json_extract(extension, '$.ref_no') = ?";
      String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,company_code,created_by,created_timestamp,comment_address,status,updated_by,updated_timestamp,comment_type,extension) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
      String updatetasksql = "update enterprise_comment set status=?,updated_by=?,updated_timestamp=?,extension=? where id = ?";
      String updatesql = "update enterprise_itsm_service_request set assignee=?, assignment_group=?, opened=?, opened_by=? where id = ?";
		
      CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
      for(CSVRecord row : dataReader) {
         System.out.println("Processing CR Task row..."+rowCount);
         String taskNo = "", taskType = "", name = "", priority = "", reqNo = "", requestType = "", desc = "", state = "", ciName = "", createdBy = "", createdAt = "", openedBy="", openedAt="", dueDate="", qty = "", assignee = "", assignmentGroup="", updatedBy="", updatedAt="", closedBy = "", closedAt = "", workstartDate = "", workendDate = "", workNotes = "", comment="";
         for(int i=0; i<row.size(); i++) {
            if(i==0) {		
               taskNo = row.get(i).trim();
            } else if(i==1) {		
               name = row.get(i).trim();
            } else if(i==2) {		
               desc = row.get(i).trim();
            } else if(i==3) {		
               reqNo = row.get(i).trim();
            } else if(i==4) {		
               taskType = row.get(i).trim();
            } else if(i==5) {		
               
            } else if(i==6) {		
               assignee = row.get(i).trim();
            } else if(i==7) {		
               assignmentGroup = row.get(i).trim();
            } else if(i==8) {		
               ciName = row.get(i).trim();
            } else if(i==9) {		
               priority = row.get(i).trim();
            } else if(i==10) {		
               state = row.get(i).trim();
            } else if(i==11) {		
               openedBy = row.get(i).trim();
            } else if(i==12) {		
               openedAt = row.get(i).trim();
            } else if(i==13) {		
               createdBy = row.get(i).trim();
            } else if(i==14) {		
               createdAt = row.get(i).trim();
            } else if(i==15) {		
               workstartDate = row.get(i).trim();
            } else if(i==16) {		
               workendDate = row.get(i).trim();
            } else if(i==17) {		
               dueDate = row.get(i).trim();
            }  else if(i==18) {		
               updatedBy = row.get(i).trim();
            } else if(i==19) {		
               updatedAt = row.get(i).trim();
            } else if(i==20) {		
               closedBy = row.get(i).trim();
            } else if(i==21) {		
               closedAt = row.get(i).trim();
            } else if(i==22) {		
               workNotes = row.get(i).trim();
            } else if(i==23) {		
               comment = row.get(i).trim();
            }
         }

         if(state.indexOf("Closed") != -1) {
            state = "Closed";
         }
         if(workstartDate == null) {
            workstartDate = "";
         }
         if(workendDate == null) {
            workendDate = "";
         }
         if(dueDate == null) {
            dueDate = "";
         }
         //name = name.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //name = name.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
         //desc = desc.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
         //desc = desc.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");

         String id = null, compName = null;
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, reqNo);
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
            id = rs.getString("id");
            compName = rs.getString("name");
         }
         rs.close();
         pst.close();

         if(id != null) {
            String taskId = null;
            pst = conn.prepareStatement(checktasksql);
            pst.setString(1, taskNo);
            rs = pst.executeQuery();
            while(rs.next()) {
               taskId = rs.getString("id");
            }
            rs.close();
            pst.close();

            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("ref_no", taskNo);
            extObj.put("assignee", assignee);
            extObj.put("dueDate", dueDate);
            extObj.put("type", taskType);
            extObj.put("component_type", "CHANGE REQUEST");
            extObj.put("component_name", compName);
            extObj.put("start_date", workstartDate);
            extObj.put("end_date", workendDate);
            extObj.put("closed_by", closedBy);
            extObj.put("closed_at", closedAt);

            if(taskId == null) {
               taskId = generateUUID();
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, taskId);
               pst.setString(2, id);
               pst.setString(3, name);
               pst.setString(4, desc);
               pst.setString(5, companyCode);
               pst.setString(6, createdBy);
               pst.setString(7, createdAt);
               pst.setString(8, "");
               pst.setString(9, state);
               pst.setString(10, updatedBy);
               pst.setString(11, updatedAt);
               pst.setString(12, "TASK");
               pst.setString(13, extObj.toString());
               pst.executeUpdate();
               pst.close();
            } else {
               pst = conn.prepareStatement(updatetasksql);
               pst.setString(1, state);
               pst.setString(2, updatedBy);
               pst.setString(3, updatedAt);
               pst.setString(4, extObj.toString());
               pst.setString(5, taskId);
               pst.executeUpdate();
               pst.close();
            }

            if(workNotes != null && workNotes.length() > 0) {
               String[] list = workNotes.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(taskId, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(work notes)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(taskId, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(work notes)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(work notes)")).trim();
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(taskId, name, ts, commentBy, note, "CHANGE REQUEST");
               }
            }

            if(comment != null && comment.length() > 0) {
               System.out.println("comment:"+comment);
               String[] list = workNotes.split("\\n");
               String ts = "", commentBy = "", note = "";
               boolean complete = false;
               for(int i=0; i<list.length; i++) {
                  String str = list[i].trim();
                  System.out.println("str:"+str);
                  if(str.trim().length() > 0) {
                     if(str.toLowerCase().endsWith("(additional comments)") || str.toLowerCase().endsWith("(work notes)")) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = "";
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        str = str.replaceAll("\\(Work Notes\\)", "").replaceAll("\\(Work notes\\)","");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(22).trim();
                     } else if(str.toLowerCase().indexOf("(additional comments)") != -1) {
                        if(note.length() > 0) {
                           //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                           //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                           addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
                        }
                        note = str.substring(str.toLowerCase().indexOf("(additional comments)")+1);
                        str = str.substring(0, str.toLowerCase().indexOf("(additional comments)")).trim();
                        str = str.replaceAll("\\(Additional comments\\)", "");
                        ts = str.substring(0, 18).trim();
                        commentBy = str.substring(19).trim();
                     } else {
                        if(note.length() == 0) {
                           note = str.trim();
                        } else {
                           note = note + "\n"+ str.trim();
                        }
                     }
                  }
               }
               if(note != null && note.length() > 0) {
                  //note = note.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
                  //note = note.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF\\xFFFD\\x09PO]", "");
                  addWorkNotes(id, name, ts, commentBy, note, "CHANGE REQUEST");
               }
            }

            // Update Service Request
            /*
            pst = conn.prepareStatement(updatesql);
            pst.setString(1, assignee);
            pst.setString(2, assignmentGroup);
            pst.setString(3, openedAt);
            pst.setString(4, openedBy);
            pst.setString(5, id);
            pst.executeUpdate();
            pst.close();
            */
         } else {
            System.out.println("Change Request not found:"+reqNo);
         }
         rowCount++;
      }
      conn.commit();
   }

   private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
   }

   private void mysqlConnect(String dbname) throws Exception {
      String userName = "peapi", password = "smartEuser2022!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }

    // Format UK Format to US Format
    protected String formatUKDateFormat(String date) throws Exception {
      DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      DateTimeFormatter df1 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      DateTimeFormatter df3 = DateTimeFormatter.ofPattern("dd/MM/yy");
      DateTimeFormatter df4 = DateTimeFormatter.ofPattern("dd/M/yy");
      DateTimeFormatter df5 = DateTimeFormatter.ofPattern("dd/M/yy");
      DateTimeFormatter df6 = DateTimeFormatter.ofPattern("d/MM/yy");
      DateTimeFormatter df7 = DateTimeFormatter.ofPattern("d/M/yy");

		if(date.indexOf("-") != -1) {
            try {
                LocalDate dt = LocalDate.parse(date, df1);
                date = df.format(dt);
            } catch(Exception ex) {
			}
            try {
				LocalDate dt = LocalDate.parse(date, df2);
				date = df.format(dt);
			} catch(Exception ex) {
			}
		} else if(date.indexOf("/") != -1 && date.length() == 8) {
			LocalDate dt = LocalDate.parse(date, df3);
			date = df.format(dt);
		} else if(date.indexOf("/") != -1 && date.length() == 7) {
			try {
				LocalDate dt = LocalDate.parse(date, df4);
				date = df.format(dt);
			} catch(Exception ex) {
			}
            try {
				LocalDate dt = LocalDate.parse(date, df5);
				date = df.format(dt);
			} catch(Exception ex) {
			}
			try {
				LocalDate dt = LocalDate.parse(date, df6);
				date = df.format(dt);
			} catch(Exception ex) {
			}
		} else if(date.indexOf("/") != -1 && date.length() == 6) {
			try {
				LocalDate dt = LocalDate.parse(date, df7);
				date = df.format(dt);
			} catch(Exception ex) {

			}
		}

		return date;
	}

   private void addNotes(String id, String notes) throws Exception {
		System.out.println("In addNotes..."+id);

		String checksql = "select count(*) count from enterprise_comment where comp_id = ?";
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, id);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,created_by,created_timestamp,updated_by,comment_type,extension,company_code) values(uuid(),?,'',?,?,current_timestamp,?,'COMMENT',json_object(),?)";
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, id);
			pst.setString(2, notes);
			pst.setString(3, "support@smarterd.com");
			pst.setString(4, "support@smarterd.com");
			pst.setString(5, companyCode);
			pst.executeUpdate();
			pst.close();
		}
	}

	private void addWorkNotes(String id, String name, String ts, String commentBy, String workNotes, String componentType) throws Exception {
		System.out.println("In addWorkNotes..."+id+":"+commentBy+":"+workNotes);

      // Remove Timezone from Timestamp - for compatible with database
      ts = ts.replaceAll("PST", "").trim();
		String checksql = "select count(*) count from enterprise_comment where comp_id = ? and comment = ?";
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, id);
		pst.setString(2, workNotes);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();
		
		if(count == 0) {
         System.out.println("Adding WorkNote...");
         OrderedJSONObject extObj = new OrderedJSONObject();
         extObj.put("component_name", name);
         extObj.put("component_type", componentType);
			String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,comment_address,created_by,updated_by,created_timestamp,updated_timestamp,comment_type,extension,company_code) values(uuid(),?,'Work Note',?,?,?,?,?,?,'WORK NOTE',?,?)";
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, id);
			pst.setString(2, workNotes);
         pst.setString(3, commentBy);
			pst.setString(4, commentBy);
			pst.setString(5, commentBy);
         pst.setString(6, ts);
         pst.setString(7, ts);
         pst.setString(8, extObj.toString());
			pst.setString(9, companyCode);
			pst.executeUpdate();
			pst.close();
		} else {
         System.out.println("WorkNote exists!!!");
      }
	}

   private String getCurrentVulnerabilityPeriod() throws Exception {
      String periodsql = "select distinct json_unquote(json_extract(extension, '$.\"scan_period\"')) scan_period from enterprise_vulnerability_log where source != 'User' order by created_timestamp desc limit 1";
      String currentPeriod = null;
      PreparedStatement pst = conn.prepareStatement(periodsql);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         currentPeriod = rs.getString("scan_period");
      }
      rs.close();
      pst.close();

      return currentPeriod;
   }

   private void processServiceRequestItemAttachment() throws Exception {
      //String ritmNo = "RITM0025257";
      String path = "/Users/bct/Downloads/Vulnr_SR_Files";
      String processedPath = "/Users/bct/Downloads/Vulnr_SR_Files/processed";
      File dir = new File(path);
      File[] directoryListing = dir.listFiles();
      if (directoryListing != null) {
         for (File child : directoryListing) {
            if(!child.isDirectory()) {
               String fileName = child.getName();
               System.out.println(fileName);
               String ritmNo = fileName.substring(fileName.lastIndexOf("_")+1, fileName.lastIndexOf(".")).trim();
               //System.out.println(ritmNo);
               // Load File
               /*
               String xlsxfileName = path+"/"+fileName;
               String csvfileName = convertExcelToCSV(xlsxfileName);
               String targetfileName = path+"/"+csvfileName;
               */
               String targetfileName = path+"/"+fileName;
               processVulnerabilityFile(ritmNo, targetfileName);
               //Files.move(Paths.get(xlsxfileName), Paths.get(processedPath+"/"+fileName));
               //Files.move(Paths.get(targetfileName), Paths.get(processedPath+"/"+csvfileName));
               Files.move(Paths.get(targetfileName), Paths.get(processedPath+"/"+fileName));
               conn.commit();
            }
         }
      } 

      /*
      CloseableHttpClient httpClient = HttpClientBuilder.create().build();
      try {
         HttpGet request = new HttpGet("https://mauricesinc.service-now.com/api/now/table/sc_req_item?sysparm_display_value=number%3D"+ritmNo+"&sysparm_fields=sys_id");         
         String encoding = Base64.getEncoder().encodeToString(("smarted_integration" + ":" + "Smarted$123").getBytes());
         request.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
         //StringEntity params = new StringEntity(json.toString());
         request.addHeader("content-type", "application/json");
         request.addHeader("charset", "utf-8");
         //request.setEntity(params);
         HttpResponse response = httpClient.execute(request);
         String result = EntityUtils.toString(response.getEntity());
         System.out.println("Response:"+result);

         // CONVERT RESPONSE STRING TO JSON ARRAY
         OrderedJSONObject ja = new OrderedJSONObject(result);
         res.put("response", ja);
         JSONObject responseObj = (JSONObject) ja.get("result");
      } catch (Exception ex) {
         throw new RuntimeException(ex);
      } finally {
         httpClient.close();
      }
      */
   }

   private void processVulnerabilityFile(String ritmNo, String filename) throws Exception {
      System.out.println("In processVulnerabilityFile..."+filename+":"+ritmNo);
      JSONArray scanIds = new JSONArray();
      String vulnrId = null, vulnrName = null, priority = "";
      CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
      for(CSVRecord row : dataReader) {
         String ipAddress = "";
         for(int i=0; i<row.size(); i++) {
            if(i==0) {		
            } else if(i==1) {		
               ipAddress = row.get(i).trim();
            } else if(i==2) {		
               vulnrName = row.get(i).trim();
            } else {	
            } 
         }
         if(ipAddress == null || ipAddress.length() == 0) {
            break;
         }
         System.out.println(vulnrName+":"+ipAddress);
         // Get Scan Id from Vulnr Log Table
         String sql = "select priority, vulnerability_id, json_unquote(json_extract(extension, '$.THREAT.scan_id')) scan_id from enterprise_vulnerability_log where vulnerability_name=? and ipaddress=? and json_extract(extension, '$.scan_period') =?";
         PreparedStatement pst = conn.prepareStatement(sql);
         pst.setString(1, vulnrName);
         pst.setString(2, ipAddress);
         pst.setString(3, "MAR-23");
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
            vulnrId = rs.getString("vulnerability_id"); 
            priority = rs.getString("priority");
            String scanId = rs.getString("scan_id");
            if(!scanIds.contains(scanId)) {
               scanIds.add(scanId);
            }
         }
         rs.close();
         pst.close();
      }

      // Get Service Request Id 
      String srId = null;
      String srsql = "select id from enterprise_itsm_service_request where json_extract(extension, '$.ref_no')=?";
      PreparedStatement pst = conn.prepareStatement(srsql);
      pst.setString(1, ritmNo);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         srId = rs.getString("id");
      }
      rs.close();
      pst.close();
      System.out.println(scanIds);
      addScanIdstoServiceRequest(srId, vulnrId, vulnrName, priority, scanIds);
   }

   private void addScanIdstoServiceRequest(String srId, String vulnrId, String vulnrName, String priority, JSONArray scanIds) throws Exception {
      System.out.println("In addScanIdstoServiceRequest..."+srId+":"+vulnrId+":"+vulnrName+":"+priority+":"+scanIds);
      String sql = "select ticket_ref, extension from enterprise_itsm_service_request where id = ?";
      String updatesql = "update enterprise_itsm_service_request set priority=?,component_id=?,component_name=?,extension=? where id = ?";
      String vulnrsql = "select priority from enterprise_vulnerability where _id = ?";
      String ticketRef = null, ext = null;
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, srId);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         ticketRef = rs.getString("ticket_ref");
         ext = rs.getString("extension");
      }
      rs.close();
      pst.close();

      if(priority == null) {
         priority = "";
      }
      
      if(ext != null && ext.length() > 0) {
         OrderedJSONObject extObj = new OrderedJSONObject(ext);
         extObj.put("scan_ids", scanIds);
         pst = conn.prepareStatement(updatesql);
         pst.setString(1, priority);
         pst.setString(2, vulnrId);
         pst.setString(3, vulnrName);
         pst.setString(4, extObj.toString());
         pst.setString(5, srId);
         pst.executeUpdate();
         pst.close();

         // Update Vulnerability Log
         updatesql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.THREAT.\"Ticket Reference\"',json_array(?)) where json_unquote(json_extract(extension, '$.THREAT.scan_id')) in (REPLACE_STRING)";
         String ids = scanIds.toString().replaceAll("[\\[\\]]", "");
         updatesql = updatesql.replaceAll("REPLACE_STRING", ids);
         pst = conn.prepareStatement(updatesql);
         pst.setString(1, ticketRef);
         pst.executeUpdate();
         pst.close();

         conn.commit();
      }

      System.out.println("Successfully added ScanIds to Service Request!!!");
   }

   private String convertExcelToCSV(String filename) throws Exception {
      // Creating a inputFile object with specific file path
      File inputFile = new File(filename);

      // Creating a outputFile object to write excel data to csv
      filename = filename.substring(0,filename.lastIndexOf(".")).trim();
      File outputFile = new File(filename+".csv");

      // For storing data into CSV files
      StringBuffer data = new StringBuffer();

      // Creating input stream
      FileInputStream fis = new FileInputStream(inputFile);
      Workbook workbook = null;

      // Get the workbook object for Excel file based on file format
      if (inputFile.getName().endsWith(".xlsx")) {
         workbook = new XSSFWorkbook(fis);
      } else if (inputFile.getName().endsWith(".xls")) {
         workbook = new HSSFWorkbook(fis);
      } else {
         fis.close();
         throw new Exception("File not supported!");
      }

      // Get first sheet from the workbook
      Sheet sheet = workbook.getSheetAt(0);

      // Iterate through each rows from first sheet
      Iterator<Row> rowIterator = sheet.iterator();

      while (rowIterator.hasNext()) {
         Row row = rowIterator.next();
         // For each row, iterate through each columns
         Iterator<Cell> cellIterator = row.cellIterator();
         while (cellIterator.hasNext()) {

            Cell cell = cellIterator.next();

            switch (cell.getCellType()) {
            case BOOLEAN:
                  data.append(cell.getBooleanCellValue() + ",");
                  break;

            case NUMERIC:
                  data.append(cell.getNumericCellValue() + ",");
                  break;

            case STRING:
                  data.append("\""+cell.getStringCellValue() + "\",");
                  break;

            case BLANK:
                  data.append("" + ",");
                  break;

            default:
                  data.append(cell + ",");
            }
         }
         // appending new line after each row
         data.append('\n');
      }

      FileOutputStream fos = new FileOutputStream(outputFile);
      fos.write(data.toString().getBytes());
      fos.close();

      return outputFile.getName();
   }

   private void updateSRVulnerabilityPriority() throws Exception {
      String sql = "select id, component_id from enterprise_itsm_service_request where request_type like 'Vulnerability%'";
      String vulnrsql = "select priority from enterprise_vulnerability where _id = ?";
      String updatesql = "update enterprise_itsm_service_request set priority = ? where id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String id = rs.getString("id");
         String compId = rs.getString("component_id");

         if(compId != null && compId.length() > 0) {
            String priority = null;
            PreparedStatement pst1 = conn.prepareStatement(vulnrsql);
            pst1.setString(1, compId);
            ResultSet rs1 = pst1.executeQuery();
            while(rs1.next()) {
               priority = rs1.getString("priority");
            }
            rs1.close();
            pst1.close();
            if(priority == null || priority.length() == 0) {
               priority = "Low";
            }
            System.out.println(priority);
            
            pst1 = conn.prepareStatement(updatesql);
            pst1.setString(1, priority);
            pst1.setString(2, id);
            pst1.executeUpdate();
            pst1.close();
         }
      }
      rs.close();
      pst.close();
      conn.commit();
   }

   private void addAttachment(String id, String attachment, String componentType) throws Exception {
		System.out.println("In addAttachment..."+id);
	
		String checksql = "select count(*) count from enterprise_comment where comp_id = ? and comment = ?";
		int count = 0;
        String[] list = attachment.split("\n");
        for(int i=0; i<list.length; i++) {
            String str = list[i].trim();
            PreparedStatement pst = conn.prepareStatement(checksql);
            pst.setString(1, id);
            pst.setString(2, str);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                count = rs.getInt("count");
            }
            rs.close();
            pst.close();
            
            if(count == 0) {
               OrderedJSONObject extObj = new OrderedJSONObject();
               extObj.put("component_type", componentType);
               String insertsql = "insert into enterprise_comment(id,comp_id,topic_name,comment,created_by,created_timestamp,updated_by,comment_type,extension,company_code) values(uuid(),'Attachment',?,?,?,current_timestamp,?,'ATTACHMENT',?,?)";
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, id);
               pst.setString(2, str);
               pst.setString(3, "suport@smarterd.com");
               pst.setString(4, "support@smarterd.com");
               pst.setString(5, extObj.toString());
               pst.setString(6, companyCode);
               pst.executeUpdate();
               pst.close();
            }
        }
	}

   public static void main(String args[]) throws Exception {
      String filename = args[0];
      String companyCode = args[1];
      String env = args[2];

      // Process ServiceNow Files
      OrderedJSONObject fileList = new OrderedJSONObject();
      String filepath = "/Users/bct/Downloads/Maurices/data/ITSM/";
      File folder = new File(filepath);
      File[] listOfFiles = folder.listFiles();
      for (File file : listOfFiles) {
         if (file.isFile()) {
            String name = file.getName();
            if(name.equals("sc_request.csv")) {
               System.out.println("Service Request...");
               fileList.put("Service Request", name);
            } else if(name.equals("sc_req_item.csv")) {
               System.out.println("Service Request Item...");
               fileList.put("Service Request Item", name);
            } else if(name.equals("sc_task.csv")) {
               System.out.println("Service Task...");
               fileList.put("Service Task", name);
            } else if(name.equals("change_request.csv")) {
               System.out.println("Change Request...");
               fileList.put("Change Request", name);
            } else if(name.equals("change_task.csv")) {
               System.out.println("Change Task...");
               fileList.put("Change Task", name);
            }
         }
      }
      System.out.println(fileList);
      SmarteServiceRequest sr = new SmarteServiceRequest(companyCode, env, "");
      if(fileList.size() == 5) {
         if(fileList.has("Service Request")) {
            filename = (String)fileList.get("Service Request");
            sr.setFile(filepath+filename);
            sr.loadSR();
         } else {
            throw new Exception("Service Request File Missing!!!");
         }
         
         if(fileList.has("Service Request Item")) {
            filename = (String)fileList.get("Service Request Item");
            sr.setFile(filepath+filename);
            sr.loadSRItem();
         } else {
            throw new Exception("Service Request Item File Missing!!!");
         }

         if(fileList.has("Service Task")) {
            filename = (String)fileList.get("Service Task");
            sr.setFile(filepath+filename);
            sr.loadSRTask();
         } else {
            throw new Exception("Service Task File Missing!!!");
         }

         if(fileList.has("Change Request")) {
            filename = (String)fileList.get("Change Request");
            sr.setFile(filepath+filename);
            sr.loadCR();
         } else {
            throw new Exception("Change Request File Missing!!!");
         }

         if(fileList.has("Change Task")) {
            filename = (String)fileList.get("Change Task");
            sr.setFile(filepath+filename);
            sr.loadCRTask();
         } else {
            throw new Exception("Change Task File Missing!!!");
         }
      } else {
         System.out.println("File Missing to process!!!");
      }

      //sr.processServiceRequestItemAttachment();
      //sr.updateSRVulnerabilityPriority();
      //sr.loadSolarwindsSR();
   }
}