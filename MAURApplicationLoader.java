import java.sql.*;
import java.util.*;

import com.mysql.cj.protocol.x.NoticeFactory;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class MAURApplicationLoader {
    private String companyCode = "MAUR";
    private String hostname = "localhost";
    private Connection conn = null;
    private CSVReader reader = null;

    public MAURApplicationLoader(String filename, String env) throws Exception {
        reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);

        if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		} else {
            hostname = "localhost";
        }
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    private OrderedJSONObject loadApplication() throws Exception {
        System.out.println("Loading MAUR Application ...");
        OrderedJSONObject res = new OrderedJSONObject();
        String insertSQL = "insert into enterprise_asset(id,asset_name,asset_desc,asset_version,extension,asset_type,asset_category,created_by,updated_by,company_code) value(uuid(), ?, ?, ?, ?,'ASSET', 'APPLICATION','support@smarterd.com','support@smarterd.com','MAUR')";
        String updateSQL = "update enterprise_asset set extension = ? where id = ?";
        String checkSQL = "select id, asset_name, extension from enterprise_asset where (asset_name like ? or 'REPLACE_STRING' like concat(asset_name, '%')) and asset_category = 'APPLICATION'";
        PreparedStatement pst1 = conn.prepareStatement(insertSQL);
        PreparedStatement pst2 = conn.prepareStatement(updateSQL);

        String[] row;
		int rowCount=0, notFound=0, found=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				//System.out.println("Processing row-" + rowCount + "...");
				
				String identifier=null, type=null, criticality=null,supportLevel=null,wave=null,assetName=null, classification=null, bf=null, location=null, version=null, notes=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						identifier = row[i].trim();
					} else if(i==1) {
						criticality = row[i].trim();
					} else if(i==2) {
					} else if(i==3) {
					} else if(i==4) {
					} else if(i==5) {
						supportLevel = row[i].trim();
					} else if(i==6) {
						wave = row[i].trim();
					} else if(i==7) {
						type = row[i].trim();
					} else if(i==8) {
					} else if(i==9) {
						assetName = row[i].trim();
					} else if(i==10) {
						classification = row[i].trim();
					} else if(i==11) {
						bf = row[i].trim();
					} else if(i==12) {
						location = row[i].trim();
					} else if(i==13) {
					} else if(i==14) {
						version = row[i].trim();
					} else if(i==15) {
					} else if(i==16) {
						notes = row[i].trim();
					}
                }

                if(type.equalsIgnoreCase("Custom Apps")) {
                    type = "Custom(homegrown)";
                } else {
                    type = "Commercial-Off-The-Shelf(COTS)";
                }
    
                String name=null, id = null, ext=null;
                String sql = checkSQL.replaceAll("REPLACE_STRING", assetName);
                PreparedStatement pst = conn.prepareStatement(sql);
                pst.setString(1, "%"+assetName+"%");
                ResultSet rs = pst.executeQuery();
                while(rs.next()) {
                    name = rs.getString("asset_name");
                    id = rs.getString("id");
                    ext = rs.getString("extension");
                }
                rs.close();
                pst.close();

                classification = classification.replaceAll("Services", "").trim();

                if(classification.equalsIgnoreCase("eComm")) {
                    classification = "eCommerce";
                } else if(classification.equalsIgnoreCase("IT Services")) {
                    classification = "Infrastructure";
                } else if(classification.equalsIgnoreCase("Human Resources")) {
                    classification = "HR";
                } else if(classification.equalsIgnoreCase("Identity and Governance")) {
                    classification = "Security";
                }
                System.out.println(classification);
                OrderedJSONObject extObj = null;
                if(id == null) {
                    System.out.println("Asset not found:"+assetName);
                    notFound++;
                    extObj = new OrderedJSONObject();
                    extObj.put("Business app/IT app", "BUSINESS APP");
                    extObj.put("Type", type);
                    extObj.put("Classification", classification.trim());
                    extObj.put("Criticality", criticality);
                    extObj.put("Supportability", supportLevel);
                    
                    pst1.setString(1, assetName);
                    pst1.setString(2, notes);
                    pst1.setString(3, version);
                    pst1.setString(4, extObj.toString());
                    pst1.addBatch();
                    
                } else {
                    System.out.println("Asset found:"+assetName+":"+name);
                    found++;
                    extObj = new OrderedJSONObject(ext);
                    extObj.put("Business app/IT app", "BUSINESS APP");
                    extObj.put("Type", type);
                    extObj.put("Classification", classification);
                    extObj.put("Criticality", criticality);
                    extObj.put("Supportability", supportLevel);
                    
                    pst2.setString(1, extObj.toString());
                    pst2.setString(2, id);
                    pst2.addBatch();
                    
                }
            }
        }

        pst1.executeBatch();
        pst2.executeBatch();
        conn.commit();

        pst1.close();
        pst2.close();

        System.out.println("Asset Found:"+found+" Asset not found:"+notFound);
        //res.put("STATUS", "Successfully loaded Asset!!!");
        //System.out.println("Successfully loaded Asset["+rowCount+"]!!!");
        return res;
    }

    private void mysqlConnect(String dbname) throws Exception {
        String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String filename = args[0];
        String env = args[1];
        
        MAURApplicationLoader loader = new MAURApplicationLoader(filename, env);
        loader.loadApplication();
    } 

}