import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.nio.charset.StandardCharsets;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import java.time.LocalDate;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.StringEscapeUtils;

public class SmartEDataLoader {
	private CSVParser schemaReader = null;
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
	String type = null;
	String tableName = null;
	int insertedCount=0, updatedCount=0;
   	
	public SmartEDataLoader(String type, String filename, String schemaFile, String companyCode, String env) throws Exception {
		this.type = type;
		this.companyCode = companyCode;
		schemaReader = CSVParser.parse(new FileReader(new File(schemaFile+"_schema.csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		dataReader = CSVParser.parse(new FileReader(new File(filename+"_data.csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}

		// Get Table Name
		tableName = getTableName(type);
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		OrderedJSONObject schemaList = new OrderedJSONObject();
		OrderedJSONObject uniqueList = new OrderedJSONObject();
		OrderedJSONObject errorList = new OrderedJSONObject();

		// Load Schema File
		for(CSVRecord record : schemaReader) {
			String sourceColumnName = record.get("Source Column Name").trim();
			String columnName = record.get("SmarterD Field Name").trim();
			String exclude = record.get("Exclude").trim();
			String unique = record.get("Unique").trim();
			String columnType = record.get("Field Type").trim();
			String dataType = record.get("Value Type").trim();
			String rule = record.get("Rule").trim();
			String comment = record.get("Notes").trim();

			OrderedJSONObject ruleObj = null;
			if(rule != null && rule.trim().length() > 0){
				ruleObj = new OrderedJSONObject(rule);
			} else {
				ruleObj = new OrderedJSONObject();
			}

			// Create Schema Object
			if(exclude.equalsIgnoreCase("N")) {
				OrderedJSONObject obj = new OrderedJSONObject();
				obj.put("column_name", columnName);
				obj.put("column_type", columnType);
				obj.put("data_type", dataType);
				obj.put("rule", ruleObj);
				obj.put("comment", comment);
				schemaList.put(sourceColumnName, obj);

				if(unique.equalsIgnoreCase("Y")) {
					uniqueList.put(sourceColumnName, obj);
				}
			}
		}
		schemaReader.close();

		// Load Data Header 
		OrderedJSONObject columnList = new OrderedJSONObject();
		List headerList = dataReader.getHeaderNames();
		int i = 0;
		Iterator iter = headerList.iterator();
		while(iter.hasNext()) {
			String columnName = (String)iter.next();
			columnList.put(String.valueOf(i), columnName);
			i++;
		}

		// Load Data
		OrderedJSONObject columnValueList = new OrderedJSONObject();
		for(CSVRecord dataRow : dataReader) {
			if(dataRow.isConsistent()) {
				for(i=0; i<dataRow.size(); i++) {
					String columnValue = dataRow.get(i).trim();

					if(columnValue == null) {
						columnValue = "";
					}

					// Prepare value for SQL Load
					columnValue = columnValue.replaceAll("'", "''"); 
					columnValue = columnValue.replaceAll("\"", "'");
					columnValue = columnValue.replaceAll("\\\\", "");
					// remove invalid unicode characters
					columnValue = columnValue.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
					// remove other unicode characters
					columnValue = columnValue.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]", "");
					if(columnValue.equals("-")) {
						columnValue = "";
					}

					// Store Value
					String columnName = (String)columnList.get(String.valueOf(i));
					columnValueList.put(columnName, columnValue);
					System.out.println(columnName+":"+columnValue);
				}
				// Check if data exists
				OrderedJSONObject dataObj = checkData(uniqueList, columnValueList);

				// Process Schema
				if(dataObj.size() == 0) {
					System.out.println("Inserting row...");
					insertData(schemaList, columnValueList);
					insertedCount++;
				} else {
					System.out.println("Updating row...");
					updateData(schemaList, columnValueList, dataObj);
					updatedCount++;
				}
			} else {
				String rowNumber = Long.toString(dataRow.getRecordNumber());
				errorList.put(rowNumber, dataRow.toString());
				System.out.println("Skipping:Combined Data Incorrect does not match column list:"+dataRow.size()+":"+rowNumber);
			}

			//break;
			conn.commit();
		}
		System.out.println("Inserted:"+insertedCount+" Updated:"+updatedCount+" Error:"+errorList.size());
		System.out.println(errorList.toString());
		dataReader.close();
		conn.close();
	}

	private OrderedJSONObject checkData(OrderedJSONObject uniqueList, OrderedJSONObject columnValueList) throws Exception {
		System.out.println("In checkData..."+uniqueList.toString());
		OrderedJSONObject res = new OrderedJSONObject();
		String sql = "select * from "+tableName+" where ";
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("SOFTWARE")) {
			sql = sql + "asset_category = '"+type+"' and ";
		}
		int count = 0;
		Iterator iter = uniqueList.keySet().iterator();
		while(iter.hasNext()) {
			String sourceColumnName = (String)iter.next();
			OrderedJSONObject schemaObj = (OrderedJSONObject)uniqueList.get(sourceColumnName);
			String columnName = (String)schemaObj.get("column_name");
			String columnValueType = (String)schemaObj.get("data_type");
			String columnValue = (String)columnValueList.get(sourceColumnName);

			// Remove any quotes from Key Value and update
			columnValue = columnValue.replaceAll("'", "");
			columnValueList.put(sourceColumnName, columnValue);

			if(columnValueType.equalsIgnoreCase("Text")) {
				if(count == 0) {
					sql = sql + columnName + " = '"+columnValue + "'";
				} else {
					sql = sql + " and " + columnName + " = '"+columnValue + "'";
				}
			} else {
				if(count == 0) {
					sql = sql + columnName + " = "+columnValue;
				} else {
					sql = sql + " and " + columnName + " = "+columnValue;
				}
			}
			count++;
		}
		System.out.println(sql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for(int i = 0; i < columnCount; i++) {
                String column = rsmd.getColumnName(i+1);
                String value = rs.getString(column);
				if(value != null && value.length() > 0 && value.startsWith("[") && value.endsWith("]")) {
					//System.out.println(value);
					if(value.startsWith("[\"") && value.endsWith("\"]")) {	
						JSONArray valueList = new JSONArray(value);
						res.put(column, valueList);
					} else if(value.length() == 2) {
						JSONArray valueList = new JSONArray();
						res.put(column, valueList);
					} else {
						res.put(column, value);
					}
				} else if(value != null && value.length() > 0 && value.startsWith("{") && value.endsWith("}")) {
					//System.out.println(value);
					OrderedJSONObject obj = new OrderedJSONObject(value);
					res.put(column, obj);
				} else {
					res.put(column, value);
				}
            }
		}

		System.out.println(res.toString());
		return res;
	}

	private void insertData(OrderedJSONObject schemaList, OrderedJSONObject columnValueList) throws Exception {
		System.out.println("In insertData...");
		OrderedJSONObject dataObj = new OrderedJSONObject();
		OrderedJSONObject extObj = new OrderedJSONObject();
		OrderedJSONObject customObj = new OrderedJSONObject();
		OrderedJSONObject lifecycleObj = new OrderedJSONObject();
		OrderedJSONObject licenseObj = new OrderedJSONObject();
		OrderedJSONObject featureList = new OrderedJSONObject();
		OrderedJSONObject knowledgeList = new OrderedJSONObject();
		OrderedJSONObject stdruleList = new OrderedJSONObject();
		OrderedJSONObject vulnrLogObj = new OrderedJSONObject();
		OrderedJSONObject worknotesObj = new OrderedJSONObject();

		// Initialize Ext Obj
		if(type.equalsIgnoreCase("APPLICATION")) {
			extObj = initializeApplicationExtension(extObj);
			dataObj.put("asset_category", type);
			dataObj.put("asset_type", "ASSET");
			dataObj.put("lifecycle_name", "");
			dataObj.put("asset_comment", new JSONArray());
			dataObj.put("asset_lifecycle_list", new JSONArray());
			dataObj.put("created_by", "support@smarterd.com");
			dataObj.put("updated_by", "support@smarterd.com");
			dataObj.put("company_code", companyCode);
		} else if(type.equalsIgnoreCase("SOFTWARE")) {
			extObj = initializeSoftwareExtension(extObj);
			dataObj.put("asset_category", type);
			dataObj.put("asset_type", "ASSET");
			dataObj.put("lifecycle_name", "");
			dataObj.put("asset_comment", new JSONArray());
			dataObj.put("asset_lifecycle_list", new JSONArray());
			dataObj.put("created_by", "support@smarterd.com");
			dataObj.put("updated_by", "support@smarterd.com");
			dataObj.put("company_code", companyCode);
		} else if(type.equalsIgnoreCase("IT ASSET")) {
			extObj = initializeITAssetExtension(extObj);
			dataObj.put("asset_category", type);
			dataObj.put("asset_type", "ASSET");
			dataObj.put("lifecycle_name", "");
			dataObj.put("asset_comment", new JSONArray());
			dataObj.put("asset_lifecycle_list", new JSONArray());
			dataObj.put("created_by", "support@smarterd.com");
			dataObj.put("updated_by", "support@smarterd.com");
			dataObj.put("company_code", companyCode);
		} else if(type.equalsIgnoreCase("CONTRACT")) {
			dataObj.put("asset_type", "AGREEMENT");
			dataObj.put("created_by", "support@smarterd.com");
			dataObj.put("updated_by", "support@smarterd.com");
			dataObj.put("company_code", companyCode);
		} else if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM")) {
			dataObj.put("incidentTime", "");
			dataObj.put("resolutionTime", "");
			dataObj.put("lifecycle", "");
			dataObj.put("priority", "");
			dataObj.put("itstakeholder", "");
			dataObj.put("ecosystem", new JSONArray());
			extObj.put("service_type", type);
			extObj.put("Category", "");
			extObj.put("Assignment Group", "");
		} else if(type.equalsIgnoreCase("VULNERABILITY")) {
			extObj.put("category", type);
			extObj.put("riskComp", "");
			extObj.put("riskScaled", "");
			extObj.put("enterprise_action", "");
			dataObj.put("createdBy", "support@smarterd.com");
			dataObj.put("updatedBy", "support@smarterd.com");
			dataObj.put("type", "VULNERABILITY");
		} else if(type.equalsIgnoreCase("PLAN")) {
			extObj.put("category", type);
			extObj.put("riskComp", "");
			extObj.put("riskScaled", "");
			extObj.put("enterprise_action", "");
			dataObj.put("createdBy", "support@smarterd.com");
			dataObj.put("updatedBy", "support@smarterd.com");
			dataObj.put("type", "VULNERABILITY");
		}

		Iterator iter = schemaList.keySet().iterator();
		while(iter.hasNext()) {
			String sourceColumnName = (String)iter.next();
			OrderedJSONObject schemaObj = (OrderedJSONObject)schemaList.get(sourceColumnName);
			String columnName = (String)schemaObj.get("column_name");
			String columnType = (String)schemaObj.get("column_type");
			String columnValue = (String)columnValueList.get(sourceColumnName);
			String columnValueType = (String)schemaObj.get("data_type");
			OrderedJSONObject ruleObj = (OrderedJSONObject)schemaObj.get("rule");

			if(columnValue == null) {
				columnValue = "";
			}

			// Process rules
			if(ruleObj.size() > 0) {
				Iterator ruleIter = ruleObj.keySet().iterator();
				while(ruleIter.hasNext()) {
					String key = (String)ruleIter.next();
					String value = (String)ruleObj.get(key);
					if(key.equalsIgnoreCase("action")) {
						stdruleList.put(value, columnValue);
					} else {
						if(columnValue.equalsIgnoreCase(key)) {
							columnValue = value;
						}
					}
				}
			}
			
			if(columnType.equalsIgnoreCase("Core")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					dataObj.put(columnName, columnValue);
					if(type.equalsIgnoreCase("VULNERABILITY") && columnName.equalsIgnoreCase("nameId")) {
						vulnrLogObj.put("vulnerability_name", columnValue);
					} else if(type.equalsIgnoreCase("VULNERABILITY") && columnName.equalsIgnoreCase("CVEs Associated")) {
						vulnrLogObj.put("CVE", columnValue);
					}
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					dataObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(dataObj.has(columnName)) {
						arr = (JSONArray)dataObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					dataObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						dataObj.put(columnName, new Integer(columnValue));
					} else {
						dataObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						//System.out.println(d);
						dataObj.put(columnName, d);
					} else {
						dataObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					//System.out.println(columnValue);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						//System.out.println(d);
						dataObj.put(columnName, d);
					} else {
						dataObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						dataObj.put(columnName, new Boolean(columnValue));
					} else {
						dataObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Extension")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					extObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					extObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(extObj.has(columnName)) {
						arr = (JSONArray)extObj.get(columnName);	
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					extObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						extObj.put(columnName, new Integer(columnValue));
					} else {
						extObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					System.out.println(columnValue);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						extObj.put(columnName, new Boolean(columnValue));
					} else {
						extObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Custom")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					//columnValue = columnValue.replaceAll("\\", "");
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					customObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					customObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(customObj.has(columnName)) {
						arr = (JSONArray)customObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					customObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						customObj.put(columnName, new Integer(columnValue));
					} else {
						customObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						customObj.put(columnName, new Boolean(columnValue));
					} else {
						customObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Features")) {
				featureList.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Knowledge")) {
				knowledgeList.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Lifecycle")) {
				lifecycleObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("License")) {
				licenseObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Core")) {
				dataObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Work Notes")) {
				worknotesObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Vulnerability_Log")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					vulnrLogObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					vulnrLogObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(vulnrLogObj.has(columnName)) {
						arr = (JSONArray)dataObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					vulnrLogObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						vulnrLogObj.put(columnName, new Integer(columnValue));
					} else {
						vulnrLogObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						vulnrLogObj.put(columnName, new Boolean(columnValue));
					} else {
						vulnrLogObj.put(columnName, new Boolean(false));
					}
				} 
			} 
		}

		// Add Lifecycle Object to List
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE")) {
			JSONArray lifecycleList = new JSONArray();
			lifecycleList.add(lifecycleObj);

			// Add Lifecycle List to Asset Obj
			dataObj.put("asset_lifecycle_list", lifecycleList);

			JSONArray licenseList = new JSONArray();
			licenseList.add(licenseObj);

			// Add License to Software Asset Obj
			dataObj.put("asset_comment", licenseList);
		}

		// Add Custom to Extension 
		extObj.put("custom", customObj);

		// Make sure single quotes in extobj is escaped - Issue when updating
		// notes, Resolution are the only field that seem to have text 
		iter = extObj.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			Object value = extObj.get(key);

			if(value instanceof String) {
				String str = (String)value;
				str = str.replaceAll("'", "''");
				str = crunchifyJSONEscapeUtil(str);
				extObj.put(key, str);
			}
		}

		// Add Extension to Asset Object
		dataObj.put("extension", extObj);

		// Generate Id
		String id = generateUUID(); 

		// Add id to Asset Obj
		if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM") || type.equalsIgnoreCase("VULNERABILITY")) {
			dataObj.put("_id", id);
		} else {
			dataObj.put("id", id);
		}
		
		// Form Insert SQL
		String insertsql = "insert into "+tableName;
		String columnstr = "", valuestr = "";
		int count = 0;
		iter = dataObj.keySet().iterator();
		while(iter.hasNext()) {
			String columnName = (String)iter.next();
			Object columnValueObj = dataObj.get(columnName);
			System.out.println(columnName+":"+columnValueObj.toString());
			if(count == 0) {
				columnstr = columnName;
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = "'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_comment")) {
					valuestr = "'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("ecosystem")) {
					if(columnValueObj != null) {
						valuestr = "'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr = "'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("asset_value")) {
					valuestr = "'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_feature_list")) {
					valuestr = "'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("groups")) {
					if(columnValueObj != null) {
						valuestr =  "'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr =  "'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr = "'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					valuestr = "\""+(String)columnValueObj+"\"";
				}
			} else {
				columnstr = columnstr + "," + columnName;
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = valuestr+","+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_comment")) {
					valuestr = valuestr+","+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("ecosystem")) {
					if(columnValueObj != null) {
						valuestr = valuestr+","+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr = valuestr+","+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("asset_value")) {
					valuestr = valuestr+","+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_feature_list")) {
					valuestr =  valuestr+","+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("groups")) {
					if(columnValueObj != null) {
						valuestr =  valuestr+","+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr =  valuestr+","+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr =  valuestr+","+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					valuestr =  valuestr+","+"\""+(String)columnValueObj+"\"";
				}
			}
			count++;
		}
		insertsql = insertsql + "(" + columnstr + ") values(" + valuestr + ")";
		PreparedStatement pst = conn.prepareStatement(insertsql);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();

		// Update Incident Category
		if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM")) {
			updateAssignmentCategory(id);
		} else if(type.equalsIgnoreCase("VULNERABILITY")) {
			// Add Vulnerability to group
			if(dataObj.has("group_name")) {
				String groupName = (String)dataObj.get("group_name");
				String source = null;
				if(dataObj.has("source")) {
					source = (String)dataObj.get("source");
				} else {
					source = "";
				}
				addVulnerabilityToGroup(id, groupName, source);
			}

			// Add Log
			createVulnerabilityLog(id, dataObj, vulnrLogObj, false);
		}

		// Process Std Rules
		if(stdruleList.size() > 0) {
			String vendorId = null, contractId = null;
			Iterator ruleIter = stdruleList.keySet().iterator();
			while(ruleIter.hasNext()) {
				String rule = (String)ruleIter.next();
				String value = (String)stdruleList.get(rule);
				if(value != null && value.trim().length() > 0) {
					if(rule.equalsIgnoreCase("CREATE VENDOR")) {
						vendorId = createVendor(value);
					} else if(rule.equalsIgnoreCase("CREATE CONTRACT")) {
						contractId = createContract(id, value);
					} else if(rule.equalsIgnoreCase("CREATE BUSINESS AREA")) {
						createSetting(value, "BUSINESS AREA", "GLOBAL");
					} else if(rule.equalsIgnoreCase("CREATE DEPARTMENT")) {
						createSetting(value, "Department", "GLOBAL");
					} else if(rule.equalsIgnoreCase("CREATE APPLICATION")) {
						createAsset(id, value, type);
					}
				}
			}

			// Create Relationship
			if(vendorId != null && contractId != null) {
				createRel(vendorId, contractId, "3RD PARTY", "AGREEMENT", "support@smarterd.com");
			}
		}

		// Add Features
		iter = featureList.keySet().iterator();
		while(iter.hasNext()) {
			String label = (String)iter.next();
			String feature = (String)featureList.get(label);
			createFeatures(id, label, feature);
		}

		// Add Knowledge
		iter = knowledgeList.keySet().iterator();
		while(iter.hasNext()) {
			String label = (String)iter.next();
			String knowledge = (String)knowledgeList.get(label);
			createKnowledge(id, label, knowledge);
		}

		// Add Work Notes
		if(worknotesObj.size() > 0) {
			//createWorkNotes(worknotesObj);
		}
	}

	private void updateData(OrderedJSONObject schemaList, OrderedJSONObject columnValueList, OrderedJSONObject dataObj) throws Exception {
		System.out.println("In updateData...");
		String id = null;
		if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM") || type.equalsIgnoreCase("VULNERABILITY")) {
			id = (String)dataObj.get("_id");
		} else {
			id = (String)dataObj.get("id");
		}
		OrderedJSONObject extObj = (OrderedJSONObject)dataObj.get("extension");
		OrderedJSONObject customObj = null;
		if(extObj.has("custom")) {
			customObj = (OrderedJSONObject)extObj.get("custom");
		} else {
			customObj = new OrderedJSONObject();
		}

		JSONArray lifecycleList = null, licenseList = null;
		OrderedJSONObject lifecycleObj = null, licenseObj = null;
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE")) {
			if(dataObj.has("asset_lifecycle_list") && dataObj.get("asset_lifecycle_list") instanceof JSONArray) {
				lifecycleList = (JSONArray)dataObj.get("asset_lifecycle_list");
				if(lifecycleList.size() > 0) {
					lifecycleObj = new OrderedJSONObject((JSONObject)lifecycleList.get(0));
					lifecycleList.clear();	// Remove Existing Object
				} else{
					lifecycleObj = new OrderedJSONObject();
				}
			} else {
				lifecycleList = new JSONArray();
				lifecycleObj = new OrderedJSONObject();
			}
			System.out.println("Lifecycle...");

			if(dataObj.has("asset_comment") && dataObj.get("asset_comment") instanceof JSONArray) {
				licenseList = (JSONArray)dataObj.get("asset_comment");
				if(licenseList != null && licenseList.size() > 0) {
					licenseObj = new OrderedJSONObject((JSONObject)licenseList.get(0));
					licenseList.clear();	// Remove Existing Object
				} else{
					licenseList = new JSONArray();
					licenseObj = new OrderedJSONObject();
				}
			} else {
				licenseList = new JSONArray();
				licenseObj = new OrderedJSONObject();
			}
			System.out.println("License...");
		}
		OrderedJSONObject featureList = new OrderedJSONObject();
		OrderedJSONObject knowledgeList = new OrderedJSONObject();
		OrderedJSONObject stdruleList = new OrderedJSONObject();
		OrderedJSONObject vulnrLogObj = null;
		OrderedJSONObject worknotesObj = new OrderedJSONObject();

		if(type.equalsIgnoreCase("VULNERABILITY") && extObj.has("THREAT")) {
			vulnrLogObj = (OrderedJSONObject)extObj.get("THREAT");
		} else {
			vulnrLogObj = new OrderedJSONObject();
		}
		System.out.println("Lifecycle Initialized.");

		Iterator iter = schemaList.keySet().iterator();
		while(iter.hasNext()) {
			String sourceColumnName = (String)iter.next();
			OrderedJSONObject schemaObj = (OrderedJSONObject)schemaList.get(sourceColumnName);
			String columnName = (String)schemaObj.get("column_name");
			String columnType = (String)schemaObj.get("column_type");
			String columnValue = (String)columnValueList.get(sourceColumnName);
			String columnValueType = (String)schemaObj.get("data_type");
			OrderedJSONObject ruleObj = (OrderedJSONObject)schemaObj.get("rule");

			// Process rules
			if(ruleObj.size() > 0) {
				Iterator ruleIter = ruleObj.keySet().iterator();
				while(ruleIter.hasNext()) {
					String key = (String)ruleIter.next();
					String value = (String)ruleObj.get(key);
					if(key.equalsIgnoreCase("action")) {
						stdruleList.put(value, columnValue);
					} else {
						if(columnValue.equalsIgnoreCase(key)) {
							columnValue = value;
						}
					}
				}
			}
			
			if(columnType.equalsIgnoreCase("Core")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						columnValue = columnValue.replaceAll("'", "''");
					}
					if(columnName.equalsIgnoreCase("description")) {
						columnValue = crunchifyJSONEscapeUtil(columnValue);
					}
					dataObj.put(columnName, columnValue);
					if(type.equalsIgnoreCase("VULNERABILITY") && columnName.equalsIgnoreCase("nameId")) {
						vulnrLogObj.put("vulnerability_name", columnValue);
					}
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					dataObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(dataObj.has(columnName)) {
						arr = (JSONArray)dataObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(arr.contains(columnValue)) { // Remove Existing Value
							arr.remove(columnValue);
						}
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
						arr.add(columnValue);
					}
					dataObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						dataObj.put(columnName, new Integer(columnValue));
					} else {
						dataObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						//System.out.println(d);
						dataObj.put(columnName, d);
					} else {
						dataObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					//System.out.println(columnValue);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						//System.out.println(d);
						dataObj.put(columnName, d);
					} else {
						dataObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						dataObj.put(columnName, new Boolean(columnValue));
					} else {
						dataObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Extension")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					//columnValue = columnValue.replaceAll("[\\]", "");
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					extObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					extObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(extObj.has(columnName)) {
						arr = (JSONArray)extObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					extObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						extObj.put(columnName, new Integer(columnValue));
					} else {
						extObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					System.out.println(columnValue);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						extObj.put(columnName, new Boolean(columnValue));
					} else {
						extObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Custom")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					customObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					customObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(customObj.has(columnName)) {
						arr = (JSONArray)customObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					customObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						customObj.put(columnName, new Integer(columnValue));
					} else {
						customObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						customObj.put(columnName, new Boolean(columnValue));
					} else {
						customObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Vulnerability_Log")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					if(columnValue == null) {
						columnValue = "";
					} else {
						if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
							columnValue = columnValue.replaceAll("'", "''");
						}
					}
					vulnrLogObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Date")) {
					columnValue = formatDate(columnValue);
					vulnrLogObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(vulnrLogObj.has(columnName)) {
						arr = (JSONArray)dataObj.get(columnName);
					}
					if(arr == null) {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							if(columnValue.indexOf("''") == -1 && columnValue.indexOf("'") != -1) {
								columnValue = columnValue.replaceAll("'", "''");
							}
							arr.add(columnValue);
						}
					}
					vulnrLogObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						vulnrLogObj.put(columnName, new Integer(columnValue));
					} else {
						vulnrLogObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						vulnrLogObj.put(columnName, new Boolean(columnValue));
					} else {
						vulnrLogObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Features")) {
				featureList.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Knowledge")) {
				knowledgeList.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Lifecycle")) {
				lifecycleObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("License")) {
				licenseObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Work Notes")) {
				worknotesObj.put(columnName, columnValue);
			}
		}

		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE")) {
			// Add Lifecycle Object to List
			lifecycleList.add(lifecycleObj);

			// Add Lifecycle List to Asset Obj
			dataObj.put("asset_lifecycle_list", lifecycleList);

			// Add License Object to List
			licenseList.add(licenseObj);

			// Add License List to Asset Obj
			dataObj.put("asset_comment", licenseList);
		}

		// Add Custom to Extension 
		extObj.put("custom", customObj);

		// Make sure single quotes in extobj is escaped - Issue when updating
		// notes, Resolution are the only field that seem to have text 
		System.out.println(extObj.toString());
		iter = extObj.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			Object value = extObj.get(key);

			if(value instanceof String) {
				String str = (String)value;
				str = str.replaceAll("'", "''");
				str = crunchifyJSONEscapeUtil(str);
				extObj.put(key, str);
			}
		}

		// Fix JSON for any special and escape characters
		System.out.println(extObj.toString());
		//String  fixedJson = crunchifyJSONEscapeUtil(extObj.toString()).value();
		//String  fixedJson = crunchifyJSONEscapeUtil(extObj.toString());
		//System.out.println(fixedJson);
		//extObj = new OrderedJSONObject(fixedJson);

		// Add Extension to Asset Object
		dataObj.put("extension", extObj);
		
		// Add Updated By
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE")) {
			dataObj.put("updated_by", "support@smarterd.com");
		} else if(type.equalsIgnoreCase("VULNERABILITY")) {
			dataObj.put("updatedBy", "support@smarterd.com");
		}

		// Form Update SQL
		String updatesql = "update "+tableName+" set ";
		String columnstr = "", valuestr = "";
		int count = 0;
		iter = dataObj.keySet().iterator();
		while(iter.hasNext()) {
			String columnName = (String)iter.next();
			Object columnValueObj = dataObj.get(columnName);
			//System.out.println(columnName+":"+columnValueObj.toString());
			if(count == 0) {
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_comment")) {
					valuestr = columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("ecosystem")) {
					if(columnValueObj != null) {
						valuestr = columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr = columnName+"="+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("asset_value")) {
					valuestr = columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_feature_list")) {
					valuestr = columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("groups")) {
					if(columnValueObj != null) {
						valuestr = columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr = columnName+"="+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr = columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					String columnValue = "";
					if(columnValueObj != null) {
						columnValue = (String)columnValueObj;
					}
					valuestr = columnName+"="+"'"+columnValue+"'";
				}
			} else {
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = valuestr+","+columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("asset_comment")) {
					valuestr = valuestr+","+columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("ecosystem")) {
					if(columnValueObj != null) {
						valuestr = valuestr+","+columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr = valuestr+","+columnName+"="+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("asset_value")) {
					if(columnValueObj instanceof OrderedJSONObject) {
						valuestr = valuestr+","+columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
					} else {
						valuestr = valuestr+","+columnName+"="+"'"+(new OrderedJSONObject()).toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("asset_feature_list")) {
					if(columnValueObj instanceof OrderedJSONObject) {
						valuestr = valuestr+","+columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
					} else {
						valuestr = valuestr+","+columnName+"="+"'"+(new OrderedJSONObject()).toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("groups")) {
					if(columnValueObj != null) {
						valuestr =  valuestr+","+columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else {
						valuestr =  valuestr+","+columnName+"="+"'"+new JSONArray().toString()+"'";
					}
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr =  valuestr+","+columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					System.out.println(columnName);
					if(columnValueObj != null) {
						if(columnValueObj instanceof Integer) {
							int columnValue = ((Integer)columnValueObj).intValue();
							valuestr =  valuestr+","+columnName+"="+columnValue;
						} else {
							String columnValue = (String)columnValueObj;
							valuestr =  valuestr+","+columnName+"="+"\""+(String)columnValue+"\"";
						}
					}
					
				}
			}
			count++;
		}
		
		if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM") || type.equalsIgnoreCase("VULNERABILITY")) {
			updatesql = updatesql + valuestr + " where _id=\""+id+"\"";
		} else {
			updatesql = updatesql + valuestr + " where id=\""+id+"\"";
		}
		
		PreparedStatement pst = conn.prepareStatement(updatesql);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();

		// Update Incident Category
		if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM")) {
			updateAssignmentCategory(id);
		} else if(type.equalsIgnoreCase("VULNERABILITY")) {
			createVulnerabilityLog(id, dataObj, vulnrLogObj, true);
		}

		// Process Std Rules
		if(stdruleList.size() > 0) {
			String vendorId = null, contractId = null;
			Iterator ruleIter = stdruleList.keySet().iterator();
			while(ruleIter.hasNext()) {
				String rule = (String)ruleIter.next();
				String value = (String)stdruleList.get(rule);
				if(rule.equalsIgnoreCase("CREATE VENDOR")) {
					vendorId = createVendor(value);
				} else if(rule.equalsIgnoreCase("CREATE CONTRACT")) {
					contractId = createContract(id, value);
				} else if(rule.equalsIgnoreCase("CREATE BUSINESS AREA")) {
					createSetting(value, "BUSINESS AREA", "GLOBAL");
				} else if(rule.equalsIgnoreCase("CREATE DEPARTMENT")) {
					createSetting(value, "Department", "GLOBAL");
				} else if(rule.equalsIgnoreCase("CREATE APPLICATION")) {
					createAsset(id, value, type);
				}
			}

			// Create Relationship
			if(vendorId != null && contractId != null) {
				createRel(vendorId, contractId, "3RD PARTY", "AGREEMENT", "support@smarterd.com");
			}
		}

		// Add Features
		iter = featureList.keySet().iterator();
		while(iter.hasNext()) {
			String label = (String)iter.next();
			String feature = (String)featureList.get(label);
			createFeatures(id, label, feature);
		}

		// Add Knowledge
		iter = knowledgeList.keySet().iterator();
		while(iter.hasNext()) {
			String label = (String)iter.next();
			String knowledge = (String)knowledgeList.get(label);
			createKnowledge(id, label, knowledge);
		}

		// Add Work Notes
		if(worknotesObj.size() > 0) {
			//createWorkNotes(worknotesObj);
		}
	}

	private void createAsset(String id, String name, String type) throws Exception {
		String checksql = "select id from enterprise_asset where asset_name = ?";
		String assetId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, name);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			assetId = rs.getString("id");
		}
		rs.close();
		pst.close();

		if(assetId == null) {
			System.out.println("Creating Asset...");
			String insertsql = "insert into enterprise_asset(id,asset_name,asset_category,lifecycle_name,asset_lifecycle_list,extension,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(?,?,?,'',json_array(),?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,?)";
			assetId = generateUUID();
			OrderedJSONObject extObj = new OrderedJSONObject();
			if(type.equals("APPLICATION")) {
				extObj = initializeApplicationExtension(extObj);
			} else if(type.equals("IT ASSET")) {
				extObj = initializeITAssetExtension(extObj);
			} else if(type.equals("SOFTWARE")) {
				extObj = initializeSoftwareExtension(extObj);
			}
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, assetId);
			pst.setString(2, name);
			pst.setString(3, type);
			pst.setString(4, extObj.toString());
			pst.setString(5, companyCode);
			pst.executeUpdate();
			pst.close();
		}

		// Create Relationship
		String relType = null;
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE")) {
			relType = "ASSET";
		} else {
			relType = type;
		}
		createRel(id, assetId, relType, "ASSET", "support@smarterd.com");
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "peapi", password = "smartEuser2022!";
		//System.out.println(hostname);
		//String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private String createVendor(String vendorName) throws Exception {
		String checksql = "select id from enterprise_strategy where enterprise_strategy_name = ? and is_activity = 'N' and plan_type = '3RD PARTY'";
		String insertsql = "insert into enterprise_strategy(id,enterprise_strategy_name,is_activity,plan_type,extension,company_code,created_by,updated_by) values(?,?,'N','3RD PARTY',json_object(),?,'support@smarterd.com','support@smarterd.com')";

		String vendorId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, vendorName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			vendorId = rs.getString("id");
		}
		rs.close();
		pst.close();

		if(vendorId == null) {
			vendorId = generateUUID();
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, vendorId);
			pst.setString(2, vendorName);
			pst.setString(3, companyCode);
			pst.executeUpdate();
			pst.close();
		}

		return vendorId;
	}

	private String createContract(String vendorId, String contractName) throws Exception {
		String checksql = "select id from enterprise_asset where asset_name = ?";
		String insertsql = "insert into enterprise_asset(id,asset_name,asset_type,extension,company_code,created_by,updated_by) values(?,?,'AGREEMENT',?,?,'support@smarterd.com','support@smarterd.com')";

		String contractId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, contractName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			contractId = rs.getString("id");
		}
		rs.close();
		pst.close();

		if(contractId == null) {
			if(vendorId == null) {
				vendorId = "";
			}
			contractId = generateUUID();
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("lifecycle", "Active");
			extObj.put("end_of_life", "");
			extObj.put("Significant?", "");
			extObj.put("Document Name", "");
			extObj.put("signed_by_name", "");
			extObj.put("signed_by_title", "");
			extObj.put("Contracting Entity", new JSONArray());
			extObj.put("primary_business_area", "");
			extObj.put("terminate_convenience", "");
			extObj.put("Amendment/Modification", "");
			extObj.put("lifecycle_auto_renewal", "");
			extObj.put("lifecycle_clause_details", "");
			extObj.put("counterparty_contact_name", "");
			extObj.put("lifecycle_expiration_type", "");
			extObj.put("counterparty_contact_email", "");
			extObj.put("counterparty_contact_phone", "");
			extObj.put("counterparty_contact_title", "");
			extObj.put("lifecycle_termination_notice", "");
			extObj.put("lifecycle_auto_renewal_period", "");
			extObj.put("Ecosystem", new JSONArray());
			extObj.put("vendor_id", vendorId);

			pst = conn.prepareStatement(insertsql);
			pst.setString(1, contractId);
			pst.setString(2, contractName);
			pst.setString(3, extObj.toString());
			pst.setString(4, companyCode);
			pst.executeUpdate();
			pst.close();
		}

		return contractId;
	}

	private void createRel(String componentId, String assetId, String componentType, String assetType, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}
    }

	private void createSetting(String name, String category, String type) throws Exception {
        System.out.println("Loading Setting Category..." + name);
		String checkSQL = "select id from enterprise_setting where setting_name=? and setting_category=? and setting_type=?";
		String insertSQL = "insert into enterprise_setting(id,setting_name,setting_desc,setting_category,setting_type,setting_value,updated_by,updated_timestamp,company_code) value(null,?,?,?,?,null,'support@smarterd.com',CURRENT_TIMESTAMP,?)";

        // Check if name already exists
		int settingId = 0;
        PreparedStatement pst = conn.prepareStatement(checkSQL);
        pst.setString(1, name.trim());
        pst.setString(2, category.trim());
		pst.setString(3, type);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            settingId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Setting Found:" + settingId);

        if (settingId == 0) {
            pst = conn.prepareStatement(insertSQL);
            pst.setString(1, name.trim());
            pst.setString(2, null);
            pst.setString(3, category);
            pst.setString(4, type);
            pst.setString(5, companyCode);

            pst.executeUpdate();
            pst.close();

            System.out.println("Successfully inserted new setting:" + settingId + ":" + name);
        } else {
            System.out.println("Setting already exists:" + name);
        }
	}

	private void createFeatures(String assetId, String label, String feature) throws Exception {
		String checksql = "select count(*) count from enterprise_comment where TOPIC_NAME = ? and comp_id = ?";
		String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,comment_type,extension,status,CREATED_BY,updated_by,COMPANY_CODE) values(null,?,?,?,'Feature',?,?,'support@smarterd.com','support@smarterd.com',?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, label);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("component_type", "ASSET");
			OrderedJSONObject statusObj = new OrderedJSONObject();
			statusObj.put("userGenerated", new Boolean(true));
			statusObj.put("applicable", "");
			statusObj.put("maturity", "");
			statusObj.put("topiclink", "");
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, assetId);
			pst.setString(2, label);
			pst.setString(3, feature);
			pst.setString(4, extObj.toString());
			pst.setString(5, statusObj.toString());
			pst.setString(6, companyCode);
			pst.executeUpdate();
			pst.close();
		}
	}

	private void createKnowledge(String assetId, String label, String knowledge) throws Exception {
		String checksql = "select count(*) count from enterprise_comment where TOPIC_NAME = ? and comp_id = ?";
		String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,comment_type,extension,status,CREATED_BY,updated_by,COMPANY_CODE) values(null,?,?,?,'Recommendation',?,?,'support@smarterd.com','support@smarterd.com',?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, label);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("component_type", "ASSET");
			OrderedJSONObject statusObj = new OrderedJSONObject();
			statusObj.put("userGenerated", new Boolean(true));
			statusObj.put("applicable", "");
			statusObj.put("maturity", "");
			statusObj.put("topiclink", "");
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, assetId);
			pst.setString(2, label);
			pst.setString(3, knowledge);
			pst.setString(4, extObj.toString());
			pst.setString(5, statusObj.toString());
			pst.setString(6, companyCode);
			pst.executeUpdate();
			pst.close();
		}
	}

	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	/*
	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	*/

	public static String crunchifyJSONEscapeUtil(String crunchifyJSON) {
		// StringBuilder(): Constructs a string builder with no characters in it and an initial capacity of 16 characters.
        final StringBuilder crunchifyNewJSON = new StringBuilder();
 
        // StringCharacterIterator class iterates over the entire String
        StringCharacterIterator crunchifyIterator = new StringCharacterIterator(crunchifyJSON);
 
		// current(): Implements CharacterIterator.current() for String.
        char crunchifyChar = crunchifyIterator.current();
 
        // DONE = \\uffff (not a character)
        while (crunchifyChar != StringCharacterIterator.DONE) {
            if (crunchifyChar == '\"') {
                crunchifyNewJSON.append("\\\"");
            } else if (crunchifyChar == '\t') {
                crunchifyNewJSON.append("\\t");
            } else if (crunchifyChar == '\f') {
                crunchifyNewJSON.append("\\f");
            } else if (crunchifyChar == '\n') {
                crunchifyNewJSON.append("\\n");
            } else if (crunchifyChar == '\r') {
                crunchifyNewJSON.append("\\r");
            } else if (crunchifyChar == '\\') {
                crunchifyNewJSON.append("\\\\");
            } else if (crunchifyChar == '/') {
                crunchifyNewJSON.append("\\/");
            } else if (crunchifyChar == '\b') {
                crunchifyNewJSON.append("\\b");
            } else {
                // Nothing matched - just as text as it is.
				// next(): Implements CharacterIterator.next() for String.
                crunchifyNewJSON.append(crunchifyChar);
            }
            crunchifyChar = crunchifyIterator.next();
        }
        return crunchifyNewJSON.toString();
    }

	private String getTableName(String type) {
		String name = null;
		if(type.equalsIgnoreCase("APPLICATION") || type.equalsIgnoreCase("IT ASSET") || type.equalsIgnoreCase("SOFTWARE") || type.equalsIgnoreCase("CONTRACT")) {
			name = "enterprise_asset";
		} else if(type.equalsIgnoreCase("INCIDENT") || type.equalsIgnoreCase("PROBLEM")) {
			name = "enterprise_incident";
		} else if(type.equalsIgnoreCase("VENDOR")) {
			name = "enterprise_strayegy";
		} else if(type.equalsIgnoreCase("VULNERABILITY")) {
			name = "enterprise_vulnerability";
		}

		return name;
	}

	private String formatDate(String d) throws Exception {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");   // 10
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/d/yyyy");   // 9
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("M/dd/yyyy");   // 9
        DateTimeFormatter df3 = DateTimeFormatter.ofPattern("MM/dd/yy");    // 8
        DateTimeFormatter df4 = DateTimeFormatter.ofPattern("M/d/yyyy");    // 8
        DateTimeFormatter df5 = DateTimeFormatter.ofPattern("MM/d/yy");     // 7
        DateTimeFormatter df6 = DateTimeFormatter.ofPattern("M/dd/yy");     // 7
        DateTimeFormatter df7 = DateTimeFormatter.ofPattern("M/d/yy");      // 6
        DateTimeFormatter df8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.000'Z'");     // ISO
        DateTimeFormatter df9 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        LocalDate dt = null;
        String formattedDate = "";
        if(d != null && d.length() > 0) {
            d = d.trim();
            if(d.length() == 6 && d.indexOf("/") != -1) {
                dt = LocalDate.parse(d, df7);
                formattedDate = df.format(dt);
			} else if (d.length() == 7 && d.indexOf("/") != -1) {    //1/01/07
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df6);
                } else {
                    dt = LocalDate.parse(d, df5);
                }
                formattedDate = df.format(dt);
            } else if (d.length() == 8 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df4);
                } else {
                    dt = LocalDate.parse(d, df3);
                }
                formattedDate = df.format(dt);
			} else if (d.length() == 9 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df2);
                } else {
                    dt = LocalDate.parse(d, df1);
                }
                formattedDate = df.format(dt);
			} else if(d.indexOf("T") != -1) {
                dt = LocalDate.parse(d, df8);
                formattedDate = df.format(dt);
			} else if(d.indexOf(" ") != -1) {
                dt = LocalDate.parse(d, df9);
                formattedDate = df.format(dt);
			} else {
                formattedDate = d;
            }
        }

        return formattedDate;
    }

	private void updateAssignmentCategory(String id) throws Exception {
		String sql = "select _id,json_unquote(json_extract(extension, '$.\"Assignment Group\"')) grp from enterprise_incident where _id=?";
		String categorysql = "select json_unquote(json_extract(extension, '$.\"storage_value\"')) storage_value from enterprise_setting where setting_category = 'Incident Group' and setting_name=?";
		String updatesql = "update enterprise_incident set extension=json_set(extension, '$.\"Assignment Category\"', ?) where _id=?";
		
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, id);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String group = rs.getString("grp");
			if(group != null && group.length() > 0) {
				PreparedStatement pst1 = conn.prepareStatement(categorysql);
				pst1.setString(1, group);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					String category = rs1.getString("storage_value");
					PreparedStatement pst2 = conn.prepareStatement(updatesql);
					pst2.setString(1, category);
					pst2.setString(2, id);
					pst2.executeUpdate();
					pst2.close();
				}
				rs1.close();
				pst1.close();
			}
		}
		rs.close();
		pst.close();
	}

	private void createVulnerabilityLog(String id, OrderedJSONObject vulnrObj, OrderedJSONObject vulnrLog, boolean existingVulnr) throws Exception {
		System.out.println("In createVulnerabilityLog..."+id+":"+vulnrLog.toString());
		String scanId = null;

		String pluginId = "";
		if(vulnrObj.has("extension")) {
			OrderedJSONObject extObj = (OrderedJSONObject)vulnrObj.get("extension");
			if(extObj.has("Scanner Plugin ID")) {
				pluginId = (String)extObj.get("Scanner Plugin ID");
			}
		}
		String scanPeriod = "";
		if(vulnrLog.has("scan_period")) {
			scanPeriod = (String)vulnrLog.get("scan_period");
		}
		String port = "";
		if(vulnrLog.has("Port")) {
			port = (String)vulnrLog.get("Port");
		}
		String service = "";
		if(vulnrLog.has("Service")) {
			service = (String)vulnrLog.get("Service");
		}
		String hostName = "";
		if(vulnrLog.has("asset_name")) {
			hostName = (String)vulnrLog.get("asset_name");
		}
		if(vulnrLog.has("scan_id")) {
			scanId = (String)vulnrLog.get("scan_id");
		} else {
			scanId = pluginId+":"+hostName+":"+port+":"+service;
		}
		String vulnrName = "";
		if(vulnrLog.has("vulnerability_name")) {
			vulnrName = (String)vulnrLog.get("vulnerability_name");
		}
		String vulnrType = "";
		if(vulnrObj.has("Vulnerability Type")) {
			vulnrType = (String)vulnrObj.get("Vulnerability Type");
		}
		String assetId = "";
		if(vulnrLog.has("asset_id")) {
			assetId = (String)vulnrLog.get("asset_id");
		}
		String hostId = "";
		if(vulnrLog.has("Host Id")) {
			hostId = (String)vulnrLog.get("Host Id");
		}
		String ipAddress = "";
		if(vulnrLog.has("IP Address")) {
			ipAddress = (String)vulnrLog.get("IP Address");
		}
		String osName = "";
		if(vulnrLog.has("OS Name")) {
			osName = (String)vulnrLog.get("OS Name");
		}
		String priority = "";
		if(vulnrObj.has("priority")) {
			priority = (String)vulnrObj.get("priority");
		}
		String lastseenOn = "";
		if(vulnrObj.has("Last Found On")) {
			lastseenOn = (String)vulnrObj.get("Last Found On");
		}
		String discoveredOn = "";
		if(vulnrObj.has("Last Discovered On")) {
			discoveredOn = (String)vulnrObj.get("Discovered On");
		}
		String lifecycle = "";
		if(vulnrObj.has("lifecycle")) {
			lifecycle = (String)vulnrObj.get("lifecycle");
		}
		String groupName = "";
		if(vulnrObj.has("group_name")) {
			groupName = (String)vulnrObj.get("group_name");
		}
		String scannerOutput = "";
		if(vulnrObj.has("Scanner Output")) {
			scannerOutput = (String)vulnrObj.get("Scanner Output");
		}
		String source = "";
		if(vulnrObj.has("source")) {
			source = (String)vulnrObj.get("source");
		}
		String status = "";
		if(vulnrLog.has("status")) {
			status = (String)vulnrLog.get("status");
		}

		linkAsset(scanId, hostName, hostId, ipAddress, osName, id, vulnrName, vulnrType, priority, lifecycle, discoveredOn, lastseenOn, port, service, scannerOutput, groupName, source, status, scanPeriod, existingVulnr);
	}

	private String linkAsset(String scanId, String hostName, String hostId, String ipAddress, String osName, String vulnrId, String vulnrName, String vulnrType, String priority, String lifecycle,
						     String discoveredOn, String lastseenOn, String ports, String service, String scannerOutput, String groupName, String adaptorType, String scanStatus, String scanPeriod, 
							 boolean existingVulnr) throws Exception {
		System.out.println("linkAsset=>"+hostName+":"+ipAddress+":"+vulnrId);
		String status = "";
		JSONArray serviceList = new JSONArray();
		if(service != null && service.length() > 0) {
			String[] str = service.split(",");
			for(int j=0; j< str.length; j++) {
				serviceList.add(str[j].trim());
			}
		} else {
			serviceList.add("");
		}

		JSONArray portList = new JSONArray();
		if(ports != null && ports.length() > 0) {
			String[] str = ports.split(",");
			for(int j=0; j< str.length; j++) {
				portList.add(str[j].trim());
			}
		} else {
			portList.add("");
		}
		
		if(osName != null && osName.length() > 0) {
			String osId = null;
			String ossql = "select id from enterprise_asset where (asset_name = ? or instr(json_extract(extension,'$.\"Other Names\"'),'REPLACE_STRING') > 0)";
			String replacestr = osName.trim();
            replacestr = replacestr.replaceAll("\\'","\\'\\'");
            ossql = ossql.replaceAll("REPLACE_STRING", replacestr);
			PreparedStatement pst = conn.prepareStatement(ossql);
			pst.setString(1, osName.trim());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				osId = rs.getString("id");
			}
			rs.close();
			pst.close();

			if(osId == null) {
				String ext = null;
				ossql = "select id, extension from enterprise_vulnerability_asset where asset_name = ?";
				pst = conn.prepareStatement(ossql);
				pst.setString(1, osName.trim());
				rs = pst.executeQuery();
				while (rs.next()) {
					osId = rs.getString("id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				// Add OS if not present
				if(osId == null || osId.length() == 0) {
					System.out.println("Asset not found. Adding["+osName+"]");
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("sync", "N");
					extObj.put("alias_name", new JSONArray());
					extObj.put("discovered_on", discoveredOn);
					extObj.put("last_found_on", lastseenOn);
					extObj.put("reviewed", "N");
					extObj.put("comment", "");
					osId = generateUUID();
					String insertSQL = "insert into enterprise_vulnerability_asset(id,asset_name,extension,asset_category,createdBy,createdAt,updatedAt) values(?,?,?,'SOFTWARE','support@smarterd.com',current_timestamp,current_timestamp)";
					pst = conn.prepareStatement(insertSQL);
					pst.setString(1, osId);
					pst.setString(2, osName);
					pst.setString(3, extObj.toString());
					pst.executeUpdate();
					conn.commit();
					pst.close();
				} else {
					String updatesql = "update enterprise_vulnerability_asset set extension=? where id=?";
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("discovered_on", discoveredOn);
					extObj.put("last_found_on", lastseenOn);
					if(!extObj.has("reviewed")) {
						extObj.put("reviewed", "N");
					}
					if(!extObj.has("comment")) {
						extObj.put("comment", "");
					}
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, extObj.toString());
					pst.setString(2, osId);
					pst.executeUpdate();
					pst.close();
				}
			} else {
				String updatesql = "update enterprise_vulnerability_asset set extension=? where asset_name=?";
				ossql = "select extension from enterprise_vulnerability_asset where asset_name = ?";
				String ext = null;
				pst = conn.prepareStatement(ossql);
				pst.setString(1, osName);
				rs = pst.executeQuery();
				while (rs.next()) {
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();
				if(ext != null) {
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("discovered_on", discoveredOn);
					extObj.put("last_found_on", lastseenOn);
					if(!extObj.has("reviewed")) {
						extObj.put("reviewed", "N");
					}
					if(!extObj.has("comment")) {
						extObj.put("comment", "");
					}
					extObj.put("sync", "Y");

					pst = conn.prepareStatement(updatesql);
					pst.setString(1, extObj.toString());
					pst.setString(2, osName);
					pst.executeUpdate();
					pst.close();
				}
			}
			System.out.println("Successfully process OS:"+osId);
		}

		// Check Hostname
		String assetId = null, assetName = null, ext=null;
		boolean hostnameFound = false, ipaddressFound = false;
		String itassetsql = "select id, asset_name, extension from enterprise_asset where asset_name = ?";
		PreparedStatement pst = conn.prepareStatement(itassetsql);
		pst.setString(1, hostName.trim());
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			assetId = rs.getString("id");
			assetName = rs.getString("asset_name");
			ext = rs.getString("extension");
			hostnameFound = true;
		}
		rs.close();
		pst.close();

		// Check IP Address
		if(assetId == null) {
			itassetsql = "select id, asset_name, extension from enterprise_asset where (json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"','$') > 0 or json_extract(extension, '$.\"Service Name\"') = ? or json_extract(extension, '$.\"Load Balancer\"') = ? or json_contains(json_extract(extension, '$.\"Other Names\"'), '\"REPLACE_STRING\"','$') > 0)";
			itassetsql = itassetsql.replaceAll("REPLACE_STRING", ipAddress.trim());
			pst = conn.prepareStatement(itassetsql);
			pst.setString(1, ipAddress.trim());
			pst.setString(2, ipAddress.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				assetId = rs.getString("id");
				assetName = rs.getString("asset_name");
				ext = rs.getString("extension");
				ipaddressFound = true;
			}
			rs.close();
			pst.close();
		}

		if(assetId == null) {
			System.out.println("IT Asset not found!!!");
			itassetsql = "select id, asset_name, extension from enterprise_vulnerability_asset where asset_name = ?";
			pst = conn.prepareStatement(itassetsql);
			pst.setString(1, hostName.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				assetId = rs.getString("id");
				assetName = rs.getString("asset_name");
				ext = rs.getString("extension");
			}
			rs.close();
			pst.close();

			if(assetId == null) {
				System.out.println("Asset not found. Adding["+hostName+"]");
				assetId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("OS", osName);
				extObj.put("sync", "N");
				extObj.put("alias_name", new JSONArray());
				extObj.put("discovered_on", discoveredOn);
				extObj.put("last_found_on", lastseenOn);
				extObj.put("reviewed", "N");
				extObj.put("comment", "");
				extObj.put("port", portList);
				extObj.put("scan_period", scanPeriod);
				extObj.put("scan_status", scanStatus);
				extObj.put("scan_id", scanId);
				String insertSQL = "insert into enterprise_vulnerability_asset(id,asset_name,extension,asset_category,createdBy,createdAt,updatedAt) values(?,?,?,'IT ASSET','support@smarterd.com',current_timestamp,current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, assetId);
				pst.setString(2, hostName);
				pst.setString(3, extObj.toString());
				pst.executeUpdate();
				conn.commit();
				pst.close();
				status = "New Asset";

				// Add to asset list
				assetName = hostName;
			} else {
				String updatesql = "update enterprise_vulnerability_asset set extension=? where id=?";
				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				extObj.put("discovered_on", discoveredOn);
				extObj.put("last_found_on", lastseenOn);
				extObj.put("scan_period", scanPeriod);
				extObj.put("scan_status", scanStatus);
				extObj.put("scan_id", scanId);
				extObj.put("reviewed", "N");
				extObj.put("sync", "N");
				if(!extObj.has("comment")) {
					extObj.put("comment", "");
				}
				JSONArray arr = null;
				if(extObj.has("port")) {
					arr = (JSONArray)extObj.get("port");
				} else {
					arr = new JSONArray();
				}
				if(portList.size() > 0) {
					if(arr.size() == 0) {
						extObj.put("port", portList);
					} else {
						Iterator iter = portList.iterator();
						while(iter.hasNext()) {
							String p = (String)iter.next();
							if(!arr.contains(p)) {
								arr.add(p);
							}
						}
						
					}
				}
				extObj.put("port", arr);
				pst = conn.prepareStatement(updatesql);
				pst.setString(1, extObj.toString());
				pst.setString(2, assetId);
				pst.executeUpdate();
				pst.close();
				status = "Existing Not Synced Asset";
				//assetList.put(assetId, hostName);
			}
		} else {
			System.out.println("IT Asset found!!!");
			String updatesql = "update enterprise_vulnerability_asset set extension=? where asset_name=?";
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			extObj.put("discovered_on", discoveredOn);
			extObj.put("last_found_on", lastseenOn);
			extObj.put("scan_period", scanPeriod);
			extObj.put("scan_status", scanStatus);
			extObj.put("scan_id", scanId);
			extObj.put("reviewed", "N");
			extObj.put("sync", "Y");
			
			if(!extObj.has("comment")) {
				extObj.put("comment", "");
			}
			JSONArray arr = null;
			if(extObj.has("port")) {
				arr = (JSONArray)extObj.get("port");
			} else {
				arr = new JSONArray();
			}
			if(portList.size() > 0) {
				if(arr.size() == 0) {
					extObj.put("port", portList);
				} else {
					Iterator iter = portList.iterator();
					while(iter.hasNext()) {
						String p = (String)iter.next();
						if(!arr.contains(p)) {
							arr.add(p);
						}
					}
					
				}
			}
			extObj.put("port", arr);

			pst = conn.prepareStatement(updatesql);
			pst.setString(1, extObj.toString());
			if(hostnameFound) {
				pst.setString(2, hostName);
			} else {
				pst.setString(2, ipAddress);
			}
			pst.executeUpdate();
			pst.close();
			status = "Existing Asset";
		}
		System.out.println("Successfully process IT Asset:"+assetId);

		// Update Vulnerability Load
		if(existingVulnr) {
			status = "Existing Vulnr "+status;
		} else {
			status = "New Vulnr "+status;	
		}

		String assetStatus = "Active";
		if((lifecycle.equalsIgnoreCase("Risk Acceptance") || lifecycle.equalsIgnoreCase("False Positive") || lifecycle.equalsIgnoreCase("Not Applicable")) && scanStatus.equalsIgnoreCase("Closed")) {
			assetStatus = "Risk Acceptance";
		} else if(scanStatus.equalsIgnoreCase("Closed")) {
			assetStatus = "Complete";
		} else if(lifecycle.equalsIgnoreCase("Risk Acceptance") || lifecycle.equalsIgnoreCase("False Positive") || lifecycle.equalsIgnoreCase("Not Applicable")) {
			assetStatus = lifecycle;
		}

		updateVulnerabilityLog(scanId, vulnrId, vulnrName, vulnrType, assetId, assetName, hostName, hostId, ipAddress, portList, serviceList, priority, status, assetStatus, lastseenOn, adaptorType, groupName, scannerOutput, scanStatus, scanPeriod);
	
		return status;
	}

	private void updateVulnerabilityLog(String scanId, String vulnrId, String vulnrName, String vulnrType, String assetId, String assetName, String hostname, String hostId, String ipAddress,
                                        JSONArray portList, JSONArray serviceList, String priority, String assetstatus, String remediationStatus, String lastseen, String source, 
                                        String groupName, String scannerOutput, String scanStatus, String scanPeriod) throws Exception {
        String checksql = "select id, extension from enterprise_vulnerability_log where vulnerability_name=? and hostname=? and json_extract(extension, '$.\"Port\"[0]') = ? and json_extract(extension, '$.\"scan_period\"')=?";
        String checkscanidsql = "select id, extension from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.\"scan_id\"')=? and json_extract(extension, '$.\"scan_period\"')=?";
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate cd = LocalDate.now();
        String loadDate = cd.format(df);

        // Check if this vulnerability already exists
        String id = null, ext = null;
        PreparedStatement pst = conn.prepareStatement(checkscanidsql);
        pst.setString(1, scanId);
        pst.setString(2, scanPeriod);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            id = rs.getString("id");
            ext = rs.getString("extension");
        }
        rs.close();
        pst.close();

        if(id == null) {
            // Check if this exists in prior load
            String status = "New";
            if(portList.size() > 0) {
                String port = (String)portList.get(0);
                checksql = "select count(*) count from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.\"scan_id\"')=? and json_extract(extension, '$.\"scan_period\"') != ?";
                int count = 0;
                pst = conn.prepareStatement(checksql);
                pst.setString(1, scanId);
                pst.setString(2, scanPeriod);
                rs = pst.executeQuery();
                while(rs.next()) {
                    count = rs.getInt("count");
                }
                rs.close();
                pst.close();

                if(count > 0) {
                    status = "Repeat";
                }
            }

            String insertsql = "insert into enterprise_vulnerability_log(id,vulnerability_id,vulnerability_name,asset_id,asset_name,ipaddress,hostname,priority,status,load_date,source,extension,created_timestamp) values(uuid(),?,?,?,?,?,?,?,?,?,?,?,current_timestamp)";
            OrderedJSONObject threatCtx = new OrderedJSONObject();
			threatCtx.put("scan_id", scanId);
		   	threatCtx.put("status", remediationStatus);
            threatCtx.put("Ticket Reference", new JSONArray());
            threatCtx.put("notes", "");
            threatCtx.put("assignee", new JSONArray());
            threatCtx.put("IP Address", ipAddress);
            threatCtx.put("Port", portList);
            threatCtx.put("Service", serviceList);
            threatCtx.put("Evidence", "");
            threatCtx.put("source", source);
            threatCtx.put("Host Id", hostId);
            threatCtx.put("scan_sync", "");
            threatCtx.put("lifecycle", "Active");
            threatCtx.put("Scanner Output", scannerOutput);
            threatCtx.put("group_name", groupName);
            threatCtx.put("Vulnerability Type", vulnrType);

            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("asset_status", assetstatus);
            extObj.put("scan_period", scanPeriod);
            extObj.put("scan_status", scanStatus);
            extObj.put("scan_sync", "");
            extObj.put("sync_name", "");
            extObj.put("sync_status", "");
            extObj.put("THREAT", threatCtx);

            pst = conn.prepareStatement(insertsql);
            pst.setString(1, vulnrId);
            pst.setString(2, vulnrName);
            pst.setString(3, assetId);
            pst.setString(4, assetName);
            pst.setString(5, ipAddress);                     
            pst.setString(6, hostname);
            pst.setString(7, priority);
            pst.setString(8, status);
            pst.setString(9, loadDate);
            pst.setString(10, source);
            pst.setString(11, extObj.toString());
            pst.executeUpdate();
            pst.close();
        } else {
            String updatesql = "update enterprise_vulnerability_log set vulnerability_id=?,asset_id=?,vulnerability_name=?,asset_name=?,ipaddress=?,hostname=?,extension = ? where id = ?";
            OrderedJSONObject extObj = new OrderedJSONObject(ext);
            OrderedJSONObject threatCtx = null;
            if(extObj.has("THREAT")) {
                threatCtx = (OrderedJSONObject)extObj.get("THREAT");
            } else {
                threatCtx = new OrderedJSONObject();
            }
			threatCtx.put("scan_id", scanId);
            threatCtx.put("status", remediationStatus);
            threatCtx.put("IP Address", ipAddress);
            threatCtx.put("Host Id", hostId);
            threatCtx.put("Port", portList);
            threatCtx.put("service", serviceList);
            threatCtx.put("Vulnerability Type", vulnrType);
            threatCtx.put("scanner_output", scannerOutput);
            threatCtx.put("group_name", groupName);

            extObj.put("scan_period", scanPeriod);
            extObj.put("scan_status", scanStatus);
            extObj.put("THREAT", threatCtx);

            pst = conn.prepareStatement(updatesql);
            pst.setString(1, vulnrId);
            pst.setString(2, assetId);
            pst.setString(3, vulnrName);
            pst.setString(4, assetName);
            pst.setString(5, ipAddress);
            pst.setString(6, hostname);
            pst.setString(7, extObj.toString());
            pst.setString(8, id);
            pst.executeUpdate();
            pst.close(); 
        }
    }

	private void addVulnerabilityToGroup(String vulnrId, String groupName, String source) throws Exception {
		System.out.println("Adding vulnerability to group..."+vulnrId+":"+groupName);
		String checksql = "select _id from enterprise_vulnerability_group where nameId=?";
		String insertsql = "insert into enterprise_vulnerability_group (_id,nameId,name,source,createdAt,updatedAt,createdBy,updatedBy,extension) values(?,?,?,?,current_timestamp,current_timestamp,?,?,?)";
		String email = "support@smarterd.com";
		String groupId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, groupName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			groupId = rs.getString("_id");
		}
		rs.close();
		pst.close();

		if(groupId == null) {
			groupId = generateUUID();
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("lifecycle", "Active");
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, groupId);
			pst.setString(2, groupName);
			pst.setString(3, groupName);
			pst.setString(4, source);
			pst.setString(5, email);
			pst.setString(6, email);
			pst.setString(7, extObj.toString());
			pst.executeUpdate();
			pst.close();
		}

		int count = 0;
		String relchecksql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
		pst = conn.prepareStatement(relchecksql);
		pst.setString(1, vulnrId);
		pst.setString(2, groupId);
		rs = pst.executeQuery();
		while (rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPONENT TYPE", "THREAT ASSET");
			ctx.put("LINKED", "N");
			String relinsertsql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(null,?,?,?,?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,?)";		
			pst = conn.prepareStatement(relinsertsql);
			pst.setString(1, vulnrId);
			pst.setString(2, groupId);
			pst.setString(3, "THREAT GROUP");
			pst.setString(4, ctx.toString());
			pst.setString(5, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", "THREAT GROUP");
			pst.setString(1, groupId);
			pst.setString(2, vulnrId);
			pst.setString(3, "THREAT ASSET");
			pst.setString(4, ctx.toString());
			pst.setString(5, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}

		System.out.println("Successfully added vulnerability to group!!!");
		conn.commit();
	}
	
	public static void main(String[] args) throws Exception {
		String type = args[0];
		String filename = args[1];
		String schemaFile = args[2];
		String companyCode = args[3];
		String env = args[4];
		SmartEDataLoader loader = new SmartEDataLoader(type, filename, schemaFile, companyCode, env);
		loader.load();
	}
}