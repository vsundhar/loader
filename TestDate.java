
import java.util.regex.*;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import org.apache.wink.json4j.OrderedJSONObject;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.time.DayOfWeek;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.math.BigInteger;
import java.util.Base64;
import java.util.stream.Collectors;
import java.util.Collections;
import org.apache.commons.lang3.RandomStringUtils;

public class TestDate {
	public static void main(String[] args) throws Exception {
		/*
		Calendar cal1 = new GregorianCalendar();
	    Calendar cal2 = new GregorianCalendar();
		String d = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		//Date currentDate = new SimpleDateFormat("MM/dd/yyyy").parse(d);
		//cal1.setTime(currentDate);
		Date currentDate = new Date();
        Date actualDate = new SimpleDateFormat("MM/dd/yyyy").parse("07/01/2021");
        cal2.setTime(actualDate);
        System.out.println(currentDate+":"+actualDate);
		long days = ChronoUnit.DAYS.between(currentDate.toInstant(), actualDate.toInstant());
		//int days = (int)( (actualDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
		System.out.println(days);
		*/
		/*
		String msg = "$ASSET_NAME";
		if(msg.indexOf("$ASSET_NAME1") != -1) {
			String msgText = msg.replaceAll("\\$ASSET_NAME", "Vijay");
			System.out.println(msgText);
		}
		*/
		
		/*
		String sql = "select count(*) from enterprise_asset where REPLACE_STRING";
		String criteria = "asset_name like [1] or json_extract(extension, '$.\"Employee Id\"') = [extension.Employee Id]";
		OrderedJSONObject dataObject = new OrderedJSONObject();
		dataObject.put("1", "Vijay");
		OrderedJSONObject extObj = new OrderedJSONObject();
		extObj.put("Employee Id", "12345");
		dataObject.put("2", extObj.toString());
		int index = 0;
		while(index != -1){
			index = criteria.indexOf("[", index);
			if(index != -1) {
				int endIndex = criteria.indexOf("]", index);
				System.out.println(index+":"+endIndex);
				String str = criteria.substring(index+1, endIndex);
				System.out.println(str);
				if(str.indexOf("extension") != -1) {
					int extIndex = str.indexOf(".");
					String extstr = str.substring(extIndex+1);
					System.out.println(extstr);
					String ext = (String) dataObject.get(Integer.toString(2));
					extObj = new OrderedJSONObject(ext);
					if(extObj.has(extstr)) {
						String value = (String)extObj.get(extstr);
						criteria = criteria.replaceAll("\\["+str+"\\]", "'"+value+"'");
					}
				} else {
					String value = (String)dataObject.get(str);
					criteria = criteria.replaceAll("\\["+str+"\\]", "'"+value+"'");
				}
				if (index != -1) {
					index++;
				}
			}
		}
		
		System.out.println("Criteria:"+criteria);
		//criteria = criteria.replaceAll("\\[", "");
		//criteria = criteria.replaceAll("\\]", "");
		sql = sql.replaceAll("REPLACE_STRING", Matcher.quoteReplacement(criteria));
		*/
		/*
		int totalPercent = Math.round(((float)2/10) * 100);
		System.out.println(totalPercent);

		String d = "08/12/2020";
		String isoFormat = "^((19|2[0-9])[0-9]{2})([- /.])(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
		//String isoFormat = "^[0-9]{4}([- /.])(((0[13578]|(10|12))\1(0[1-9]|[1-2][0-9]|3[0-1]))|(02\1(0[1-9]|[1-2][0-9]))|((0[469]|11)\1(0[1-9]|[1-2][0-9]|30)))$";
		String usFormat = "^(((0[13578]|(10|12))/(0[1-9]|[1-2][0-9]|3[0-1]))|(02/(0[1-9]|[1-2][0-9]))|((0[469]|11)/(0[1-9]|[1-2][0-9]|30)))/[0-9]{4}$";
		String format = null;

		if(d.matches(isoFormat)) {
			format = "iso";
		} else if(d.matches(usFormat)) {
			format = "us";
		}
		System.out.println(format);
		*/

		/*
		// Get calendar set to current date and time
		Calendar c = Calendar.getInstance();

		// Set the calendar to monday of the current week
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		System.out.println();
		// Print dates of the current week starting on Monday
		SimpleDateFormat df = new SimpleDateFormat("EEE MM/dd/yyyy");
		System.out.println(df.format(c.getTime()));
		c.add(Calendar.DATE, 7);
		System.out.println(df.format(c.getTime()));
		System.out.println();
		*/

		// Add one holiday for testing
		/*
		TestDate td = new TestDate();
		String sd = "02/10/2021";
		String ed = "03/01/2021";
        List<LocalDate> holidays = new ArrayList<>();
        holidays.add(LocalDate.of(2020, 5, 11));
        holidays.add(LocalDate.of(2020, 5, 1));
 
		long days = td.getBusinessDays(sd, ed, Optional.empty());
		System.out.println(days);
		*/

		/*
		String a = "a";
		String base64encodedString = Base64.getEncoder().encodeToString(a.getBytes("utf-8"));
		BigInteger i = new BigInteger(base64encodedString.getBytes());
		int j = i.intValue();
		System.out.println(j);
		*/

		/*
		String date = "2021-06-10T15:12:44.137-0700";
		if(date.indexOf("T") != -1) {
			date = date.substring(0, date.indexOf("T"));
			DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate dt = LocalDate.parse(date, df);
			date = df1.format(dt);
			System.out.println(date);
		}
		*/
		
		/*
		DateTimeFormatter df2 = DateTimeFormatter.ofPattern("M/d/yyyy");
		String enddate = "8/1/2021";
		LocalDate ed = LocalDate.parse(enddate, df2);
		System.out.println(ed.toString());
		*/

		/*
		LocalDate currentDate = LocalDate.now();
        String month = currentDate.getMonth().name();
		String year = Integer.toString(currentDate.getYear()).substring(2);
		System.out.println(year);
		*/
		/*
		String runText = "${PI}";
		int openTagCount = runText.split("\\$\\{", -1).length - 1;
        int closeTagCount = runText.split("}", -1).length - 1;
		System.out.println(openTagCount+":"+closeTagCount);
		*/
		/*
		TestDate td = new TestDate();
		String pwd = td.generateCommonLangPassword();
		System.out.println(pwd);
		*/

		/*
		String s = "1.1";
		String [] sArr = s.split("[.]");
		System.out.println(sArr.length);
		*/

		/*
		String pad = "0000";
		int idx = 11;
		String uniqueKey = (String)pad.subSequence(0, pad.length() - Integer.toString(idx).length());
		System.out.println(uniqueKey);
		*/

		/*
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String lastUpdated = "11/07/2022";
		String currentDate = "11/08/2022";
		LocalDate lastUpdatedDate = LocalDate.parse(lastUpdated, df);
		long days = getBusinessDays(currentDate, lastUpdatedDate.format(df), Optional.empty());
		System.out.println(days);
		*/

		/*
		LocalDate ld = LocalDate.now();
		System.out.println(ld.getMonth().toString().substring(0,3));
		System.out.println(Integer.toString(ld.getYear()).substring(2));
		*/
		/*
		String aliasName = "it_cio(mix)/Business";
		aliasName = aliasName.replaceAll("[\\(\\)\\/]","_");
		System.out.println(aliasName);
		*/
		/*
		String slashstr = "hacla\\Vijay Sundhar;hacla\\Julian Mazaira";
		slashstr = slashstr.replaceAll(";", ",");
		if(slashstr.indexOf(",") != -1) {
			String[] list = slashstr.split(",");
			for(int i=0; i<list.length; i++) {
				String str = list[i];
				if(str.indexOf("\\") != -1) {
					str = str.substring(str.indexOf("\\")+1);
				}
				System.out.println(str);
			}
		} else if(slashstr.indexOf("\\") != -1) {
			slashstr = slashstr.substring(slashstr.indexOf("\\")+1);
			System.out.println(slashstr);
		} else {
			System.out.println(slashstr);
		}
		*/
		/*
		String v = "MADS/Test.csv";
		if(v.indexOf("/") != -1) {
			System.out.println(v.substring(v.indexOf("/")+1));
		}
		*/
		/*
		String d = "Jun 8, 2023";
		DateTimeFormatter df13 = DateTimeFormatter.ofPattern("MMM d, yyyy");
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		if(d.indexOf(",") != -1) {
			LocalDate dt = LocalDate.parse(d, df13);
            d = df.format(dt);
			System.out.println(d);
		}
		*/
		/*
		String d = "2022-11-16T15:39:24+04:00";
		if(d.indexOf("+") != -1) {
			d = d.substring(0,d.indexOf("+"));
		}
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");     // ISO_INSTANT
		LocalDateTime dt = LocalDateTime.parse(d, df1);
		d = df.format(dt);
		System.out.println(d);
		*/

		int count = 150;
		int batch = (int)Math.ceil((double)count/(double)100);
		System.out.println(batch);
	}

	private static long getBusinessDays(String startdate, String enddate, Optional<List<LocalDate>> holidays) {
		System.out.println(startdate+":"+enddate);
		long businessDays = 0;
        if(startdate != null && enddate != null & holidays != null) {
			DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate sd = LocalDate.parse(startdate, df);
			LocalDate ed = LocalDate.parse(enddate, df);

			Predicate<LocalDate> isHoliday = date -> holidays.isPresent() ? holidays.get().contains(date) : false;
			Predicate<LocalDate> isWeekend = date -> date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY;
			
			long daysBetween = ChronoUnit.DAYS.between(sd, ed);
			if(daysBetween < 0) {
				daysBetween = Math.abs(daysBetween);
			}
			System.out.println(daysBetween);
			businessDays = Stream.iterate(ed, date -> date.plusDays(1)).limit(daysBetween).filter(isHoliday.or(isWeekend).negate()).count();
		}
        return businessDays;
    }

	private String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
		String totalChars = RandomStringUtils.randomAlphanumeric(2);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
		.concat(numbers)
		.concat(specialChar)
		.concat(totalChars);
		List<Character> pwdChars = combinedChars.chars()
		.mapToObj(c -> (char) c)
		.collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream()
		.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
		.toString();
		return password;
	}
}