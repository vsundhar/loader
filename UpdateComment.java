import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.*;
import java.time.Duration;
import java.time.Instant;

import  org.apache.commons.text.WordUtils;

import java.text.*;
import java.sql.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class UpdateComment {
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
	String filename = null;
   	
	public UpdateComment(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		this.filename =filename;

		if(env.equalsIgnoreCase("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equalsIgnoreCase("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String commentsql = "select id, comp_id, extension from enterprise_comment where comment_type in ('RECOMMENDATION', 'FEATURE')";
		String capabilitysql = "select business_capability_category from enterprise_business_capability where id = ?";
		String assetsql = "select asset_category from enterprise_asset where id = ?";
		String plansql = "select category from enterprise_threat_plan where _id = ?";
		String subplansql = "select category from enterprise_threat_subplan where _id = ?";
		String updatesql = "update enterprise_comment set extension = ? where id = ?";
		PreparedStatement pst2 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(commentsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String compId = rs.getString("comp_id");
			String ext = rs.getString("extension");
			System.out.println("Updating..."+id);

			OrderedJSONObject extObj = null;
			if(ext != null && ext.length() > 0) {
				extObj = new OrderedJSONObject(ext);
			} else {
				extObj = new OrderedJSONObject();
			}

			PreparedStatement pst1 = conn.prepareStatement(capabilitysql);
			pst1.setString(1, compId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String category = rs1.getString("business_capability_category");
				if(category.equals("0") || category.equals("1")) {
					category = "BUSINESS CAPABILITY";
				} else if(category.equals(2)) {
					category = "IT CAPABILITY";
				} else if(category.equals("3")) {
					category = "CYBERSECURITY";
				} else if(category.equals("4")) {
					category = "COMPLIANCE";
				}
				extObj.put("component_type", category);
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();

			pst1 = conn.prepareStatement(assetsql);
			pst1.setString(1, compId);
			rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String category = rs1.getString("asset_category");
				extObj.put("component_type", category);
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();

			pst1 = conn.prepareStatement(plansql);
			pst1.setString(1, compId);
			rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String category = rs1.getString("category");
				extObj.put("component_type", "PLAN");
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();

			pst1 = conn.prepareStatement(subplansql);
			pst1.setString(1, compId);
			rs1 = pst1.executeQuery();
			while(rs1.next()) { 
				String category = rs1.getString("category");
				extObj.put("component_type", "SUBPLAN");
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();
		}
		rs.close();
		pst.close();

		pst2.executeBatch();
		pst2.close();
		conn.commit();
		System.out.println("Successfully updated comment!!!");
	}

	private void updateIPAddress() throws Exception {
		String sql = "select id, extension from enterprise_asset where asset_category = 'IT ASSET'";
		String updatesql = "update enterprise_asset set extension = ? where id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String ext = rs.getString("extension");

			JSONArray ipArr = new JSONArray();
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			if(extObj.has("IP Address") && extObj.get("IP Address") instanceof String) {
				String ipAddress = (String)extObj.get("IP Address");
				if(ipAddress != null && ipAddress.length() > 0) {
					String[] ipList = ipAddress.trim().split(",");
					for(int j=0; j<ipList.length;j++) {
						String address = ipList[j];
						ipArr.add(address.trim());
					}
				}
				extObj.put("IP Address", ipArr);
			} else if(extObj.has("custom")) {
				OrderedJSONObject customObj = null;
				System.out.println(id+":"+extObj.get("custom"));
				customObj = (OrderedJSONObject)extObj.get("custom");
				if(customObj.has("IP Address")) {
					String ipAddress = (String)customObj.get("IP Address");
					ipArr.add(ipAddress);
				}
				extObj.put("IP Address", ipArr);
			}
			
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();

		conn.commit();
	}

	private void updateIPAddressFromSheet(String filename) throws Exception {
		String sql = "select id, extension from enterprise_asset where asset_name = ?";
		String updatesql = "update enterprise_asset set asset_name=?, extension = ? where id = ?";
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0;
		String[] row;
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			System.out.println(rowCount);
			String assetName=null, ipAddress=null;
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						assetName = row[i].trim();
					} else if(i==47) {
						ipAddress = row[i].trim();
					} 
				}
				System.out.println(assetName+":"+ipAddress);
				PreparedStatement pst = conn.prepareStatement(sql);
				pst.setString(1, assetName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					String id = rs.getString("id");
					String ext = rs.getString("extension");

					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					JSONArray ipArr = null;
					if(extObj.has("IP Address")) {
						ipArr = (JSONArray)extObj.get("IP Address");
						if(ipAddress != null && ipAddress.length() > 0) {
							if(!ipArr.contains(ipAddress)) {
								ipArr.add(ipAddress);
							}
						}
					} else {
						ipArr = new JSONArray();
						if(ipAddress != null && ipAddress.length() > 0) {
							ipArr.add(ipAddress);
						}
					}
					extObj.put("IP Address", ipArr);

					pst1.setString(1, assetName);
					pst1.setString(2, extObj.toString());
					pst1.setString(3, id);
					pst1.addBatch();
					System.out.println("Updated!!!");
				}
				rs.close();
				pst.close();
			}
		}

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateMissedIPAddressLinkToVulnr() throws Exception {
		String sql = "select asset_id, json_extract(extension, '$.\"IP Address\"') ip from enterprise_rel a, enterprise_asset b where a.asset_id=b.id and asset_rel_type = 'ASSET' and json_extract(asset_rel_context, '$.\"COMPONENT TYPE\"') = 'THREAT ASSET' and b.asset_category='IT ASSET'";
		String ipsql = "select id, asset_name from enterprise_asset where instr(json_extract(extension, '$.\"IP Address\"'), ?) > 0 and id != ?";

		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String assetId = rs.getString("asset_id");
			String ipList = rs.getString("ip");
			if(ipList != null && ipList.length() > 0) {
				JSONArray arr = new JSONArray(ipList);
				if(arr.size() > 0) {
					Iterator iter = arr.iterator();
					while(iter.hasNext()) {
						String ip = (String)iter.next();
						PreparedStatement pst1 = conn.prepareStatement(ipsql);
						pst1.setString(1, ip);
						pst1.setString(2, assetId);
						ResultSet rs1 = pst1.executeQuery();
						while(rs1.next()) {
							String id = rs1.getString("id");
							String name = rs1.getString("asset_name");
							System.out.println(name+":"+ip);
						}
						rs1.close();
						pst1.close();

					}
				}
			}
		}
		rs.close();
		pst.close();

	}

	private void updatePortFromSheet(String filename) throws Exception {
		String sql = "select id, extension from enterprise_asset where asset_name = ?";
		String updatesql = "update enterprise_asset set asset_name=?, extension = ? where id = ?";
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0, hostCount=0, ipCount=0, discoveryCount=0, notFoundCount=0;
		String[] row;
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			String hostName=null, ipAddress=null, vulnrName=null, port=null, service=null;
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==2) {			
						hostName = row[i].trim();
					} else if(i==8) {			
						ipAddress = row[i].trim();
					} else if(i==9) {
						vulnrName = row[i].trim();
					} else if(i==28) {
						port = row[i].trim();
					} else if(i==29) {
						service = row[i].trim();
					} 
				}
				System.out.println(hostName+":"+ipAddress+":"+vulnrName+":"+port+":"+service);

				// Get Vulnerability
				String vulnrId = null;
				String vulnrsql = "select _id from enterprise_vulnerability where nameId = ?";
				PreparedStatement pst = conn.prepareStatement(vulnrsql);
				pst.setString(1, vulnrName.trim());
				ResultSet rs = pst.executeQuery();
				while (rs.next()) {
					vulnrId = rs.getString("_id");
				}
				rs.close();
				pst.close();
				
				// Check Hostname
				String assetId = null, ext=null;
				JSONArray assetList = new JSONArray();
				boolean hostnameFound = false, ipaddressFound = false, discoveryFound = false;
				String itassetsql = "select id, extension from enterprise_asset where asset_name = ?";
				pst = conn.prepareStatement(itassetsql);
				pst.setString(1, hostName.trim());
				rs = pst.executeQuery();
				while (rs.next()) {
					assetId = rs.getString("id");
					ext = rs.getString("extension");
					hostnameFound = true;
					hostCount++;
					assetList.add(assetId);
					System.out.println("Hostname Match!!!");
				}
				rs.close();
				pst.close();

				// Check IP Address
				if(assetId == null) {
					itassetsql = "select id, extension from enterprise_asset where (json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"','$') > 0 or json_extract(extension, '$.\"Service Name\"') = ? or json_extract(extension, '$.\"Load Balancer\"') = ? or json_contains(json_extract(extension, '$.\"Other Names\"'), '\"REPLACE_STRING\"','$') > 0)";
					itassetsql = itassetsql.replaceAll("REPLACE_STRING", ipAddress.trim());
					pst = conn.prepareStatement(itassetsql);
					pst.setString(1, ipAddress.trim());
					pst.setString(2, ipAddress.trim());
					rs = pst.executeQuery();
					while (rs.next()) {
						assetId = rs.getString("id");
						ext = rs.getString("extension");
						ipaddressFound = true;
						ipCount++;
						if(!assetList.contains(assetId)) {
							assetList.add(assetId);
						}
						System.out.println("IP Address Match!!!");
					}
					rs.close();
					pst.close();
				}

				if(assetId == null) {
					itassetsql = "select id, extension from enterprise_vulnerability_asset where asset_name = ?";
					pst = conn.prepareStatement(itassetsql);
					pst.setString(1, hostName.trim());
					rs = pst.executeQuery();
					while (rs.next()) {
						assetId = rs.getString("id");
						ext = rs.getString("extension");
						discoveryFound = true;
						discoveryCount++;
						assetList.add(assetId);
						System.out.println("Discovery Match!!!");
					}
					rs.close();
					pst.close();
				}

				Iterator iter = assetList.iterator();
				while(iter.hasNext()) {
					assetId = (String)iter.next();
					int relId = 0;
					String ctx = null;
					String relchecksql = "select id, asset_rel_context from enterprise_rel where component_id=? and asset_id = ?";
					pst = conn.prepareStatement(relchecksql);
					pst.setString(1, vulnrId);
					pst.setString(2, assetId);
					rs = pst.executeQuery();
					while (rs.next()) {
						relId = rs.getInt("id");
						ctx = rs.getString("asset_rel_context");
					}
					rs.close();
					pst.close();

					if(relId > 0) {
						String updaterelsql = "update enterprise_rel set asset_rel_context = ? where component_id = ? and asset_id = ?";
						OrderedJSONObject ctxObj = new OrderedJSONObject(ctx);
						JSONArray dateList = null, portList=null, serviceList=null;
						OrderedJSONObject threatCtx = (OrderedJSONObject)ctxObj.get("THREAT");
						threatCtx.put("IP Address", ipAddress);
		
						if(threatCtx.has("Port")) {
							portList = (JSONArray)threatCtx.get("Port");
							if(!portList.contains(port)) {
								portList.add(port);
							}
						} else {
							portList = new JSONArray();
							portList.add(port);
						}
						threatCtx.put("Port", portList);
		
						if(threatCtx.has("Service")) {
							serviceList = (JSONArray)threatCtx.get("Service");
							if(!serviceList.contains(service)) {
								serviceList.add(service);
							}
						} else {
							serviceList = new JSONArray();
							serviceList.add(service);
						}
						threatCtx.put("Service", serviceList);
		
						ctxObj.put("THREAT", threatCtx);

						pst = conn.prepareStatement(updaterelsql);
						pst.setString(1, ctxObj.toString());
						pst.setString(2, vulnrId);
						pst.setString(3, assetId);
						pst.executeUpdate();
						pst.close();

						// Get Reverse Context
						ctx=null;
						threatCtx=null;
						pst = conn.prepareStatement(relchecksql);
						pst.setString(1, assetId);
						pst.setString(2, vulnrId);
						rs = pst.executeQuery();
						while (rs.next()) {
							ctx = rs.getString("asset_rel_context");
						}
						rs.close();
						pst.close();
						
						ctxObj = new OrderedJSONObject(ctx);
						threatCtx = (OrderedJSONObject)ctxObj.get("THREAT");
						threatCtx.put("IP Address", ipAddress);
						if(threatCtx.has("Port")) {
							portList = (JSONArray)threatCtx.get("Port");
							if(!portList.contains(port)) {
								portList.add(port);
							}
						} else {
							portList = new JSONArray();
							portList.add(port);
						}
						threatCtx.put("Port", portList);

						if(threatCtx.has("Service")) {
							serviceList = (JSONArray)threatCtx.get("Service");
							if(!serviceList.contains(service)) {
								serviceList.add(service);
							}
						} else {
							serviceList = new JSONArray();
							serviceList.add(service);
						}
						threatCtx.put("Service", serviceList);

						ctxObj.put("THREAT", threatCtx);

						pst = conn.prepareStatement(updaterelsql);
						pst.setString(1, ctxObj.toString());
						pst.setString(2, assetId);
						pst.setString(3, vulnrId);
						pst.executeUpdate();
						pst.close();
					}
				}

				if(assetList.size() == 0) {
					System.out.println("Asset not found["+hostName+":"+ipAddress+"]!!!");
					notFoundCount++;
				}
			} 
		}
		System.out.println("Total Host:"+hostCount+" Total IP Count:"+ipCount+" Total Discovered Count:"+discoveryCount+" Not Found:"+notFoundCount);
		conn.commit();
	}

	private void updateGroupName() throws Exception {
		String sql = "select a._id id, b.name name from enterprise_vulnerability a, enterprise_vulnerability_group b, enterprise_rel c where a._id = c.component_id and c.asset_id = b._id group by a._id";
		String assetsql = "select asset_id, asset_rel_context from enterprise_rel where component_id = ? and asset_rel_type = 'ASSET'";
		String updatesql = "update enterprise_rel set asset_rel_context = json_set(asset_rel_context, '$.THREAT.\"group_name\"', ?) where component_id = ? and asset_id = ?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String name = rs.getString("name");
			System.out.println(name+":"+id);
			PreparedStatement pst1 = conn.prepareStatement(assetsql);
			pst1.setString(1, id);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String assetId = rs1.getString("asset_id");
				String ctx = rs1.getString("asset_rel_context");
				
				PreparedStatement pst2 = conn.prepareStatement(updatesql);
				pst2.setString(1, name);
				pst2.setString(2, id);
				pst2.setString(3, assetId);
				pst2.addBatch();
				pst2.setString(1, name);
				pst2.setString(2, assetId);
				pst2.setString(3, id);
				pst2.addBatch();
				pst2.executeBatch();
				pst2.close();
			}
			rs1.close();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateAssetManagerColumn() throws Exception {
		String sql = "select id, extension from enterprise_asset";
		String updatesql = "update enterprise_asset set extension=? where id = ?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			String mgr = null;
			if(extObj.has("Manager")) {
				OrderedJSONObject mgrObj = (OrderedJSONObject)extObj.get("Manager");
				if(mgrObj.has("asset_name")) {
					mgr = (String)mgrObj.get("asset_name");
				}
			}
			if(mgr == null) {
				mgr = "";
			}
			System.out.println(mgr);
			extObj.put("Manager", mgr);

			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateCollaborationRiskDetails() throws Exception {
		String risksql = "select id, internal_id, assetClass, description, vulnerability, raw_risk,current_risk, estimated_risk, control_effectiveness, new_control_effectiveness, cia, selected_control from enterprise_risk_register_old";
		String collsql = "select id, component_id, extension from enterprise_collaboration where component_type = 'CAPABILITY' and json_extract(extension, '$.\"risk_register\"') = ?";
		String updatesql = "update enterprise_collaboration set extension = ? where id = ?";
		String insertsql = "insert into enterprise_threat_class(_id,name,type,behavior,assetClass,extension,lifecycle,owner,prevention,threats,recommendation,csf,createdAt,updatedAt,createdBy,updatedBy) values(?,?,'',?,?,?,'ACTIVE','','',json_array(),'',json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
		String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		PreparedStatement pst1 = conn.prepareStatement(insertsql);
		PreparedStatement pst2 = conn.prepareStatement(insertrelsql);
		PreparedStatement pst = conn.prepareStatement(risksql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String internalId = rs.getString("internal_id");
			String ac = rs.getString("assetClass");
			String desc = rs.getString("description");
			String vulnr = rs.getString("vulnerability");
			String rr = rs.getString("raw_risk");
			String cr = rs.getString("current_risk");
			String er = rs.getString("estimated_risk");
			String cia = rs.getString("cia");
			String controls = rs.getString("selected_control");

			String h = "0", p = "0", r = "0";
			OrderedJSONObject rrObj = null;
			if(rr != null && rr.length() > 0) {
				rrObj = new OrderedJSONObject(rr);
				
				if(rrObj.has("harm")) {
					h = (String)rrObj.get("harm");
				}
				if(rrObj.has("probablity")) {
					p = (String)rrObj.get("probablity");
				}
				if(rrObj.has("risk")) {
					r = (String)rrObj.get("risk");
				}
			} else {
				rrObj = new OrderedJSONObject();
			}
			rrObj.put("harm", Integer.parseInt(h));
			rrObj.put("probablity", Integer.parseInt(h));
			rrObj.put("risk", Integer.parseInt(r));
			JSONArray acList = null;
			if(ac != null && ac.length() > 0) {
				acList = new JSONArray(ac);
			} else {
				acList = new JSONArray();
			}
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("internal_id", internalId);
			extObj.put("raw_risk", rrObj);
			extObj.put("assessment", "Y");
			pst1.setString(1, id);
			pst1.setString(2, desc);
			pst1.setString(3, vulnr);
			pst1.setString(4, acList.toString());
			pst1.setString(5, extObj.toString());
			pst1.addBatch();
			System.out.println(internalId+":"+id);

			// Get Collaboration 
			PreparedStatement pst3 = conn.prepareStatement(collsql);
			pst3.setString(1, internalId);
			ResultSet rs3 = pst3.executeQuery();
			while(rs3.next()) {
				String riskId = rs3.getString("id");
				String controlId = rs3.getString("component_id");
				System.out.println("Control Id:"+controlId);

				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("LINKED", "Y");
				ctx.put("COMPONENT TYPE", "CAPABILITY");
				pst2.setString(1, id);
				pst2.setString(2, controlId);
				pst2.setString(3, "THREAT CLASS");
				pst2.setString(4, ctx.toString());
				pst2.setString(5, "support@smarterd.com");
				pst2.setString(6, "support@smarterd.com");
				pst2.setString(7, companyCode);
				pst2.addBatch();
				ctx.put("COMPONENT TYPE", "THREAT CLASS");
				pst2.setString(1, controlId);
				pst2.setString(2, id);
				pst2.setString(3, "CAPABILITY");
				pst2.setString(4, ctx.toString());
				pst2.setString(5, "support@smarterd.com");
				pst2.setString(6, "support@smarterd.com");
				pst2.setString(7, companyCode);
				pst2.addBatch();

				/*
				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				if(cr != null && cr.length() > 0) {
					OrderedJSONObject crObj = new OrderedJSONObject(cr);
					extObj.put("current_risk", crObj);
				}
				if(er != null && er.length() > 0) {
					OrderedJSONObject erObj = new OrderedJSONObject(er);
					extObj.put("estimated_risk", erObj);
				}
				if(cia != null && cia.length() > 0) {
					OrderedJSONObject ciaObj = new OrderedJSONObject(cia);
					extObj.put("cia", ciaObj);
				}
				if(controls != null && controls.length() > 0) {
					JSONArray controlsObj = new JSONArray(controls);
					extObj.put("selected_component", controlsObj);
				}
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
				*/
			}
			rs3.close();
			pst3.close();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		pst2.executeBatch();
		pst2.close();
		conn.commit();
	}

	private void rollupThreatRawRisk() throws Exception {
		String tcsql = "select _id, extension from enterprise_threat_class";
		String tsql = "select _id, extension from enterprise_threat a, enterprise_rel b where a._id=b.asset_id and b.component_id=? and b.asset_rel_type='THREAT'";
		String tcupdatesql = "update enterprise_threat_class set extension = ? where _id=?";
		PreparedStatement pst2 = conn.prepareStatement(tcupdatesql);
		PreparedStatement pst = conn.prepareStatement(tcsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String tcId = rs.getString("_id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			OrderedJSONObject tcrrObj = (OrderedJSONObject)extObj.get("raw_risk");

			int totalCount=0, totalHarm=0, totalProbability=0, totalRisk=0;
			PreparedStatement pst1 = conn.prepareStatement(tsql);
			pst1.setString(1, tcId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String tId = rs1.getString("_id");
				String text = rs1.getString("extension");
				OrderedJSONObject textObj = new OrderedJSONObject(text);
				OrderedJSONObject trrObj = (OrderedJSONObject)textObj.get("raw_risk");
				int h = ((Integer)trrObj.get("harm")).intValue();
				int p = ((Integer)trrObj.get("probability")).intValue();
				int r = ((Integer)trrObj.get("risk")).intValue();
				totalHarm += h;
				totalProbability += p;
				totalRisk += r;
				totalCount++;
			}	
			rs1.close();
			pst1.close();
			System.out.println(totalCount+":"+totalHarm+":"+totalProbability);
			int harm = (int) Math.ceil(totalHarm / totalCount);
			int probability = (int) Math.ceil(totalProbability / totalCount);
			int risk = harm * probability;
			tcrrObj.put("harm", new Integer(harm));
			tcrrObj.put("probability", new Integer(probability));
			tcrrObj.put("risk", new Integer(risk));
			extObj.put("raw_risk", tcrrObj);
			pst2.setString(1, extObj.toString());
			pst2.setString(2, tcId);
			pst2.addBatch();
		}
		rs.close();
		pst.close();

		pst2.executeBatch();
		pst2.close();
		conn.commit();
	}

	private void updateVulnerabilityLogRemediationStatus() throws Exception {
		String sql = "select component_id, nameId, asset_id, asset_name json_unquote(json_extract(asset_rel_context, '$.THREAT.\"status\"')) status from enterprise_rel a, enterprise_vulnerability b, enterprise_asset c where a.component_id = b._id and a.asset_id = c.id and asset_rel_type = 'ASSET' and json_extract(asset_rel_context, '$.\"COMPONENT TYPE\"') = 'THREAT ASSET'";
		String updatesql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.\"remediation_status\"', ?) where vulnerability_id = ? and asset_id = ?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String vulnrId = rs.getString("component_id");
			String assetId = rs.getString("asset_id");
			String status = rs.getString("status");
			System.out.println(vulnrId+":"+assetId+":"+status);

			if(status == null) {
				status = "";
			}
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, status);
			pst1.setString(2, vulnrId);
			pst1.setString(3, assetId);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();

		conn.commit();
	}

	private void updateVulnerabilityRemediationStatus() throws Exception {
		String sql = "select id from enterprise_rel where json_extract(asset_rel_context, '$.THREAT.\"scan_id\"') = ?";
		String updaterelsql = "update enterprise_rel set asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"status\"', ?) where json_extract(asset_rel_context, '$.THREAT.\"scan_id\"') = ?";
		String updatesql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.\"remediation_status\"', ?) where json_extract(extension, '$.\"scan_id\"') = ?";
		
		CSVReader reader = new CSVReader(new FileReader(new File("./data/Terminal Services Encryption Level is not FIPS-140 Compliant.csv")), ',', '"', 0);
		System.out.println("Successfully read data file");
		int rowCount = 0;
		String[] row;
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst2 = conn.prepareStatement(updaterelsql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			System.out.println(rowCount);
			String scanId=null, status=null;
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						scanId = row[i].trim();
					} else if(i==1) {
						status = row[i].trim();
					} 
				}
				System.out.println(scanId+":"+status);
				pst1.setString(1, status);
				pst1.setString(2, scanId);
				pst1.addBatch();
				pst2.setString(1, status);
				pst2.setString(2, scanId);
				pst2.addBatch();
			}
		}

		pst1.executeBatch();
		pst1.close();
		pst2.executeBatch();
		pst2.close();
		conn.commit();
	}

	private void updateCVE() throws Exception {
		String sql = "select _id, extension from enterprise_vulnerability";
		String updatesql = "update enterprise_vulnerability set extension=? where _id=?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);	
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			JSONArray cveList = new JSONArray();
			if(extObj.has("CVEs Associated")) {
				String cve = (String)extObj.get("CVEs Associated");
				String[] arr = cve.split(",");
				for(int i=0; i<arr.length; i++) {
					cveList.add(arr[i].trim());
				}
			}
			extObj.put("CVEs Associated", cveList);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateEnterpriseAssessmentDetail() throws Exception {
		String sql = "select id, control_id, extension from enterprise_assessment_detail";
		String controlsql = "select parent_capability_name, child_capability_name,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity, business_capability_comment from enterprise_business_capability where id = ?";
		String updatesql = "update enterprise_assessment_detail set extension=? where id = ?";
		PreparedStatement pst2 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String controlId = rs.getString("control_id");
			String ext = rs.getString("extension");

			PreparedStatement pst1 = conn.prepareStatement(controlsql);
			pst1.setString(1, controlId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String parentName = rs1.getString("parent_capability_name");
				String childName = rs1.getString("child_capability_name");
				String nistMapping = rs1.getString("nist_mapping");
				String artifacts = rs1.getString("artifacts");
				String notes = rs1.getString("business_capability_comment");
				String cm = rs1.getString("current_maturity");

				if(nistMapping == null) {
					nistMapping = "";
				}

				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				extObj.put("nist_mapping", nistMapping);
				extObj.put("current_maturity", cm);
				extObj.put("parent_control_name", parentName);
				extObj.put("control_name", childName);

				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();
		}
		rs.close();
		pst.close();

		pst2.executeBatch();
		pst2.close();
		conn.commit();
	}

	private void updateCMMCControlId() throws Exception {
		String updatesql = "update enterprise_business_capability set internal_id=? where internal_id = ?";
		String updateassessmentdetailsql = "update enterprise_assessment_detail set extension=json_set(extension, '$.\"internal_id\"',?) where json_extract(extension,'$.\"internal_id\"') = ?";
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0;
		String[] row;
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst2 = conn.prepareStatement(updateassessmentdetailsql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			System.out.println(rowCount);
			String v1Id=null, v2Id=null;
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						v1Id = row[i].trim();
					} else if(i==1) {
						v2Id = row[i].trim();
					} 
				}
				System.out.println(v1Id+":"+v2Id);
				pst1.setString(1, v2Id);
				pst1.setString(2, v1Id);
				pst1.addBatch();
				pst2.setString(1, v2Id);
				pst2.setString(2, v1Id);
				pst2.addBatch();
			}
		}

		pst1.executeBatch();
		pst1.close();
		pst2.executeBatch();
		pst2.close();
		conn.commit();
	}

	private void syncVulnerabilityLog() throws Exception {
		String sql = "select component_id, nameId, asset_id, asset_name, json_unquote(json_extract(asset_rel_context, '$.THREAT.\"status\"')) status,json_unquote(json_extract(asset_rel_context, '$.THREAT.\"Port\"')) port, json_unquote(json_extract(b.extension, '$.\"Last Found On\"')) last_seen, json_unquote(json_extract(b.extension, '$.\"Vulnerability Type\"')) vulnr_type, priority, source from enterprise_rel a, enterprise_vulnerability b, enterprise_asset c where a.component_id = b._id and a.asset_id = c.id and asset_rel_type = 'ASSET' and json_extract(asset_rel_context, '$.\"COMPONENT TYPE\"') = 'THREAT ASSET'";
		String checksql = "select count(*) count from enterprise_vulnerability_log where vulnerability_id like ? and asset_id like ?";
		String updatesql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.\"remediation_status\"', ?) where vulnerability_id = ? and asset_id = ?";
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String vulnrName = rs.getString("nameId");
			String vulnrId = rs.getString("component_id");
			String assetId = rs.getString("asset_id");
			String assetName = rs.getString("asset_name");
			String status = rs.getString("status");
			String lastSeen = rs.getString("last_seen");
			String port = rs.getString("port");
			String vulnrType = rs.getString("vulnr_type");
			String priority = rs.getString("priority");
			String source = rs.getString("source");

			lastSeen = formatDate(lastSeen);
			LocalDate scandt = LocalDate.parse(lastSeen, df);
			String month = scandt.getMonth().name().substring(0,3);
			String year = Integer.toString(scandt.getYear()).substring(2);
			String scanPeriod = month+"-"+year;
			String dt = scandt.format(df);

			JSONArray vulnrtypeList = null;
			if(vulnrType != null && vulnrType.length() > 0) {
				vulnrtypeList = new JSONArray(vulnrType);
			} else {
				vulnrtypeList = new JSONArray();
			}

			JSONArray portList = null;
			if(port != null && port.length() > 0) {
				portList = new JSONArray(port);
			} else {
				portList = new JSONArray();
			}

			if(status == null) {
				status = "";
			}
			int count = 0;
			PreparedStatement pst1 = conn.prepareStatement(checksql);
			pst1.setString(1, vulnrId);
			pst1.setString(2, assetId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				count = rs1.getInt("count");
			}
			rs1.close();
			pst1.close();

			if(count == 0) {
				pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, vulnrId);
				pst1.setString(2, "%");
				rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				OrderedJSONObject extObj = new OrderedJSONObject();
				String assetStatus = "New";
				extObj.put("asset_status", assetStatus);
				extObj.put("remediation_status", status);
				extObj.put("Port", portList);
				extObj.put("Vulnerability Type", vulnrtypeList);

				if(count == 0) {
					System.out.println("Vulnr and Asset Not Found:"+vulnrName+"-"+assetName+"-"+priority);
					extObj.put("scan_period", scanPeriod);
				} else if(count == 1) {
					System.out.println("Vulnr Found and Asset Not Found:"+assetName+"-"+priority);
					extObj.put("scan_period", scanPeriod);
				} else if(count > 1) {
					System.out.println("Multiple Vulnr Found and Asset Not Found:"+vulnrId+":"+assetName+"-"+priority);
					dt = "11/1/21";
					extObj.put("scan_period", "OCT-21");
				}

				String insertsql = "insert into enterprise_vulnerability_log values(uuid(),?,?,?,?,?,?,?,?,?,?,?,current_timestamp)";	
				pst = conn.prepareStatement(insertsql);
				pst.setString(1, vulnrId);
				pst.setString(2, vulnrName);
				pst.setString(3, assetId);
				pst.setString(4, assetName);
				pst.setString(5, "");                     
				pst.setString(6, "");
				pst.setString(7, priority);
				pst.setString(8, assetStatus);
				pst.setString(9, dt);
				pst.setString(10, source);
				pst.setString(11, extObj.toString());
				pst.executeUpdate();
				pst.close();
			}
		}
		rs.close();
		pst.close();

		conn.commit();
	}

	private void updateITAssetCriticality() throws Exception {
		String sql = "select a.id id, json_unquote(json_extract(b.extension, '$.\"Criticality\"')) app_criticality from enterprise_asset a, enterprise_asset b, enterprise_rel c where c.component_id = a.id and c.asset_id = b.id and a.asset_category = 'IT ASSET' and b.asset_category = 'APPLICATION'";
		String updatesql = "update enterprise_asset set extension = json_set(extension, '$.\"Criticality\"', ?) where id = ?";
		OrderedJSONObject assetList = new OrderedJSONObject();
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String criticality = rs.getString("app_criticality");
			if(criticality != null && criticality.trim().length() > 0) {
				if(assetList.has(id)) {
					String c = (String)assetList.get(id);
					if(criticality.equalsIgnoreCase("Critical")) {
						assetList.put(id, criticality);
					} else if(!c.equalsIgnoreCase("Critical") && criticality.equalsIgnoreCase("High")) {
						assetList.put(id, criticality); 
					} else if(!c.equalsIgnoreCase("High") && criticality.equalsIgnoreCase("Medium")) {
						assetList.put(id, criticality); 
					} else if(!c.equalsIgnoreCase("Medium") && criticality.equalsIgnoreCase("Low")) {
						assetList.put(id, criticality); 
					}
				} else {
					assetList.put(id, criticality); 
				}
			}
		}
		rs.close();
		pst.close();

		Iterator iter = assetList.keySet().iterator();
		while(iter.hasNext()) {
			String id = (String)iter.next();
			String criticality = (String)assetList.get(id);
			pst1.setString(1, criticality);
			pst1.setString(2, id);
			pst1.addBatch();
		}

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void mysqlConnect(String dbname) throws Exception {
    	//String userName = "admin", password = "smarterD2018!";
		String userName = "peapi", password = "smartEuser2022!";
		//String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private String formatDate(String d) throws Exception {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");   // 10
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/d/yyyy");   // 9
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("M/dd/yyyy");   // 9
        DateTimeFormatter df3 = DateTimeFormatter.ofPattern("MM/dd/yy");    // 8
        DateTimeFormatter df4 = DateTimeFormatter.ofPattern("M/d/yyyy");    // 8
        DateTimeFormatter df5 = DateTimeFormatter.ofPattern("MM/d/yy");     // 7
        DateTimeFormatter df6 = DateTimeFormatter.ofPattern("M/dd/yy");     // 7
        DateTimeFormatter df7 = DateTimeFormatter.ofPattern("M/d/yy");      // 6
        DateTimeFormatter df8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.000'Z'");     // ISO

        LocalDate dt = null;
        String formattedDate = "";
        if(d != null && d.length() > 0) {
            if(d.length() == 6 && d.indexOf("/") != -1) {
                dt = LocalDate.parse(d, df7);
                formattedDate = df.format(dt);
			} else if (d.length() == 7 && d.indexOf("/") != -1) {    //1/01/07
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df6);
                } else {
                    dt = LocalDate.parse(d, df5);
                }
                formattedDate = df.format(dt);
            } else if (d.length() == 8 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df4);
                } else {
                    dt = LocalDate.parse(d, df3);
                }
                formattedDate = df.format(dt);
			} else if (d.length() == 9 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df2);
                } else {
                    dt = LocalDate.parse(d, df1);
                }
                formattedDate = df.format(dt);
			} else if(d.indexOf("T") != -1) {
                dt = LocalDate.parse(d, df8);
                formattedDate = df.format(dt);
			} else {
                formattedDate = d;
            }
        }

        return formattedDate;
    }

	private void printExtAttributes(String type) throws Exception {
		String sql = null;
		
		if(type.equals("APPLICATION")) {
			sql = "select extension from enterprise_asset where asset_name = 'Oracle Xstore/Xadmin'";
		}
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			Iterator iter = extObj.keySet().iterator();
			while(iter.hasNext()) {
				String key = (String)iter.next();
				String value = extObj.get(key).toString();
				System.out.println(key+":"+value);
			}
		}
		rs.close();
		pst.close();
	}

	private void createAssignmentGroup() throws Exception {
		String sql = "select distinct json_unquote(json_extract(extension, '$.\"Assignment Group\"')) grp from enterprise_incident";
		String insertsql = "insert into enterprise_setting values(null,?,'','Incident Group','','SECURITY',?,'support@smarterd.com',current_timestamp,'','',json_object('definition','','storage_value','','value_order','','visible_check',false,'customer_managed',false,'smarterd_admin',false))";
		PreparedStatement pst1 = conn.prepareStatement(insertsql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String group = rs.getString("grp");
			pst1.setString(1, group);
			pst1.setString(2, companyCode);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateAssignmentCategory() throws Exception {
		String sql = "select _id,json_unquote(json_extract(extension, '$.\"Assignment Group\"')) grp from enterprise_incident";
		String categorysql = "select json_unquote(json_extract(extension, '$.\"storage_value\"')) storage_value from enterprise_setting where setting_category = 'Incident Group' and setting_name=?";
		String updatesql = "update enterprise_incident set extension=json_set(extension, '$.\"Assignment Category\"', ?) where _id=?";
		
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String group = rs.getString("grp");
			if(group != null && group.length() > 0) {
				PreparedStatement pst1 = conn.prepareStatement(categorysql);
				pst1.setString(1, group);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					String category = rs1.getString("storage_value");
					PreparedStatement pst2 = conn.prepareStatement(updatesql);
					pst2.setString(1, category);
					pst2.setString(2, id);
					pst2.executeUpdate();
					pst2.close();
				}
				rs1.close();
				pst1.close();
			}
		}
		rs.close();
		pst.close();

		conn.commit();
	}

	private void cleanupVulnerabilityLog() throws Exception {
		String sql = "select id, json_unquote(json_extract(extension, '$.THREAT.\"scan_id\"')) scan_id from enterprise_vulnerability_log";
		String deletesql = "delete from enterprise_vulnerability_log where id = ?";
		PreparedStatement pst1 = conn.prepareStatement(deletesql);
		JSONArray scanList = new JSONArray();
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String scanId = rs.getString("scan_id");

			if(scanList.contains(scanId)) {
				System.out.println("Scan Id already exists...deleting");
				pst1.setString(1, id);
				pst1.addBatch();
				count++;
			} else {
				scanList.add(scanId);
			}
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
		conn.close();
		System.out.println("Deleted:"+count);
	}

	private void updateSettingValueOrder() throws Exception {
		String sql = "select id, extension from enterprise_setting";
		String updatesql = "update enterprise_setting set extension = ? where id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = null;
			if(ext != null && ext.length() > 0) {
				extObj = new OrderedJSONObject(ext);
				String valueOrder = null;
				if(extObj.has("value_order")) {
					valueOrder = (String)extObj.get("value_order");
					if(valueOrder == null || valueOrder.length() == 0) {
						extObj.put("value_order", "0");
					}
				} else {
					extObj.put("value_order", "0");
				}
				System.out.println(valueOrder);
			} else {
				System.out.println("Ext is null");
				extObj = new OrderedJSONObject();
				extObj.put("value_order", "0");
			}

			pst1.setString(1, extObj.toString());
			pst1.setInt(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateCMMCV2ObjectiveId() throws Exception {
		String sql = "select b.id id, a.internal_id parent_internal_id, substring_index(b.internal_id,'.',-1) internal_id from enterprise_business_capability a, enterprise_business_capability b where a.id = b.parent_capability_id and a.business_capability_domain = 'CMMC_V2_L2' and b.business_capability_domain='CMMC_V2_L2' and a.business_capability_level = 2 and b.business_capability_level = 3";
		String updatesql = "update enterprise_business_capability set internal_id = ? where id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		OrderedJSONObject idMap = new OrderedJSONObject();
		idMap.put("1", "a");
		idMap.put("2", "b");
		idMap.put("3", "c");
		idMap.put("4", "d");
		idMap.put("5", "e");
		idMap.put("6", "f");
		idMap.put("7", "g");
		idMap.put("8", "h");
		idMap.put("9", "i");
		idMap.put("10", "j");
		idMap.put("11", "k");
		idMap.put("12", "l");
		idMap.put("13", "m");
		idMap.put("14", "n");
		idMap.put("15", "o");
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String parentInternalId = rs.getString("parent_internal_id");
			String internalId = rs.getString("internal_id");
			internalId = (String)idMap.get(internalId);
			internalId = parentInternalId+"."+internalId;

			pst1.setString(1, internalId);
			pst1.setString(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateAssessmentLevelCMMCV2ObjectiveId() throws Exception {
		String sql = "select id, internal_id from enterprise_business_capability where business_capability_domain = 'CMMC_V2_L2' and business_capability_level = 3";
		String updatesql = "update enterprise_assessment_detail set extension=json_set(extension, '$.\"internal_id\"',?) where control_id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String internalId = rs.getString("internal_id");

			pst1.setString(1, internalId);
			pst1.setString(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void loadMLResult() throws Exception {
		String sql = "select id from enterprise_ml_result where json_extract(extension, '$.raw_token') = ?";
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0, matchedCount = 0;
		String[] row;

		while ((row = reader.readNext()) != null) {
			rowCount++;
			String name="", desc="", token="", type="", ids="", category="", linkedcomponent="", location="", priority="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						name = row[i].trim();
					} else if(i==1) {
						token = row[i].trim();
					} else if(i==2) {
						type = row[i].trim();
					} else if(i==3) {
						ids = row[i].trim();
					} else if(i==4) {
						category = row[i].trim();
					} else if(i==5) {
						linkedcomponent = row[i].trim();
					} else if(i==6) {
						location = row[i].trim();
					} else if(i==7) {
						priority = row[i].trim();
					} 
				}
				System.out.println(token);
				JSONArray tokenList = null;
				if(token != null && token.length() > 0) {
					tokenList = new JSONArray(token);
				} else {
					tokenList = new JSONArray();
				}
				System.out.println(rowCount+":"+name);
				System.out.println(ids);
				JSONArray idList = new JSONArray(ids);
				if(idList.size() > 0) {
					/*
					String[] idArr = ids.trim().split(",");
					for(int j=0; j<idArr.length;j++) {
						String t = idArr[j];
						idList.add(t.trim());
					}
					*/
					JSONArray categoryList = new JSONArray(category);
					/*
					String[] categoryArr = category.trim().split(",");
					for(int j=0; j<categoryArr.length;j++) {
						String t = categoryArr[j];
						categoryList.add(t.trim());
					}
					*/
					JSONArray linkedcomponentList = new JSONArray(linkedcomponent);
					/*
					String[] linkedcomponentArr = linkedcomponent.trim().split(",");
					for(int j=0; j<linkedcomponentArr.length;j++) {
						String t = linkedcomponentArr[j];
						linkedcomponentList.add(t.trim());
					}
					*/
					JSONArray locationList = new JSONArray(location);
					/*
					String[] locationArr = location.trim().split(",");
					for(int j=0; j<locationArr.length;j++) {
						String t = locationArr[j];
						locationList.add(t.trim());
					}
					*/

					// Check if Problem exists
					int id = 0;
					PreparedStatement pst = conn.prepareStatement(sql);
					pst.setString(1, token);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						id = rs.getInt("id");
					}
					rs.close();
					pst.close();

					// Insert
					if(id == 0) {
						String insertsql = "insert into enterprise_ml_result(id,name,description,token_list,component_type,component_ids,component_category_list,linked_component_list,location_list,status,priority,extension,created_by,created_timestamp,updated_by,updated_timestamp) values(null,?,'',?,?,?,?,?,?,'Review',?,?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp)";
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("raw_token", token.trim());
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, name);
						pst.setString(2, tokenList.toString());
						pst.setString(3, type);
						pst.setString(4, idList.toString());
						pst.setString(5, categoryList.toString());
						pst.setString(6, linkedcomponentList.toString());
						pst.setString(7, locationList.toString());
						pst.setString(8, priority);
						pst.setString(9, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						System.out.println("Problem already exists!!!");
					}
				} else {
					System.out.println("No Incident Found for Problem. Skipping...");
				}
			}

			conn.commit();
		}
	}

	private void loadMLDictionary() throws Exception {

	}

	private void getVulnrSummaryByCriticalApplication() throws Exception {
		String sql = "select  from enterprise_asset where asset_category = 'APPLICATION' and json_unquote(json_extract(extension, '$.\"SLA Level\"')) like 'Gold%'";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");

		}
	}

	// Asset -> Vulnerability Asset Reconcillation
	// This is used to sync Vulnerability Asset and Log table for assets that were sync'ed without finding matching asset 
	// but, later the IP Address was attached to different asset
	private void resyncAssetWithVulnerability() throws Exception {
        System.out.println("In resyncAssetWithVulnerability...");
        String sql = "select asset_name, id, json_extract(extension, '$.\"IP Address\"') ip_address from enterprise_asset where asset_category = 'IT ASSET'";
        String vulnrassetsql = "select id, asset_name, extension from enterprise_vulnerability_asset where (asset_name = ? or json_contains(extension, '\"REPLACE_STRING\"', '$.\"IP Address\"') > 0)";
        String vulnrassetupdatesql = "update enterprise_vulnerability_asset set id = ?, asset_name=? where id = ?";
        String vulnrlogupdatesql = "update enterprise_vulnerability_log set asset_id = ?, asset_name = ? where asset_id = ?";
		String vulnrloghistoryupdatesql = "update enterprise_vulnerability_log_history set asset_id = ?, asset_name = ? where asset_id = ?";

		int rowCount = 0;
		// Get all IT Assets from enterprise_asset table
        PreparedStatement pst = conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
			String id = rs.getString("id");
			String assetName = rs.getString("asset_name");
            String ipAddress = rs.getString("ip_address");

			rowCount++;
            
			// Get IPs 
            JSONArray ipList = null;
            if(ipAddress != null && ipAddress.length() > 0) {
                ipList = new JSONArray(ipAddress);

				// Find matching Vulnr Asset in Vulnerability Asset Table
                Iterator iter = ipList.iterator();
                while(iter.hasNext()) {
                    String ip = (String)iter.next();
					String s = vulnrassetsql.replaceAll("REPLACE_STRING", ip);
                    PreparedStatement pst1 = conn.prepareStatement(s);
                    pst1.setString(1, ip);
                    ResultSet rs1 = pst1.executeQuery();
                    while(rs1.next()) {
                        String assetId = rs1.getString("id");
						String hostName = rs1.getString("asset_name");
                        String ext = rs1.getString("extension");

						// Get Id from Vulnr Log Table
						String logassetId = null, logassetName = "";
						String logsql = "select distinct asset_id, asset_name from enterprise_vulnerability_log where ipaddress = ?";
						PreparedStatement pst4 = conn.prepareStatement(logsql);
						pst4.setString(1, ip);
						ResultSet rs2 = pst4.executeQuery();
						while(rs2.next()) {
							logassetId = rs2.getString("asset_id");
							logassetName = rs2.getString("asset_name");
						}
						rs2.close();
						pst4.close();

						String history = "";
						if(logassetId == null) {
							logsql = "select distinct asset_id, asset_name from enterprise_vulnerability_log_history where ipaddress = ?";
							pst4 = conn.prepareStatement(logsql);
							pst4.setString(1, ip);
							rs2 = pst4.executeQuery();
							while(rs2.next()) {
								logassetId = rs2.getString("asset_id");
								logassetName = rs2.getString("asset_name");
								history = "History";
							}
							rs2.close();
							pst4.close();
						}
						if(logassetId == null) {
							logassetId = "";
							logassetName = "";
							history = "";
						}
                        
                        OrderedJSONObject extObj = new OrderedJSONObject(ext);
                        String sync = (String)extObj.get("sync");

						// Scanerio 1 - If Ids do not match, sync Vulnr Asset and Vulnr Log to Asset Id
                        if(!id.equals(assetId) && sync.equalsIgnoreCase("Y")) {
							// Used the generate output and copied to excel to analyze
							System.out.println(rowCount+":"+assetName+":"+ip+":"+hostName+":"+logassetName+":"+id+":"+assetId+":"+logassetId+":"+history);
                            
							//----------------------------------------
							// First generate the report and validate and later uncomment below lines to execute
							//----------------------------------------
							PreparedStatement pst2 = conn.prepareStatement(vulnrassetupdatesql);
							pst2.setString(1, id);
							pst2.setString(2, assetName);
                            pst2.setString(3, assetId);
							System.out.println(pst2.toString());
                            pst2.executeUpdate();
							pst2.close();

							if(logassetId != null && logassetId.length() > 0) {
								if(history.length() > 0) {
									PreparedStatement pst5 = conn.prepareStatement(vulnrloghistoryupdatesql);
									pst5.setString(1, id);
									pst5.setString(2, assetName);
									pst5.setString(3, logassetId);
									System.out.println(pst5.toString());
									pst5.executeUpdate();
									pst5.close();
								} else {
									PreparedStatement pst3 = conn.prepareStatement(vulnrlogupdatesql);
									pst3.setString(1, id);
									pst3.setString(2, assetName);
									pst3.setString(3, logassetId);
									System.out.println(pst3.toString());
									pst3.executeUpdate();
									pst3.close();
								}
							}
                        }
                    }
                    rs1.close();
                    pst1.close();
                }
            }
        }
        rs.close();
        pst.close();

        conn.commit();

        System.out.println("Successfully Resynced Vulnerability Asset with Asset DB!!!");
    }

	// Vulnerability Asset -> Asset Reconcillation
	// This is used to sync Vulnerability Asset and Log table for assets that were sync'ed to an asset 
	// but, later the IP Address was detached and not attached again
	private void resyncVulnerabilityWithAsset() throws Exception {
        System.out.println("In resyncVulnerabilityAsset...");
        String assetsql = "select id, asset_name, json_unquote(json_extract(extension, '$.\"IP Address\"')) ip_address from enterprise_asset where id = ? and (asset_name = ? or json_contains(extension, '\"REPLACE_STRING\"', '$.\"IP Address\"') > 0 or json_extract(extension, '$.\"Service Name\"') = ? or json_extract(extension, '$.\"Load Balancer\"') = ? or json_contains(extension, '\"REPLACE_STRING\"', '$.\"Other Names\"') > 0)";
		String assetipsql = "select id, asset_name, json_unquote(json_extract(extension, '$.\"IP Address\"')) ip_address from enterprise_asset where (asset_name = ? or json_contains(extension, '\"REPLACE_STRING\"', '$.\"IP Address\"') > 0 or json_extract(extension, '$.\"Service Name\"') = ? or json_extract(extension, '$.\"Load Balancer\"') = ? or json_contains(extension, '\"REPLACE_STRING\"', '$.\"Other Names\"') > 0)";
		String assetchecksql = "select id, asset_name, json_unquote(json_extract(extension, '$.\"IP Address\"')) ip_address from enterprise_asset where id = ?";
        String vulnrassetsql = "select id, asset_name, extension from enterprise_vulnerability_asset where json_extract(extension, '$.reviewed') != 'D' and asset_category = 'IT ASSET'";
       	String vulnrassetupdatesql = "update enterprise_vulnerability_asset set id = ?, asset_name=? where id = ?";
        String vulnrlogupdatesql = "update enterprise_vulnerability_log set asset_id = ?, asset_name = ? where asset_id = ?";
		String vulnrloghistoryupdatesql = "update enterprise_vulnerability_log_history set asset_id = ?, asset_name = ? where asset_id = ?";

        PreparedStatement pst2 = conn.prepareStatement(vulnrassetupdatesql);
        PreparedStatement pst3 = conn.prepareStatement(vulnrlogupdatesql);

		// Get all Assets from enterprise_vulnerability_asset table
        PreparedStatement pst = conn.prepareStatement(vulnrassetsql);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
			String id = rs.getString("id");
			String assetName = rs.getString("asset_name");
            String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			JSONArray ipList = null;
			if(extObj.has("IP Address")) {
				ipList = (JSONArray)extObj.get("IP Address");
			} else {
				ipList = new JSONArray();
			}
			String sync = "";
			if(extObj.has("sync")) {
				sync = (String)extObj.get("sync");
			}

			// System.out.println("IP Missing in Vulnerability Asset!!!");
			if(ipList.size() == 0) {
				//System.out.println(id+":"+assetName+":"+sync);
				String[] list = assetName.split("\\.");
				if(list.length == 4) {	// Update enterprise_vulnerability_asset Missing IP from assetName
					ipList.add(assetName);
					extObj.put("IP Address", ipList);
					String updateipsql = "update enterprise_vulnerability_asset set extension=? where id = ?";
					PreparedStatement pst1 = conn.prepareStatement(updateipsql);
					pst1.setString(1, extObj.toString());
					pst1.setString(2, id);
					pst1.executeUpdate();
				} else {	// Reconcile IP with Asset Table
					int count = 0;
					String assetnamesql = "select id, asset_name, json_extract(extension, '$.\"IP Address\"') ip_address from enterprise_asset where asset_name = ?";
					PreparedStatement pst1 = conn.prepareStatement(assetnamesql);
					pst1.setString(1, assetName);
					ResultSet rs1 = pst1.executeQuery();
					while(rs1.next()) {
						String aId = rs1.getString("id");
						String name = rs1.getString("asset_name");
						String ip = rs1.getString("ip_address");
						ipList = new JSONArray(ip);
					
						//System.out.println(id+":"+aId+":"+ip);
						if(aId.equals(id)) {
							if(ipList.size() >  0) {
								String ipAddress = (String)ipList.get(0);	// Get the first one
								JSONArray ipAddressList = new JSONArray();
								ipAddressList.add(ipAddress);
								extObj.put("IP Address", ipAddressList);
								String updateipsql = "update enterprise_vulnerability_asset set extension=? where id = ?";
								PreparedStatement pst4 = conn.prepareStatement(updateipsql);
								pst4.setString(1, extObj.toString());
								pst4.setString(2, id);
								pst4.executeUpdate();
							} else {
								System.out.println(assetName+":"+name+":"+id+":"+aId+":"+ip+":"+""+":Case 1 - IP Missing in Asset["+sync+"]!!!");
							}
						} else {
							System.out.println(assetName+":"+name+":"+id+":"+aId+":"+ipList.toString()+":"+""+":Case 2 - ID not Matching["+sync+"]!!!");
						}
						count++;
					}	
					rs1.close();
					pst1.close();	

					if(count == 0 && sync.equals("Y")) {
						System.out.println(assetName+":"+""+":"+id+":"+""+":"+ipList.toString()+":"+""+":Case 3 - Asset not Found["+sync+"]!!!");
					}
				}
			} 

			if(ipList.size() > 1) {
				System.out.println("More than one IP Configured!!!");
			} else if(ipList.size() == 1) {
				String ipAddress = ((String)ipList.get(0)).trim();
				// Get IPs 
				String assetId=null, name=null, ip=null;
				String s = assetsql.replaceAll("REPLACE_STRING", ipAddress);	// Check if Vulnr Asset Id and IP Address Combination Match in Asset
				PreparedStatement pst1 = conn.prepareStatement(s);
				pst1.setString(1, id);
				pst1.setString(2, ipAddress);
				pst1.setString(3, ipAddress);
				pst1.setString(4, ipAddress);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					assetId = rs1.getString("id");
					name = rs1.getString("asset_name");
					ip = rs1.getString("ip_address");
				}
				rs1.close();
				pst1.close();

				if(assetId == null) {
					pst1 = conn.prepareStatement(assetchecksql);	// Check if Vulnr Asset Id Match
					pst1.setString(1, id);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						assetId = rs1.getString("id");
						name = rs1.getString("asset_name");
						ip = rs1.getString("ip_address");
					}
					rs1.close();
					pst1.close();

					if(assetId == null) {	// Check if IP Match and depending on sync status, do reconcile
						String ipassetId=null, ipassetName=null;
						s = assetipsql.replaceAll("REPLACE_STRING", ipAddress);	// Check if Vulnr IP Address Match
						pst1 = conn.prepareStatement(s);
						pst1.setString(1, ipAddress);
						pst1.setString(2, ipAddress);
						pst1.setString(3, ipAddress);
						//System.out.println(pst1.toString());
						rs1 = pst1.executeQuery();
						while(rs1.next()) {
							ipassetId = rs1.getString("id");
							ipassetName = rs1.getString("asset_name");
							ip = rs1.getString("ip_address");
						}
						rs1.close();
						pst1.close();

						if(ipassetId == null) {
							//System.out.println(assetName+":"+name+":"+id+":"+assetId+":"+ipAddress+":"+ip+":Case 4 - Id & IP Address Not Matching["+sync+"]!!!");
							// Reset Vulnr Asset Sync Flag to 'N' and Update with New Id
							if(sync.equalsIgnoreCase("Y")) {
								System.out.println(assetName+":"+name+":"+id+":"+assetId+":"+ipAddress+":"+ip+":Case 4 - Id & IP Address Not Matching["+sync+"]!!!");
								System.out.println("Vulnr Asset Synced but Asset Not found!!!");
								String updatesql = "update enterprise_vulnerability_asset set extension=json_set(extension, '$.sync', 'N') where id = ?";
								pst1 = conn.prepareStatement(updatesql);
								pst1.setString(1, id);
								pst1.executeUpdate();
								pst1.close();
							}
						} else {
							if(sync.equalsIgnoreCase("Y")) {
								System.out.println(assetName+":"+ipassetName+":"+id+":"+ipassetId+":"+ipAddress+":"+ip+":Case 5 - Id Not matching - IP Address Match to different Asset");
							}
							/*
							// Update Vulnr Asset Id and Name from Asset
							pst1 = conn.prepareStatement(vulnrassetupdatesql);
							pst1.setString(1, ipassetId);
							pst1.setString(2, assetName);
							pst1.setString(3, id);
							//System.out.println(pst1.toString());
							pst1.executeUpdate();
							pst1.close();

							// Get Id from Vulnr Log Table
							String logassetId = null, logassetName = "";
							String logsql = "select distinct asset_id, asset_name from enterprise_vulnerability_log where ipaddress = ?";
							pst1 = conn.prepareStatement(logsql);
							pst1.setString(1, ipAddress);
							rs1 = pst1.executeQuery();
							while(rs1.next()) {
								logassetId = rs1.getString("asset_id");
								logassetName = rs1.getString("asset_name");
							}
							rs1.close();
							pst1.close();

							String history = "";
							if(logassetId == null) {
								logsql = "select distinct asset_id, asset_name from enterprise_vulnerability_log_history where ipaddress = ?";
								pst1 = conn.prepareStatement(logsql);
								pst1.setString(1, ipAddress);
								rs1 = pst1.executeQuery();
								while(rs1.next()) {
									logassetId = rs1.getString("asset_id");
									logassetName = rs1.getString("asset_name");
									history = "History";
								}
								rs1.close();
								pst1.close();
							}
							if(logassetId == null) {
								logassetId = "";
								logassetName = "";
								history = "";
							}

							if(logassetId != null && logassetId.length() > 0) {
								if(history.length() > 0) {
									pst1 = conn.prepareStatement(vulnrloghistoryupdatesql);
									pst1.setString(1, ipassetId);
									pst1.setString(2, assetName);
									pst1.setString(3, logassetId);
									//System.out.println(pst5.toString());
									pst1.executeUpdate();
									pst1.close();
								} else {
									pst1 = conn.prepareStatement(vulnrlogupdatesql);
									pst1.setString(1, ipassetId);
									pst1.setString(2, assetName);
									pst1.setString(3, logassetId);
									//System.out.println(pst3.toString());
									pst1.executeUpdate();
									pst1.close();
								}
							}
							*/
						}
					} else {	
						String ipassetId=null, ipassetName=null;
						s = assetipsql.replaceAll("REPLACE_STRING", ipAddress);	// Check if Vulnr IP Address Match
						pst1 = conn.prepareStatement(s);
						pst1.setString(1, ipAddress);
						pst1.setString(2, ipAddress);
						pst1.setString(3, ipAddress);
						//System.out.println(pst1.toString());
						rs1 = pst1.executeQuery();
						while(rs1.next()) {
							ipassetId = rs1.getString("id");
							ipassetName = rs1.getString("asset_name");
							ip = rs1.getString("ip_address");
						}
						rs1.close();
						pst1.close();

						if(ipassetId == null) {
							System.out.println("ID Matching but IP Not Matching!!!");
							if(sync.equalsIgnoreCase("Y")) {	// Reset ID and Sync to 'N'
								String updatesql = "update enterprise_vulnerability_asset set id = uuid(), extension=json_set(extension, '$.sync', 'N') where id = ?";
								pst1 = conn.prepareStatement(updatesql);
								pst1.setString(1, id);
								pst1.executeUpdate();
								pst1.close();
							}
						} else if(!id.equals(ipassetId)) {
							System.out.println("Id matching but, IP Matched to different Asset!!!");
						}
					}
				}
			}
        }
        rs.close();
        pst.close();
        conn.commit();

        System.out.println("Successfully Resynced Vulnerability Asset with Asset DB!!!");
    }

	// Cleanup spaces in IP Address List in IT Assets
	private void cleanupAssetIPAddress() throws Exception {
		String sql = "select id, extension from enterprise_asset where asset_category = 'IT ASSET'";
		String updatesql = "update enterprise_asset set extension=? where id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);

			JSONArray ipArr = null;
			if(extObj.has("IP Address")) {
				ipArr = (JSONArray)extObj.get("IP Address");
			} else {
				ipArr = new JSONArray();
			}
			System.out.println(id+":"+extObj.toString());
			JSONArray newList = new JSONArray();
			Iterator iter = ipArr.iterator();
			while(iter.hasNext()) {
				String str = ((String)iter.next()).trim();
				newList.add(str);
			}
			System.out.println(newList.toString());
			extObj.put("IP Address", newList);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
		System.out.println("Successfully updated IP List");
	}

	private void updateComponentIds() throws Exception {
		String sql = "select id, component_ids from enterprise_ml_result";
		String updatesql = "update enterprise_ml_result set component_ids = ? where id = ?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String componentIds = rs.getString("component_ids");
			if(componentIds != null && componentIds.length() > 0) {
				System.out.println(id);
				JSONArray idList = new JSONArray(componentIds);
				JSONArray newList = new JSONArray();
				if(idList.length() > 100) {
					for(int i=0; i<100; i++) {
						String str = (String)idList.get(i);
						newList.add(str);
					}
					System.out.println(idList.length());
					PreparedStatement pst1 = conn.prepareStatement(updatesql);
					pst1.setString(1, newList.toString());
					pst1.setString(2, id);
					pst1.executeUpdate();
					pst1.close();
				}
			}
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updatedRelatedControls() throws Exception {
		String sql = "select id, json_unquote(json_extract(business_capability_ext, '$.related_controls')) related_controls from enterprise_business_capability";
		String updatesql = "update enterprise_business_capability set business_capability_ext = json_set(business_capability_ext, '$.related_controls', ?) where id = ?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String rc = rs.getString("related_controls");
			JSONArray rcList = new JSONArray();
			if(rc != null && rc.length() > 0) {
				String[] rcArr = rc.split(",");
				for (int i = 0; i < rcArr.length; i++) {
					rcList.put(rcArr[i].trim());
				}
			}
			System.out.println(rcList.toString());
			
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, rcList.toString());
			pst1.setString(2, id);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updatePossiblePatches() throws Exception {
		String sql = "select _id, extension from enterprise_vulnerability";
		String updatesql = "update enterprise_vulnerability set extension = ? where _id = ?";

		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			JSONArray arr = (JSONArray)extObj.get("Possible Patches");
			String str = (String)arr.get(0);
			System.out.println(str);
			if(str.equals("'-")) {
				arr = new JSONArray();
				extObj.put("Possible Patches", arr);

				PreparedStatement pst1 = conn.prepareStatement(updatesql);
				pst1.setString(1, extObj.toString());
				pst1.setString(2, id);
				pst1.executeUpdate();
				pst1.close();
			}

			/*
			JSONArray ppList = new JSONArray();
			if(pp != null && pp.trim().length() > 0) {
				String[] ppArr = pp.split(",");
				for (int i = 0; i < ppArr.length; i++) {
					ppList.put(ppArr[i].trim());
				}
			}
			extObj.put("Possible Patches", ppList);
			
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.executeUpdate();
			pst1.close();
			*/
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateCVEAssociated() throws Exception {
		String sql = "select _id, extension from enterprise_vulnerability";
		String updatesql = "update enterprise_vulnerability set extension = ? where _id = ?";

		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			String cve = (String)extObj.get("CVEs Associated");
			JSONArray cveList = new JSONArray();
			if(cve != null && cve.trim().length() > 0) {
				String[] cveArr = cve.split(",");
				for (int i = 0; i < cveArr.length; i++) {
					String str = cveArr[i].trim();
					str = str.replaceAll("'", "");
					if(!str.equals("-")) {
						cveList.put(cveArr[i].trim());
					}
				}
			}
			extObj.put("CVEs Associated", cveList);
			
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, extObj.toString());
			pst1.setString(2, id);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateDeploymentModel() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0, matchedCount = 0;
		String[] row;
		String updatesql = "update enterprise_asset set extension=json_set(extension, '$.\"Application Deployment Model\"', ?) where asset_name = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		while ((row = reader.readNext()) != null) {
			rowCount++;
			String name="", model="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						name = row[i].trim();
					} else if(i==1) {
						model = row[i].trim();
					}
				}
				if(model == null) {
					model = "";
				}

				String checksql = "select json_unquote(json_extract(extension, '$.\"Application Deployment Model\"')) model from enterprise_asset where asset_name = ?";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, name);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					String dmodel = rs.getString("model");
					
					if(dmodel == null || dmodel.length() == 0) {
						System.out.println(model+":"+dmodel);
						pst1.setString(1, model);
						pst1.setString(2, name);
						pst1.addBatch();
					}
				}
				rs.close();
				pst.close();
			}
		}

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void reconcileMauricesAssets() throws Exception {
		System.out.println("Successfully read data file:" + filename);
		int rowCount = 0, businessAppcontractMissingCount = 0, itAppContractMissingCount = 0, businessITAppInBusinessAppCount = 0;
		String[] row;
		OrderedJSONObject contractAssetList = new OrderedJSONObject();
		OrderedJSONObject businessAppList = new OrderedJSONObject();
		OrderedJSONObject itAppList = new OrderedJSONObject();
		JSONArray softwareList = new JSONArray();
		JSONArray duplicates = new JSONArray();

		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String sla="", contractAssetName="", businessApp="", businessITApp="", itApp="", software="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						sla = row[i].trim();
					} else if(i==1) {
						contractAssetName = row[i].trim();
					} else if(i==2) {
						businessApp = row[i].trim();
					} else if(i==3) {
						businessITApp = row[i].trim();
					} else if(i==4) {
						itApp = row[i].trim();
					} else if(i==5) {
						software = row[i].trim();
					} 
				}

				//System.out.println(sla+":"+contractAssetName);

				if(!contractAssetList.has(contractAssetName)) {
					contractAssetList.put(contractAssetName, sla);
				}
			}
		}
		reader.close();

		//System.out.println("Total Rows:"+rowCount+"\n Total Contract Asset List:"+contractAssetList.size());
		rowCount = 0;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String businessApp="", businessITApp="", itApp="", software="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
					} else if(i==1) {
					} else if(i==2) {
						businessApp = row[i].trim();
					} else if(i==3) {
						businessITApp = row[i].trim();
					} else if(i==4) {
						itApp = row[i].trim();
					} else if(i==5) {
						software = row[i].trim();
					} 
				}
				if(businessApp.length() > 0 && !businessAppList.has(businessApp)) {
					String sla = "";
					if(contractAssetList.has(businessApp)) {
						sla = (String)contractAssetList.get(businessApp);
					} else {
						//System.out.println("Business App not found in Contract List!!!");
						businessAppcontractMissingCount++;
						//System.out.println(businessApp);
					}
					//System.out.println(businessApp);
					businessAppList.put(businessApp, sla);
				}
			}
		}
		
		reader.close();
		rowCount = 0;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String businessApp="", businessITApp="", itApp="", software="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
					} else if(i==1) {
					} else if(i==2) {
					} else if(i==3) {
						businessITApp = row[i].trim();
					} else if(i==4) {
						itApp = row[i].trim();
					} else if(i==5) {
						software = row[i].trim();
					} 
				}
	
				if(businessITApp.length() > 0 && businessAppList.has(businessITApp)) {
					businessITAppInBusinessAppCount++;
					//System.out.println(businessITApp);	// Duplicate
					duplicates.add(businessITApp);
				} else if(businessITApp.length() > 0 && !businessAppList.has(businessITApp)) {
					String sla = "";
					if(contractAssetList.has(businessITApp)) {
						sla = (String)contractAssetList.get(businessITApp);
					} else {
						//System.out.println("Software App not found in Contract List!!!");
						businessAppcontractMissingCount++;
						//System.out.println(businessITApp);
					}
					System.out.println(businessITApp);
					businessAppList.put(businessITApp, sla);
				}

				if(itApp.length() > 0 && !itAppList.has(itApp)) {
					String sla = "";
					if(contractAssetList.has(itApp)) {
						sla = (String)contractAssetList.get(itApp);
					} else {
						//System.out.println("IT App not found in Contract List!!!");
						itAppContractMissingCount++;
						//System.out.println(itApp);
					}
					itAppList.put(itApp, sla);
				}

				if(software.length() > 0 && !softwareList.contains(software)) {
					softwareList.add(software);
				}
			}
		}
	
		System.out.println("Total Contract Asset:"+contractAssetList.size());
		System.out.println("Total Business App:"+businessAppList.size());
		System.out.println("Total IT App:"+itAppList.size());
		System.out.println("Total Software:"+softwareList.size());
		System.out.println("Business App Missing in Contract:"+businessAppcontractMissingCount);
		System.out.println("Business IT App in Business App:"+businessITAppInBusinessAppCount);
		System.out.println("IT App Missing in Contract:"+itAppContractMissingCount);
	}

	// Reconcile Rackspace Assets
	// Format - 1
	private void updateRackspaceVM() throws Exception {
		String[] row;
		int rowCount = 0;
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String assetName="", env="", type="", machineType="", externalIp="", internalIp="", dc="", tag="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						assetName = row[i].trim();
					} else if(i==1) {
						env = row[i].trim();
					} else if(i==2) {
						type = row[i].trim();
					} else if(i==3) {
						machineType = row[i].trim();
					} else if(i==4) {
						externalIp = row[i].trim();
					} else if(i==5) {
						internalIp = row[i].trim();
					} else if(i==6) {
						dc = row[i].trim();
					} else if(i==7) {
						tag = row[i].trim();
					} 
				}
				if(externalIp == null) {
					externalIp = "";
				} else {
					externalIp = externalIp.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
					externalIp = externalIp.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
				}
				if(internalIp == null) {
					internalIp = "";
				} else {
					internalIp = internalIp.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
					internalIp = internalIp.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
				}

				System.out.println(assetName+":"+env+":"+type+":"+externalIp+":"+internalIp+":"+dc);

				JSONArray ipAddress = new JSONArray();
				JSONArray privateIpAddress = new JSONArray();
				JSONArray publicIpAddress = new JSONArray();
				if(externalIp.length() > 0) {
					ipAddress.add(externalIp);
					publicIpAddress.add(externalIp);
				}
				if(internalIp.length() > 0) {
					ipAddress.add(internalIp);
					privateIpAddress.add(internalIp);
				}
				JSONArray techOwner = new JSONArray();
				techOwner.add("Mastan Reddy");
				JSONArray queue = new JSONArray();
				queue.add("ServiceNow-ECommSupport-SG");
				JSONArray classification = new JSONArray();
				if(type.indexOf("Web") != -1) {
					classification.add("MAUR");
				} else if(type.indexOf("Store") != -1) {
					classification.add("MAUR");
				} else if(type.indexOf("BCC") != -1) {
					classification.add("BCC");
				} else if(type.indexOf("Search Index") != -1) {
					classification.add("MDEX");
				} else if(type.indexOf("Search Admin") != -1) {
					classification.add("EDCA");
				} else if(type.indexOf("XCC Coupon") != -1) {
					classification.add("XCC");
				} else if(type.indexOf("Jenkins") != -1) {
					classification.add("JKNS");
				} else if(type.indexOf("Indentity ") != -1) {
					classification.add("IPA");
				} else if(type.indexOf("XCC PEM") != -1) {
					classification.add("PEM");
				} else if(type.indexOf("Weblogic Service") != -1) {
					classification.add("BTM");
				} else if(type.indexOf("XCC CCM") != -1) {
					classification.add("CCM");
				} else if(type.indexOf("XCC Admin") != -1) {
					classification.add("XCC");
				} else if(type.indexOf("Blue Voyant") != -1) {
					classification.add("BVYT");
				} else if(type.indexOf("DR DB") != -1) {
					classification.add("DB");
				} else if(type.indexOf("DB Node") != -1) {
					classification.add("DB");
				} else if(type.indexOf("Qroc") != -1) {
					classification.add("QROC");
				}

				if(env.indexOf("PRD") != -1) {
					env = "PROD";
				} else if(env.indexOf("SIT") != -1) {
					env = "SIT";
				} else if(env.indexOf("UAT") != -1) {
					env = "UAT";
				} else if(env.indexOf("DEV") != -1) {
					env = "DEV";
				} else if(env.indexOf("PRF") != -1) {
					env = "PERF";
				} else if(env.indexOf("Production") != -1) {
					env = "PROD";
				} else if(env.indexOf("Performance") != -1) {
					env = "PERF";
				} else if(env.indexOf("Lower") != -1) {
					env = "DEV";
				} else {
					env = "PROD";
				}

				if(dc.indexOf("ORD") != -1) {
					dc = "Rackspace Chicago";
				} else if(dc.indexOf("IAD") != -1) {
					dc = "Rackspace Virginia";
				}

				OrderedJSONObject lcObj = new OrderedJSONObject();
				lcObj.put("id", "0");
				lcObj.put("current", new Boolean(true));
				lcObj.put("lifecycle_name", "Invest");
				lcObj.put("startdate", "");
				lcObj.put("enddate", "");
				lcObj.put("impact", "");
				lcObj.put("lifecycle_status", "");
				lcObj.put("comment", "");
				JSONArray lcList = new JSONArray();
				lcList.add(lcObj);

				String id = null, ext = null;
				String checksql = "select id, extension from enterprise_asset where asset_name = ?";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, assetName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(id != null) {
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					extObj.put("Environment", env);
					extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					extObj.put("Type", "Server");
					if(type.equalsIgnoreCase("Server")) {
						extObj.put("Sub-Type", "Physical Server");
					} else {
						extObj.put("Sub-Type", "Virtual Server");
					}
					extObj.put("Organization", "Maurices");
					extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Katia Corbo");
					extObj.put("Technical Owner", techOwner);
					extObj.put("Application Support Queue", queue);
					extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					extObj.put("Classification", classification);

					String updatesql = "update enterprise_asset set owner=?,asset_location_name=?,lifecycle_name=?,asset_lifecycle_list=?,extension=? where id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, "Austin Pritchard");
					pst.setString(2, dc);
					pst.setString(3, "Invest");
					pst.setString(4, lcList.toString());
					pst.setString(5, extObj.toString());
					pst.setString(6, id);
					pst.executeUpdate();
					pst.close();
					System.out.println("Successfully updated "+assetName);
				} else {
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					extObj.put("Environment", env);
					extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					extObj.put("Type", "Server");
					if(type.equalsIgnoreCase("Server")) {
						extObj.put("Sub-Type", "Physical Server");
					} else {
						extObj.put("Sub-Type", "Virtual Server");
					}
					extObj.put("Organization", "Maurices");
					extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Katia Corbo");
					extObj.put("Technical Owner", techOwner);
					extObj.put("Application Support Queue", queue);
					extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					extObj.put("Classification", classification);

					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,asset_location_name,lifecycle_name,asset_lifecycle_list,owner,extension,asset_category,created_by,updated_by,company_code) values(uuid(),?,?,?,?,?,?,?,'IT ASSET','support@smarterd.com','support@smarterd.com',?)";
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetName);
					pst.setString(2, env);
					pst.setString(3, dc);
					pst.setString(4, "Invest");
					pst.setString(5, lcList.toString());
					pst.setString(6, "Austin Pritchard");
					pst.setString(7, extObj.toString());
					pst.setString(8, companyCode);
					pst.executeUpdate();
					pst.close();
				}
			}
		}
		conn.commit();
		conn.close();
	}

	// Reconcile Rackspace Assets
	// Format - 2
	private void updateRackspaceVMFormat2() throws Exception {
		String[] row;
		int rowCount = 0;
		// Update existing asset List lifecycle
		String updatelcsql = "update enterprise_asset set lifecycle_name='Decommissioned' and asset_lifecycle_list=json_set(asset_lifecycle_list,'$[0].lifecycle_name','Decommissioned') where asset_category = 'IT ASSET' and asset_location_name like 'Rack%'";
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String assetName="", env="", type="", subtype="", externalIp="", internalIp="", dc="", tag="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
					} else if(i==1) {
						assetName = row[i].trim();
					} else if(i==2) {
						tag = row[i].trim();
					} else if(i==3) {
						type = row[i].trim();
					} else if(i==4) {
						subtype = row[i].trim();
					} else if(i==5) {
						dc = row[i].trim();
					} else if(i==6) {
						externalIp = row[i].trim();
					} else if(i==7) {
						internalIp = row[i].trim();
					} 
				}
				if(externalIp == null) {
					externalIp = "";
				} else {
					externalIp = externalIp.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
					externalIp = externalIp.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
				}
				if(internalIp == null) {
					internalIp = "";
				} else {
					internalIp = internalIp.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
					internalIp = internalIp.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");

				}
				if(type.equalsIgnoreCase("Virtual Machine")) {
					type = "Server";
					subtype = "Virtual Server";
				} else if(type.equalsIgnoreCase("Server")) {
					subtype = "Physical Server";
				} else if(type.equalsIgnoreCase("Other")) {
					if(subtype.toLowerCase().indexOf("switch") != -1) {
						type = "Network Device";
						subtype = "Switch";
					} else if(subtype.toLowerCase().indexOf("hypervisor") != -1) {
						type = "Server";
						subtype = "Virtual Server";
					} else if(subtype.toLowerCase().indexOf("storage") != -1) {
						type = "Storage Device";
						subtype = "Storage Array";
					} else if(subtype.toLowerCase().indexOf("nas") != -1) {
						type = "Storage Device";
						subtype = "Storage Array";
					} else if(subtype.toLowerCase().indexOf("rac cluster") != -1) {
						type = "Server";
						subtype = "Physical Server";
					}
				} else if(type.equalsIgnoreCase("Firewall")) {
					type = "Network Device";
					subtype = "Firewall";
				} else if(type.equalsIgnoreCase("Hypervisor")) {
					type = "Server";
					subtype = "Virtual Server";
				} else if(type.equalsIgnoreCase("Load Balancer")) {
					type = "Network Device";
					subtype = "Load Balancer";
				} 

				//System.out.println(assetName+":"+env+":"+type+":"+externalIp+":"+internalIp+":"+dc);

				JSONArray ipAddress = new JSONArray();
				JSONArray privateIpAddress = new JSONArray();
				JSONArray publicIpAddress = new JSONArray();
				if(externalIp.length() > 0) {
					ipAddress.add(externalIp);
					publicIpAddress.add(externalIp);
				}
				if(internalIp.length() > 0) {
					String[] ipList = internalIp.split(",");
					for(int i=0; i<ipList.length;i++) {
						String str = ipList[i].trim();
						ipAddress.add(str);
						privateIpAddress.add(str);
					}
				}
				JSONArray techOwner = new JSONArray();
				techOwner.add("Mastan Reddy");
				JSONArray queue = new JSONArray();
				queue.add("ServiceNow-ECommSupport-SG");
				JSONArray classification = new JSONArray();
				if(tag.indexOf("Web") != -1) {
					classification.add("MAUR");
				} else if(tag.indexOf("Store") != -1) {
					classification.add("MAUR");
				} else if(tag.indexOf("BCC") != -1) {
					classification.add("BCC");
				} else if(tag.indexOf("Search Index") != -1) {
					classification.add("MDEX");
				} else if(tag.indexOf("Search Admin") != -1) {
					classification.add("EDCA");
				} else if(tag.indexOf("XCC Coupon") != -1) {
					classification.add("XCC");
				} else if(tag.indexOf("Jenkins") != -1) {
					classification.add("JKNS");
				} else if(tag.indexOf("Indentity ") != -1) {
					classification.add("IPA");
				} else if(tag.indexOf("XCC PEM") != -1) {
					classification.add("PEM");
				} else if(tag.indexOf("Weblogic Service") != -1) {
					classification.add("BTM");
				} else if(tag.indexOf("XCC CCM") != -1) {
					classification.add("CCM");
				} else if(tag.indexOf("XCC Admin") != -1) {
					classification.add("XCC");
				} else if(tag.indexOf("Blue Voyant") != -1) {
					classification.add("BVYT");
				} else if(tag.indexOf("DR DB") != -1) {
					classification.add("DB");
				} else if(tag.indexOf("DB Node") != -1) {
					classification.add("DB");
				} else if(tag.indexOf("Qroc") != -1) {
					classification.add("QROC");
				}

				if(tag.indexOf("PRD") != -1) {
					env = "PROD";
				} else if(tag.indexOf("SIT") != -1) {
					env = "SIT";
				} else if(tag.indexOf("UAT") != -1) {
					env = "UAT";
				} else if(tag.indexOf("DEV") != -1) {
					env = "DEV";
				} else if(tag.indexOf("PRF") != -1) {
					env = "PERF";
				} else if(tag.indexOf("Production") != -1) {
					env = "PROD";
				} else if(tag.indexOf("Performance") != -1) {
					env = "PERF";
				} else if(tag.indexOf("Lower") != -1) {
					env = "DEV";
				} else {
					env = "PROD";
				}

				if(dc.indexOf("ORD") != -1) {
					dc = "Rackspace Chicago";
				} else if(dc.indexOf("IAD") != -1) {
					dc = "Rackspace Virginia";
				}

				OrderedJSONObject lcObj = new OrderedJSONObject();
				lcObj.put("id", "0");
				lcObj.put("current", new Boolean(true));
				lcObj.put("lifecycle_name", "Invest");
				lcObj.put("startdate", "");
				lcObj.put("enddate", "");
				lcObj.put("impact", "");
				lcObj.put("lifecycle_status", "");
				lcObj.put("comment", "");
				JSONArray lcList = new JSONArray();
				lcList.add(lcObj);

				String id = null, ext = null;
				String checksql = "select id, extension from enterprise_asset where asset_name = ?";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, assetName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(id != null) {
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					extObj.put("Environment", env);
					if(type.equalsIgnoreCase("server")) {
						extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					}
					extObj.put("Type", type);
					extObj.put("Sub-Type", subtype);
					extObj.put("Organization", "Maurices");
					extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Katia Corbo");
					extObj.put("Technical Owner", techOwner);
					extObj.put("Application Support Queue", queue);
					extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					extObj.put("Classification", classification);

					String updatesql = "update enterprise_asset set owner=?,asset_location_name=?,lifecycle_name=?,asset_lifecycle_list=?,extension=? where id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, "Austin Pritchard");
					pst.setString(2, dc);
					pst.setString(3, "Invest");
					pst.setString(4, lcList.toString());
					pst.setString(5, extObj.toString());
					pst.setString(6, id);
					pst.executeUpdate();
					pst.close();
					
					System.out.println("Successfully updated "+assetName);
				} else {
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					extObj.put("Environment", env);
					if(type.equalsIgnoreCase("server")) {
						extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					}
					extObj.put("Type", type);
					extObj.put("Sub-Type", subtype);
					extObj.put("Organization", "Maurices");
					extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Katia Corbo");
					extObj.put("Technical Owner", techOwner);
					extObj.put("Application Support Queue", queue);
					extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					extObj.put("Classification", classification);
					
					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,asset_location_name,lifecycle_name,asset_lifecycle_list,owner,extension,asset_category,created_by,updated_by,company_code) values(uuid(),?,?,?,?,?,?,?,'IT ASSET','support@smarterd.com','support@smarterd.com',?)";
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetName);
					pst.setString(2, env);
					pst.setString(3, dc);
					pst.setString(4, "Invest");
					pst.setString(5, lcList.toString());
					pst.setString(6, "Austin Pritchard");
					pst.setString(7, extObj.toString());
					pst.setString(8, companyCode);
					pst.executeUpdate();
					pst.close();
					
					System.out.println("New Asset:"+assetName);
				}
			}
		}
		conn.commit();
		conn.close();
	}

	// Reconcile Server List
	private void reconcileServerList() throws Exception {
		String[] row;
		int rowCount = 0;
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String assetName="", env="", type="", machineType="", externalIp="", internalIp="", dc="", owner="",techOwner="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						assetName = row[i].trim();
					} else if(i==1) {
						internalIp = row[i].trim();;
					} else if(i==2) {
						externalIp = row[i].trim();;
					} else if(i==3) {
						owner = row[i].trim();
					} else if(i==4) {
						techOwner = row[i].trim();
					}
				}
				if(internalIp == null) {
					internalIp = "";
				}
				if(externalIp == null) {
					externalIp = "";
				}
				if(techOwner == null) {
					techOwner = "";
				}

				JSONArray techOwnerList = new JSONArray();
				if(techOwner.length() > 0) {
					String[] owners = techOwner.split("/");
					for(int i=0; i<owners.length;i++) {
						String str = owners[i].trim();
						if(str.equalsIgnoreCase("Magesh Mayilvaganan")) {
							continue;
						} else {
							if(str.equalsIgnoreCase("Mayank")) {
								str = "Mayank Rauthan";
							} else if(str.equalsIgnoreCase("Gopinath")) {
								str = "Gopinath Maasilamani";
							} else if(str.equalsIgnoreCase("Ganga")) {
								str = "Gangadhar Narapuram";
							}
							techOwnerList.add(str);
						}
					}

				}

				//System.out.println(assetName+":"+internalIp);
				//System.out.println(assetName+":"+env+":"+externalIp+":"+internalIp+":"+dc);

				String id = null, ext = null;
				String checksql = null;
				PreparedStatement pst = null;
				if(assetName.length() > 0) {
					checksql = "select id, extension from enterprise_asset where asset_name = ?";
					pst = conn.prepareStatement(checksql);
					pst.setString(1, assetName);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						id = rs.getString("id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();

					if(id == null) {
						checksql = "select id, extension from enterprise_asset where json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"','$') = 1";
						checksql = checksql.replaceAll("REPLACE_STRING", internalIp);
						pst = conn.prepareStatement(checksql);
						rs = pst.executeQuery();
						while(rs.next()) {
							id = rs.getString("id");
							ext = rs.getString("extension");
						}
						rs.close();
						pst.close();
					}
				} else if(internalIp.length() > 0) {
					checksql = "select id, extension from enterprise_asset where json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"','$') = 1";
					checksql = checksql.replaceAll("REPLACE_STRING", internalIp);
					pst = conn.prepareStatement(checksql);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						id = rs.getString("id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
				}
				
				OrderedJSONObject extObj = null;
				if(id != null) {
					extObj = new OrderedJSONObject(ext);

					JSONArray ipAddress = null;
					JSONArray privateIpAddress = null;
					JSONArray publicIpAddress = null;
					if(extObj.has("IP Address")) {
						ipAddress = (JSONArray)extObj.get("IP Address");
					} else {
						ipAddress = new JSONArray();
					}
					if(extObj.has("Private IP Address")) {
						privateIpAddress = (JSONArray)extObj.get("Private IP Address");
					} else {
						privateIpAddress = new JSONArray();
					}
					if(extObj.has("Public IP Address")) {
						publicIpAddress = (JSONArray)extObj.get("Public IP Address");
					} else {
						publicIpAddress = new JSONArray();
					}
					if(externalIp.length() > 0) {
						String[] ipList = externalIp.split(",");
						for(int i=0; i<ipList.length;i++) {
							String str = ipList[i].trim();
							if(!publicIpAddress.contains(str)) {
								publicIpAddress.add(str);
							}
							if(!ipAddress.contains(str)) {
								ipAddress.add(str);
							}
						}
					}
					if(internalIp.length() > 0) {
						if(!privateIpAddress.contains(internalIp)) {
							privateIpAddress.add(internalIp);
						}
						if(!ipAddress.contains(internalIp)) {
							ipAddress.add(internalIp);
						}
					}
					
					/*
					techOwner.add("Julius Pelejo");
					JSONArray queue = new JSONArray();
					queue.add("ServiceNow-ECommSupport-SG");
					JSONArray classification = new JSONArray();
					*/

					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					//extObj.put("Environment", env);
					//extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					//extObj.put("Type", "Server");
					//extObj.put("Sub-Type", "Virtual Server");
					extObj.put("Organization", "Maurices");
					//extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Adinarayana Golla");
					extObj.put("Technical Owner", techOwnerList);
					//extObj.put("Application Support Queue", queue);
					//extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					//extObj.put("Classfication", classification);

					/*
					if(dc.indexOf("ORD") != -1) {
						dc = "Rackspace Chicago";
					} else if(dc.indexOf("IAD") != -1) {
						dc = "Rackspace Virginia";
					}
					*/
					/*
					String updatesql = "update enterprise_asset set owner=?,asset_location_name=?,extension=? where id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, "Austin Pritchard");
					pst.setString(2, dc);
					pst.setString(3, extObj.toString());
					pst.setString(4, id);
					pst.executeUpdate();
					pst.close();
					*/
					/*
					String updatesql = "update enterprise_asset set owner=?,extension=? where id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, "Austin Pritchard");
					pst.setString(2, extObj.toString());
					pst.setString(3, id);
					pst.executeUpdate();
					pst.close();
					System.out.println("Successfully updated "+assetName);
					*/
					String updatesql = "update enterprise_asset set asset_lifecycle_list=?, lifecycle_name=? where id = ?";
					OrderedJSONObject lcObj = new OrderedJSONObject();
					lcObj.put("id", "0");
					lcObj.put("current", new Boolean(true));
					lcObj.put("lifecycle_name", "Invest");
					lcObj.put("startdate", "");
					lcObj.put("enddate", "");
					lcObj.put("impact", "");
					lcObj.put("lifecycle_status", "");
					lcObj.put("comment", "");
					JSONArray lcList = new JSONArray();
					lcList.add(lcObj);
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, lcList.toString());
					pst.setString(2, "Invest");
					pst.setString(3, id);
					pst.executeUpdate();
					pst.close();
					System.out.println("Successfully updated "+assetName);
				} else {	
					// Get location from server name
					String location = assetName.substring(0,3);
					// Get Environment
					env = assetName.substring(3,4);
					// Get OS
					String os = assetName.substring(4,5);
					
					System.out.println(location+":"+env+":"+os);
					if(location.equalsIgnoreCase("DC2")) {
						location = "Chaska DC";
					} else if(location.equalsIgnoreCase("DC4")) {
						location = "Ashburn-OCI";
					} else if(location.equalsIgnoreCase("DC6")) {
						location = "Chaska DC";
					}
					if(env.equalsIgnoreCase("p")) {
						env = "PROD";
					} else if(env.equalsIgnoreCase("d")) {
						env = "DEV";
					} else if(env.equalsIgnoreCase("t")) {
						env = "UAT";
					} 
					if(os.equalsIgnoreCase("L")) {
						os = "Red Hat Enterprise Linux 8 (64-bit)";
					} else if(os.equalsIgnoreCase("W")) {
						os = "Microsoft Windows Server 2019";
					} else {
						os = "";
					}

					extObj = new OrderedJSONObject();
					extObj = initializeITAssetExtension(extObj);
					//extObj.put("Environment", env);
					//extObj.put("OS", "Red Hat Enterprise Linux 7 (64-bit)");
					extObj.put("Type", "Server");
					extObj.put("Sub-Type", "Virtual Server");
					extObj.put("Organization", "Maurices");
					//extObj.put("Business Unit", "eCommerce");
					extObj.put("Manager", "Adinarayana Golla");
					extObj.put("Technical Owner", techOwnerList);
					//extObj.put("Application Support Queue", queue);
					//extObj.put("Infra Support Queue", queue);
					extObj.put("Operation Support Provider", "BCT");
					extObj.put("Environment", env);
					extObj.put("OS", os);

					JSONArray ipAddress = new JSONArray();
					JSONArray privateIpAddress = new JSONArray();
					JSONArray publicIpAddress = new JSONArray();
					
					if(externalIp.length() > 0) {
						String[] ipList = externalIp.split(",");
						for(int i=0; i<ipList.length;i++) {
							String str = ipList[i].trim();
							publicIpAddress.add(str);
							ipAddress.add(str);
						}
					}
					if(internalIp.length() > 0) {
						privateIpAddress.add(internalIp);
						ipAddress.add(internalIp);
					}
					extObj.put("Private IP Address", privateIpAddress);
					extObj.put("Public IP Address", publicIpAddress);
					extObj.put("IP Address", ipAddress);
					extObj.put("Floating IP Address", new JSONArray());
					extObj.put("Scan IP Address", new JSONArray());

					String desc = "";
					if(assetName.indexOf("FINTHBDB") != -1) {
						desc = "THUB Database Server";
					} else if(assetName.indexOf("ZABAPP") != -1) {
						desc = "Zabbix Application Server";
					} else if(assetName.indexOf("ZABADB") != -1) {
						desc = "Zabbix Database Server";
					} else if(assetName.indexOf("ZABAPRX") != -1) {
						desc = "Zabbix Application Server";
					}

					//extObj.put("Classfication", classification);
					/*
					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,asset_location_name,asset_category,asset_type,lifecycle_name,asset_comment,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(uuid(),?,?,?,?,?,?,?,?,?,?,?,?)";
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetName);
					pst.setString(2, desc);
					pst.setString(3, location);
					pst.setString(4, "IT ASSET");
					pst.setString(5, "ASSET");
					pst.setString(6, "Invest");
					pst.setString(7, new JSONArray().toString());
					pst.setString(8, new JSONArray().toString());
					pst.setString(9, extObj.toString());
					pst.setString(10, "support#smarterd.com");
					pst.setString(11, "support#smarterd.com");
					pst.setString(12, "MAUR");
					pst.executeUpdate();
					pst.close();
					*/

					System.out.println("Asset Not Found:"+assetName+":"+internalIp);
				}
			}
		}
		conn.commit();
		conn.close();
	}

	private void validateAssetListByIP() throws Exception {
        String sql = "select id, asset_name, asset_location_name, owner, lifecycle_name, json_unquote(json_extract(extension, '$.Type')) type, json_unquote(json_extract(extension, '$.\"Sub-Type\"')) subtype, json_unquote(json_extract(extension, '$.\"Technical Owner\"')) tech_owner, json_unquote(json_extract(extension, '$.\"OS\"')) os, json_unquote(json_extract(extension, '$.\"Environment\"')) env from enterprise_asset where json_contains(json_extract(extension, '$.\"IP Address\"'),'\"REPLACE_STRING\"', '$') = 1";
		JSONArray ipList = new JSONArray();
		ipList.add("10.102.10.188");
		ipList.add("10.102.10.70");
		ipList.add("10.167.107.164");
		ipList.add("10.96.1.129");
		ipList.add("10.96.1.113");
		ipList.add("10.167.117.215");
		ipList.add("10.96.1.114");
		ipList.add("10.102.9.113");
		ipList.add("10.102.10.221");
		ipList.add("10.102.10.128");
		ipList.add("10.96.4.157");
		ipList.add("10.102.16.102");
		ipList.add("10.96.19.233");
		ipList.add("10.167.115.242");
		ipList.add("10.102.10.215");
		ipList.add("10.96.19.169");
		ipList.add("10.102.15.36");
		ipList.add("10.102.6.247");
		ipList.add("10.102.16.174");
		ipList.add("10.102.10.222");
		ipList.add("10.102.10.62");
		ipList.add("10.102.9.228");
		ipList.add("10.102.10.214");
		ipList.add("10.96.19.243");
		ipList.add("10.102.16.216");
		ipList.add("10.167.115.219");
		ipList.add("10.102.16.109");
		ipList.add("10.102.10.227");
		ipList.add("10.102.10.78");
		ipList.add("10.167.106.242");
		ipList.add("10.162.115.108");
		ipList.add("10.96.1.128");
		ipList.add("10.102.10.42");
		ipList.add("10.102.16.40");
		ipList.add("10.167.120.242");
		ipList.add("10.96.19.209");
		ipList.add("10.102.16.23");
		ipList.add("10.102.16.134");
		ipList.add("10.102.10.212");
		ipList.add("10.102.50.37");
		ipList.add("10.102.12.164");
		ipList.add("10.102.10.43");
		ipList.add("10.102.9.219");
		ipList.add("10.96.17.10");
		ipList.add("10.160.34.108");
		ipList.add("10.167.115.210");
		ipList.add("10.102.4.164");

        Iterator iter = ipList.iterator();
        while(iter.hasNext()) {
            String ip = (String)iter.next();
            String s = sql.replaceAll("REPLACE_STRING", ip);
            PreparedStatement pst = conn.prepareStatement(s);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                String id = rs.getString("id");
                String name = rs.getString("asset_name");
                String loc = rs.getString("asset_location_name");
                String lc = rs.getString("lifecycle_name");
                String owner = rs.getString("owner");
                String techOwner = rs.getString("tech_owner");
                String type = rs.getString("type");
                String subtype = rs.getString("subtype");
                String os = rs.getString("os");
                String env = rs.getString("env");
                if(techOwner != null && techOwner.length() > 0) {
                    techOwner = techOwner.replaceAll("[\\[\\]]", "").replaceAll("\"", "");
                }
                if(lc == null) {
                    lc = "";
                }
                if(loc == null) {
                    loc = "";
                }
                if(type == null) {
                    type = "";
                }
                if(subtype == null) {
                    subtype = "";
                }
                if(env == null) {
                    env = "";
                }
				System.out.println(ip+":"+name+":"+type+":"+lc);
				/*
                OrderedJSONObject obj = new OrderedJSONObject();
                obj.put("id", id);
                obj.put("asset_name", name);
                obj.put("lifecycle", lc);
                obj.put("location", loc);
                obj.put("env", env);
                obj.put("type", type);
                obj.put("sub_type", subtype);
                obj.put("os", os);
                obj.put("owner", owner);
                obj.put("tech_owner", techOwner);

                assetList.add(obj);
				*/
            }
            rs.close();
            pst.close();
        }
        //res.put("asset_list", assetList);
        //return res;
    }

	private void generateApplicationMetadataFromITAsset() throws Exception {
		String sql = "select id, asset_name, asset_location_name, extension from enterprise_asset where asset_category = 'IT ASSET' and lower(json_unquote(json_extract(extension, '$.Type'))) = 'server' and lifecycle_name != 'Decommissioned'";
		int mismatchCount = 0, count = 0, rackCount = 0;
		String insertsql = "insert into enterprise_asset_application_dictionary values(null,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,?,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String assetId = rs.getString("id");
			String assetName = rs.getString("asset_name");
			String location = rs.getString("asset_location_name");
			String ext = rs.getString("extension");
	
			OrderedJSONObject extObj = new OrderedJSONObject(ext);

			// Get App Support Queue & Infra Support Queue
			JSONArray appQueueList = null, infraQueueList = null;
			if(extObj.has("Application Support Queue")) {
				appQueueList = (JSONArray)extObj.get("Application Support Queue");
			} else {
				appQueueList = new JSONArray();
			}
			if(extObj.has("Infra Support Queue")) {
				infraQueueList = (JSONArray)extObj.get("Infra Support Queue");
			} else {
				infraQueueList = new JSONArray();
			}

			// Check if this asset already exists
			String checksql = "select id, app_name, extension from enterprise_asset_application_dictionary where asset_name=?";
			int id = 0;
			String appName = null, dictext = null;
			PreparedStatement pst1 = conn.prepareStatement(checksql);
			pst1.setString(1, assetName);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				id = rs1.getInt("id");
				appName = rs1.getString("app_name");
				dictext = rs1.getString("extension");
			}
			rs1.close();
			pst1.close();

			//System.out.println(assetName);
			if(assetName.startsWith("11")) {
				rackCount++;
				continue;
				//assetName = assetName.substring(assetName.indexOf("-")+1, assetName.indexOf("."));
			} else if(!assetName.toLowerCase().startsWith("dc")) {
				mismatchCount++;
				if(id == 0) {
					OrderedJSONObject dictextObj = new OrderedJSONObject();
					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, assetName);
					pst1.setString(2, "");
					pst1.setString(3, "");
					pst1.setString(4, "");
					pst1.setString(5, "");
					pst1.setString(6, "");
					pst1.setString(7, "");
					pst1.setString(8, "N");
					pst1.setString(9, "Y");
					pst1.setString(10, dictextObj.toString());
					pst1.setString(11, "support@smarterd.com");
					pst1.setString(12, "support@smarterd.com");
					pst1.executeUpdate();
					pst1.close();
				}
				continue;
			}

			// Set App Index
			int appIndex = 5, locEndIndex = 3;

			// Remove last numeric values from name
			String str = "";
			String ch = assetName.substring(assetName.length()-1);
			if(onlyDigits(ch)) {
				ch = assetName.substring(assetName.length()-2);
				if(ch.indexOf("-") != -1) {
					ch = ch.replace("-", "");
				}
				if(onlyDigits(ch)) {
					str = assetName.substring(0, assetName.length()-2);
				} else {
					str = assetName.substring(0,assetName.length()-1);
				}
			} else {
				str = assetName;
			}
			//System.out.println(assetName+":"+str);

			// Get location from server name
			location = str.substring(0,locEndIndex).toUpperCase();
			if(location.equalsIgnoreCase("DC2")) {
				location = "Chaska DC";		// 10.96
			} else if(location.equalsIgnoreCase("DC4")) {
				location = "Ashburn-OCI";	// 10.102
			} else if(location.equalsIgnoreCase("DC5")) {
				location = "Pheonix-OCI";	
			} else if(location.equalsIgnoreCase("DC6")) {
				location = "Duluth";
			}

			// Get Environment
			String env = str.substring(locEndIndex,locEndIndex+1).toUpperCase();
			//System.out.println(str+":"+env);
			if(!(env.equalsIgnoreCase("D") || env.equalsIgnoreCase("P") || env.equalsIgnoreCase("T") || 
				 env.equalsIgnoreCase("Q") || env.equalsIgnoreCase("S"))) {
				env = "";
				appIndex--;
			} else if(env.equalsIgnoreCase("D")) {
				env = "DEV";
			} else if(env.equalsIgnoreCase("P")) {
				env = "PROD";
			} else if(env.equalsIgnoreCase("T")) {
				env = "TEST";
			} else if(env.equalsIgnoreCase("Q")) {
				env = "UAT";
			} else if(env.equalsIgnoreCase("S")) {
				env = "SIT";
			} else {
				env = "";
			}

			// Get OS
			String os = "";
			if(env.length() > 0) {
				os = str.substring(locEndIndex+1,locEndIndex+2).toUpperCase();
			} else {
				os = str.substring(locEndIndex,locEndIndex+1).toUpperCase();
			}
			if(!(os.equalsIgnoreCase("L") || os.equalsIgnoreCase("W"))) {
				os = "";
				appIndex--;
			}
			if(os.equalsIgnoreCase("L")) {
				os = "Linux";
			} else if(os.equalsIgnoreCase("W")) {
				os = "Windows";
			} else {
				os = "";
			}

			// Get App Code
			String appCode = "", category = "";
			int index = 0;
			if(str.lastIndexOf("APP") != -1) {
				index = str.lastIndexOf("APP");
			} else if(str.lastIndexOf("app") != -1) {
				index = str.lastIndexOf("app");
			} else if(str.lastIndexOf("App") != -1) {
				index = str.lastIndexOf("App");
			} else if(str.lastIndexOf("DB") != -1) {
				index = str.lastIndexOf("DB");
			} else if(str.lastIndexOf("db") != -1) {
				index = str.lastIndexOf("db");
			} else if(str.lastIndexOf("DMZ") != -1) {
				index = str.lastIndexOf("DMZ");
			} else if(str.lastIndexOf("dmz") != -1) {
				index = str.lastIndexOf("dmz");
			}
			//System.out.println(str+":"+index);
			if(index > 0) {
				if(os.length() > 0) {
					appCode = str.substring(appIndex,index);
				} else {
					appCode = str.substring(appIndex,index);
				}
				category = str.substring(index);
			} else {
				if(os.length() > 0) {
					appCode = str.substring(appIndex);
				} else {
					appCode = str.substring(appIndex);
				}
			}
			appCode = appCode.replace("-","").toUpperCase();
			if(appCode.length() > 0) {
				ch = appCode.substring(appCode.length()-1);
				if(onlyDigits(ch)) {
					appCode = appCode.substring(0,appCode.length()-1);
				}
			}
			category = category.toUpperCase();
			if(category.indexOf("-") != -1) {
				category = category.substring(0, category.indexOf("-"));
			}
			if(category.length() > 0) {
				ch = category.substring(category.length()-1);
				if(onlyDigits(ch)) {
					category = category.substring(0,category.length()-1);
				}
			}
			System.out.println(assetName+":"+str+":"+location+":"+env+":"+os+":"+appCode+":"+category);

			if(id == 0) {
				OrderedJSONObject dictextObj = new OrderedJSONObject();
				dictextObj.put("app_support_queue", appQueueList);
				dictextObj.put("infra_support_queue", appQueueList);
				dictextObj.put("Ecosystem",new JSONArray());
				dictextObj.put("SLA", "");
				pst1 = conn.prepareStatement(insertsql);
				pst1.setString(1, assetName);
				pst1.setString(2, location);
				pst1.setString(3, env);
				pst1.setString(4, os);
				pst1.setString(5, appCode);
				pst1.setString(6, "");
				pst1.setString(7, category);
				pst1.setString(8, "Y");
				pst1.setString(9, "Y");
				pst1.setString(10, dictextObj.toString());
				pst1.setString(11, "support@smarterd.com");
				pst1.setString(12, "support@smarterd.com");
				pst1.executeUpdate();
				pst1.close();
			} else {
				OrderedJSONObject dictextObj = null;
				if(dictext != null && dictext.length() > 0) {
					try {
						dictextObj = new OrderedJSONObject(dictext);
					} catch(Exception ex) {
						dictextObj = new OrderedJSONObject();
					}
				} else {
					dictextObj = new OrderedJSONObject();
				}
				if(appQueueList.size() > 0) {
					dictextObj.put("app_support_queue", appQueueList);
				}
				if(infraQueueList.size() > 0) {
					dictextObj.put("infra_support_queue", appQueueList);
				}

				// Get App Details if App is attached
				if(appName != null && appName.length() > 0) {
					String appId = null, sla = null, criticality = null, ecosystem = null, appQueue = null, infraQueue = null;
					String appsql = "select id, json_unquote(json_extract(extension, '$.\"SLA Level\"')) sla, json_unquote(json_extract(extension, '$.Criticality')) criticality, json_extract(extension, '$.Ecosystem') ecosystem, json_extract(extension, '$.\"Application Support Queue\"') app_queue, json_extract(extension, '$.\"Infra Support Queue\"') infra_queue from enterprise_asset where asset_name = ? and asset_category = 'APPLICATION'";
					pst1 = conn.prepareStatement(appsql);
					pst1.setString(1, appName);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						appId = rs1.getString("id");
						sla = rs1.getString("sla");
						criticality = rs1.getString("criticality");
						appQueue = rs1.getString("app_queue");
						infraQueue = rs1.getString("infra_queue");
						ecosystem = rs1.getString("ecosystem");
					}
					rs1.close();
					pst1.close();

					if(appId != null) {
						dictextObj.put("asset_id",appId);
						if(appQueue != null && appQueue.length() > 0 && appQueueList.size() == 0) {
							appQueueList = new JSONArray(appQueue);
							dictextObj.put("app_support_queue", appQueueList);
						}
						if(infraQueue != null && infraQueue.length() > 0 && infraQueueList.size() == 0) {
							infraQueueList = new JSONArray(infraQueue);
							dictextObj.put("infra_support_queue", infraQueueList);
						}
						if(ecosystem != null && ecosystem.length() > 0) {
							JSONArray ecoArr = new JSONArray(ecosystem);
							dictextObj.put("Ecosystem",new JSONArray());
						}
						if(sla != null && sla.length() > 0) {
							if(sla.indexOf(" ") != -1) {
								sla = sla.substring(0, sla.indexOf(" ")).trim();
							}
							dictextObj.put("sla",sla);
						} else if(criticality != null && criticality.length() >0) {
							if(criticality.equalsIgnoreCase("critical")) {
								sla = "Gold";
							} else if(criticality.equalsIgnoreCase("high")) {
								sla = "Silver";
							} else if(criticality.equalsIgnoreCase("medium")) {
								sla = "Bronze";
							}  
							dictextObj.put("sla",sla);
						}
					}
				}
				
				String updatesql = "update enterprise_asset_application_dictionary set loc=?,env=?,os=?,app_code=?,category=?,extension=? where id = ?";
				pst1 = conn.prepareStatement(updatesql);
				pst1.setString(1, location);
				pst1.setString(2, env);
				pst1.setString(3, os);
				pst1.setString(4, appCode);
				pst1.setString(5, category);
				pst1.setString(6, dictextObj.toString());
				pst1.setInt(7, id);
				pst1.executeUpdate();
				pst1.close();
			}

			// Update App Code and Category in Asset Classification 
			JSONArray cObj = new JSONArray();
			if(appCode != null && appCode.length() > 0) {
				cObj.add(appCode);
			}
			if(category != null && category.length() > 0) {
				cObj.add(category);
			}
			if(cObj.size() > 0){
				extObj.put("Classification", cObj);
				String updatesql = "update enterprise_asset set extension=? where id = ?";
				pst1 = conn.prepareStatement(updatesql);
				pst1.setString(1, extObj.toString());
				pst1.setString(2, assetId);
				pst1.executeUpdate();
				pst1.close();
			}

			count++;
		}
		rs.close();
		pst.close();

		conn.commit();
		conn.close();
		System.out.println("Matched Count:"+count+" Unmatched Count:"+mismatchCount);
	}

	private void loadModelToTypeMapping() throws Exception {
		String[] row;
		int rowCount = 0;
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String mfg="", model="", type="", subtype="", notes="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						mfg = row[i].trim();
					} else if(i==1) {
						model = row[i].trim();;
					} else if(i==2) {
						type = row[i].trim();;
					} else if(i==3) {
						subtype = row[i].trim();
					} else if(i==4) {
						notes = row[i].trim();
					}
				}

				if(type == null) {
					type = "";
				}
				if(subtype == null) {
					subtype = "";
				}

				String insertsql = "insert into SE.enterprise_device_model(id,model,vendor,type,sub_type,company_code,notes,extension,status) values(null,?,?,?,?,?,'',?,?)";
				OrderedJSONObject extObj = new OrderedJSONObject();
				PreparedStatement pst = conn.prepareStatement(insertsql);
				pst.setString(1, model);
				pst.setString(2, mfg);
				pst.setString(3, type);
				pst.setString(4, subtype);
				pst.setString(5, companyCode);
				pst.setString(6, extObj.toString());
				pst.setString(7, "Active");
				pst.executeUpdate();
				pst.close();
			}
		}
		conn.commit();
		conn.close();
	}

	private void updateVulnerabilityStatus() throws Exception {
		JSONArray vulnrList = new JSONArray();
		String[] row;
		int rowCount = 0, totalVulnrCount = 0;
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String scanId="", workflowId="", workflowState="", firstIngestedDate="", lastIngestedDate="", priority="", scanPeriod="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						scanId = row[i].trim();
					} else if(i==1) {
						workflowId = row[i].trim();;
					} else if(i==2) {
						workflowState = row[i].trim();;
					} else if(i==3) {
						firstIngestedDate = row[i].trim();
					} else if(i==4) {
						lastIngestedDate = row[i].trim();
					} else if(i==5) {
						priority = row[i].trim();
					} else if(i==6) {
						scanPeriod = row[i].trim();
					}
				}
				System.out.println(firstIngestedDate+":"+lastIngestedDate);
				firstIngestedDate = formatDate(firstIngestedDate);
				lastIngestedDate = formatDate(lastIngestedDate);

				// Check sql
				String id = null, ext=null;
				String checksql = "select _id, b.extension extension from enterprise_vulnerability_log a, enterprise_vulnerability b where a.vulnerability_id=b._id and json_extract(a.extension, '$.THREAT.scan_id') = ? and json_extract(a.extension,'$.scan_period')='NOV-22'";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, scanId);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("_id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				System.out.println(scanId+":"+id+":"+firstIngestedDate+":"+lastIngestedDate+":"+scanPeriod);

				if(id != null && !vulnrList.contains(id)) {
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("First Ingested On", firstIngestedDate);
					extObj.put("Last Ingested On", lastIngestedDate);
					String updatesql = "update enterprise_vulnerability set extension = ? where _id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, extObj.toString());
					pst.setString(2, id);
					pst.executeUpdate();
					pst.close();

					/*
					String updatesql = "update enterprise_vulnerability_log set status = ?,priority=? where json_extract(extension, '$.THREAT.scan_id') = ?";
					OrderedJSONObject extObj = new OrderedJSONObject();
					PreparedStatement pst = conn.prepareStatement(updatesql);
					pst.setString(1, status);
					pst.setString(2, priority);
					pst.setString(3, scanId);
					pst.executeUpdate();
					pst.close();
					*/
					vulnrList.add(id);
				}
			}
		}
		conn.commit();
		conn.close();
		System.out.println("Total Vulnr Updated:"+vulnrList.size());
	}
	
	private boolean isAlphaNumeric(String s) {
        return s != null && s.matches("^[a-zA-Z0-9]*$");
    }

	private boolean onlyDigits(String str) {
        // Traverse the string from start to end
		int n = str.length();
        for (int i = 0; i < n; i++) {
            // Check if character is
            // not a digit between 0-9
            // then return false
            if (str.charAt(i) < '0' || str.charAt(i) > '9') {
                return false;
            }
        }
		// If we reach here, that means
		// all characters were digits.
        return true;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());
		extObj.put("IP Address", new JSONArray());
		extObj.put("Private IP Address", new JSONArray());
		extObj.put("Public IP Address", new JSONArray());
		extObj.put("Scan IP Address", new JSONArray());
		extObj.put("Floating IP Address", new JSONArray());

        return extObj;
    }

	private void generateNIST171Score() throws Exception {
		String nist171score = "3.1.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.5;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.7;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.8;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.10;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.11;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.12;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.13;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.14;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.15;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.16;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.17;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.18;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.19;score penalty: 3;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.20;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.21;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.1.22;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.2.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.2.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.2.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.2;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.5;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.7;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.8;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.3.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.5;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.6;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.7;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.8;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.4.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.3;score penalty: 3-5;?;FULLY IMPLEMENTED;;IMPLEMENTED FOR REMOTE;;NOT FULLY IMPLEMENTED;3.5.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.5;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.7;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.8;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.10;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.5.11;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.6.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.6.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.6.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.1;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.4;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.5;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.7.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.1;score penalty: 3;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.2;score penalty: 3;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.3;score penalty: 5;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.5;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.7;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.8;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.8.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.9.1;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.9.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.5;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.10.6;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.11.1;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.11.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.11.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.12.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.12.2;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.12.3;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.12.4;score penalty: N/A;?;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.3;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.4;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.5;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.6;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.7;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.8;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.9;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.10;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.11;score penalty: 3-5;?;FULLY IMPLEMENTED;;IMPLEMENTED WITHOUT FIPS;;NOT FULLY IMPLEMENTED;3.13.12;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.13;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.14;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.15;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.13.16;score penalty: 1;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.1;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.2;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.3;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.4;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.5;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.6;score penalty: 5;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED;3.14.7;score penalty: 3;FULLY IMPLEMENTED;;NOT FULLY IMPLEMENTED";
		OrderedJSONObject nist171scoreObj = new OrderedJSONObject();
		
	}

	// Missed loading Objectives due to some issue
	// This is used to load the Objectives to existing assessment 
	private void loadObjectivesToAssessment() throws Exception {
		String assessmentId = "05496675-9057-11ed-8898-0ae85b2c5055";
		String domain = "CMMC_V2_L2";
		String assessmentsql = "select control_id from enterprise_assessment_detail where assessment_id = ? and control_level = 2";
		String controlobjectivesql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions,json_extract(business_capability_ext, '$.custom.\"Services Offered\"') services_offered from enterprise_business_capability where business_capability_level = 3 and parent_capability_id = ? and business_capability_domain = ?";
		String assessmentdetailinsertsql = "insert into enterprise_assessment_detail (id, assessment_id, control_id, parent_control_id, control_family, control_level, status, severity, effort, artifacts, notes, extension, created_timestamp, created_by, updated_timestamp, updated_by) values(uuid(),?,?,?,?,?,'','','',?,?,?,current_timestamp,?,current_timestamp,?)";
		String checkassessmentcontroldetailsql = "select id, extension from enterprise_assessment_detail where assessment_id=? and control_id = ?";
		int rowCount = 0;
		PreparedStatement pst = conn.prepareStatement(assessmentsql);
		pst.setString(1, assessmentId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String controlId = rs.getString("control_id");
			System.out.println(controlId);
			PreparedStatement pst1 = conn.prepareStatement(controlobjectivesql);
			pst1.setString(1, controlId);
			pst1.setString(2, domain);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String objectiveId = rs1.getString("id");
				String parentId = rs1.getString("parent_capability_id");
				String internalId = rs1.getString("internal_id");
				String familyName = rs1.getString("family");
				String parentName = rs1.getString("parent_capability_name");
				String childName = rs1.getString("child_capability_name");
				String currentMaturity = rs1.getString("current_maturity");
				String level = rs1.getString("business_capability_level");
				String score = rs1.getString("weighted_score");
				String notes = rs1.getString("business_capability_comment");
				String artifacts = rs1.getString("artifacts");
				String nistMapping = rs1.getString("nist_mapping");
				String standard = rs1.getString("standard");
				String questions = rs1.getString("questions");
				String applicableQuestions = rs1.getString("applicable_questions");
				String requirements = rs1.getString("requirements");
				String services = rs1.getString("services_offered");
				if(score == null || score.length() == 0) {
					score = "";
				}

				if(standard == null) {
					standard = "";
				}

				JSONArray qList = null;
				if(questions != null && questions.length() > 0) {
					qList = new JSONArray(questions);
				} else {
					qList = new JSONArray();
				}

				OrderedJSONObject aqList = null;
				if(applicableQuestions != null && applicableQuestions.length() > 0) {
					aqList = new OrderedJSONObject(applicableQuestions);
				} else {
					aqList = new OrderedJSONObject();
				}

				// Get Parent Questions
				JSONArray parentQuestionList = getParentQuestionList(parentId);
				if(aqList.size() == 0) {    // Add all parent questions, if aqList is empty
					qList.addAll(parentQuestionList);
				} else {
					Iterator iter = aqList.keySet().iterator();
					while(iter.hasNext()) {
						String qId = (String)iter.next();
						String aFlag = (String)aqList.get(qId);
						if(aFlag == null || aFlag.length() == 0 || aFlag.equalsIgnoreCase("Y")) {
							Iterator parentIter = parentQuestionList.iterator();
							while(parentIter.hasNext()) {
								OrderedJSONObject qObj = (OrderedJSONObject)parentIter.next();
								String pqId = (String)qObj.get("id");
								if(pqId.equalsIgnoreCase(qId)) {
									qList.add(qObj);
								}
							}
						}
					}
				}
				JSONArray servicesList = null;
				if(services != null && services.length() > 0) {
					servicesList = new JSONArray(services);
				} else {
					servicesList = new JSONArray();
				}

				OrderedJSONObject extObj = null;
				String detailId = null, detailExt = null;
				PreparedStatement pst2 = conn.prepareStatement(checkassessmentcontroldetailsql);
				pst2.setString(1, assessmentId);
				pst2.setString(2, objectiveId);
				ResultSet rs2 = pst2.executeQuery();
				while(rs2.next()) {
					detailId = rs2.getString("id");
					detailExt = rs2.getString("extension");
				}
				rs2.close();
				pst2.close();

				if(detailId == null) {
					System.out.println("Inserting..."+childName+":"+parentName);
					extObj = new OrderedJSONObject();
					extObj.put("internal_id", internalId);
					extObj.put("weighted_score", score);
					extObj.put("parent_control_name", parentName);
					extObj.put("control_name", childName);
					extObj.put("nist_mapping", nistMapping);
					extObj.put("current_maturity", currentMaturity);
					extObj.put("standard", standard);
					extObj.put("questions", qList);
					extObj.put("services_offered", servicesList);

					PreparedStatement pst3 = conn.prepareStatement(assessmentdetailinsertsql);
					pst3.setString(1, assessmentId);
					pst3.setString(2, objectiveId);
					pst3.setString(3, parentId);
					pst3.setString(4, familyName);
					pst3.setString(5, level);
					pst3.setString(6, notes);
					pst3.setString(7, artifacts);
					pst3.setString(8, extObj.toString());
					pst3.setString(9, "support@smarterd.com");
					pst3.setString(10, "support@smarterd.com");
					pst3.executeUpdate();
					pst3.close();
					rowCount++;
				}
			}
			rs1.close();
			pst1.close();

		}
		rs.close();
		pst.close();
		conn.commit();
		System.out.println("Successfully inserted Objectives:"+rowCount);
	}

	private JSONArray getParentQuestionList(String id) throws Exception {
        String parentquestionsql = "select parent_capability_id, json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions from enterprise_business_capability where id = ? and business_capability_level > 0";
        JSONArray qList = new JSONArray();
        String parentId = null;
        PreparedStatement pst = conn.prepareStatement(parentquestionsql);
        pst.setString(1, id);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            parentId = rs.getString("parent_capability_id");
            String questions = rs.getString("questions");
            if(questions != null && questions.length() > 0) {
                JSONArray list = new JSONArray(questions);
                qList.addAll(list);
            }
        }
        rs.close();
        pst.close();

        if(parentId != null && parentId.length() > 0) {
            JSONArray list = getParentQuestionList(parentId);
            qList.addAll(list);
        }

        return qList;
    }

	// Compare unique Vulnerability Log asset IP with Asset IP
	private void validateVulnrLogIPWithAsset() throws Exception {
		String vulnrlogsql = "select distinct ipaddress,hostname from enterprise_vulnerability_log where json_extract(extension, '$.scan_period') = 'NOV-22'";
		String assetsql = "select id, asset_name from enterprise_asset where json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"', '$') > 0";
		String vulnrassetsql = "select id, asset_name, json_unquote(json_extract(extension, '$.sync')) sync from enterprise_vulnerability_asset where json_contains(json_extract(extension, '$.\"IP Address\"'), '\"REPLACE_STRING\"', '$') > 0";
		int totalVulnrCount = 0, missingAssetCount=0, missingBothAssetCount=0;
		PreparedStatement pst = conn.prepareStatement(vulnrlogsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String ip = rs.getString("ipaddress");
			String hostname = rs.getString("hostname");

			// Check Asset Table
			String assetId=null,assetName=null;
			String sql = assetsql.replaceAll("REPLACE_STRING", ip);
			PreparedStatement pst1 = conn.prepareStatement(sql);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				assetId = rs1.getString("id");
				assetName = rs1.getString("asset_name");
			}
			rs1.close();
			pst1.close();
			if(assetId != null) {
				//System.out.println("Asset Found:"+assetId+":"+assetName);
			} else {
				// Check if it is in Vulner Asset Table
				String vulnrAssetId=null,vulnrAssetName=null,sync=null;
				sql = vulnrassetsql.replaceAll("REPLACE_STRING", ip);
				pst1 = conn.prepareStatement(sql);
				rs1 = pst1.executeQuery();
				while(rs1.next()) {
					vulnrAssetId = rs1.getString("id");
					vulnrAssetName = rs1.getString("asset_name");
					sync = rs1.getString("sync");
				}
				rs1.close();
				pst1.close();

				if(vulnrAssetId != null) {
					missingAssetCount++;
					System.out.println("Missing Asset:"+ip+":"+vulnrAssetName+":"+sync);
				} else {
					System.out.println("IP Not Found:"+ip+":"+hostname);
					missingBothAssetCount++;
				}
			}
			totalVulnrCount++;
		}
		rs.close();
		pst.close();
		conn.close();
		System.out.println("Total Count:"+totalVulnrCount+" Missing Asset Count:"+missingAssetCount+" Missing Both Count:"+missingBothAssetCount);
	}

	private void categorizeVulnerabilities() throws Exception {
		//String testVulnr = "Oracle Java SE Multiple Vulnerabilities (July 2015 CPU) (Unix) (Bar Mitzvah)";
		//String scanPeriod = "NOV-22";
		String vulnrsql = "select _id, nameId, json_unquote(json_extract(extension, '$.enterprise_action')) possible_solution from enterprise_vulnerability where (json_extract(extension, '$.\"Vulnerability Type\"') is null or json_extract(extension, '$.\"Vulnerability Type\"') = '') and priority != 'Info'";
		String vulnrlogsql = "select asset_name, ipaddress from enterprise_vulnerability_log where vulnerability_name = ? and json_extract(extension, '$.scan_period') = ?";
		OrderedJSONObject actionList = initializeVulnerabilityAction();
		OrderedJSONObject typeList = initializeVulnerabilityTypes();
		PreparedStatement pst = conn.prepareStatement(vulnrsql);
		//pst.setString(1, testVulnr);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String name = rs.getString("nameId");
			String solution = rs.getString("possible_solution");
			//System.out.println(name);
			//System.out.println(solution);
			
			// Classify Mitigation Action
			String action = "";
			if(solution != null && solution.length() > 0 && !solution.equalsIgnoreCase("N/A")) {
				boolean found = false;
				Iterator iter = actionList.keySet().iterator();
				while(iter.hasNext()) {
					String key = (String)iter.next();
					action = (String)actionList.get(key);
					if(solution.toLowerCase().indexOf(key) != -1) {
						//System.out.println("Action:"+action);
						found = true;
						break;
					}
				}

				if(!found) {
					System.out.println("Did not find match:"+name);
				}
			}

			// Classify Type
			String type = "";
			if(!name.startsWith("CVE")) {
				boolean found = false;
				Iterator iter = typeList.keySet().iterator();
				while(iter.hasNext()) {
					String key = (String)iter.next();
					type = (String)typeList.get(key);
					if(name.toLowerCase().indexOf(key) != -1) {
						//System.out.println("Action:"+action);
						found = true;
						break;
					}
				}

				if(!found) {
					System.out.println("Did not match type:"+name);
				}
			}

			if(type == null) {
				type = "";
			}
			if(action == null) {
				action = "";
			}
			if(type.length() == 0 || action.length() == 0) {
				//System.out.println(name+":"+type+":"+action);
			}

			// Update Vulnerability
			String updatesql = "update enterprise_vulnerability set extension=json_set(extension,'$.mitigationStatus', ?), extension=json_set(extension, '$.\"Vulnerability Type\"',?) where _id=?";
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, action);
			pst1.setString(2, type);
			pst1.setString(3, id);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();

		conn.commit();
		conn.close();
	}

	private OrderedJSONObject initializeVulnerabilityTypes() throws Exception {
		OrderedJSONObject typeList = new OrderedJSONObject();
		typeList.put("rhel", "OS");
		typeList.put("unix", "OS");
		typeList.put("java", "Application");
		typeList.put("weblogic", "Application");
		typeList.put("log4j", "Application");
		typeList.put("apache", "Application");
		typeList.put("tomcat", "Application");
		typeList.put("web server", "Application");
		typeList.put("Web Application", "Application");
		typeList.put("jquery", "Application");
		typeList.put("sql injection", "Application");
		typeList.put("coherence", "Application");
		typeList.put("application", "Application");
		typeList.put("http", "Application");
		typeList.put("ssh", "Encryption");
		typeList.put("ssl", "Encryption");
		typeList.put("tls", "Encryption");
		typeList.put("nfs", "Storage");
		typeList.put("smb", "Storage");
		typeList.put("directories", "Storage");
		typeList.put("backup", "Storage");
		typeList.put("files", "Storage");
		typeList.put("esxi", "Storage");
		typeList.put("terminal service", "System Admin");
		typeList.put("zabbix", "System Admin");
		typeList.put("deep web", "System Admin");
		typeList.put("snmp", "System Admin");
		typeList.put("dns", "System Admin");
		typeList.put("telnet", "System Admin");
		typeList.put("ntp", "System Admin");
		typeList.put("rsh", "System Admin");
		typeList.put("splunk", "System Admin");
		typeList.put("x server", "System Admin");
		typeList.put("display", "System Admin");
		typeList.put("desktop", "System Admin");
		typeList.put("rlogin", "System Admin");
		typeList.put("ipmi", "System Admin");
		typeList.put("elmah", "System Admin");
		typeList.put("solarwinds", "System Admin");
		typeList.put("jenkins", "System Admin");
		typeList.put("printer", "System Admin");
		typeList.put("vmware", "Network");
		typeList.put("f5", "Network");
		typeList.put("database", "Database");
		typeList.put("sql server", "Database");
		typeList.put("tns", "Database");
		typeList.put("airport", "Network");
		typeList.put("traffic", "Network");
		
		return typeList;
	}

	private OrderedJSONObject initializeVulnerabilityAction() throws Exception {
		OrderedJSONObject actionList = new OrderedJSONObject();
		actionList.put("patch", "Patch");
		actionList.put("upgrade", "Upgrade");
		actionList.put("upgrading", "Upgrade");
		actionList.put("update", "Upgrade");
		actionList.put("updating", "Upgrade");
		actionList.put("disable", "Configuration");
		actionList.put("disabling", "Configuration");
		actionList.put("enable", "Configuration");
		actionList.put("enabling", "Configuration");
		actionList.put("purchase", "Configuration");
		actionList.put("generate", "Configuration");
		actionList.put("configure", "Configuration");
		actionList.put("reconfigure", "Configuration");
		actionList.put("make sure", "Configuration");
		actionList.put("return", "Configuration");
		actionList.put("prevent", "Configuration");
		actionList.put("enforce", "Configuration");
		actionList.put("place", "Configuration");
		actionList.put("apply", "Configuration");
		actionList.put("workaround", "Configuration");
		actionList.put("restrict", "Configuration");
		actionList.put("remove", "Configuration");
		actionList.put("force", "Configuration");
		actionList.put("allow", "Configuration");
		actionList.put("replace", "Configuration");	
		actionList.put("deny", "Configuration");	
		actionList.put("limit", "Configuration");	
		actionList.put("encode", "Configuration");	
		actionList.put("escape", "Configuration");	
		actionList.put("encoding", "Configuration");
		actionList.put("escaping", "Configuration");	
		actionList.put("fix", "Configuration");
		actionList.put("ensure", "Configuration");
		actionList.put("delete", "Configuration");
		actionList.put("protect", "Configuration");
		actionList.put("change", "Configuration");
		actionList.put("reset", "Configuration");
		actionList.put("reissue", "Configuration");
		actionList.put("filter", "Configuration");

		return actionList;
	}

	private void updateIncidentTime() throws Exception {
		String sql = "select _id, incidentTime, resolutionTime from enterprise_incident";
		String updatesql = "update enterprise_incident set incidentTime=?, resolutionTime=? where _id = ?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String incidentTime = rs.getString("incidentTime").trim();
			String resolutionTime = rs.getString("resolutionTime");
			
			incidentTime = formatIncidentTime(incidentTime);

			if(resolutionTime != null && resolutionTime.length() > 0) {
				resolutionTime = formatIncidentTime(resolutionTime.trim());
			}

			pst1.setString(1, incidentTime);
			pst1.setString(2, resolutionTime);
			pst1.setString(3, id);
			pst1.addBatch();

			System.out.println(incidentTime+":"+resolutionTime);
		} 
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();

		conn.commit();
		conn.close();
	}

	private String formatIncidentTime(String incidentTime) throws Exception {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		DateTimeFormatter df2 = DateTimeFormatter.ofPattern("MM/dd/yy HH:mm");
		DateTimeFormatter df3 = DateTimeFormatter.ofPattern("M/dd/yy HH:mm");
		DateTimeFormatter df4 = DateTimeFormatter.ofPattern("M/dd/yy H:mm");
		DateTimeFormatter df5 = DateTimeFormatter.ofPattern("MM/d/yy HH:mm");
		DateTimeFormatter df6 = DateTimeFormatter.ofPattern("M/d/yy HH:mm");
		DateTimeFormatter df7 = DateTimeFormatter.ofPattern("M/d/yy H:mm");

		if(incidentTime.indexOf("-") != -1) {
			LocalDateTime dt = LocalDateTime.parse(incidentTime, df1);
			incidentTime = df.format(dt);
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 14) {
			LocalDateTime dt = LocalDateTime.parse(incidentTime, df2);
			incidentTime = df.format(dt);
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 13) {
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df3);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df5);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 12) {
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df4);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df6);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 11) {
			LocalDateTime dt = LocalDateTime.parse(incidentTime, df7);
			incidentTime = df.format(dt);
		}

		return incidentTime;
	}

	private void updateVulnerabilityLogWithAssetDetail() throws Exception {
		String id = null, assetId = null, ext=null;
		String logsql = "select id, asset_id, extension extension from enterprise_vulnerability_log a where json_extract(a.extension,'$.scan_period')='NOV-22'";
		String assetsql = "select lifecycle_name,asset_location_name,owner,json_unquote(json_extract(extension,'$.\"Manager\"')) manager, json_extract(extension,'$.\"Technical Owner\"') tech_owner,json_extract(extension,'$.\"Classification\"') classification, json_unquote(json_extract(extension,'$.\"Type\"')) type,json_unquote(json_extract(extension,'$.\"Sub-Type\"')) subtype,json_unquote(json_extract(extension,'$.\"OS\"')) os,json_unquote(json_extract(extension,'$.\"Environment\"')) env,json_unquote(json_extract(extension,'$.\"OEM Vendor\"')) make,json_unquote(json_extract(extension,'$.\"Model\"')) model,json_extract(extension,'$.\"Application Support Queue\"') app_queue,json_extract(extension,'$.\"Infra Support Queue\"') infra_queue from enterprise_asset where id = ?";
		String updatelogsql = "update enterprise_vulnerability_log set extension = ? where id = ?";
		PreparedStatement pst = conn.prepareStatement(logsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
			assetId = rs.getString("asset_id");
			ext = rs.getString("extension");
			OrderedJSONObject extObj = new OrderedJSONObject(ext);
			System.out.println(id);

			// Get Asset Detail
			PreparedStatement pst1 = conn.prepareStatement(assetsql);
			pst1.setString(1, assetId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String lc = rs1.getString("lifecycle_name");
				String loc = rs1.getString("asset_location_name");
				String type = rs1.getString("type");
				String subtype = rs1.getString("subtype");
				String owner = rs1.getString("owner");
				String manager = rs1.getString("manager");
				String techOwner = rs1.getString("tech_owner");
				String classification = rs1.getString("classification");
				String os = rs1.getString("os");
				String env = rs1.getString("env");
				String vendor = rs1.getString("make");
				String model = rs1.getString("model");
				String appQueue = rs1.getString("app_queue");
				String infraQueue = rs1.getString("infra_queue");

				if(type == null) {
					type = "";
				}
				if(subtype == null) {
					subtype = "";
				}
				if(lc == null) {
					lc = "";
				}
				if(loc == null) {
					loc = "";
				}
				if(owner == null) {
					owner = "";
				}
				if(manager == null) {
					manager = "";
				}
				JSONArray techOwnerList = null; 
				if(techOwner != null && techOwner.length() > 0) {
					techOwnerList = new JSONArray(techOwner);
				} else {
					techOwnerList = new JSONArray();
				}
				JSONArray classificationList = null; 
				if(classification != null && classification.length() > 0) {
					classificationList = new JSONArray(classification);
				} else {
					classificationList = new JSONArray();
				}
				if(os == null) {
					os = "";
				}
				if(env == null) {
					env = "";
				}
				if(model == null) {
					model = "";
				}
				if(vendor == null) {
					vendor = "";
				}
				
				JSONArray appQueueList = null; 
				if(appQueue != null && appQueue.length() > 0) {
					appQueueList = new JSONArray(appQueue);
				} else {
					appQueueList = new JSONArray();
				}
				JSONArray infraQueueList = null; 
				if(infraQueue != null && infraQueue.length() > 0) {
					infraQueueList = new JSONArray(infraQueue);
				} else {
					infraQueueList = new JSONArray();
				}

				// Initialize Asset Object
				OrderedJSONObject assetObj = new OrderedJSONObject();
				assetObj.put("type", type);
				assetObj.put("sub_type", subtype);
				assetObj.put("lifecycle", lc);
				assetObj.put("location", loc);
				assetObj.put("owner", owner);
				assetObj.put("manager", manager);
				assetObj.put("technical_owner", techOwnerList);
				assetObj.put("environment", env);
				assetObj.put("os", os);
				assetObj.put("vendor", vendor);
				assetObj.put("model", model);
				assetObj.put("classification", classificationList);
				assetObj.put("infra_queue", infraQueueList);
				assetObj.put("app_queue", appQueueList);

				PreparedStatement pst2 = conn.prepareStatement(updatelogsql);
				extObj.put("ASSET", assetObj);
				pst2.setString(1, extObj.toString());
				pst2.setString(2, id);
				pst2.executeUpdate();
				pst2.close();
			}
			rs1.close();
			pst1.close();
		}
		rs.close();
		pst.close();	
		conn.commit();
	}

	private void validateHACLAServerLoadFile() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		int rowCount = 0;
		CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		
		List headerList = dataReader.getHeaderNames();
		int i = 0;
		Iterator iter = headerList.iterator();
		while(iter.hasNext()) {
			String columnName = (String)iter.next();
			System.out.println(i+":"+columnName);
			i++;
		}
		for(CSVRecord dataRow : dataReader) {
			rowCount++;
			for(i=0; i<dataRow.size(); i++) {
				System.out.println(i+":"+dataRow.get(i).trim());
			}
			if(rowCount == 1) {
				break;
			}
		}
	}

	private void initializeAssetCompliance() throws Exception {
		String assetsql = "select id, asset_name, json_unquote(json_extract(extension, '$.Type')) type, json_unquote(json_extract(extension, '$.\"Sub-Type\"')) subtype, json_unquote(json_extract(extension, '$.OS')) os, json_unquote(json_extract(extension, '$.Environment')) env, asset_location_name, lifecycle_name,json_unquote(json_extract(extension, '$.\"VM Mgr\"')) vm_mgr,json_unquote(json_extract(extension, '$.\"Endpoint Mgr\"')) endpoint_mgr,json_unquote(json_extract(extension, '$.Endpoint')) endpoint, json_extract(extension, '$.\"IP Address\"') ip from enterprise_asset where asset_category = 'IT ASSET' and lower(json_unquote(json_extract(extension, '$.Type'))) in ('server', 'network device', 'storage device', 'ip address') and lifecycle_name != 'Decommissioned' union select id, asset_name, json_unquote(json_extract(extension, '$.Type')) type, json_unquote(json_extract(extension, '$.\"Sub-Type\"')) subtype, json_unquote(json_extract(extension, '$.OS')) os, json_unquote(json_extract(extension, '$.Environment')) env, asset_location_name, lifecycle_name,json_unquote(json_extract(extension, '$.\"VM Mgr\"')) vm_mgr,json_unquote(json_extract(extension, '$.\"Endpoint Mgr\"')) endpoint_mgr,json_unquote(json_extract(extension, '$.Endpoint')) endpoint, json_extract(extension, '$.\"IP Address\"') ip from enterprise_asset where asset_category = 'IT ASSET' and lower(json_unquote(json_extract(extension, '$.Type'))) = 'workstation' and lower(json_unquote(json_extract(extension, '$.\"Sub-Type\"'))) in ('persistent vdi','non-persistent vdi') and lifecycle_name != 'Decommissioned' order by asset_location_name,type,asset_name";
		String insertsql = "insert into enterprise_asset_compliance(id,asset_id,asset_name,ip_address,type,subtype,location,env,lifecycle,os,internal,vm_mgr,endpoint_manager,endpoint_agent,log_agent,db_agent,app_agent,vulnr_agent,extension) values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String deletesql = "delete from enterprise_asset_compliance";
		
		// Clear Compliance Table
		PreparedStatement pst = conn.prepareStatement(deletesql);
		pst.executeUpdate();
		pst.close();

		// Check all Servers
		pst = conn.prepareStatement(assetsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String assetId = rs.getString("id");
			String assetName =  rs.getString("asset_name");
			String type =  rs.getString("type");
			String subtype =  rs.getString("subtype");
			String loc =  rs.getString("asset_location_name");
			String env =  rs.getString("env");
			String lc = rs.getString("lifecycle_name");
			String os =  rs.getString("os");
			String endpointMgr = rs.getString("endpoint_mgr");
			String endpoint = rs.getString("endpoint");
			String vmMgr = rs.getString("vm_mgr");
			String ip = rs.getString("ip");
			
			JSONArray ipList = new JSONArray();
			if(ip != null && ip.length() > 0) {
				JSONArray ips = new JSONArray(ip);
				Iterator ipIter = ips.iterator();
				while(ipIter.hasNext()) {
					String str = (String)ipIter.next();
					if(!(str.matches("^.*[a-zA-Z]+.*$") && ipList.contains(str))) {
						ipList.add(str);
					}
				}
			}
			if(loc == null) {
				loc = "";
			}
			System.out.println(os);

			// Check Vulnerability Log
			// Use Scan Period
			int vulnrCount = 0;
			String vulnrsql = "select count(*) count from enterprise_vulnerability_log where asset_name = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
			PreparedStatement pst1 = conn.prepareStatement(vulnrsql);
			pst1.setString(1, assetName);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				vulnrCount = rs1.getInt("count");
			}
			rs1.close();
			pst1.close();

			pst1 = conn.prepareStatement(insertsql);
			pst1.setString(1, assetId);
			pst1.setString(2, assetName);
			pst1.setString(3, ipList.toString());
			pst1.setString(4, type.toUpperCase());
			pst1.setString(5, subtype.toUpperCase());
			pst1.setString(6, loc);
			pst1.setString(7, env);
			pst1.setString(8, lc);
			pst1.setString(9, os);
			pst1.setString(10, "Y");
	
			if(vmMgr != null && vmMgr.length() > 0) {
				if(vmMgr.equalsIgnoreCase("Yes")) {
					pst1.setString(10, "N");
					pst1.setString(11, "Y");
				} else if(vmMgr.equalsIgnoreCase("No")) {
					pst1.setString(11, "N");
				}
			} else {
				if(subtype.equalsIgnoreCase("virtual server") || subtype.toLowerCase().indexOf("vdi") != -1) {
					pst1.setString(11, "N");
				} else {
					pst1.setString(11, "");
				}
			}
			if(endpointMgr != null && endpointMgr.length() > 0) {
				if(endpointMgr.equalsIgnoreCase("Yes")) {
					pst1.setString(10, "N");
					pst1.setString(12, "Y");
				} else if(endpointMgr.equalsIgnoreCase("No")) {
					pst1.setString(12, "N");
				}
			} else {
				if(type.equalsIgnoreCase("workstation") || type.equalsIgnoreCase("mobile device")) {
					pst1.setString(12, "N");
				} else {
					pst1.setString(12, "");
				}
			}
			if(endpoint != null && endpoint.length() > 0) {
				if(endpoint.equalsIgnoreCase("Yes")) {
					pst1.setString(10, "N");
					pst1.setString(13, "Y");
				} else if(endpoint.equalsIgnoreCase("No")) {
					pst1.setString(13, "N");
				}
			} else {
				if(type.equalsIgnoreCase("server")) {
					pst1.setString(13, "N");
				} else {
					pst1.setString(13, "");
				}
			}
			pst1.setString(14, "");	// log_agent
			pst1.setString(15, "");	// db_agent
			pst1.setString(16, "");	// app_agent

			if(vulnrCount > 0) {
				pst1.setString(17, "Y");
			} else {
				pst1.setString(17, "N");
			}
			OrderedJSONObject extObj = new OrderedJSONObject();
			pst1.setString(18, extObj.toString());
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();

		// Process Vulnerability Discovered Asset that are not syned
		pst = conn.prepareStatement(assetsql);
		rs = pst.executeQuery();
		String vulnrassetsql = "select id, asset_name, json_extract(extension, '$.\"IP Address\"') ip from enterprise_vulnerability_asset where json_extract(extension, '$.sync') = 'N' and asset_category = 'IT ASSET'";
		while(rs.next()) {
			String assetId = rs.getString("id");
			String assetName =  rs.getString("asset_name");
			String ip = rs.getString("ip");
			
			JSONArray ipList = null;
			if(ip != null && ip.length() > 0) {
				ipList = new JSONArray(ip);
			} else {
				ipList = new JSONArray();
				if(assetName.indexOf(".") != -1) {
					ip = assetName;
					ipList.add(ip);
				}
			}

			// Check if this IP Address already exists
			int assetCount = 0;
			if(ipList.size() > 0) {
				ip = (String)ipList.get(0);
			}
			if(ip != null && ip.length() > 0) {
				String assetchecksql = "select count(*) count from enterprise_asset where json_contains(json_unquote(json_extract(extension, '$.\"IP Address\"')), '\"REPLACE_STRING\"') > 0";
				assetchecksql = assetchecksql.replaceAll("REPLACE_STRING", ip);
				PreparedStatement pst1 = conn.prepareStatement(assetchecksql);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					assetCount = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();
			}

			if(assetCount == 0) {
				OrderedJSONObject extObj = new OrderedJSONObject();
				PreparedStatement pst1 = conn.prepareStatement(insertsql);
				pst1.setString(1, assetId);
				pst1.setString(2, assetName);
				pst1.setString(3, ipList.toString());
				pst1.setString(4, "UNKNOWN");
				pst1.setString(5, "UNKNOWN");
				pst1.setString(6, "");
				pst1.setString(7, "");
				pst1.setString(8, "");
				pst1.setString(9, "");
				pst1.setString(10, "N");
				pst1.setString(11, "");		// VM Mgr
				pst1.setString(12, "");		// Endpoint Mgr
				pst1.setString(13, "");		// Endpoint
				pst1.setString(14, "");		// log_agent
				pst1.setString(15, "");		// db_agent
				pst1.setString(16, "");		// app_agent
				pst1.setString(17, "Y");	// vulnr_agent 
				pst1.setString(18, extObj.toString());
				pst1.executeUpdate();
				pst1.close();
			}
		}
		rs.close();
		pst.close();

		// Process all devices
		String devicesql = "select b.id id, a.asset_name asset_name, location, json_extract(a.extension, '$.\"IP Address\"') ip, json_unquote(json_extract(b.extension, '$.\"Type\"')) type,json_unquote(json_extract(b.extension, '$.\"Sub-Type\"')) subtype,json_unquote(json_extract(a.extension, '$.\"Endpoint Mgr\"')) endpoint_mgr, json_unquote(json_extract(a.extension, '$.\"Endpoint\"')) endpoint,case when (json_extract(a.extension,'$.\"Last check-in\"') is not null or json_extract(a.extension,'$.\"Last check-in\"') != '') then datediff(curdate(),str_to_date(json_unquote(json_extract(a.extension,'$.\"Last check-in\"')), '%m/%d/%Y')) else 0 end days_last_reported from enterprise_device a, enterprise_asset b where a.group_id=b.id and b.lifecycle_name != 'Decommissioned' order by type";
		pst = conn.prepareStatement(devicesql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String assetId = rs.getString("id");
			String assetName =  rs.getString("asset_name");
			String ip = rs.getString("ip");
			String loc = rs.getString("location");
			String type = rs.getString("type");
			String subtype = rs.getString("subtype");
			String endpointMgr = rs.getString("endpoint_mgr");
			String endpoint = rs.getString("endpoint");
			int days = rs.getInt("days_last_reported");
			
			JSONArray ipList = new JSONArray();
			if(ip != null && ip.length() > 0) {
				JSONArray ips = new JSONArray(ip);
				Iterator ipIter = ips.iterator();
				while(ipIter.hasNext()) {
					String str = (String)ipIter.next();
					if(!(str.matches("^.*[a-zA-Z]+.*$") && ipList.contains(str))) {
						ipList.add(str);
					}
				}
			} else {
				if(assetName.indexOf(".") != -1) {
					ip = assetName;
					ipList.add(ip);
				}
			}

			if(subtype == null) {
				subtype = "";
			}

			if(endpointMgr != null && endpointMgr.length() > 0) {
				if(endpointMgr.equalsIgnoreCase("Yes")) {
					endpointMgr = "Y";
				} else {
					endpointMgr = "N";
				}
			} else{
				endpointMgr = "";
			}
			if(endpoint != null && endpoint.length() > 0) {
				if(endpoint.equalsIgnoreCase("Yes")) {
					endpoint = "Y";
				} else {
					endpoint = "N";
				}
			} else{
				endpoint = "";
			}

			if(subtype.equalsIgnoreCase("LAPTOP") || subtype.equalsIgnoreCase("DESKTOP") || subtype.equalsIgnoreCase("POS") || 
			   subtype.equalsIgnoreCase("TABLET") || subtype.equalsIgnoreCase("PHYSICAL SERVER")) {
				if(endpoint == null || endpoint.length() == 0) {
					endpoint = "N";
				}
			}

			// Check Vulnerability Log
			// Use Scan Period
			int vulnrCount = 0;
			String vulnrsql = "select count(*) count from enterprise_vulnerability_log where ipaddress = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
			PreparedStatement pst1 = conn.prepareStatement(vulnrsql);
			pst1.setString(1, ip);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				vulnrCount = rs1.getInt("count");
			}
			rs1.close();
			pst1.close();

			/*
			String type = "";
			if(subtype.equalsIgnoreCase("LAPTOP") || subtype.equalsIgnoreCase("DESKTOP") || subtype.equalsIgnoreCase("POS")) {
				type = "WORKSTATION";	
			} else if(subtype.equalsIgnoreCase("TABLET")) {
				type = "MOBILE DEVICE";
			} else if(subtype.equalsIgnoreCase("PHYSICAL SERVER")) {
				type = "SERVER";
			}
			*/

			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("days_last_reported", new Integer(days));

			pst1 = conn.prepareStatement(insertsql);
			pst1.setString(1, assetId);
			pst1.setString(2, assetName);
			pst1.setString(3, ipList.toString());
			pst1.setString(4, type.toUpperCase());
			pst1.setString(5, subtype.toUpperCase());
			pst1.setString(6, "");
			pst1.setString(7, "");
			pst1.setString(8, "");
			pst1.setString(9, "");
			if(type.equalsIgnoreCase("NETWORK DEVICE")) {
				pst1.setString(10, "Y");
			} else {
				pst1.setString(10, "N");
			}
			pst1.setString(11, "N");
			pst1.setString(12, endpointMgr);
			pst1.setString(13, endpoint);
			pst1.setString(14, "");	// log_agent
			pst1.setString(15, "");	// db_agent
			pst1.setString(16, "");	// app_agent
			if(vulnrCount > 0) {	// vulnr_agent
				pst1.setString(17, "Y");
			} else {
				pst1.setString(17, "N");
			}
			pst1.setString(18, extObj.toString());
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();

		conn.commit();
	}

	private void syncMLIncident() throws Exception {
		String sql = "select id, name from enterprise_ml_result";
		String incidentsql = "select ticketRef from enterprise_incident where lower(json_unquote(json_extract(extension, '$.Category'))) = ? and lower(json_unquote(json_extract(extension, '$.\"Sub Category\"'))) = ? and json_extract(extension, '$.service_type') = 'INCIDENT'";
		String mlincidentsql = "insert into enterprise_ml_incident(problem_id,ticketRef,extension) values(?, ?, json_object())";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String name = rs.getString("name");

			String category = name.substring(0, name.indexOf(" ")).trim();
			String subcategory = name.substring(name.indexOf(" ")+1);
			System.out.println(category+":"+subcategory);
			
			PreparedStatement pst2 = conn.prepareStatement(mlincidentsql);
			PreparedStatement pst1 = conn.prepareStatement(incidentsql);
			pst1.setString(1, category.toLowerCase());
			pst1.setString(2, subcategory.toLowerCase());
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String ticketRef = rs1.getString("ticketRef");
				pst2.setString(1, id);
				pst2.setString(2, ticketRef);
				pst2.addBatch();
			}
			rs1.close();
			pst1.close();

			pst2.executeBatch();
			pst2.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateMLIncidentCategory() throws Exception {
		String sql = "select id, component_category_list,linked_component_list from enterprise_ml_result";
		String catsql = "select distinct json_unquote(json_extract(extension, '$.Category')) cat from enterprise_incident";
		String subcatsql = "select distinct json_unquote(json_extract(extension, '$.\"Sub Category\"')) subcat from enterprise_incident";
		String updatesql = "update enterprise_ml_result set component_category_list=?, linked_component_list=? where id=?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		JSONArray catList = new JSONArray();
		JSONArray subcatList = new JSONArray();

		PreparedStatement pst = conn.prepareStatement(catsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String cat = rs.getString("cat");
			catList.add(cat);
		}
		rs.close();
		pst.close();
		System.out.println(catList);
		pst = conn.prepareStatement(subcatsql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String subcat = rs.getString("subcat");
			subcatList.add(subcat);
		}
		rs.close();
		pst.close();
		
		pst = conn.prepareStatement(sql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String category = rs.getString("component_category_list");
			String subcategory = rs.getString("linked_component_list");
			
			category = category.replaceAll("[\\[\\]]","").replaceAll("\'","");
			subcategory = subcategory.replaceAll("[\\[\\]]","").replaceAll("\'","");
			System.out.println(category+":"+subcategory);

			JSONArray newcatList = new JSONArray();
			Iterator iter = catList.iterator();
			while(iter.hasNext()) {
				String cat = (String)iter.next();
				if(cat.equalsIgnoreCase(category)) {
					System.out.println("Cat Matched");
					newcatList.add(cat);
					break;
				}
			}

			JSONArray newsubcatList = new JSONArray();
			iter = subcatList.iterator();
			while(iter.hasNext()) {
				String subcat = (String)iter.next();
				if(subcat.equalsIgnoreCase(subcategory)) {
					System.out.println("SubCat Matched");
					newsubcatList.add(subcat);
					break;
				}
			}
			pst1.setString(1, newcatList.toString());
			pst1.setString(2, newsubcatList.toString());
			pst1.setString(3, id);
			pst1.addBatch();
			System.out.println(newcatList+":"+newsubcatList);
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateMLIncidentTokenList() throws Exception {
		String sql = "select id, token_list from enterprise_ml_result";
		String catsql = "select distinct json_unquote(json_extract(extension, '$.Category')) cat from enterprise_incident";
		String subcatsql = "select distinct json_unquote(json_extract(extension, '$.\"Sub Category\"')) subcat from enterprise_incident";
		String updatesql = "update enterprise_ml_result set token_list=? where id=?";
		PreparedStatement pst1 = conn.prepareStatement(updatesql);

		JSONArray catList = new JSONArray();
		JSONArray subcatList = new JSONArray();

		PreparedStatement pst = conn.prepareStatement(catsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String cat = rs.getString("cat");
			catList.add(cat);
		}
		rs.close();
		pst.close();
		System.out.println(catList);
		pst = conn.prepareStatement(subcatsql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String subcat = rs.getString("subcat");
			subcatList.add(subcat);
		}
		rs.close();
		pst.close();
		
		pst = conn.prepareStatement(sql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String tokens = rs.getString("token_list");

			JSONArray tokenList = new JSONArray(tokens);
			JSONArray newtokenList = new JSONArray();

			JSONArray newcatList = new JSONArray();
			Iterator iter = catList.iterator();
			while(iter.hasNext()) {
				String cat = (String)iter.next();
				Iterator iter1 = tokenList.iterator();
				while(iter1.hasNext()) {
					String token = (String)iter1.next();
					if(cat.equalsIgnoreCase(token)) {
						System.out.println("Token Matched");
						if(!newtokenList.contains(cat)) {
							newtokenList.add(cat);
						}
						break;
					}
				}
			}

			JSONArray newsubcatList = new JSONArray();
			iter = subcatList.iterator();
			while(iter.hasNext()) {
				String subcat = (String)iter.next();
				Iterator iter1 = tokenList.iterator();
				while(iter1.hasNext()) {
					String token = (String)iter1.next();
					if(subcat.equalsIgnoreCase(token)) {
						System.out.println("SubCat Matched");
						if(!newtokenList.contains(subcat)) {
							newtokenList.add(subcat);
						}
						break;
					}
				}
			}
			System.out.println(newtokenList);
			pst1.setString(1, newtokenList.toString());
			pst1.setString(2, id);
			pst1.addBatch();
			
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateIncidentSettingFromData(String field, String settingCategory, String settingType, String fieldType) throws Exception {
		String sql = null;
		if(fieldType.equals("EXT")) {
			sql = "select distinct json_unquote(json_extract(extension, '$.\""+field+"\"')) value from enterprise_incident";
		} else {
			sql = "select distinct "+field+" value from enterprise_incident";
		}
		//String subcatsql = "select distinct json_unquote(json_extract(extension, '$.\"Sub Category\"')) subcat from enterprise_incident";
		String checksql = "select count(*) count from enterprise_setting where setting_name = ? and setting_category = ?";
		String insertsql = "insert into enterprise_setting(setting_name,setting_category,setting_type,company_code,updated_by,extension) values(?,?,?,?,'support@smarterd.com',json_object())";
		PreparedStatement pst1 = conn.prepareStatement(insertsql);

		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String value = rs.getString("value");
			if(value != null && value.trim().length() > 0) {
				int count = 0;
				PreparedStatement pst2 = conn.prepareStatement(checksql);
				pst2.setString(1, value);
				pst2.setString(2, settingCategory);
				ResultSet rs2 = pst2.executeQuery();
				while(rs2.next()) {
					count = rs2.getInt("count");
				}
				rs2.close();
				pst2.close();

				if(count == 0) {
					pst1.setString(1, value);
					pst1.setString(2, settingCategory);
					pst1.setString(3, settingType);
					pst1.setString(4, companyCode);
					pst1.addBatch();
				}
			}
		}
		rs.close();
		pst.close();
		
		/*
		pst = conn.prepareStatement(subcatsql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String subcat = rs.getString("subcat");
			int count = 0;
			PreparedStatement pst2 = conn.prepareStatement(checksql);
			pst2.setString(1, subcat);
			pst2.setString(2, "Incident Sub Category");
			ResultSet rs2 = pst2.executeQuery();
			while(rs2.next()) {
				count = rs2.getInt("count");
			}
			rs2.close();
			pst2.close();

			if(count == 0) {
				pst1.setString(1, subcat);
				pst1.setString(2, "Incident Sub Category");
				pst1.addBatch();
			}
		}
		rs.close();
		*/

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void updateAssignmentGroupSetting() throws Exception {
		String insertsql = "insert into enterprise_setting(setting_name,setting_category,setting_value,setting_type,company_code,updated_by,extension) values(?,?,?,'SECURITY','DHA','support@smarterd.com',json_object())";
		String checksql = "select count(*) count from enterprise_setting where setting_name = ? and setting_category = ?";
		PreparedStatement pst1 = conn.prepareStatement(insertsql);

		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		int rowCount = 0;
		CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		
		for(CSVRecord dataRow : dataReader) {
			rowCount++;
			String ag = "", as = "";
			for(int i=0; i<dataRow.size(); i++) {
				if(i == 0) {
					ag = dataRow.get(i).trim();
				} else if(i == 1) {
					as = dataRow.get(i).trim();
				} 
			}

			int count = 0;
			PreparedStatement pst = conn.prepareStatement(checksql);
			pst.setString(1, ag);
			pst.setString(2, "Incident Group");
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				pst1.setString(1, ag);
				pst1.setString(2, "Incident Group");
				pst1.setString(3, as);
				pst1.addBatch();
			}

			/*
			count = 0;
			pst = conn.prepareStatement(checksql);
			pst.setString(1, as);
			pst.setString(2, "Incident Section");
			rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				pst1.setString(1, as);
				pst1.setString(2, "Incident Section");
				pst1.addBatch();
			}
			*/
		}

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	private void formatStoreNetworkInvetoryFile() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		FileWriter fw = new FileWriter("store_network_inventory.csv");
		CSVPrinter printer = new CSVPrinter(fw,  CSVFormat.DEFAULT);
		printer.printRecord("Store No.", "State", "Wan Type", "Device Type", "Asset Name", "IP Address");

		int rowCount = 0;
		String[] row;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			System.out.println(rowCount);
			if (rowCount > 1) {
				String storeNo = "", state = "", routerName="", routerIp="", wanType="", switch1="",switch1Ip="",switch2="",switch2Ip="";
				String ap1="",ap1Ip="",ap2="",ap2Ip="",ap3="",ap3Ip="",voip="",voipIp=""; 
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						storeNo = row[i];	
					} else if(i==1) {		
						state = row[i];	
					} else if(i==2) {		
					} else if(i==3) {		
					} else if(i==4) {		
						routerName = row[i];	
					} else if(i==5) {		
						routerIp = row[i];	
					} else if(i==6) {	
						wanType = row[i];	
					} else if(i==7) {		
					} else if(i==8) {	
					} else if(i==9) {		
					} else if(i==10) {	
						switch1 = row[i];	
					} else if(i==11) {	
						switch1Ip = row[i];	
					} else if(i==12) {	
						switch2 = row[i];	
					} else if(i==13) {	
						switch2Ip = row[i];	
					} else if(i==14) {	
						ap1 = row[i];	
					} else if(i==15) {	
						ap1Ip = row[i];	
					} else if(i==16) {	
						ap2 = row[i];	
					} else if(i==17) {	
						ap2Ip = row[i];	
					} else if(i==18) {	
						ap3 = row[i];	
					} else if(i==19) {	
						ap3Ip = row[i];	
					} else if(i==20) {	
						voip = row[i];	
					} else if(i==21) {	
						voipIp = row[i];	
					} 
				}
				String record = storeNo+","+state+","+wanType+","+"Router"+","+routerName+","+routerIp;
				printer.printRecord(record);
				record = storeNo+","+state+","+wanType+","+"Switch"+","+switch1+","+switch1Ip;
				printer.printRecord(record);
				if(switch2.length() > 0) {
					record = storeNo+","+state+","+wanType+","+"Switch"+","+switch2+","+switch2Ip;
					printer.printRecord(record);
				}
				if(ap1.length() > 0) {
					record = storeNo+","+state+","+wanType+","+"Access Point"+","+ap1+","+ap1Ip;
					printer.printRecord(record);
				}
				if(ap2.length() > 0) {
					record = storeNo+","+state+","+wanType+","+"Access Point"+","+ap2+","+ap2Ip;
					printer.printRecord(record);
				}
				if(ap3.length() > 0) {
					record = storeNo+","+state+","+wanType+","+"Access Point"+","+ap3+","+ap3Ip;
					printer.printRecord(record);
				}
				if(voip.length() > 0) {
					record = storeNo+","+state+","+wanType+","+"VoIP"+","+voip+","+voipIp;
					printer.printRecord(record);
				}
			}
		}

		printer.flush();
		printer.close();
	}

	private void updateVulnerabilityLifecycleStatus() throws Exception {
		String vulnrsql = "select _id from enterprise_vulnerability";
		String vulnrlogsql = "select json_unquote(json_extract(extension, '$.THREAT.status')) status from enterprise_vulnerability_log where vulnerability_id = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
		String updatesql = "update enterprise_vulnerability set extension=json_set(extension, '$.lifecycle', ?) where _id = ?";
		PreparedStatement pst = conn.prepareStatement(vulnrsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("_id");
			String vulnrstatus = "";
			PreparedStatement pst1 = conn.prepareStatement(vulnrlogsql);
			pst1.setString(1, id);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				String status = rs1.getString("status");
				if(status.equalsIgnoreCase("Active") || status.equalsIgnoreCase("Open")) {
					vulnrstatus = "Active";
				} else if(!(vulnrstatus.equalsIgnoreCase("Active") || vulnrstatus.equalsIgnoreCase("Complete")) && (status.equalsIgnoreCase("Complete") || status.equalsIgnoreCase("Risk Acceptance") || status.equalsIgnoreCase("False Positive"))) {
					vulnrstatus = "Complete";
				}
				System.out.println("Status:"+status+":"+vulnrstatus);
			}
			rs1.close();
			pst1.close();
			if(vulnrstatus.length() == 0) {
				vulnrstatus = "Complete";
			}
			System.out.println("Vulnr Status:"+vulnrstatus);
			pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, vulnrstatus);
			pst1.setString(2, id);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateEnterpriseIP() throws Exception {
		String assetsql = "select id, asset_name,lifecycle_name,asset_location_name, json_unquote(json_extract(extension,'$.Type')) type, json_unquote(json_extract(extension,'$.\"Sub-Type\"')) subtype,json_unquote(json_extract(extension,'$.Environment')) env,json_unquote(json_extract(extension,'$.OS')) os,json_extract(extension, '$.\"IP Address\"') ip, json_extract(extension, '$.\"Private IP Address\"') private_ip, json_extract(extension, '$.\"Public IP Address\"') public_ip, json_extract(extension, '$.\"Floating IP Address\"') floating_ip, json_extract(extension, '$.\"Scan IP Address\"') scan_ip from enterprise_asset where asset_category = 'IT ASSET' and (json_extract(extension, '$.isgroupasset') = false or json_extract(extension, '$.isgroupasset') is null)";
		String devicesql = "select id, asset_name, lifecycle, location, json_unquote(json_extract(extension,'$.asset_type')) type, group_id, json_extract(extension, '$.\"IP Address\"') ip from enterprise_device";
		String insertsql = "insert into enterprise_ip(asset_id,group_id,ip_address,ip_type,extension) values(?,?,?,?,?)";
		String checksql = "select count(*) count from enterprise_ip where ip_address = ?";
		
		// Cleanup
		String deletesql = "delete from enterprise_ip";
		PreparedStatement pst = conn.prepareStatement(deletesql);
		pst.executeUpdate();
		pst.close();

		pst = conn.prepareStatement(assetsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String assetName = rs.getString("asset_name");
			String lc = rs.getString("lifecycle_name");
			String loc = rs.getString("asset_location_name");
			String type = rs.getString("type");
			String subtype = rs.getString("subtype");
			String env = rs.getString("env");
			String os = rs.getString("os");
			String totalIP = rs.getString("ip");
			String privateIp = rs.getString("private_ip");
			String publicIp = rs.getString("public_ip");
			String floatIp = rs.getString("floating_ip");
			String scanIp = rs.getString("scan_ip");
			System.out.println(type+":"+assetName+":"+os);

			JSONArray ipList = new JSONArray(), privateIpList = new JSONArray(), publicIpList = new JSONArray(), floatIpList = new JSONArray(), scanIpList = new JSONArray();
			if(privateIp != null && privateIp.length() > 0) {
				try {
					JSONArray arr = new JSONArray(privateIp);
					Iterator iter = arr.iterator();
					while(iter.hasNext()) {
						String str = (String)iter.next();
						if(str.indexOf(".") != -1) {
							str = str.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
							str = str.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
							if(!privateIpList.contains(str)) {
								privateIpList.add(str);
							}
						}
					}
				} catch(Exception ex) {
					if(!privateIpList.contains(privateIp)) {
						privateIpList.add(privateIp);
					}
				}
			}
			if(publicIp != null && publicIp.length() > 0) {
				JSONArray arr = new JSONArray(publicIp);
				Iterator iter = arr.iterator();
				while(iter.hasNext()) {
					String str = (String)iter.next();
					if(str.indexOf(".") != -1) {
						str = str.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
						str = str.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
						if(!publicIpList.contains(str)) {
							publicIpList.add(str);
						}
					}
				}
			} 
			if(floatIp != null && floatIp.length() > 0) {
				JSONArray arr = new JSONArray(floatIp);
				Iterator iter = arr.iterator();
				while(iter.hasNext()) {
					String str = (String)iter.next();
					if(str.indexOf(".") != -1) {
						str = str.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
						str = str.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
						if(!floatIpList.contains(str)) {
							floatIpList.add(str);
						}
					}
				}
			}
			if(scanIp != null && scanIp.length() > 0) {
				JSONArray arr = new JSONArray(scanIp);
				Iterator iter = arr.iterator();
				while(iter.hasNext()) {
					String str = (String)iter.next();
					if(str.indexOf(".") != -1) {
						str = str.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD\\uFEFF]", "");
						str = str.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC\\uFEFF]", "");
						if(!scanIpList.contains(str)) {
							scanIpList.add(str);
						}
					}
				}
			}
			/*
			Iterator iter = privateIpList.iterator();
			while(iter.hasNext()) {
				String addr = (String)iter.next();
				if(!ipList.contains(addr)) {
					ipList.add(addr);
				}
			}
			iter = publicIpList.iterator();
			while(iter.hasNext()) {
				String addr = (String)iter.next();
				if(!ipList.contains(addr)) {
					ipList.add(addr);
				}
			}
			iter = floatIpList.iterator();
			while(iter.hasNext()) {
				String addr = (String)iter.next();
				if(!ipList.contains(addr)) {
					ipList.add(addr);
				}
			}
			iter = scanIpList.iterator();
			while(iter.hasNext()) {
				String addr = (String)iter.next();
				if(!ipList.contains(addr)) {
					ipList.add(addr);
				}
			}
			*/

			// Check if this asses exists
			List wrongIPList = null;
			Iterator iter = privateIpList.iterator();
			while(iter.hasNext()) {
				String ip = (String)iter.next();
				if(ip.indexOf(",") != -1) {	// Wrongly stored IPs
					String[] ips = ip.split(",");
					wrongIPList = Arrays.asList(ips);
					continue;
				} else if(ip.matches("^.*[a-zA-Z]+.*$")) {
					continue;
				}
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, ip);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					System.out.println("Adding IP..."+ip);
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("asset_name", assetName);
					extObj.put("type", type);
					extObj.put("sub_type", subtype);
					extObj.put("lifecycle", lc);
					extObj.put("env", env);
					extObj.put("os", os);
					extObj.put("location", loc);
					extObj.put("source", "VM Mgr");

					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, id);
					pst1.setString(2, "");
					pst1.setString(3, ip);
					pst1.setString(4, "Private");
					pst1.setString(5, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
				}
			}

			if(wrongIPList != null && wrongIPList.size() > 0) {
				iter = wrongIPList.iterator();
				while(iter.hasNext()) {
					String ip = ((String)iter.next()).trim();
					int count = 0;
					PreparedStatement pst1 = conn.prepareStatement(checksql);
					pst1.setString(1, ip);
					ResultSet rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						System.out.println("Adding IP..."+ip);
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("asset_name", assetName);
						extObj.put("type", type);
						extObj.put("sub_type", subtype);
						extObj.put("lifecycle", lc);
						extObj.put("env", env);
						extObj.put("os", os);
						extObj.put("location", loc);
						extObj.put("source", "VM Mgr");

						pst1 = conn.prepareStatement(insertsql);
						pst1.setString(1, id);
						pst1.setString(2, "");
						pst1.setString(3, ip);
						pst1.setString(4, "Private");
						pst1.setString(5, extObj.toString());
						pst1.executeUpdate();
						pst1.close();
					}
				}
			}

			wrongIPList = null;
			iter = publicIpList.iterator();
			while(iter.hasNext()) {
				String ip = (String)iter.next();
				if(ip.indexOf(",") != -1) {	// Wrongly stored IPs
					String[] ips = ip.split(",");
					wrongIPList = Arrays.asList(ips);
					continue;
				} else if(ip.matches("^.*[a-zA-Z]+.*$")) {
					continue;
				}
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, ip);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					System.out.println("Adding IP..."+ip);
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("asset_name", assetName);
					extObj.put("type", type);
					extObj.put("sub_type", subtype);
					extObj.put("lifecycle", lc);
					extObj.put("env", env);
					extObj.put("os", os);
					extObj.put("location", loc);
					extObj.put("source", "VM Mgr");

					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, id);
					pst1.setString(2, "");
					pst1.setString(3, ip);
					pst1.setString(4, "Public");
					pst1.setString(5, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
				}
			}

			if(wrongIPList != null && wrongIPList.size() > 0) {
				iter = wrongIPList.iterator();
				while(iter.hasNext()) {
					String ip = ((String)iter.next()).trim();
					int count = 0;
					PreparedStatement pst1 = conn.prepareStatement(checksql);
					pst1.setString(1, ip);
					ResultSet rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						System.out.println("Adding IP..."+ip);
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("asset_name", assetName);
						extObj.put("type", type);
						extObj.put("sub_type", subtype);
						extObj.put("lifecycle", lc);
						extObj.put("env", env);
						extObj.put("os", os);
						extObj.put("location", loc);
						extObj.put("source", "VM Mgr");

						pst1 = conn.prepareStatement(insertsql);
						pst1.setString(1, id);
						pst1.setString(2, "");
						pst1.setString(3, ip);
						pst1.setString(4, "Private");
						pst1.setString(5, extObj.toString());
						pst1.executeUpdate();
						pst1.close();
					}
				}
			}

			wrongIPList = null;
			iter = floatIpList.iterator();
			while(iter.hasNext()) {
				String ip = (String)iter.next();
				if(ip.indexOf(",") != -1) {	// Wrongly stored IPs
					String[] ips = ip.split(",");
					wrongIPList = Arrays.asList(ips);
					continue;
				} else if(ip.matches("^.*[a-zA-Z]+.*$")) {
					continue;
				}
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, ip);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					System.out.println("Adding IP..."+ip);
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("asset_name", assetName);
					extObj.put("type", type);
					extObj.put("sub_type", subtype);
					extObj.put("lifecycle", lc);
					extObj.put("env", env);
					extObj.put("os", os);
					extObj.put("location", loc);
					extObj.put("source", "VM Mgr");

					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, id);
					pst1.setString(2, "");
					pst1.setString(3, ip);
					pst1.setString(4, "Floating");
					pst1.setString(5, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
				}
			}
			if(wrongIPList != null && wrongIPList.size() > 0) {
				iter = wrongIPList.iterator();
				while(iter.hasNext()) {
					String ip = ((String)iter.next()).trim();
					int count = 0;
					PreparedStatement pst1 = conn.prepareStatement(checksql);
					pst1.setString(1, ip);
					ResultSet rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						System.out.println("Adding IP..."+ip);
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("asset_name", assetName);
						extObj.put("type", type);
						extObj.put("sub_type", subtype);
						extObj.put("lifecycle", lc);
						extObj.put("env", env);
						extObj.put("os", os);
						extObj.put("location", loc);
						extObj.put("source", "VM Mgr");

						pst1 = conn.prepareStatement(insertsql);
						pst1.setString(1, id);
						pst1.setString(2, "");
						pst1.setString(3, ip);
						pst1.setString(4, "Private");
						pst1.setString(5, extObj.toString());
						pst1.executeUpdate();
						pst1.close();
					}
				}
			}

			wrongIPList = null;
			iter = scanIpList.iterator();
			while(iter.hasNext()) {
				String ip = (String)iter.next();
				if(ip.indexOf(",") != -1) {	// Wrongly stored IPs
					String[] ips = ip.split(",");
					wrongIPList = Arrays.asList(ips);
					continue;
				} else if(ip.matches("^.*[a-zA-Z]+.*$")) {
					continue;
				}
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, ip);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					System.out.println("Adding IP..."+ip);
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("asset_name", assetName);
					extObj.put("type", type);
					extObj.put("sub_type", subtype);
					extObj.put("lifecycle", lc);
					extObj.put("env", env);
					extObj.put("os", os);
					extObj.put("location", loc);
					extObj.put("source", "VM Mgr");

					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, id);
					pst1.setString(2, "");
					pst1.setString(3, ip);
					pst1.setString(4, "Scan");
					pst1.setString(5, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
				}
			}
			if(wrongIPList != null && wrongIPList.size() > 0) {
				iter = wrongIPList.iterator();
				while(iter.hasNext()) {
					String ip = ((String)iter.next()).trim();
					int count = 0;
					PreparedStatement pst1 = conn.prepareStatement(checksql);
					pst1.setString(1, ip);
					ResultSet rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						System.out.println("Adding IP..."+ip);
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("asset_name", assetName);
						extObj.put("type", type);
						extObj.put("sub_type", subtype);
						extObj.put("lifecycle", lc);
						extObj.put("env", env);
						extObj.put("os", os);
						extObj.put("location", loc);
						extObj.put("source", "VM Mgr");

						pst1 = conn.prepareStatement(insertsql);
						pst1.setString(1, id);
						pst1.setString(2, "");
						pst1.setString(3, ip);
						pst1.setString(4, "Private");
						pst1.setString(5, extObj.toString());
						pst1.executeUpdate();
						pst1.close();
					}
				}
			}
		}
		rs.close();
		pst.close();

		// Load Devices
		pst = conn.prepareStatement(devicesql);
		rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String groupId = rs.getString("group_id");
			String assetName = rs.getString("asset_name");
			String type = rs.getString("type");
			String lc = rs.getString("lifecycle");
			String loc = rs.getString("location");
			String ipAddr = rs.getString("ip");

			// Get Sub-Type, Env and OS from Group Asset
			String groupsubtype = "", groupenv = "", groupos = "";
			String groupassetsql = "select json_unquote(json_extract(extension,'$.\"Sub-Type\"')) subtype,json_unquote(json_extract(extension,'$.Environment')) env,json_unquote(json_extract(extension,'$.OS')) os from enterprise_asset where id = ?";
			PreparedStatement pst1 = conn.prepareStatement(groupassetsql);
			pst1.setString(1, groupId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				groupsubtype = rs1.getString("subtype");
				groupenv = rs1.getString("env");
				groupos = rs1.getString("os");
			}
			rs1.close();
			pst1.close();

			if(groupsubtype == null) {
				groupsubtype = "";
			}
			if(groupenv == null) {
				groupenv = "";
			}
			if(groupos == null) {
				groupos = "";
			}

			JSONArray ipList = null;
			if(ipAddr != null && ipAddr.length() > 0) {
				ipList = new JSONArray(ipAddr);
			} else {
				ipList = new JSONArray();
			}

			// Check if this assets exists
			Iterator iter = ipList.iterator();
			while(iter.hasNext()) {
				String ip = (String)iter.next();
				int count = 0;
				pst1 = conn.prepareStatement(checksql);
				pst1.setString(1, ip);
				rs1 = pst1.executeQuery();
				while(rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					System.out.println("Adding Device..."+assetName);
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("asset_name", assetName);
					extObj.put("type", type);
					extObj.put("sub_type", groupsubtype);
					extObj.put("lifecycle", lc);
					extObj.put("env", groupenv);
					extObj.put("os", groupos);
					extObj.put("location", loc);
					extObj.put("source", "Endpoint Mgr");
					pst1 = conn.prepareStatement(insertsql);
					pst1.setString(1, id);
					pst1.setString(2, groupId);
					pst1.setString(3, ip);
					pst1.setString(4, "Private");
					pst1.setString(5, extObj.toString());
					pst1.executeUpdate();
					pst1.close();
				}
			}
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateMLCategory() throws Exception {
		String sql = "select id, component_category_list, linked_component_list, token_list from enterprise_ml_result";
		String updatesql = "update enterprise_ml_result set component_category_list=?, linked_component_list=?, token_list=? where id = ?";
		String settingsql = "select setting_name from enterprise_setting where lower(setting_name) = lower(?) and setting_category = 'Incident Sub Category'";

		WordUtils wordUtils = new WordUtils();

		PreparedStatement pst1 = conn.prepareStatement(updatesql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String category = rs.getString("component_category_list");
			String subcategory = rs.getString("linked_component_list");
			//String token = rs.getString("token_list");

			JSONArray tokenList = new JSONArray();
			JSONArray catList = new JSONArray(category);
			JSONArray subcatList = new JSONArray(subcategory);

			JSONArray newcatList = new JSONArray();
			Iterator iter = catList.iterator();
			while(iter.hasNext()) {
				String cat = (String)iter.next();
				if(cat.equalsIgnoreCase("cdc") || cat.equalsIgnoreCase("grp")) {
					cat = cat.toUpperCase();
				} else {
					cat = wordUtils.capitalize(cat);
				}
				//System.out.println(cat);
				newcatList.add(cat);
				tokenList.add(cat);
			}

			JSONArray newsubcatList = new JSONArray();
			iter = subcatList.iterator();
			while(iter.hasNext()) {
				String subcat = (String)iter.next();
				PreparedStatement pst2 = conn.prepareStatement(settingsql);
				pst2.setString(1, subcat);
				ResultSet rs2 = pst2.executeQuery();
				while(rs2.next()) {
					subcat = rs2.getString("setting_name");
					System.out.println(subcat);
					newsubcatList.add(subcat);
					tokenList.add(subcat);
				}
				rs2.close();
				pst2.close();
			}

			pst1.setString(1, newcatList.toString());
			pst1.setString(2, newsubcatList.toString());
			pst1.setString(3, tokenList.toString());
			pst1.setString(4, id);
			pst1.addBatch();
		}
		rs.close();
		pst.close();

		pst1.executeBatch();
		pst1.close();
		conn.commit();
	}

	public void updateHACLACategory() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);

		int rowCount = 0;
		String[] row;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			//System.out.println(rowCount);
			if (rowCount > 1) {
				String ticketRef = "", category = "", subcategory = "", assignmentGroup = "";
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						ticketRef = row[i];	
					} else if(i==1) {		
						String str = row[i];
						//if(str.indexOf("Service") != -1) {
							System.out.println(str);
							String[] tokens = str.split(":");
							if(tokens.length == 4) {
								for(int j=0; j<tokens.length; j++) {
									if(j == 0) {
										assignmentGroup = tokens[j].trim();
									} else if(j == 1) {
									} else if(j == 2) {
										category = tokens[j].trim();
									} else if(j == 3) {
										subcategory = tokens[j].trim();
									} 
								}
							} else if(tokens.length == 3) {
								for(int j=0; j<tokens.length; j++) {
									if(j == 0) {
										assignmentGroup = tokens[j].trim();
									} else if(j == 1) {
										category = tokens[j].trim();
									} else if(j == 2) {
										subcategory = tokens[j].trim();
									} 
								}
								if(assignmentGroup.equalsIgnoreCase("IMAC") || assignmentGroup.equalsIgnoreCase("Service")) {
									String temp = category;
									category = subcategory;
									subcategory = temp;
								} else if(assignmentGroup.equalsIgnoreCase("Applications")) {
									//category = subcategory;
									//subcategory = "";
								}
							} else if(tokens.length == 2) {
								for(int j=0; j<tokens.length; j++) {
									if(j == 0) {
										assignmentGroup = tokens[j].trim();
									} else if(j == 1) {
										category = tokens[j].trim();
									}
								}
								
								if(assignmentGroup.equalsIgnoreCase("Erroneous Tickets")) {
									subcategory = category;
									category = assignmentGroup;
									assignmentGroup = "Error";
								} 

								if(category.equalsIgnoreCase("Configuration/Reconfiguration") || category.equalsIgnoreCase("Unresponsive/Frozen/Lags") || 
									category.equalsIgnoreCase("Error Code or Condition") || category.equalsIgnoreCase("Slowness") || 
									category.equalsIgnoreCase("Connection Problem") || category.equalsIgnoreCase("Upgrade/Enhancement to Infrastructure") ||
									category.equalsIgnoreCase("Data Extraction/Recovery") || category.equalsIgnoreCase("Badge Activation/Reactivation/Deactivation") ||
									category.equalsIgnoreCase("Deployment/Migration") || category.equalsIgnoreCase("Report/Query Request") || 
									category.equalsIgnoreCase("Cannot Login") || category.equalsIgnoreCase("Bulk or Test Account Creations") || 
									category.equalsIgnoreCase("Conference Room Configuration/Set-up") || category.equalsIgnoreCase("Maintenance/System Enhancement") ||
									category.equalsIgnoreCase("Employee Transfer") || category.equalsIgnoreCase("Create Account") || 
									category.equalsIgnoreCase("Remote Access") || category.equalsIgnoreCase("Installation Request") ||
									category.indexOf("User Home Folder") != -1 || category.equalsIgnoreCase("Feed Problem") ||
									category.equalsIgnoreCase("Boot Issue") || category.equalsIgnoreCase("Font Installation") ||
									category.equalsIgnoreCase("Install/Update/Replace Certificate") || category.equalsIgnoreCase("Bulk or Scheduled Move/Relocation Request") ||
									category.equalsIgnoreCase("Equipment Pickup Request") || category.equalsIgnoreCase("Announcement Request") ||
									category.equalsIgnoreCase("Conference Bridge") || category.equalsIgnoreCase("Set-up Request") || category.equalsIgnoreCase("Testing") ||
									category.equalsIgnoreCase("Upgrade/Enhancement to Infrastructure") || category.equalsIgnoreCase("Data Extraction/Recovery") || 
									category.equalsIgnoreCase("Missing/Unavailable") || category.equalsIgnoreCase("Power Supply") || category.equalsIgnoreCase("Data Management") || 
									category.equalsIgnoreCase("Deployment/Migration") || category.equalsIgnoreCase("Maintenance/System Enhancement")) {
									subcategory = category;
									category = "Unknown";
								} else if(category.equalsIgnoreCase("Toner/Ink Replacement") || category.equalsIgnoreCase("Paper Jam") || category.equalsIgnoreCase("Low Toner")) {
									subcategory = category;
									category = "Printers/Copiers";
								} else if(category.equalsIgnoreCase("Display Issue, Single Monitor")) {
									subcategory = category;
									category = "Monitor";
								} else if(category.equalsIgnoreCase("Replacement, Keyboard/Mouse Set (USB)") || category.equalsIgnoreCase("Replacement, Keyboard (USB)")) {
									subcategory = category;
									category = "Keyboard";
								} else if(category.equalsIgnoreCase("CD/DVD Playback")) {
									subcategory = category;
									category = "CD/DVD";
								} else if(category.equalsIgnoreCase("TV Configuration")) {
									subcategory = category;
									category = "TV";
								}
							} else {
								category = "Default";
								subcategory = "Default";
								assignmentGroup = "Default";
							}
						//}
					} else {
						break;
					}
				}	
				if(subcategory.indexOf(",") != -1) {
					subcategory = subcategory.substring(0, subcategory.indexOf(",")).trim();
				}
				System.out.println(ticketRef+":"+assignmentGroup+":"+category+":"+subcategory);

				// Update Incident
				String updatesql = "update enterprise_incident set extension=json_set(extension,'$.Category',?),extension=json_set(extension,'$.\"Sub Category\"',?),extension=json_set(extension,'$.\"Assignment Group\"',?) where ticketRef=?";
				PreparedStatement pst = conn.prepareStatement(updatesql);
				pst.setString(1, category);
				pst.setString(2, subcategory);
				pst.setString(3, assignmentGroup);
				pst.setString(4, ticketRef);
				pst.executeUpdate();
				pst.close();
			}
		}
		conn.commit();
	}

	private void updateHACLAAssetCategory() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		int rowCount = 0;
		String updatesql = "update enterprise_asset set extension=json_set(extension, '$.Category', ?) where asset_name = ?";
		CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		for(CSVRecord dataRow : dataReader) {
			rowCount++;
			String assetName = "", category = "";
			for(int i=0; i<dataRow.size(); i++) {
				if(i == 0) {
					assetName = dataRow.get(i).trim();
				} else if(i == 1) {
					category = dataRow.get(i).trim();
				} 
			}

			PreparedStatement pst = conn.prepareStatement(updatesql);
			pst.setString(1, category);
			pst.setString(2, assetName);
			pst.executeUpdate();
			pst.close();
		}
		conn.commit();
	}

	private void sendEmail(JSONArray emailList) throws Exception {
		Properties properties = System.getProperties();
		final String from = "support@smarterd.com";
		final String username = "AKIA4NQIVXQRKYLPST7I";
		final String password = "BIoEBHI7NSwOuEMpiF2lH7UDm1X++8DMbinyDDX1eBZp";
		Session session = null;
		OrderedJSONObject gateway = null;

		String host = "email-smtp.us-east-1.amazonaws.com", port = "587";
		if(gateway != null) {
			host = (String)gateway.get("host");
			port = (String)gateway.get("port");
			//username = (String)gateway.get("username");
			//password = (String)gateway.get("password");

			properties.setProperty("mail.smtp.host", host);
			properties.setProperty("mail.smtp.port", port);

			// Get the default Session object.
			session = Session.getDefaultInstance(properties);
		} else {
			properties.setProperty("mail.smtp.auth", "true");
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.smtp.host", host);
			properties.setProperty("mail.smtp.port", port);
			properties.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");

			// Get the default Session object.
			session = Session.getInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		}

		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);
		message.setHeader("Content-Type", "html");

		// Set From: header field of the header.
		message.setFrom(new InternetAddress(from));

		// Set To: header field of the header.
		System.out.println("****************************EMAIL LIST:"+emailList.size());
		Iterator iter = emailList.iterator();
		while(iter.hasNext()) {
			OrderedJSONObject emailDetail = (OrderedJSONObject)iter.next();
			JSONArray emails = (JSONArray)emailDetail.get("EMAIL_LIST");
			JSONArray ccemails = null;
			if(emailDetail.has("CC_LIST")) {
				ccemails = (JSONArray)emailDetail.get("CC_LIST");
			} else {
				ccemails = new JSONArray();
			}
			JSONArray bccemails = null;
			if(emailDetail.has("BCC_LIST")) {
				bccemails = (JSONArray)emailDetail.get("BCC_LIST");
			} else {
				bccemails = new JSONArray();
			}
			String msg = (String)emailDetail.get("MSG");
			String subject = (String)emailDetail.get("ALERT_NAME");
			String type = (String)emailDetail.get("TYPE");
			if(type != null && type.length() > 0) {
				subject = subject + " ["+type+"]";
			}
			if(emails.size() > 0) {
				Iterator emailIter = emails.iterator();
				while(emailIter.hasNext()) {
					String email = (String)emailIter.next();
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
				}
				Iterator ccIter = ccemails.iterator();
				while(ccIter.hasNext()) {
					String cc = (String)ccIter.next();
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				}
				Iterator bccIter = bccemails.iterator();
				while(bccIter.hasNext()) {
					String bcc = (String)bccIter.next();
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
				}

				// Set Subject: header field
				message.setSubject(subject);

				// Now set the actual message
				//System.out.println("Msg:"+msg);
				message.setText(msg, "utf-8", "html");
				message.saveChanges();

				// Send message
				Transport.send(message);
				System.out.println("Email sent Successfully["+subject+"]");
			}
		}
	}

	private void addVulnerabilityAssetToGroup() throws Exception {
        System.out.println("In addVulnerabilityAssetToGroup...");
        String insertsql = "insert into enterprise_asset(id, asset_name, asset_desc, asset_category, lifecycle_name, asset_lifecycle_list, owner,asset_location_name,extension,created_by,updated_by,created_timestamp,updated_timestamp,company_code) values(?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?)";
		String groupName = "";
		// Store Network
		//groupName = "Group-Vulnr-Scan-Store-Network-Devices";
		//JSONArray ipList = new JSONArray("['10.160.34.1','10.160.34.108','10.160.34.161','10.160.34.177','10.160.34.193','10.160.34.241','10.160.34.33','10.160.34.97','10.160.4.1','10.160.4.108','10.160.4.161','10.160.4.164','10.160.4.177','10.160.4.193','10.160.4.241','10.160.4.33','10.160.4.97','10.160.45.1','10.160.45.108','10.160.45.161','10.160.45.164','10.160.45.177','10.160.45.193','10.160.45.241','10.160.45.33','10.160.45.97','10.160.95.1','10.160.95.108','10.160.95.161','10.160.95.164','10.160.95.177','10.160.95.193','10.160.95.241','10.160.95.33','10.160.95.97','10.161.232.1','10.161.232.108','10.161.232.161','10.161.232.164','10.161.232.177','10.161.232.193','10.161.232.241','10.161.232.33','10.161.232.97','10.161.58.1','10.161.58.108','10.161.58.161','10.161.58.164','10.161.58.177','10.161.58.193','10.161.58.241','10.161.58.33','10.161.58.97','10.162.115.1','10.162.115.108','10.162.115.161','10.162.115.164','10.162.115.177','10.162.115.193','10.162.115.241','10.162.115.33','10.162.115.97','10.162.220.1','10.162.220.108','10.162.220.161','10.162.220.164','10.162.220.177','10.162.220.193','10.162.220.241','10.162.220.33','10.162.220.97','10.163.13.1','10.163.13.108','10.163.13.161','10.163.13.164','10.163.13.177','10.163.13.193','10.163.13.241','10.163.13.33','10.163.13.97','10.163.153.1','10.163.153.108','10.163.153.161','10.163.153.164','10.163.153.177','10.163.153.193','10.163.153.241','10.163.153.33','10.163.153.97','10.167.100.1','10.167.100.161','10.167.100.177','10.167.100.193','10.167.100.241','10.167.100.33','10.167.100.97','10.167.101.1','10.167.101.161','10.167.101.177','10.167.101.193','10.167.101.241','10.167.101.33','10.167.101.97','10.167.115.1','10.167.115.161','10.167.115.177','10.167.115.193','10.167.115.241','10.167.115.33','10.167.115.97','10.167.116.1','10.167.116.161','10.167.116.177','10.167.116.193','10.167.116.241','10.167.116.33','10.167.116.97','10.167.117.1','10.167.117.177','10.167.117.193','10.167.117.209','10.167.117.212','10.167.117.241','10.167.117.33','10.167.117.97','10.167.120.1','10.167.120.161','10.167.120.177','10.167.120.193','10.167.120.241','10.167.120.33','10.167.120.97']");
		
		// Data Center
		// groupName = "Group-Vulnr-Scan-Datacenter-Devices";
		//JSONArray ipList = new JSONArray("['10.96.0.1','10.96.0.129','10.96.0.130','10.96.0.137','10.96.0.138','10.96.0.139','10.96.0.145','10.96.0.146','10.96.0.153','10.96.0.154','10.96.0.155','10.96.0.161','10.96.0.162','10.96.0.163','10.96.0.169','10.96.0.170','10.96.0.171','10.96.0.193','10.96.0.194','10.96.0.197','10.96.0.198','10.96.0.2','10.96.0.201','10.96.0.202','10.96.0.203','10.96.0.204','10.96.0.212','10.96.1.1','10.96.1.113','10.96.1.114','10.96.1.128','10.96.1.129','10.96.1.129','10.96.1.2','10.96.1.3','10.96.18.23','10.96.18.24','10.96.2.1','10.96.2.2','10.96.2.25','10.96.2.3','10.96.3.1','10.96.3.101','10.96.3.102','10.96.3.103','10.96.3.104','10.96.3.105','10.96.3.106','10.96.3.107','10.96.3.108','10.96.3.109','10.96.3.110','10.96.3.111','10.96.3.112','10.96.3.2','10.96.3.3','10.96.5.1','10.96.5.2','10.96.5.3','10.98.16.1','10.98.17.1','10.98.18.1','10.98.18.120','10.98.19.1','10.98.20.1','10.98.4.100','10.98.4.107','10.98.4.107','10.98.4.2','10.98.4.34','10.98.64.1','10.98.65.1','10.98.66.1']");
		
		// Duluth Office
		// groupName = "Group-Vulnr-Scan-Duluth-Devices";
		//JSONArray ipList = new JSONArray("['10.95.10.1','10.95.11.1','10.95.12.1','10.95.16.1','10.95.18.1','10.95.19.235','10.95.20.1','10.95.22.1','10.95.24.1','10.95.24.58','10.95.36.1','10.95.36.57','10.95.38.1','10.95.38.10','10.95.38.11','10.95.38.12','10.95.38.14','10.95.38.15','10.95.38.51','10.95.38.53','10.95.38.58','10.95.38.60','10.95.38.61','10.95.38.65','10.95.38.88','10.95.40.1','10.95.40.117','10.95.40.13','10.95.40.14','10.95.40.15','10.95.40.17','10.95.40.18','10.95.40.19','10.95.40.52','10.95.42.1','10.95.42.10','10.95.42.107','10.95.42.108','10.95.42.11','10.95.42.115','10.95.42.118','10.95.42.12','10.95.42.13','10.95.42.14','10.95.42.16','10.95.42.17','10.95.42.18','10.95.42.20','10.95.42.21','10.95.42.22','10.95.42.24','10.95.42.25','10.95.42.52','10.95.44.1','10.95.44.10','10.95.44.102','10.95.44.11','10.95.44.174','10.95.44.51','10.95.44.52','10.95.44.53','10.95.44.54','10.95.44.56','10.95.44.57','10.95.44.58','10.95.44.59','10.95.44.60','10.95.44.62','10.95.44.63','10.95.44.79','10.95.44.83','10.95.44.84','10.95.44.96','10.95.9.1','10.95.9.10','10.95.9.109','10.95.9.11','10.95.9.111','10.95.9.112','10.95.9.113','10.95.9.114','10.95.9.115','10.95.9.116','10.95.9.117','10.95.9.118','10.95.9.119','10.95.9.12','10.95.9.120','10.95.9.122','10.95.9.123','10.95.9.124','10.95.9.13','10.95.9.14','10.95.9.15','10.95.9.53','10.95.9.54','10.95.9.55','10.95.9.56','10.95.9.57','10.95.9.58','10.95.9.59','10.95.9.60','10.95.9.61','10.95.9.62','10.95.9.63','10.95.9.64','10.95.9.65','10.95.9.66','10.95.9.67','10.95.9.68','10.95.9.69','10.95.9.70','10.95.9.71','10.95.9.72','10.95.9.73','10.95.9.74','10.95.9.75','10.95.9.76','10.95.9.77','10.95.9.78','10.95.9.79','10.95.9.80','10.95.9.81','10.95.9.82','10.95.9.83','10.95.9.84','10.95.9.85','10.95.9.86','10.95.9.87','10.95.9.88','10.95.9.89','10.95.9.90','10.95.9.91','10.95.9.92','10.95.9.93','10.95.9.94','10.95.9.95','10.95.9.96','10.95.9.97','10.95.9.98','10.95.9.99','10.95.96.1','10.95.96.172']");
		// JSONArray ipList = new JSONArray("['10.95.18.64','10.95.19.231','10.95.20.62','10.95.21.231','10.95.21.232','10.95.21.233','10.95.23.231','10.95.23.232','10.95.24.59','10.95.25.231','10.95.25.232','10.95.25.234','10.95.25.235','10.95.25.237','10.95.9.100','10.95.9.101','10.95.9.102','10.95.9.103','10.95.9.104','10.95.9.105']");
		
		// St Paul Core Switch
		//groupName = "Group-Vulnr-Scan-StPaul-Devices";
		//JSONArray ipList = new JSONArray("['10.94.10.1','10.94.10.129','10.94.10.130','10.94.10.201','10.94.10.202','10.94.10.85','10.94.11.1','10.94.11.50','10.94.12.1','10.94.12.201','10.94.12.203','10.94.12.205','10.94.13.1','10.94.13.206','10.94.14.1','10.94.8.1','10.94.8.2','10.94.8.254','10.94.8.3','10.94.8.4','10.94.8.5','10.94.8.6','10.94.8.7','10.94.9.1','10.94.9.249','10.94.9.254','10.94.9.33','10.94.9.65']");
		
		// Core Device Management IPs
		groupName = "Group-Vulnr-Scan-Core-Mgmt-Devices";
		JSONArray ipList = new JSONArray("['10.96.18.10','10.96.18.11','10.96.20.5','10.96.4.11','10.96.4.12','10.96.4.153','10.96.4.19','10.96.4.2','10.96.4.20','10.96.4.26','10.96.4.27','10.96.4.3','10.96.4.42','10.96.4.43','10.96.19.4','10.96.19.7']");
	
		String desc = "Reconciled Asset IP list Discovered through Vulnerability Scan";
		String type = "IP ADDRESS";
		String subtype = "";
		String loc = "Chaska DC";
		String assetId = generateUUID();
		
        OrderedJSONObject extObj = new OrderedJSONObject();
        extObj.put("Type", type);
        extObj.put("Sub-Type", subtype);
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());
		extObj.put("IP Address", ipList);
		extObj.put("Private IP Address",ipList);
		extObj.put("Public IP Address", new JSONArray());
		extObj.put("Scan IP Address", new JSONArray());
		extObj.put("Floating IP Address", new JSONArray());
		extObj.put("Endpoint", "No");
		extObj.put("Endpoint Detail", new OrderedJSONObject());
		extObj.put("Business app/IT ap", "");
		extObj.put("isgroupasset", new Boolean(true));
		extObj.put("groupassetcount", new Integer(ipList.size()));

        OrderedJSONObject lcObj = new OrderedJSONObject();
        lcObj.put("lifecycle_name", "Invest");
        lcObj.put("current", new Boolean(true));
        lcObj.put("startdate", "");
        lcObj.put("enddate", "");
        JSONArray lcList = new JSONArray();
        lcList.add(lcObj);

        PreparedStatement pst = conn.prepareStatement(insertsql);
        pst.setString(1, assetId);
        pst.setString(2, groupName);
        pst.setString(3, desc);
        pst.setString(4, "IT ASSET");
        pst.setString(5, "Invest");
        pst.setString(6, lcList.toString());
        pst.setString(7, "");
        pst.setString(8, loc);
        pst.setString(9, extObj.toString());
        pst.setString(10, "support@smarterd.com");
        pst.setString(11, "support@smarterd.com");
		pst.setString(12, companyCode);
        pst.executeUpdate();
        pst.close();
	
		//assetId = "9d40aa4c-37c7-4bd3-a076-025f8b081e44";

        // Add to IP Table
        Iterator iter = ipList.iterator();
        while(iter.hasNext()) {
            String ip = (String)iter.next();
            createIP(assetId, assetId, type, subtype, ip, "Private", "Vulnr Agent");
        }

		conn.commit();
        System.out.println("Successfully created Group Asset and added to IP List");
    }

	// Create IP Entry in IP Table
    protected void createIP(String assetId, String groupId, String type, String subtype, String ip, String ipType, String source) throws Exception {
        System.out.println("In createIP..."+assetId+":"+groupId+":"+ip);
        String checksql = "select id, ip_address, extension from enterprise_ip where ip_address = ?";
        String insertsql = "insert into enterprise_ip(asset_id, group_id, ip_address, ip_type, extension) values(?,?,?,?,?)";
        String updatesql = "update enterprise_ip set group_id=?,ip_address=?,ip_type=?,extension=? where id = ?";
        int ipId = 0;
        String ext = null;
        PreparedStatement pst = conn.prepareStatement(checksql);
        pst.setString(1, ip);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            ipId = rs.getInt("id");
            ext = rs.getString("extension");
        }
        rs.close();
        pst.close();

        if(groupId == null) {
            groupId = "";
        }

		/*
        if(privateIP == null) {
            privateIP = new JSONArray();
        }
        if(publicIP == null) {
            publicIP = new JSONArray();
        }
        if(floatingIPList == null) {
            floatingIPList = new JSONArray();
        }
        if(scanIPList == null) {
            scanIPList = new JSONArray();
        }

        JSONArray ipList = new JSONArray();
        if(privateIP.size() > 0) {
            Iterator ipIter = privateIP.iterator();
            while(ipIter.hasNext()) {
                String ip = (String)ipIter.next();
                if(!ipList.contains(ip)) {
                    ipList.add(ip);
                }
            }
        }
        if(publicIP.size() > 0) {
            Iterator ipIter = publicIP.iterator();
            while(ipIter.hasNext()) {
                String ip = (String)ipIter.next();
                if(!ipList.contains(ip)) {
                    ipList.add(ip);
                }
            }
        }
        if(floatingIPList.size() > 0) {
            Iterator ipIter = floatingIPList.iterator();
            while(ipIter.hasNext()) {
                String ip = (String)ipIter.next();
                if(!ipList.contains(ip)) {
                    ipList.add(ip);
                }
            }
        }
        if(scanIPList.size() > 0) {
            Iterator ipIter = scanIPList.iterator();
            while(ipIter.hasNext()) {
                String ip = (String)ipIter.next();
                if(!ipList.contains(ip)) {
                    ipList.add(ip);
                }
            }
        }
		*/

        OrderedJSONObject extObj = null;
        if(ipId == 0) {
            extObj = new OrderedJSONObject();
            JSONArray sourceList = new JSONArray();
            sourceList.add(source);
            extObj.put("source", sourceList);
			extObj.put("type", type.toUpperCase());
			extObj.put("sub_type", subtype.toUpperCase());

            pst = conn.prepareStatement(insertsql);
            pst.setString(1, assetId);
            pst.setString(2, groupId);
            pst.setString(3, ip);
            pst.setString(4, ipType);
            pst.setString(5, extObj.toString());
            pst.executeUpdate();
            pst.close();
        } else {
            // Update Extension
            extObj = new OrderedJSONObject(ext);
            JSONArray sourceList = null;
            if(extObj.has("source")) {
                sourceList = (JSONArray)extObj.get("source");
            } else {
                sourceList = new JSONArray();
            }
            if(source != null && source.length() > 0 && !sourceList.contains(source)) {
                sourceList.add(source);
            }
            extObj.put("source", sourceList);

            pst = conn.prepareStatement(updatesql);
            pst.setString(1, groupId);
            pst.setString(2, ip);
			pst.setString(3, ipType);
            pst.setString(4, extObj.toString());
            pst.setInt(5, ipId);
            pst.executeUpdate();
            pst.close();
        }
    }

	private void loadQIDToCVEMapping() throws Exception {
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		int rowCount = 0, batchCount=0;
		String checksql = "select count(*) count from SE.enterprise_qualys_cve_mapping where qid = ?";
		String insertsql = "insert into SE.enterprise_qualys_cve_mapping values(?,?,?,?,?,?,?,?,?,?)";
		String updatesql = "update SE.enterprise_qualys_cve_mapping set cve_list=?,title=?,subcategory=?,category=?,reference=?,cvebase=?,cve3base=?,created=?,updated=? where qid=?";
		PreparedStatement pst1 = conn.prepareStatement(insertsql);
		PreparedStatement pst2 = null;

		String deletesql = "delete from SE.enterprise_qualys_cve_mapping";
		PreparedStatement pst = conn.prepareStatement(deletesql);
		pst.executeUpdate();
		pst.close();
		conn.commit();

		Instant startTime = Instant.now();

		CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		for(CSVRecord dataRow : dataReader) {
			/*
			if(rowCount == 0) {
				pst1 = conn.prepareStatement(insertsql);
		 		pst2 = conn.prepareStatement(updatesql);
			}
			*/
			String qid = "", title="", subcategory="", category="", cve = "", reference="", cvebase="", cve3base="", updated="", created="";
			for(int i=0; i<dataRow.size(); i++) {
				if(i == 0) {
					qid = dataRow.get(i).trim();
				} else if(i == 1) {
					title = dataRow.get(i).trim();
				} else if(i == 2) {
					subcategory = dataRow.get(i).trim();
				} else if(i == 3) {
					category = dataRow.get(i).trim();
				} else if(i == 4) {
					cve = dataRow.get(i).trim();
				} else if(i == 5) {
					reference = dataRow.get(i).trim();
				} else if(i == 6) {
					cvebase = dataRow.get(i).trim();
				} else if(i == 7) {
					cve3base = dataRow.get(i).trim();
				} else if(i == 8) {
					
				} else if(i == 9) {
					updated = dataRow.get(i).trim();
				} else if(i == 10) {
					created = dataRow.get(i).trim();
				} 
			}
			rowCount++;
			batchCount++;

			// parse created and updated timestamp
			if(created.indexOf("at") != -1) {
				created = created.substring(0, created.indexOf("at")-1).trim();
			}
			if(updated.indexOf("at") != -1) {
				updated = updated.substring(0, updated.indexOf("at")-1).trim();
			}
			System.out.println(title+":"+created+":"+updated);

			JSONArray cveList = new JSONArray();
			if(cve.length() > 0) {
				String [] list = cve.split(",");
				for(int i=0; i<list.length; i++) {
					String str = list[i].trim();
					cveList.add(str);
				}
			}
			/*
			int count = 0;
			PreparedStatement pst = conn.prepareStatement(checksql);
			pst.setString(1, qid);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				pst1.setString(1, qid);
				pst1.setString(2, cveList.toString());
				pst1.setString(3, title);
				pst1.setString(4, subcategory);
				pst1.setString(5, category);
				pst1.setString(6, reference);
				pst1.setString(7, cvebase);
				pst1.setString(8, cve3base);
				pst1.setString(9, created);
				pst1.setString(10, updated);
			} else {
				pst2.setString(1, qid);
				pst2.setString(2, cveList.toString());
				pst2.setString(3, title);
				pst2.setString(4, subcategory);
				pst2.setString(5, category);
				pst2.setString(6, reference);
				pst2.setString(7, cvebase);
				pst2.setString(8, cve3base);
				pst2.setString(9, created);
				pst2.setString(10, updated);
			}
			*/
			pst1.setString(1, qid);
			pst1.setString(2, cveList.toString());
			pst1.setString(3, title);
			pst1.setString(4, subcategory);
			pst1.setString(5, category);
			pst1.setString(6, reference);
			pst1.setString(7, cvebase);
			pst1.setString(8, cve3base);
			pst1.setString(9, created);
			pst1.setString(10, updated);
			pst1.addBatch();

			if(rowCount == 1000) {
				pst1.executeBatch();
				//pst2.executeBatch();
				//pst1.close();
				//pst2.close();
				System.out.println("Processed row:"+rowCount);
				rowCount = 0;
			}
		}
		conn.commit();
		conn.close();
		Instant endTime = Instant.now();
		long duration = Duration.between(startTime, endTime).toMinutes();
		System.out.println("Successfully loaded Qualys QID Data["+duration+" mts]");
	}

	private void updateVulnerabilityLifecycle() throws Exception {
		String sql = "select json_unquote(json_extract(extension, '$.THREAT.status')) status from enterprise_vulnerability_log where vulnerability_id = ?";
		String updatesql = "update enterprise_vulnerability set extension=json_set(extension, '$.lifecycle', ?) where _id = ?";
		String status = "";
		String vulnrsql = "select _id from enterprise_vulnerability";
		PreparedStatement pst1 = conn.prepareStatement(vulnrsql);
		ResultSet rs1 = pst1.executeQuery();
		while(rs1.next()) {
			String id = rs1.getString("_id");
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, id);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				String currentstatus = rs.getString("status");
				if(status.length() == 0) {
					status = currentstatus;
				} else if(currentstatus.equalsIgnoreCase("Open") && !status.equalsIgnoreCase("In Progress")) {
					status = "In Progress";
				} else if(currentstatus.equalsIgnoreCase("Active") && !status.equalsIgnoreCase("In Progress")) {
					status = "Active";
				} else if(currentstatus.equalsIgnoreCase("False Positive") && !(status.equalsIgnoreCase("In Progress") || status.equalsIgnoreCase("Active"))) {
					status = "False Positive";
				} else if(currentstatus.equalsIgnoreCase("Risk Acceptance") && !(status.equalsIgnoreCase("In Progress") || status.equalsIgnoreCase("Active"))) {
					status = "Risk Acceptance";
				} else if(currentstatus.equalsIgnoreCase("Complete") && !(status.equalsIgnoreCase("In Progress") || status.equalsIgnoreCase("Active"))) {
					status = "Complete";
				} 
			}
			rs.close();
			pst.close();

			pst = conn.prepareStatement(updatesql);
			pst.setString(1, status);
			pst.setString(2, id);
			pst.executeUpdate();
			pst.close();
		}
		rs1.close();
		pst1.close();

		conn.commit();
		conn.close();
	}

	private void updateMAURVulnerabilityLog(String period) throws Exception {
		String sql = "select id, json_unquote(json_extract(extension, '$.THREAT.scan_id')) scan_id, json_unquote(json_extract(extension, '$.scan_period')) scan_period, extension from enterprise_vulnerability_log_history where json_extract(extension, '$.scan_period') = ?";
		String checksql = "select id, extension from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.scan_id') = ?";
		String updatesql = "update enterprise_vulnerability_log set extension = ? where id = ?";
		String insertsql = "insert into enterprise_vulnerability_log select * from enterprise_vulnerability_log_history where id = ?";

		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, period);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String historyId = rs.getString("id");
			String scanId = rs.getString("scan_id");
			String scanPeriod = rs.getString("scan_period");
			String historyExt = rs.getString("extension");
			System.out.println(scanId+":"+scanPeriod);

			String logId = null, ext = null;
			PreparedStatement pst1 = conn.prepareStatement(checksql);
			pst1.setString(1, scanId);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				logId = rs1.getString("id");
				ext = rs1.getString("extension");
			}
			rs1.close();
			pst1.close();

			if(logId != null) {
				System.out.println("Found!!!");
				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				String currentscanPeriod = (String)extObj.get("scan_period");
				JSONArray scanPeriodList = null;
				if(extObj.has("scan_period_list")) {
					scanPeriodList = (JSONArray)extObj.get("scan_period_list");
					if(!scanPeriodList.contains(scanPeriod)) {
						scanPeriodList.add(scanPeriod);
					}
				} else {
					scanPeriodList = new JSONArray();
					scanPeriodList.add(scanPeriod);
				}
				if(!scanPeriodList.contains(currentscanPeriod)) {
					scanPeriodList.add(currentscanPeriod);
				}
				extObj.put("scan_period_list", scanPeriodList);
				pst1 = conn.prepareStatement(updatesql);
				pst1.setString(1, extObj.toString());
				pst1.setString(2, logId);
				pst1.executeUpdate();
				pst1.close();
			} else {
				System.out.println("Not Found!!!");
				pst1 = conn.prepareStatement(insertsql);
				pst1.setString(1, historyId);
				pst1.executeUpdate();
				pst1.close();

				OrderedJSONObject historyExtObj = new OrderedJSONObject(historyExt);
				OrderedJSONObject threatCtx = (OrderedJSONObject)historyExtObj.get("THREAT");
				String status = (String)threatCtx.get("status");
				if(status.equalsIgnoreCase("Active") || status.equalsIgnoreCase("Open") || status.equalsIgnoreCase("In-Progress")) {
					status = "Complete";
					threatCtx.put("status", status);
					historyExtObj.put("THREAT", threatCtx);
				}
				if(!historyExtObj.has("ASSET")) {
					OrderedJSONObject assetObj = new OrderedJSONObject();
					assetObj.put("os", "");
					assetObj.put("type", "");
					assetObj.put("sub_type", "");
					assetObj.put("location", "");
					assetObj.put("owner", "");
					historyExtObj.put("ASSET", assetObj);
				}
				JSONArray scanPeriodList = new JSONArray();
				scanPeriodList.add(scanPeriod);
				historyExtObj.put("scan_period_list", scanPeriodList);
				pst1 = conn.prepareStatement(updatesql);
				pst1.setString(1, historyExtObj.toString());
				pst1.setString(2, historyId);
				pst1.executeUpdate();
				pst1.close();
			}
		}

		conn.commit();
		conn.close();
	}

	private void formatIncidentDesc() throws Exception {
		String sql = "select name, description from enterprise_incident where ticketRef = ?";
		String updatesql = "update enterprise_incident set name=?, description=? where ticketRef = ?";
		String ticketRef = "INC0318235";
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, ticketRef);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String name = rs.getString("name");
			String desc = rs.getString("description").trim();
			if(name.startsWith(".")) {
				name = name.substring(1);
			}
			if(desc.startsWith(".")) {
				desc = desc.substring(1);
			}
			desc = desc.replaceAll("\\.nn", ".<br>");
			desc = desc.replaceAll("nn", "<br>");
			desc = desc.replaceAll("\\.n", ".<br>");
			desc = desc.replaceAll("n-", "<br>");
			desc = desc.replaceAll("\\:n", ":<br>");
			desc = desc.replaceAll(",n", ",<br>");
			desc = desc.replaceAll(", n", ",<br>");

			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, name);
			pst1.setString(2, desc);
			pst1.setString(3, ticketRef);
			pst1.executeUpdate();
			pst1.close();
		}
		rs.close();
		pst.close();
		conn.commit();
	}

	private void updateIncidentDesc() throws Exception {
		String sql = "select name, description from enterprise_incident where ticketRef = ?";
		String updatesql = "update enterprise_incident set name=?, description=? where ticketRef = ?";

		CSVParser dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		for(CSVRecord dataRow : dataReader) {
			String ticketRef = "", name = "", desc = "";
			for(int i=0; i<dataRow.size(); i++) {
				if(i == 0) {
					ticketRef = dataRow.get(i).trim();
				} else if(i == 1) {
				} else if(i == 2) {
					name = dataRow.get(i).trim();
				} else if(i == 3) {
					desc = dataRow.get(i).trim();
					break;
				} 
			}
			System.out.println(ticketRef+":"+name+":"+desc);
			name = name.replaceAll("\n", "");
			desc = desc.replaceAll("\n", "<br>");
			PreparedStatement pst1 = conn.prepareStatement(updatesql);
			pst1.setString(1, name);
			pst1.setString(2, desc);
			pst1.setString(3, ticketRef);
			pst1.executeUpdate();
			pst1.close();
			//break;
		}
		conn.commit();
	}

	private void updateSoftwareFromPOMXML() throws Exception {
		String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,asset_version,lifecycle_name,asset_category,asset_type,asset_lifecycle_list,extension,owner,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(?,?,?,?,'Invest','SOFTWARE','ASSET',?,?,'Vijay Sundhar','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'SMARTE')";
		String relinsertsql = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,'ASSET',?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'SMARTE')";
		String compId1 = "58fb0e6c-b908-47c0-b5eb-ad259691a93f", compId2 = "e88fea37-6b3d-4d2f-8e8a-5cab40d03e26";
		String loaderId = Long.toString(System.currentTimeMillis());

		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new File(filename));
		doc.getDocumentElement().normalize();
		NodeList dependencyList = doc.getElementsByTagName("dependency");
		int n = dependencyList.getLength();
		for (int i=0; i<n; i++) {
			Node current = dependencyList.item(i);
			NodeList nodeList = current.getChildNodes();
			int count = nodeList.getLength();
			String assetName = "", version= "", desc = "";
			for (int j=0; j<count; j++) {
				Node node = nodeList.item(j);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = node.getNodeName();
					String nodeValue = node.getTextContent();
					if(nodeName.equalsIgnoreCase("groupId")) {
						desc = nodeValue;
					} else if(nodeName.equalsIgnoreCase("artifactId")) {
						assetName = nodeValue;
					} else if(nodeName.equalsIgnoreCase("version") && nodeValue.indexOf("$") == -1) {
						version = nodeValue;
					}
				}
			}
			System.out.println(assetName+":"+version+":"+desc);
			JSONArray ecosystem = new JSONArray();
			ecosystem.add("API");
			ecosystem.add("Java");
			ecosystem.add(assetName);
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("Category", "Programming");
			extObj.put("Sub-Category", "API");
			extObj.put("Ecosystem", ecosystem);
			extObj.put("Organization", "SmarterD");
			extObj.put("Application Deployment Model", "Public Cloud");
			extObj.put("Support Type", "Self Support");
			extObj.put("loader_id", loaderId);

			JSONArray lcList = new JSONArray();
			OrderedJSONObject lcObj = new OrderedJSONObject();
			lcObj.put("id", "0");
			lcObj.put("lifecycle_name", "Invest");
			lcObj.put("lifecycle_status", "GREEN");
			lcObj.put("comment", "");
			lcObj.put("impact", "");
			lcObj.put("startdate", "2022-07-01");
			lcObj.put("enddate", "");
			lcObj.put("current", new Boolean(true));
			lcList.add(lcObj);

			String id = generateUUID();
			PreparedStatement pst = conn.prepareStatement(insertsql);
			pst.setString(1, id);
			pst.setString(2, assetName);
			pst.setString(3, desc);
			pst.setString(4, version);
			pst.setString(5, lcList.toString());
			pst.setString(6, extObj.toString());
			pst.executeUpdate();
			pst.close();

			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPONENT TYPE", "ASSET");
			ctx.put("LINKED", "N");
			pst = conn.prepareStatement(relinsertsql);
			pst.setString(1, compId1);
			pst.setString(2, id);
			pst.setString(3, ctx.toString());
			pst.addBatch();
			pst.setString(1, id);
			pst.setString(2, compId1);
			pst.setString(3, ctx.toString());
			pst.addBatch();
			pst.setString(1, compId2);
			pst.setString(2, id);
			pst.setString(3, ctx.toString());
			pst.addBatch();
			pst.setString(1, id);
			pst.setString(2, compId2);
			pst.setString(3, ctx.toString());
			pst.addBatch();
			pst.executeBatch();
			pst.close();
			System.out.println("-----------------");
		}
		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
   	}

	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		UpdateComment loader = new UpdateComment(filename, companyCode, env);
		//loader.process();
		
		//loader.updateAssetManagerColumn();
		//loader.updateGroupName();
		//loader.updateCollaborationRiskDetails();
		//loader.rollupThreatRawRisk();
		
		//loader.updateEnterpriseAssessmentDetail();
		//loader.updateCMMCControlId();
		//loader.updateCMMCV2ObjectiveId();
		//loader.loadObjectivesToAssessment();
		
		//loader.updateITAssetCriticality();
		//loader.printExtAttributes("APPLICATION");
	
		//loader.createAssignmentGroup();
		//loader.updateAssignmentCategory();
		
		//loader.updateSettingValueOrder();
		
		//loader.loadMLResult();
		
		//loader.cleanupAssetIPAddress();
		//loader.updateComponentIds();
		//loader.updatedRelatedControls();
		
		//loader.updateDeploymentModel();
		//loader.reconcileMauricesAssets();
		//loader.updateRackspaceVM();
		loader.updateRackspaceVMFormat2();
		//loader.reconcileServerList();
		//loader.validateAssetListByIP();
		//loader.generateApplicationMetadataFromITAsset();
		//loader.loadModelToTypeMapping();

		//loader.updateIncidentTime();
		
		//loader.validateHACLAServerLoadFile();
		//loader.initializeAssetCompliance();
		
		//loader.syncMLIncident();
		//loader.updateMLIncidentCategory();
		//loader.updateAssignmentGroupSetting();
		//loader.updateMLIncidentTokenList();
		//loader.formatStoreNetworkInvetoryFile();
		
		//loader.updateMLCategory();
		//loader.updateHACLACategory();
		//loader.updateHACLAAssetCategory();

		// Initialize Incident Setting
		//loader.updateIncidentSettingFromData("Category", "Incident Category", "SECURITY", "EXT");
		//loader.updateIncidentSettingFromData("Sub Category", "Incident Sub Category", "SECURITY", "EXT");
		//loader.updateIncidentSettingFromData("Assignment Group", "Incident Group", "SECURITY", "EXT");
		//loader.updateIncidentSettingFromData("Resolution Code", "Incident Resolution Code", "SECURITY", "EXT");
		//loader.updateIncidentSettingFromData("On-Hold Reason", "Incident On Hold Reason", "SECURITY", "EXT");
		//loader.updateIncidentSettingFromData("source", "Incident Source", "SECURITY", "CORE");
		//loader.updateIncidentSettingFromData("lifecycle", "Incident Status", "SECURITY", "CORE");
		//loader.updateIncidentSettingFromData("priority", "Incident Priority", "SECURITY", "CORE");

		// Email Test
		/*
		OrderedJSONObject email = new OrderedJSONObject();
		JSONArray toList = new JSONArray();
		toList.add("vsundhar@maurices.com");
		JSONArray ccList = new JSONArray();
		ccList.add("support@smarterd.com");
		email.put("MSG", "Hello World!!!");
		email.put("ALERT_NAME", "TEST");
		email.put("TYPE", "TEST");
		email.put("EMAIL_LIST", toList);
		email.put("CC_LIST", ccList);
		JSONArray emailList = new JSONArray();
		emailList.add(email);
		loader.sendEmail(emailList);
		*/

		//loader.updateEnterpriseIP();

		//----------------------------------
		// Vulnerability Update Functions
		//----------------------------------
		//loader.loadQIDToCVEMapping();
		//loader.addVulnerabilityAssetToGroup();
		//loader.updateVulnerabilityLifecycle();
		//loader.updateMAURVulnerabilityLog("MAR-22");
		//loader.updateVulnerabilityLifecycleStatus();
		//loader.updateVulnerabilityLogWithAssetDetail();	// Initialize Vulnerability log asset with asset context
		//loader.updateIPAddressFromSheet(filename);
		//loader.updatePortFromSheet(filename);
		//loader.updateCVE();
		//loader.updateCVEAssociated();
		//loader.updatePossiblePatches();
		//loader.updateMissedIPAddressLinkToVulnr();
		//loader.updateVulnerabilityLogRemediationStatus();
		//loader.updateVulnerabilityRemediationStatus();
		//loader.syncVulnerabilityLog();
		//loader.resyncAssetWithVulnerability();
		//loader.resyncVulnerabilityWithAsset();
		//loader.cleanupVulnerabilityLog();
		//loader.updateVulnerabilityStatus();
		//loader.validateVulnrLogIPWithAsset();
		//loader.categorizeVulnerabilities();
		//loader.updateVulnerabilityPriority();

		//loader.formatIncidentDesc();
		//loader.updateIncidentDesc();
		//loader.updateSoftwareFromPOMXML();
	}
}