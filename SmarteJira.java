import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.io.File;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.Project;
import net.rcarz.jiraclient.ProjectCategory;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.IssueType;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Watches;
import net.sf.json.JSON;
import net.sf.json.JSONObject;

import net.rcarz.jiraclient.Comment;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.agile.AgileClient;
import net.rcarz.jiraclient.agile.Sprint;
import net.rcarz.jiraclient.agile.TimeTracking;
import net.rcarz.jiraclient.agile.Resolution;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;

public class SmarteJira {
   private String companyCode = null;
   private Connection conn = null;
   String hostname = "35.227.181.195";
   
   public SmarteJira(String companyCode, String env) throws Exception {
      this.companyCode = companyCode;

      if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
      // Connect Mysql
      mysqlConnect(companyCode);
   }

   public OrderedJSONObject createIssue(String ProjectName, String projectKey, String projectCategory, String name, String desc, String assignee, 
                                       JSONArray watcherList, String startdate, String enddate) throws Exception {
      System.out.println("In createIssue["+name+":"+desc+":"+assignee+"]");
      String error = "";
      String assigneeError = "";
      JSONArray watcherError = new JSONArray();
      OrderedJSONObject res = new OrderedJSONObject();
      Issue newIssue = null;

      // Get Jira Client
      JiraClient jira = getJiraClient();
      if(jira != null) {
         try {
            if(projectKey == null || projectKey.length() == 0) {
               projectKey = "SEC";
            }
            //String type = "11289";
            String type = "Epic";
            //String type = "Security Compliance";

            String watchers = watcherList.toString();
            watchers = watchers.replaceAll("[\\[\\]]", "");
            watchers = watchers.replaceAll("\"", "");

            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate ed = LocalDate.parse(enddate, df1);
            enddate = ed.format(df);

            /*
            String estimate = "0d";
            LocalDate currentDate = LocalDate.now();
            long days = ChronoUnit.DAYS.between(ed, currentDate);
            long weeks = ChronoUnit.WEEKS.between(ed, currentDate);
            long months = ChronoUnit.MONTHS.between(ed, currentDate);         
            long years = ChronoUnit.YEARS.between(ed, currentDate);
            System.out.println(days+":"+weeks+":"+months+":"+years);
            if(years > 0) {
               estimate = years+"y";
            } else if(months > 0) {
               estimate = months+"m";
            } else if(weeks > 0) {
               estimate = weeks+"w";
            } else if(days > 0) {
               estimate = days+"d";
            }
            */

            //557058:bbfaf9a7-b18b-47d4-a640-a094f4a4d8c3
            // Create Issue
            // Summary & Reporter
            newIssue = jira.createIssue(projectKey, type)
            .field("customfield_10006", name)
            .field(Field.SUMMARY, name)
            .field(Field.DESCRIPTION, desc)
            //.field(Field.ASSIGNEE, "557058")
            .field(Field.PRIORITY, "High")
            //.field(Field.CREATED_DATE, startdate)
            .field(Field.DUE_DATE, enddate)
            //.field(Field.TIME_ESTIMATE, estimate)
            //.field(Field.PROJECT, ProjectName)
            //.field(Field.DUE_DATE, enddate)
            //.field(Field.WATCHES, watchers)
            //.field(Field.REPORTER, "jpatel")
            .execute();

            // Add Comment
            newIssue.addComment("Test Comment");
            newIssue.update();

            //String vijay = "557058:54d41bba-cbf7-4730-acd3-e7746ce46808";
            //String bharat = "557058:bbfaf9a7-b18b-47d4-a640-a094f4a4d8c3";

            //String bharat = "bbfaf9a7b18b47d4a640a094f4a4d8c3";
            /*
            newIssue.transition()
            .field(Field.ASSIGNEE, "bbfaf9a7-b18b-47d4-a640-a094f4a4d8c3")
            .execute("To Do");
            */
            /*
            Map<String, String> stringMap = new HashMap<String, String>(); 
            stringMap.put("name", assignee); 
            stringMap = Collections.unmodifiableMap(stringMap);

            newIssue.transition()
            .field(Field.ASSIGNEE, stringMap)
            .execute(TRANSITION_ASSIGN_ASSIGNEE);
            */
            
            /*
            newIssue.update()
            .field(Field.ASSIGNEE, Field.valueById(vijay))
            .execute();

            OrderedJSONObject ext = new OrderedJSONObject();
            if(watcherList != null) {
               ext.put("watcher_list", watcherList);
            }
            */

            /*
            try {
               //newIssue.addWatcher(vijay);
               newIssue.update().field(Field., Field.valueById(vijay)).execute();
            } catch(Exception ex) {
               watcherError.add(ex.getMessage());
            }
            */

            /*
            // Add Watcher
            Iterator iter = watcherList.iterator();
            while(iter.hasNext()) {
               String watcher = (String)iter.next();
               try {
                  newIssue.addWatcher(watcher);
               } catch(Exception ex) {
                  watcherError.add(ex.getMessage());
               }
            }
            */

            try {
               //newIssue.addWatcher(bharat);
               //newIssue.update().field(Field.WATCHES, Field.valueById(vijay)).execute();
            } catch(Exception ex) {
               watcherError.add(ex.getMessage());
            }
            
            // Add Entry into Jira Table
            /*
            OrderedJSONObject dataobject = new OrderedJSONObject();
            UUID uuid = UUID.randomUUID();
            String id = uuid.toString();
            dataobject.put("1", id);
            dataobject.put("2", ProjectName);
            dataobject.put("3", "");
            dataobject.put("4", projectKey);
            dataobject.put("5", projectCategory);
            dataobject.put("6", desc);
            dataobject.put("7", newIssue.toString());
            dataobject.put("8", desc);
            dataobject.put("9", "To Do");
            dataobject.put("10", "");
            dataobject.put("11", assignee);
            dataobject.put("12", startdate);
            dataobject.put("13", enddate);
            dataobject.put("14", ext.toString());
            dataobject.put("15", "");
            dataobject.put("16", companyCode);
            */
         } catch(Exception ex) {
            ex.printStackTrace();
            error = ex.getMessage();
         }

         // Assign 
         /*
         try {
         newIssue.transition()
         .field(Field.ASSIGNEE, assignee)
         .execute("To Do");

         newIssue.update().field(Field.ASSIGNEE, assignee);
         } catch(Exception ex) {
         assigneeError = ex.getMessage();
         }
         */
         /*
         // Add Watcher
         Iterator iter = watcherList.iterator();
         while(iter.hasNext()) {
         String watcher = (String)iter.next();
         try {
         newIssue.addWatcher(watcher);
         } catch(Exception ex) {
         watcherError.add(ex.getMessage());
         }
         }
         */
         res.put("Issue", newIssue.toString());
         res.put("error", error);
         res.put("assigneeError", assigneeError);
         res.put("watcherError", watcherError);
      } else {
         res.put("STATUS", "ERROR CONNECTING TO JIRA INSTANCE!!!");
      }
      System.out.println("Successfully created Jira Issue"+res.toString());
      return res;
   } 

   public JSONArray getProjectList(String projKey, LinkedHashSet projCategory) throws Exception {
      System.out.println("getProjectList:"+projKey+":"+projCategory);
      JSONArray res = new JSONArray();
      JiraClient jira = getJiraClient();
      RestClient rest = jira.getRestClient();
      List projectList = jira.getProjects();
      System.out.println("Projects:"+projectList.size());
      //List projectList = Project.getAll(rest);
      Iterator iter = projectList.iterator();
      while(iter.hasNext()) {
         OrderedJSONObject projObj = new OrderedJSONObject();
         Project project = (Project)iter.next();
         String key = project.getKey();
         String name = project.getName();
         String desc = project.getDescription();
         User user = project.getLead();
         String lead = "", id="";
         if(user != null) {
            lead = user.getName();
            id = user.getId();
         }
         System.out.println(lead+":"+id);
         ProjectCategory pc = project.getCategory();
         String category = "";
         if(pc != null) {
            category = pc.getName();
         }
         if(projKey.equalsIgnoreCase(key)) {
            projObj.put("name", name);
            projObj.put("key", key);
            projObj.put("desc", desc);
            projObj.put("category", category);
            projObj.put("lead", lead);
            res.add(projObj);
         }

         System.out.println("Key:"+key+" Name:"+name+" Category:"+category);
      }
      System.out.println("getProjectList:"+res.toString());
      return res;
   }

   public JSONArray getEpicList(String projectKey) throws Exception {
		System.out.println("In getEpicList..."+projectKey);
		JSONArray res = new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
  
		JiraClient jira = getJiraClient();
		if(jira != null) {
			Issue.SearchResult sr = jira.searchIssues("project="+projectKey+" and issueType = Epic and status != 'Done'", "key,name,description,status,customfield_10006");
			Iterator iter = sr.iterator();
			while (iter.hasNext()) {
				Issue jiraIssue = (Issue)iter.next();
				String key = jiraIssue.getKey();
				String name = (String)jiraIssue.getField("customfield_10006");
				String desc = jiraIssue.getDescription();
				String status = "";
				if(jiraIssue.getStatus() != null) {
					status = jiraIssue.getStatus().getName();
				}
				if(desc == null) {
					desc = "";
				}
				if(status == null) {
					status = "";
				}
			
				/*
				AgileClient agile = new AgileClient(jira);
				net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
				String name = issue.getName();
				*/

				OrderedJSONObject issueObj = new OrderedJSONObject();
				issueObj.put("key", key);
				issueObj.put("name", name);
				issueObj.put("desc", desc);
				issueObj.put("status", status);
				res.add(issueObj);
			}
         System.out.println(res.toString());
			System.out.println("getIssueList:"+res.size());
		}

		return res;
	}

   public JSONArray getIssuesList(String projectKey, String searchText) throws Exception {
      System.out.println("In listIssues...");
      JSONArray res = new JSONArray();
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      /*
      JiraClient jira = getJiraClient();
      AgileClient agile = new AgileClient(jira);
      Issue.SearchResult sr = jira.searchIssues("project="+projectKey+" and Summary ~ '"+searchText+"'");
      System.out.println("Total: " + sr.total);
      Iterator iter = sr.iterator();
      while (iter.hasNext()) {
         Issue jiraIssue = (Issue)iter.next();
         String id = jiraIssue.getId();
         String key = jiraIssue.getKey();
         String desc = jiraIssue.getDescription();
         String assignee = "", userId = "";
         if(jiraIssue.getAssignee() != null) {
            User user = jiraIssue.getAssignee();
            assignee = user.getDisplayName();
         }
         System.out.println("Assignee:"+assignee+":"+userId);
         String status = jiraIssue.getStatus().getName();
         String priority = jiraIssue.getPriority().getName();
         int subtaskCount = jiraIssue.getSubtasks().size();
         String createDate = "";
         if(jiraIssue.getCreatedDate() != null) {
            createDate = sdf.format(jiraIssue.getCreatedDate());
         }
         String expiryDate = "";
         if(jiraIssue.getDueDate() != null) {
            expiryDate = sdf.format(jiraIssue.getDueDate());
         }

         net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
         String name = issue.getName();
         String sprintName="", sprintStartdate="", sprintEnddate="";
         int sprintIssueCount=0;
         if(issue.getSprint() != null) {
            Sprint sprint = issue.getSprint();
            sprintName = sprint.getName();
            if(sprint.getStartDate() != null) {
               sprintStartdate = sdf.format(sprint.getStartDate());
            }
            if(sprint.getEndDate() != null) {
               sprintEnddate = sdf.format(sprint.getEndDate());
            }
            sprintIssueCount = sprint.getIssues().size();
         }
         
         TimeTracking tt = null;
         String timeEstimate="", timeSpent="";
         if(issue.getTimeTracking() != null) {
             tt = issue.getTimeTracking();
             timeEstimate = tt.getOriginalEstimate();
             timeSpent = tt.getTimeSpent();
         }

         Resolution resolution = null;
         String resName = "", resDesc = "";
         if(issue.getResolution() != null) {
            resolution = issue.getResolution();
            resName = resolution.getName();
            resDesc = resolution.getDescription();
         }

         OrderedJSONObject ext = new OrderedJSONObject();
         ext.put("sprint_name", sprintName);
         ext.put("sprine_startdate", sprintStartdate);
         ext.put("sprint_enddate", sprintEnddate);
         ext.put("sprint_issue_count", new Integer(sprintIssueCount));
         ext.put("time_estimate", timeEstimate);
         ext.put("time_spent", timeSpent);
         ext.put("resolution_name", resName);
         ext.put("resolutiom_desc", resDesc);

         OrderedJSONObject issueObj = new OrderedJSONObject();
         issueObj.put("name", name);
         issueObj.put("key", key);
         issueObj.put("desc", desc);
         issueObj.put("assignee", assignee);
         issueObj.put("status", status);
         issueObj.put("priority", priority);
         issueObj.put("subtask_count", new Integer(subtaskCount));
         issueObj.put("create_date", createDate);
         issueObj.put("expiry_date", expiryDate);
         issueObj.put("time_estimate", timeEstimate);
         issueObj.put("time_spent", timeSpent);
         issueObj.put("resolution_name", resName);
         issueObj.put("resolution_desc", resDesc);
         issueObj.put("sprint_name", sprintName);
         issueObj.put("sprint_startdate", sprintStartdate);
         issueObj.put("sprint_enddate", sprintEnddate);
         issueObj.put("sprint_issue_count", new Integer(sprintIssueCount));

         res.add(issueObj);
         
         System.out.println("Id:"+id+" Issue Name:"+name);
      }

      System.out.println("getIssueList:"+res.size());

      return res;
      */

      JiraClient jira = getJiraClient();
		RestClient rc = jira.getRestClient();
		URI uri = null;
      if(searchText != null && searchText.length() > 0) {
         uri = createSearchURI("rest/api/latest/search", rc, "project="+projectKey+" and Summary ~ '"+searchText+"'", "key,summary,issuetype,status,assignee");
      } else {
         uri = createSearchURI("rest/api/latest/search", rc, "project="+projectKey, "key,summary,issuetype,status,watchers");
      }
		JSON result = rc.get(uri);
		JSONObject obj = (JSONObject)result;
		net.sf.json.JSONArray issueList = (net.sf.json.JSONArray)obj.get("issues");
		System.out.println("Total: " + issueList.size());
		Iterator iter = issueList.iterator();
		while (iter.hasNext()) {
         JSONObject issue = (JSONObject)iter.next();
			System.out.println(issue.toString());
			String id=null, issueKey=null, issueName=null, issueType=null, status=null;
			Iterator iter1 = issue.keySet().iterator();
			while(iter1.hasNext()) {
				String key = (String)iter1.next();
				if(key.equals("key")) {
					issueKey = (String)issue.get(key);
				} else if(key.equals("fields")) {
					JSONObject fieldList = (JSONObject)issue.get("fields");
					if(fieldList.containsKey("summary")) {
						issueName = (String)fieldList.get("summary");
					} 
					if(fieldList.containsKey("issuetype")) {
						JSONObject iObj = (JSONObject)fieldList.get("issuetype");
						issueType = (String)iObj.get("name");
					} 

					if(fieldList.containsKey("status")) {
						JSONObject sObj = (JSONObject)fieldList.get("status");
						status = (String)sObj.get("name");
					} 
            }
         }
         System.out.println(issueType);
         /*
         OrderedJSONObject issueObj = new OrderedJSONObject();
         issueObj.put("key", issueKey);
         issueObj.put("name", issueName);
         issueObj.put("status", status);
         issueObj.put("type", issueType);

         res.add(issueObj);
         */
		}
			
		System.out.println("getIssuesList:"+res.size());
		
		return res;
   }

   public OrderedJSONObject getIssueDetail(JSONArray issueList) throws Exception {
		System.out.println("In getIssueDetail..."+issueList);
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		OrderedJSONObject res = new OrderedJSONObject();
		if(issueList != null && issueList.size() > 0) {
			String issues = issueList.toString();
			issues = issues.replaceAll("[\\[\\]]", "");
			issues = issues.replaceAll("\"","");
			JiraClient jira = getJiraClient();
			if(jira != null) {
            JSONArray list = new JSONArray();
				AgileClient agile = new AgileClient(jira);
				Issue.SearchResult sr = jira.searchIssues("issueKey in ("+issues+")");
				System.out.println("Total: " + sr.total);
				Iterator iter = sr.iterator();
				while (iter.hasNext()) {
					Issue jiraIssue = (Issue)iter.next();
					String id = jiraIssue.getId();
					String key = jiraIssue.getKey();
					String desc = jiraIssue.getDescription();
					Issue parentIssue = jiraIssue.getParent();
					String parentKey = "";
					if(parentIssue != null) {
						parentKey = parentIssue.getKey();
					}
					List commentList = jiraIssue.getComments();
					String comment = commentList.toString();
					comment = comment.replaceAll("[\\[\\]]", "");

					Project project = jiraIssue.getProject();
               List issueTypes = project.getIssueTypes();
               System.out.println("issue types:"+issueTypes.toString());
               /*
               Iterator iter1 = issueTypes.iterator();
               while(iter1.hasNext()) {
                  
               }
               */
					String projectName = project.getName();
					String projectKey = project.getKey();
					ProjectCategory pc = project.getCategory();
					String projectCategory = "";
					if(pc != null) {
						projectCategory = pc.getName();
					}
					IssueType issueType = jiraIssue.getIssueType();
					String type = "Task";
					if(issueType != null) {
						type = issueType.getName();
					}
					String assignee = "";
					if(jiraIssue.getAssignee() != null) {
						assignee = jiraIssue.getAssignee().getDisplayName();
					}
					String status = jiraIssue.getStatus().getName();
					String priority = jiraIssue.getPriority().getName();
					//int subtaskCount = jiraIssue.getSubtasks().size();
					String createDate = "";
					if(jiraIssue.getCreatedDate() != null) {
						createDate = sdf.format(jiraIssue.getCreatedDate());
					}
					String expiryDate = "";
					if(jiraIssue.getDueDate() != null) {
						expiryDate = sdf.format(jiraIssue.getDueDate());
					}
					String watchers = "";
					Watches watches = jiraIssue.getWatches();
					if(watches != null) {
						List watcherList = watches.getWatchers();
						watchers = watcherList.toString();
						watchers = watchers.replaceAll("[\\[\\]]", "");
					}
					
					net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
					String name = issue.getName();
					/*
					String sprintName="", sprintStartdate="", sprintEnddate="";
					int sprintIssueCount=0;
					if(issue.getSprint() != null) {
						Sprint sprint = issue.getSprint();
						sprintName = sprint.getName();
						if(sprint.getStartDate() != null) {
							sprintStartdate = sdf.format(sprint.getStartDate());
						}
						if(sprint.getEndDate() != null) {
							sprintEnddate = sdf.format(sprint.getEndDate());
						}
						sprintIssueCount = sprint.getIssues().size();
					}
					
					TimeTracking tt = null;
					String timeEstimate="", timeSpent="";
					if(issue.getTimeTracking() != null) {
						tt = issue.getTimeTracking();
						timeEstimate = tt.getOriginalEstimate();
						timeSpent = tt.getTimeSpent();
					}
			
					Resolution resolution = null;
					String resName = "", resDesc = "";
					if(issue.getResolution() != null) {
						resolution = issue.getResolution();
						resName = resolution.getName();
						resDesc = resolution.getDescription();
					}
			
					OrderedJSONObject ext = new OrderedJSONObject();
					ext.put("sprint_name", sprintName);
					ext.put("sprine_startdate", sprintStartdate);
					ext.put("sprint_enddate", sprintEnddate);
					ext.put("sprint_issue_count", new Integer(sprintIssueCount));
					ext.put("time_estimate", timeEstimate);
					ext.put("time_spent", timeSpent);
					ext.put("resolution_name", resName);
					ext.put("resolutiom_desc", resDesc);
					*/
					
					OrderedJSONObject issueObj = new OrderedJSONObject();
					issueObj.put("project_key", projectKey);
					issueObj.put("project_name", projectName);
					issueObj.put("project_category", projectCategory);
					issueObj.put("name", name);
					issueObj.put("key", key);
					issueObj.put("desc", desc);
					issueObj.put("type", type);
					issueObj.put("assignee", assignee);
					issueObj.put("status", status);
					issueObj.put("priority", priority);
					issueObj.put("create_date", createDate);
					issueObj.put("expiry_date", expiryDate);
					issueObj.put("comment", comment);
					issueObj.put("parent_key", parentKey);
					issueObj.put("watchers", watchers);
					list.add(issueObj);
					/*
					res.put("subtask_count", new Integer(subtaskCount));
					res.put("time_estimate", timeEstimate);
					res.put("time_spent", timeSpent);
					res.put("resolution_name", resName);
					res.put("resolution_desc", resDesc);
					res.put("sprint_name", sprintName);
					res.put("sprint_startdate", sprintStartdate);
					res.put("sprint_enddate", sprintEnddate);
					res.put("sprint_issue_count", new Integer(sprintIssueCount));
					*/
					
					System.out.println("Id:"+id+" Issue Name:"+name);
				}
            
            res.put("issues_list", list);
				System.out.println("getIssuesList:"+list.size());
			}
		} else {
			System.out.println("Invalid Issue Key!!!");
		}
  
		return res;
	}

   public JSONArray getIssueDetailNew(JSONArray issueList) throws Exception {
      JSONArray res = new JSONArray();
      String issues = issueList.toString();
      issues = issues.replaceAll("[\\[\\]]", "");
      issues = issues.replaceAll("\"","");

      JiraClient jira = getJiraClient();
      RestClient rc = jira.getRestClient();
      URI uri = createSearchURI("rest/api/latest/search", rc, "issueKey in ("+issues+")", "parent,project,type,summary,description,status,priority,assignee,watchers,comment,created,due");
      JSON result = rc.get(uri);
      JSONObject obj = (JSONObject)result;
      net.sf.json.JSONArray  list = (net.sf.json.JSONArray)obj.get("issues");
      System.out.println("Total: " + issueList.size());
      Iterator iter = list.iterator();
      while (iter.hasNext()) {
         JSONObject issue = (JSONObject)iter.next();
         System.out.println(issue.toString());
         String id=null, parentKey=null, issueKey=null, issueName=null, issueType=null, status=null;
         Iterator iter1 = issue.keySet().iterator();
         while(iter1.hasNext()) {
            String key = (String)iter1.next();
            if(key.equals("key")) {
               issueKey = (String)issue.get(key);
            } else if(key.equals("fields")) {
               JSONObject fieldList = (JSONObject)issue.get("fields");
               if(fieldList.containsKey("summary")) {
                  issueName = (String)fieldList.get("summary");
               } 
               if(fieldList.containsKey("issuetype")) {
                  JSONObject iObj = (JSONObject)fieldList.get("type");
                  issueType = (String)iObj.get("name");
               } 
               if(fieldList.containsKey("status")) {
                  JSONObject sObj = (JSONObject)fieldList.get("status");
                  status = (String)sObj.get("name");
               } 
            }
         }

         OrderedJSONObject issueObj = new OrderedJSONObject();
         issueObj.put("key", issueKey);
         issueObj.put("name", issueName);
         issueObj.put("status", status);
         issueObj.put("type", issueType);

         res.add(issueObj);
      }
         
      System.out.println("getIssuesList:"+res.size());
      
		return res;
   }

   private String getIssueStatus(String key) throws Exception {
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

      // Get Jira Client
      JiraClient jira = getJiraClient();
      AgileClient agile = new AgileClient(jira);

      Issue jiraIssue = jira.getIssue(key);
      String id = jiraIssue.getId();
      String desc = jiraIssue.getDescription();
      IssueType type = jiraIssue.getIssueType();
      String assignee = "";
      if(jiraIssue.getAssignee() != null) {
         assignee = jiraIssue.getAssignee().getDisplayName();
      }
      String status = jiraIssue.getStatus().getName();
      String priority = jiraIssue.getPriority().getName();
      int subtaskCount = jiraIssue.getSubtasks().size();
      String createDate = "";
      if(jiraIssue.getCreatedDate() != null) {
         createDate = sdf.format(jiraIssue.getCreatedDate());
      }
      String expiryDate = "";
      if(jiraIssue.getDueDate() != null) {
         expiryDate = sdf.format(jiraIssue.getDueDate());
      }
      List commentList = jiraIssue.getComments();
      Comment comment = (Comment)commentList.get(0);

      System.out.println(comment.getBody());
      System.out.println(desc+":"+status+":"+expiryDate+":"+type.getName()+":"+assignee);

      net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
      String sprintName="", sprintStartdate="", sprintEnddate="", sprintState="";
      int sprintIssueCount=0;
      String name = issue.getName();

      if(issue.getSprint() != null) {
         Sprint sprint = issue.getSprint();
         sprintName = sprint.getName();
         sprintState = sprint.getState();
         if(sprint.getStartDate() != null) {
            sprintStartdate = sdf.format(sprint.getStartDate());
         }
         if(sprint.getEndDate() != null) {
            sprintEnddate = sdf.format(sprint.getEndDate());
         }
         sprintIssueCount = sprint.getIssues().size();
      }
      
      TimeTracking tt = null;
      String timeEstimate="", timeSpent="";
      if(issue.getTimeTracking() != null) {
         tt = issue.getTimeTracking();
         timeEstimate = tt.getOriginalEstimate();
         timeSpent = tt.getTimeSpent();
      }

      Resolution resolution = null;
      String resName = "", resDesc = "";
      if(issue.getResolution() != null) {
         resolution = issue.getResolution();
         resName = resolution.getName();
         resDesc = resolution.getDescription();
      }
      
      System.out.println("Jira Status=>"+name+":"+status);
      System.out.println(desc+":"+createDate+":"+expiryDate);
		return status;
	}

   public void sync() throws Exception {
      System.out.println("In sync...");
      String insertsql = "insert into enterprise_jira(id, project_name, project_key, project_desc, project_category, issue_name, issue_key, issue_desc, issue_status,"+
      "issue_priority,issue_assignee,issue_startdate,issue_enddate,extension,created_by,company_code) values(uuid(), ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      String updatesql = "update enterprise_jira set issue_desc=?, issue_status=?,issue_priority=?,issue_startdate=?,issue_enddate=?,extension=? where id = ?";                     
      String checksql = "select id from enterprise_jira where project_name = ? and issue_name = ?";
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      JiraClient jira = getJiraClient();
      AgileClient agile = new AgileClient(jira);
      String projName = "Security";
      String projKey = "SEC";
      LinkedHashSet projCategory = new LinkedHashSet<>();
      //projCategory.add("Security Compliance");
     //projCategory.add("physec");
      //projCategory.add("global it");

      JSONArray projectList = getProjectList(projKey, projCategory);
      Iterator projIter = projectList.iterator();
      while(projIter.hasNext()) {
         OrderedJSONObject projObj = (OrderedJSONObject)projIter.next();
         String projectName = (String)projObj.get("name");
         String projectKey = (String)projObj.get("key");
         String projectDesc = (String)projObj.get("desc");
         String projectCategory = (String)projObj.get("category");
         String projectLead = (String)projObj.get("lead");
         System.out.println(projectName+":"+projectDesc);
         Issue.SearchResult sr = jira.searchIssues("project = "+projectKey);
         if(sr != null) {
            System.out.println("Total: " + sr.total);
            Iterator iter = sr.iterator();
            while (iter.hasNext()) {
               Issue jiraIssue = (Issue)iter.next();
               String id = jiraIssue.getId();
               //String name = jiraIssue.getName();
               String key = jiraIssue.getKey();
               String desc = jiraIssue.getDescription();
               String assignee = "";
               if(jiraIssue.getAssignee() != null) {
                  assignee = jiraIssue.getAssignee().getDisplayName();
               }
               String status = jiraIssue.getStatus().getName();
               String priority = jiraIssue.getPriority().getName();
               int subtaskCount = jiraIssue.getSubtasks().size();
               String createDate = "";
               if(jiraIssue.getCreatedDate() != null) {
                  createDate = sdf.format(jiraIssue.getCreatedDate());
               }
               String expiryDate = "";
               if(jiraIssue.getDueDate() != null) {
                  expiryDate = sdf.format(jiraIssue.getDueDate());
               }

               net.rcarz.jiraclient.agile.Issue issue = (net.rcarz.jiraclient.agile.Issue)agile.getIssue(key);
               String name = issue.getName();
               String sprintName="", sprintStartdate="", sprintEnddate="", sprintState="";
               int sprintIssueCount=0;
               if(issue.getSprint() != null) {
                  Sprint sprint = issue.getSprint();
                  sprintName = sprint.getName();
                  sprintState = sprint.getState();
                  if(sprint.getStartDate() != null) {
                     sprintStartdate = sdf.format(sprint.getStartDate());
                  }
                  if(sprint.getEndDate() != null) {
                     sprintEnddate = sdf.format(sprint.getEndDate());
                  }
                  sprintIssueCount = sprint.getIssues().size();
               }
               
               TimeTracking tt = null;
               String timeEstimate="", timeSpent="";
               if(issue.getTimeTracking() != null) {
                  tt = issue.getTimeTracking();
                  timeEstimate = tt.getOriginalEstimate();
                  timeSpent = tt.getTimeSpent();
               }

               Resolution resolution = null;
               String resName = "", resDesc = "";
               if(issue.getResolution() != null) {
                  resolution = issue.getResolution();
                  resName = resolution.getName();
                  resDesc = resolution.getDescription();
               }

               OrderedJSONObject ext = new OrderedJSONObject();
               ext.put("sprint_name", sprintName);
               ext.put("sprint_state", sprintState);
               ext.put("sprint_startdate", sprintStartdate);
               ext.put("sprint_enddate", sprintEnddate);
               ext.put("sprint_issue_count", new Integer(sprintIssueCount));
               ext.put("time_estimate", timeEstimate);
               ext.put("time_spent", timeSpent);
               ext.put("resolution_name", resName);
               ext.put("resolutiom_desc", resDesc);
               ext.put("project_lead", projectLead);
               System.out.println("Sprint Detail:"+ext.toString());

               // Check Issue
               String issueId = null;
               PreparedStatement pst = conn.prepareStatement(checksql);
               pst.setString(1, projectName);
               pst.setString(2, name);
               ResultSet rs = pst.executeQuery();
               while(rs.next()) {
                  issueId = rs.getString("id");
               }
               rs.close();
               pst.close();
               System.out.println(projectName+":"+projectDesc+":"+name+":"+status);
               if(issueId == null) {
                  pst = conn.prepareStatement(insertsql);
                  pst.setString(1, projectName);
                  pst.setString(2, projectKey);
                  pst.setString(3, projectDesc);
                  pst.setString(4, projectCategory);
                  pst.setString(5, name);
                  pst.setString(6, key);
                  pst.setString(7, desc);
                  pst.setString(8, status);
                  pst.setString(9, priority);
                  pst.setString(10, assignee);
                  pst.setString(11, createDate);
                  pst.setString(12, expiryDate);
                  pst.setString(13, ext.toString());
                  pst.setString(14, "support@smarterd.com");
                  pst.setString(15, companyCode);
                  System.out.println(pst.toString());
                  pst.executeUpdate();
                  pst.close();
               } else {
                  pst = conn.prepareStatement(updatesql);
                  pst.setString(1,desc);
                  pst.setString(2, status);
                  pst.setString(3, priority);
                  pst.setString(4, createDate);
                  pst.setString(5, expiryDate);
                  pst.setString(6, ext.toString());
                  pst.setString(7, issueId);
                  System.out.println(pst.toString());
                  pst.executeUpdate();
                  pst.close();
               }
            }
         }
      }
      conn.commit();
      System.out.println("Successfully synced Jira!!!");
   }

   private void mysqlConnect(String dbname) throws Exception {
      String userName = "root", password = "smarterD2018!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
     conn.setAutoCommit(false);
     System.out.println("Successfully connected to Mysql DB:"+dbname);
  }

  private String getFiscalYear(String startdate, String company) throws Exception {
      String fiscalYear=null, fiscalyearStart=null;
      String sql = "select fiscal_year, fiscal_year_start from SE.enterprise where company_code = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, company.toUpperCase());
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         fiscalYear = rs.getString("fiscal_year");
         fiscalyearStart = rs.getString("fiscal_year_start");
      }
      rs.close();
      pst.close();
      
      LocalDate currentDate = LocalDate.now();
      String currentYear = Integer.toString(currentDate.getYear());
   
      if(fiscalyearStart == null || fiscalyearStart.length() == 0) {
         fiscalyearStart = "01/01";
      }
      if(fiscalYear == null || fiscalYear.length() == 0) {
         fiscalYear = "FY"+currentYear.substring(2);
      }
      String year = fiscalYear.substring(2);
      System.out.println("Year:"+year);
      if(startdate != null && startdate.length() > 0) {
         DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");

         LocalDate firstDate = LocalDate.parse(startdate, df);
         LocalDate secondDate = LocalDate.parse(fiscalyearStart+"/20"+year, df);
         System.out.println("First Date:"+firstDate+" Second Date:"+secondDate);

         long diff = ChronoUnit.YEARS.between(secondDate, firstDate);
         System.out.println("Diff:"+diff);

         if(diff > 0) {
            int y = Integer.parseInt(year) + (int)diff;
            String fy = "FY"+Integer.toString(y);
            fiscalYear = fy;
         } 
      }

      return fiscalYear;
   }

   private JiraClient getJiraClient() throws Exception {
      JiraClient client = null;
      /*
      String username = "vsundhar@smarterd.com";
      String password = "oPCp7JvIjGhYL88AZJhw10B3";
      String url = "https://smarterd.atlassian.net/";
      */

      // Flexport
      String username = "jpatel@flexport.com";
      String password = "LB2z7VvZN0ooLKeSadbT68B1";
      String url = "https://flexport.atlassian.net/";
      
      if(url != null && url.trim().length() > 0) {
			BasicCredentials creds = new BasicCredentials(username, password);
			client = new JiraClient(url, creds);
      }
      System.out.println("Successfully connected to "+companyCode+" Jira Instance!!!");
		return client;
   }

   protected static URI createSearchURI(String baseUri, RestClient restclient, String jql, String includedFields) throws Exception {
      Map<String, String> queryParams = new HashMap<String, String>();
      queryParams.put("jql", jql);
      if (includedFields != null) {
          queryParams.put("fields", includedFields);
      }

      URI searchUri = restclient.buildURI(baseUri, queryParams);
      return searchUri;
  }
   
   public static void main(String args[]) throws Exception {
      String companyCode = args[0];
      String env = args[1];
      //String projectKey = "SEC";
      //String projectKey = "SEP";
      //String projectName = "Platform";

      String projectKey = "SEC";
      String projectName = "Security";
      SmarteJira jira = new SmarteJira(companyCode, env);

      //LinkedHashSet list = new LinkedHashSet<>();
      //jira.getProjectList(projectKey, list);

      //jira.getEpicList(projectKey);

      //jira.getIssuesList(projectKey, "");

      JSONArray issueList = new JSONArray();
      issueList.add("SEC-147");
      //issueList.add("SEC-324");
      //jira.getIssueDetail(issueList);
      
      //jira.sync();

      //String issueKey = "SEC-2738";
      //String issueKey = "SEP-143";
      //String status = jira.getIssueStatus(issueKey);
      
      String projectCategory = "Security";
      String name = "SmarterD Test Issue-2";
      String desc = "SmarterD Test Issue-2";
      String assignee = "";
      JSONArray watcherList = new JSONArray();
      //watcherList.add("vsundhar@smarterd.com");
      String startdate = "06/10/2020";
      String enddate = "06/15/2020";
      OrderedJSONObject res = jira.createIssue(projectName, projectKey, "", name, desc, assignee, watcherList, startdate, enddate);
   
      /*
      String fy = jira.getFiscalYear("06/01/2021", "FLEX");
      System.out.println(fy);
      */

      //{"1":"createIssue","2":"SEC","3":"Security","4":"SEC","5":"new ticket 2","6":"","7":"Epic","8":"","9":[],"10":"","11":"08/11/2021","12":"","13":"","14":"94705447-f4e9-47f4-ba44-b49352b46d3f","15":"PLAN","16":"FLEX"
   }
}