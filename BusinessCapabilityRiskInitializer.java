import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.*;
import java.text.*;

public class BusinessCapabilityRiskInitializer {
    private String hostname = "localhost";
    private String companyCode = null, domain=null;
    private Connection conn = null;

    public BusinessCapabilityRiskInitializer(String companyCode, String domain, String env) throws Exception {
        this.companyCode = companyCode;
        this.domain = domain;
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    public void initializeRiskValues() throws Exception {
        String selectSQL = "select id, business_capability_ext from enterprise_business_capability where business_capability_domain = ?";
        String selectRootSQL = "select id, business_capability_ext from enterprise_business_capability where id = ? and business_capability_domain = ?";
        String selectSESQL = "select business_capability_ext from SE.enterprise_business_capability where id = ?";
        String selectRootSESQL = "select business_capability_ext from SE.enterprise_business_capability where id = ?";
        String updateSQL = "update enterprise_business_capability set business_capability_ext = ? where id = ?";
        String updateRootSQL = "update enterprise_business_capability set business_capability_ext = ? where id = ?";
        String updateSESQL = "update SE.enterprise_business_capability set business_capability_ext = ? where id = ?";
        String updateRootSESQL = "update SE.enterprise_business_capability set business_capability_ext = ? where id = ?";

        PreparedStatement pst = conn.prepareStatement(selectSQL);
        pst.setString(1, domain);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            String id = rs.getString("id");
            String ext = rs.getString("business_capability_ext");
            OrderedJSONObject extObj = new OrderedJSONObject(ext);

            // Get SE Detail
            String seExt = null;
            OrderedJSONObject seExtObj = null;
            PreparedStatement pst1 = conn.prepareStatement(selectSESQL);
            pst1.setString(1, id);
            ResultSet rs1 = pst1.executeQuery();
            while(rs1.next()) {
                seExt = rs.getString("business_capability_ext");
            }
            rs1.close();
            pst1.close();

            if(seExt != null && seExt.length() > 0) {
                seExtObj = new OrderedJSONObject(seExt);
            } else {
                seExtObj = new OrderedJSONObject();
            }

            // Control Score
            int riskScore = (int)(Math.random()*100);
            extObj.put("risk", new Integer(riskScore));
            pst1 = conn.prepareStatement(updateSQL);
            pst1.setString(1, extObj.toString());
            pst1.setString(2, id);
            pst1.executeUpdate();
            pst1.close();
            System.out.println("Updated Control Risk:"+id+":"+riskScore);
            
            // Average Score across all customers for this control
            int overallRiskScore = (int)(Math.random()*100);
            seExtObj.put("risk", new Integer(overallRiskScore));
            pst1 = conn.prepareStatement(updateSESQL);
            pst1.setString(1, seExtObj.toString());
            pst1.setString(2, id);
            pst1.executeUpdate();
            pst1.close();
            System.out.println("Updated Overall Control SE:"+id+":"+overallRiskScore);
            System.out.println("--------------------------");
        }
        rs.close();
        pst.close();

        // Overall Score across all the controls
        String ext = null;
        OrderedJSONObject extObj = null;
        pst = conn.prepareStatement(selectRootSQL);
        pst.setString(1, domain);
        pst.setString(2, domain);
        rs = pst.executeQuery();
        while(rs.next()) {
            ext = rs.getString("business_capability_ext");
        }
        rs.close();
        pst.close();

        if(ext != null && ext.length() > 0) {
            extObj = new OrderedJSONObject(ext);
        } else {
            extObj = new OrderedJSONObject();
        }

        int p1 = (int)(Math.random()*100);
        int p2 = (int)(Math.random()*100);
        int p3 = (int)(Math.random()*100);
        int p4 = (int)(Math.random()*100);
        int p5 = (int)(Math.random()*100);
        int p6 = (int)(Math.random()*100);
        int p7 = (int)(Math.random()*100);
        int p8 = (int)(Math.random()*100);
        int p9 = (int)(Math.random()*100);
        int p10 = (int)(Math.random()*100);
        int p11 = (int)(Math.random()*100);
        int p12 = (int)(Math.random()*100);
        OrderedJSONObject periodHistory = new OrderedJSONObject();
        OrderedJSONObject fy = new OrderedJSONObject();
        fy.put("Jan", new Integer(p1));
        fy.put("Feb", new Integer(p2));        
        fy.put("Mar", new Integer(p3));
        fy.put("Apr", new Integer(p4));
        fy.put("May", new Integer(p5));
        fy.put("Jun", new Integer(p6));
        fy.put("Jul", new Integer(p7));
        fy.put("Aug", new Integer(p8));
        fy.put("Sep", new Integer(p9));
        fy.put("Oct", new Integer(p10));
        fy.put("Nov", new Integer(p11));
        fy.put("Dec", new Integer(p12));
        fy.put("current", "Dec");
        periodHistory.put("FY18", fy);

        p1 = (int)(Math.random()*100);
        p2 = (int)(Math.random()*100);
        p3 = (int)(Math.random()*100);
        p4 = (int)(Math.random()*100);
        p5 = (int)(Math.random()*100);
        p6 = (int)(Math.random()*100);
        p7 = (int)(Math.random()*100);
        fy = new OrderedJSONObject();
        fy.put("Jan", new Integer(p1));
        fy.put("Feb", new Integer(p2));        
        fy.put("Mar", new Integer(p3));
        fy.put("Apr", new Integer(p4));
        fy.put("May", new Integer(p5));
        fy.put("Jun", new Integer(p6));
        fy.put("Jul", new Integer(p7));
        fy.put("current", "Jul");
        periodHistory.put("FY19", fy);

        extObj.put("period_history", periodHistory);

        pst = conn.prepareStatement(updateRootSQL);
        pst.setString(1,extObj.toString());
        pst.setString(2, domain);
        pst.executeUpdate();
        pst.close();
        System.out.println("Updated Root:"+domain+":"+extObj.toString());

        // Overall Score across all Customers
        ext = null;
        extObj = null;
        pst = conn.prepareStatement(selectRootSESQL);
        pst.setString(1, domain);
        rs = pst.executeQuery();
        while(rs.next()) {
            ext = rs.getString("business_capability_ext");
        }
        rs.close();
        pst.close();

        if(ext != null && ext.length() > 0) {
            extObj = new OrderedJSONObject(ext);
        } else {
            extObj = new OrderedJSONObject();
        }

        p1 = (int)(Math.random()*100);
        p2 = (int)(Math.random()*100);
        p3 = (int)(Math.random()*100);
        p4 = (int)(Math.random()*100);
        p5 = (int)(Math.random()*100);
        p6 = (int)(Math.random()*100);
        p7 = (int)(Math.random()*100);
        p8 = (int)(Math.random()*100);
        p9 = (int)(Math.random()*100);
        p10 = (int)(Math.random()*100);
        p11 = (int)(Math.random()*100);
        p12 = (int)(Math.random()*100);

        periodHistory = new OrderedJSONObject();
        fy = new OrderedJSONObject();
        fy.put("Jan", new Integer(p1));
        fy.put("Feb", new Integer(p2));        
        fy.put("Mar", new Integer(p3));
        fy.put("Apr", new Integer(p4));
        fy.put("May", new Integer(p5));
        fy.put("Jun", new Integer(p6));
        fy.put("Jul", new Integer(p7));
        fy.put("Aug", new Integer(p8));
        fy.put("Sep", new Integer(p9));
        fy.put("Oct", new Integer(p10));
        fy.put("Nov", new Integer(p11));
        fy.put("Dec", new Integer(p12));
        fy.put("current", "Dec");
        periodHistory.put("FY18", fy);

        p1 = (int)(Math.random()*100);
        p2 = (int)(Math.random()*100);
        p3 = (int)(Math.random()*100);
        p4 = (int)(Math.random()*100);
        p5 = (int)(Math.random()*100);
        p6 = (int)(Math.random()*100);
        p7 = (int)(Math.random()*100);
        fy = new OrderedJSONObject();
        fy.put("Jan", new Integer(p1));
        fy.put("Feb", new Integer(p2));        
        fy.put("Mar", new Integer(p3));
        fy.put("Apr", new Integer(p4));
        fy.put("May", new Integer(p5));
        fy.put("Jun", new Integer(p6));
        fy.put("Jul", new Integer(p7));
        fy.put("current", "Jul");
        periodHistory.put("FY19", fy);
        extObj.put("period_history", periodHistory);

        pst = conn.prepareStatement(updateRootSESQL);
        pst.setString(1,extObj.toString());
        pst.setString(2, domain);
        pst.executeUpdate();
        System.out.println("Updated SE Root:"+domain+":"+extObj.toString());
        pst.close();
        conn.commit();
        conn.close();
    }

    private void initializeSERiskValues() throws Exception {
        String insertRiskSQL = "insert into SE.enterprise_risk(id, comp_id,risk,extension,is_root,company_code) values(uuid(),?,?,?,?,?)";
        String selectRiskSQL = "select round(sum(json_extract(risk,'$.risk'))/count(*)) risk from SE.enterprise_risk where comp_id=?";
        String selectCISRiskSQL = "select round(sum(json_extract(risk,?))/count(*)) risk from SE.enterprise_risk where comp_id='CIS'";
        String updateRiskSQL = "update SE.enterprise_risk set average_risk=? where comp_id=?";

        OrderedJSONObject rObj = new OrderedJSONObject();
        OrderedJSONObject arObj = new OrderedJSONObject();
        OrderedJSONObject extObj = new OrderedJSONObject();
        String companyCode = "FLEX", compId=null, root="N";
        int r1=0,r2=0,r3=0,ar=0,min=0,max=0;
        String fiscalYear = "FY20";

        // Child Ids for Malware Family 
        String[] controlList = new String[8];
        controlList[0] = "5a74a56c-e358-4d38-a256-753b4cfa8085";
        controlList[1] = "671d0c7b-af66-44da-813b-7e6f74a1bfc5";
        controlList[2] = "a4c2f3e0-e94a-47cc-9aa3-a79e8cab90d3";
        controlList[3] = "a63eaae0-d214-4fc8-8cd8-c71023257104";
        controlList[4] = "abd08512-60ab-4dbf-b0e9-150df3dd853b";
        controlList[5] = "b462cfb3-9253-4ce2-b7cc-838ec420d87e";
        controlList[6] = "e6580c3d-7f71-488e-a7ec-1810aae818da";
        controlList[7] = "f0d0cee7-dd2f-427f-ae33-d7c6b55424a0";

        String[] companyList = new String[3];
        companyList[0] = "BCT";
        companyList[1] = "BKS";
        companyList[2] = "LITE";

        String[] month = new String[5];
        month[0]="Apr";
        month[1]="May";
        month[2]="Jun";
        month[3]="Jul";
        month[4]="Aug";

        PreparedStatement pst1 = conn.prepareStatement(insertRiskSQL);
        for(int i=0; i<companyList.length; i++) {
            companyCode = companyList[i];
            for(int j=0; j<controlList.length; j++) {
                compId = controlList[j];
                root = "N";
                int risk = (int)(Math.random()*100);
                rObj.put("risk", new Integer(risk));
                pst1.setString(1, compId);
                pst1.setString(2, rObj.toString());
                pst1.setString(3, extObj.toString());
                pst1.setString(4, root);
                pst1.setString(5, companyCode);
                pst1.addBatch();

                if (min == 0) {
                    min = risk;
                } else if(risk < min) {
                    min = risk;
                }

                if (max == 0) {
                    max = risk;
                } else if(risk > max) {
                    max = risk;
                }
            }
            root = "Y";
            compId = "CIS";
            OrderedJSONObject periodHistory = new OrderedJSONObject();
            OrderedJSONObject fy = new OrderedJSONObject();

            periodHistory.clear();
            fy.clear();
            rObj.clear();
            for(int k=0;k<month.length;k++) {
                String m = month[k];
                int risk = (int)(Math.random()*100);
                fy.put(m, new Integer(risk));
            }
            periodHistory.put(fiscalYear, fy);
            rObj.put("period_history", periodHistory);
            rObj.put("min", new Integer(min));
            rObj.put("max", new Integer(max));
            pst1.setString(1, compId);
            pst1.setString(2, rObj.toString());
            pst1.setString(3, extObj.toString());
            pst1.setString(4, root);
            pst1.setString(5, companyCode);
            pst1.addBatch();
        }
        pst1.executeBatch();
        pst1.close();
        conn.commit();

        PreparedStatement pst2 = conn.prepareStatement(updateRiskSQL);
        for(int j=0; j<controlList.length; j++) {
            compId = controlList[j];
            pst1 = conn.prepareStatement(selectRiskSQL);
            pst1.setString(1, compId);
            ResultSet rs = pst1.executeQuery();
            int risk = 0;
            arObj.clear();
            while(rs.next()) {
                risk = rs.getInt("risk");
            }
            arObj.put("risk", new Integer(risk));
            pst2.setString(1, arObj.toString());
            pst2.setString(2, compId);
            pst2.addBatch();
            rs.close();
            pst1.close();
        }
        
        arObj.clear();
        OrderedJSONObject periodHistory = new OrderedJSONObject();
        OrderedJSONObject fy = new OrderedJSONObject();
        compId = "CIS";
        for(int i=0; i<month.length; i++) {
            String m = month[i];
            String key = "$.period_history."+fiscalYear+"."+m;
            pst1 = conn.prepareStatement(selectCISRiskSQL);
            pst1.setString(1, key);
            ResultSet rs = pst1.executeQuery();
            int risk = 0;
            while(rs.next()) {
                risk = rs.getInt("risk");
            }
            rs.close();
            pst1.close();    
            fy.put(m, new Integer(risk));
        }
        periodHistory.put(fiscalYear, fy);
        arObj.put("period_history", periodHistory);
        pst2.setString(1, arObj.toString());
        pst2.setString(2, compId);
        pst2.addBatch();
       
        pst2.executeBatch();
        pst2.close();
        conn.commit();
    }

    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String dbname = args[0];
        String domain = args[1];
        String env = args[2];

        BusinessCapabilityRiskInitializer initializer = new BusinessCapabilityRiskInitializer(dbname, domain, env);
        //initializer.initializeRiskValues();
        initializer.initializeSERiskValues();
    }
}