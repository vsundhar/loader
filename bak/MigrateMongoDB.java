import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.bson.Document; 
import org.bson.types.ObjectId;
import org.bson.types.BasicBSONList;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.util.Date;

public class MigrateMongoDB {
	private CSVReader reader = null;
	private String companyCode = null;
	private MongoClient mongoClient = null;
    private MongoDatabase db = null;	
	private Connection conn = null;
	private String adapterType = "tenable";
	private Map groupList = new HashMap();
	private Map groupConfig = new HashMap();
    private Map groupAssetList = new HashMap();
	private Map groupCapabilityList = new HashMap();
	private String type = null;
	private List<DBObject> threatClassList = new ArrayList<DBObject>();
	private int currentCount = 0;
	private String createdAt = "";
	private String updatedAt = "";
	private String defaultGroupName = null;
	private List assetClassList = null;
	private List threatClassIdList = null;
	private List capabilityList = null;
	
	public MigrateMongoDB(String companyCode) throws Exception {
		this.companyCode = companyCode;
        
        // Connect Mongo
        mongoConnect(companyCode);
        
        // Connect Mysql
		mysqlConnect(companyCode);
	}

	public void migrateThreatPlan() throws Exception {
		int rowCount=0;
		String colName = "enterprise_threat_plan";		
		initialize(colName);
		
		String sql = "insert into enterprise_threat_plan values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("ThreatPlan");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Plans...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String name = doc.getString("name");
			String planType = doc.getString("planType");
			String desc = doc.getString("description");
			List capabilityList = null;
			if(doc.get("capability") != null) {
				 capabilityList = (ArrayList)doc.get("capability");
			}
			JSONArray capability = null;
			if(capabilityList != null && capabilityList.indexOf("[") != -1) {
				capability = new JSONArray(capabilityList);
			} else {
				capability = new JSONArray();
			}
			String domain = doc.getString("domain");
			String startdate = doc.getString("startDate");
			String enddate = doc.getString("endDate");
			String requestedEnddate = doc.getString("requestedEndDate");
			String owner = doc.getString("owner");
			String manager = doc.getString("manager");
			String compControls = doc.get("compensatingControls").toString();
			String extension = doc.get("extension").toString();
			String threatList = doc.get("threatList").toString();
			String status = doc.getString("status");
			String impl = doc.getString("implementation");
			String category = doc.getString("category");
			String completion = doc.getString("completion");
			String createdAt = doc.get("createdAt").toString();
			String updatedAt = doc.get("updatedAt").toString();

			pst.setString(1, id);
			pst.setString(2, name);
			pst.setString(3, planType);
			pst.setString(4, desc);
			pst.setString(5, capability.toString());
			pst.setString(6, domain);
			pst.setString(7, startdate);
			pst.setString(8, enddate);
			pst.setString(9, requestedEnddate);
			pst.setString(10, owner);
			pst.setString(11, manager);
			pst.setString(12, compControls);
			pst.setString(13, extension);
			pst.setString(14, threatList);
			pst.setString(15, status);
			pst.setString(16, impl);
			pst.setString(17, category);
			pst.setString(18, completion);
			pst.setString(19, "support@smarterd.com");
			pst.setString(20, "support@smarterd.com");
			pst.addBatch();
		}	

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated ThreatPlans!!!");
	}

	public void migrateSubPlan() throws Exception {
		String colName = "enterprise_threat_subplan";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_threat_subplan values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("SubPlan");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " SubPlans...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String planId = doc.get("threatPlan").toString();
			String subplanId = "";
			if(doc.containsKey("subplan")) {
				subplanId = doc.get("subplan").toString();
			}
			String name = doc.getString("name");
			String desc = doc.getString("description");
			String planType = "";
			if(doc.getString("planType") != null) {
				planType = doc.getString("planType");
			}
			if(planType.length() > 5) {
				planType = "";
			}
			String category = doc.getString("category");
			boolean root = doc.getBoolean("root");
			String testingProcedures = doc.getString("testingProcedures");
			String guidance = doc.getString("guidance");
			String startdate = doc.getString("startDate");
			String enddate = doc.getString("endDate");
			String requestedEnddate = doc.getString("requestedEndDate");
			String estimatedEnddate = doc.getString("estimatedEndDate");
			String status = doc.getString("status");
			String impl = doc.getString("implementation");
			String completion = doc.getString("completion");
			String extension = "{}";
			if(doc.get("extension") != null) {
				extension = doc.get("extension").toString();
			}
			List capabilityList = null;
			if(doc.get("capability") != null && doc.get("capability") instanceof List) {
				 capabilityList = (ArrayList)doc.get("capability");
			}
			JSONArray capability = null;
			if(capabilityList != null && capabilityList.indexOf("[") != -1) {
				capability = new JSONArray(capabilityList);
			} else {
				capability = new JSONArray();
			}
			String owner = doc.getString("owner");
			String manager = doc.getString("manager");
			//System.out.println(planType);
			pst.setString(1, id);
			pst.setString(2, planId);
			pst.setString(3, subplanId);
			pst.setString(4, name);
			pst.setString(5, desc);
			pst.setString(6, planType);
			pst.setString(7, category);
			pst.setBoolean(8, root);
			pst.setString(9, testingProcedures);
			pst.setString(10, guidance);
			pst.setString(11, startdate);
			pst.setString(12, enddate);
			pst.setString(13, requestedEnddate);
			pst.setString(14, estimatedEnddate);
			pst.setString(15, status);
			pst.setString(16, impl);
			pst.setString(17, completion);
			pst.setString(18, extension);
			pst.setString(19, capability.toString());
			pst.setString(20, owner);
			pst.setString(21, manager);
			pst.setString(22, "support@smarterd.com");
			pst.setString(23, "support@smarterd.com");
			//System.out.println(pst.toString());
			pst.executeUpdate();
		}	

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated SubPlans!!!");
	}

	private void migrateThreatClass() throws Exception {
		String colName = "enterprise_threat_class";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_threat_class values(?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?,?)";
		String relsql = "insert into enterprise_rel values(null,?,?,?,?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,?)";
		PreparedStatement pst1 = conn.prepareStatement(relsql);
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetClass");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Threat Class...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String recommendation = doc.get("recommandation").toString();
			String csf = doc.get("csf").toString();
			String name = doc.getString("name");
			String type = doc.getString("type");
			String behavior = doc.getString("behavior");
			List assetclassList = null;
			if(doc.get("assetclass") != null && doc.get("assetclass") instanceof List) {
				assetclassList = (ArrayList)doc.get("assetclass");
			}
			JSONArray assetclass = new JSONArray();
			if(assetclassList != null) {
				Iterator acIter = assetclassList.iterator();
				while(acIter.hasNext()) {
					String str = (String)acIter.next();
					assetclass.add(str);
				}
			}

			String ext = "{}";
			if(doc.get("extension") != null) {
				ext = doc.get("extension").toString();
			}
			String lifecycle = doc.getString("lifecycle");
			String owner = doc.getString("owner");
			String prevention = doc.getString("prevention");
			List threats = null;
			if(doc.get("threats") != null && doc.get("threats") instanceof List) {
				threats = (ArrayList)doc.get("threats");
			}
			/*
			JSONArray threatList = null;
			if(threats != null && threats.indexOf("[") != -1) {
				threatList = new JSONArray(threats);
			} else {
				threatList = new JSONArray();
			}
			*/

			pst.setString(1, id);
			pst.setString(2, name);
			pst.setString(3, type);
			pst.setString(4, behavior);
			pst.setString(5, assetclass.toString());
			pst.setString(6, ext);
			pst.setString(7, lifecycle);
			pst.setString(8, owner);
			pst.setString(9, prevention);
			pst.setString(10, "[]");
			pst.setString(11, recommendation);
			pst.setString(12, csf);
			pst.setString(13, "support@smarterd.com");
			pst.setString(14, "support@smarterd.com");
			pst.addBatch();

			Iterator threatIter = threats.iterator();
			while(threatIter.hasNext()) {
				String threatId = ((ObjectId)threatIter.next()).toString();
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT CLASS");
				ctx.put("LINKED", "N");
				pst1.setString(1, id);
				pst1.setString(2, threatId);
				pst1.setString(3, "THREAT");
				pst1.setString(4, ctx.toString());
				pst1.setString(5, companyCode);
				pst1.addBatch();

				ctx.clear();
				ctx.put("COMPONENT TYPE", "THREAT");
				ctx.put("LINKED", "N");
				pst1.setString(1, threatId);
				pst1.setString(2, id);
				pst1.setString(3, "THREAT CLASS");
				pst1.setString(4, ctx.toString());
				pst1.setString(5, companyCode);
				pst1.addBatch();
			}
		}

		pst.executeBatch();
		pst.close();
		pst1.executeBatch();
		pst1.close();
		conn.commit();

		System.out.println("Successfully migrated Threat Class!!!");
	}

	private void migrateThreat() throws Exception {
		String colName = "enterprise_threat";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_threat values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetThreat");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Threat...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String threatId = doc.getString("id");
			String name = doc.getString("name");
			String desc = doc.getString("description");
			String source = doc.getString("source");
			String info = doc.getString("information");
			String criticality = doc.getString("criticality");
			String countermeasures = doc.getString("countermeasures");
			String applicable = doc.getString("applicable");
			List assetclassList = null;
			if(doc.get("assetclass") != null && doc.get("assetclass") instanceof List) {
				assetclassList = (ArrayList)doc.get("assetclass");
			}
			JSONArray assetclass = new JSONArray();
			if(assetclassList != null) {
				Iterator acIter = assetclassList.iterator();
				while(acIter.hasNext()) {
					String str = (String)acIter.next();
					assetclass.add(str);
				}
			}
			List vulnrList = null;
			if(doc.get("vulnerability") != null && doc.get("vulnerability") instanceof List) {
				vulnrList = (ArrayList)doc.get("vulnerability");
			}
			JSONArray vulnerability = new JSONArray();
			if(vulnrList != null) {
				Iterator vulnrIter = vulnrList.iterator();
				while(vulnrIter.hasNext()) {
					String str = (String)vulnrIter.next();
					vulnerability.add(str);
				}
			}
			
			List tcList = null;
			if(doc.get("threatclasses") != null && doc.get("threatclasses") instanceof List) {
				tcList = (ArrayList)doc.get("threatclasses");
			}
			JSONArray threatclass = new JSONArray();
			if(tcList != null) {
				Iterator tcIter = tcList.iterator();
				while(tcIter.hasNext()) {
					String str = ((ObjectId)tcIter.next()).toString();
					threatclass.add(str);
				}
			}

			String vulnerabilities = "[]";
			if(doc.get("vulnerabilities") != null) {
				vulnerabilities = doc.get("vulnerabilities").toString();
			}

			String ext = "{}";
			if(doc.get("extension") != null) {
				ext = doc.get("extension").toString();
			}
			String lifecycle = doc.getString("lifecycle");
			String owner = doc.getString("owner");
			String region = doc.getString("region");

			pst.setString(1, id);
			pst.setString(2, threatId);
			pst.setString(3, name);
			pst.setString(4, desc);
			pst.setString(5, source);
			pst.setString(6, info);
			pst.setString(7, criticality);
			pst.setString(8, countermeasures);
			pst.setString(9, ext);
			pst.setString(10, lifecycle);
			pst.setString(11, applicable);
			pst.setString(12, owner);
			pst.setString(13, region);
			pst.setString(14, assetclass.toString());
			pst.setString(15, threatclass.toString());
			pst.setString(16, vulnerability.toString());
			pst.setString(17, "support@smarterd.com");
			pst.setString(18, "support@smarterd.com");
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Threat!!!");
	}

	private void migrateVulnerability() throws Exception {
		String colName = "enterprise_vulnerability";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_vulnerability values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("ThreatAsset");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Vulnerability...");
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String subscribedUsers = doc.get("subscribed_Users").toString();
			List groupList = null;
			if(doc.get("groups") != null && doc.get("groups") instanceof List) {
				groupList = (ArrayList)doc.get("groups");
			}
			JSONArray groups = null;
			if(groupList != null && groupList.indexOf("[") != -1) {
				groups = new JSONArray(groupList);
			} else {
				groups = new JSONArray();
			}
			String responses = doc.getString("responses");
			String priority = doc.getString("priority");
			String reportLabel = doc.getString("reportLabel");
			String nameId = doc.getString("nameId");
			String desc = doc.getString("description");
			String domain = doc.getString("domain");
			String subdomain = doc.getString("subDomain");
			String coaType = doc.getString("coaType");
			String coaCost = doc.getString("coaCost");
			String coaImpact = doc.getString("coaImpact");
			String coaEfficacy = doc.getString("coaEfficacy");
			String type = doc.getString("type");
			String source = doc.getString("source");
			String createdAt = doc.getString("createdAt");
			String count = doc.getString("count");
			String ext = "{}";
			if(doc.get("extension") != null) {
				ext = doc.get("extension").toString();
			}
			String owner = doc.getString("owner");
			if(owner != null && owner.startsWith("{")) {
				OrderedJSONObject ownerObj = new OrderedJSONObject(owner);
				if(ownerObj.containsKey("fullName")) {
					owner = ownerObj.getString("fullName");
				} else {
					owner = "";
				}
			}
			if(owner == null) {
				owner = "";
			}

			Date dt = new Date(createdAt);
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.add(Calendar.MONTH, 6);
			dt = cal.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			createdAt = sdf.format(dt);

			pst.setString(1, id);
			pst.setString(2, subscribedUsers);
			pst.setString(3, groups.toString());
			pst.setString(4, responses);
			pst.setString(5, priority);
			pst.setString(6, reportLabel);
			pst.setString(7, nameId);
			pst.setString(8, desc);
			pst.setString(9, domain);
			pst.setString(10, subdomain);
			pst.setString(11, coaType);
			pst.setString(12, coaCost);
			pst.setString(13, coaImpact);
			pst.setString(14, coaEfficacy);
			pst.setString(15, type);
			pst.setString(16, source);
			pst.setString(17, createdAt);
			pst.setString(18, createdAt);
			pst.setString(19, "support@smarterd.com");
			pst.setString(20, "support@smarterd.com");
			pst.setString(21, count);
			pst.setString(22, owner);
			pst.setString(23, ext);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Vulnerability!!!");
	}

	private void migrateVulnerabilityGroup() throws Exception {
		String colName = "enterprise_vulnerability_group";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_vulnerability_group values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String relsql = "insert into enterprise_rel values(null,?,?,?,?,'support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		PreparedStatement pst1 = conn.prepareStatement(relsql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetGroup");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Vulnerability Group...");
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			List assetList = null;
			if(doc.get("assets") != null) {
				assetList = (ArrayList)doc.get("assets");
			}
			
			String subscribedUsers = doc.get("subscribed_Users").toString();
			String nameId = doc.getString("nameId");
			String name = doc.getString("name");
			String desc = doc.getString("description");
			String domain = doc.getString("domain");
			String subdomain = doc.getString("subDomain");
			String type = doc.getString("type");
			String count = doc.getString("currentCount");
			String source = doc.getString("source");
			String createdAt = doc.getString("createdAt");
			String ext = "{}";
			if(doc.get("extension") != null) {
				ext = doc.get("extension").toString();
			}
			String owner = doc.getString("owner");

			Date dt = new Date(createdAt);
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.add(Calendar.MONTH, 6);
			dt = cal.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			createdAt = sdf.format(dt);

			pst.setString(1, id);
			pst.setString(2, subscribedUsers);
			pst.setString(3, "[]");
			pst.setString(4, nameId);
			pst.setString(5, name);
			pst.setString(6, desc);
			pst.setString(7, domain);
			pst.setString(8, subdomain);
			pst.setString(9, type);
			pst.setString(10, source);
			pst.setString(11, createdAt);
			pst.setString(12, createdAt);
			pst.setString(13, "support@smarterd.com");
			pst.setString(14, "support@smarterd.com");
			pst.setString(15, count);
			pst.setString(16, owner);
			pst.setString(17, ext);
			pst.addBatch();

			Iterator vulnrIter = assetList.iterator();
			while(vulnrIter.hasNext()) {
				String vulnrId = ((ObjectId)vulnrIter.next()).toString();
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT GROUP");
				ctx.put("LINKED", "N");
				pst1.setString(1, id);
				pst1.setString(2, vulnrId);
				pst1.setString(3, "THREAT ASSET");
				pst1.setString(4, ctx.toString());
				pst1.setString(5, companyCode);
				pst1.addBatch();

				ctx.clear();
				ctx.put("COMPONENT TYPE", "THREAT ASSET");
				ctx.put("LINKED", "N");
				pst1.setString(1, vulnrId);
				pst1.setString(2, id);
				pst1.setString(3, "THREAT GROUP");
				pst1.setString(4, ctx.toString());
				pst1.setString(5, companyCode);
				pst1.addBatch();
			}
		}

		pst.executeBatch();
		pst.close();
		pst1.executeBatch();
		pst1.close();
		conn.commit();

		System.out.println("Successfully migrated Vulnerability Group!!!");
	}

	private void migrateIncidence() throws Exception {
		String colName = "enterprise_incident";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_incident values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("Incident");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Incidence...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			List ecoList = null;
			if(doc.get("ecosystem") != null && doc.get("ecosystem") instanceof List) {
				ecoList = (ArrayList)doc.get("ecosystem");
			}
			JSONArray ecosystem = null;
			if(ecoList != null && ecoList.indexOf("[") != -1) {
				ecosystem = new JSONArray(ecoList);
			} else {
				ecosystem = new JSONArray();
			}
			String name = doc.getString("name");
			String desc = doc.getString("description");
			String ticketRef = doc.getString("ticketRef");
			String impactDetails = doc.getString("impactDetails");
			String resolutionDetails = doc.getString("resolutionDetails");
			String priority = doc.getString("priority");
			String lifecycle = doc.getString("lifecycle");
			String incidentTime = doc.getString("incidentTime");
			String resolutionTime = doc.getString("resolutionTime");
			String businessStakeholder = doc.getString("businessstakeholder");
			String itStakeholder = doc.getString("itstakeholder");
			String source = doc.getString("source");
			String ext = "{}";

			pst.setString(1, id);
			pst.setString(2, ecosystem.toString());
			pst.setString(3, name);
			pst.setString(4, ticketRef);
			pst.setString(5, desc);
			pst.setString(6, impactDetails);
			pst.setString(7, resolutionDetails);
			pst.setString(8, source);
			pst.setString(9, priority);
			pst.setString(10, lifecycle);
			pst.setString(11, incidentTime);
			pst.setString(12, resolutionTime);
			pst.setString(13, businessStakeholder);
			pst.setString(14, itStakeholder);
			pst.setString(15, ext);
			pst.setString(16, "support@smarterd.com");
			pst.setString(17, "support@smarterd.com");
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Incidence!!!");
	}

	private void migrateNotification() throws Exception {
		String colName = "enterprise_notification";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_notification values(?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("Notification");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Notification...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String packageRef = doc.get("package_ref").toString();
			String componentId = doc.get("component_id").toString();
			String alertName = doc.getString("alert_name");
			String message = doc.getString("message");
			String status = doc.getString("status");
			String ext = "{}";
			if(doc.get("extension") != null) {
				ext = doc.get("extension").toString();
			}
			List emailList = null;
			if(doc.get("email") != null && doc.get("email") instanceof List) {
				emailList = (ArrayList)doc.get("email");
			}
			JSONArray email = null;
			if(emailList != null && emailList.indexOf("[") != -1) {
				email = new JSONArray(emailList);
			} else {
				email = new JSONArray();
			}

			pst.setString(1, id);
			pst.setString(2, packageRef);
			pst.setString(3, componentId);
			pst.setString(4, alertName);
			pst.setString(5, message);
			pst.setString(6, status);
			pst.setString(7, ext);
			pst.setString(8, email.toString());
			pst.setString(9, companyCode);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Notification!!!");
	}

	private void migrateEnterprise() throws Exception {
		String colName = "SE.enterprise";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into SE.enterprise values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,?)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB("SE").getCollection("Enterprise");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Enterprise...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String code = doc.getString("company_code");
			String parentCode = doc.getString("parent_company_code");
			String fiscalYear = doc.getString("fiscal_year");
			String fiscalYearStart = doc.getString("fiscal_year_start");
			String balance = doc.getString("balance");
			String name = doc.getString("company_name");
			String desc = doc.getString("company_desc");
			String logo = doc.getString("company_logo");
			String industry = doc.getString("industry");
			String website = doc.getString("website");
			String unique = null;
			if(doc.getString("uniqueness") != null) {
				unique = doc.get("uniqueness").toString();
			}
			OrderedJSONObject uniqueness = null;
			if(unique != null) {
				uniqueness = new OrderedJSONObject(unique);
			} else {
				uniqueness = new OrderedJSONObject();
			}
			String securityFrmk = doc.getString("security_control_framework");
			String itFrmk = doc.getString("it_control_framework");
			String integration = null;
			if(doc.getString("integration_setting") != null) {
				integration = doc.get("integration_setting").toString();
			}
			OrderedJSONObject integrationSetting = null;
			if(integration != null) {
				integrationSetting = new OrderedJSONObject(integration);
			} else {
				integrationSetting = new OrderedJSONObject();
			}
			String slack = null;
			if(doc.getString("slack") != null) {
				slack = doc.get("slack").toString();
			}
			OrderedJSONObject slackSetting = null;
			if(slack != null) {
				slackSetting = new OrderedJSONObject(slack);
			} else {
				slackSetting = new OrderedJSONObject();
			}
			String enterprise = null;
			if(doc.getString("enterprise_setting") != null) {
				enterprise = doc.get("enterprise_setting").toString();
			}
			OrderedJSONObject enterpriseSetting = null;
			if(enterprise != null) {
				enterpriseSetting = new OrderedJSONObject(enterprise);
			} else {
				enterpriseSetting = new OrderedJSONObject();
			}
			String securityTemplate = doc.getString("security_status_report_template");
			String itTemplate = doc.getString("it_status_report_template");
			boolean enableAlert = doc.getBoolean("enableAlert");
			String alertScheduleTime = doc.getString("alert_schedule_time");
			String alertSchedulePeriod = doc.getString("alert_schedule_period");
			String snapshotSchedulePeriod = doc.getString("snapshot_schedule_period");
			String status = doc.getString("status");
			boolean registered = doc.getBoolean("registered");

			pst.setString(1, id);
			pst.setString(2, code);
			pst.setString(3, name);
			pst.setString(4, desc);
			pst.setString(5, "");
			pst.setString(6, logo);
			pst.setBoolean(7, registered);
			pst.setString(8, industry);
			pst.setString(9, "");
			pst.setString(10, fiscalYear);
			pst.setString(11, fiscalYearStart);
			pst.setString(12, "");
			pst.setString(13, "");
			pst.setString(14, "");
			pst.setString(15, enterpriseSetting.toString());
			pst.setString(16, integrationSetting.toString());
			pst.setString(17, uniqueness.toString());
			pst.setString(18, slackSetting.toString());
			pst.setString(19, securityTemplate);
			pst.setString(20, itTemplate);
			pst.setBoolean(21, enableAlert);
			pst.setString(22, alertScheduleTime);
			pst.setString(23, alertSchedulePeriod);
			pst.setString(24, snapshotSchedulePeriod);
			pst.setString(25, website);
			pst.setString(26, securityFrmk);
			pst.setString(27, itFrmk);
			pst.setString(28, "");
			pst.setString(29, "");
			pst.setString(30, "N");
			pst.setString(31, "");
			pst.setString(32, "support@smarterd.com");

			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Enterprise!!!");
	}

	private void migrateAlert() throws Exception {
		String colName = "enterprise_alert";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_alert values(?,?,?,?,?,?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("Alert");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Alert...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			List receiverList = null;
			if(doc.get("receiver") != null) {
				receiverList = (List)doc.get("receiver");
			}
			JSONArray receiver = new JSONArray();
			if(receiverList != null) {
				Iterator receiverIter = receiverList.iterator();
				while(receiverIter.hasNext()) {
					String str = (String)receiverIter.next();
					receiver.add(str);
				}
			}
			String alertName = doc.getString("alertName");
			String componentType = "";
			if(doc.getString("componentType") != null) {
				componentType = doc.getString("componentType");
			}
			JSONArray grp = new JSONArray();
			String valueTable = doc.getString("valueTable");
			String operator = doc.getString("operator");
			String columnName = "{}";
			if(doc.getString("columnName") != null) {
				columnName = doc.get("columnName").toString();
			}
			OrderedJSONObject columnObj = new OrderedJSONObject(columnName);
			OrderedJSONObject ruleObj = new OrderedJSONObject();
			ruleObj.put("valueTable", valueTable);
			ruleObj.put("operator", operator);
			ruleObj.put("columnName", columnObj);
			grp.add(ruleObj);
			
			String message = doc.getString("message");

			pst.setString(1, id);
			pst.setString(2, receiver.toString());
			pst.setString(3, alertName);
			pst.setString(4, componentType);
			pst.setString(5, grp.toString());
			pst.setString(6, message);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Alert!!!");
	}

	private void migratePackage() throws Exception {
		String colName = "enterprise_package";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_package values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("Package");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " Package...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String valueTags = "[]";
			if(doc.get("value_tags") != null) {
				valueTags = doc.get("value_tags").toString();
			}
			String keywordTags = "[]";
			if(doc.get("keyword_tags") != null) {
				keywordTags = doc.get("keyword_tags").toString();
			}
			String comments = "[]";
			if(doc.get("comments") != null) {
				comments = doc.get("comments").toString();
			}
			boolean pub = doc.getBoolean("public");
			String ownerId = doc.getString("owner_id");
			String name = doc.getString("name");
			String desc = doc.getString("description");
			String category = doc.getString("category");
			String packageType = doc.getString("package_type");
			String price = doc.getString("price");
			String access = doc.getString("access_right");
			String domain = doc.getString("domain");
			String area = doc.getString("area");
			String sourceName = doc.getString("source_name");
			String componentType = doc.getString("type");
			String size = doc.getString("size");
			String sector = doc.getString("sector");
			String revenue = doc.getString("revenue");
			String avgReview = doc.getString("avg_review");
			String noReviews = doc.getString("num_reviews");
			String uri = doc.getString("package_uri");

			pst.setString(1, id);
			pst.setString(2, valueTags);
			pst.setString(3, keywordTags);
			pst.setString(4, comments);
			pst.setBoolean(5, pub);
			pst.setString(6, ownerId);
			pst.setString(7, name);
			pst.setString(8, desc);
			pst.setString(9, category);
			pst.setString(10, packageType);
			pst.setString(11, price);
			pst.setString(12, access);
			pst.setString(13, domain);
			pst.setString(14, area);
			pst.setString(15, sourceName);
			pst.setString(16, componentType);
			pst.setString(17, size);
			pst.setString(18, sector);
			pst.setString(19, revenue);
			pst.setString(20, avgReview);
			pst.setString(21, noReviews);
			pst.setString(22, uri);
			pst.setString(23, companyCode);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Package!!!");
	}

	private void migratePackageRel() throws Exception {
		String colName = "enterprise_package_rel";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_package_rel values(?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("PackageRel");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " PackageRel...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String shared = doc.get("shared").toString();
			String pkg = doc.getString("package");
			String user = doc.getString("user");
			String packageName = doc.getString("package_name");
			boolean requested = doc.getBoolean("requested");
			boolean admin = doc.getBoolean("admin");
			String status = doc.getString("status");
			String access = doc.getString("read_access");
			
			pst.setString(1, id);
			pst.setString(2, shared);
			pst.setString(3, pkg);
			pst.setString(4, user);
			pst.setString(5, packageName);
			pst.setBoolean(6, requested);
			pst.setBoolean(7, admin);
			pst.setString(8, status);
			pst.setString(9, access);
			pst.setString(10, companyCode);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated PackageRel!!!");
	}

	private void migrateUser() throws Exception {
		String colName = "enterprise_user";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_user values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,'')";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("User");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " User...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			List packages = null;
			if(doc.get("packages") != null) {
				packages = (List)doc.get("packages");
			}
			List packageRels = null;
			if(doc.get("packageRels") != null) {
				packageRels = (List)doc.get("packageRels");
			}
			List accessRights = null;
			if(doc.get("accessRights") != null) {
				accessRights = (List)doc.get("accessRights");
			}
			int loginAttempts = 0;
			if(doc.containsKey("login_attemps")) {
				loginAttempts = doc.getInt("login_attempts");
			}
			String userName = doc.getString("username");
			String fullname = doc.getString("fullName");
			String email = doc.getString("email");
			String accessRight = doc.getString("access_right");
			boolean registered = doc.getBoolean("registered");
			String accessLevel = doc.getString("accessLevel");
			boolean admin = doc.getBoolean("admin");
			boolean thirdParty = doc.getBoolean("thirdParty");
			String tPartyVendor = doc.getString("tPartyVendor");
			String jobTitle = doc.getString("job_title");
			String department = doc.getString("department");
			String officeLocation = doc.getString("office_location");
			String officePhone = doc.getString("office_phone");
			String userStatus = doc.getString("user_status");
			String bookmarkedPkg = doc.get("bookmarked_packages").toString();
			String salt = doc.getString("salt");
			String hash = doc.getString("hash");
			String role = doc.getString("role");

			JSONArray packagesList = new JSONArray();
			if(packages != null && packages.size() > 0) {
				Iterator pIter = packages.iterator();
				while(pIter.hasNext()) {
					String str = ((ObjectId)pIter.next()).toString();
					packagesList.add(str);
				}
			}
			JSONArray packageRelsList = new JSONArray();
			if(packageRels != null && packageRels.size() > 0) {
				Iterator pIter = packageRels.iterator();
				while(pIter.hasNext()) {
					String str = ((ObjectId)pIter.next()).toString();
					packageRelsList.add(str);
				}
			}
			JSONArray accessRightsList = new JSONArray();
			if(accessRights != null && accessRights.size() > 0) {
				Iterator pIter = accessRights.iterator();
				while(pIter.hasNext()) {
					String str = (String)pIter.next();
					accessRightsList.add(str);
				}
			}
			
			pst.setString(1, id);
			pst.setString(2, packagesList.toString());
			pst.setString(3, packageRelsList.toString());
			pst.setString(4, accessRightsList.toString());
			pst.setInt(5, loginAttempts);
			pst.setString(6, userName);
			pst.setString(7, fullname);
			pst.setString(8, email);
			pst.setString(9, accessRight);
			pst.setBoolean(10, registered);
			pst.setString(11, accessLevel);
			pst.setBoolean(12, admin);
			pst.setBoolean(13, thirdParty);
			pst.setString(14, tPartyVendor);
			pst.setString(15, jobTitle);
			pst.setString(16, department);
			pst.setString(17, officeLocation);
			pst.setString(18, officePhone);
			pst.setString(19, userStatus);
			pst.setString(20, bookmarkedPkg);
			pst.setString(21, salt);
			pst.setString(22, hash);
			pst.setString(23, role);
			pst.setString(24, companyCode);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated User!!!");
	}

	private void migrateAccount() throws Exception {
		String colName = "enterprise_account";		
		initialize(colName);
		
		int rowCount=0;
		String sql = "insert into enterprise_account values(?,?,?,?,?,current_timestamp,current_timestamp)";
		PreparedStatement pst = conn.prepareStatement(sql);
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("accounts");
		BasicDBObject criteria = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		List docList = coll.find(criteria, fields).toArray();
		System.out.println("Migrating "+docList.size()+ " accounts...");
		Date dt = new Date();
		Iterator iter = docList.iterator();
		while(iter.hasNext()) {
			BasicDBObject doc = (BasicDBObject)iter.next();
			String id = doc.get("_id").toString();
			String number = doc.getString("number");
			String name = doc.getString("name");
			String group = doc.getString("group");
			String lifecycle = doc.getString("lifecycle");
			
			pst.setString(1, id);
			pst.setString(2, number);
			pst.setString(3, name);
			pst.setString(4, group);
			pst.setString(5, lifecycle);
			pst.addBatch();
		}

		pst.executeBatch();
		pst.close();
		conn.commit();

		System.out.println("Successfully migrated Accounts!!!");
	}


	private void initialize(String colName) throws Exception {
		String sql = "delete from "+colName;
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.executeUpdate();
		conn.commit();
	}

	private void mongoConnect(String dbname) throws Exception {
		String username = "smartEuser";
		String pwd = "smarterD2018!";
		//String hostname = "35.227.181.195";
		//String hostname = "35.227.158.31";
		String hostname = "localhost";
		String encoded_pwd = URLEncoder.encode(pwd, "UTF-8");
        MongoClientURI connectionString = new MongoClientURI("mongodb://"+username+":"+encoded_pwd+"@"+hostname+":27017/"+dbname+"?maxPoolSize=10");
        mongoClient = new MongoClient(connectionString);
        db = mongoClient.getDatabase(dbname);
        System.out.println("Successfully connected to Mongo DB:"+dbname);
    }
    
    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
		//String hostname = "35.227.181.195";
		String hostname = "35.197.106.183";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
	}
	
	private String camelCase(String str) throws Exception {
		return(str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase());
	}

	private String formatDate(String d) throws Exception {
        String formattedDate = "";
        if(d != null && d.length() > 0) {
            if (d.length() == 7 && d.indexOf("/") != -1) {    //1/01/07
                SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yy");
                Date date = formatter.parse(d);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                formattedDate = Integer.toString(month)+"/"+Integer.toString(day)+"/"+Integer.toString(year);
            } else if (d.length() == 8 && d.indexOf("/") != -1) {
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
                Date date = formatter.parse(d);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                formattedDate = Integer.toString(month)+"/"+Integer.toString(day)+"/"+Integer.toString(year);
			} else if(d.indexOf("T") != -1) {
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
				Date date = sdf.parse(d);
				formattedDate = new SimpleDateFormat("MM/dd/yyyy").format(date);
			} else {
                formattedDate = d;
            }
        }

        return formattedDate;
	}

	public static void main(String[] args) throws Exception {
		String d = "2018-02-01T05:00:00.000Z";
		String d1 = "2018-02-01";
		String d2 = "02/01/2018";

		String companyCode = args[0];
		MigrateMongoDB loader = new MigrateMongoDB(companyCode);
		//loader.migrateThreatPlan();
		//loader.migrateSubPlan();
		//loader.migrateThreatClass();
		//loader.migrateThreat();
		loader.migrateVulnerability();
		//loader.migrateVulnerabilityGroup();
		//loader.migrateIncidence();
		//loader.migrateEnterprise();
		//loader.migrateUser();
		//loader.migrateAlert();
		//loader.migrateNotification();
		//loader.migratePackage();
		//loader.migratePackageRel();
		//loader.migrateAccount();

		//String res = loader.formatDate(d);
		//System.out.println(res);
	}
}