import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.Instant;
import com.tenable.io.api.TenableIoClient;
import com.tenable.io.api.exports.ExportsApi;
import com.tenable.io.api.exports.models.VulnsExportRequest;
import com.tenable.io.api.exports.models.VulnsExportFilters;
import com.tenable.io.api.models.SeverityLevel;
import com.tenable.io.api.models.VulnerabilityState;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;

public class MauricesPlanLoader {
	private CSVReader reader = null;
	private String companyCode = null;
	private Connection conn = null;
	private String env = "localhost";
	private String updatedBy = "support@smarterd.com";
	
	public MauricesPlanLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		this.env = env;

		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);
        
        // Connect Mysql
		mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String[] row;
		int rowCount=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String lifecycle=null, priority=null, name=null, desc=null, asset=null;
				String capability=null, assignee=null, status=null, startdate=null, enddate=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						name = row[i].trim();
					} else if(i==1) {
						desc = row[i];
					} else if(i==2) {
						capability = row[i].trim();
					} else if(i==3) {
						assignee = row[i].trim();
					} else if(i==4) {
						priority = row[i].trim();
					} else if(i==5) {
						lifecycle = row[i].trim();
					} else if(i==6) {
						status = row[i].trim(); 
					} else if(i==7) {
						startdate = row[i].trim();
					} else if(i==8) {
						enddate = row[i].trim();
					} else if(i==9) {
						asset = row[i].trim();
					}
				}

				// check if Threat exists
				BasicDBObject criteria = new BasicDBObject();
				criteria.put("name", name);
				Document doc = getDocument(criteria, "ThreatPlan");
				ObjectId id = null;

				if(doc == null) {
					System.out.println("Plan not found. Inserting...");
					BasicDBObject plan = new BasicDBObject();
					id = ObjectId.get();
					plan.put("_id", id);
					plan.put("name", name);
					plan.put("planType", "IT");
					plan.put("description", desc);
					plan.put("capability", "");
					plan.put("domain", "");
					plan.put("startDate", startdate);
					plan.put("endDate", enddate);
					plan.put("requestedEndDate", "");
					plan.put("owner", assignee);
					plan.put("manager", "");
					plan.put("status", status);
					plan.put("implementation", lifecycle);

					BasicBSONList capabilityList = new BasicBSONList();
					if(capability.length() > 0) {
						capabilityList.add(capability);
					}
					plan.put("capability", capabilityList);

					BasicDBObject compensatingControl = new BasicDBObject();
					compensatingControl.put("title", "");
					compensatingControl.put("testing", "");
					compensatingControl.put("guidance", "");
					compensatingControl.put("objective", "");
					compensatingControl.put("status", status);
					compensatingControl.put("implementationStage", lifecycle);
					compensatingControl.put("analyst", "");
					compensatingControl.put("stakeholder", "");
					plan.put("compensatingControls", compensatingControl);

					BasicDBObject extension = new BasicDBObject();
					extension.put("plan_count", new Integer(0));
					extension.put("activity_count", new Integer(0));
					extension.put("priority", priority);
					extension.put("justification", "");
					extension.put("reqCapital", new Double(0.0));
					extension.put("reqExpense", new Double(0.0));
					extension.put("currentMaturity", new Integer(0));
					extension.put("targetMaturity", new Integer(0));
					extension.put("targetDate", "");
					extension.put("assignee", assignee);
					plan.put("extension", extension);

					DBCollection coll = mongoClient.getDB(companyCode).getCollection("ThreatPlan");
					coll.insert(plan);
					System.out.println("Successfully inserted Plan!!!");
					
					// Process Asset Relationship
					if(asset.length() > 0) {
						System.out.println("Processing Asset Relationship...["+asset+"]");
						loadAssetRel(id.toString(), asset);
					}
				
					// Process Capability Relationship
					if(capability.length() > 0) {
						System.out.println("Processing Capability Relationship...["+capability+"]");
						loadCapabilityRel(id.toString(), capability);
					}
				} else {
					id = doc.getObjectId("_id");
					System.out.println("Plan found. Skipping..."+id.toString());
				}

				lifecycle = null;
				priority = null;
				name = null;
				desc = null;
				startdate = null;
				enddate = null;
				capability = null;
				asset=null;
				status=null;
				assignee=null;
			}
		}
		System.out.println("Total Rows Inserted["+rowCount+"]!!!");
		conn.commit();
		conn.close();
	}

	private void loadAssetRel(String id, String asset) throws Exception {
		// Get id from asset
		String sql = "select id from enterprise_asset where asset_name = ?";
		String enterpriseRelInsertSQL = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_timestamp,created_by,updated_timestamp,updated_by,company_code) values(null, ?, ?, ?, ?, now(),?,now(),?,?)";
		String assetId = null;
		PreparedStatement pst1 = conn.prepareStatement(sql);
		pst1.setString(1, asset);
		ResultSet rs1 = pst1.executeQuery();
		while(rs1.next()) {
			assetId = rs1.getString("id");
		}
		rs1.close();
		pst1.close();

		if(assetId != null) {
			System.out.println("Asset Found..."+assetId);
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPONENT TYPE", "THREATPLAN");
			pst1 = conn.prepareStatement(enterpriseRelInsertSQL);
			pst1.setString(1, id);
			pst1.setString(2, assetId);
			pst1.setString(3, "ASSET");
			pst1.setString(4, ctx.toString());
			pst1.setString(5, updatedBy);
			pst1.setString(6, updatedBy);
			pst1.setString(7, companyCode);
			pst1.addBatch();

			ctx.put("COMPONENT TYPE", "ASSET");
			pst1.setString(1, assetId);
			pst1.setString(2, id);
			pst1.setString(3, "THREATPLAN");
			pst1.setString(4, ctx.toString());
			pst1.setString(5, updatedBy);
			pst1.setString(6, updatedBy);
			pst1.setString(7, companyCode);
			pst1.addBatch();

			pst1.executeBatch();
			pst1.close();
			System.out.println("Successfully inserted asset relationship!!!");
		}
	}

	private void loadCapabilityRel(String id, String capability) throws Exception {
		// Get id from asset
		String sql = "select id, business_capability_factor from enterprise_business_capability where child_capability_name = ?";
		String enterpriseRelInsertSQL = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_timestamp,created_by,updated_timestamp,updated_by,company_code) values(null, ?, ?, ?, ?, now(),?,now(),?,?)";
		String updateFactorSQL = "update enterprise_business_capability set business_capability_factor = ? where id = ?";
		String capabilityId = null, factor = null;
		PreparedStatement pst1 = conn.prepareStatement(sql);
		pst1.setString(1, capability);
		ResultSet rs1 = pst1.executeQuery();
		while(rs1.next()) {
			capabilityId = rs1.getString("id");
			factor = rs1.getString("business_capability_factor");
		}
		rs1.close();
		pst1.close();

		if(capabilityId != null) {
			System.out.println("Capability Found..."+capabilityId);
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPONENT TYPE", "THREATPLAN");
			pst1 = conn.prepareStatement(enterpriseRelInsertSQL);
			pst1.setString(1, id);
			pst1.setString(2, capabilityId);
			pst1.setString(3, "CAPABILITY");
			pst1.setString(4, ctx.toString());
			pst1.setString(5, updatedBy);
			pst1.setString(6, updatedBy);
			pst1.setString(7, companyCode);
			pst1.addBatch();
			
			ctx.put("COMPONENT TYPE", "CAPABILITY");
			pst1.setString(1, capabilityId);
			pst1.setString(2, id);
			pst1.setString(3, "THREATPLAN");
			pst1.setString(4, ctx.toString());
			pst1.setString(5, updatedBy);
			pst1.setString(6, updatedBy);
			pst1.setString(7, companyCode);
			pst1.addBatch();

			pst1.executeBatch();
			pst1.close();
			System.out.println("Successfully inserted capability relationship!!!");

			// Update Factor
			OrderedJSONObject factorObj = new OrderedJSONObject(factor);
			factorObj.put("active", new Boolean(true));
			factorObj.put("backLog", new Boolean(false));
			pst1 = conn.prepareStatement(updateFactorSQL);
			pst1.setString(1, factorObj.toString());
			pst1.setString(2, capabilityId);
			pst1.executeUpdate();
			System.out.println("Successfully updated capability factor!!!");	
		}
	}
    
    private void mysqlConnect(String dbname) throws Exception {
		String userName = "root", password = "smarterD2018!", hostname="localhost";
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		} else {
			hostname = "localhost";
		}
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		MauricesPlanLoader loader = new MauricesPlanLoader(filename, companyCode, env);
		loader.load();
	}
}