import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.util.Date;

public class ThreatLoader {
	private CSVReader reader = null;
	private String companyCode = null;
	private Connection conn = null;
	private String adapterType = "tenable";
	private Map groupList = new HashMap();
	private Map groupConfig = new HashMap();
    private Map groupAssetList = new HashMap();
	private Map groupCapabilityList = new HashMap();
	private String type = null;
	private List<DBObject> threatClassList = new ArrayList<DBObject>();
	private int currentCount = 0;
	private String createdAt = "";
	private String updatedAt = "";
	private String defaultGroupName = null;
	private List assetClassList = null;
	private List threatClassIdList = null;
	private List capabilityList = null;
	
	public ThreatLoader(String filename, String companyCode) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);
        
        // Connect Mongo
        mongoConnect(companyCode);
        
        // Connect Mysql
		mysqlConnect(companyCode);

		getCurrentCount();

		// Load Threat Class
		loadThreatClass();
	}
	
	public void processThreat() throws Exception {
		String[] row;
		int rowCount=0;
		
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("ThreatAsset");

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String lifecycle="Active", priority=null, name=null, desc=null, action=null, asset=null;
				String assetMfg=null, ecosystem=null, targetAssetType=null, targetAssetMfg=null, cve=null, targetAsset=null, os=null;
				for(int i=0; i<row.length; i++) {
					if(rowCount == 2 && i==0) {
						createdAt = row[i];
						updatedAt = createdAt;
					} else if(i==1) {			
						lifecycle = row[i];
					} else if(i==2) {
						cve = row[i];
					} else if(i==3) {
						priority = row[i];
					} else if(i==4) {
						name = row[i].trim();
					} else if(i==5) {
						desc = row[i];
					} else if(i==6) {
						action = row[i];
					} else if(i==7) {
						type = row[i].toUpperCase(); 
					} else if(i==8) {
						asset = row[i];
					} else if(i==9) {
						assetMfg = row[i];
					} else if(i==10) {
						ecosystem = row[i]; 
					} else if(i==11) {
						targetAsset = row[i];
					} else if(i==12) {
						os = row[i];
					} else if(i==13) {
						targetAssetType = row[i];
					} else if(i==14) {
						targetAssetMfg = row[i];
					} else if(i==15) {
						adapterType = row[i];
					} 
				}

				System.out.println("Asset Mfg:"+assetMfg+" Target Mfg:"+targetAssetMfg+":"+createdAt);

				// Initialize Timestamp
				if(rowCount == 2) {
					if(createdAt == null || createdAt.length() == 0) {
						createdAt = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date());
						updatedAt = createdAt;
					} else {
						//createdAt = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date(createdAt));
						createdAt = formatDate(createdAt);
						updatedAt = createdAt;
					}

					// Initialize Threat Config
					String t = createdAt.replaceAll("/", "-");
					initializeThreatGroupConfig(t);
					System.out.println("Group Config:"+groupConfig.toString());
					
					// Initialize Threat Group
					initializeThreatGroup();
					System.out.println("Group List:"+groupList.toString());

					//setLastGroupInactive();
				}

				// Convert lifecycle to camelcase
				lifecycle = camelCase(lifecycle);
				
				// Get Priority Value
				//priority = getPriorityValue(priority);

				// check if Vulnerability exists
				BasicDBObject criteria = new BasicDBObject();
				criteria.put("nameId", name);
				criteria.put("source", adapterType);
				//int count = getCount(criteria, "ThreatAsset");
				Document doc = getDocument(criteria,companyCode, "ThreatAsset");
				ObjectId id = null;
				int count = 0;
				if(doc == null) {
					System.out.println("Threat not found. Inserting...");
					BasicDBObject threatAsset = new BasicDBObject();
					id = ObjectId.get();
					threatAsset.put("_id", id);
					threatAsset.put("subscribed_Users",  new BasicBSONList());
					threatAsset.put("groups",  new BasicBSONList());
					threatAsset.put("responses", "");
					threatAsset.put("companyCode", companyCode); 
					threatAsset.put("priority", priority);
					threatAsset.put("reportLabel", "");
					threatAsset.put("nameId", name);
					threatAsset.put("description", desc);
					threatAsset.put("domain", "");
					threatAsset.put("subDomain", "");
					threatAsset.put("coaType", "");
					threatAsset.put("coaCost", "");
					threatAsset.put("coaImpact", "");
					threatAsset.put("coaEfficacy", "");
					threatAsset.put("type", "Vulnerability");
					threatAsset.put("source", adapterType);
					threatAsset.put("createdAt", updatedAt);
					threatAsset.put("updatedAt", updatedAt);
					count++;
					threatAsset.put("count", new Integer(count));

					BasicDBObject owner = new BasicDBObject();
					owner.put("packages",  new BasicBSONList());
					owner.put("packageRels",  new BasicBSONList());
					owner.put("accessRights",  new BasicBSONList());
					threatAsset.put("owner", owner);

					BasicDBObject extension = new BasicDBObject();
					extension.put("category", type);
					extension.put("riskComp", "");
					extension.put("riskScaled", "");
					extension.put("enterprise_action", action);
					extension.put("lifecycle", "Active");
					threatAsset.put("extension", extension);

					coll.insert(threatAsset);
				} else {
					id = doc.getObjectId("_id");
					count = ((Integer)doc.get("count")).intValue();
					count++;
					
					// Update Threat Asset
					BasicDBObject values = new BasicDBObject();
					values.put("count", new Integer(count));
					values.put("updatedAt", updatedAt);
					DBObject update = new BasicDBObject();
					update.put("$set", values);
					BasicDBObject parameter = new BasicDBObject();
					parameter.put("_id", id);
					coll.update(parameter, update);
					System.out.println("Threat found. Updating..."+id.toString());
				}
				
				// Add Vulnerability to group
				if(id != null) {
					// Process Asset Relationship
					if(asset != null && asset.trim().length() > 0) {
						System.out.println("Processing Asset Relationship...["+asset+"]");
						JSONArray ecoArr = new JSONArray();
						ecoArr.add(ecosystem);
						processAsset(id.toString(), name, lifecycle, priority, asset, assetMfg, ecoArr, targetAsset, targetAssetType, targetAssetMfg, os);
					}

					// Get Capability List from Asset
					// getCapabilityListFromAsset(asset);
					
				
					// Process Capability Relationship
					/*
					if(capabilityList.size() > 0) {
						System.out.println("Processing Capability Relationship...["+capabilityList+"]");
						processCapability(id.toString(), name, lifecycle, priority, capabilityList);
					}
					*/

					addVulnerabilityToGroup(id, name.toLowerCase(), asset, ecosystem, assetMfg, targetAsset, os, capabilityList);
				}

				lifecycle = null;
				priority = null;
				name = null;
				desc = null;
				action = null;
				asset = null;
				assetMfg = null;
				targetAsset = null;
				targetAssetType = null;
				targetAssetMfg = null;
				os=null;
				assetClassList = null;
				threatClassIdList = null;
				capabilityList = null;
			}
		}
	
		// create Group
		createGroups();
		
		// Update Current Count
		updateCurrentCount();

		rowCount = rowCount - 1;

	}
	
	private int getCount(DBObject query, String collName) throws Exception {
		DBCollection coll = mongoClient.getDB(companyCode).getCollection(collName);
		int count = coll.find(query).count();
		return count;
	}

	private Document getDocument(BasicDBObject criteria, String db, String collName) throws Exception {
		MongoCollection coll = mongoClient.getDatabase(db).getCollection(collName);
        Document doc = (Document)coll.find(criteria).first();
		return doc;
	}

	private void removeDocument(BasicDBObject criteria, String collName) throws Exception {
		MongoCollection coll = mongoClient.getDatabase(companyCode).getCollection(collName);
       	coll.deleteOne(criteria);
	}
	
	private void processAsset(String threatId, String threatName, String lifecycle, String priority, String asset, String assetMfg, JSONArray ecosystem, 
							  String targetAsset, String targetAssetType, String targetAssetMfg, String os) throws Exception {
		System.out.println("processAsset=>"+asset);
		PreparedStatement pst = null;
		ResultSet rs = null;

		// Get Target Asset Id
		String targetAssetId = null, ext=null;
		if(targetAsset != null && targetAsset.length()>0) {
			String sql = "select id, extension from enterprise_asset where asset_name = ? and asset_category = 'IT ASSET'";
			pst = conn.prepareStatement(sql);
			pst.setString(1, targetAsset.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				targetAssetId = rs.getString("id");
				ext = rs.getString("extension");
			}
			rs.close();
			pst.close();

			if(targetAssetId == null || targetAssetId.length() == 0) {
				System.out.println("Asset not found. Adding["+targetAsset+"]");
				targetAssetId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("OS", os);
				extObj.put("Type", targetAssetType);
				extObj.put("OEM Vendor", targetAssetMfg);
				String insertSQL = "insert into enterprise_asset(id,asset_name,extension,company_code,asset_category,created_by,updated_by,created_timestamp) values(?,?,?,?,'IT ASSET','support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, targetAssetId);
				pst.setString(2, targetAsset);
				pst.setString(3, extObj.toString());
				pst.setString(4, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			} else {
				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				extObj.put("OS", os);
				extObj.put("Type", targetAssetType);
				extObj.put("OEM Vendor", targetAssetMfg);
				String updateSQL = "update enterprise_asset set asset_category='IT ASSET', extension=? where id = ?";
				pst = conn.prepareStatement(updateSQL);
				pst.setString(1, extObj.toString());
				pst.setString(2, targetAssetId);
				pst.executeUpdate();
				pst.close();
				conn.commit();
			}
		}

		// Get Target OS Id
		String osId=null;
		if(os != null && os.length()>0) {
			String sql = "select id from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, os.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				osId = rs.getString("id");
			}
			rs.close();
			pst.close();

			// Add OS if not present
			if(osId == null || osId.length() == 0) {
				System.out.println("Asset not found. Adding["+os+"]");
				osId = generateUUID();
				String insertSQL = "insert into enterprise_asset(id,asset_name,company_code,asset_category,created_by,updated_by,created_timestamp) values(?,?,?,'SOFTWARE','support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, osId);
				pst.setString(2, os);
				pst.setString(3, companyCode);
				pst.executeUpdate();
				pst.close();
				conn.commit();
			} else {
				String updateSQL = "update enterprise_asset set asset_category='SOFTWARE' where id = ?";
				pst = conn.prepareStatement(updateSQL);
				pst.setString(1, osId);
				pst.executeUpdate();
				pst.close();
				conn.commit();
			}

			// Check if OS and Target Asset are linked. If not, link it
			int count = 0;
			sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, targetAssetId);
			pst.setString(2, osId);
			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "ASSET");
				ctx.put("LINKED", "N");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+targetAssetId+"','"+osId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();

				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+osId+"','"+targetAssetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}
		}

		String[] assets = asset.split(",");
		for(int i=0; i<assets.length; i++) {
			String assetName = assets[i].trim();

			// Get Asset Id
			String assetId = null;
			String sql = "select id, extension from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	rs = pst.executeQuery();
	        while (rs.next()) {
				assetId = rs.getString("id");
				ext = rs.getString("extension");
	        }
	        rs.close();
			pst.close();
			
			int count = 0;
			if(assetId == null || assetId.length() == 0) {
				System.out.println("Asset not found. Adding["+assetName+"]");
				assetId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("Ecosystem", ecosystem);
				extObj.put("OEM Vendor", assetMfg);
				String insertSQL = "insert into enterprise_asset(id,asset_name,extension,company_code,asset_category,created_by,updated_by,created_timestamp) values(?,?,?,?,'SOFTWARE','support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, assetId);
				pst.setString(2, assetName);
				pst.setString(3, extObj.toString());
				pst.setString(4, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			} else {
				System.out.println("Asset found. Updating["+assetName+":"+assetMfg+"]");
				OrderedJSONObject extObj = null;
				if(ext != null && ext.length() > 0) {
					extObj = new OrderedJSONObject(ext);
				} else {
					extObj = new OrderedJSONObject();
				}
				extObj.put("Ecosystem", ecosystem);
				extObj.put("OEM Vendor", assetMfg);
				String updateSQL = "update enterprise_asset set extension=? where id = ?";
				pst = conn.prepareStatement(updateSQL);
				pst.setString(1, extObj.toString());
				pst.setString(2, assetId);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, threatId);
				pst.setString(2, assetId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			}
		
			// Insert Relationship
			if(count == 0) {
				OrderedJSONObject threatCtx = new OrderedJSONObject();
				threatCtx.put("id", threatId);
				threatCtx.put("threatName", threatName);
				threatCtx.put("lifecycle", lifecycle);
				threatCtx.put("priority", priority);
				threatCtx.put("type", "");
				
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT ASSET");
				ctx.put("LINKED", "N");
				ctx.put("THREAT", threatCtx);
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+threatId+"','"+assetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				ctx.put("COMPONENT TYPE", "ASSET");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+threatId+"','THREAT ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}

			// Check Asset to Target Asset Relationship
			count = 0;
			sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetId);
			pst.setString(2, targetAssetId);
			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "ASSET");
				ctx.put("LINKED", "N");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+targetAssetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();

				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+targetAssetId+"','"+assetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}
        }
        System.out.println("Successfully processed Asset List!!!");
    }
    
    private void processGroupAsset(String groupId, String groupName, String lifecycle, String priority, Map asset) throws Exception {
		System.out.println("processGroupAsset...");
		PreparedStatement pst = null;
		ResultSet rs = null;
        Iterator iter = asset.keySet().iterator();
        while(iter.hasNext()) {
			String assetName = (String)iter.next();
			Map assetDetail = (Map)asset.get(assetName);
			String ecosystem="",assetMfg="";
			if(assetDetail.containsKey("ecosystem")) {
				ecosystem = (String)assetDetail.get("ecosystem");
			}
			if(assetDetail.containsKey("assetMfg")) {
				assetMfg = (String)assetDetail.get("assetMfg");
			}
			
			// Get Asset Id
			String assetId = null, ext=null;
			String sql = "select id, extension from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	rs = pst.executeQuery();
	        while (rs.next()) {
				assetId = rs.getString("id");
				ext = rs.getString("extension");
	        }
	        rs.close();
			pst.close();
			
			int count = 0;
			if(assetId == null || assetId.length() == 0) {
				System.out.println("Asset not found. Adding["+assetName+"]");
				assetId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("Ecosystem", ecosystem);
				extObj.put("OEM Vendor", assetMfg);
				String insertSQL = "insert into enterprise_asset(id,asset_name,extension,company_code,asset_category,created_by,updated_by,created_timestamp) values(?,?,?,?,'SOFTWARE','support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, assetId);
				pst.setString(2, assetName);
				pst.setString(3, extObj.toString());
				pst.setString(4, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			} else {
				OrderedJSONObject extObj = null;
				if(ext != null && ext.length() > 0) {
					extObj = new OrderedJSONObject(ext);
				} else {
					extObj = new OrderedJSONObject();
				}
				extObj.put("Ecosystem", ecosystem);
				extObj.put("OEM Vendor", assetMfg);
				String updateSQL = "update enterprise_asset set asset_category='SOFTWARE', extension=? where id = ?";
				pst = conn.prepareStatement(updateSQL);
				pst.setString(1, extObj.toString());
				pst.setString(2, assetId);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, groupId);
				pst.setString(2, assetId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			}
			
			// Insert Relationship
			if(count == 0) {
				OrderedJSONObject threatCtx = new OrderedJSONObject();
				threatCtx.put("id", groupId);
				threatCtx.put("threatName", groupName);
				threatCtx.put("lifecycle", lifecycle);
				threatCtx.put("priority", priority);
				threatCtx.put("type", "");
				
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT GROUP");
				ctx.put("LINKED", "N");
				ctx.put("THREAT", threatCtx);
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+groupId+"','"+assetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				ctx.put("COMPONENT TYPE", "ASSET");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+groupId+"','THREAT GROUP','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}
        }
        System.out.println("Successfully processed Group Asset List!!!");
	}
	
	private void processCapability(String threatId, String threatName, String lifecycle, String priority, List capability) throws Exception {
        System.out.println("processCapability=>"+capability);
		Iterator capabilityIter = capability.iterator();
		while(capabilityIter.hasNext()) {
			String capabilityName = (String)capabilityIter.next();
			
			// Get Asset Id
			String capabilityId = null;
			String sql = "select id from enterprise_business_capability where child_capability_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, capabilityName);
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
	        	capabilityId = rs.getString("id");
	        }
	        rs.close();
	        pst.close();
			
			// Check if relationship exists
			if(capabilityId != null && capabilityId.length() > 0) {
				int count = 0;
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, threatId);
				pst.setString(2, capabilityId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			
				// Insert Relationship
				if(count == 0) {
					OrderedJSONObject threatCtx = new OrderedJSONObject();
					threatCtx.put("id", threatId);
					threatCtx.put("threatName", threatName);
					threatCtx.put("lifecycle", lifecycle);
					threatCtx.put("priority", priority);
					threatCtx.put("type", "");
					
					OrderedJSONObject ctx = new OrderedJSONObject();
					ctx.put("COMPONENT TYPE", "THREAT ASSET");
					ctx.put("LINKED", "N");
					ctx.put("THREAT", threatCtx);
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+threatId+"','"+capabilityId+"','CAPABILITY','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					ctx.put("COMPONENT TYPE", "CAPABILITY");
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+capabilityId+"','"+threatId+"','THREAT ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					conn.commit();
				}
			} else {
				System.out.println("Capability not found. Skipping["+capabilityName+"]");
			}
        }
        System.out.println("Successfully processed Capability List!!!");
    }
    
    private void processGroupCapability(String groupId, String groupName, String lifecycle, String priority, List capability) throws Exception {
        System.out.println("processGroupCapability=>"+capability);
		Iterator iter = capability.iterator();
        while(iter.hasNext()) {
			String capabilityName = (String)iter.next();
			
			// Get Asset Id
			String capabilityId = null;
			String sql = "select id from enterprise_business_capability where child_capability_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, capabilityName);
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
	        	capabilityId = rs.getString("id");
	        }
	        rs.close();
	        pst.close();
			
			// Check if relationship exists
			if(capabilityId != null && capabilityId.length() > 0) {
				int count = 0;
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, groupId);
				pst.setString(2, capabilityId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			
				// Insert Relationship
				if(count == 0) {
					OrderedJSONObject threatCtx = new OrderedJSONObject();
					threatCtx.put("id", groupId);
					threatCtx.put("threatName", groupName);
					threatCtx.put("lifecycle", lifecycle);
					threatCtx.put("priority", priority);
					threatCtx.put("type", "");
					
					OrderedJSONObject ctx = new OrderedJSONObject();
					ctx.put("COMPONENT TYPE", "THREAT GROUP");
					ctx.put("LINKED", "N");
					ctx.put("THREAT", threatCtx);
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+groupId+"','"+capabilityId+"','CAPABILITY','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					ctx.put("COMPONENT TYPE", "CAPABILITY");
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+capabilityId+"','"+groupId+"','THREAT GROUP','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					conn.commit();
				}
			} else {
				System.out.println("Capability not found. Skipping["+capabilityName+"]");
			}
        }
        System.out.println("Successfully processed Group Capability List!!!");
	}
	
	private void addVulnerabilityToGroup(ObjectId id, String threatName, String asset, String ecosystem, String assetMfg, String targetAsset, String os, List capability) throws Exception {
		System.out.println("Adding threat to group..."+id+":"+threatName+":"+asset+":"+targetAsset+":"+os+":"+capability);
		boolean found = false;
		Iterator iter = groupList.keySet().iterator();
		while(iter.hasNext() && !found) {
			String groupName = (String)iter.next();
			System.out.println(groupName);
            JSONArray tagList = (JSONArray)groupConfig.get(groupName);
			Iterator tagIter = tagList.iterator();
			System.out.println(groupName+":"+tagList.toString());
            while(tagIter.hasNext() && !found) {
                String tagName = ((String)tagIter.next()).toLowerCase();
                if(threatName.indexOf(tagName) != -1) {
					System.out.println("Adding threat to group:"+id+":"+threatName);
					Map threatList = (Map)groupList.get(groupName);
					
					int count = threatList.size();
					if(!threatList.containsValue(id)) {
						threatList.put(Integer.toString(count), id);
					}
					if(threatClassIdList != null) {
						threatList.put("COUNT", new Integer(threatClassIdList.size()));
					}
					groupList.put(groupName, threatList);
					
					System.out.println(threatList.toString());
					// Add Assets to group
					if(asset != null && asset.length() > 0) {
						Map list = (Map)groupAssetList.get(groupName);
						String[] assetList = asset.split(",", -1);
						for(int i=0; i<assetList.length; i++) {
							String assetName = assetList[i].trim();
							System.out.println("Adding asset to group:"+assetName+":"+list.size());
							if(assetName != null && assetName.length()>0 && !list.containsKey(assetName)) {
								Map assetDetail = new HashMap();
								assetDetail.put("asset", assetName.trim());
								assetDetail.put("ecosystem", ecosystem);
								assetDetail.put("assetMfg", assetMfg);
								assetDetail.put("targetAsset", targetAsset);
								assetDetail.put("os", os);
								list.put(assetName, assetDetail);
							}
						}
						if(assetClassList != null) {
							 list.put("COUNT", new Integer(assetClassList.size()));
						}
						groupAssetList.put(groupName, list);
						System.out.println("Asset added to group!!!");
					}

					// Add Capabilities to group
					if(capability != null && capability.size() > 0) {
						List list = (ArrayList)groupCapabilityList.get(groupName);
						Iterator capabilityIter = capability.iterator();
						while(capabilityIter.hasNext()) {
							String capabilityName = (String)capabilityIter.next();
							list.add(capabilityName);
						}
						if(capabilityList != null) {
							list.add("COUNT:"+new Integer(capabilityList.size()));
						}
						groupCapabilityList.put(groupName, list);
					}
					found = true;
                }
            }
		}

		if(!found) {
			System.out.println("Adding threat to default group:"+id+":"+threatName);
			Map threatList = (Map)groupList.get(defaultGroupName);
			int count = threatList.size();
			if(!threatList.containsValue(id)) {
				threatList.put(Integer.toString(count), id);
			}
			if(threatClassIdList != null) {
				threatList.put("COUNT", new Integer(threatClassIdList.size()));
			}
			groupList.put(defaultGroupName, threatList);
			System.out.println("Added threats to default group!!!");

			// Add Assets to group
			if(asset != null && asset.length() > 0) {
				Map list = (Map)groupAssetList.get(defaultGroupName);
				String[] assetList = asset.split(",", -1);
				for(int i=0; i<assetList.length; i++) {
					String assetName = assetList[i].trim();
					System.out.println("Adding asset to Default group:"+assetName+":"+list.size());
					if(assetName != null && assetName.length()>0 && !list.containsKey(assetName)) {
						Map assetDetail = new HashMap();
						assetDetail.put("asset", assetName.trim());
						assetDetail.put("targetAsset", targetAsset);
						assetDetail.put("os", os);
						list.put(assetName, assetDetail);
					}
				}
				if(assetClassList != null) {
					list.put("COUNT", new Integer(assetClassList.size()));
			   	}
				groupAssetList.put(defaultGroupName, list);
				System.out.println("Added Assets to default group!!!");
			}

			// Add Capabilities to group
			if(capability != null && capability.size() > 0) {
				List list = (ArrayList)groupCapabilityList.get(defaultGroupName);
				Iterator capabilityIter = capability.iterator();
				while(capabilityIter.hasNext()) {
					String capabilityName = (String)capabilityIter.next();
					list.add(capabilityName);
				}
				if(capabilityList != null) {
					list.add("COUNT:"+new Integer(capabilityList.size()));
				}
				groupCapabilityList.put(defaultGroupName, list);

				System.out.println("Added Capabilities to default group!!!");
			}
		}
	}
	
	private void createGroups() throws Exception {
		System.out.println("Createing groups...Group List...");
		type = "Vulnerability";
		int count = 1;
		Iterator iter = groupList.keySet().iterator();
		while(iter.hasNext()) {
			String name = (String)iter.next();

			Map threatList = (Map)groupList.get(name);
			if(threatList.size() > 0) {
				Integer threatCount = new Integer(0);
				if(threatList.containsKey("COUNT")) {
					threatCount = (Integer)threatList.get("COUNT");
					threatList.remove("COUNT");
				}

				BasicBSONList list = new BasicBSONList();
				list.putAll(threatList);
				
				// check if Threat Group exists
				BasicDBObject criteria = new BasicDBObject();
				criteria.put("nameId", name);
				//int count = getCount(criteria, "AssetGroup");
				Document doc = getDocument(criteria, companyCode, "AssetGroup");
				if(doc != null) {
					removeDocument(criteria, "AssetGroup");
					System.out.println("Successfully removed collection["+name+"]!!!");
					/*
					if(name.lastIndexOf(":") == -1) {
						name = name + ":1";
					} else {
						String s = name.substring(name.lastIndexOf(":"));
						if(s.length() <= 2) {
							s = Integer.toString(Integer.parseInt(s)+1);
							name = name + ":" + s; 
						} else {
							name = name + ":1";
						}
					}
					*/
					count = currentCount;
				} else { // Increment Count
					count = currentCount + 1;
				}
		
				System.out.println("Inserting Threat Group...");		
				BasicDBObject group = new BasicDBObject();
				ObjectId id = ObjectId.get();
				group.put("_id", id);
				group.put("assets", list);
				group.put("subscribed_Users",  new BasicBSONList());
				group.put("nameId", name);
				group.put("name", name.substring(0,name.indexOf("-")));
				group.put("description", "");
				group.put("domain", "");
				group.put("subDomain", "");
				group.put("type", type);
				group.put("currentCount", new Integer(count));
				group.put("source", adapterType);
				group.put("createdAt", createdAt);
				group.put("updatedAt", updatedAt);

				Integer assetCount = new Integer(0);
				Map asset = null;
				if(groupAssetList.containsKey(name)) {
					asset = (Map)groupAssetList.get(name);
					if(asset.containsKey("COUNT")) {
						assetCount = (Integer)asset.get("COUNT");
						asset.remove("COUNT");
					}
				}
				Integer controlCount = new Integer(0);
				List capability = null;
				if(groupCapabilityList.containsKey(name)) {
					String countKey = null;
					capability = (List)groupCapabilityList.get(name);
					Iterator capabilityIter = capability.iterator();
					while(capabilityIter.hasNext()) {
						String c = (String)capabilityIter.next();
						if(c.indexOf("COUNT:") != -1) {
							controlCount = new Integer(c.substring(c.indexOf(":")+1));
							countKey = c;
							break;
						}
					}
					if(countKey != null) {
						capability.remove(countKey);
					}
				}
				
				BasicDBObject extension = new BasicDBObject();
				extension.put("lifecycle", "New");
				extension.put("threats", threatCount);
				extension.put("assets", assetCount);
				extension.put("controls", controlCount);
				group.put("extension", extension);

				group.put("owner",  new BasicBSONList());
				DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetGroup");
				coll.insert(group);
				
				// Process Group Asset
				if(asset != null) {
					processGroupAsset(id.toString(), name, "NEW", "0", asset);
				}

				// Process Group Capability
				if(capability != null) {
					processGroupCapability(id.toString(), name, "NEW", "0", capability);
				}
			}
		}
	}
	
	private void initializeThreatGroup() throws Exception {
		Iterator iter = groupConfig.keySet().iterator();
		while(iter.hasNext()) {
			String name = (String)iter.next();
			groupList.put(name, new HashMap());
            groupAssetList.put(name, new HashMap());
            groupCapabilityList.put(name, new ArrayList());
		}

		System.out.println("Successfully initialized groups!!!");
	}

	private void initializeThreatGroupConfig(String timestamp) throws Exception {
		// Get Group Config
        BasicDBObject criteria = new BasicDBObject();
		criteria.put("company_code", companyCode);
        MongoCollection coll = mongoClient.getDatabase("SE").getCollection("Enterprise");
        Document doc = (Document)coll.find(criteria).first();
		String  docStr = doc.toJson();
		OrderedJSONObject obj = new OrderedJSONObject(docStr);
		System.out.println("Enterprise Setting["+companyCode+"]:"+docStr);
		if(obj.has("enterprise_setting")) {
			OrderedJSONObject settingObj = (OrderedJSONObject)obj.get("enterprise_setting");
			if(settingObj.has("security")) {
				OrderedJSONObject securityObj = (OrderedJSONObject)settingObj.get("security");
				if(securityObj.has("group_categories")) {
					OrderedJSONObject grpCategory = (OrderedJSONObject)securityObj.get("group_categories");
					Iterator iter = grpCategory.keySet().iterator();
					while(iter.hasNext()) {
						String groupName = (String)iter.next();
						OrderedJSONObject groupData = (OrderedJSONObject)grpCategory.get(groupName);
						JSONArray tagList = null;
						if(groupData.has("tags")) {
							tagList = (JSONArray)groupData.get("tags");
						} else {
							tagList = new JSONArray();
						}
						
						boolean timeDependant = false;
						if(groupData.has("timeDependent")) {
							timeDependant = ((Boolean)groupData.get("timeDependent")).booleanValue();
							if(timeDependant) {
								//String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
								groupName = groupName+"-"+timestamp;
							}
						}
						
						// String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
						//groupName = groupName+"-"+timestamp;
						groupConfig.put(groupName, tagList);
					}
				}
			}
		}

		// Add default group
		//String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
		defaultGroupName = "Default-"+timestamp;
		groupConfig.put(defaultGroupName, new JSONArray());
	}

	private void getCapabilityListFromAsset(String asset) throws Exception {
		System.out.println("getCapabilityListFromAsset=>"+asset);
		
		assetClassList = getAssetClassFromAsset(asset);
		
		threatClassIdList = getThreatClassFromAssetClass(assetClassList);

		// Get Capability Details
		capabilityList = getCapabilityDetailsFromThreatClass(threatClassIdList);

		//List capabilityList = getCapabilityListFromAssetClass(assetClassList);
	}
	
	private List getAssetClassFromAsset(String asset) throws Exception {
		List assetClassList = new ArrayList();
		
		String[] assetList = asset.split(",", -1);

		// Get Asset Class for all assets
		for(int i=0; i<assetList.length; i++) {
			String assetName = assetList[i].trim();
			
			String sql = "select asset_category,replace(json_extract(extension, '$.\"Type\"'), '\"', '') assetclass1,replace(json_extract(extension, '$.\"Sub-Type\"'), '\"', '') assetclass2,json_extract(extension, '$.\"Ecosystem\"') assetclass3,replace(json_extract(extension, '$.\"Business app/IT app\"'), '\"', '') assetclass4 from enterprise_asset where asset_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
                String assetCategory = rs.getString("asset_category");
                String assetClass1 = rs.getString("assetclass1");
                String assetClass2 = rs.getString("assetclass2");
                String assetClass3 = rs.getString("assetclass3");
				String assetClass4 = rs.getString("assetclass4");
				if(assetClass1 != null && assetClass1.length() > 0) {
					assetClassList.add(assetClass1.toLowerCase());
				}
				if(assetClass2 != null && assetClass2.length() > 0) {
					assetClassList.add(assetClass2.toLowerCase());
				}
				if(assetClass3 != null && assetClass3.length() > 0 && assetClass3.indexOf("[\"") != -1) {
					JSONArray arr = new JSONArray(assetClass3);
					Iterator arrIter = arr.iterator();
					while(arrIter.hasNext()) {
						String str = (String)arrIter.next();
						if(str != null && str.length() > 0) {
							assetClassList.add(str.toLowerCase());
						}
					}
				} else if(assetClass3 != null && assetClass3.length() > 0 && assetClass3.indexOf("[") != -1) {
					assetClass3 = assetClass3.replaceAll("[", "");
					assetClass3 = assetClass3.replaceAll("]", "");
					String[] str = assetClass3.split(",");
					for(int j=0; j<str.length; j++) {
						String cl = str[j];
						assetClassList.add(cl.toLowerCase());
					}
				} else if(assetClass3 != null && assetClass3.length() > 0) {
					assetClass3 = assetClass3.replaceAll("\"", "");
					if(assetClass3.length() > 0) {
						assetClassList.add(assetClass3.toLowerCase());
					}
				}
                if(assetClass4 != null && assetClass4.length() > 0 && assetCategory.equals("APPLICATION")) {
                    assetClassList.add(assetClass4.toLowerCase());
                }
                System.out.println("Asset Id:"+assetName+":"+assetClass1+":"+assetClass2+":"+assetClass3+":"+assetClass4);
	        }
	        rs.close();
            pst.close();
		}

		return assetClassList;
	}
	
	private List getThreatClassFromAssetClass(List assetClassList) throws Exception {
		List<String> threatClassIdList = new ArrayList<String>();

		// Get Threat Class for all Asset Class
		Iterator iter = assetClassList.iterator();
		while(iter.hasNext()) {
			String assetClass = (String)iter.next();

			// Iterator through all the threat asset class and find if assetclass matches
			if(assetClass != null) {
				Iterator threatClassIter = threatClassList.iterator();
				while(threatClassIter.hasNext()) {
					DBObject obj = (DBObject)threatClassIter.next();
					if(obj.get("assetclass") != null) {
						String threatAssetClass = ((String)obj.get("assetclass")).toLowerCase();
						System.out.println(assetClass+":"+threatAssetClass);
						if(threatAssetClass != null && threatAssetClass.indexOf(assetClass) != -1) {
							threatClassIdList.add(obj.get("_id").toString());
						}
					}
				}
			}
		}

		return threatClassIdList;
	}

	private List getCapabilityDetailsFromThreatClass(List threatClassList) throws Exception {
        System.out.println("getCapabilityDetails:"+threatClassList.toString());
        List list = new ArrayList();
        
        String parm = "";
        Iterator iter = threatClassList.iterator();
        while(iter.hasNext()) {
            String threatClassId = ((String)iter.next());
            /*
            if(parm.length() == 0) {
                parm = "'"+threatClassId+"'";
            } else {
                parm = ",'"+threatClassId+"'";
            }
			*/
			
			// Execute Query
            String sql = "select b.id id from enterprise_rel a, enterprise_business_capability b where a.asset_id = ? and a.component_id=b.id and a.asset_rel_type=? and json_extract(asset_rel_context, '$.\"COMPONENT TYPE\"') = 'CAPABILITY'";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, threatClassId);
            pst.setString(2, "THREAT CLASS");
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                String id = rs.getString("id");
                System.out.println("Id:"+id);
                list.add(id);
			}
			rs.close();
			pst.close();
        }
        
        return list;
    }

	private void loadThreatClass() throws Exception {
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetClass");
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		fields.put("csf", 1);
		fields.put("assetclass", 2);
		threatClassList = coll.find(allQuery,fields).toArray();
	}

	private void getCurrentCount() throws Exception {
		BasicDBObject criteria = new BasicDBObject();
		criteria.put("company_code", companyCode);
		Document doc = getDocument(criteria, "SE", "Enterprise");
		Document tenableSetting = (Document)((Document)doc.get("integration_setting")).get(adapterType);
		if(tenableSetting.containsKey("currentCount")) {
			currentCount = ((Integer)tenableSetting.get("currentCount")).intValue();
		}
		System.out.println("Current Count:"+currentCount);
	}

	private void initializeTimestamp(String d) throws Exception {
		createdAt = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date(d));
		updatedAt = createdAt;
		System.out.println(createdAt+":"+updatedAt);
	}

	private void setLastGroupInactive() throws Exception {
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetGroup");
		List queryParm1 = new ArrayList();
		queryParm1.add(new BasicDBObject().append("extension.lifecycle","NEW"));
		//queryParm1.add(new BasicDBObject("extension.lifecycle","Active"));
		//List queryParm2 = new ArrayList();
		queryParm1.add(new BasicDBObject().append("source",adapterType));
		Iterator iter = groupList.keySet().iterator();
		while(iter.hasNext()) {
			String name = (String)iter.next();
			queryParm1.add(new BasicDBObject("name",name.substring(0, name.indexOf("-"))));
			BasicDBObject allQuery = new BasicDBObject();
			//allQuery.put("$or", queryParm1);
			allQuery.put("$and", queryParm1);
			BasicDBObject values = new BasicDBObject();
			values.put("updatedAt", updatedAt);
			values.put("extension.lifecycle", "Inactive");
			BasicDBObject update = new BasicDBObject("$set", values);
			coll.updateMulti(allQuery, update);
		}
		System.out.println("Successfully updated Asset group to Inactive status!!!");
	}

	private void updateCurrentCount() throws Exception {
		DBCollection coll = mongoClient.getDB("SE").getCollection("Enterprise");
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.put("company_code", companyCode);
		String key = "integration_setting."+adapterType+".currentCount";
		int count = currentCount + 1;
		BasicDBObject values = new BasicDBObject();
		values.put(key, new Integer(count));
		BasicDBObject update = new BasicDBObject("$set", values);
		coll.update(allQuery, update);
		System.out.println("Successfully updated currentCount["+count+"]!!!");
	}

	private String getPriorityValue(String priority) throws Exception {
		if(priority != null && priority.length() > 0) {
			if(priority.equalsIgnoreCase("critical")) {
				priority = "5";
			} else if(priority.equalsIgnoreCase("high")) {
				priority = "4";
			} else if(priority.equalsIgnoreCase("medium")) {
				priority = "3";
			} else if(priority.equalsIgnoreCase("low")) {
				priority = "2";
			} else {
				priority = "1";
			}
		}

		return priority;
	}
	
	private void mongoConnect(String dbname) throws Exception {
		String username = "smartEuser";
		String pwd = "smarterD2018!";
		//String hostname = "35.227.181.195";
		String hostname = "35.227.158.31";
		String encoded_pwd = URLEncoder.encode(pwd, "UTF-8");
        MongoClientURI connectionString = new MongoClientURI("mongodb://"+username+":"+encoded_pwd+"@"+hostname+":27017/"+dbname+"?maxPoolSize=10");
        mongoClient = new MongoClient(connectionString);
        db = mongoClient.getDatabase(dbname);
        System.out.println("Successfully connected to Mongo DB:"+dbname);
    }
    
    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
		//String hostname = "35.227.181.195";
		String hostname = "35.197.106.183";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
	}
	
	private String camelCase(String str) throws Exception {
		return(str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase());
    }

	private String formatDate(String d) throws Exception {
        String formattedDate = "";
        if(d != null && d.length() > 0) {
            if (d.length() == 7 && d.indexOf("/") != -1) {    //1/01/07
                SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yy");
                Date date = formatter.parse(d);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                formattedDate = Integer.toString(month)+"/"+Integer.toString(day)+"/"+Integer.toString(year);
            } else if (d.length() == 8 && d.indexOf("/") != -1) {
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
                Date date = formatter.parse(d);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                formattedDate = Integer.toString(month)+"/"+Integer.toString(day)+"/"+Integer.toString(year);
            } else {
                formattedDate = d;
            }
        }

        return formattedDate;
	}

	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		ThreatLoader loader = new ThreatLoader(filename, companyCode);
		loader.processThreat();
	}
}