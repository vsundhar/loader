import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.bson.Document; 
import org.bson.types.ObjectId;
import org.bson.types.BasicBSONList;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.Instant;
import com.tenable.io.api.TenableIoClient;
import com.tenable.io.api.exports.ExportsApi;
import com.tenable.io.api.exports.models.VulnsExportRequest;
import com.tenable.io.api.exports.models.VulnsExportFilters;
import com.tenable.io.api.models.SeverityLevel;
import com.tenable.io.api.models.VulnerabilityState;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;

public class TenableAdapter {
	private CSVReader reader = null;
	private String companyCode = null;
	private MongoClient mongoClient = null;
    private MongoDatabase db = null;	
    private Connection conn = null;
	private Map groupList = new HashMap();
	private Map groupConfig = new HashMap();
	private Map groupAssetList = new HashMap();
	private Map groupCapabilityList = new HashMap();
	private List<DBObject> threatClassList = new ArrayList<DBObject>();
	private String type = null;
	private String accessKey = "68ab6aed6703250a8ac5697afbd43e832b7fdd8f5d01d54dd4bac2785aa5c17a";
	private String secretKey = "4d4d53f3cc479fefd1acf37640b207c51e689a56ad61c01b65845a18892e9476";
	private String url = "cloud.tenable.com";
	private String schema = "https";
	private String user = "jchung-user@cybriant.com";
	private TenableIoClient tenableClient = null;
	
	public TenableAdapter(String companyCode) throws Exception {
		this.companyCode = companyCode;

        // Connect Mongo
        mongoConnect(companyCode);
        
        // Connect Mysql
		mysqlConnect(companyCode);

		// Connect Tenable
		//tenableClient = new TenableIoClient(url, schema, accessKey, secretKey);
		//System.out.println("Successfully connect to Tenable Cloud API!!!");
		
		// Initialize Threat Config
		//initializeThreatGroupConfig();
		//System.out.println("Group Config:"+groupConfig.toString());
        
        // Initialize Threat Group
		//initializeThreatGroup();
		//System.out.println("Group List:"+groupList.toString());
	}
	/*
	public void exportVulnerabilities() throws Exception {
		ExportsApi export = tenableClient.getExportsApi();

		// Create Filter
		VulnsExportFilters filter = new VulnsExportFilters();
		List<SeverityLevel> severity = new ArrayList<SeverityLevel>();
		severity.add(SeverityLevel.valueOf("CRITICAL"));
		severity.add(SeverityLevel.valueOf("HIGH"));
		severity.add(SeverityLevel.valueOf("MEDIUM"));
		filter.setSeverity(severity);

		List<VulnerabilityState> state = new ArrayList<VulnerabilityState>();
		state.add(VulnerabilityState.valueOf("OPEN"));
		state.add(VulnerabilityState.valueOf("REOPENED"));
		state.add(VulnerabilityState.valueOf("FIXED"));
		filter.setState(state);

		String lastDate = "06/01/2019";
		long lastTime = new java.util.Date(lastDate).getTime()/1000L;
		filter.setSince(lastTime);

		System.out.println(filter.toString());

		// Create Request
		VulnsExportRequest request = new VulnsExportRequest(5000, filter);
		String id = export.vulnsRequestExport(request);

		System.out.print("Export UUID:"+id);

		//export.vulnsDownloadChunk(id, 5000, Long.toString(lastTime)+"-Vulnerabilities-export");
	}
	*/

	public void exportVulnerabilities() throws Exception {
		url = "https://cloud.tenable.com/vulns/export";
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		post.setHeader("Accept", "application/json");
		post.setHeader("Content-type", "application/json");
		String key = "accessKey="+accessKey+";secretKey="+secretKey;
		System.out.println(key);
		post.setHeader("X-ApiKeys", key);

		String body = "{}";
		StringEntity entity = new StringEntity(body);
		post.setEntity(entity);

		HttpResponse response = client.execute(post);

		System.out.println(response.getStatusLine().getStatusCode());
	}
	
	public void processThreat() throws Exception {
		String[] row;
		int rowCount=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String lifecycle=null, priority=null, name=null, desc=null, action=null, asset=null;
				String cve=null, targetAsset=null, os=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						lifecycle = row[i];
					} else if(i==1) {
						cve = row[i];
					} else if(i==2) {
						priority = row[i];
					} else if(i==3) {
						name = row[i].trim();
					} else if(i==4) {
						desc = row[i];
					} else if(i==5) {
						action = row[i];
					} else if(i==6) {
						type = row[i]; 
					} else if(i==7) {
						asset = row[i];
					} else if(i==8) {
						targetAsset = row[i];
					} else if(i==9) {
						os = row[i];
					}
				}

				// Get Capability List from Asset
				List capabilityList = getCapabilityListFromAsset(asset);

				// check if Threat exists
				BasicDBObject criteria = new BasicDBObject();
				criteria.put("nameId", name);
				//int count = getCount(criteria, "ThreatAsset");
				Document doc = getDocument(criteria, "ThreatAsset");
				ObjectId id = null;

				if(doc == null) {
					System.out.println("Threat not found. Inserting...");
					BasicDBObject threatAsset = new BasicDBObject();
					id = ObjectId.get();
					threatAsset.put("_id", id);
					threatAsset.put("subscribed_Users",  new BasicBSONList());
					threatAsset.put("groups",  new BasicBSONList());
					threatAsset.put("responses", "");
					threatAsset.put("companyCode", companyCode); 
					threatAsset.put("priority", priority);
					threatAsset.put("reportLabel", "");
					threatAsset.put("nameId", name);
					threatAsset.put("description", desc);
					threatAsset.put("domain", "");
					threatAsset.put("subDomain", "");
					threatAsset.put("coaType", "");
					threatAsset.put("coaCost", "");
					threatAsset.put("coaImpact", "");
					threatAsset.put("coaEfficacy", "");
					threatAsset.put("type", type);

					BasicDBObject owner = new BasicDBObject();
					owner.put("packages",  new BasicBSONList());
					owner.put("packageRels",  new BasicBSONList());
					owner.put("accessRights",  new BasicBSONList());
					threatAsset.put("owner", owner);

					BasicDBObject extension = new BasicDBObject();
					extension.put("riskComp", "");
					extension.put("riskScaled", "");
					extension.put("enterprise_action", action);
					extension.put("lifecycle", lifecycle);
					extension.put("os", os);
					extension.put("cve", cve);
					threatAsset.put("extension", extension);

					DBCollection coll = mongoClient.getDB(companyCode).getCollection("ThreatAsset");
					coll.insert(threatAsset);
					
					// Process Asset Relationship
					if(asset != null && asset.trim().length() > 0) {
						System.out.println("Processing Asset Relationship...["+asset+"]");
						processAsset(id.toString(), name, lifecycle, priority, asset, targetAsset, os);
					}
				
					// Process Capability Relationship
					if(capabilityList.size() > 0) {
						System.out.println("Processing Capability Relationship...["+capabilityList.toString()+"]");
						processCapability(id.toString(), name, lifecycle, priority, capabilityList);
					}
				} else {
					id = doc.getObjectId("_id");
					System.out.println("Threat found. Skipping..."+id.toString());
				}

				// Add Threat to group
				if(id != null) {
					addThreatToGroup(id, name.toLowerCase(), asset, targetAsset, os, capabilityList);
				}

				lifecycle = null;
				priority = null;
				name = null;
				desc = null;
				action = null;
				asset = null;
				targetAsset = null;
				os=null;
			}
		}
		
		// create Group
		createGroups();
	}
	
	private int getCount(DBObject query, String collName) throws Exception {
		DBCollection coll = mongoClient.getDB(companyCode).getCollection(collName);
		int count = coll.find(query).count();
		return count;
	}

	private Document getDocument(BasicDBObject criteria, String collName) throws Exception {
		MongoCollection coll = mongoClient.getDatabase(companyCode).getCollection(collName);
        Document doc = (Document)coll.find(criteria).first();
		return doc;
	}

	private void removeDocument(BasicDBObject criteria, String collName) throws Exception {
		MongoCollection coll = mongoClient.getDatabase(companyCode).getCollection(collName);
       	coll.deleteOne(criteria);
	}
	
	private void processAsset(String threatId, String threatName, String lifecycle, String priority, String asset, String targetAsset, String os) throws Exception {
		System.out.println("processAsset=>"+asset);
		PreparedStatement pst = null;
		ResultSet rs = null;

		// Get Target Asset Id
		String targetAssetId = null;
		if(targetAsset != null && targetAsset.length()>0) {
			String sql = "select id from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, targetAsset.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				targetAssetId = rs.getString("id");
			}
			rs.close();
			pst.close();

			if(targetAssetId == null || targetAssetId.length() == 0) {
				System.out.println("Asset not found. Adding["+targetAsset+"]");
				targetAssetId = generateUUID();
				OrderedJSONObject ext = new OrderedJSONObject();
				ext.put("OS", os);
				String insertSQL = "insert into enterprise_asset(id,asset_name,extension,company_code,created_by,updated_by,created_timestamp) values(?,?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, targetAssetId);
				pst.setString(2, targetAsset);
				pst.setString(3, ext.toString());
				pst.setString(4, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			}
		}

		// Get Target OS Id
		String osId = null;
		if(os != null && os.length()>0) {
			String sql = "select id from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, os.trim());
			rs = pst.executeQuery();
			while (rs.next()) {
				osId = rs.getString("id");
			}
			rs.close();
			pst.close();

			// Add OS if not present
			if(osId == null || osId.length() == 0) {
				System.out.println("Asset not found. Adding["+os+"]");
				osId = generateUUID();
				String insertSQL = "insert into enterprise_asset(id,asset_name,company_code,created_by,updated_by,created_timestamp) values(?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, osId);
				pst.setString(2, os);
				pst.setString(3, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			}
		}

		String[] assetList = asset.split(",", -1);
		for(int i=0; i<assetList.length; i++) {
			String assetName = assetList[i].trim();
			
			// Get Asset Id
			String assetId = null;
			String sql = "select id from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	rs = pst.executeQuery();
	        while (rs.next()) {
	        	assetId = rs.getString("id");
	        }
	        rs.close();
			pst.close();
			
			int count = 0;
			if(assetId == null || assetId.length() == 0) {
				System.out.println("Asset not found. Adding["+assetName+"]");
				assetId = generateUUID();
				String insertSQL = "insert into enterprise_asset(id,asset_name,company_code,created_by,updated_by,created_timestamp) values(?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, assetId);
				pst.setString(2, assetName);
				pst.setString(3, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			} else {
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, threatId);
				pst.setString(2, assetId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			}
		
			// Insert Relationship
			if(count == 0) {
				OrderedJSONObject threatCtx = new OrderedJSONObject();
				threatCtx.put("id", threatId);
				threatCtx.put("threatName", threatName);
				threatCtx.put("lifecycle", lifecycle);
				threatCtx.put("priority", priority);
				threatCtx.put("type", "");
				
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT ASSET");
				ctx.put("LINKED", "N");
				ctx.put("THREAT", threatCtx);
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+threatId+"','"+assetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				ctx.put("COMPONENT TYPE", "ASSET");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+threatId+"','THREAT ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}

			// Check Asset to Target Asset Relationship
			count = 0;
			sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetId);
			pst.setString(2, targetAssetId);
			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "ASSET");
				ctx.put("LINKED", "N");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+targetAssetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}
        }
        System.out.println("Successfully processed Asset List!!!");
    }
    
    private void processGroupAsset(String groupId, String groupName, String lifecycle, String priority, Map asset) throws Exception {
		System.out.println("processGroupAsset...");
		PreparedStatement pst = null;
		ResultSet rs = null;
        Iterator iter = asset.keySet().iterator();
        while(iter.hasNext()) {
			String assetName = (String)iter.next();
			Map assetDetail = (Map)asset.get(assetName);
			String targetAsset = (String)assetDetail.get("targetAsset");
			String os = (String)assetDetail.get("os");
			String targetAssetId = null;
			if(targetAsset != null && targetAsset.length()>0) {
				String sql = "select id from enterprise_asset where asset_name = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, targetAsset.trim());
				rs = pst.executeQuery();
				while (rs.next()) {
					targetAssetId = rs.getString("id");
				}
				rs.close();
				pst.close();
	
				if(targetAssetId == null || targetAssetId.length() == 0) {
					System.out.println("Asset not found. Adding["+targetAsset+"]");
		
					targetAssetId = generateUUID();
					OrderedJSONObject ext = new OrderedJSONObject();
					ext.put("OS", os);
					String insertSQL = "insert into enterprise_asset(id,asset_name,extension,company_code,created_by,updated_by,created_timestamp) values(?,?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
					pst = conn.prepareStatement(insertSQL);
					pst.setString(1, targetAssetId);
					pst.setString(2, targetAsset);
					pst.setString(3, ext.toString());
					pst.setString(4, companyCode);
					pst.executeUpdate();
					conn.commit();
					pst.close();
				}
			}
	
			// Get Target OS Id
			String osId = null;
			if(os != null && os.length()>0) {
				String sql = "select id from enterprise_asset where asset_name = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, os.trim());
				rs = pst.executeQuery();
				while (rs.next()) {
					osId = rs.getString("id");
				}
				rs.close();
				pst.close();
	
				// Add OS if not present
				if(osId == null || osId.length() == 0) {
					System.out.println("Asset not found. Adding["+os+"]");
					osId = generateUUID();
					String insertSQL = "insert into enterprise_asset(id,asset_name,company_code,created_by,updated_by,created_timestamp) values(?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
					pst = conn.prepareStatement(insertSQL);
					pst.setString(1, osId);
					pst.setString(2, os);
					pst.setString(3, companyCode);
					pst.executeUpdate();
					conn.commit();
					pst.close();
				}
			}
			
			// Get Asset Id
			String assetId = null;
			String sql = "select id from enterprise_asset where asset_name = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	rs = pst.executeQuery();
	        while (rs.next()) {
	        	assetId = rs.getString("id");
	        }
	        rs.close();
			pst.close();
			
			int count = 0;
			if(assetId == null || assetId.length() == 0) {
				System.out.println("Asset not found. Adding["+assetName+"]");
				assetId = generateUUID();
				String insertSQL = "insert into enterprise_asset(id,asset_name,company_code,created_by,updated_by,created_timestamp) values(?,?,?,'support@smarterd.com','support@smarterd.com',current_timestamp)";
				pst = conn.prepareStatement(insertSQL);
				pst.setString(1, assetId);
				pst.setString(2, assetName);
				pst.setString(3, companyCode);
				pst.executeUpdate();
				conn.commit();
				pst.close();
			} else {
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, groupId);
				pst.setString(2, assetId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			}
			
			// Insert Relationship
			if(count == 0) {
				OrderedJSONObject threatCtx = new OrderedJSONObject();
				threatCtx.put("id", groupId);
				threatCtx.put("threatName", groupName);
				threatCtx.put("lifecycle", lifecycle);
				threatCtx.put("priority", priority);
				threatCtx.put("type", "");
				
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "THREAT GROUP");
				ctx.put("LINKED", "N");
				ctx.put("THREAT", threatCtx);
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+groupId+"','"+assetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				ctx.put("COMPONENT TYPE", "ASSET");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+groupId+"','THREAT GROUP','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}

			// Check Asset to Target Asset Relationship
			count = 0;
			sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, assetId);
			pst.setString(2, targetAssetId);
			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count == 0) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				ctx.put("COMPONENT TYPE", "ASSET");
				ctx.put("LINKED", "N");
				sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
				sql += "null,'"+assetId+"','"+targetAssetId+"','ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
			
				pst = conn.prepareStatement(sql);
				pst.executeUpdate();
				pst.close();
			
				conn.commit();
			}
        }
        System.out.println("Successfully processed Group Asset List!!!");
	}
	
	private void processCapability(String threatId, String threatName, String lifecycle, String priority, List capability) throws Exception {
        System.out.println("processCapability=>"+capability);
		Iterator capabilityIter = capability.iterator();
		while(capabilityIter.hasNext()) {
			String capabilityName = (String)capabilityIter.next();
			
			// Get Asset Id
			String capabilityId = null;
			String sql = "select id from enterprise_business_capability where child_capability_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, capabilityName);
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
	        	capabilityId = rs.getString("id");
	        }
	        rs.close();
	        pst.close();
			
			// Check if relationship exists
			if(capabilityId != null && capabilityId.length() > 0) {
				int count = 0;
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, threatId);
				pst.setString(2, capabilityId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			
				// Insert Relationship
				if(count == 0) {
					OrderedJSONObject threatCtx = new OrderedJSONObject();
					threatCtx.put("id", threatId);
					threatCtx.put("threatName", threatName);
					threatCtx.put("lifecycle", lifecycle);
					threatCtx.put("priority", priority);
					threatCtx.put("type", "");
					
					OrderedJSONObject ctx = new OrderedJSONObject();
					ctx.put("COMPONENT TYPE", "THREAT ASSET");
					ctx.put("LINKED", "N");
					ctx.put("THREAT", threatCtx);
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+threatId+"','"+capabilityId+"','CAPABILITY','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					ctx.put("COMPONENT TYPE", "CAPABILITY");
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+capabilityId+"','"+threatId+"','THREAT ASSET','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					conn.commit();
				}
			} else {
				System.out.println("Capability not found. Skipping["+capabilityName+"]");
			}
        }
        System.out.println("Successfully processed Capability List!!!");
    }
    
    private void processGroupCapability(String groupId, String groupName, String lifecycle, String priority, List capability) throws Exception {
        System.out.println("processGroupCapability=>"+capability);
		Iterator iter = capability.iterator();
        while(iter.hasNext()) {
			String capabilityName = (String)iter.next();
			
			// Get Asset Id
			String capabilityId = null;
			String sql = "select id from enterprise_business_capability where child_capability_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, capabilityName);
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
	        	capabilityId = rs.getString("id");
	        }
	        rs.close();
	        pst.close();
			
			// Check if relationship exists
			if(capabilityId != null && capabilityId.length() > 0) {
				int count = 0;
				sql = "select count(*) count from enterprise_rel where component_id=? and asset_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setString(1, groupId);
				pst.setString(2, capabilityId);
				rs = pst.executeQuery();
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				pst.close();
			
				// Insert Relationship
				if(count == 0) {
					OrderedJSONObject threatCtx = new OrderedJSONObject();
					threatCtx.put("id", groupId);
					threatCtx.put("threatName", groupName);
					threatCtx.put("lifecycle", lifecycle);
					threatCtx.put("priority", priority);
					threatCtx.put("type", "");
					
					OrderedJSONObject ctx = new OrderedJSONObject();
					ctx.put("COMPONENT TYPE", "THREAT GROUP");
					ctx.put("LINKED", "N");
					ctx.put("THREAT", threatCtx);
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+groupId+"','"+capabilityId+"','CAPABILITY','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					ctx.put("COMPONENT TYPE", "CAPABILITY");
					sql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(";
					sql += "null,'"+capabilityId+"','"+groupId+"','THREAT GROUP','"+ctx.toString()+"','support@smarterd.com',current_timestamp,'support@smarterd.com',current_timestamp,'"+companyCode+"')";
				
					pst = conn.prepareStatement(sql);
					pst.executeUpdate();
					pst.close();
				
					conn.commit();
				}
			} else {
				System.out.println("Capability not found. Skipping["+capabilityName+"]");
			}
        }
        System.out.println("Successfully processed Group Capability List!!!");
	}
	
	private void addThreatToGroup(ObjectId id, String threatName, String asset, String targetAsset, String os, List capability) throws Exception {
		System.out.println("Adding threat to group..."+id+":"+threatName+":"+asset+":"+capability);
		boolean found = false;
		Iterator iter = groupList.keySet().iterator();
		while(iter.hasNext() && !found) {
			String groupName = (String)iter.next();
			System.out.println(groupName);
            JSONArray tagList = (JSONArray)groupConfig.get(groupName);
			Iterator tagIter = tagList.iterator();
			System.out.println(groupName+":"+tagList.toString());
            while(tagIter.hasNext() && !found) {
                String tagName = ((String)tagIter.next()).toLowerCase();
                if(threatName.indexOf(tagName) != -1) {
					System.out.println("Adding threat to group:"+id+":"+threatName);
					Map threatList = (Map)groupList.get(groupName);
					
					int count = threatList.size();
					if(!threatList.containsValue(id)) {
						threatList.put(Integer.toString(count), id);
					}
					groupList.put(groupName, threatList);
					
					System.out.println(threatList.toString());
					// Add Assets to group
					if(asset != null && asset.length() > 0) {
						Map list = (Map)groupAssetList.get(groupName);
						String[] assetList = asset.split(",", -1);
						for(int i=0; i<assetList.length; i++) {
							String assetName = assetList[i].trim();
							System.out.println("Adding asset to group:"+assetName);
							if(assetName != null && assetName.length()>0 && !list.containsKey(assetName)) {
								Map assetDetail = new HashMap();
								assetDetail.put("asset", assetName.trim());
								assetDetail.put("targetAsset", targetAsset);
								assetDetail.put("os", os);
								list.put(assetName, assetDetail);
								groupAssetList.put(groupName, list);
							}
						}
					}

					// Add Capabilities to group
					if(capability.size() > 0) {
						List list = (ArrayList)groupCapabilityList.get(groupName);
						Iterator capabilityIter = capability.iterator();
						while(capabilityIter.hasNext()) {
							String capabilityName = (String)capabilityIter.next();
							list.add(capabilityName);
						}
						groupCapabilityList.put(groupName, list);
					}
					found = true;
                }
            }
		}

		if(!found) {
			String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
			String groupName = "Default-"+timestamp;
			System.out.println("Adding threat to default group:"+id+":"+threatName);
			Map threatList = (Map)groupList.get(groupName);
			int count = threatList.size();
			if(!threatList.containsValue(id)) {
				threatList.put(Integer.toString(count), id);
			}
			groupList.put(groupName, threatList);
			
			// Add Assets to group
			if(asset != null && asset.length() > 0) {
				Map list = (Map)groupAssetList.get(groupName);
				String[] assetList = asset.split(",", -1);
				for(int i=0; i<assetList.length; i++) {
					String assetName = assetList[i].trim();
					System.out.println("Adding asset to Default group:"+assetName);
					if(assetName != null && assetName.length()>0 && !list.containsKey(assetName)) {
						Map assetDetail = new HashMap();
						assetDetail.put("asset", assetName.trim());
						assetDetail.put("targetAsset", targetAsset);
						assetDetail.put("os", os);
						list.put(assetName, assetDetail);
						groupAssetList.put(groupName, list);
					}
				}
			}

			// Add Capabilities to group
			if(capability.size() > 0) {
				List list = (ArrayList)groupCapabilityList.get(groupName);
				Iterator capabilityIter = capability.iterator();
				while(capabilityIter.hasNext()) {
					String capabilityName = (String)capabilityIter.next();
					list.add(capabilityName);
				}
				groupCapabilityList.put(groupName, list);
			}
		}
	}
	
	private void createGroups() throws Exception {
		System.out.println("Createing groups...Group List...");
		type = "Vulnerability";
		String date = new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date(System.currentTimeMillis()));
		Iterator iter = groupList.keySet().iterator();
		while(iter.hasNext()) {
			String name = (String)iter.next();

			Map threatList = (Map)groupList.get(name);
			BasicBSONList list = new BasicBSONList();
			list.putAll(threatList);
			
			// check if Threat Group exists
			BasicDBObject criteria = new BasicDBObject();
			criteria.put("nameId", name);
			//int count = getCount(criteria, "AssetGroup");
			Document doc = getDocument(criteria, "AssetGroup");
			if(doc != null) {
				removeDocument(criteria, "AssetGroup");
				System.out.println("Successfully removed collection["+name+"]!!!");
				/*
				if(name.lastIndexOf(":") == -1) {
					name = name + ":1";
				} else {
					String s = name.substring(name.lastIndexOf(":"));
					if(s.length() <= 2) {
						s = Integer.toString(Integer.parseInt(s)+1);
						name = name + ":" + s; 
					} else {
						name = name + ":1";
					}
				}
				*/
			}
	
			System.out.println("Inserting Threat Group...");		
			//String nameId = name+"-"+date;
			BasicDBObject group = new BasicDBObject();
			ObjectId id = ObjectId.get();
			group.put("_id", id);
			group.put("assets", list);
			group.put("subscribed_Users",  new BasicBSONList());
			group.put("nameId", name);
			group.put("description", "");
			group.put("domain", "");
			group.put("subDomain", "");
			group.put("type", type);
			
			BasicDBObject extension = new BasicDBObject();
			extension.put("lifecycle", "NEW");
			group.put("extension", extension);

			group.put("owner",  new BasicBSONList());
			DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetGroup");
			coll.insert(group);
			
			// Process Group Asset
			if(groupAssetList.containsKey(name)) {
				Map asset = (Map)groupAssetList.get(name);
				processGroupAsset(id.toString(), name, "NEW", "0", asset);
			}

			// Process Group Capability
			if(groupCapabilityList.containsKey(name)) {
				List capability = (List)groupCapabilityList.get(name);
				processGroupCapability(id.toString(), name, "NEW", "0", capability);
			}
		}
	}
	
	private void initializeThreatGroup() throws Exception {
		Iterator iter = groupConfig.keySet().iterator();
		while(iter.hasNext()) {
			String name = (String)iter.next();
			groupList.put(name, new HashMap());
            groupAssetList.put(name, new HashMap());
            groupCapabilityList.put(name, new ArrayList());
		}
		
		String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
		String groupName = "Default-"+timestamp;
        
        groupList.put(groupName, new HashMap());
        groupAssetList.put(groupName, new HashMap());
        groupCapabilityList.put(groupName, new ArrayList());

		System.out.println("Successfully initialized groups!!!");
	}

	private void initializeThreatGroupConfig() throws Exception {
		// Get Group Config
        BasicDBObject criteria = new BasicDBObject();
		criteria.put("company_code", companyCode);
        MongoCollection coll = mongoClient.getDatabase("SE").getCollection("Enterprise");
        Document doc = (Document)coll.find(criteria).first();
		String  docStr = doc.toJson();
		OrderedJSONObject obj = new OrderedJSONObject(docStr);
		System.out.println("Enterprise Setting["+companyCode+"]:"+docStr);
		if(obj.has("enterprise_setting")) {
			OrderedJSONObject settingObj = (OrderedJSONObject)obj.get("enterprise_setting");
			if(settingObj.has("security")) {
				OrderedJSONObject securityObj = (OrderedJSONObject)settingObj.get("security");
				if(securityObj.has("group_categories")) {
					OrderedJSONObject grpCategory = (OrderedJSONObject)securityObj.get("group_categories");
					Iterator iter = grpCategory.keySet().iterator();
					while(iter.hasNext()) {
						String groupName = (String)iter.next();
						OrderedJSONObject groupData = (OrderedJSONObject)grpCategory.get(groupName);
						JSONArray tagList = null;
						if(groupData.has("tags")) {
							tagList = (JSONArray)groupData.get("tags");
						} else {
							tagList = new JSONArray();
						}
						/*
						boolean timeDependant = false;
						if(groupData.has("timeDependent")) {
							timeDependant = ((Boolean)groupData.get("timeDependent")).booleanValue();
							if(timeDependant) {
								String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
								groupName = groupName+"-"+timestamp;
							}
						}
						*/
						String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
						groupName = groupName+"-"+timestamp;
						groupConfig.put(groupName, tagList);
					}
				}
			}
		}

		// Add default group
		String timestamp = new SimpleDateFormat("MMddyyyy").format(new java.util.Date());
		String groupName = "Default-"+timestamp;
		groupConfig.put(groupName, new JSONArray());
	}

	private List getCapabilityListFromAsset(String asset) throws Exception {
		System.out.println("getCapabilityListFromAsset=>"+asset);
		List<String> threatClassIdList = new ArrayList<String>();
		List assetClassList = new ArrayList();
		String[] assetList = asset.split(",", -1);

		// Get Asset Class for all assets
		for(int i=0; i<assetList.length; i++) {
			String assetName = assetList[i].trim();
			
			// Get Asset Id
			String sql = "select replace(json_extract(extension, '$.\"Type\"'), '\"', '') assetclass from enterprise_asset where asset_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, assetName.trim());
        	ResultSet rs = pst.executeQuery();
	        while (rs.next()) {
				String assetClass = rs.getString("assetClass");
				assetClassList.add(assetClass);
	        }
	        rs.close();
			pst.close();
		}

		// Get Threat Class for all Asset Class
		Iterator iter = assetClassList.iterator();
		while(iter.hasNext()) {
			String assetClass = (String)iter.next();

			// Iterator through all the threat asset class and find if assetclass matches
			if(assetClass != null) {
				Iterator threatClassIter = threatClassList.iterator();
				while(threatClassIter.hasNext()) {
					DBObject obj = (DBObject)threatClassIter.next();
					String threatAssetClass = (String)obj.get("assetclass");
					System.out.println(assetClass+":"+threatAssetClass);
					if(threatAssetClass != null && threatAssetClass.indexOf(assetClass) != -1) {
						threatClassIdList.add(obj.get("_id").toString());
					}
				}
			}
		}

		// Get Capability Details
		List capabilityList = getCapabilityDetailsFromThreatClass(threatClassIdList);

		return capabilityList;
	}

	private List getCapabilityDetailsFromThreatClass(List threatClassList) throws Exception {
        System.out.println("getCapabilityDetailsFromThreatClass:"+threatClassList.toString());
        List list = new ArrayList();
        
        String parm = "";
        Iterator iter = threatClassList.iterator();
        while(iter.hasNext()) {
            String threatClassId = ((String)iter.next());
            /*
            if(parm.length() == 0) {
                parm = "'"+threatClassId+"'";
            } else {
                parm = ",'"+threatClassId+"'";
            }
            */

            // Execute Query
            String sql = "select b.id id from enterprise_rel a, enterprise_business_capability b where a.asset_id = ? and a.component_id=b.id and a.asset_rel_type=? and json_extract(asset_rel_context, '$.\"COMPONENT TYPE\"') = 'CAPABILITY'";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, threatClassId);
            pst.setString(2, "THREAT CLASS");
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                String id = rs.getString("id");
                System.out.println("Id:"+id);
                list.add(id);
			}
			rs.close();
			pst.close();
        }
        
        return list;
    }

	private void loadThreatClass() throws Exception {
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("AssetClass");
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		fields.put("csf", 1);
		fields.put("assetclass", 2);
		threatClassList = coll.find(allQuery,fields).toArray();
	}
	
	private void mongoConnect(String dbname) throws Exception {
		String username = "smartEuser";
		String pwd = "smarterD2018!";
		String hostname = "35.227.181.195";
		//String hostname = "35.227.158.31";
		String encoded_pwd = URLEncoder.encode(pwd, "UTF-8");
        MongoClientURI connectionString = new MongoClientURI("mongodb://"+username+":"+encoded_pwd+"@"+hostname+":27017/"+dbname+"?maxPoolSize=10");
        mongoClient = new MongoClient(connectionString);
        db = mongoClient.getDatabase(dbname);
        System.out.println("Successfully connected to Mongo DB:"+dbname);
    }
    
    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
		String hostname = "35.227.181.195";
		//String hostname = "35.197.106.183";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	public static void main(String[] args) throws Exception {
		//String filename = args[0];
		String companyCode = args[0];
		TenableAdapter loader = new TenableAdapter(companyCode);
		loader.exportVulnerabilities();
		//loader.process();
	}
}