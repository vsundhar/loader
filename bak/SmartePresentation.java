import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;
import java.awt.Rectangle;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFAutoShape;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;

import org.apache.poi.xslf.usermodel.XSLFConnectorShape;
import org.apache.poi.xslf.usermodel.XSLFGroupShape;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class SmartePresentation {
   private String companyCode = null;
   private XMLSlideShow ppt = null;
   private XSLFSlideMaster slideMaster = null;
   private XSLFSlideLayout layout = null;

   public SmartePresentation(String companyCode) throws Exception {
      this.companyCode = companyCode;
   }

   public void processPPT(String reportName) throws Exception {
      System.out.println("In processPPT=>"+reportName);
      File file = new File("security_management_report_template.pptx");
      FileInputStream inputstream = new FileInputStream(file);
      ppt = new XMLSlideShow(inputstream);
      System.out.println("Successfully read template presentation!!!");

      List slideList = ppt.getSlides();
      List slideDataList = getSlideData(reportName);
      if(slideDataList.size() > 0) {
         Iterator iter = slideDataList.iterator();
         while(iter.hasNext()) {
            BasicDBObject slideData = (BasicDBObject)iter.next();
            int slideNo = slideData.getInt("slideNo");
            System.out.println(slideNo);
            XSLFSlide slide = (XSLFSlide)slideList.get(slideNo);
            switch(slideNo) {
               case 1:
                  //processSlideMaster(slide);
                  processSlide1(slide, slideData);
                  break;
               case 2:
                  processSlide2(slide, slideData);
                  break;
               case 3:
                  processSlide3(slide, slideData);
                  break;
               case 4:
                  processSlide4(slide, slideData);
                  break;
               case 5:
                  processSlide5(slide, slideData);
                  break;
               default:
                  break;
            }
         }

         //create a file object
         File f = new File(reportName+".pptx");
         FileOutputStream out = new FileOutputStream(f);
         
         //save the changes in a file
         ppt.write(out);
         out.close();
         System.out.println(companyCode+" Presentation created successfully["+reportName+"]!!!");
      } else {
         System.out.println("Data not found["+reportName+"]!!!");
      }
   }

   private void processSlideMaster(XSLFSlide slide) throws Exception {
      layout = slide.getSlideLayout();
      for (XSLFShape shape : layout.getShapes()) {
         String name = shape.getShapeName();
         java.awt.geom.Rectangle2D anchor = shape.getAnchor();
         System.out.println(name+":"+shape.getClass().getName());
         if(name.equals("Company Logo")) {
            System.out.println(anchor.toString());
            XSLFPictureShape companyLogo = (XSLFPictureShape)shape;
            byte[] pictureData = IOUtils.toByteArray(new FileInputStream(companyCode.toLowerCase()+"_logo.png"));
            XSLFPictureData logoData = companyLogo.getPictureData();
            logoData.setData(pictureData);
            companyLogo.setAnchor(new Rectangle(895, 5, 60, 25));
         } else if(name.equals("Footer")) {
            XSLFAutoShape footer = (XSLFAutoShape)shape;
            XSLFTextRun r = footer.setText("Copyright 2019");
            r.setFontFamily("Calibri Light");
            r.setFontSize(10.);
         }
      }
   }

   private void processSlide0(XSLFSlide slide, BasicDBObject data) throws Exception {
      if(data.containsKey("slideDetail")) {
         BasicDBObject detail = (BasicDBObject)data.get("slideDetail");
         XSLFSlideLayout layout = slide.getSlideLayout();
         for (XSLFShape shape : slide.getShapes()) {
            String name = shape.getShapeName();
            java.awt.geom.Rectangle2D anchor = shape.getAnchor();
            System.out.println(name+":"+shape.getClass().getName());
            if(name.equals("Company Logo")) {
               XSLFPictureShape companyLogo = (XSLFPictureShape)shape;
               byte[] pictureData = IOUtils.toByteArray(new FileInputStream(companyCode.toLowerCase()+"_logo.png"));
               XSLFPictureData logoData = companyLogo.getPictureData();
               logoData.setData(pictureData);
            }
         }
      } else {
         System.out.println("Data Missing for Slide 1!!!");
      }
   }

   private void processSlide1(XSLFSlide slide, OrderedJSONObject data) throws Exception {
      if(data.containsKey("slideDetail")) {
         OrderedJSONObject detail = (OrderedJSONObject)data.get("slideDetail");
         String roadmapTxt = detail.getString("Roadmap");
         String programTxt = detail.getString("Program");
         String budgetTxt = detail.getString("Budget");
         String controlsTxt = detail.getString("Controls");
         String threatsTxt = detail.getString("Threats");
         String assetsTxt = detail.getString("Asset");
         XSLFSlideLayout layout = slide.getSlideLayout();
         for (XSLFShape shape : slide.getShapes()) {
            String name = shape.getShapeName();
            java.awt.geom.Rectangle2D anchor = shape.getAnchor();
            System.out.println(name+":"+shape.getClass().getName());
            if(name.equals("Roadmap")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(roadmapTxt);
                  }
               }
            } else if(name.equals("Program")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(programTxt);
                  }
               }
            } else if(name.equals("Budget")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(budgetTxt);
                  }
               }
            } else if(name.equals("Controls")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(controlsTxt);
                  }
               }
            } else if(name.equals("Threats")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(threatsTxt);
                  }
               }
            } else if(name.equals("Assets")) {
               XSLFGroupShape roadmap = (XSLFGroupShape)shape;
               for (XSLFShape groupshape : roadmap.getShapes()) {
                  String groupshapeName = groupshape.getShapeName();
                  if(groupshapeName.equals("Detail")) {
                     XSLFAutoShape detailShape = (XSLFAutoShape)groupshape;
                     detailShape.setText(assetsTxt);
                  }
               }
            }
         }
      } else {
         System.out.println("Data Missing for Slide 1!!!");
      }
   }

   private void processSlide2(XSLFSlide slide, OrderedJSONObject data) throws Exception {
      XSLFSlideLayout layout = slide.getSlideLayout();
      if(data.containsKey("slideDetail")) {
         for (XSLFShape shape : slide.getShapes()) {
            String name = shape.getShapeName();
            java.awt.geom.Rectangle2D anchor = shape.getAnchor();
            System.out.println(name+":"+shape.getClass().getName());
            if(name.equals("Highlights")) {
               XSLFTextBox highlight = (XSLFTextBox)shape;  
               highlight.clearText();
               OrderedJSONObject detail = (OrderedJSONObject)data.get("slideDetail");
               Iterator iter = detail.keySet().iterator();
               while(iter.hasNext()) {
                  String key = (String)iter.next();
                  String value = (String)detail.get(key);
                  XSLFTextParagraph p1 = highlight.addNewTextParagraph();
                  if(value.indexOf(":") != -1) {
                     p1.setIndentLevel(0);
                     p1.setBullet(true);
                     XSLFTextRun r = p1.addNewTextRun();
                     r.setText("  "+key);
                     r.setFontFamily("Calibri Light");
                     r.setFontSize(28.);
                     String[] subPoints = value.split(":");
                     for(int i=0; i<subPoints.length; i++) {
                        String str = subPoints[i];
                        p1.setIndentLevel(2);
                        XSLFTextRun r1 = p1.addNewTextRun();
                        r1.setText("  "+str);
                        r1.setFontFamily("Calibri Light");
                        r1.setFontSize(20.);
                     }
                  } else {
                     p1.setIndentLevel(0);
                     p1.setBullet(true);
                     XSLFTextRun r1 = p1.addNewTextRun();
                     r1.setText("  "+value);
                     r1.setFontFamily("Calibri Light");
                     r1.setFontSize(28.);
                  }
                  
               }
            }
         }
      } else {
         System.out.println("Data Missing for Slide 2!!!");
      }
   }

   private void processSlide3(XSLFSlide slide, OrderedJSONObject data) throws Exception {
      XSLFSlideLayout layout = slide.getSlideLayout();
      if(data.containsKey("slideDetail")) {
         /*
         for (XSLFShape shape : slide.getShapes()) {
            String name = shape.getShapeName();
            java.awt.geom.Rectangle2D anchor = shape.getAnchor();
            System.out.println(name+":"+shape.getClass().getName());
            if(name.equals("Control Status")) {
               XSLFPictureShape controlStatus = (XSLFPictureShape)shape;  
               BasicDBObject detail = (BasicDBObject)data.get("slideDetail");
               Iterator iter = detail.keySet().iterator();
               while(iter.hasNext()) {
                  String key = (String)iter.next();
                  String value = (String)detail.get(key);
                  System.out.println(value);
                  GridFS gridFS = new GridFS(db);
                  GridFSDBFile imageForOutput = gridFS.findOne(value);
                  byte[] pictureData = IOUtils.toByteArray(imageForOutput.getInputStream());
                  XSLFPictureData pd = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
                  XSLFPictureShape pic = slide.createPicture(pd);
                  //controlStatus.setSvgImage(pd);
			         //imageForOutput.writeTo(value);
               }
            }
         }
         */
         OrderedJSONObject detail = (OrderedJSONObject)data.get("slideDetail");
         Iterator iter = detail.keySet().iterator();
         while(iter.hasNext()) {
            String key = (String)iter.next();
            String value = (String)detail.get(key);
            System.out.println(value);
            /*
            GridFS gridFS = new GridFS(db);
            GridFSDBFile imageForOutput = gridFS.findOne(value);
            byte[] pictureData = IOUtils.toByteArray(imageForOutput.getInputStream());
            XSLFPictureData pd = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
            XSLFPictureShape status = slide.createPicture(pd);
            status.setAnchor(new Rectangle(50, 75, 850, 175));
            */
         }
      } else {
         System.out.println("Data Missing for Slide 3!!!");
      }
   }

   private void processSlide4(XSLFSlide slide, OrderedJSONObject data) throws Exception {
      /*
      XSLFSlideLayout layout = slide.getSlideLayout();
      if(data.containsKey("slideDetail")) {
         BasicDBObject detail = (BasicDBObject)data.get("slideDetail");
         Iterator iter = detail.keySet().iterator();
         while(iter.hasNext()) {
            String key = (String)iter.next();
            String value = (String)detail.get(key);
            System.out.println(value);
            GridFS gridFS = new GridFS(db);
            GridFSDBFile imageForOutput = gridFS.findOne(value);
            byte[] pictureData = IOUtils.toByteArray(imageForOutput.getInputStream());
            XSLFPictureData pd = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
            XSLFPictureShape status = slide.createPicture(pd);
            status.setAnchor(new Rectangle(50, 75, 850, 400));
         }
      } else {
         System.out.println("Data Missing for Slide 3!!!");
      }
      */
   }

   private void processSlide5(XSLFSlide slide, OrderedJSONObject data) throws Exception {
      /*
      XSLFSlideLayout layout = slide.getSlideLayout();
      if(data.containsKey("slideDetail")) {
         BasicDBObject detail = (BasicDBObject)data.get("slideDetail");
         Iterator iter = detail.keySet().iterator();
         while(iter.hasNext()) {
            String key = (String)iter.next();
            String value = (String)detail.get(key);
            System.out.println(value);
            GridFS gridFS = new GridFS(db);
            GridFSDBFile imageForOutput = gridFS.findOne(value);
            byte[] pictureData = IOUtils.toByteArray(imageForOutput.getInputStream());
            XSLFPictureData pd = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
            XSLFPictureShape status = slide.createPicture(pd);
            status.setAnchor(new Rectangle(50, 75, 850, 400));
         }
      } else {
         System.out.println("Data Missing for Slide 3!!!");
      }
      */
   }

   public void loadData() throws Exception {

      // Retrieve Image from MongoDB
      //List<GridFSDBFile> imageForOutput = gridFS.find("tiger.jpg");
      //imageForOutput.get(0).writeTo("E:/tiperImageDOWNLOADEDfromMongoDB.jpg");
   }
   
   public static void main(String args[]) throws Exception {
      String reportName = "DEMO_Security_Management_Report_09182019";
      String companyCode = args[0];
      
      SmartePresentation ppt = new SmartePresentation(companyCode);
      //ppt.loadData();
      ppt.processPPT(reportName);
   }
}