import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;
import java.awt.Rectangle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class SmarteXSLX {
   private String companyCode = null;
   private com.mongodb.MongoClient mongoClient = null;
   private DB db = null;	
   
   public SmarteXSLX(String companyCode) throws Exception {
      this.companyCode = companyCode;
   }

   public void generateXSLX(String reportName) throws Exception {
      System.out.println("In processPPT=>"+reportName);
      //File file = new File("security_management_report_template.pptx");
      //FileInputStream inputstream = new FileInputStream(file);
      //System.out.println("Successfully read template XL!!!");
      String[] columns = { "First Name", "Last Name", "Email", "Date Of Birth" };

      Workbook workbook = new XSSFWorkbook();
      Sheet sheet = workbook.createSheet("Projects");

      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerFont.setFontHeightInPoints((short) 14);
      headerFont.setColor(IndexedColors.RED.getIndex());

      CellStyle headerCellStyle = workbook.createCellStyle();
      headerCellStyle.setFont(headerFont);

      // Create a Row
      Row headerRow = sheet.createRow(0);

      for (int i = 0; i < columns.length; i++) {
         Cell cell = headerRow.createCell(i);
         cell.setCellValue(columns[i]);
         cell.setCellStyle(headerCellStyle);
      }

      int rowNum = 1;

      Row row = sheet.createRow(rowNum++);
      row.createCell(0).setCellValue("Vijay");
      row.createCell(1).setCellValue("Sundhar");
      row.createCell(2).setCellValue("vsundhar@smarterd.com");
      row.createCell(3).setCellValue("10/28/1968");

      Row row1 = sheet.createRow(rowNum++);
      Cell c1 = row1.createCell(0);
      c1.setCellValue("Rahul");
      CellStyle style = c1.getCellStyle();
      short indent = 16;
      style.setIndention(indent);
      c1.setCellStyle(style);
      row1.createCell(1).setCellValue("Sundhar");
      row1.createCell(2).setCellValue("rsundhar@smarterd.com");
      row1.createCell(3).setCellValue("21/28/2002");

      for (int i = 0; i < columns.length; i++) {
         sheet.autoSizeColumn(i);
      }

      //create a file object
      File f = new File(reportName+".xslx");
      FileOutputStream out = new FileOutputStream(f);
      workbook.write(out);
      out.close();
      System.out.println(companyCode+" Presentation created successfully["+reportName+"]!!!");
   }
   
   public static void main(String args[]) throws Exception {
      String reportName = "DEMO_Security_Management_Report_09182019";
      String companyCode = args[0];
      
      SmarteXSLX xslx = new SmarteXSLX(companyCode);
      xslx.generateXSLX(reportName);
   }
}