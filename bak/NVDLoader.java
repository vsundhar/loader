import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class NVDLoader {
	private OrderedJSONObject reader = null;
	private String companyCode = null;
	private String hostname = "localhost";
    private Connection conn = null;
	private Map groupList = new HashMap();
	private Map groupConfig = new HashMap();
	
	public NVDLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new OrderedJSONObject(new FileReader(new File(filename)));
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
	}
	
	public void processNVDData() throws Exception {
		String updatedTimestamp = (String)reader.get("CVE_data_timestamp");  
	
		Iterator iter = reader.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			if(key.equalsIgnoreCase("CVE_Items")) {
				JSONArray value = (JSONArray)reader.get(key);
				Iterator itemIter = value.iterator();
				while(itemIter.hasNext()) {
					String threatDesc = null, publishedDate = null, lastModifiedDate=null, cveId=null;
					BasicBSONList refObj = new BasicBSONList();
					BasicDBObject impactObj = new BasicDBObject();
					BasicDBObject vendorObj = new BasicDBObject();

					//Create new Document
					BasicDBObject nvdData = new BasicDBObject();
					ObjectId id = ObjectId.get();
					nvdData.put("_id", id);

					OrderedJSONObject item = (OrderedJSONObject)itemIter.next();
					OrderedJSONObject oValue = (OrderedJSONObject)item.get("cve");
					Iterator oValueIter = oValue.keySet().iterator();
					while(oValueIter.hasNext()) {
						String oValueKey = (String)oValueIter.next();
						if(oValueKey.equalsIgnoreCase("CVE_data_meta")) {
							OrderedJSONObject meta = (OrderedJSONObject)oValue.get(oValueKey);
							cveId = (String)meta.get("ID");
						} else if(oValueKey.equalsIgnoreCase("description")) {
							OrderedJSONObject desc = (OrderedJSONObject)oValue.get(oValueKey);
							Iterator descIter = desc.keySet().iterator();
							while(descIter.hasNext()) {
								String descKey = (String)descIter.next();
								JSONArray descValue = (JSONArray)desc.get(descKey);
								Iterator descValueIter = descValue.iterator();
								while(descValueIter.hasNext()) {
									OrderedJSONObject descObj = (OrderedJSONObject)descValueIter.next();
									Iterator descObjIter = descObj.keySet().iterator();
									while(descObjIter.hasNext()) {
										String descObjKey = (String)descObjIter.next();
										threatDesc = ((String)descObj.get(descObjKey)).trim();
									}	
								}
							}
						} else if(oValueKey.equalsIgnoreCase("references")) {
							OrderedJSONObject refList = (OrderedJSONObject)oValue.get(oValueKey);
							JSONArray refArr = (JSONArray)refList.get("reference_data");
							Iterator refArrIter = refArr.iterator();
							while(refArrIter.hasNext()) {
								String name=null,url=null;
								BasicDBObject ref = new BasicDBObject();
								OrderedJSONObject refArrItem = (OrderedJSONObject)refArrIter.next();
								name = (String)refArrItem.get("name");
								url = (String)refArrItem.get("url");
								ref.put("name", name);
								ref.put("url", url);
								refObj.add(ref);
							}
						} else if(oValueKey.equalsIgnoreCase("affects")) {
							OrderedJSONObject affects = (OrderedJSONObject)oValue.get(oValueKey);
							OrderedJSONObject vendor = (OrderedJSONObject)affects.get("vendor");
							JSONArray vendorArr = (JSONArray)vendor.get("vendor_data");
							Iterator vendorArrIter = vendorArr.iterator();
							while(vendorArrIter.hasNext()) {
								OrderedJSONObject vendorData = (OrderedJSONObject)vendorArrIter.next();
								String vendorName = (String)vendorData.get("vendor_name");
								vendorName = vendorName.replace(".", "-");
								JSONArray productList = null;
								if(vendorObj.containsKey("vendorName")) {
									productList = (JSONArray)vendorObj.get(vendorName);
								} else {
									productList = new JSONArray();
								}
								OrderedJSONObject product = (OrderedJSONObject)vendorData.get("product");
								JSONArray productArr = (JSONArray)product.get("product_data");
								Iterator productArrIter = productArr.iterator();
								while(productArrIter.hasNext()) {
									OrderedJSONObject productItem = (OrderedJSONObject)productArrIter.next();
									String productName = (String)productItem.get("product_name");
									productList.add(productName);
								} 
								vendorObj.put(vendorName, productList);
							}
						}
					}

					OrderedJSONObject impact = (OrderedJSONObject)item.get("impact");
					if(impact.has("baseMetricV2")){
						OrderedJSONObject baseMetricV2 = (OrderedJSONObject)impact.get("baseMetricV2");
						String severity = (String)baseMetricV2.get("severity");
						String impactScore = ((Double)baseMetricV2.get("impactScore")).toString();
						String exploitabilityScore = ((Double)baseMetricV2.get("exploitabilityScore")).toString();
						impactObj.put("severity", severity);
						impactObj.put("impactScore", impactScore);
						impactObj.put("exploitabilityScore", exploitabilityScore);
					}

					publishedDate = (String)item.get("publishedDate");
					lastModifiedDate = (String)item.get("lastModifiedDate");

					// Add to document
					nvdData.put("desc", threatDesc);
					nvdData.put("reference", refObj);
					nvdData.put("impact", impactObj);
					nvdData.put("vendor", vendorObj);
					nvdData.put("key", threatDesc.hashCode());
					nvdData.put("updatedDate", updatedTimestamp);
					nvdData.put("publishedDate", publishedDate);
					nvdData.put("lastModifiedDate", lastModifiedDate);

					// Insert Document
					insertDocument(threatDesc, nvdData);
					System.out.println("Processed:"+threatDesc);
					/*
					System.out.println("Desc:"+threatDesc);
					System.out.println("Ref:"+refObj.toString());
					System.out.println("Impact:"+impactObj.toString());
					System.out.println("Vendor:"+vendorObj.toString());
					*/
				}
			} else if(key.equalsIgnoreCase("CVE_data_numberOfCVEs")) {
				String value = (String)reader.get(key);
				System.out.println("Size:"+value);
			}
		}
	}

	private void insertDocument(String desc, BasicDBObject doc) throws Exception {
		// check if Threat exists
		BasicDBObject criteria = new BasicDBObject();
		criteria.put("key", desc.hashCode());
		DBCollection coll = mongoClient.getDB(companyCode).getCollection("NVD");
		int count = coll.find(criteria).count();

		if(count == 0) {
			coll.insert(doc);
		} else {
			System.out.println("Document already found. Skipping!!!");
		}
	}
    
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		NVDLoader loader = new NVDLoader(filename, companyCode, env);
		//loader.processNVDData();
	}
}