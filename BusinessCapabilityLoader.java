import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class BusinessCapabilityLoader {
	private String domain = "UCM";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public BusinessCapabilityLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Check if Root capability exists
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] line;
		int rowCount=0;
		String parentCapability = null, childCapability = null, owner = null;
		String internalId = null, level = null, desc = "", category = "0";
		String id = null, childId = null, parentId = null, status = "Y", ext=null, factor = null, assetFactor=null, planFactor=null, nextFiscalYear = "", order="0";
		String currentMaturity = "", targetMaturity = "", baselineMaturity = "", criticality = "", securityFunction = "";
		String targetDate = "", isoRef = "", socRef = "", evidence="", population="", ac="", eco="";

		while ((line = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 2) {
				System.out.println("Processing row-" + rowCount + "...");
				
				for(int i=0; i<line.length; i++) {
					if (i==0) {
						internalId = line[i];
					} else if (i==1) {
						parentCapability = line[i].trim();
					} else if (i==2) {
						childCapability = line[i].trim();
					} else if (i==3) {
						desc = line[i];
					} else if (i==4) {
						owner = line[i];
					} else if (i==5) {
						currentMaturity = line[i];
					} else if (i==6) {
						targetMaturity = line[i];
					} else if (i==7) {
						baselineMaturity = line[i];
					} else if (i==8) {
						targetDate = line[i];
					} else if (i==9) {
						criticality = line[i];
					} else if (i==10) {
						securityFunction = line[i];
					} else if (i==11) {
						ac = line[i];
					} else if (i==12) {
						eco = line[i];
					} else if (1==13) {
						isoRef = line[i];
					} else if (i==14) {
						socRef = line[i];
					} else if (i==15) {
						evidence = line[i];
					} else if (i==16) {
						population = line[i];
					}
				}
					
				System.out.println(internalId+":"+parentCapability+":"+childCapability+":"+desc+":"+socRef+":"+isoRef);

				if(parentCapability == null || parentCapability.length() == 0) {
					parentCapability = internalId;
				}
				if(childCapability == null || childCapability.length() == 0) {
					if(desc.length() > 70) {
						String str = desc.substring(0, 70).trim();
						int index = str.lastIndexOf(" ");
						str = str.substring(0, index);
						childCapability = str+"...";
					} else {
						childCapability = desc;
					}
				}

				if(currentMaturity.equalsIgnoreCase("Initial")) {
					currentMaturity = "1";
				} else if(currentMaturity.equalsIgnoreCase("Repeatable")) {
					currentMaturity = "2";
				} else if(currentMaturity.equalsIgnoreCase("3")) {
					currentMaturity = "Defined";
				} else if(currentMaturity.equalsIgnoreCase("Managed")) {
					currentMaturity = "4";
				} else if(currentMaturity.equalsIgnoreCase("Matured")) {
					currentMaturity = "5";
				}
				if(targetMaturity.equalsIgnoreCase("Initial")) {
					targetMaturity = "1";
				} else if(targetMaturity.equalsIgnoreCase("Repeatable")) {
					targetMaturity = "2";
				} else if(targetMaturity.equalsIgnoreCase("3")) {
					targetMaturity = "Defined";
				} else if(targetMaturity.equalsIgnoreCase("Managed")) {
					targetMaturity = "4";
				} else if(targetMaturity.equalsIgnoreCase("Matured")) {
					targetMaturity = "5";
				}
				if(baselineMaturity.equalsIgnoreCase("Initial")) {
					baselineMaturity = "1";
				} else if(baselineMaturity.equalsIgnoreCase("Repeatable")) {
					baselineMaturity = "2";
				} else if(baselineMaturity.equalsIgnoreCase("3")) {
					baselineMaturity = "Defined";
				} else if(baselineMaturity.equalsIgnoreCase("Managed")) {
					baselineMaturity = "4";
				} else if(baselineMaturity.equalsIgnoreCase("Matured")) {
					baselineMaturity = "5";
				}
				if(criticality.equalsIgnoreCase("Low")) {
					criticality = "0";
				} else if(criticality.equalsIgnoreCase("Medium")) {
					criticality = "1";
				} else if(criticality.equalsIgnoreCase("High")) {
					criticality = "2";
				} else if(criticality.equalsIgnoreCase("Critical")) {
					criticality = "3";
				} 

				JSONArray sf = new JSONArray();
				sf.add(securityFunction);

				JSONArray acArr = null;
				if(ac != null && ac.length() > 0) {
					String[] strArr = ac.split("\n");
					List acList = Arrays.asList(strArr);
					acArr = new JSONArray(acList);
				} else {
					acArr = new JSONArray();
				}

				JSONArray ecoArr = null;
				if(eco != null && eco.length() > 0) {
					String[] strArr = ac.split("\n");
					List ecoList = Arrays.asList(strArr);
					ecoArr = new JSONArray(ecoList);
				} else {
					ecoArr = new JSONArray();
				}

				JSONArray isoArr = null;
				if(isoRef != null && isoRef.length() > 0 && !isoRef.equalsIgnoreCase("N/A")) {
					String[] isoRefArr = isoRef.split("\n");
					List isoRefList = Arrays.asList(isoRefArr);
					isoArr = new JSONArray(isoRefList);
				} else {
					isoArr = new JSONArray();
				}

				JSONArray socArr = null;
				if(socRef != null && socRef.length() > 0 && !socRef.equalsIgnoreCase("N/A")) {
					String[] socRefArr = socRef.split("\n");
					List socRefList = Arrays.asList(socRefArr);
					socArr = new JSONArray(socRefList);
				} else {
					socArr = new JSONArray();
				}
				OrderedJSONObject cObj = new OrderedJSONObject();
				cObj.put("ISO 27001", isoArr);
				cObj.put("SOC 2", socArr);
				
				// Check Capability
				String capabilityId = null;
				if(internalId != null && internalId.length() > 0) {
					OrderedJSONObject res = checkCapability(internalId);
					if(res.size() > 0) {
						capabilityId = (String)res.get("id");
						factor = (String)res.get("factor");
						ext = (String)res.get("ext");
					}
				} else {
					OrderedJSONObject res = checkCapability(parentCapability, childCapability);
					if(res.size() > 0) {
						capabilityId = (String)res.get("id");
						factor = (String)res.get("factor");
						ext = (String)res.get("ext");
					}
				}
				if(capabilityId == null || capabilityId.length() == 0) {	
					System.out.println("Inserting Control:"+parentId+":"+parentCapability+":"+childId+":"+childCapability);
					OrderedJSONObject extObject = new OrderedJSONObject();
					OrderedJSONObject factorObj = new OrderedJSONObject();
					
					if(capabilityList.has(parentCapability)) {
						extObject.put("family", parentCapability);
						extObject.put("securityFunction", sf);
						extObject.put("compliance", cObj);
						extObject.put("evidence", evidence);
						extObject.put("population", population);
						extObject.put("criticality", criticality);
						extObject.put("assetClass", acArr);
						extObject.put("ecosystem", ecoArr);
			
						factorObj.put("currentMaturity", currentMaturity);
						factorObj.put("targetMaturity", targetMaturity);
						factorObj.put("baselineMaturity", baselineMaturity);
						factorObj.put("targetDate", targetDate);
						factorObj.put("current_risk", "NA");
						factorObj.put("current_risk_value", "0");
						parentId = (String)capabilityList.get(parentCapability);
						childId = generateUUID();
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, extObject.toString(), factorObj.toString(), owner, "2", levelTwo);
						levelTwo++;
					} else {
						levelTwo = 0;
						parentId = generateUUID();
						childId = generateUUID();
						extObject.put("securityFunction", new JSONArray());
						extObject.put("compliance", new OrderedJSONObject());
						extObject.put("evidence", "");
						extObject.put("population", "");
						extObject.put("criticality", "");
						extObject.put("assetClass", new JSONArray());
						extObject.put("ecosystem", new JSONArray());

						factorObj.put("currentMaturity", "");
						factorObj.put("targetMaturity", "");
						factorObj.put("baselineMaturity", "");
						factorObj.put("targetDate", "");
						factorObj.put("current_risk", "NA");
						factorObj.put("current_risk_value", "0");
						insertCapability(parentId, rootId, internalId, rootId, parentCapability, "", extObject.toString(), factorObj.toString(), owner, "1", levelOne);
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, extObject.toString(), factorObj.toString(), owner, "2", levelTwo);
						capabilityList.put(parentCapability, parentId);
						
						levelOne++;
						levelTwo++;
					}	
				} else {
					System.out.println("Updating Control:"+parentId+":"+parentCapability+":"+childId+":"+childCapability);
					OrderedJSONObject factorObj = new OrderedJSONObject(factor);
					OrderedJSONObject extObject = new OrderedJSONObject(ext);
					extObject.put("family", parentCapability);
					extObject.put("securityFunction", sf);
					extObject.put("compliance", cObj);
					extObject.put("evidence", evidence);
					extObject.put("population", population);
					extObject.put("criticality", criticality);
					extObject.put("assetClass", acArr);
					extObject.put("ecosystem", ecoArr);
		
					factorObj.put("currentMaturity", currentMaturity);
					factorObj.put("targetMaturity", targetMaturity);
					factorObj.put("baselineMaturity", baselineMaturity);
					factorObj.put("targetDate", targetDate);

					updateCapability(capabilityId, internalId, parentCapability, childCapability, desc, extObject.toString(), factorObj.toString(), owner);
					capabilityList.put(parentCapability, parentId);
				}

				id = null;
				childId = null;
				parentId = null;
				internalId = null;
				parentCapability = "";
				childCapability = "";
				owner = "";
				desc = "";
				status = "Y";
				order = "0";
				level = null;
				category = "0";
				factor = null;
				assetFactor = null;
				planFactor = null;
				ext = null;
				currentMaturity="";
				targetMaturity="";
				baselineMaturity="";
				targetDate="";
				criticality="";
				securityFunction="";
				evidence="";
				isoRef="";
				socRef="";
				population="";
				ac="";
				eco="";
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		OrderedJSONObject res = checkCapability("UCM", "UCM");
		if(res.size() == 0) {
            OrderedJSONObject extObject = new OrderedJSONObject();
            String e = extObject.toString();
            OrderedJSONObject factorObject = new OrderedJSONObject();
            String f = factorObject.toString();

            rootId = "UCM";
            insertCapability(rootId, rootId, "", "UCM", "UCM", "UCM Controls", e, f, "", "0", 0);
            capabilityList.put(rootId, rootId);
        }
		System.out.println("Root Id:"+rootId);
	}
	
	private OrderedJSONObject checkCapability(String internalId) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		String capabilityCheckSQL = "select id,business_capability_factor,business_capability_ext from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
            String id = rs.getString("id");
            String factor = rs.getString("business_capability_ext");
            String ext = rs.getString("business_capability_factor");
            res.put("id", id);
            res.put("factor", factor);
            res.put("ext", ext);
		}
        rs.close();
        pst.close();
		return res;
	}

	private OrderedJSONObject checkCapability(String parent, String child) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		String capabilityCheckSQL = "select id,business_capability_factor,business_capability_ext from enterprise_business_capability where parent_capability_name = ? and child_capability_name=?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
            String factor = rs.getString("business_capability_ext");
            String ext = rs.getString("business_capability_factor");
            res.put("id", id);
            res.put("factor", factor);
            res.put("ext", ext);
        }
        rs.close();
        pst.close();
		
		return res;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String ext, String factor, String owner, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String status = "Y";
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();
   		
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
    }
    
    private void updateCapability(String id, String internalId, String parent, String child, String desc, 
								  String ext, String factor, String owner) throws Exception {
		String capabilityUpdateSQL = "update enterprise_business_capability set internal_id=?,parent_capability_name=?,child_capability_name=?,business_capability_desc=?,owner=?,business_capability_factor=?,business_capability_ext=?,updated_timestamp=now() where id=?"; 
   		
		PreparedStatement pst = conn.prepareStatement(capabilityUpdateSQL);
		
		pst.setString(1, internalId);
		pst.setString(2, parent);
		pst.setString(3, child);
		pst.setString(4, desc);
		pst.setString(5, owner);
		pst.setString(6, factor);
        pst.setString(7, ext);
        pst.setString(8, id);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		BusinessCapabilityLoader loader = new BusinessCapabilityLoader(filename, companyCode, env);
		loader.process();
	}
}