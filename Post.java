import au.com.bytecode.opencsv.CSVReader;
import java.io.*;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class Post {
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public Post(String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
	}
	
	public void invoke() throws Exception {
		OrderedJSONObject parm = new OrderedJSONObject();
		parm.put("1", "SE");
		String data = parm.toString();
		String action = "scheduleAlert";
		String command = "enterprise_alert";

		String urlStr = "https://preprod.smarterd.com:8080/smarterd/api/smarte";
		String query = "COMMAND="+command+"&ACTION="+action+"&DATA="+data;
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/text");

		PrintStream ps = new PrintStream(conn.getOutputStream());
        ps.print(query);
        ps.close();

		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String l = null;
        while ((l=br.readLine())!=null) {
            System.out.println(l);
        }
        br.close();

		conn.disconnect();
	}
		
	public static void main(String[] args) throws Exception {
		String companyCode = args[0];
		String env = args[1];
		Post post = new Post(companyCode, env);
		post.invoke();
	}
}