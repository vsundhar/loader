import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class UCMRequirementLoader {
	private String domain = "UCM";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
	OrderedJSONObject controlNFList = new OrderedJSONObject();
   	
	public UCMRequirementLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        //checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		Map rowCountList = new HashMap();
		int rowCount = 0;
		String controlsql = "select id,child_capability_name from enterprise_business_capability where internal_id = ? and business_capability_category = 3";
		String reqsql = "select id from enterprise_business_capability where child_capability_name = ? and business_capability_category = 3";

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String rInternalId=null, pInternalId=null, cInternalId=null, cInternalIdArr = null, aInternalId=null;
				String rName=null, cName=null, aName=null, aDesc=null;
				String owner="", department="", status=null, targetDate="", maturity="";
				String assetClass="", priority="0";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						rName = row[i].trim();
					} else if(i==1) {
						cInternalIdArr = row[i].trim();
					} else if(i==2) {
						aName = row[i].trim();
					} else if(i==3) {
						department = row[i].trim();
					} else if(i==4) {
						maturity = row[i].trim();
						if(maturity.equalsIgnoreCase("TBD")) {
							maturity = "0.0";
						}
					} else if(i==5) {
						status = row[i].trim();
						if(status.equalsIgnoreCase("TBD")) {
							status = "";
						}
					} else if(i==6) {
						aDesc = row[i].trim();
					}
				}

				if(maturity.equals("5")) {
					maturity = "5.0";
				} else if(maturity.equals("4")) {
					maturity = "4.0";
				} else if(maturity.equals("3")) {
					maturity = "3.0";
				} else if(maturity.equals("2")) {
					maturity = "2.0";
				} else if(maturity.equals("1")) {
					maturity = "1.0";
				} else {
					maturity = "0.0";
				}
				
				JSONArray sf = new JSONArray();

				OrderedJSONObject cObj = new OrderedJSONObject();
				OrderedJSONObject extObject = new OrderedJSONObject();
				extObject.put("family", rName);
				extObject.put("securityFunction", sf);
				extObject.put("compliance", cObj);
				extObject.put("evidence", "");
				extObject.put("population", "");
				
				System.out.println(rName+":"+cInternalId+":"+aName);

				// Get Family
				String parentsql = "select id,internal_id from enterprise_business_capability where child_capability_name = ? and business_capability_category = 3";
				String rId = null;
				PreparedStatement pst = conn.prepareStatement(parentsql);
				pst.setString(1, rName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					rId = rs.getString("id");
					rInternalId = rs.getString("internal_id");
				}
				rs.close();
				pst.close();

				// Get Control Id
				String cId = null;
				if(cInternalIdArr.equalsIgnoreCase("TBD")) {
					cName = "TBD";
					cInternalId = rInternalId+".TBD";
					if(capabilityList.has(cInternalId)) {
						cId = (String)capabilityList.get(cInternalId);
					} else { 
						cId = generateUUID();
						insertCapability(cId, rId, cInternalId, rName, cName, "Requirements not linked to Control Activity", extObject.toString(), "", "", "2", 0);
						rowCountList.put(cInternalId, new Integer(0));
						capabilityList.put(cInternalId, cId);
					}
				}
					
				String[] controlArr = cInternalIdArr.split("\n");
				for(int i=0; i<controlArr.length; i++) {
					if(cId == null) {
						cInternalId = controlArr[i];
						pst = conn.prepareStatement(controlsql);
						pst.setString(1, cInternalId);
						rs = pst.executeQuery();
						while(rs.next()) {
							cId = rs.getString("id");
							cName = rs.getString("child_capability_name");
						}
						rs.close();
						pst.close();
					}
					
					if(cId != null) {
						String aId = null;
						pst = conn.prepareStatement(reqsql);
						pst.setString(1, aName);
						rs = pst.executeQuery();
						while(rs.next()) {
							aId = rs.getString("id");
						}
						rs.close();
						pst.close();

						if(aId == null) {	
							aId = generateUUID();
							int aCount = 0;
							if(rowCountList.containsKey(cInternalId)) {
								aCount = ((Integer)rowCountList.get(cInternalId)).intValue();
							}
							aCount++;
							aInternalId = cInternalId +"."+String.valueOf(aCount);
							OrderedJSONObject customField = new OrderedJSONObject();
							customField.put("Target Date", "");
							customField.put("State", "");
							extObject.put("custom", customField);
							extObject.put("control_status", "");
							extObject.put("department", department);
							String ext = extObject.toString();
							insertCapability(aId, cId, aInternalId, cName, aName, aDesc, ext, maturity, owner, "3", 0);
							capabilityList.put(cInternalId+":"+aName, aId);
							rowCountList.put(cInternalId, new Integer(aCount));

							createComplianceRelationship(cId, aId);
						} else {
							System.out.println("Requirement found...Rolling up Maturity");
							rollupMaturity(aId, cId);
						}
					} else {
						System.out.println("Cannot find Control:"+cInternalId);
						controlNFList.put(cInternalId, "Control Not Found");
					}
				}
			}
		}
		
		conn.commit();
		conn.close();

		System.out.println(controlNFList.toString());
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability("UCM");
		
		if(rootId == null || rootId.length() == 0) {
			OrderedJSONObject extObject = new OrderedJSONObject();
			String ext = extObject.toString();

			//rootId = generateUUID();
			rootId = "UCM";
			insertCapability(rootId, rootId, "", "UCM", "UCM", "UCM Controls", ext, "", "", "0", 0);
		}
		capabilityList.put(rootId, rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}

	private String checkCapability(String parent, String child, String level) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name=? and business_capability_level = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		pst.setString(3, level);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String ext, String maturity, String owner, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String status = "Y";
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("currentMaturity", maturity);
		factorObject.put("targetMaturity", "0");
		factorObject.put("baselineMaturity", "");
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();
   		
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void createComplianceRelationship(String cId, String aId) throws Exception {
		System.out.println("createComplianceRelationship:"+cId+":"+aId);
		String checkrelsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
        String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		String compsql = "select asset_id,replace(json_extract(asset_rel_context, '$.\"COMPLIANCE\"'),'\"','') compliance from enterprise_rel where component_id = ? and asset_rel_type = 'COMPLIANCE'";
		PreparedStatement pst = conn.prepareStatement(insertrelsql);
		String updatedBy = "support@smarterd.com";
		OrderedJSONObject ctx = new OrderedJSONObject();
		ctx.put("LINKED", "Y");

		// Get CompId 
		PreparedStatement pst1 = conn.prepareStatement(compsql);
		pst1.setString(1, cId);
		ResultSet rs1 = pst1.executeQuery();
		while(rs1.next()) {
			String compId = rs1.getString("asset_id");
			String compName = rs1.getString("compliance");

			int count = 0;
			PreparedStatement pst2 = conn.prepareStatement(checkrelsql);
			pst2.setString(1, aId);
			pst2.setString(2, compId);
			ResultSet rs2 = pst2.executeQuery();
			while(rs2.next()) {
				count = rs2.getInt("count");
			}
			rs2.close();
			pst2.close();

			if(count == 0) {
				if(aId != null) {
					ctx.put("COMPONENT TYPE", "CAPABILITY");
					ctx.put("COMPLIANCE", compName);
					pst.setString(1, compId);
					pst.setString(2, aId);
					pst.setString(3, "COMPLIANCE");
					pst.setString(4, ctx.toString());
					pst.setString(5, updatedBy);
					pst.setString(6, updatedBy);
					pst.setString(7, companyCode);
					pst.addBatch();
					ctx.put("COMPONENT TYPE", "COMPLIANCE");
					pst.setString(1, aId);
					pst.setString(2, compId);
					pst.setString(3, "CAPABILITY");
					pst.setString(4, ctx.toString());
					pst.setString(5, updatedBy);
					pst.setString(6, updatedBy);
					pst.setString(7, companyCode);
					pst.addBatch();
				}
			}
		}
		rs1.close();
		pst1.close();

		pst.executeBatch();
		pst.close();
	}

	private void rollupMaturity(String id, String parentId) throws Exception {
		String parentsql = "select replace(json_extract(business_capability_factor, '$.\"currentMaturity\"'), '\"', '') maturity from enterprise_business_capability where parent_capability_id = ?";
		String updatesql = "update enterprise_business_capability set business_capability_factor = json_set(business_capability_factor, '$.\"currentMaturity\"', ?) where id = ?";

		int count = 0;
		double totalMaturity = 0.0;
		PreparedStatement pst = conn.prepareStatement(parentsql);
		pst.setString(1, parentId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count++;
			String maturity = (String)rs.getString("maturity");
			if(maturity != null && maturity.length() > 0) {
				totalMaturity += Double.parseDouble(maturity);
			}
		}
		rs.close();
		pst.close();

		if(count > 0) {
			double avgMaturity = Math.round((totalMaturity/count) * 10.0) / 10.0;
			pst = conn.prepareStatement(updatesql);
			pst.setString(1, Double.toString(avgMaturity));
			pst.setString(2, parentId);
			pst.executeUpdate();
			pst.close();

			// Rollup to next level up
			id = parentId;
			parentId = null;
			String parentIdsql = "select parent_capability_id from enterprise_business_capability where id = ? and business_capability_level > 0";
			pst = conn.prepareStatement(parentIdsql);
			pst.setString(1, id);
			rs = pst.executeQuery();
			while(rs.next()) {
				parentId = rs.getString("parent_capability_id");
			}
			rs.close();
			pst.close();
			if(parentId != null) {
				rollupMaturity(id, parentId);
			}
		}

	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		UCMRequirementLoader loader = new UCMRequirementLoader(filename, companyCode, env);
		loader.process();
	}
}