import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class SOCLoader {
	private String domain = "SOC 2";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public SOCLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		String previousParent = null;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String rId=null, rName=null, rDesc="", pId=null, parentCapability=null, pDesc="", securityFunction=null, parentDesc=null, cId=null, childCapability=null, desc=null, internalId=null;
				String status=null, ucmRef=null, isoRef=null, socRef=null, evidence=null, population=null, owner=null;
				String department="",maturity="",targetDate="",state="";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						rId = row[i].trim();
					} else if(i==1) {
						rName = row[i].trim();
					} else if(i==2) {
						rDesc = row[i].trim();
					} else if(i==3) {
						pId = row[i].trim();
					} else if(i==4) {
						parentCapability = row[i].trim();
					} else if(i==5) {
						pDesc = row[i].trim();
					} else if(i==6) {
						cId = row[i].trim();
					} else if(i==7) {
						childCapability = row[i].trim();
					} else if(i==8) {
						desc = row[i].trim();
					} else if(i==9) {
						maturity = row[i].trim();
					} else if(i==10) {
						status = row[i].trim();
					} else if(i==11) {
						department = row[i].trim();
					} else if(i==12) {
						owner = row[i].trim();
					} else if(i==13) {
						targetDate = row[i].trim();
					} else if(i==14) {
						state = row[i].trim();
					} 
				}

				if(maturity.equals("5")) {
					maturity = "5.0";
				} else if(maturity.equals("4")) {
					maturity = "4.0";
				} else if(maturity.equals("3")) {
					maturity = "3.0";
				} else if(maturity.equals("2")) {
					maturity = "2.0";
				} else if(maturity.equals("1")) {
					maturity = "1.0";
				} else {
					maturity = "0.0";
				}
					
				System.out.println(cId+":"+parentCapability+":"+childCapability+":"+desc);

				/*
				JSONArray isoArr = null;
				if(isoRef != null && isoRef.length() > 0 && !isoRef.equalsIgnoreCase("N/A")) {
					String[] isoRefArr = isoRef.split("\n");
					List isoRefList = Arrays.asList(isoRefArr);
					isoArr = new JSONArray(isoRefList);
				} else {
					isoArr = new JSONArray();
				}
				*/

				if(status == null) {
					status = "";
				}
				if(rDesc == null) {
					rDesc = "";
				}
				if(pDesc == null) {
					pDesc = "";
				}
				if(desc == null) {
					desc = "";
				} else {
					//desc = desc.replaceAll("[^\\u0020-\\u007e\\u00a0-\\u00ff]","");
				}
				if(childCapability.length() > 140) {
					childCapability = wrapText(childCapability);
				}

				OrderedJSONObject extObject = new OrderedJSONObject();
				extObject.put("family", rName);
				extObject.put("securityFunction", new JSONArray());
				extObject.put("comment", "");
				extObject.put("custom", new OrderedJSONObject());

				String ext = extObject.toString();
				
				// Check Capability
				String parentId = null, childId=null;
				//String capabilityId = checkCapability(parentCapability, childCapability);
				//if(capabilityId == null || capabilityId.length() == 0) {	
					String id = null;
					if(!capabilityList.has(rName)) {
						String capabilityId = checkCapability("SOC 2", rName);
						if(capabilityId == null) {
							OrderedJSONObject parentExt = new OrderedJSONObject();
							parentExt.put("securityFunction", new JSONArray());
							extObject.put("comment", "");
							id = generateUUID();
							insertCapability(id, rootId, rId, rootId, rName, rDesc, parentExt.toString(), "", "1", levelOne);
						} else {
							System.out.println("Updating capability...");
							updateCapability(capabilityId, ext);
						}
						capabilityList.put(rName, id);
					} else {
						id = (String)capabilityList.get(rName);
					}
					if(!capabilityList.has(parentCapability)) {
						String capabilityId = checkCapability(rName, parentCapability);
						if(capabilityId == null) {
							OrderedJSONObject parentExt = new OrderedJSONObject();
							parentExt.put("securityFunction", new JSONArray());
							extObject.put("comment", "");
							parentId = generateUUID();
							insertCapability(parentId, id, pId, rName, parentCapability, pDesc, parentExt.toString(), "", "2", levelOne);
						} else {
							System.out.println("Updating capability...");
							updateCapability(capabilityId, ext);
						}
						capabilityList.put(parentCapability, parentId);
					} else {
						parentId = (String)capabilityList.get(parentCapability);
					}
					if(childCapability.trim().length() > 0 && !capabilityList.has(childCapability)) {
						String capabilityId = checkCapability(parentCapability, childCapability);
						if(capabilityId == null) {
							levelTwo = 0;
							childId = generateUUID();
							insertCapability(childId, parentId, cId, parentCapability, childCapability, desc, ext, "", "3", levelTwo);
							levelOne++;
							levelTwo++;
						} else {
							System.out.println("Updating capability...");
							updateCapability(capabilityId, ext);
						}
					}	
				//}
				
				System.out.println(parentId+":"+parentCapability+":"+childId+":"+childCapability);
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability("SOC 2", "SOC 2");
		
		if(rootId == null || rootId.length() == 0) {
			OrderedJSONObject extObject = new OrderedJSONObject();
			String ext = extObject.toString();

			//rootId = generateUUID();
			rootId = "SOC 2";
			insertCapability(rootId, rootId, "", rootId, rootId, "SOC 2 Controls", ext, "", "0", 0);
		}
		capabilityList.put(rootId, rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String ext, String owner, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "4";
   		String createdBy = "support@smarterd.com";
   		String status = "Y";
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();
   		
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void updateCapability(String id, String ext) throws Exception {
		String updatesql = "update enterprise_business_capability set business_capability_ext = ? where id = ?";
		PreparedStatement pst = conn.prepareStatement(updatesql);
		pst.setString(1, ext);
		pst.setString(2, id);
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?characterEncoding=utf8&useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private String wrapText(String text) throws Exception {
		String str = "";

		if(text.length() > 0) {
			String[] strList = text.split(" ");
			for(int i=0; i<strList.length; i++) {
				String s = strList[i];
				int len = s.length();
				int totalLen = str.length()+len;
				if(totalLen <= 140) {
					str = str + " " + s;
				} else {
					str += "...";
					break;
				}
			}
		}

		return str;
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		SOCLoader loader = new SOCLoader(filename, companyCode, env);
		loader.process();
	}
}