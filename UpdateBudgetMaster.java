import java.sql.*;
import java.util.*;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;

public class UpdateBudgetMaster {
    private String updatedBy = null;
    private String companyCode = null;
    private String hostname = "localhost";
    private Connection conn = null;
    private CSVReader reader = null;

    public UpdateBudgetMaster(String filename, String companyCode, String env) throws Exception {
        this.companyCode = companyCode;

        if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    private void update() throws Exception {
        System.out.println("Updating Budget Master..."); 
        String updatebudgetsql = "update enterprise_budget set committed_capex = ?, committed_opex = ? where cost_center_number=? and fiscal_year=?";
        
		String relsql = "select distinct financial_id from enterprise_strategy where plan_type = '3RD PARTY' and is_activity = 'Y'";
		PreparedStatement pst = conn.prepareStatement(relsql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String ccNo = rs.getString("financial_id");
			if(ccNo != null && ccNo.length() > 0) {
				int year = 21;
				
				// Move 2 years back and 4 years forward
				year = year - 2;
				for(int i=0; i<6; i++) {
					String fy = "FY"+year;
					String capex = "FY 20"+year+ " COMMITTED CAPEX";
					String opex = "FY 20"+year+ " COMMITTED";

					String thirdpartyrelsql = "select sum(json_unquote(json_extract(extension, '$.\""+capex+"\"'))) total_capex, sum(json_unquote(json_extract(extension, '$.\""+opex+"\"'))) total_opex from enterprise_strategy where financial_id = ?";
					double totalCommittedCapex = 0.0, totalCommittedOpex = 0.0; 
					PreparedStatement pstmt1 = conn.prepareStatement(thirdpartyrelsql);
					pstmt1.setString(1, ccNo);
					System.out.println(pstmt1.toString());
					ResultSet rs1 = pstmt1.executeQuery();
					while(rs1.next()) {
						totalCommittedCapex = rs1.getDouble("total_capex");
						totalCommittedOpex = rs1.getDouble("total_opex");
					}
					rs1.close();
					pstmt1.close();
						
					pstmt1 = conn.prepareStatement(updatebudgetsql);
					pstmt1.setDouble(1,totalCommittedCapex);
					pstmt1.setDouble(2,totalCommittedOpex);
					pstmt1.setString(3, ccNo);
					pstmt1.setString(4, fy);
					System.out.println(pstmt1.toString());
					pstmt1.executeUpdate();
					pstmt1.close();

					year++;
				}
			}
		}
		rs.close();
		pst.close();
        System.out.println("Successfully updated budget master!!!");
		conn.commit();
    }

    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String filename = args[0];
		String companyCode = args[1];
        String env = args[2];
        
        UpdateBudgetMaster bm = new UpdateBudgetMaster(filename, companyCode, env);
        bm.update();
    } 
}