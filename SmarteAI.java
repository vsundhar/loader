import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.io.File;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Base64;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.ContentType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;

public class SmarteAI {
   private String companyCode = null;
   private Connection conn = null;
   //static String apiKey = "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";
   static String apiKey = "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";
   static String aiUrl = "https://api.openai.com/v1/chat/completions";
   
   String hostname = "localhost";
   //String hostName = "dhamysqldb01";
   public SmarteAI(String companyCode, String env) throws Exception {
      this.companyCode = companyCode;
        
      // Connect Mysql
      mysqlConnect(companyCode);
   }

   private void clean() throws Exception {
      conn.commit();
      conn.close();
   }

   private void processRequest() throws Exception {
      System.out.println("In processRequest...");

      // Build Model Request
      OrderedJSONObject reqObj = new OrderedJSONObject();
      reqObj.put("model",  "gpt-3.5-turbo");
      reqObj.put("temperature", new Integer(1));
      //reqObj.put("max_tokens", new Integer(256));
      //reqObj.put("top_p", new Integer(1));
      //reqObj.put("frequency_penalty", new Integer(0));
      //reqObj.put("presence_penalty", new Integer(0));

      OrderedJSONObject contentObj = new OrderedJSONObject();
      contentObj.put("role", "user");
      contentObj.put("content", "What is the current state of Technology in Retail\n");
      JSONArray msgList = new JSONArray();
      msgList.add(contentObj);
      reqObj.put("messages", msgList);
     
		// Add Options
      System.out.println(aiUrl);
      /*
      CUrl curl = new CUrl(aiUrl);
		curl = curl.opt("-X", "POST");
		curl = curl.opt("-H", "Authorization: Bearer "+apiKey);
		curl = curl.opt("-H", "Content-Type: application/json");
      curl = curl.opt("-d", reqObj.toString());
      //curl = curl.opt("-G", aiUrl);
      System.out.println(curl.toString());
		String res = curl.exec(null);

      System.out.println(res);
      if(res.length() > 0) {
         try {
            //OrderedJSONObject resObj = new OrderedJSONObject(res);
            //System.out.println(resObj.toString());
         } catch(Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
         }
      } else {
         System.out.println("No Response!!!");
      }
      */
      /*
      URL url = new URL (aiUrl);
      HttpURLConnection con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setRequestProperty("Authorization", "Bearer "+apiKey);
      con.setDoOutput(true);
      */

      HttpUriRequest request = RequestBuilder.post()
      .setUri(URI.create(aiUrl))
      .setHeader("Content-Type", "application/json")
      .setHeader("Authorization", "Bearer "+apiKey)
      .setEntity(new StringEntity(reqObj.toString()))
      .build();

      CloseableHttpClient httpclient = HttpClients.createDefault();
      HttpResponse response = httpclient.execute(request);

      System.out.println("Status code: " + response.getStatusLine().getStatusCode());
      String responseString = new BasicResponseHandler().handleResponse(response);
      System.out.println("Response: " + responseString);

      //HttpResponse<String> response = con.send(request, BodyHandlers.ofString());
      //System.out.println(response.body());

      /*
      try(OutputStream os = con.getOutputStream()) {
         byte[] input = reqObj.toString().getBytes("utf-8");
         os.write(input, 0, input.length);			
      }
      */
      /*
      try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
         StringBuilder response = new StringBuilder();
         String responseLine = null;
         while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
         }
         System.out.println(response.toString());
      }
      */
   }

   private void mysqlConnect(String dbname) throws Exception {
      String userName = "root", password = "smarterD2018!";
      //String userName = "smarterd", password = "uXrPwz64Dg21DgYTMF";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }

   public static void main(String args[]) throws Exception {
      String companyCode = args[0];
      String env = args[1];

      SmarteAI ai = new SmarteAI(companyCode, env);
      ai.processRequest();
   }
}