import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class CMMCLevel3Loader {
	private String domain = "CMMC L3";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public CMMCLevel3Loader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;

		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}

		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		String controlFamily=null, familyInternalId=null, capabilityName=null, parentCapability=null, internalId=null, notes=null, artifacts=null, nistId=null;
		String familyId=null, parentId = null, prevParentId = null;
		int childCount = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				String childCapability=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						if(row[i].trim().length() > 0) {
							if(row[i].indexOf("(") != -1) {
								controlFamily = row[i].substring(0, row[i].indexOf("(")).trim();
								familyInternalId = row[i].substring(row[i].indexOf("(")+1, row[i].indexOf(")")).trim();
							} else {
								controlFamily = row[i].trim();
							}
						}
						System.out.println("Control Family:"+controlFamily);
					} else if(i==1) {
						if(row[i].trim().length() > 0) {
							capabilityName = row[i].trim();
						}
						System.out.println("Capability:"+capabilityName);
					} else if(i==2) {
						if(row[i].trim().length() > 0) {
							if(row[i].indexOf("(") != -1) {
								internalId = row[i].trim().substring(0, row[i].indexOf("(")).trim();
								nistId = row[i].substring(row[i].indexOf("(")+1, row[i].indexOf(")")).trim();
							} else {
								internalId = row[i].trim();
								nistId = "";
							}
						}
						System.out.println("Internal Id:"+internalId);
						System.out.println("NIST Id:"+nistId);
					} else if(i==3) {
						if(row[i].trim().length() > 0) {
							if(parentId != null) {
								updateNotes(parentId, notes, artifacts);
							}
							parentCapability = row[i].trim();
							notes = "";
							artifacts = "";
						}
						System.out.println("Practice Name:"+parentCapability);
					} else if(i==4) {
						childCapability = row[i].trim();
						System.out.println("Objective:"+childCapability);
					} else if(i==5) {
						if(row[i].trim().length() > 0) {
							if(notes == null || notes.length()==0) {
								notes = row[i].trim();
							} else {
								notes = notes+"\n\n"+row[i];
							}
						}
						System.out.println("Notes:"+notes);
					} else if(i==6) {
						if(row[i].trim().length() > 0) {
							if(artifacts == null || artifacts.length()==0) {
								artifacts = row[i].trim();
							} else {
								artifacts = artifacts+"\n\n"+row[i];
							}
						}
						System.out.println("Artifacts:"+artifacts);
					}
				}
					
				//System.out.println(controlFamily+":"+familyInternalId+":"+parentCapability+":"+childCapability+":"+internalId);
				//System.out.println(notes+":"+artifacts);
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("compliance", new OrderedJSONObject());
				extObj.put("assetClass", new JSONArray());
				extObj.put("ecosystem", new JSONArray());
				extObj.put("securityFunction", new JSONArray());
				extObj.put("COST", new Integer(0));
				extObj.put("IMPACT", new Integer(0));
				extObj.put("EFFICACY", new Integer(0));
				extObj.put("user_risk", new Double(0.0));
				extObj.put("user_priority", "Low");
				extObj.put("frequency", new JSONArray());
				extObj.put("control_status", "");
				extObj.put("criticality", "0");	

				OrderedJSONObject customObj = new OrderedJSONObject();
				customObj.put("Capability", capabilityName);
				customObj.put("Notes", "");
				extObj.put("custom", customObj);

				// Check Family
				familyId = checkCapability(domain, controlFamily);		
				if(familyId == null) {
					familyId = generateUUID();
					insertCapability(familyId, rootId, familyInternalId, domain, controlFamily, "", extObj.toString(), "1", 1);
				}

				parentId = checkCapability(internalId);		
				if(parentId == null) {
					extObj.put("family", controlFamily);
					
					parentId = generateUUID();
					insertCapability(parentId, familyId, internalId, controlFamily, parentCapability, "", extObj.toString(), "2", 1);
					childCount = 0;
				} else {
					updateNISTMapping(parentId, nistId);
				}

				String childId = null;
				// Load only Questions that have codes or Yes and No
				if(childCapability != null && (childCapability.startsWith("3.") || childCapability.startsWith("Yes"))) {
					childId = checkCapability(parentCapability, childCapability);		
					if(childId == null) {
						extObj.put("family", controlFamily);

						childCount++;
						String childInternalId = internalId +"."+Integer.toString(childCount);
						System.out.println("Child Internal Id:"+childInternalId);
						
						childId = generateUUID();
						insertCapability(childId, parentId, childInternalId, parentCapability, childCapability, "", extObj.toString(), "3", 1);
					}
				}
			}
		}
		
		conn.commit();
		conn.close();
	}

	private void updateNotes(String id, String notes, String artifacts) throws Exception {
		String updatesql = "update enterprise_business_capability set business_capability_comment=?,business_capability_ext=json_set(business_capability_ext, '$.custom.\"Artifacts\"', ?) where id = ?";
		PreparedStatement pst = conn.prepareStatement(updatesql);
		pst.setString(1, notes);
		pst.setString(2, artifacts);
		pst.setString(3, id);
		pst.executeUpdate();
		pst.close();
	}

	private void updateNISTMapping(String id, String nistId) throws Exception {
		String updatesql = "update enterprise_business_capability set business_capability_ext=json_set(business_capability_ext, '$.custom.\"NIST Mapping\"', ?) where id = ?";
		PreparedStatement pst = conn.prepareStatement(updatesql);
		pst.setString(1, nistId);
		pst.setString(2, id);
		pst.executeUpdate();
		pst.close();
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability(domain, domain);
		
		if(rootId == null || rootId.length() == 0) {
			rootId = domain;
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("custom", new OrderedJSONObject());
			extObj.put("activity_level", new Integer(2));
			extObj.put("requirement_level", new Integer(3));
			extObj.put("reporting_level", new Integer(2));
			extObj.put("rollup_maturity", "Yes");

			insertCapability(domain, domain, "", domain, domain, domain+" Controls", extObj.toString(), "0", 0);
			capabilityList.put(domain, rootId);
		}
		
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}

	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, String ext, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String owner = "", status = "Y";
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		factorObject.put("currentMaturity", "0.0");
		factorObject.put("targetMaturity", "0.0");
		factorObject.put("baselineMaturity", "0.0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		CMMCLevel3Loader loader = new CMMCLevel3Loader(filename, companyCode, env);
		loader.process();
	}
}