import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class ISOLoader {
	private String domain = "ISO 27001";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0, levelThree = 0;
   	
	public ISOLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		String previousParent = null;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String rId=null, rName=null, pId=null, parentCapability=null, parentDesc=null, cId=null, childCapability=null, desc=null;
				String internalGuidance="", externalGuidance="";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						rId = row[i].trim();
					} else if(i==1) {
						rName = row[i].trim();
					} else if(i==2) {
						pId = row[i].trim();
					} else if(i==3) {
						parentCapability = row[i].trim();
					} else if(i==4) {
						parentDesc = row[i].trim();
					} else if(i==5) {
						cId = row[i].trim();
					} else if(i==6) {
						childCapability = row[i].trim();
					} else if(i==7) {
						desc = row[i].trim();
					} else if(i==8) {
						internalGuidance = row[i].trim();
					} else if(i==9) {
						externalGuidance = row[i].trim();
					}
				}
					
				System.out.println(cId+":"+parentCapability+":"+childCapability+":"+desc);

				OrderedJSONObject extObject = new OrderedJSONObject();
				extObject.put("family", rName);
				extObject.put("internal_guidance", internalGuidance);
				extObject.put("external_guidance", externalGuidance);
				extObject.put("evidence", "");
				extObject.put("population", "");
				extObject.put("revision", "No");
				extObject.put("duplicate", "No");
				extObject.put("comment", "");
				extObject.put("criticality", "");
				extObject.put("securityFunction", new JSONArray());
				extObject.put("ecosystem", new JSONArray());
				extObject.put("assetClass", new JSONArray());
				extObject.put("compliance", new OrderedJSONObject());
				OrderedJSONObject f1 = new OrderedJSONObject();
				f1.put("type", "");
				f1.put("schedule", "");
				OrderedJSONObject f2 = new OrderedJSONObject();
				f2.put("type", "");
				f2.put("schedule", "");
				JSONArray f = new JSONArray();
				f.add(f1);
				f.add(f2);
				extObject.put("frequency", f);

				String ext = extObject.toString();
				
				// Check Capability
				String parentId = null, childId=null;
				String capabilityId = checkCapability(cId);
				if(capabilityId == null || capabilityId.length() == 0) {	
					String id = null;
					if(!capabilityList.has(rId)) {
						levelOne++;
						levelTwo = 0;
						OrderedJSONObject parentExt = new OrderedJSONObject();
						parentExt.put("family", rName);
						parentExt.put("internal_guidance", internalGuidance);
						parentExt.put("external_guidance", externalGuidance);
						parentExt.put("evidence", "");
						parentExt.put("population", "");
						parentExt.put("revision", "No");
						parentExt.put("duplicate", "No");
						parentExt.put("comment", "");
						parentExt.put("criticality", "");
						parentExt.put("securityFunction", new JSONArray());
						parentExt.put("ecosystem", new JSONArray());
						parentExt.put("assetClass", new JSONArray());
						parentExt.put("compliance", new OrderedJSONObject());
						parentExt.put("frequency", f);
						
						id = generateUUID();
						insertCapability(id, rootId, rId, rootId, rName, "", parentExt.toString(), "", "1", levelOne);
						capabilityList.put(rId, id);
					} else {
						id = (String)capabilityList.get(rId);
					}
					if(!capabilityList.has(pId)) {
						levelThree = 0;
						OrderedJSONObject parentExt = new OrderedJSONObject();
						parentExt.put("family", rName);
						parentExt.put("internal_guidance", internalGuidance);
						parentExt.put("external_guidance", externalGuidance);
						parentExt.put("evidence", "");
						parentExt.put("population", "");
						parentExt.put("revision", "No");
						parentExt.put("duplicate", "No");
						parentExt.put("comment", "");
						parentExt.put("criticality", "");
						parentExt.put("securityFunction", new JSONArray());
						parentExt.put("ecosystem", new JSONArray());
						parentExt.put("assetClass", new JSONArray());
						parentExt.put("compliance", new OrderedJSONObject());
						parentExt.put("frequency", f);
						parentId = generateUUID();
						insertCapability(parentId, id, pId, rName, parentCapability, parentDesc, parentExt.toString(), "", "2", levelTwo);
						capabilityList.put(pId, parentId);
						levelTwo++;
					} else {
						parentId = (String)capabilityList.get(pId);
					}
					if(!capabilityList.has(cId)) {
						childId = generateUUID();
						insertCapability(childId, parentId, cId, parentCapability, childCapability, desc, ext, "", "3", levelThree);
						levelThree++;
						capabilityList.put(cId, childId);
					}	
				}
				
				System.out.println(parentId+":"+parentCapability+":"+childId+":"+childCapability);
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability("ISO 27001");
		
		if(rootId == null || rootId.length() == 0) {
			OrderedJSONObject extObject = new OrderedJSONObject();
			String ext = extObject.toString();

			rootId = "ISO 27001";
			insertCapability(rootId, rootId, rootId, rootId, rootId, "ISO 27001 Controls", ext, "", "0", 0);
		}
		capabilityList.put(rootId, rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String ext, String owner, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String status = "Y";
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();
   		
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		ISOLoader loader = new ISOLoader(filename, companyCode, env);
		loader.process();
	}
}