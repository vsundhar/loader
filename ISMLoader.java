import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

// Australian Information Security Management Framework
public class ISMLoader {
	private String domain = "ISM";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
	String guideline="", family = null, comment=""; 
    
	public ISMLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;

		if(env.equalsIgnoreCase("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equalsIgnoreCase("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}

		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0, levelOne = 0, levelTwo = 0, levelThree = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				String sNo = "", activity="", control="", desc="", applicability="";

				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						if(row[i].trim().length() > 0) {	
							sNo = row[i];
						}
					} else if(i==1) {
						if(row[i].trim().length() > 0) {
							guideline = row[i];
						}
					} else if(i==2) {
						if(row[i].trim().length() > 0) {
							family = row[i].trim();
						}
					} else if(i==3) {
						if(row[i].trim().length() > 0) {
							activity = row[i].trim();
						}
					} else if(i==4) {
						control = row[i].trim();
					} else if(i==5) {
						if(row[i].trim().length() > 0) {
							desc = row[i].trim();
						}
					} else if(i==6) {
						applicability = row[i].trim();
					} else if(i==7) {
					} else if(i==8) {
						comment = row[i].trim();
					}
				}
					
				System.out.println(sNo+":"+guideline+":"+family+":"+activity+":"+control);
				if(applicability.equalsIgnoreCase("Yes")) {
					// Check Capability
					String familyId = null, activityId=null, controlId=null, internalId = null;
					familyId = checkCapability(domain, family);
					if(familyId == null) {
						familyId = generateUUID();
						levelOne++;
						levelTwo = 0;
						levelThree = 0;
						internalId = Integer.toString(levelOne);
						insertCapability(familyId, rootId, internalId, domain, family, "", "1", levelOne);
						capabilityList.put(familyId, internalId);
					} else {
						internalId = (String)capabilityList.get(familyId);
					}

					activityId = checkCapability(family, activity);
					if(activityId == null) {
						activityId = generateUUID();
						levelTwo++;
						levelThree = 0;
						internalId = internalId+"."+Integer.toString(levelTwo);
						insertCapability(activityId, familyId, internalId, family, activity, "", "2", levelTwo);
						capabilityList.put(activityId, internalId);
					} else {
						internalId = internalId + "." + (String)capabilityList.get(activityId);
					}

					controlId = checkCapability(activity, control);
					if(controlId == null) {
						controlId = generateUUID();
						levelThree++;
						internalId = internalId+"."+Integer.toString(levelThree);
						insertCapability(controlId, activityId, internalId, activity, control, desc, "3", levelThree);
					}
				}
			}
		}
		
		conn.commit();
		conn.close();
	}

	private void update2020Assessment() throws Exception {
		String[] row;
		int rowCount=0;
		String sql = "select id from enterprise_assessment_detail where json_extract(extension, '$.control_name')=?";
		String updatesql = "update enterprise_assessment_detail set control_in_place=?,status=? where id = ?";
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				String controlName = "", status="", control="", review="", artifacts="";

				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						controlName = row[i];
					} else if(i==1) {
						status = row[i];
					} else if(i==2) {
			
					} else if(i==3) {
						review = row[i].trim();
					} else if(i==4) {
						artifacts = row[i].trim();
					}
				}

				String id = null;
				PreparedStatement pst = conn.prepareStatement(sql);
				pst.setString(1, controlName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
				}
				rs.close();
				pst.close();
				if(id != null) {
					System.out.println(controlName+":"+status+":"+artifacts);
					if(status.equalsIgnoreCase("Compliant")) {
						status = "Fully Implemented";
					} else if(status.equalsIgnoreCase("Non-Compliant")) {
						status = "Not Implemented";
					} else if(status.equalsIgnoreCase("Not Applicable")) {
						status = "N/A";
					} else if(status.equalsIgnoreCase("TBC")) {
						status = "Not Implemented";
					} else {
						status = "Not Implemented";
					}
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, review);
					pst.setString(2, status);
					pst.setString(3, id);
					pst.executeUpdate();
					pst.close();

					// Rollup Assessment
					rollupAssessment(id);

					// Add Artifacts
					if(artifacts.length() > 0) {
						String[] list = artifacts.split("\n");
						for(int i=0; i<list.length; i++) {
							createKnowledge(id, list[i].trim(), "");
						}
					}

				} else {
					System.out.println("Control not found:"+controlName);
				}
			}
		}
		conn.commit();
		conn.close();
	}

	 // Rollup Assessment Status/Severity/Effort
    private void rollupAssessment(String id) throws Exception {
        System.out.println("In rollupAssessment:"+id);
        String controldomainsql = "select assessment_id,parent_control_id,business_capability_domain,control_level from enterprise_assessment_detail a, enterprise_business_capability b where a.id=? and a.control_id=b.id";
        String controlrootsql = "select json_unquote(json_extract(business_capability_ext, '$.\"rollup_assessment\"')) rollup_assessment from enterprise_business_capability where business_capability_level = 0 and business_capability_domain=?";
        String assessmentchilddetailsql = "select json_unquote(json_extract(extension, '$.internal_id')) internal_id,status,severity,effort,gaps,control_in_place,recommendation, json_unquote(json_extract(extension, '$.response')) response from enterprise_assessment_detail where parent_control_id = ? and assessment_id=? order by internal_id";
        String assessmentparentdetailsql = "select json_unquote(json_extract(extension, '$.internal_id')) internal_id,status,severity,effort,gaps,control_in_place,recommendation, json_unquote(json_extract(extension, '$.response')) response from enterprise_assessment_detail where control_id = ? and assessment_id=? order by internal_id";
        //String updatecontrolsql = "update enterprise_assessment_detail set status=?, severity=?, effort=?, gaps=?, control_in_place=? where control_id = ? and assessment_id=?";
        String updatecontrolsql = "update enterprise_assessment_detail set status=?, gaps=?, control_in_place=?, recommendation = ? where control_id = ? and assessment_id=?";
        LinkedHashMap rollupObj = new LinkedHashMap();

        // Get Assessment Id, Parent Control and Domain
        String assessmentId=null,parentControlId=null,domain=null,level=null;
        PreparedStatement pst = conn.prepareStatement(controldomainsql);
        pst.setString(1, id);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            assessmentId = rs.getString("assessment_id");
            parentControlId = rs.getString("parent_control_id");
            domain = rs.getString("business_capability_domain");
            level = rs.getString("control_level");
        }
        rs.close();
        pst.close();
        System.out.println(assessmentId+":"+parentControlId+":"+domain+":"+level);

        if(level.equals("3")) {
            // Check Asssessment Rollup Flag
            String rollupFlag = "Y";
            pst = conn.prepareStatement(controlrootsql);
            pst.setString(1, domain);
            rs = pst.executeQuery();
            while(rs.next()) {
                rollupFlag = rs.getString("rollup_assessment");
            }
            rs.close();
            pst.close();

            if(rollupFlag == null || rollupFlag.length() == 0) {
                rollupFlag = "Y";
            }

            System.out.println("Rollup Flag:"+rollupFlag);
            // Get Assessment Details
            if(rollupFlag.equalsIgnoreCase("Y")) {
                String overallStatus = "", overallSeverity = "", overallEffort = "";
                String overallGaps = "", overallControlInPlace = "", overallRecommendation = "";

                // Get Child Details and rollup to parent
                pst = conn.prepareStatement(assessmentchilddetailsql);
                pst.setString(1, parentControlId);
                pst.setString(2, assessmentId);
                rs = pst.executeQuery();
                while(rs.next()) {
                    String internalId = rs.getString("internal_id");
                    String status = rs.getString("status");
                    //String severity = rs.getString("severity");
                    //String effort = rs.getString("effort");
                    String gaps = rs.getString("gaps");
                    String controlInPlace = rs.getString("control_in_place");
                    //String response = rs.getString("response");
                    String recommendation = rs.getString("recommendation");

                    if(status != null && status.length() > 0) {
                        if(overallStatus.length() == 0) {
                            overallStatus = status;
                        } else if(status.equalsIgnoreCase("Not Implemented")) {
                            overallStatus = status;
                        }
                    }
                    internalId = internalId.substring(internalId.lastIndexOf(".")+1);
                   
                    if(gaps != null && gaps.trim().length() > 0) {
                        LinkedHashMap gapList = null;
                        ArrayList gapArr = null;
                        if(rollupObj.containsKey("gaps")) {
                            gapList = (LinkedHashMap)rollupObj.get("gaps");
                            if(gapList.containsKey(gaps)) {
                                gapArr = (ArrayList)gapList.get(gaps);
                            } else {
                                gapArr = new ArrayList();
                            }
                        } else {
                            gapList = new LinkedHashMap();
                            gapArr = new ArrayList();
                        }
                        gapArr.add(internalId);
                        gapList.put(gaps, gapArr);
                        rollupObj.put("gaps", gapList);
                    }
                    if(controlInPlace != null && controlInPlace.trim().length() > 0) {
                        LinkedHashMap controlList = null;
                        ArrayList controlArr = null;
                        if(rollupObj.containsKey("controls")) {
                            controlList = (LinkedHashMap)rollupObj.get("controls");
                            if(controlList.containsKey(controlInPlace)) {
                                controlArr = (ArrayList)controlList.get(controlInPlace);
                            } else {
                                controlArr = new ArrayList();
                            }
                        } else {
                            controlList = new LinkedHashMap();
                            controlArr = new ArrayList();
                        }
                        controlArr.add(internalId);
                        controlList.put(controlInPlace, controlArr);
                        rollupObj.put("controls", controlList);
                    }
                   
                    if(recommendation != null && recommendation.trim().length() > 0) {
                        LinkedHashMap recommendationList = null;
                        ArrayList recommendationArr = null;
                        if(rollupObj.containsKey("recommendation")) {
                            recommendationList = (LinkedHashMap)rollupObj.get("recommendation");
                            if(recommendationList.containsKey(recommendation)) {
                                recommendationArr = (ArrayList)recommendationList.get(recommendation);
                            } else {
                                recommendationArr = new ArrayList();
                            }
                        } else {
                            recommendationList = new LinkedHashMap();
                            recommendationArr = new ArrayList();
                        }
                        recommendationArr.add(internalId);
                        recommendationList.put(recommendation, recommendationArr);
                        rollupObj.put("recommendation", recommendationList);
                    }
                }
                rs.close();
                pst.close();

                if(rollupObj.containsKey("gaps")) {
                    int count = 0;
                    LinkedHashMap gapList = (LinkedHashMap)rollupObj.get("gaps");
                    Iterator iter = gapList.keySet().iterator();
                    while(iter.hasNext()) {
                        String gaps = (String)iter.next();
                        if(gaps != null && gaps.trim().length() > 0) {
                            ArrayList arr = (ArrayList)gapList.get(gaps);
                            String ids = arr.toString().replaceAll("\"", "");
                            if(overallGaps.trim().length() == 0) {
                                overallGaps = gaps+" "+ids; 
                            } else {
                                overallGaps = overallGaps+"\n"+gaps+" "+ids;
                            }
                            count++;
                        }
                    }
                }
                if(rollupObj.containsKey("controls")) {
                    int count = 0;
                    LinkedHashMap list = (LinkedHashMap)rollupObj.get("controls");
                    Iterator iter = list.keySet().iterator();
                    while(iter.hasNext()) {
                        String controlInPlace = (String)iter.next();
                        if(controlInPlace != null && controlInPlace.trim().length() > 0) {
                            ArrayList arr = (ArrayList)list.get(controlInPlace);
                            String ids = arr.toString().replaceAll("\"", "");
                            if(overallControlInPlace.trim().length() == 0) {
                                overallControlInPlace = controlInPlace+" "+ids; 
                            } else {
                                overallControlInPlace = overallControlInPlace+"\n"+controlInPlace+" "+ids;
                            }
                            count++;
                        }
                    }
                }
                if(rollupObj.containsKey("recommendation")) {
                    int count = 0;
                    LinkedHashMap list = (LinkedHashMap)rollupObj.get("recommendation");
                    Iterator iter = list.keySet().iterator();
                    while(iter.hasNext()) {
                        String recommendation = (String)iter.next();
                        if(recommendation != null && recommendation.trim().length() > 0) {
                            ArrayList arr = (ArrayList)list.get(recommendation);
                            String ids = arr.toString().replaceAll("\"", "");
                            if(overallRecommendation.trim().length() == 0) {
                                overallRecommendation = recommendation+" "+ids; 
                            } else {
                                overallRecommendation = overallRecommendation+"\n"+recommendation+" "+ids;
                            }
                            count++;
                        }
                    }
                }

                // Update Control
                pst = conn.prepareStatement(updatecontrolsql);
                pst.setString(1, overallStatus);
                pst.setString(2, overallGaps);
                pst.setString(3, overallControlInPlace);
                pst.setString(4, overallRecommendation);
                pst.setString(5, parentControlId);
                pst.setString(6, assessmentId);
                pst.executeUpdate();
                pst.close();
                conn.commit();

                System.out.println("Successfully rolled-up Assessment!!!");
            }
        }
    }

	private void createKnowledge(String assetId, String label, String knowledge) throws Exception {
		String checksql = "select count(*) count from enterprise_comment where TOPIC_NAME = ? and comp_id = ?";
		String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,comment_type,comment_address,extension,status,CREATED_BY,updated_by,COMPANY_CODE) values(uuid(),?,?,?,'Recommendation',?,?,?,'support@smarterd.com','support@smarterd.com',?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, label);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("component_type", "ASSET");
			OrderedJSONObject statusObj = new OrderedJSONObject();
			statusObj.put("userGenerated", new Boolean(true));
			statusObj.put("applicable", "");
			statusObj.put("maturity", "");
			statusObj.put("topiclink", "");
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, assetId);
			pst.setString(2, label);
			pst.setString(3, knowledge);
			pst.setString(4, "artifacts_referenced");
			pst.setString(5, extObj.toString());
			pst.setString(6, statusObj.toString());
			pst.setString(7, companyCode);
			pst.executeUpdate();
			pst.close();
		}
	}

	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability(domain, domain);
		
		if(rootId == null || rootId.length() == 0) {
			rootId = domain;
			insertCapability(rootId, rootId, domain, domain, domain, "Information Security Management", "0", 0);
			capabilityList.put(domain, domain);
		}
		
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}

	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id,child_capability_name from enterprise_business_capability where  internal_id = ? and business_capability_domain = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		pst.setString(2, domain);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_comment,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String owner = "", status = "Y";
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		OrderedJSONObject customObj = new OrderedJSONObject();
		customObj.put("Guideline", guideline);

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", family);
		extObject.put("assetClass", new JSONArray());
		extObject.put("ecosystem", new JSONArray());
		extObject.put("securityFunction", "");
		extObject.put("custom", customObj);
		extObject.put("priority", "");
		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, comment);
		pst.setString(16, ext);
		pst.setString(17, createdBy);
		pst.setString(18, createdBy);
		pst.setString(19, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
	}
	
	private void mysqlConnect(String dbname) throws Exception {
     	String userName = "peapi", password = "smartEuser2022!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		ISMLoader loader = new ISMLoader(filename, companyCode, env);
		//loader.process();
		loader.update2020Assessment();
	}
}