import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.commons.csv.CSVFormat;    // SmarteSchemaLoader uses this - Moving forward approach
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.wink.json4j.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.UUID;
import java.util.Iterator;
import java.time.Duration;
import java.time.Instant;

public class SmarteLoaderManager {

   public SmarteLoaderManager() {
   }

   public void load() throws Exception {
      Instant startTime = Instant.now();
      Connection conn = initializeConnection("SE");
      // Get all Jobs
      String sql = "select id,command,company_code from enterprise_job where active = 'Y'";
      PreparedStatement pst = conn.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         int id = rs.getInt("id");
         String command = rs.getString("command");
         String cc = rs.getString("company_code");

         SmarteLoader loader = (SmarteLoader) Class.forName(command).getConstructor().newInstance();
         System.out.println("Created Loader Instance!!!");
         loader.initialize(cc);
         System.out.println("Successfully Initialized loader!!!");
         loader.load();
         loader.cleanup();
      }
      rs.close();
      pst.close();


      Instant endTime = Instant.now();
      long duration = Duration.between(startTime, endTime).toMinutes();
		System.out.println("Successfully loaded Qualys Data["+duration+" mts]");
   }

   private Connection initializeConnection(String dbname) throws Exception {
      String hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
      String userName = "peapi", password = "smartEuser2022!";
      //String userName = "smarterd", password = "uXrPwz64Dg21DgYTMF";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      Connection conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
      return conn;
   }
}
