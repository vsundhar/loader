import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class BCTSecurityPlanLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public BCTSecurityPlanLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		String serviceName=null, requirement=null, desc=null, deliverable=null, type=null,status=null, responsibility=null;
		String category=null, timeline=null, targetDate=null, assignee=null, comment=null;

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				//System.out.println("Processing row-" + rowCount + "...");
				
				for(int i=0; i<row.length; i++) {
					if(i==0) {
						if(row[i] != null && row[i].length() > 0 ) {
							serviceName = row[i].trim();
						}
					} else if(i==1) {
						if(row[i] != null && row[i].length() > 0 ) {
							requirement = row[i].trim();
						}
					} else if(i==2) {	
						if(row[i] != null && row[i].length() > 0 ) {		
							desc = row[i];
						}
					} else if(i==3) {
						if(row[i] != null && row[i].length() > 0 ) {
							deliverable = row[i];
						}
					} else if(i==4) {
						if(row[i] != null && row[i].length() > 0 ) {
							type = row[i];
						}
					} else if(i==5) {
						if(row[i] != null && row[i].length() > 0 ) {
							status = row[i];
						}
					} else if(i==6) {
						if(row[i] != null && row[i].length() > 0 ) {
							responsibility = row[i].trim();
						}
					} else if(i==7) {
						if(row[i] != null && row[i].length() > 0 ) {
							category = row[i];
						}
					} else if(i==8) {
						if(row[i] != null && row[i].length() > 0 ) {
							timeline = row[i];
						}
					} else if(i==9) {
						if(row[i] != null && row[i].length() > 0 ) {
							targetDate = row[i];
						}
					} else if(i==10) {
						if(row[i] != null && row[i].length() > 0 ) {
							assignee = row[i];
						}
					} else if(i==11) {
						if(row[i] != null && row[i].length() > 0 ) {
							comment = row[i];
						}
					}
				}

				String planId = "4cc7e78d-ba5e-42ae-9544-5be25c8a5b46";

				// Insert SubPlan
				String subplansql = "select _id from enterprise_threat_subplan where name = ?";
				String subplaninsertsql = "insert into enterprise_threat_subplan(_id, name, description, threatPlan,subplan,root,planType, category, implementation, status, owner, extension, createdAt, updatedAt) values(?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp)";
				String subplanId = null;
				if(requirement != null && requirement.length() > 0) {
					PreparedStatement pst = conn.prepareStatement(subplansql);
					pst.setString(1, requirement);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						subplanId = rs.getString("_id");
					}
					rs.close();
					pst.close();
					System.out.println(requirement+":"+subplanId);
					
					if(subplanId == null) {
						subplanId = generateUUID();
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("fiscal_year", "FY21");
						extObj.put("roadmap", "N");
						extObj.put("assignee", "");
						extObj.put("level", new Integer(2));
						
						pst = conn.prepareStatement(subplaninsertsql);
						pst.setString(1, subplanId);
						pst.setString(2, requirement);
						pst.setString(3, desc);
						pst.setString(4, planId);
						pst.setString(5, null);
						pst.setBoolean(6, true);
						pst.setString(7, "CS");
						pst.setString(8, category);
						pst.setString(9, timeline);
						pst.setString(10, status);
						pst.setString(11, "");
						pst.setString(12, extObj.toString());
						pst.executeUpdate();
						pst.close();
					}
				}

				if(deliverable != null && deliverable.length() > 0) {
					String activityId = null;
					PreparedStatement pst = conn.prepareStatement(subplansql);
					pst.setString(1, deliverable);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						activityId = rs.getString("_id");
					}
					rs.close();
					pst.close();
					System.out.println(deliverable+":"+activityId);
					
					if(activityId == null) {
						activityId = generateUUID();
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("fiscal_year", "FY21");
						extObj.put("roadmap", "N");
						extObj.put("assignee", "");
						extObj.put("level", new Integer(3));
						
						pst = conn.prepareStatement(subplaninsertsql);
						pst.setString(1, activityId);
						pst.setString(2, deliverable);
						pst.setString(3, "");
						pst.setString(4, planId);
						pst.setString(5, subplanId);
						pst.setBoolean(6, false);
						pst.setString(7, "CS");
						pst.setString(8, category);
						pst.setString(9, timeline);
						pst.setString(10, status);
						pst.setString(11, "");
						pst.setString(12, extObj.toString());
						pst.executeUpdate();
						pst.close();
					}
				}
			}
		}
		System.out.println("Total Assets:"+rowCount);
		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		BCTSecurityPlanLoader loader = new BCTSecurityPlanLoader(filename, companyCode, env);
		loader.process();
	}
}