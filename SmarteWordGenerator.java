import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;

import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.IBody;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;

import org.apache.xmlbeans.XmlCursor;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class SmarteWordGenerator {
   private String companyCode = null;
   private XWPFTable templateTable = null;
   private XWPFParagraph templateParagraph = null;
  
   public SmarteWordGenerator(String companyCode) throws Exception {
      this.companyCode = companyCode;
   }

   public void generateReport(String reportName) throws Exception {
      try (XWPFDocument document = new XWPFDocument(Files.newInputStream(Paths.get("./"+reportName+".docx")))) {
         initializeTemplate(document);

         //if(templateTable != null && templateParagraph != null) {
            List<IBodyElement> elements = document.getBodyElements();
            for(int n = 0; n < elements.size(); n++) {
               IBodyElement element = elements.get(n);
               if(element instanceof XWPFParagraph) {
                  XWPFParagraph p1 = (XWPFParagraph) element;
                  String str = p1.getText();
                  if(str.equals("{Family Name}")) {
                     updateTemplateParagraph("First Family");
                  }
               } else if(element instanceof XWPFTable) {
                  XWPFTable t = (XWPFTable) element;
                  List<XWPFTableRow> tableRows = t.getRows();
                  int rowCount = tableRows.size();
                  System.out.println(rowCount);
                  if(rowCount == 7) {
                     updateTemplateTable(tableRows);
                     /*
                     XmlCursor cursor = t.getCTTbl().newCursor();
                     XWPFParagraph p = document.insertNewParagraph(cursor);
                     XWPFRun run = p.createRun();
                     run.setText("");
                     document.insertTable(n, templateTable);
                     //document.getBody().insertNewTbl(cursor);
                     break;
                     */
                     n++;
                     document.insertTable(n, templateTable);
                     break;
                  }
               }
            }

            // save the docs
            try (FileOutputStream out = new FileOutputStream("./vs_"+reportName+".docx")) {
               document.write(out);
               document.close();
               document.close();
            }
        // } else {
        //    System.out.println("Invalid Template File!!!");
        // }
      }
   }

   private void initializeTemplate(XWPFDocument doc) throws Exception {
      List<XWPFParagraph> headerList = doc.getParagraphs();
      Iterator hIter = headerList.iterator();
      while(hIter.hasNext()) {
         XWPFParagraph paragraph = (XWPFParagraph)hIter.next();
         String str = paragraph.getText();
         if(str.equals("{Family Name}")) {
            templateParagraph = paragraph;
            break;
         }
      }

      List<XWPFTable> tables = doc.getTables();
      for (int x=0; x<tables.size(); x++) {
         if(x == 3) {
            templateTable = tables.get(x);
            break;
         }
      }
   }

   private void updateTemplateParagraph(String content) throws Exception {
      XWPFParagraph paragraph = templateParagraph;
      String str = paragraph.getText();
      if(str.equals("{Family Name}")) {
         str = str.replaceFirst(".*", content);
         updateParagraph(str, paragraph);
      }
   }

   private void updateTemplateTable(List<XWPFTableRow> tableRows) throws Exception {
      for (int r=0; r<tableRows.size();r++) {
         XWPFTableRow tableRow = tableRows.get(r);
         List<XWPFTableCell> tableCells = tableRow.getTableCells();
         System.out.println("Row Cells:"+r+":"+tableCells.size());
         for (int c=0; c<tableCells.size();c++) {
            XWPFTableCell tableCell = tableCells.get(c);
            if(r == 0) {
               if(c == 0) {
                  String practiceName = tableCell.getText();
                  System.out.println("Practice Name:"+practiceName);
               }
            } else if(r == 1) {
               if(c == 1) { // Status
                  updateTableCell("Implemented", tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               } else if(c == 2) { // Severity
                  updateTableCell("High", tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               } else if(c == 3) { // Effort
                  updateTableCell("Low", tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               }
            } else if(r == 2) {
               if(c == 0) {
                  updateTableCell("Vijay", tableCell);
               } else if(c == 1) {
                  updateTableCell("Sundhar", tableCell);
               }
            } else {
               if(c > 0) {
                  updateTableCell("Vijay Sundhar", tableCell);
               }
            }
         }
      }
   }

   private void updateTableCell(String content, XWPFTableCell tableCell) throws Exception {
      XWPFParagraph paragraph = tableCell.getParagraphs().get(0);

      // Clear Content
      List<XWPFRun> runList = paragraph.getRuns();
      Iterator iter = runList.iterator();
      while(iter.hasNext()) {
         XWPFRun run = (XWPFRun)iter.next();
         String runText = run.getText(run.getTextPosition());
         String replaced = runText.replaceFirst(".*", "");
         run.setText(replaced, 0);
      }

      // Update Content
      if(runList.size() > 0) {
         XWPFRun run = paragraph.getRuns().get(0);
         run.setText(content, 0);
      } else {
         String str = tableCell.getText();
         String replaced = str.replaceFirst(".*", content);
         tableCell.setText(replaced);
      }
   }

   private void updateParagraph(String content, XWPFParagraph paragraph) throws Exception {
      // Clear Content
      List<XWPFRun> runList = paragraph.getRuns();
      Iterator iter = runList.iterator();
      while(iter.hasNext()) {
         XWPFRun run = (XWPFRun)iter.next();
         String runText = run.getText(run.getTextPosition());
         String replaced = runText.replaceFirst(".*", "");
         run.setText(replaced, 0);
      }

      // Update Content
      if(runList.size() > 0) {
         XWPFRun run = paragraph.getRuns().get(0);
         run.setText(content, 0);
      }
   }

   private static void removeParagraphs(XWPFTableCell tableCell) {
      int count = tableCell.getParagraphs().size();
      for(int i = 0; i < count; i++){
         tableCell.removeParagraph(i);
      }
   }

   public void loadData() throws Exception {
   }

   /*
   //Iterate through the list and check for table element type
   Iterator<IBodyElement> docElementsIterator = doc.getBodyElementsIterator();
   while (docElementsIterator.hasNext()) {
         IBodyElement docElement = docElementsIterator.next();
         if ("TABLE".equalsIgnoreCase(docElement.getElementType().name())) {
            //Get List of table and iterate it
            List<XWPFTable> xwpfTableList = docElement.getBody().getTables();
            for (XWPFTable xwpfTable : xwpfTableList) {
               int rowCount = xwpfTable.getNumberOfRows();
               if(rowCount == 7) {
                  for (int i = 0; i < xwpfTable.getRows().size(); i++) {
                     if(i > 2) {
                        for (int j = 0; j < xwpfTable.getRow(i).getTableCells().size(); j++) {
                           //System.out.println(xwpfTable.getRow(i).getCell(j).getText());
                           if(j > 0) {
                              XWPFTableCell tableCell = xwpfTable.getRow(i).getCell(j);
                              removeParagraphs(tableCell);
                              tableCell.setText("Vijay Sundhar");
                           }
                        }
                     }
                  }
                  break;
               }
            }
         }
   }
   */
   
   public static void main(String args[]) throws Exception {
      String reportName = "Scope of Work MAU Template";
      String companyCode = args[0];
      
      SmarteWordGenerator gen = new SmarteWordGenerator(companyCode);
      gen.generateReport(reportName);
   }
}