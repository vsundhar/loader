import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Properties;
import java.io.File;
import java.awt.Rectangle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.util.RegionUtil;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.text.*;
import java.sql.*;

public class SmarteXSLX {
   private String companyCode = null;
   private Connection conn = null;
   String hostname="localhost";
   
   public SmarteXSLX(String companyCode, String env) throws Exception {
      this.companyCode = companyCode;

		if(env.equalsIgnoreCase("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
   }

   public void generateAssessmentList(String domain, JSONArray controlList) throws Exception {
      System.out.println("In generateAssessmentList...");
      String reportName = companyCode+"_Assessment_Control_Requirements";
      String controlrootSQL = "select json_unquote(json_extract(business_capability_ext, '$.\"use_frequency_audit_to_initialize_assessment\"')) audit_flag, json_unquote(json_extract(business_capability_ext, '$.\"requirement_framework\"')) requirement_framework from enterprise_business_capability where business_capability_domain = ? and business_capability_level = '0'";
      Workbook wb = new XSSFWorkbook();

      // Get Audit Flag
      String auditFlag = "N", requirementFramework = null;
      PreparedStatement pst = conn.prepareStatement(controlrootSQL);
      pst.setString(1, domain);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         auditFlag = rs.getString("audit_flag");
         requirementFramework = rs.getString("requirement_framework");
      }
      rs.close();
      pst.close();

      if(auditFlag == null || auditFlag.length() == 0) {
         auditFlag = "N";
      }

      generateControlList(wb, auditFlag, domain, controlList);
      generateRequirementList(wb, auditFlag, requirementFramework, domain, controlList);

      // Write it to file
      File f = new File(reportName+".xlsx");
      FileOutputStream out = new FileOutputStream(f);
      wb.write(out);
      out.close();
      System.out.println("Successfully generated Assessment List!!!");
   }

   private void generateControlList(Workbook wb, String auditFlag, String domain, JSONArray controlList) throws Exception {
      System.out.println("In generateControlList...");
      String[] columns = { "Family", "Control", "Question", "Comment"};
      String controlsql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where business_capability_level = '2' and business_capability_domain = ?";
      String controlfiltersql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where id in (REPLACE_STRING) and business_capability_level = '2' and business_capability_domain = ?";
      String controlauditsql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions,json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where business_capability_level = '2' and business_capability_domain = ? and instr(json_extract(business_capability_ext, '$.frequency'),'Audit') > 0";
     
      Sheet sheet = wb.createSheet("Control List");
      sheet.setColumnWidth(0, 9000);
      sheet.setColumnWidth(1, 9000);
      sheet.setColumnWidth(2, 15000);
      sheet.setColumnWidth(3, 30000);

      Font headerFont = wb.createFont();
      headerFont.setBold(true);
      headerFont.setFontHeightInPoints((short) 14);
      headerFont.setColor(IndexedColors.RED.getIndex());

      CellStyle headerCellStyle = wb.createCellStyle();
      headerCellStyle.setFont(headerFont);
      headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
      headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
      headerCellStyle.setBorderTop(BorderStyle.THIN);
      headerCellStyle.setBorderBottom(BorderStyle.THIN);
      headerCellStyle.setBorderRight(BorderStyle.THIN);
      headerCellStyle.setBorderLeft(BorderStyle.THIN);

      // Create a Row
      int rowNum = 0;
      Row headerRow = sheet.createRow(rowNum);

      for (int i = 0; i < columns.length; i++) {
         Cell cell = headerRow.createCell(i);
         cell.setCellValue(columns[i]);
         cell.setCellStyle(headerCellStyle);
      }

      CellStyle cs1 = wb.createCellStyle();
      cs1.setAlignment(HorizontalAlignment.CENTER);
      cs1.setVerticalAlignment(VerticalAlignment.CENTER);
      cs1.setWrapText(true);
      cs1.setBorderBottom(BorderStyle.THIN);
      cs1.setBorderTop(BorderStyle.THIN);
      cs1.setBorderBottom(BorderStyle.THIN);
      cs1.setBorderRight(BorderStyle.THIN);
      cs1.setBorderLeft(BorderStyle.THIN);

      CellStyle cs2 = wb.createCellStyle();
      cs2.setWrapText(true);
      cs2.setBorderTop(BorderStyle.THIN);
      cs2.setBorderBottom(BorderStyle.THIN);
      cs2.setBorderRight(BorderStyle.THIN);
      cs2.setBorderLeft(BorderStyle.THIN);

      PreparedStatement pst = null;
      if(auditFlag.equalsIgnoreCase("Y")) {
         pst = conn.prepareStatement(controlauditsql);
      } else {
         if(controlList.size() == 0) {
            pst = conn.prepareStatement(controlsql);
         } else {
            String filter = controlList.toString();
            filter = filter.replaceAll("[\\[\\]]", "");
            controlfiltersql = controlfiltersql.replaceAll("REPLACE_STRING", filter);
            pst = conn.prepareStatement(controlfiltersql);
         }
      }
      pst.setString(1, domain);
      //System.out.println(pst.toString());
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String controlId = rs.getString("id");
         String parentId = rs.getString("parent_capability_id");
         String internalId = rs.getString("internal_id");
         String familyName = rs.getString("family");
         String parentName = rs.getString("parent_capability_name");
         String childName = rs.getString("child_capability_name");
         String currentMaturity = rs.getString("current_maturity");
         String level = rs.getString("business_capability_level");
         String score = rs.getString("weighted_score");
         String notes = rs.getString("business_capability_comment");
         String artifacts = rs.getString("artifacts");
         String nistMapping = rs.getString("nist_mapping");
         String standard = rs.getString("standard");
         String questions = rs.getString("questions");
         String applicableQuestions = rs.getString("applicable_questions");
         String requirements = rs.getString("requirements");

         JSONArray qList = null;
         if(questions != null && questions.length() > 0) {
            qList = new JSONArray(questions);
         } else {
            qList = new JSONArray();
         }

         OrderedJSONObject aqList = null;
         if(applicableQuestions != null && applicableQuestions.length() > 0) {
            aqList = new OrderedJSONObject(applicableQuestions);
         } else {
            aqList = new OrderedJSONObject();
         }

         // Get Parent Questions
         JSONArray parentQuestionList = getParentQuestionList(parentId);
         if(aqList.size() == 0) {    // Add all parent questions, if aqList is empty
            qList.addAll(parentQuestionList);
         } else {
            Iterator iter = aqList.keySet().iterator();
            while(iter.hasNext()) {
                  String qId = (String)iter.next();
                  String aFlag = (String)aqList.get(qId);
                  if(aFlag == null || aFlag.length() == 0 || aFlag.equalsIgnoreCase("Y")) {
                     Iterator parentIter = parentQuestionList.iterator();
                     while(parentIter.hasNext()) {
                        OrderedJSONObject qObj = (OrderedJSONObject)parentIter.next();
                        String pqId = (String)qObj.get("id");
                        if(pqId.equalsIgnoreCase(qId)) {
                           qList.add(qObj);
                        }
                     }
                  }
            }
         }
         System.out.println(rowNum+":"+parentName+":"+childName+":"+qList.size());
         rowNum = rowNum+1;
         int startRow = rowNum;
         Row row = sheet.createRow(rowNum);
         Cell cell0 = row.createCell(0);
         cell0.setCellValue(parentName);
         cell0.setCellStyle(cs1);
         Cell cell1 = row.createCell(1);
         cell1.setCellValue(childName+"\n"+internalId);
         cell1.setCellStyle(cs1);

         if(qList.size() == 0) {
            Cell cell2 = row.createCell(2);
            cell2.setCellValue("");
            cell2.setCellStyle(cs2);
         } else {
            for(int i=0; i<qList.size(); i++) {
               JSONObject qObj = (JSONObject)qList.get(i);
               if(qObj.has("question")) {
                  String q = (String)qObj.get("question");
                  if(i > 0) {
                     rowNum = rowNum + 1;
                     row = sheet.createRow(rowNum);
                  }
                  Cell cell2 = row.createCell(2);
                  cell2.setCellValue(q);
                  cell2.setCellStyle(cs2);

                  // For Comment
                  Cell cell3 = row.createCell(3);
                  cell3.setCellValue("");
                  cell3.setCellStyle(cs2);
               }
            }
         }
         int endRow = rowNum;
         if(endRow > startRow) {
            CellRangeAddress cr = new CellRangeAddress(startRow,endRow,0,0);
            sheet.addMergedRegion(cr);
            setBorder(sheet, BorderStyle.THIN, cr);

            cr = new CellRangeAddress(startRow,endRow,1,1);
            sheet.addMergedRegion(cr);
            setBorder(sheet, BorderStyle.THIN, cr);
         }
         
         //sheet.addMergedRegion(new CellRangeAddress(startRow,endRow,1,1));
      }
      rs.close();
      pst.close();
      /*
      for (int i = 0; i < columns.length; i++) {
         sheet.autoSizeColumn(i);
      }
      */

      System.out.println("Successfully generated Control worksheet!!!");
   }

   public void generateRequirementList(Workbook wb, String auditFlag, String requirementFramework, String domain, JSONArray controlList) throws Exception {
      System.out.println("In generateRequirementList...");
      String[] columns = { "Family", "Control", "Requirement", "Questions"};
      String controlsql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where business_capability_level in ('2', '3') and business_capability_domain = ?";
      String controlfiltersql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where id in (REPLACE_STRING) and business_capability_level in ('2', '3') and business_capability_domain = ?";
      String controlauditsql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions,json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where business_capability_level in ('2', '3') and business_capability_domain = ? and instr(json_extract(business_capability_ext, '$.frequency'),'Audit') > 0";
      String controlrequirementsql = "select id,parent_capability_id,internal_id,parent_capability_name,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) family,business_capability_level,business_capability_comment,json_unquote(json_extract(business_capability_ext, '$.\"requirements\"')) requirements,json_unquote(json_extract(business_capability_ext, '$.custom.\"Standard\"')) standard,json_unquote(json_extract(business_capability_ext, '$.custom.\"Weighted Score\"')) weighted_score,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_ext, '$.custom.\"Artifacts\"')) artifacts,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions, json_unquote(json_extract(business_capability_ext, '$.\"applicable_questions\"')) applicable_questions from enterprise_business_capability where internal_id in (REPLACE_STRING) and business_capability_category = '5'";

      Sheet sheet = wb.createSheet("Requirement List");
      sheet.setColumnWidth(0, 9000);
      sheet.setColumnWidth(1, 9000);
      sheet.setColumnWidth(3, 15000);
      sheet.setColumnWidth(4, 15000);

      Font headerFont = wb.createFont();
      headerFont.setBold(true);
      headerFont.setFontHeightInPoints((short) 14);
      headerFont.setColor(IndexedColors.RED.getIndex());

      CellStyle headerCellStyle = wb.createCellStyle();
      headerCellStyle.setFont(headerFont);
      headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
      headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

      // Create a Row
      int rowNum = 0;
      Row headerRow = sheet.createRow(rowNum);

      for (int i = 0; i < columns.length; i++) {
         Cell cell = headerRow.createCell(i);
         cell.setCellValue(columns[i]);
         cell.setCellStyle(headerCellStyle);
      }

      CellStyle cs1 = wb.createCellStyle();
      cs1.setAlignment(HorizontalAlignment.CENTER);
      cs1.setVerticalAlignment(VerticalAlignment.CENTER);
      cs1.setWrapText(true);

      CellStyle cs2 = wb.createCellStyle();
      cs2.setWrapText(true);

      PreparedStatement pst = null;
      if(auditFlag.equalsIgnoreCase("Y")) {
         pst = conn.prepareStatement(controlauditsql);
      } else {
         if(controlList.size() == 0) {
            pst = conn.prepareStatement(controlsql);
         } else {
            String filter = controlList.toString();
            filter = filter.replaceAll("[\\[\\]]", "");
            controlfiltersql = controlfiltersql.replaceAll("REPLACE_STRING", filter);
            pst = conn.prepareStatement(controlfiltersql);
         }
      }
      pst.setString(1, domain);
      //System.out.println(pst.toString());
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String controlId = rs.getString("id");
         String parentId = rs.getString("parent_capability_id");
         String internalId = rs.getString("internal_id");
         String familyName = rs.getString("family");
         String parentName = rs.getString("parent_capability_name");
         String childName = rs.getString("child_capability_name");
         String currentMaturity = rs.getString("current_maturity");
         String level = rs.getString("business_capability_level");
         String score = rs.getString("weighted_score");
         String notes = rs.getString("business_capability_comment");
         String artifacts = rs.getString("artifacts");
         String nistMapping = rs.getString("nist_mapping");
         String standard = rs.getString("standard");
         String questions = rs.getString("questions");
         String applicableQuestions = rs.getString("applicable_questions");
         String requirements = rs.getString("requirements");

         if(requirementFramework != null && requirementFramework.length() > 0) {
            if(requirements != null && requirements.length() > 0) {
               OrderedJSONObject reqObj = new OrderedJSONObject(requirements);
               if(reqObj.has("requirements")) {
                  JSONArray reqArr = (JSONArray)reqObj.get("requirements");
                  if(reqArr.size() > 0) {
                     String reqs = reqArr.toString();
                     reqs = reqs.replaceAll("[\\[\\]]", "");
                     String reqsql = controlrequirementsql;
                     reqsql = reqsql.replaceAll("REPLACE_STRING", reqs);
                     PreparedStatement pst4 = conn.prepareStatement(reqsql);
                     System.out.println("Requirements:"+pst4.toString());
                     ResultSet rs4 = pst4.executeQuery();
                     while(rs4.next()) {
                        String rcontrolId = rs4.getString("id");
                        String rparentId = rs4.getString("parent_capability_id");
                        String rinternalId = rs4.getString("internal_id");
                        String rfamilyName = rs4.getString("family");
                        String rparentName = rs4.getString("parent_capability_name");
                        String rchildName = rs4.getString("child_capability_name");
                        String rcurrentMaturity = rs4.getString("current_maturity");
                        String rlevel = rs4.getString("business_capability_level");
                        String rscore = rs4.getString("weighted_score");
                        String rnotes = rs4.getString("business_capability_comment");
                        String rartifacts = rs4.getString("artifacts");
                        String rnistMapping = rs4.getString("nist_mapping");
                        String rstandard = rs4.getString("standard");
                        String rquestions = rs4.getString("questions");
                        String rapplicableQuestions = rs4.getString("applicable_questions");

                        JSONArray rqList = null;
                        if(rquestions != null && rquestions.length() > 0) {
                              rqList = new JSONArray(rquestions);
                        } else {
                              rqList = new JSONArray();
                        }

                        OrderedJSONObject raqList = null;
                        if(rapplicableQuestions != null && rapplicableQuestions.length() > 0) {
                              raqList = new OrderedJSONObject(rapplicableQuestions);
                        } else {
                              raqList = new OrderedJSONObject();
                        }

                        // Get Parent Questions
                        JSONArray rparentQuestionList = getParentQuestionList(rparentId);
                        if(raqList.size() == 0) {    // Add all parent questions, if aqList is empty
                              rqList.addAll(rparentQuestionList);
                        } else {
                              Iterator iter = raqList.keySet().iterator();
                              while(iter.hasNext()) {
                                 String qId = (String)iter.next();
                                 String aFlag = (String)raqList.get(qId);
                                 if(aFlag == null || aFlag.length() == 0 || aFlag.equalsIgnoreCase("Y")) {
                                    Iterator parentIter = rparentQuestionList.iterator();
                                    while(parentIter.hasNext()) {
                                          OrderedJSONObject qObj = (OrderedJSONObject)parentIter.next();
                                          String pqId = (String)qObj.get("id");
                                          if(pqId.equalsIgnoreCase(qId)) {
                                             rqList.add(qObj);
                                          }
                                    }
                                 }
                              }
                        }

                        rowNum++;
                        int startRow = rowNum;
                        Row row = sheet.createRow(rowNum);
                        Cell cell0 = row.createCell(0);
                        cell0.setCellValue(familyName);
                        cell0.setCellStyle(cs1);
                        Cell cell1 = row.createCell(1);
                        cell1.setCellValue(parentName);
                        cell1.setCellStyle(cs1);
                        Cell cell2 = row.createCell(2);
                        cell2.setCellValue(rchildName+"\n"+rinternalId);
                        cell2.setCellStyle(cs2);
                        
                        if(rqList.size() == 0) {
                           Cell cell3 = row.createCell(3);
                           cell2.setCellValue("");
                           cell2.setCellStyle(cs2);
                        } else {
                           for(int i=0; i<rqList.size(); i++) {
                              JSONObject qObj = (JSONObject)rqList.get(i);
                              if(qObj.has("question")) {
                                 String q = (String)qObj.get("question");
                                 if(i > 0) {
                                    row = sheet.createRow(rowNum);
                                 }
                                 Cell cell3 = row.createCell(3);
                                 cell3.setCellValue(q);
                                 cell3.setCellStyle(cs2);
                                 rowNum++;
                              }
                           }
                        }
                       
                        int endRow = rowNum;
                        sheet.addMergedRegion(new CellRangeAddress(startRow,endRow,0,0));
                        sheet.addMergedRegion(new CellRangeAddress(startRow,endRow,1,1));
                     }
                     rs4.close();
                     pst4.close();
                  }
               }
            } else {
               rowNum++;
               Row row = sheet.createRow(rowNum);
               Cell cell0 = row.createCell(0);
               cell0.setCellValue(familyName);
               cell0.setCellStyle(cs1);
               Cell cell1 = row.createCell(1);
               cell1.setCellValue(parentName);
               cell1.setCellStyle(cs1);
               Cell cell2 = row.createCell(2);
               cell2.setCellValue("");
               cell2.setCellStyle(cs2);
               Cell cell3 = row.createCell(3);
               cell3.setCellValue("");
               cell3.setCellStyle(cs2);
            }
         } else if(level.equals("3")) {
            JSONArray qList = null;
            if(questions != null && questions.length() > 0) {
               qList = new JSONArray(questions);
            } else {
               qList = new JSONArray();
            }

            OrderedJSONObject aqList = null;
            if(applicableQuestions != null && applicableQuestions.length() > 0) {
               aqList = new OrderedJSONObject(applicableQuestions);
            } else {
               aqList = new OrderedJSONObject();
            }

            // Get Parent Questions
            JSONArray parentQuestionList = getParentQuestionList(parentId);
            if(aqList.size() == 0) {    // Add all parent questions, if aqList is empty
               qList.addAll(parentQuestionList);
            } else {
               Iterator iter = aqList.keySet().iterator();
               while(iter.hasNext()) {
                     String qId = (String)iter.next();
                     String aFlag = (String)aqList.get(qId);
                     if(aFlag == null || aFlag.length() == 0 || aFlag.equalsIgnoreCase("Y")) {
                        Iterator parentIter = parentQuestionList.iterator();
                        while(parentIter.hasNext()) {
                           OrderedJSONObject qObj = (OrderedJSONObject)parentIter.next();
                           String pqId = (String)qObj.get("id");
                           if(pqId.equalsIgnoreCase(qId)) {
                              qList.add(qObj);
                           }
                        }
                     }
               }
            }
            System.out.println(rowNum+":"+parentName+":"+childName+":"+qList.size());
            rowNum++;
            int startRow = rowNum;
            Row row = sheet.createRow(rowNum);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue(familyName);
            cell0.setCellStyle(cs1);
            Cell cell1 = row.createCell(1);
            cell1.setCellValue(parentName);
            cell1.setCellStyle(cs1);
            Cell cell2 = row.createCell(2);
            cell2.setCellValue(childName+"\n"+internalId);
            cell2.setCellStyle(cs2);

            if(qList.size() == 0) {
               Cell cell3 = row.createCell(3);
               cell2.setCellValue("");
               cell2.setCellStyle(cs2);
            } else {
               for(int i=0; i<qList.size(); i++) {
                  JSONObject qObj = (JSONObject)qList.get(i);
                  if(qObj.has("question")) {
                     String q = (String)qObj.get("question");
                     if(i > 0) {
                        row = sheet.createRow(rowNum);
                     }
                     Cell cell3 = row.createCell(3);
                     cell3.setCellValue(q);
                     cell3.setCellStyle(cs2);
                     rowNum++;
                  }
               }
            }
           
            int endRow = rowNum;
            sheet.addMergedRegion(new CellRangeAddress(startRow,endRow,0,0));
            sheet.addMergedRegion(new CellRangeAddress(startRow,endRow,1,1));
         }
      }
      rs.close();
      pst.close();
      /*
      for (int i = 0; i < columns.length; i++) {
         sheet.autoSizeColumn(i);
      }
      */
      System.out.println("Successfully created Requirement List!!!");
   }

   private JSONArray getParentQuestionList(String id) throws Exception {
        String parentquestionsql = "select parent_capability_id, json_unquote(json_extract(business_capability_ext, '$.\"questions\"')) questions from enterprise_business_capability where id = ? and business_capability_level > 0";
        JSONArray qList = new JSONArray();
        String parentId = null;
        PreparedStatement pst = conn.prepareStatement(parentquestionsql);
        pst.setString(1, id);
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            parentId = rs.getString("parent_capability_id");
            String questions = rs.getString("questions");
            if(questions != null && questions.length() > 0) {
                JSONArray list = new JSONArray(questions);
                qList.addAll(list);
            }
        }
        rs.close();
        pst.close();

        if(parentId != null && parentId.length() > 0) {
            JSONArray list = getParentQuestionList(parentId);
            qList.addAll(list);
        }

        return qList;
   }

   private void setBorder(Sheet sheet, BorderStyle borderStyle, CellRangeAddress region) {
      RegionUtil.setBorderTop(borderStyle, region, sheet);
      RegionUtil.setBorderBottom(borderStyle, region, sheet);
      RegionUtil.setBorderLeft(borderStyle, region, sheet);
      RegionUtil.setBorderRight(borderStyle, region, sheet);
   }

   private void mysqlConnect(String dbname) throws Exception {
      String userName = "root", password = "smarterD2018!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
   
   public static void main(String args[]) throws Exception {
		String companyCode = args[0];
		String env = args[1];
      JSONArray controlList = new JSONArray();
      String domain = "NIST CSF";
      
      SmarteXSLX xslx = new SmarteXSLX(companyCode, env);
      xslx.generateAssessmentList(domain, controlList);
   }
}