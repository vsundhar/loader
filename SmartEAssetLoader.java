import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class SmartEAssetLoader {
	private CSVReader schemaReader = null;
	private CSVReader dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public SmartEAssetLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		schemaReader = new CSVReader(new FileReader(new File(filename+"_schema.csv")), ',', '"', 0);
		dataReader = new CSVReader(new FileReader(new File(filename+"_data.csv")), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		OrderedJSONObject schemaList = new OrderedJSONObject();
		OrderedJSONObject uniqueList = new OrderedJSONObject();
		// Read Schema File
		String[] schemaRow;
		int rowCount=0;
		while ((schemaRow = schemaReader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				String sourceColumnName=null, columnName=null, exclude=null, unique=null, columnType=null, dataType=null, rule=null, comment=null;
				for(int i=0; i<schemaRow.length; i++) {
					if(i==0) {			
						sourceColumnName = schemaRow[i].trim();
					} else if(i==1) {
						columnName = schemaRow[i].trim();
					} else if(i==2) {
						exclude = schemaRow[i].trim();
					} else if(i==3) {
						unique = schemaRow[i].trim();
					} else if(i==4) {
						columnType = schemaRow[i].trim();
					} else if(i==5) {
						dataType = schemaRow[i].trim();
					} else if(i==6) {
						rule = schemaRow[i].trim();
					} else if(i==7) {
						comment = schemaRow[i].trim();
						comment = comment.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
					}
                }
				
				OrderedJSONObject ruleObj = null;
				if(rule != null && rule.trim().length() > 0){
					ruleObj = new OrderedJSONObject(rule);
				} else {
					ruleObj = new OrderedJSONObject();
				}

				// Create Schema Object
				if(exclude.equalsIgnoreCase("N")) {
					OrderedJSONObject obj = new OrderedJSONObject();
					obj.put("column_name", columnName);
					obj.put("column_type", columnType);
					obj.put("data_type", dataType);
					obj.put("rule", ruleObj);
					obj.put("comment", comment);
					schemaList.put(sourceColumnName, obj);

					if(unique.equalsIgnoreCase("Y")) {
						uniqueList.put(sourceColumnName, obj);
					}
				}
			}
		}
		System.out.println(schemaList.toString());

		// Read Data File
		String[] dataRow;
		rowCount=0;
		OrderedJSONObject columnList = new OrderedJSONObject();
		OrderedJSONObject columnValueList = new OrderedJSONObject();
		while((dataRow = dataReader.readNext()) != null) {
			rowCount++;
			if(rowCount == 1) {
				for(int i=0; i<dataRow.length; i++) {
					String columnName = dataRow[i].trim();
					columnList.put(String.valueOf(i), columnName);
                }
				System.out.println(columnList.toString());
			} else {
				System.out.println("Processing row-" + rowCount + "...");
				for(int i=0; i<dataRow.length; i++) {
					String columnValue = dataRow[i].trim();

					// Prepare value for SQL Load
					columnValue = columnValue.replaceAll("'", "''"); 
					columnValue = columnValue.replaceAll("\"", "'");
					// remove invalid unicode characters
					columnValue = columnValue.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
					// remove other unicode characters
					columnValue = columnValue.replaceAll("[\\uD83D\\uFFFD\\uFE0F\\u203C\\u3010\\u3011\\u300A\\u166D\\u200C\\u202A\\u202C\\u2049\\u20E3\\u300B\\u300C\\u3030\\u065F\\u0099\\u0F3A\\u0F3B\\uF610\\uFFFC]", "");
					if(columnValue.equals("-")) {
						columnValue = "";
					}

					// Store Value
					String columnName = (String)columnList.get(String.valueOf(i));
					columnValueList.put(columnName, columnValue);
                }

				// Check if asset exists
				OrderedJSONObject assetObj = checkAsset(uniqueList, columnValueList);

				// Process Schema
				if(assetObj.size() == 0) {
					System.out.println("Inserting row...");
					insertAsset(schemaList, columnValueList);
				} else {
					System.out.println("Updating row...");
					updateAsset(schemaList, columnValueList, assetObj);
				}
			}

			/*
			if(rowCount == 2) {
				break;
			}
			*/
		}

		conn.commit();
		conn.close();
	}

	private OrderedJSONObject checkAsset(OrderedJSONObject uniqueList, OrderedJSONObject columnValueList) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		String sql = "select * from enterprise_asset where ";
		int count = 0;
		Iterator iter = uniqueList.keySet().iterator();
		while(iter.hasNext()) {
			String sourceColumnName = (String)iter.next();
			OrderedJSONObject schemaObj = (OrderedJSONObject)uniqueList.get(sourceColumnName);
			String columnName = (String)schemaObj.get("column_name");
			String columnValueType = (String)schemaObj.get("data_type");
			String columnValue = (String)columnValueList.get(sourceColumnName);
			if(columnValueType.equalsIgnoreCase("Text")) {
				if(count == 0) {
					sql = sql + columnName + " = '"+columnValue + "'";
				} else {
					sql = sql + " and " + columnName + " = '"+columnValue + "'";
				}
			} else {
				if(count == 0) {
					sql = sql + columnName + " = "+columnValue;
				} else {
					sql = sql + " and " + columnName + " = "+columnValue;
				}
			}
			count++;
		}
		System.out.println(sql);
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for(int i = 0; i < columnCount; i++) {
                String column = rsmd.getColumnName(i+1);
                String value = rs.getString(column);
				if(value != null && value.length() > 0 && value.substring(0, 1).equals("[")) {
					JSONArray valueList = new JSONArray(value);
					res.put(column, valueList);
				} else if(value != null && value.length() > 0 && value.substring(0, 1).equals("{")) {
					System.out.println(value);
					OrderedJSONObject obj = new OrderedJSONObject(value);
					res.put(column, obj);
				} else {
					res.put(column, value);
				}
            }
		}

		System.out.println(res.toString());
		return res;
	}

	private void insertAsset(OrderedJSONObject schemaList, OrderedJSONObject columnValueList) throws Exception {
		System.out.println("In insertAsset...");
		OrderedJSONObject assetObj = new OrderedJSONObject();
		OrderedJSONObject extObj = new OrderedJSONObject();
		OrderedJSONObject customObj = new OrderedJSONObject();
		OrderedJSONObject lifecycleObj = new OrderedJSONObject();
		OrderedJSONObject featureList = new OrderedJSONObject();
		OrderedJSONObject stdruleList = new OrderedJSONObject();

		// Initialize Ext Obj
		if(columnValueList.has("Asset Category")) {
			String category = (String)columnValueList.get("Asset Category");
			if(category.equalsIgnoreCase("APPLICATION")) {
				extObj = initializeApplicationExtension(extObj);
			} else if(category.equalsIgnoreCase("SOFTWARE")) {
				extObj = initializeSoftwareExtension(extObj);
			} else if(category.equalsIgnoreCase("IT ASSET")) {
				extObj = initializeITAssetExtension(extObj);
			}

			// Initialize Asset Object
			assetObj.put("lifecycle_name", "");

			Iterator iter = schemaList.keySet().iterator();
			while(iter.hasNext()) {
				String sourceColumnName = (String)iter.next();
				OrderedJSONObject schemaObj = (OrderedJSONObject)schemaList.get(sourceColumnName);
				String columnName = (String)schemaObj.get("column_name");
				String columnType = (String)schemaObj.get("column_type");
				String columnValue = (String)columnValueList.get(sourceColumnName);
				String columnValueType = (String)schemaObj.get("data_type");
				OrderedJSONObject ruleObj = (OrderedJSONObject)schemaObj.get("rule");

				// Process rules
				if(ruleObj.size() > 0) {
					Iterator ruleIter = ruleObj.keySet().iterator();
					while(ruleIter.hasNext()) {
						String key = (String)ruleIter.next();
						String value = (String)ruleObj.get(key);
						if(key.equalsIgnoreCase("std_rule")) {
							stdruleList.put(value, columnValue);
						} else {
							if(columnValue.equalsIgnoreCase(key)) {
								columnValue = value;
							}
						}
					}
				}
				
				if(columnType.equalsIgnoreCase("Extension")) {
					if(columnValueType.equalsIgnoreCase("Text")) {
						extObj.put(columnName, columnValue);
					} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
						if(extObj.has(columnName)) {
							arr = (JSONArray)extObj.get(columnName);
						} else {
							arr = new JSONArray();
						}
						if(columnValue != null && columnValue.length() > 0) {
							if(!arr.contains(columnValue)) {
								arr.add(columnValue);
							}
						}
						extObj.put(columnName, arr);
					} else if(columnValueType.equalsIgnoreCase("Integer")) {
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0";
							}
							extObj.put(columnName, new Integer(columnValue));
						} else {
							extObj.put(columnName, new Integer(0));
						}
					} else if(columnValueType.equalsIgnoreCase("Decimal")) {
						DecimalFormat df = new DecimalFormat("#");
						df.setMaximumFractionDigits(2);
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0.0";
							}
							BigDecimal d = new BigDecimal(columnValue);
							System.out.println(d);
							extObj.put(columnName, d);
						} else {
							extObj.put(columnName, new Double(0.0));
						}
					} else if(columnValueType.equalsIgnoreCase("Currency")) {
						DecimalFormat df = new DecimalFormat("#");
						df.setMaximumFractionDigits(2);
						System.out.println(columnValue);
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0.0";
							}
							columnValue = columnValue.replaceAll("$", "");
							columnValue = columnValue.replaceAll(",", "");
							BigDecimal d = new BigDecimal(columnValue);
							System.out.println(d);
							extObj.put(columnName, d);
						} else {
							extObj.put(columnName, new Double(0.0));
						}
					} else if(columnValueType.equalsIgnoreCase("Boolean")) {
						if(columnValue != null && columnValue.length() > 0) {
							extObj.put(columnName, new Boolean(columnValue));
						} else {
							extObj.put(columnName, new Boolean(false));
						}
					} 
				} else if(columnType.equalsIgnoreCase("Custom")) {
					if(columnValueType.equalsIgnoreCase("Text")) {
						//columnValue = columnValue.replaceAll("\\", "");
						customObj.put(columnName, columnValue);
					} else if(columnValueType.equalsIgnoreCase("Array")) {
						JSONArray arr = null;
						if(customObj.has(columnName)) {
							arr = (JSONArray)customObj.get(columnName);
						} else {
							arr = new JSONArray();
						}
						if(columnValue != null && columnValue.length() > 0) {
							if(!arr.contains(columnValue)) {
								arr.add(columnValue);
							}
						}
						customObj.put(columnName, arr);
					} else if(columnValueType.equalsIgnoreCase("Integer")) {
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0";
							}
							customObj.put(columnName, new Integer(columnValue));
						} else {
							customObj.put(columnName, new Integer(0));
						}
					} else if(columnValueType.equalsIgnoreCase("Decimal")) {
						DecimalFormat df = new DecimalFormat("#");
						df.setMaximumFractionDigits(2);
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0.0";
							}
							BigDecimal d = new BigDecimal(columnValue);
							System.out.println(d);
							customObj.put(columnName, d);
						} else {
							customObj.put(columnName, new Double(0.0));
						}
					} else if(columnValueType.equalsIgnoreCase("Currency")) {
						DecimalFormat df = new DecimalFormat("#");
						df.setMaximumFractionDigits(2);
						if(columnValue != null && columnValue.length() > 0) {
							if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
								columnValue = "0.0";
							}
							columnValue = columnValue.replaceAll("$", "");
							columnValue = columnValue.replaceAll(",", "");
							BigDecimal d = new BigDecimal(columnValue);
							System.out.println(d);
							customObj.put(columnName, d);
						} else {
							customObj.put(columnName, new Double(0.0));
						}
					} else if(columnValueType.equalsIgnoreCase("Boolean")) {
						if(columnValue != null && columnValue.length() > 0) {
							customObj.put(columnName, new Boolean(columnValue));
						} else {
							customObj.put(columnName, new Boolean(false));
						}
					} 
				} else if(columnType.equalsIgnoreCase("Features")) {
					featureList.put(columnName, columnValue);
				} else if(columnType.equalsIgnoreCase("Lifecycle")) {
					lifecycleObj.put(columnName, columnValue);
				} else if(columnType.equalsIgnoreCase("Core")) {
					assetObj.put(columnName, columnValue);
				}
			}

			// Add Lifecycle Object to List
			JSONArray lifecycleList = new JSONArray();
			lifecycleList.add(lifecycleObj);

			// Add Lifecycle List to Asset Obj
			assetObj.put("asset_lifecycle_list", lifecycleList);

			// Add Custom to Extension 
			extObj.put("custom", customObj);

			// Fix JSON for any special and escape characters
			final String fixedJson = new FixedJson(extObj.toString()).value();
			extObj = new OrderedJSONObject(fixedJson);

			// Add Extension to Asset Object
			assetObj.put("extension", extObj);

			// Generate Id
			String id = generateUUID(); 

			// Add id to Asset Obj
			assetObj.put("id", id);

			// Add Company Code, Updated By
			assetObj.put("company_code", companyCode);
			assetObj.put("created_by", "support@smarterd.com");
			assetObj.put("updated_by", "support@smarterd.com");

			// Form Insert SQL
			String insertsql = "insert into enterprise_asset";
			String columnstr = "", valuestr = "";
			int count = 0;
			iter = assetObj.keySet().iterator();
			while(iter.hasNext()) {
				String columnName = (String)iter.next();
				Object columnValueObj = assetObj.get(columnName);
				System.out.println(columnName+":"+columnValueObj.toString());
				if(count == 0) {
					columnstr = columnName;
					if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
						valuestr = "'"+((JSONArray)columnValueObj).toString()+"'";
					} else if(columnName.equalsIgnoreCase("extension")) {
						valuestr = "'"+((OrderedJSONObject)columnValueObj).toString()+"'";
					} else {
						valuestr = "\""+(String)columnValueObj+"\"";
					}
				} else {
					columnstr = columnstr + "," + columnName;
					if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
						valuestr = valuestr+","+"'"+((JSONArray)columnValueObj).toString()+"'";
					} else if(columnName.equalsIgnoreCase("extension")) {
						valuestr =  valuestr+","+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
					} else {
						valuestr =  valuestr+","+"\""+(String)columnValueObj+"\"";
					}
				}
				count++;
			}
			insertsql = insertsql + "(" + columnstr + ") values(" + valuestr + ")";
			PreparedStatement pst = conn.prepareStatement(insertsql);
			System.out.println(pst.toString());
			pst.executeUpdate();
			pst.close();

			// Process Std Rules
			if(stdruleList.size() > 0) {
				String vendorId = null, contractId = null;
				Iterator ruleIter = stdruleList.keySet().iterator();
				while(ruleIter.hasNext()) {
					String rule = (String)ruleIter.next();
					String value = (String)stdruleList.get(rule);
					if(value != null && value.trim().length() > 0) {
						if(rule.equalsIgnoreCase("CREATE VENDOR")) {
							vendorId = createVendor(value);
						} else if(rule.equalsIgnoreCase("CREATE CONTRACT")) {
							contractId = createContract(id, value);
						} else if(rule.equalsIgnoreCase("CREATE BUSINESS AREA")) {
							createSetting(value, "BUSINESS AREA", "GLOBAL");
						} else if(rule.equalsIgnoreCase("CREATE DEPARTMENT")) {
							createSetting(value, "Department", "GLOBAL");
						}
					}
				}

				// Create Relationship
				if(vendorId != null && contractId != null) {
					createRel(vendorId, contractId, "3RD PARTY", "AGREEMENT", "support@smarterd.com");
				}
			}

			// Add Features
			iter = featureList.keySet().iterator();
			while(iter.hasNext()) {
				String label = (String)iter.next();
				String feature = (String)featureList.get(label);
				createFeatures(id, label, feature);
			}
		} else {
			System.out.println("Skipping:Asset Category not configured!!!");
		}
	}

	private void updateAsset(OrderedJSONObject schemaList, OrderedJSONObject columnValueList, OrderedJSONObject assetObj) throws Exception {
		System.out.println("In updateAsset...");
		String id = (String)assetObj.get("id");
		OrderedJSONObject extObj = (OrderedJSONObject)assetObj.get("extension");
		OrderedJSONObject customObj = null;
		if(extObj.has("custom")) {
			customObj = (OrderedJSONObject)extObj.get("custom");
		} else {
			customObj = new OrderedJSONObject();
		}

		// Make sure single quotes in extobj is escaped - Issue when updating
		// notes is the only field that seem to have text 
		String notes = "";
		if(extObj.has("notes")) {
			notes = (String)extObj.get("notes");
			notes = notes.replaceAll("'", "''");
		}
		extObj.put("notes", notes);

		JSONArray lifecycleList = null;
		OrderedJSONObject lifecycleObj = null;
		if(assetObj.has("asset_lifecycle_list")) {
			lifecycleList = (JSONArray)assetObj.get("asset_lifecycle_list");
			if(lifecycleList.size() > 0) {
				lifecycleObj = new OrderedJSONObject((JSONObject)lifecycleList.get(0));
				lifecycleList.clear();	// Remove Existing Object
			} else{
				lifecycleObj = new OrderedJSONObject();
			}
		} else {
			lifecycleList = new JSONArray();
			lifecycleObj = new OrderedJSONObject();
		}
		OrderedJSONObject featureList = new OrderedJSONObject();
		OrderedJSONObject stdruleList = new OrderedJSONObject();

		Iterator iter = schemaList.keySet().iterator();
		while(iter.hasNext()) {
			String sourceColumnName = (String)iter.next();
			OrderedJSONObject schemaObj = (OrderedJSONObject)schemaList.get(sourceColumnName);
			String columnName = (String)schemaObj.get("column_name");
			String columnType = (String)schemaObj.get("column_type");
			String columnValue = (String)columnValueList.get(sourceColumnName);
			String columnValueType = (String)schemaObj.get("data_type");
			OrderedJSONObject ruleObj = (OrderedJSONObject)schemaObj.get("rule");

			// Process rules
			if(ruleObj.size() > 0) {
				Iterator ruleIter = ruleObj.keySet().iterator();
				while(ruleIter.hasNext()) {
					String key = (String)ruleIter.next();
					String value = (String)ruleObj.get(key);
					if(key.equalsIgnoreCase("std_rule")) {
						stdruleList.put(value, columnValue);
					} else {
						if(columnValue.equalsIgnoreCase(key)) {
							columnValue = value;
						}
					}
				}
			}
			
			if(columnType.equalsIgnoreCase("Extension")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					//columnValue = columnValue.replaceAll("[\\]", "");
					extObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(extObj.has(columnName)) {
						arr = (JSONArray)extObj.get(columnName);
					} else {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							arr.add(columnValue);
						}
					}
					extObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						extObj.put(columnName, new Integer(columnValue));
					} else {
						extObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					System.out.println(columnValue);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						extObj.put(columnName, d);
					} else {
						extObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						extObj.put(columnName, new Boolean(columnValue));
					} else {
						extObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Custom")) {
				if(columnValueType.equalsIgnoreCase("Text")) {
					customObj.put(columnName, columnValue);
				} else if(columnValueType.equalsIgnoreCase("Array")) {
					JSONArray arr = null;
					if(customObj.has(columnName)) {
						arr = (JSONArray)customObj.get(columnName);
					} else {
						arr = new JSONArray();
					}
					if(columnValue != null && columnValue.length() > 0) {
						if(!arr.contains(columnValue)) {
							arr.add(columnValue);
						}
					}
					customObj.put(columnName, arr);
				} else if(columnValueType.equalsIgnoreCase("Integer")) {
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0";
						}
						customObj.put(columnName, new Integer(columnValue));
					} else {
						customObj.put(columnName, new Integer(0));
					}
				} else if(columnValueType.equalsIgnoreCase("Decimal")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Currency")) {
					DecimalFormat df = new DecimalFormat("#");
					df.setMaximumFractionDigits(2);
					if(columnValue != null && columnValue.length() > 0) {
						if(columnValue.equals("-") || columnValue.equalsIgnoreCase("NA") || columnValue.equalsIgnoreCase("N/A")) {
							columnValue = "0.0";
						}
						columnValue = columnValue.replaceAll("$", "");
						columnValue = columnValue.replaceAll(",", "");
						BigDecimal d = new BigDecimal(columnValue);
						System.out.println(d);
						customObj.put(columnName, d);
					} else {
						customObj.put(columnName, new Double(0.0));
					}
				} else if(columnValueType.equalsIgnoreCase("Boolean")) {
					if(columnValue != null && columnValue.length() > 0) {
						customObj.put(columnName, new Boolean(columnValue));
					} else {
						customObj.put(columnName, new Boolean(false));
					}
				} 
			} else if(columnType.equalsIgnoreCase("Features")) {
				featureList.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Lifecycle")) {
				lifecycleObj.put(columnName, columnValue);
			} else if(columnType.equalsIgnoreCase("Core")) {
				assetObj.put(columnName, columnValue);
			}
		}

		// Add Lifecycle Object to List
		lifecycleList.add(lifecycleObj);

		// Add Lifecycle List to Asset Obj
		assetObj.put("asset_lifecycle_list", lifecycleList);

		// Add Custom to Extension 
		extObj.put("custom", customObj);

		// Fix JSON for any special and escape characters
		final String fixedJson = new FixedJson(extObj.toString()).value();
		extObj = new OrderedJSONObject(fixedJson);

		// Add Extension to Asset Object
		assetObj.put("extension", extObj);

		// Add Updated By
		assetObj.put("updated_by", "support@smarterd.com");

		// Form Insert SQL
		String updatesql = "update enterprise_asset set ";
		String columnstr = "", valuestr = "";
		int count = 0;
		iter = assetObj.keySet().iterator();
		while(iter.hasNext()) {
			String columnName = (String)iter.next();
			Object columnValueObj = assetObj.get(columnName);

			System.out.println(columnName);
			if(count == 0) {
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr = columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					String columnValue = "";
					if(columnValueObj != null) {
						columnValue = (String)columnValueObj;
					}
					valuestr = columnName+"="+"'"+columnValue+"'";
				}
			} else {
				if(columnName.equalsIgnoreCase("asset_lifecycle_list")) {
					valuestr = valuestr+","+columnName+"="+"'"+((JSONArray)columnValueObj).toString()+"'";
				} else if(columnName.equalsIgnoreCase("extension")) {
					valuestr =  valuestr+","+columnName+"="+"'"+((OrderedJSONObject)columnValueObj).toString()+"'";
				} else {
					String columnValue = "";
					if(columnValueObj != null) {
						columnValue = (String)columnValueObj;
					}
					valuestr =  valuestr+","+columnName+"="+"\""+(String)columnValue+"\"";
				}
			}
			count++;
		}
		updatesql = updatesql + valuestr + " where id=\""+id+"\"";
		PreparedStatement pst = conn.prepareStatement(updatesql);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();

		// Process Std Rules
		if(stdruleList.size() > 0) {
			String vendorId = null, contractId = null;
			Iterator ruleIter = stdruleList.keySet().iterator();
			while(ruleIter.hasNext()) {
				String rule = (String)ruleIter.next();
				String value = (String)stdruleList.get(rule);
				if(rule.equalsIgnoreCase("CREATE VENDOR")) {
					vendorId = createVendor(value);
				} else if(rule.equalsIgnoreCase("CREATE CONTRACT")) {
					contractId = createContract(id, value);
				} else if(rule.equalsIgnoreCase("CREATE BUSINESS AREA")) {
					createSetting(value, "BUSINESS AREA", "GLOBAL");
				} else if(rule.equalsIgnoreCase("CREATE DEPARTMENT")) {
					createSetting(value, "Department", "GLOBAL");
				}
			}

			// Create Relationship
			if(vendorId != null && contractId != null) {
				createRel(vendorId, contractId, "3RD PARTY", "AGREEMENT", "support@smarterd.com");
			}
		}

		// Add Features
		iter = featureList.keySet().iterator();
		while(iter.hasNext()) {
			String label = (String)iter.next();
			String feature = (String)featureList.get(label);
			createFeatures(id, label, feature);
		}
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private String createVendor(String vendorName) throws Exception {
		String checksql = "select id from enterprise_strategy where enterprise_strategy_name = ? and is_activity = 'N' and plan_type = '3RD PARTY'";
		String insertsql = "insert into enterprise_strategy(id,enterprise_strategy_name,is_activity,plan_type,extension,company_code,created_by,updated_by) values(?,?,'N','3RD PARTY',json_object(),?,'support@smarterd.com','support@smarterd.com')";

		String vendorId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, vendorName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			vendorId = rs.getString("id");
		}
		rs.close();
		pst.close();

		if(vendorId == null) {
			vendorId = generateUUID();
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, vendorId);
			pst.setString(2, vendorName);
			pst.setString(3, companyCode);
			pst.executeUpdate();
			pst.close();
		}

		return vendorId;
	}

	private String createContract(String vendorId, String contractName) throws Exception {
		String checksql = "select id from enterprise_asset where asset_name = ?";
		String insertsql = "insert into enterprise_asset(id,asset_name,asset_type,extension,company_code,created_by,updated_by) values(?,?,'AGREEMENT',?,?,'support@smarterd.com','support@smarterd.com')";

		String contractId = null;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, contractName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			contractId = rs.getString("id");
		}
		rs.close();
		pst.close();

		if(contractId == null) {
			if(vendorId == null) {
				vendorId = "";
			}
			contractId = generateUUID();
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("lifecycle", "Active");
			extObj.put("end_of_life", "");
			extObj.put("Significant?", "");
			extObj.put("Document Name", "");
			extObj.put("signed_by_name", "");
			extObj.put("signed_by_title", "");
			extObj.put("Contracting Entity", new JSONArray());
			extObj.put("primary_business_area", "");
			extObj.put("terminate_convenience", "");
			extObj.put("Amendment/Modification", "");
			extObj.put("lifecycle_auto_renewal", "");
			extObj.put("lifecycle_clause_details", "");
			extObj.put("counterparty_contact_name", "");
			extObj.put("lifecycle_expiration_type", "");
			extObj.put("counterparty_contact_email", "");
			extObj.put("counterparty_contact_phone", "");
			extObj.put("counterparty_contact_title", "");
			extObj.put("lifecycle_termination_notice", "");
			extObj.put("lifecycle_auto_renewal_period", "");
			extObj.put("Ecosystem", new JSONArray());
			extObj.put("vendor_id", vendorId);

			pst = conn.prepareStatement(insertsql);
			pst.setString(1, contractId);
			pst.setString(2, contractName);
			pst.setString(3, extObj.toString());
			pst.setString(4, companyCode);
			pst.executeUpdate();
			pst.close();
		}

		return contractId;
	}

	private void createRel(String componentId, String assetId, String componentType, String assetType, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}
    }

	private void createSetting(String name, String category, String type) throws Exception {
        System.out.println("Loading Setting Category..." + name);
		String checkSQL = "select id from enterprise_setting where setting_name=? and setting_category=? and setting_type=?";
		String insertSQL = "insert into enterprise_setting(id,setting_name,setting_desc,setting_category,setting_type,setting_value,updated_by,updated_timestamp,company_code) value(null,?,?,?,?,null,'support@smarterd.com',CURRENT_TIMESTAMP,?)";

        // Check if name already exists
		int settingId = 0;
        PreparedStatement pst = conn.prepareStatement(checkSQL);
        pst.setString(1, name.trim());
        pst.setString(2, category.trim());
		pst.setString(3, type);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            settingId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Setting Found:" + settingId);

        if (settingId == 0) {
            pst = conn.prepareStatement(insertSQL);
            pst.setString(1, name.trim());
            pst.setString(2, null);
            pst.setString(3, category);
            pst.setString(4, type);
            pst.setString(5, companyCode);

            pst.executeUpdate();
            pst.close();

            System.out.println("Successfully inserted new setting:" + settingId + ":" + name);
        } else {
            System.out.println("Setting already exists:" + name);
        }
	}

	private void createFeatures(String assetId, String label, String feature) throws Exception {
		String checksql = "select count(*) count from enterprise_comment where TOPIC_NAME = ? and comp_id = ?";
		String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,comment_type,extension,status,CREATED_BY,updated_by,COMPANY_CODE) values(null,?,?,?,'Feature',?,?,'support@smarterd.com','support@smarterd.com',?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, label);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			OrderedJSONObject extObj = new OrderedJSONObject();
			extObj.put("component_type", "ASSET");
			OrderedJSONObject statusObj = new OrderedJSONObject();
			statusObj.put("userGenerated", new Boolean(true));
			statusObj.put("applicable", "");
			statusObj.put("maturity", "");
			statusObj.put("topiclink", "");
			pst = conn.prepareStatement(insertsql);
			pst.setString(1, assetId);
			pst.setString(2, label);
			pst.setString(3, feature);
			pst.setString(4, extObj.toString());
			pst.setString(5, statusObj.toString());
			pst.setString(6, companyCode);
			pst.executeUpdate();
			pst.close();
		}
	}

	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		SmartEAssetLoader loader = new SmartEAssetLoader(filename, companyCode, env);
		loader.load();
	}
}