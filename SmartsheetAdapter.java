import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.smartsheet.api.*;
import com.smartsheet.api.models.*;
//import com.smartsheet.api.oauth.*;
import java.io.FileInputStream;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class SmartsheetAdapter {
	private String companyCode = null;
	private String username = "vsundhar@smarterd.com";
	//private String username = "jira_integrations@flexport.com";
	//private String seAccessToken = "b2swn68jh4wb04knje55g95vyf";
	private String maurAccessToken = "6b5gxols6ybjojet4urobqlpsg";
	//private String ssUrl = "https://flexport.atlassian.net/";
	private Smartsheet smartsheet = null;
   	
	public SmartsheetAdapter(String companyCode) throws Exception {
		this.companyCode = companyCode;
		smartsheet = SmartsheetFactory.createDefaultClient(maurAccessToken);
		System.out.println("Successfully connected to Smartsheet!!!");
	}
	
    public OrderedJSONObject getSheets() throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		PagedResult<Sheet> sheets = smartsheet.sheetResources().listSheets(null,null,null);

		List list = sheets.getData();
		Iterator iter = list.iterator();
		while(iter.hasNext()) {
			Sheet s = (Sheet)iter.next();
			String id = Long.toString(s.getId());
			String name = s.getName();
			//System.out.println(name);
			//if(!name.startsWith("Test") && !name.startsWith("Copy") && name.endsWith("Project Plan") && !id.equals("8679711405565828")) {
			if((name.startsWith("PSRT") || name.startsWith("PRST")) && name.endsWith("Project Plan") &&  
			!id.equals("8679711405565828") && !id.equals("7396717164291972") && !id.equals("299885170845572") && 
			!id.equals("7976129358260100") && !id.equals("8059264355854212")) {
				System.out.println(id+":"+name);
				res.put(id, name);
			}
		}

		return res;
	}

	public OrderedJSONObject getSheetDetails(long id) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		HashMap<Long, String> columnMap = new HashMap<Long, String>(); 

		Map fieldList = getMappedFields();
		
		Sheet sheet = smartsheet.sheetResources().getSheet(id, null, null, null, null, null, null, null);
		System.out.println("Loaded " + sheet.getRows().size() + " rows from sheet: " + sheet.getName());
		
		for (Column column : sheet.getColumns()) {
			columnMap.put(column.getId(), column.getTitle());
		}

		long rootId = 0;
		for (Row row : sheet.getRows()) {
			long parentId = 0;
			if(row.getParentId() != null) {
				parentId = row.getParentId();
			}
			long siblingId = 0;
			if(row.getSiblingId() != null) {
				siblingId = row.getSiblingId();
			}
			long rowId = row.getId();
			int rowNo = row.getRowNumber();

			if(parentId == 0) {
				rootId = rowId;
			}
			
			System.out.println(rootId+":"+parentId+":"+siblingId+":"+rowId+":"+rowNo);;

			String name=null, desc=null, owner=null, priority=null, stage=null, status=null,startdate=null, enddate=null, assignee=null, completion=null, updatedAt=null, updatedBy=null;
			List cellList = row.getCells();
			Iterator cellIter = cellList.iterator();
			while(cellIter.hasNext()) {
				Cell c = (Cell)cellIter.next();
				Long columnId = c.getColumnId();
				String columnName = (String)columnMap.get(columnId);
				if(fieldList.containsKey(columnName)) {
					String mappedName = (String)fieldList.get(columnName);
					if(mappedName.equalsIgnoreCase("name")) {
						name = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("description")) {
						desc = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("owner")) {
						owner = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("priority")) {
						priority = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("implementation")) {
						stage = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("status")) {
						status = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("startDate")) {
						startdate = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("endDate")) {
						enddate = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("assignee")) {
						assignee = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("completion")) {
						if(c.getValue() != null) {
							int d = (int)Math.round(((Double)c.getValue()).doubleValue() * 100);
							completion = String.valueOf(d);
						} else {
							completion = "";
						}
					} else if(mappedName.equalsIgnoreCase("updatedAt")) {
						updatedAt = (String)c.getValue();
					} else if(mappedName.equalsIgnoreCase("updatedBy")) {
						updatedBy = (String)c.getValue();
					}
				}
			}
			//System.out.println(name+":"+desc+":"+owner+":"+priority+":"+stage+":"+status+":"+startdate+":"+enddate+":"+assignee+":"+completion+":"+updatedAt+":"+updatedBy);
			System.out.println(name+":"+desc+":"+stage+":"+status+":"+startdate+":"+enddate+":"+assignee+":"+completion+":"+updatedAt+":"+updatedBy);
			if(parentId == 0) {
				System.out.println("Insert Plan");
			} else if(rootId == parentId) {
				System.out.println("Insert SubPlan with root TRUE");
			} else if(rootId != parentId) {
				System.out.println("Insert SubPlan with root FASLE");
			}
		}

		return res;
	}

	public OrderedJSONObject getMAURSheetDetails(long id, String planName) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		HashMap<Long, String> columnMap = new HashMap<Long, String>(); 
		Map fieldList = getMappedFields();
		
		Sheet sheet = smartsheet.sheetResources().getSheet(id, null, null, null, null, null, null, null);
		String sheetName = sheet.getName().trim();
		List rowList = sheet.getRows();
		int rowCount = rowList.size();
	
		for (Column column : sheet.getColumns()) {
			columnMap.put(column.getId(), column.getTitle());
		}
		System.out.println(columnMap.toString());
		int startRow = 0;
		Iterator rowIter = rowList.iterator();
		while(rowIter.hasNext()) {
			Row row = (Row)rowIter.next();
			List cellList = row.getCells();
			Iterator cellIter = cellList.iterator();
			while(cellIter.hasNext()) {
				Cell c = (Cell)cellIter.next();
				Long columnId = c.getColumnId();
				String columnName = (String)columnMap.get(columnId);
				if(fieldList.containsKey(columnName)) {
					String mappedName = (String)fieldList.get(columnName);
					if(mappedName.equalsIgnoreCase("name")) {
						if(c.getValue() != null) {
							String colname = ((String)c.getValue()).trim();
							if(colname.equalsIgnoreCase(planName)) {
								startRow = row.getRowNumber();
								break;
							}
						}
					}
				}
			}
			if(startRow > 0) {
				break;
			}
		}
		System.out.println("-------------------------");
		System.out.println(sheetName+":"+planName+":"+startRow);

		if(startRow > 0) {
			long rootId = 0;
			rowIter = rowList.iterator();
			while(rowIter.hasNext()) {
				Row row = (Row)rowIter.next();
				int rowNo = row.getRowNumber();
			
				if(rowNo >= startRow) {
					long parentId = 0;
					if(row.getParentId() != null) {
						parentId = row.getParentId();
					}
					long siblingId = 0;
					if(row.getSiblingId() != null) {
						siblingId = row.getSiblingId();
					}
					long rowId = row.getId();

					if(parentId == 0) {
						rootId = rowId;
					}

					String name=null, desc=null, owner=null, priority=null, stage=null, status=null,startdate=null, enddate=null, assignee=null, completion=null, updatedAt=null, updatedBy=null;
					boolean validRow = true;
					List cellList = row.getCells();
					Iterator cellIter = cellList.iterator();
					while(cellIter.hasNext()) {
						Cell c = (Cell)cellIter.next();
						Long columnId = c.getColumnId();
						String columnName = (String)columnMap.get(columnId);
						if(fieldList.containsKey(columnName)) {
							String mappedName = (String)fieldList.get(columnName);
							if(mappedName.equalsIgnoreCase("name")) {
								name = (String)c.getValue();
								if(name == null) {
									validRow = false;
									break;
								}
							} else if(mappedName.equalsIgnoreCase("description")) {
								desc = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("owner")) {
								owner = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("priority")) {
								priority = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("implementation")) {
								stage = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("status")) {
								status = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("startDate")) {
								startdate = (String)c.getValue();
								if(startdate != null && startdate.length() > 0) {
									//System.out.println(startdate);
									String format = getDateFormat(startdate);
									if(format.equals("iso")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd");
										Date dt = df.parse(startdate);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										startdate=sdf.format(dt);
									} else if(format.equals("iso 8601")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
										Date dt = df.parse(startdate);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										startdate=sdf.format(dt);
									} else {
										startdate = "";
									}
								}
							} else if(mappedName.equalsIgnoreCase("endDate")) {
								enddate = (String)c.getValue();
								if(enddate != null && enddate.length() > 0) {
									String format = getDateFormat(enddate);
									if(format.equals("iso")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd");
										Date dt = df.parse(enddate);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										enddate=sdf.format(dt);
									} else if(format.equals("iso 8601")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
										Date dt = df.parse(enddate);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										enddate=sdf.format(dt);
									} else {
										enddate = "";
									}
								}
							} else if(mappedName.equalsIgnoreCase("assignee")) {
								assignee = (String)c.getValue();
							} else if(mappedName.equalsIgnoreCase("completion")) {
								if(c.getValue() != null) {
									int d = (int)Math.round(((Double)c.getValue()).doubleValue() * 100);
									completion = String.valueOf(d);
								} else {
									completion = "";
								}
								System.out.println(completion);
							} else if(mappedName.equalsIgnoreCase("updatedAt")) {
								updatedAt = (String)c.getValue();
								if(updatedAt != null && updatedAt.length() > 0) {
									String format = getDateFormat(updatedAt);
									if(format.equals("iso")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd");
										Date dt = df.parse(updatedAt);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										updatedAt=sdf.format(dt);
									} else if(format.equals("iso 8601")) {
										SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
										Date dt = df.parse(updatedAt);
										SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
										updatedAt=sdf.format(dt);
									} else {
										updatedAt = "";
									}
								}
							} else if(mappedName.equalsIgnoreCase("updatedBy")) {
								updatedBy = (String)c.getValue();
							}
						}
					}
					if(validRow) {
						//System.out.println(rootId+":"+parentId+":"+siblingId+":"+rowId+":"+rowNo);;
						//System.out.println(name+":"+desc+":"+stage+":"+status+":"+startdate+":"+enddate+":"+assignee+":"+completion+":"+updatedAt+":"+updatedBy);
						/*
						System.out.println(rowId+":"+name);
						if(parentId == 0) {
							System.out.println("Insert Plan");
						} else if(rootId == parentId) {
							System.out.println("Insert SubPlan with root TRUE");
						} else if(rootId != parentId) {
							System.out.println("Insert SubPlan with root FASLE");
						}
						*/
					}
				}
			}
		} else {
			System.out.println("SheetName and Start Row Name not Matching:"+id+":"+sheetName+":"+planName);
		}

		return res;
	}

	public Map getMappedNames(Map list) throws Exception {
		Map names = new HashMap();
		Iterator iter = list.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			String name = (String)list.get(key);
			if(key.equals("8070030597351300")) {
				names.put(name, "PSRT-01 Finance Project Plan");
			} else if(key.equals("3433596221450116")) {
				names.put(name, "PSRT-02 HR Project Plan");
			} else if(key.equals("2858620359600004")) {
				names.put(name, "PSRT-03,05-07 Infrastructure Project Plan");
			} else if(key.equals("7976129358260100")) {
				names.put(name, "PSRT-00 PMO Project Plan");
			} else if(key.equals("8059264355854212")) {
				names.put(name, "PSRT-13 Merchandising Project Plan");
			} else if(key.equals("7470728040736644")) {
				names.put(name, "PSRT-13 Merchandising / MP&A Project Plan");
			} else if(key.equals("1732170696943492")) {
				names.put(name, "PSRT-18 Integrations Project Plan");
			} else if(key.equals("299885170845572")) {
				names.put(name, "PSRT-03,05-07 Infrastructure Project Plan");
			}
		}

		return names;
	}

	private Map getMappedFields() throws Exception {
		Map fieldList = new HashMap();
		/*
		fieldList.put("Task Name", "name");
		fieldList.put("Notes", "description");
		fieldList.put("Owner", "owner");
		fieldList.put("Priority", "priority");
		fieldList.put("Stage", "implementation");
		fieldList.put("Status", "status");
		fieldList.put("Start Date", "startDate");
		fieldList.put("End Date", "endDate");
		fieldList.put("Assigned To", "assignee");
		fieldList.put("% Complete", "completion");
		*/

		fieldList.put("Primary", "name");
		fieldList.put("Details", "description");
		fieldList.put("Priority", "priority");
		fieldList.put("Health", "status");
		fieldList.put("Status", "implementation");
		fieldList.put("Start Date", "startDate");
		fieldList.put("Due Date", "endDate");
		fieldList.put("Assignee", "assignee");
		fieldList.put("% Complete", "completion");
		fieldList.put("Modified", "updatedAt");
		fieldList.put("Modified By", "updatedBy");

		return fieldList;
	}

	private String getDateFormat(String d) throws Exception {
		String isoFormat = "^((19|2[0-9])[0-9]{2})([- /.])(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
		String usFormat = "^(((0[13578]|(10|12))/(0[1-9]|[1-2][0-9]|3[0-1]))|(02/(0[1-9]|[1-2][0-9]))|((0[469]|11)/(0[1-9]|[1-2][0-9]|30)))/[0-9]{4}$";
		String format = "";

		if(d.matches(isoFormat)) {
			format = "iso";
		} else if(d.matches(usFormat)) {
			format = "us";
		} else if(d.indexOf("T") != -1) {
			format = "iso 8601";
		}

		return format;
	}
	
	public static void main(String[] args) throws Exception {
		String companyCode = args[0];
		SmartsheetAdapter adapter = new SmartsheetAdapter(companyCode);
		OrderedJSONObject res = adapter.getSheets();
		//System.out.println(res.toString());
		
		String id = "8070030597351300";
		adapter.getMAURSheetDetails(Long.parseLong(id), "PSRT-01 Finance Project Plan");
		
		/*
		int count = 0;
		Map names = adapter.getMappedNames(res);
		Iterator iter = res.keySet().iterator();
		while(iter.hasNext()) {
			String id = (String)iter.next();
			String name = (String)res.get(id);
			if(names.containsKey(name)) {
				name = (String)names.get(name);
			}
			adapter.getMAURSheetDetails(Long.parseLong(id), name);
			count ++;
		}
		*/
	}
}