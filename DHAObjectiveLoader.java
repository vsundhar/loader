import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class DHAObjectiveLoader {
	private CSVReader dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public DHAObjectiveLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = new CSVReader(new FileReader(new File(filename+"_data.csv")), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String[] dataRow;
		int rowCount=0;
		while ((dataRow = dataReader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				String assetName=null, internalId=null, objective=null;
				for(int i=0; i<dataRow.length; i++) {
					if(i==0) {			
					} else if(i==1) {			
						assetName = dataRow[i].trim();
					} else if(i==2) {
						internalId = dataRow[i].trim();
					} else if(i==3) {
						objective = dataRow[i].trim();
						objective = objective.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
					} 
                }	

				if(objective.length() > 0) {
					// Get Asset Id
					String assetId = null;
					String checksql = "select id from enterprise_asset where asset_name = ?";
					PreparedStatement pst = conn.prepareStatement(checksql);
					pst.setString(1, assetName);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						assetId = rs.getString("id");
					}
					rs.close();
					pst.close();

					if(assetId == null) {
						String insertsql = "insert into enterprise_asset(id,asset_name,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
						assetId = generateUUID();
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj = initializeApplicationExtension(extObj);
						extObj.put("custom", new OrderedJSONObject());
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, assetId);
						pst.setString(2, assetName);
						pst.setString(3, extObj.toString());
						pst.setString(4, companyCode);
						pst.executeUpdate();
						pst.close();
					}

					// Check if Objective exists
					String strategyId = null;
					checksql = "select id from enterprise_business_strategy where business_strategy_name = ?";
					pst = conn.prepareStatement(checksql);
					pst.setString(1, objective);
					rs = pst.executeQuery();
					while(rs.next()) {
						strategyId = rs.getString("id");
					}
					rs.close();
					pst.close();

					if(strategyId == null) {
						String insertsql = "insert into enterprise_business_strategy(id, business_strategy_name, internal_id, strategy_type, extension, created_by, updated_by, company_code) values(?,?,?,'STRATEGY',json_object(),'support@smarterd.com','support@smarterd.com',?)";
						strategyId = generateUUID();
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, strategyId);
						pst.setString(2, objective);
						pst.setString(3, internalId);
						pst.setString(4, companyCode);
						pst.executeUpdate();
						pst.close();
					}

					createRel(strategyId, assetId, "STRATEGY", "ASSET", ctx, "support@smarterd.com");
				}
			}	
		}

		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private void createRel(String componentId, String assetId, String componentType, String assetType, OrderedJSONObject ctx, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";

		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}
    }


	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		DHAObjectiveLoader loader = new DHAObjectiveLoader(filename, companyCode, env);
		loader.load();
	}
}