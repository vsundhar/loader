import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class COBITLoader {
	private String domain = "COBIT 2019";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
	String cobitArea=null, cobitDomain=null, objective = null;
	String family = null; 
    
	public COBITLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;

		if(env.equalsIgnoreCase("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equalsIgnoreCase("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}

		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
        System.out.println("Successfully read data file:" + filename);
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0, levelOne = 0, levelTwo = 0;
		String parentInternalId=null, parentCapability=null, parentDesc=null, internalId=null, childCapability=null, desc = null;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
			
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						if(row[i].trim().length() > 0) {	
							cobitArea = row[i];
						}
					} else if(i==1) {
						if(row[i].trim().length() > 0) {
							cobitDomain = row[i];
						}
					} else if(i==2) {
						parentInternalId = row[i].trim();
					} else if(i==3) {
						if(row[i].trim().length() > 0) {
							parentCapability = row[i].trim();
						}
					} else if(i==4) {
						if(row[i].trim().length() > 0) {
							parentDesc = row[i].trim();
						}
					} else if(i==5) {
						if(row[i].trim().length() > 0) {
							objective = row[i].trim();
						}
					} else if(i==6) {
						internalId = row[i].trim();
					} else if(i==7) {
						childCapability = row[i].trim();
					} else if(i==8) {
						desc = row[i].trim();
					}
				}
					
				System.out.println(parentInternalId+":"+parentCapability+":"+childCapability+":"+desc+":"+internalId);
				
				// Check Capability
				String parentId = null, childId=null;
				// Check Parent
				if(capabilityList.has(parentInternalId)) {		
					parentId = (String)capabilityList.get(parentInternalId);
					childId = generateUUID();
					insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, "2", levelTwo);
					levelTwo++;
				} else {
					parentId = generateUUID();
					childId = generateUUID();
					levelTwo = 0;
					family = parentCapability;
					insertCapability(parentId, rootId, parentInternalId, domain, parentCapability, "", "1", levelOne);
					insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, "2", levelTwo);
					capabilityList.put(parentInternalId, parentId);
					
					levelOne++;
					levelTwo++;
				}
				
				System.out.println(parentId+":"+parentCapability+":"+childId+":"+childCapability);
			}
		}
		
		conn.commit();
		conn.close();
	}

	private void loadActivity() throws Exception {
		int rowCount=0;
		String[] row;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				String practiceId = null, name = null; 
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						practiceId = row[i].trim();
					} else if(i==1) {
						name = row[i].trim();
					}
				}

				String activityId = name.substring(0, name.indexOf(".")).trim();
				name = name.substring(name.indexOf(".")+1).trim();
				String capabilityCheckSQL = "select id,child_capability_name,json_unquote(json_extract(business_capability_ext, '$.family')) family, json_unquote(json_extract(business_capability_ext, '$.securityFunction')) sf, json_unquote(json_extract(business_capability_ext, '$.custom.Area')) area from enterprise_business_capability where  internal_id = ? and business_capability_domain = ?";
				PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
				pst.setString(1, practiceId);
				pst.setString(2, domain);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					String parentId = rs.getString("id");
					String parentName = rs.getString("child_capability_name");
					family = rs.getString("family"); 
					cobitDomain = rs.getString("sf");
					cobitArea = rs.getString("area");
					String childId = generateUUID();
					insertCapability(childId, parentId, activityId, parentName, name, "", "3", rowCount);
				}
				rs.close();
				pst.close();
			}
		}

		conn.commit();
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability(domain, domain);
		
		if(rootId == null || rootId.length() == 0) {
			rootId = domain;
			insertCapability(rootId, rootId, domain, domain, domain, "COBIT Governance and Management Objectives", "0", 0);
			capabilityList.put(domain, domain);
		}
		
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}

	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id,child_capability_name from enterprise_business_capability where  internal_id = ? and business_capability_domain = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		pst.setString(2, domain);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String owner = "", status = "Y";
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		OrderedJSONObject customObj = new OrderedJSONObject();
		customObj.put("Area", cobitArea);

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", family);
		extObject.put("assetClass", new JSONArray());
		extObject.put("ecosystem", new JSONArray());
		extObject.put("securityFunction", cobitDomain);
		extObject.put("custom", customObj);
		extObject.put("priority", "");
		if(level.equals("1")) {
			extObject.put("objective", objective);
		}
		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
	}
	
	private void mysqlConnect(String dbname) throws Exception {
     	String userName = "peapi", password = "smartEuser2022!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		COBITLoader loader = new COBITLoader(filename, companyCode, env);
		//loader.process();
		loader.loadActivity();
	}
}