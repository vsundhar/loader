import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.DayOfWeek;

public class MAUREnhancementLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public MAUREnhancementLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String[] row;
		Map rowCountList = new HashMap();
		int rowCount = 0;

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				//System.out.println("Processing row-" + rowCount + "...");
				
				String internalId="",urgent="",priority="",category="",name="",desc="",scope="",benefits="",risks="";
				String resourceReq="",considerations="",requestedBy="",dept="",requestType="",targetDate="",dateSubmitted="";
				String stage="",assignee="",hleHours="",requestedDate="",releaseDate="",completionDate="";
				String managerApproval="",businessPMOApproval="",itPMOApproval="",cioApproval="",hoursApproval="";
				String comment="",statusComment="",owner="",actualHours="";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						internalId = row[i].trim();
					} else if(i==1) {
						urgent = row[i].trim();
					} else if(i==2) {
						priority = row[i].trim();
					} else if(i==3) {
						category = row[i].trim();
					} else if(i==4) {
						name = row[i].trim();
					} else if(i==5) {
						desc = row[i].trim();
					} else if(i==6) {
						scope = row[i].trim();
					} else if(i==7) {
						benefits = row[i].trim();
					} else if(i==8) {
						risks = row[i].trim();
					} else if(i==9) {
						resourceReq = row[i].trim();
					} else if(i==10) {
						considerations = row[i].trim();
					} else if(i==11) {
						requestedBy = row[i].trim();
					} else if(i==12) {
						dept = row[i].trim();
					} else if(i==13) {
						requestType = row[i].trim();
					} else if(i==14) {
						targetDate = row[i].trim();
					} else if(i==15) {
						dateSubmitted = row[i].trim();
					} else if(i==16) {
						owner = row[i].trim();
					} else if(i==17) {
						managerApproval = row[i].trim();
					} else if(i==18) {
						businessPMOApproval = row[i].trim();
					} else if(i==19) {
						itPMOApproval = row[i].trim();
					} else if(i==20) {
						cioApproval = row[i].trim();
					} else if(i==21) {
						hoursApproval = row[i].trim();
					} else if(i==22) {
						comment = row[i].trim();
					} else if(i==23) {
						stage = row[i].trim();
					} else if(i==24) {
						assignee = row[i].trim();
					} else if(i==25) {
						requestedDate = row[i].trim();
					} else if(i==26) {
						statusComment = row[i].trim();
					} else if(i==27) {
						hleHours = row[i].trim();
					} else if(i==28) {
						releaseDate = row[i].trim();
					} else if(i==29) {
						actualHours = row[i].trim();
					} else if(i==30) {
						completionDate = row[i].trim();
					} 
				}
					
				//System.out.println(internalId+":"+name+":"+requestedDate);

				if(requestType.equalsIgnoreCase("Project")) {
					requestType = "Project > 150 hrs";
				} else {
					requestType = "Major 40 - 150 hrs";
				}
				JSONArray cf = new JSONArray();
				if(dept.trim().length() > 0) {
					cf.add(dept);
				}

				dateSubmitted = dateSubmitted.substring(0, dateSubmitted.indexOf(" ")).trim();
				String planFiscalYear = generateFiscalYear(dateSubmitted);

				DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/dd/yy");

				if(dateSubmitted.length() > 0) {
					LocalDate dt = LocalDate.parse(dateSubmitted, df1);
					dateSubmitted = dt.format(df);
				}
				if(requestedDate.length() > 0) {
					LocalDate dt = LocalDate.parse(requestedDate, df1);
					requestedDate = dt.format(df);
				}
				
				OrderedJSONObject planextObj = new OrderedJSONObject();
				planextObj.put("template", "Enhancement");
				planextObj.put("roadmap", "N");
				planextObj.put("auto", "Y"); // To track roadmaps that was auto generated
				planextObj.put("internal_id", internalId);
				planextObj.put("Enhancement Number", internalId);
				planextObj.put("fiscal_year", planFiscalYear);
				planextObj.put("plannedStartDate", "");
				planextObj.put("plannedEndDate", "");
				planextObj.put("size", "");
				planextObj.put("duration", new Long(0));
				planextObj.put("system_risk", new Integer(0));
				planextObj.put("system_priority", "");
				planextObj.put("predecessor", new Long(0));
				planextObj.put("Requested By", "MAU");
				planextObj.put("SoW Submitted Date", "");
				planextObj.put("Scope of Work", scope);
				planextObj.put("Outcome", benefits);
				planextObj.put("Resources Working", resourceReq);
				planextObj.put("priority", priority);
				planextObj.put("crossFunctionalTeam", cf);
				planextObj.put("assignee", "");

				// Add Risk
				if(risks.trim().length() > 0 && !risks.equalsIgnoreCase("NA") && !risks.equalsIgnoreCase("N/A") && !risks.equalsIgnoreCase("test") && 
				   !risks.equalsIgnoreCase("-") && !risks.equalsIgnoreCase("none") && !risks.equalsIgnoreCase("TBD") && !risks.equalsIgnoreCase("TBD") &&
				   !risks.equalsIgnoreCase("unknown") && !risks.equalsIgnoreCase("No risks, thanks")) {
					planextObj.put("Any Other Impact", risks);
				}

				String planId = null, ext = null;
				String checksql = "select _id, extension from enterprise_threat_plan where json_extract(extension, '$.\"Enhancement Number\"')=?";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, internalId);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					planId = rs.getString("_id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(stage.equalsIgnoreCase("Work In Progress")) {
					stage = "In-Flight";
				} else if(stage.equalsIgnoreCase("Moved to Project Process")) {
					stage = "Moved";
				} else if(stage.equalsIgnoreCase("LOE Estimating in Progress")) {
					stage = "Estimation";
				} else if(stage.equalsIgnoreCase("Closed - Not Moving forward")) {
					stage = "Closed";
				} else if(stage.equalsIgnoreCase("Moved to Analytics Process (JIRA)")) {
					stage = "Moved";
				} else if(stage.equalsIgnoreCase("On Hold - waiting on business")) {
					stage = "On Hold";
				} else if(stage.equalsIgnoreCase("Submitted - Needs to be estimated")) {
					stage = "SoW";
				} 
				System.out.println(stage);

				if(planId == null) {
					System.out.println("New Enhancement..."+internalId);
					/*
					planId = generateUUID();
					String insertsql = "insert into enterprise_threat_plan values(?,?,?,?,json_array(),'',?,?,'',?,'',json_object(),?,'','',?,?,'0',current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
					PreparedStatement pst = conn.prepareStatement(insertsql);
					pst.setString(1, planId);
					pst.setString(2, name);
					pst.setString(3, "EN");
					pst.setString(4, desc);
					pst.setString(5, dateSubmitted);
					pst.setString(6, requestedDate);
					pst.setString(7, assignee);
					pst.setString(8, planextObj.toString());
					pst.setString(9, stage);
					pst.setString(10, category);
					pst.executeUpdate();
					pst.close();

					// Add Notes
					if(considerations.trim().length() > 0 && !considerations.equalsIgnoreCase("NA")) {
						insertsql = "insert into enterprise_comment values(null,?,'',?,?,'support@smarterd.com',current_timestamp,'','','support@smarterd.com',current_timestamp,'WORK NOTE',json_object())";
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, planId);
						pst.setString(2, considerations);
						pst.setString(3, companyCode);
						pst.executeUpdate();
						pst.close();
					}
					*/

					// Add Risk
					/*
					if(risks.trim().length() > 0 && !risks.equalsIgnoreCase("NA") && !risks.equalsIgnoreCase("N/A") && !risks.equalsIgnoreCase("test") && 
					!risks.equalsIgnoreCase("-") && !risks.equalsIgnoreCase("none") && !risks.equalsIgnoreCase("TBD") && !risks.equalsIgnoreCase("TBD") &&
					!risks.equalsIgnoreCase("unknown") && !risks.equalsIgnoreCase("No risks, thanks")) {
						
					}
					*/
				} else {
					//System.out.println("Emhancement Already Exists!!!");
					//System.out.println(stage);
					/*
					String updatesql = "update enterprise_threat_plan set implementation=? where _id=?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, stage);
					pst.setString(2, planId);
					pst.executeUpdate();
					pst.close();
					*/
				}
			}
		}
		
		conn.commit();
		conn.close();

		System.out.println("Successfully loaded Plans!!!");
	}

	protected String generateFiscalYear(String startdate) throws Exception {
        LocalDate currentDate = LocalDate.now();
        String currentYear = Integer.toString(currentDate.getYear());
		String fiscalYearStart = "08/01";
        String fiscalYear = "FY23";

        String planFiscalYear = fiscalYear;

        if(startdate != null && startdate.length() > 0) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/dd/yy");
            LocalDate sDate = null;
			if(startdate.length() == 8) {
				sDate = LocalDate.parse(startdate, df1);
			} else {
				sDate = LocalDate.parse(startdate, df);
			}
            LocalDate fysDate = LocalDate.parse(fiscalYearStart+"/20"+fiscalYear.substring(2), df);
            int month = sDate.getMonthValue();
            int year = sDate.getYear();
            int fiscalMonth = fysDate.getMonthValue();
            //System.out.println(month+":"+fiscalMonth+":"+year);
            if(fiscalMonth == 1) {
                planFiscalYear = "FY"+Integer.toString(year).substring(2);
            } else {
                int fy = (month >= fiscalMonth) ? year+1 : year;
                planFiscalYear = "FY"+Integer.toString(fy).substring(2);
            }
        }

        return planFiscalYear;
    }

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "admin", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		MAUREnhancementLoader loader = new MAUREnhancementLoader(filename, companyCode, env);
		loader.load();
	}
}