import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class CISAttckMappingLoader {
	private String domain = "CIS 8";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public CISAttckMappingLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String parentId=null, childId=null, assetType=null, securityFunction=null, childName=null;
				String techniqueId=null, techniqueName=null, techniqueDesc=null, tacticId=null, tacticName=null;
				String malware=null, ransomware=null, webAppHacking=null, insiderAndPrivilegeMisuse=null, targetedIntrusion=null;
				String attckTypeCount=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						parentId = row[i].trim();
					} else if(i==1) {
						childId = row[i].trim();
					} else if(i==2) {
						assetType = row[i].trim();
					} else if(i==3) {
						securityFunction = row[i].trim();
					} else if(i==4) {
						childName = row[i].trim();
					} else if(i==5) {
					} else if(i==6) {
					} else if(i==7) {
					} else if(i==8) {
					} else if(i==9) {
					} else if(i==10) {
						techniqueId = row[i].trim();
					} else if(i==11) {
						techniqueName = row[i].trim();
					} else if(i==12) {
						techniqueDesc = row[i].trim();
					} else if(i==13) {
						tacticId = row[i].trim();
					} else if(i==14) {
						tacticName = row[i].trim();
					} else if(i==15) {
					} else if(i==16) {
					} else if(i==17) {
					} else if(i==18) {
					} else if(i==19) {
					} else if(i==20) {
					} else if(i==21) {
					} else if(i==22) {
						malware = row[i].trim();
					} else if(i==23) {
						ransomware = row[i].trim();
					} else if(i==24) {
						webAppHacking = row[i].trim();
					} else if(i==25) {
						insiderAndPrivilegeMisuse = row[i].trim();
					} else if(i==26) {
						targetedIntrusion = row[i].trim();
					} else if(i==27) {
						attckTypeCount = row[i].trim();
					}
				}
				System.out.println(parentId+":"+childId+":"+malware+":"+ransomware+":"+insiderAndPrivilegeMisuse+":"+webAppHacking+":"+targetedIntrusion+":"+attckTypeCount);

				// Get Control Id
				String controlId = null;
				String controlsql = "select id from enterprise_business_capability where internal_id = ? and business_capability_domain = 'CIS 8'";
				PreparedStatement pst = conn.prepareStatement(controlsql);
				pst.setString(1, childId);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					controlId = rs.getString("id");
				}
				rs.close();
				pst.close();


				if(malware.equalsIgnoreCase("Y")) {
					String tcName = "Malware";
					// Get Threat Class
					String tcId = null, ac = null;
					String tcsql = "select _id, assetclass from enterprise_threat_class where name = ?";
					pst = conn.prepareStatement(tcsql);
					pst.setString(1, tcName);
					rs = pst.executeQuery();
					while(rs.next()) {
						tcId = rs.getString("_id");
						ac = rs.getString("assetclass");
					}
					rs.close();
					pst.close();

					// Create Threat
					String tId = null, ext = null;
					String tsql = "select _id, extension from enterprise_threat where id = ?";
					pst = conn.prepareStatement(tsql);
					pst.setString(1, techniqueId);
					rs = pst.executeQuery();
					while(rs.next()) {
						tId = rs.getString("_id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
					
					if(tId == null) {
						tId = generateUUID();
						String insertsql = "insert into enterprise_threat values(?,?,?,?,'','','','',?,'ACTIVE','','','',json_array(),json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						OrderedJSONObject riskObj = new OrderedJSONObject();
						riskObj.put("harm", new Integer(0));
						riskObj.put("probability", new Integer(0));
						riskObj.put("risk", new Integer(0));
						JSONArray tacticList = new JSONArray();
						if(tacticId != null && tacticId.length() > 0) {
							OrderedJSONObject tacticObj = new OrderedJSONObject();
							tacticObj.put("id", tacticId);
							tacticObj.put("name", tacticName);
							tacticList.add(tacticObj);
						}
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("assessment", "Y");
						extObj.put("raw_risk", riskObj);
						extObj.put("threat_class", tcName);
						extObj.put("tactic_1", tacticList);
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, tId);
						pst.setString(2, techniqueId);
						pst.setString(3, techniqueName);
						pst.setString(4, techniqueDesc);
						pst.setString(5, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						JSONArray tacticList = null;
						if(extObj.has("tactic_1")) {
							tacticList = (JSONArray)extObj.get("tactic_1");
							Iterator iter = tacticList.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject obj = (OrderedJSONObject)iter.next();
								String id = (String)obj.get("id");
								if(!id.equalsIgnoreCase(tacticId)) {
									OrderedJSONObject tacticObj = new OrderedJSONObject();
									tacticObj.put("id", tacticId);
									tacticObj.put("name", tacticName);
									tacticList.add(tacticObj);
								}
							}
						} else {
							tacticList = new JSONArray();
							if(tacticId != null && tacticId.length() > 0) {
								OrderedJSONObject tacticObj = new OrderedJSONObject();
								tacticObj.put("id", tacticId);
								tacticObj.put("name", tacticName);
								tacticList.add(tacticObj);
							}
						}
						extObj.put("tactic_1", tacticList);

						// Update 
						String updatesql = "update enterprise_threat set extension=? where _id=?";
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, extObj.toString());
						pst.setString(2, tId);
						pst.executeUpdate();
						pst.close();
					}

					// Link Threat Class to Threat
					String relsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
					String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
					int count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, tId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, tId);
						pst.setString(2, tcId);
						pst.setString(3, "THREAT");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst.setString(1, tcId);
						pst.setString(2, tId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					// Link Control to Threat Class
					count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, controlId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, controlId);
						pst.setString(2, tcId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, tcId);
						pst.setString(2, controlId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}
					System.out.println("Successfully created 1!!!");
				}

				if(ransomware.equalsIgnoreCase("Y")) {
					String tcName = "Ransomware";
					// Get Threat Class
					String tcId = null, ac = null;
					String tcsql = "select _id, name, assetclass from enterprise_threat_class where name = ?";
					pst = conn.prepareStatement(tcsql);
					pst.setString(1, tcName);
					rs = pst.executeQuery();
					while(rs.next()) {
						tcId = rs.getString("_id");
						ac = rs.getString("assetclass");
					}
					rs.close();
					pst.close();

					// Create Threat
					String tId = null, ext = null;
					String tsql = "select _id, extension from enterprise_threat where id = ?";
					pst = conn.prepareStatement(tsql);
					pst.setString(1, techniqueId);
					rs = pst.executeQuery();
					while(rs.next()) {
						tId = rs.getString("_id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
					
					if(tId == null) {
						tId = generateUUID();
						String insertsql = "insert into enterprise_threat values(?,?,?,?,'','','','',?,'ACTIVE','','','',json_array(),json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						OrderedJSONObject riskObj = new OrderedJSONObject();
						riskObj.put("harm", new Integer(0));
						riskObj.put("probability", new Integer(0));
						riskObj.put("risk", new Integer(0));
						JSONArray tacticList = new JSONArray();
						if(tacticId != null && tacticId.length() > 0) {
							OrderedJSONObject tacticObj = new OrderedJSONObject();
							tacticObj.put("id", tacticId);
							tacticObj.put("name", tacticName);
							tacticList.add(tacticObj);
						}
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("assessment", "Y");
						extObj.put("raw_risk", riskObj);
						extObj.put("threat_class", tcName);
						extObj.put("tactic_1", tacticList);
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, tId);
						pst.setString(2, techniqueId);
						pst.setString(3, techniqueName);
						pst.setString(4, techniqueDesc);
						pst.setString(5, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						JSONArray tacticList = null;
						if(extObj.has("tactic_1")) {
							tacticList = (JSONArray)extObj.get("tactic_1");
							Iterator iter = tacticList.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject obj = (OrderedJSONObject)iter.next();
								String id = (String)obj.get("id");
								if(!id.equalsIgnoreCase(tacticId)) {
									OrderedJSONObject tacticObj = new OrderedJSONObject();
									tacticObj.put("id", tacticId);
									tacticObj.put("name", tacticName);
									tacticList.add(tacticObj);
								}
							}
						} else {
							tacticList = new JSONArray();
							if(tacticId != null && tacticId.length() > 0) {
								OrderedJSONObject tacticObj = new OrderedJSONObject();
								tacticObj.put("id", tacticId);
								tacticObj.put("name", tacticName);
								tacticList.add(tacticObj);
							}
						}
						extObj.put("tactic_1", tacticList);

						// Update 
						String updatesql = "update enterprise_threat set extension=? where _id=?";
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, extObj.toString());
						pst.setString(2, tId);
						pst.executeUpdate();
						pst.close();
					}

					// Link Threat Class to Threat
					String relsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
					String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
					int count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, tId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, tId);
						pst.setString(2, tcId);
						pst.setString(3, "THREAT");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst.setString(1, tcId);
						pst.setString(2, tId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					// Link Control to Threat Class
					count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, controlId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, controlId);
						pst.setString(2, tcId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, tcId);
						pst.setString(2, controlId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					System.out.println("Successfully created 2!!!");
				}

				if(webAppHacking.equalsIgnoreCase("Y")) {
					String tcName = "Web Application Hacking";
					// Get Threat Class
					String tcId = null, ac = null;
					String tcsql = "select _id, name, assetclass from enterprise_threat_class where name = ?";
					pst = conn.prepareStatement(tcsql);
					pst.setString(1, tcName);
					rs = pst.executeQuery();
					while(rs.next()) {
						tcId = rs.getString("_id");
						ac = rs.getString("assetclass");
					}
					rs.close();
					pst.close();

					String tId = null, ext = null;
					String tsql = "select _id, extension from enterprise_threat where id = ?";
					pst = conn.prepareStatement(tsql);
					pst.setString(1, techniqueId);
					rs = pst.executeQuery();
					while(rs.next()) {
						tId = rs.getString("_id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
					
					if(tId == null) {
						tId = generateUUID();
						String insertsql = "insert into enterprise_threat values(?,?,?,?,'','','','',?,'ACTIVE','','','',json_array(),json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						OrderedJSONObject riskObj = new OrderedJSONObject();
						riskObj.put("harm", new Integer(0));
						riskObj.put("probability", new Integer(0));
						riskObj.put("risk", new Integer(0));
						JSONArray tacticList = new JSONArray();
						if(tacticId != null && tacticId.length() > 0) {
							OrderedJSONObject tacticObj = new OrderedJSONObject();
							tacticObj.put("id", tacticId);
							tacticObj.put("name", tacticName);
							tacticList.add(tacticObj);
						}
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("assessment", "Y");
						extObj.put("raw_risk", riskObj);
						extObj.put("threat_class", tcName);
						extObj.put("tactic_1", tacticList);
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, tId);
						pst.setString(2, techniqueId);
						pst.setString(3, techniqueName);
						pst.setString(4, techniqueDesc);
						pst.setString(5, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						JSONArray tacticList = null;
						if(extObj.has("tactic_1")) {
							tacticList = (JSONArray)extObj.get("tactic_1");
							Iterator iter = tacticList.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject obj = (OrderedJSONObject)iter.next();
								String id = (String)obj.get("id");
								if(!id.equalsIgnoreCase(tacticId)) {
									OrderedJSONObject tacticObj = new OrderedJSONObject();
									tacticObj.put("id", tacticId);
									tacticObj.put("name", tacticName);
									tacticList.add(tacticObj);
								}
							}
						} else {
							tacticList = new JSONArray();
							if(tacticId != null && tacticId.length() > 0) {
								OrderedJSONObject tacticObj = new OrderedJSONObject();
								tacticObj.put("id", tacticId);
								tacticObj.put("name", tacticName);
								tacticList.add(tacticObj);
							}
						}
						extObj.put("tactic_1", tacticList);

						// Update 
						String updatesql = "update enterprise_threat set extension=? where _id=?";
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, extObj.toString());
						pst.setString(2, tId);
						pst.executeUpdate();
						pst.close();
					}

					// Link Threat Class to Threat
					String relsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
					String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
					int count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, tId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, tId);
						pst.setString(2, tcId);
						pst.setString(3, "THREAT");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst.setString(1, tcId);
						pst.setString(2, tId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					// Link Control to Threat Class
					count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, controlId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, controlId);
						pst.setString(2, tcId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, tcId);
						pst.setString(2, controlId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}
					System.out.println("Successfully created 3!!!");
				}

				if(targetedIntrusion.equalsIgnoreCase("Y")) {
					String tcName = "Targeted Intrusion";
					// Get Threat Class
					String tcId = null, ac = null;
					String tcsql = "select _id, name, assetclass from enterprise_threat_class where name = ?";
					pst = conn.prepareStatement(tcsql);
					pst.setString(1, tcName);
					rs = pst.executeQuery();
					while(rs.next()) {
						tcId = rs.getString("_id");
						ac = rs.getString("assetclass");
					}
					rs.close();
					pst.close();

					String tId = null, ext = null;
					String tsql = "select _id, extension from enterprise_threat where id = ?";
					pst = conn.prepareStatement(tsql);
					pst.setString(1, techniqueId);
					rs = pst.executeQuery();
					while(rs.next()) {
						tId = rs.getString("_id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
					
					if(tId == null) {
						tId = generateUUID();
						String insertsql = "insert into enterprise_threat values(?,?,?,?,'','','','',?,'ACTIVE','','','',json_array(),json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						OrderedJSONObject riskObj = new OrderedJSONObject();
						riskObj.put("harm", new Integer(0));
						riskObj.put("probability", new Integer(0));
						riskObj.put("risk", new Integer(0));
						JSONArray tacticList = new JSONArray();
						if(tacticId != null && tacticId.length() > 0) {
							OrderedJSONObject tacticObj = new OrderedJSONObject();
							tacticObj.put("id", tacticId);
							tacticObj.put("name", tacticName);
							tacticList.add(tacticObj);
						}
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("assessment", "Y");
						extObj.put("raw_risk", riskObj);
						extObj.put("threat_class", tcName);
						extObj.put("tactic_1", tacticList);
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, tId);
						pst.setString(2, techniqueId);
						pst.setString(3, techniqueName);
						pst.setString(4, techniqueDesc);
						pst.setString(5, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						JSONArray tacticList = null;
						if(extObj.has("tactic_1")) {
							tacticList = (JSONArray)extObj.get("tactic_1");
							Iterator iter = tacticList.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject obj = (OrderedJSONObject)iter.next();
								String id = (String)obj.get("id");
								if(!id.equalsIgnoreCase(tacticId)) {
									OrderedJSONObject tacticObj = new OrderedJSONObject();
									tacticObj.put("id", tacticId);
									tacticObj.put("name", tacticName);
									tacticList.add(tacticObj);
								}
							}
						} else {
							tacticList = new JSONArray();
							if(tacticId != null && tacticId.length() > 0) {
								OrderedJSONObject tacticObj = new OrderedJSONObject();
								tacticObj.put("id", tacticId);
								tacticObj.put("name", tacticName);
								tacticList.add(tacticObj);
							}
						}
						extObj.put("tactic_1", tacticList);

						// Update 
						String updatesql = "update enterprise_threat set extension=? where _id=?";
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, extObj.toString());
						pst.setString(2, tId);
						pst.executeUpdate();
						pst.close();
					}

					// Link Threat Class to Threat
					String relsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
					String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
					int count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, tId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, tId);
						pst.setString(2, tcId);
						pst.setString(3, "THREAT");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst.setString(1, tcId);
						pst.setString(2, tId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					// Link Control to Threat Class
					count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, controlId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, controlId);
						pst.setString(2, tcId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, tcId);
						pst.setString(2, controlId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}
					System.out.println("Successfully created 4!!!");
				}

				if(insiderAndPrivilegeMisuse.equalsIgnoreCase("Y")) {
					String tcName = "Insider and Privilege Misuse";
					// Get Threat Class
					String tcId = null, ac = null;
					String tcsql = "select _id, name, assetclass from enterprise_threat_class where name = ?";
					pst = conn.prepareStatement(tcsql);
					pst.setString(1, tcName);
					rs = pst.executeQuery();
					while(rs.next()) {
						tcId = rs.getString("_id");
						ac = rs.getString("assetclass");
					}
					rs.close();
					pst.close();

					String tId = null, ext = null;
					String tsql = "select _id, extension from enterprise_threat where id = ?";
					pst = conn.prepareStatement(tsql);
					pst.setString(1, techniqueId);
					rs = pst.executeQuery();
					while(rs.next()) {
						tId = rs.getString("_id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();
					
					if(tId == null) {
						tId = generateUUID();
						String insertsql = "insert into enterprise_threat values(?,?,?,?,'','','','',?,'ACTIVE','','','',json_array(),json_array(),json_array(),current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						OrderedJSONObject riskObj = new OrderedJSONObject();
						riskObj.put("harm", new Integer(0));
						riskObj.put("probability", new Integer(0));
						riskObj.put("risk", new Integer(0));
						JSONArray tacticList = new JSONArray();
						if(tacticId != null && tacticId.length() > 0) {
							OrderedJSONObject tacticObj = new OrderedJSONObject();
							tacticObj.put("id", tacticId);
							tacticObj.put("name", tacticName);
							tacticList.add(tacticObj);
						}
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("assessment", "Y");
						extObj.put("raw_risk", riskObj);
						extObj.put("threat_class", tcName);
						extObj.put("tactic_1", tacticList);
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, tId);
						pst.setString(2, techniqueId);
						pst.setString(3, techniqueName);
						pst.setString(4, techniqueDesc);
						pst.setString(5, extObj.toString());
						pst.executeUpdate();
						pst.close();
					} else {
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						JSONArray tacticList = null;
						if(extObj.has("tactic_1")) {
							tacticList = (JSONArray)extObj.get("tactic_1");
							Iterator iter = tacticList.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject obj = (OrderedJSONObject)iter.next();
								String id = (String)obj.get("id");
								if(!id.equalsIgnoreCase(tacticId)) {
									OrderedJSONObject tacticObj = new OrderedJSONObject();
									tacticObj.put("id", tacticId);
									tacticObj.put("name", tacticName);
									tacticList.add(tacticObj);
								}
							}
						} else {
							tacticList = new JSONArray();
							if(tacticId != null && tacticId.length() > 0) {
								OrderedJSONObject tacticObj = new OrderedJSONObject();
								tacticObj.put("id", tacticId);
								tacticObj.put("name", tacticName);
								tacticList.add(tacticObj);
							}
						}
						extObj.put("tactic_1", tacticList);

						// Update 
						String updatesql = "update enterprise_threat set extension=? where _id=?";
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, extObj.toString());
						pst.setString(2, tId);
						pst.executeUpdate();
						pst.close();
					}

					// Link Threat Class to Threat
					String relsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
					String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
					int count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, tId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, tId);
						pst.setString(2, tcId);
						pst.setString(3, "THREAT");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "THREAT");
						pst.setString(1, tcId);
						pst.setString(2, tId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}

					// Link Control to Threat Class
					count = 0;
					pst = conn.prepareStatement(relsql);
					pst.setString(1, tcId);
					pst.setString(2, controlId);
					rs = pst.executeQuery();
					while(rs.next()) {
						count = rs.getInt("count");
					}
					rs.close();
					pst.close();
					if(count == 0) {
						OrderedJSONObject ctx = new OrderedJSONObject();
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "THREAT CLASS");
						pst = conn.prepareStatement(insertrelsql);
						pst.setString(1, controlId);
						pst.setString(2, tcId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, tcId);
						pst.setString(2, controlId);
						pst.setString(3, "THREAT CLASS");
						pst.setString(4, ctx.toString());
						pst.setString(5, "support@smarterd.com");
						pst.setString(6, "support@smarterd.com");
						pst.setString(7, companyCode);
						pst.addBatch();
						pst.executeBatch();
						pst.close();
					}
					System.out.println("Successfully created 5!!!");
				}
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		CISAttckMappingLoader loader = new CISAttckMappingLoader(filename, companyCode, env);
		loader.process();
	}
}