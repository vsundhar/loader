import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Channel;
import com.github.seratch.jslack.api.model.Field;
import com.github.seratch.jslack.api.rtm.RTMClient;
import com.github.seratch.jslack.api.rtm.RTMMessageHandler;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import com.github.seratch.jslack.*;

public class SlackAdapter {
    private String url = "https://smarterd.slack.com";
    private Slack slack = null;
    public SlackAdapter() {
        slack = Slack.getInstance();
    }

    public void post() throws Exception {
        Payload payload = Payload.builder()
        .channel("#random")
        .username("support@smarterd.com")
        .iconEmoji(":smile_cat:")
        .text("Hello World!")
        .build();

        WebhookResponse response = slack.send(url, payload);
        System.out.println("Successfully posted message!!!");
    }

    public static void main(String[] args) throws Exception {
        SlackAdapter slack = new SlackAdapter();
        slack.post();
    } 

}