import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Base64;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.CustomFieldOption;
import net.rcarz.jiraclient.Project;
import net.rcarz.jiraclient.ProjectCategory;
import net.rcarz.jiraclient.RestClient;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.IssueType;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.User;
import net.rcarz.jiraclient.Watches;
import net.sf.json.JSON;
import net.sf.json.JSONObject;

import net.rcarz.jiraclient.Comment;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.agile.AgileClient;
import net.rcarz.jiraclient.agile.Sprint;
import net.rcarz.jiraclient.agile.TimeTracking;
import net.rcarz.jiraclient.agile.Resolution;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.ContentType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;

public class SmarteRiskSense extends SmarteLoader {
   //String API_KEY = "11ecdb86-0d86-21fa-becf-06ddf54bb37e:VE5tQzIlNGNDZW9wd1ltN0plZyZFTDdCMiVLZDVReXZiRF4vbkFNMy1HS1NzVjhYJFNeT1lUTDlQUDUxZVNCUA==";
   // smarterd user key
   // SmarterD
   // String API_KEY = "11edb8a7-8a34-8c06-ab4b-06ddf54bb37e:UWk1ZlNzI3FwZmxecHBnd21mOCoqYjJaNzZaYU9NJjEtN0dqJjA5WWFzZVZWSDYlayRpckJGdGteLW4lMSFycQ==";
   // Bharat
   String API_KEY = "11eddf7c-f48a-1a61-99ec-0628e1528d2e:eGpwWUZ4T0ZTMExNLUxCcndoUUxlNkk1bHNyMmstREk3amhIaGg4dSVVVi9ZMkJERERVIUpTWTRRTEw4IVp0NQ==";
   String clientId = "1224";
   String rsurl = "https://platform4.risksense.com/api/v1/client";
   
   public SmarteRiskSense() {
   }

   public void load() throws Exception {
      System.out.println("In load...");
      String sql = "select json_unquote(json_extract(extension, '$.THREAT.scan_id')) scan_id from enterprise_vulnerability_log where json_extract(extension, '$.scan_period') = ? and priority != 'Info'"; //and json_unquote(json_extract(extension, '$.THREAT.status')) in ('Complete','False Positive','Risk Acceptance')";
      int count = 0, totalCount = 0;
      JSONArray vulnrList = new JSONArray();

      // Get Current Scan Period
      String scanPeriod = getCurrentScanPeriod();

      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, scanPeriod);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String scanId = rs.getString("scan_id");
         //scanId = "1103205085";
         vulnrList.add(scanId);
         count++;
         if(count == 50) {
            String ids = vulnrList.toString().replaceAll("[\\[\\]]", "").replaceAll("\"","");
            search(ids, scanPeriod);
            count = 0;
            vulnrList.clear();
            //break;
         }
         totalCount++;
      }
      rs.close();
      pst.close();
      System.out.println("Total Rows Processed:"+totalCount);
   }

   private OrderedJSONObject sync(String name, String desc, String type, String reason, JSONArray idList, String priority) throws Exception {
      System.out.println("In sync..."+name+":"+type+":"+idList.toString()+":"+priority);
      String status = "";
      OrderedJSONObject statusObj = new OrderedJSONObject();
      String workflowId = "", workflowType = "";

      if(type.equalsIgnoreCase("Complete")) {
         workflowType = "remediation";
      } else if(type.equalsIgnoreCase("Risk Acceptance")) {
         workflowType = "acceptance";
      } else if(type.equalsIgnoreCase("False Positive")) {
         workflowType = "falsePositive";
      }

      if(workflowType.length() > 0) {
         // Expiration Date
         DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
         LocalDateTime ldt = LocalDateTime.now();
         ldt = ldt.plusYears(10);
         String expirationDate = ldt.format(df);
         System.out.println(expirationDate);

         // Format Host Ids
         String ids = idList.toString().replaceAll("[\\[\\]]", "");
         ids = ids.replaceAll("\"", "");

         // Format 1 - For Empty Workflow - 
         // Filter Format - {"subject":"hostFinding","filterRequest":{"filters":[]}}
         /*
         OrderedJSONObject subjectFilter = new OrderedJSONObject();
         subjectFilter.put("subject", "hostFinding");
         OrderedJSONObject filterRequest = new OrderedJSONObject();
         filterRequest.put("filters", new JSONArray());
         subjectFilter.put("filterRequest", filterRequest);

         String subjectFilterRequest = subjectFilter.toString();
         String requestParm = "name="+name+"&description="+desc+"&overrideControl=NONE&subjectFilterRequest="+subjectFilterRequest+"&expirationDate="+expirationDate+"&reason="+reason+"&isWorkflowEmpty=true";
         */

         // Format 2 - For Workflow with hostd 
         // Filter Format - {"subject":"hostFinding","filterRequest":{"filters":[{"field":"id","exclusive":false,"operator":"IN","value":"313308453,313308690"},{"field":"id","exclusive":false,"operator":"IN","value":"313308453,313308690"}]}} 
         OrderedJSONObject subjectFilter = new OrderedJSONObject();
         subjectFilter.put("subject", "hostFinding");
         OrderedJSONObject filterRequest = new OrderedJSONObject();
         OrderedJSONObject filter = new OrderedJSONObject();
         filter.put("field", "id");
         filter.put("exclusive", new Boolean(false));
         filter.put("operator", "IN");
         filter.put("value", ids);
         JSONArray filterList = new JSONArray();
         filterList.add(filter);
         filterList.add(filter); // Must add the same filter twice
         filterRequest.put("filters", filterList);
         subjectFilter.put("filterRequest", filterRequest);

         String subjectFilterRequest = subjectFilter.toString();
         System.out.println(subjectFilterRequest);

         String wfUrl = rsurl+"/"+clientId+"/workflowBatch/"+workflowType+"/request";
      
         HttpPost post = new HttpPost(wfUrl);
         post.addHeader("accept", "application/json");
         post.addHeader("x-api-key", API_KEY);
         post.addHeader("Content-Type", "multipart/form-data");

         // Step 1 Create Workflow
         /*
         StringBody nameBody = new StringBody(name, ContentType.TEXT_PLAIN);
         StringBody descBody = new StringBody(desc, ContentType.TEXT_PLAIN);
         StringBody overrideBody = new StringBody("AUTHORIZED", ContentType.TEXT_PLAIN);
         StringBody filterBody = new StringBody(subjectFilterRequest, ContentType.APPLICATION_JSON);
         StringBody dateBody = new StringBody(expirationDate, ContentType.TEXT_PLAIN);
         StringBody reasonBody = new StringBody(reason, ContentType.TEXT_PLAIN);
         StringBody workflowBody = new StringBody("false", ContentType.TEXT_PLAIN);
         */

         MultipartEntityBuilder builder = MultipartEntityBuilder.create();
         builder.addTextBody("name", name, ContentType.TEXT_PLAIN);
         builder.addTextBody("description", "hostFinding", ContentType.TEXT_PLAIN);
         builder.addTextBody("overrideControl", "AUTHORIZED", ContentType.TEXT_PLAIN);
         builder.addTextBody("subjectFilterRequest", subjectFilterRequest.toString(), ContentType.TEXT_PLAIN);
         builder.addTextBody("expirationDate", expirationDate, ContentType.TEXT_PLAIN);
         builder.addTextBody("reason", reason, ContentType.TEXT_PLAIN);
         builder.addTextBody("isEmptyWorkflow", "false", ContentType.TEXT_PLAIN);
         HttpEntity reqEntity = builder.build();
         post.setEntity(reqEntity);
         System.out.println(post.toString());
   
         /*      
         List<NameValuePair> urlParameters = new ArrayList<>();
         urlParameters.add(new BasicNameValuePair("name", name));
         urlParameters.add(new BasicNameValuePair("description", desc));
         urlParameters.add(new BasicNameValuePair("overrideControl", "AUTHORIZED"));
         urlParameters.add(new BasicNameValuePair("subjectFilterRequest", subjectFilterRequest));
         urlParameters.add(new BasicNameValuePair("expirationDate", expirationDate));
         urlParameters.add(new BasicNameValuePair("reason", reason));
         urlParameters.add(new BasicNameValuePair("isEmptyWorkflow", "false"));
         post.setEntity(new UrlEncodedFormEntity(urlParameters));
         */

         try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            CloseableHttpResponse response = httpClient.execute(post)) {
            String res = EntityUtils.toString(response.getEntity());
            System.out.println(res);
         }

         // Used for cUrl and method below
         String requestParm = "name="+name+"&description="+desc+"&overrideControl=AUTHORIZED&subjectFilterRequest="+subjectFilterRequest+"&expirationDate="+expirationDate+"&reason="+reason+"&isEmptyWorkflow=false"; 
         requestParm = URLEncoder.encode(requestParm, "UTF-8" ); 

         /*
         URL url = new URL(wfUrl);
         HttpURLConnection http = (HttpURLConnection)url.openConnection();
         http.setRequestMethod("POST");
         http.setDoOutput(true);
         http.setRequestProperty("accept", "application/json");
         http.setRequestProperty("x-api-key", API_KEY);
         http.setRequestProperty("Content-Type", "multipart/form-data");

         byte[] out = requestParm.getBytes(StandardCharsets.UTF_8);
         int length = out.length;
         http.setFixedLengthStreamingMode(length);
         http.connect();
         
         try(OutputStream os = http.getOutputStream()) {
            os.write(out);
            OrderedJSONObject res = null;
            StringBuilder response = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
               response.append(responseLine.trim());
            }
            if(response.length() > 0) {
               res = new OrderedJSONObject(response.toString());
            } else {
               res = new OrderedJSONObject();
            }

            // Get Workflow Id
            workflowId = (String)res.get("id");

            System.out.println("Successfully created workflow["+workflowId+"]"+res.toString());
            http.disconnect();
         } catch(Exception ex) {
            ex.printStackTrace();
            status = "Error creating workflow:"+ex.getMessage();
         }
         */

         /* Using cUrl
         String command = "curl -X 'POST' 'https://platform4.risksense.com/api/v1/client/1224/workflowBatch/remediation/request' -H 'accept: application/json' -H 'x-api-key: 11ecdb86-0d86-21fa-becf-06ddf54bb37e:VE5tQzIlNGNDZW9wd1ltN0plZyZFTDdCMiVLZDVReXZiRF4vbkFNMy1HS1NzVjhYJFNeT1lUTDlQUDUxZVNCUA==' -H 'Content-Type: multipart/form-data' -F 'name=SmarterD Test-1' -F 'overrideControl=AUTHORIZED' -F 'subjectFilterRequest={\"subject\":\"hostFinding\",\"filterRequest\":{\"filters\":[{\"field\":\"id\",\"exclusive\":false,\"operator\":\"IN\",\"value\":\"313308239\"},{\"field\":\"id\",\"exclusive\":false,\"operator\":\"IN\",\"value\":\"313308239\"}]}}' -F 'expirationDate=2032-06-10' -F 'description=SmarterD Test-1' -F 'reason=SmarterD Test-1' -F 'isEmptyWorkflow=false'";
         Process process = Runtime.getRuntime().exec(command);
         BufferedReader reader =  new BufferedReader(new InputStreamReader(process.getInputStream()));
         StringBuilder builder = new StringBuilder();
         String line = null;
         while((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
         }
         reader.close();
         String result = builder.toString();
         System.out.println(result);
         process.waitFor();
         int exitCode = process.exitValue();
         process.destroy();
         */

         // Step 5 - Update Sync flag and Workflow Id 
         if(status.length() == 0) {
            /*
            String updatesql = "update enterprise_rel set asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"scan_sync\"', ?),asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"sync_id\"', ?),asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"sync_name\"', ?) where json_unquote(json_extract(asset_rel_context, '$.THREAT.\"scan_id\"')) in (REPLACE_STRING)";
            ids = idList.toString().replaceAll("[\\[\\]]", "");
            updatesql = updatesql.replaceAll("REPLACE_STRING", ids);
            PreparedStatement pst = conn.prepareStatement(updatesql);
            pst.setString(1, "Sync");
            pst.setString(2, workflowId);
            pst.setString(3, name);
            pst.executeUpdate();
            pst.close();
            
            String updatelogsql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.\"scan_sync\"', ?),extension=json_set(extension, '$.\"sync_id\"', ?),extension=json_set(extension, '$.\"sync_name\"', ?) where json_unquote(json_extract(extension, '$.\"scan_id\"')) in (REPLACE_STRING)";
            updatelogsql = updatelogsql.replaceAll("REPLACE_STRING", ids);
            pst = conn.prepareStatement(updatelogsql);
            pst.setString(1, "Sync");
            pst.setString(2, workflowId);
            pst.setString(3, name);
            pst.executeUpdate();
            pst.close();

            conn.commit();
            System.out.println("Successfully synced Vulnerability Status!!!");
            status = "Successfully approved workflow!!!";
            */
         }
      } else {
         System.out.println("Invalid workflow type:"+type);
         status = "Invalid workflow type:"+type;
         workflowId = "";
      }

      statusObj.put("STATUS", status);
      statusObj.put("sync_id", workflowId);
      return statusObj;
   }

   private OrderedJSONObject approve(String wfName, String type) throws Exception {
      System.out.println("In approve..."+wfName+":"+type);
      String status = "";
      OrderedJSONObject statusObj = new OrderedJSONObject();
      String workflowType = "";

      // Get Workflow Id 
      String workflowId = searchWorkflow(wfName);

      if(workflowId != null && workflowId.length() > 0) {
         if(type.equalsIgnoreCase("Complete")) {
            workflowType = "remediation";
         } else if(type.equalsIgnoreCase("Risk Acceptance")) {
            workflowType = "acceptance";
         } else if(type.equalsIgnoreCase("False Positive")) {
            workflowType = "falsePositive";
         }

         if(workflowType.length() > 0) {
            // Expiration Date
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime ldt = LocalDateTime.now();
            ldt = ldt.plusDays(120);
            String expirationDate = ldt.format(df);
            System.out.println(expirationDate);

            ldt = LocalDateTime.now();
            ldt = ldt.plusDays(120);
            expirationDate = DateTimeFormatter.ISO_INSTANT.format(ldt);
            String approveUrl = rsurl+"/"+clientId+"/workflowBatch/"+workflowType+"/approve";
            URL url = new URL(approveUrl);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http = (HttpURLConnection)url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("x-api-key", API_KEY);
            http.setRequestProperty("content-type", "application/json");

            OrderedJSONObject bodyObj = new OrderedJSONObject();
            bodyObj.put("workflowBatchUuid", workflowId);
            bodyObj.put("expirationDate", expirationDate);
            bodyObj.put("overrideExpirationDate", new Boolean(true));

            byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
            int length = out.length;
            http.setFixedLengthStreamingMode(length);
            http.connect();
         
            try(OutputStream os = http.getOutputStream()) {
               os.write(out);

               OrderedJSONObject res = null;
               StringBuilder response = new StringBuilder();
               BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
               String responseLine = null;
               while ((responseLine = br.readLine()) != null) {
                  response.append(responseLine.trim());
               }
               if(response.length() > 0) {
                  res = new OrderedJSONObject(response.toString());
               } else {
                  res = new OrderedJSONObject();
               }

               System.out.println("Successfully approved workflow["+workflowId+"]:"+res.toString());
               http.disconnect();
            } catch(Exception ex) {
               ex.printStackTrace();
               status = "Error auto approving Medium/Low workflow["+workflowId+"]:"+ex.getMessage();
            }
            
            // Step 5 - Update Sync flag and Workflow Id 
            if(status.length() == 0) {
               String updatesql = "update enterprise_rel set asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"scan_sync\"', ?),asset_rel_context=json_set(asset_rel_context, '$.THREAT.\"sync_id\"', ?) where json_extract(asset_rel_context, '$.THREAT.\"sync_name\"') = ?";
               PreparedStatement pst = conn.prepareStatement(updatesql);
               pst.setString(1, "Approve");
               pst.setString(2, workflowId);
               pst.setString(3, wfName);
               pst.executeUpdate();
               pst.close();
               
               String updatelogsql = "update enterprise_vulnerability_log set extension=json_set(extension, '$.\"scan_sync\"', ?),extension=json_set(extension, '$.\"sync_id\"', ?) where json_extract(extension, '$.\"sync_name\"') = ?";
               pst = conn.prepareStatement(updatelogsql);
               pst.setString(1, "Approve");
               pst.setString(2, workflowId);
               pst.setString(3, wfName);
               pst.executeUpdate();
               pst.close();

               conn.commit();
               System.out.println("Successfully synced Vulnerability Status!!!");
               status = "Successfully approved workflow!!!";
            }
         } else {
            System.out.println("Invalid workflow type:"+type);
            status = "Invalid workflow type:"+type;
            workflowId = "";
         }
      } else {
         System.out.println("Cannot find workflow :"+wfName);
         status = "Cannot find workflow :"+wfName;
         workflowId = "";
      }

      statusObj.put("STATUS", status);
      statusObj.put("sync_id", workflowId);
      return statusObj;
   }

   private void add(String name, JSONArray hostList) throws Exception {
      /*
      String mapUrl = rsurl+"/"+clientId+"/workflowBatch/"+workflowType+"/"+workflowId+"/map";
      url = new URL(mapUrl);
      http = (HttpURLConnection)url.openConnection();
      http.setRequestMethod("POST");
      http.setDoOutput(true);
      http.setRequestProperty("x-api-key", API_KEY);
      http.setRequestProperty("content-type", "application/json");

      //bodyObj.clear();
      JSONArray filterArr = new JSONArray();
      OrderedJSONObject filterObj = new OrderedJSONObject();
      filterObj.put("field", "id");
      filterObj.put("exclusive", new Boolean(false));
      filterObj.put("operator", "IN");
      filterObj.put("value", ids);
      filterArr.add(filterObj);

      JSONArray sortArr = new JSONArray();
      OrderedJSONObject sortObj = new OrderedJSONObject();
      sortObj.put("field", "id");
      sortObj.put("direction", "ASC");
      sortArr.add(sortObj);

      OrderedJSONObject bodyObj = new OrderedJSONObject();
      bodyObj.put("subject", "Host Remediation Status Update");
      bodyObj.put("filters", filterArr);
      bodyObj.put("sort", sortArr);
      bodyObj.put("projection", "basic");
      bodyObj.put("page", new Integer(0));
      bodyObj.put("size", new Integer(20));

      byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
      int length = out.length;
      http.setFixedLengthStreamingMode(length);
      http.connect();
      
      try(OutputStream os = http.getOutputStream()) {
         os.write(out);

         OrderedJSONObject res = null;
         StringBuilder response = new StringBuilder();
         BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
         String responseLine = null;
         while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
         }
         if(response.length() > 0) {
            res = new OrderedJSONObject(response.toString());
         } else {
            res = new OrderedJSONObject();
         }

         System.out.println("Successfully Added Hosts to workflow["+workflowId+"]:"+res.toString());
         http.disconnect();
      } catch(Exception ex) {
         ex.printStackTrace();
         status = "Error adding host Ids to workflow["+workflowId+"]:"+ex.getMessage();
      }
      */
   }

   private String searchWorkflow(String id) throws Exception {
      System.out.println("In search..."+id);
      String workflowId = "";
      OrderedJSONObject statusList = new OrderedJSONObject();

      String searchUrl = rsurl+"/"+clientId+"/workflowBatch/search";
      URL url = new URL(searchUrl);
      HttpURLConnection http = (HttpURLConnection)url.openConnection();
      http = (HttpURLConnection)url.openConnection();
      http.setRequestMethod("POST");
      http.setDoOutput(true);
      http.setRequestProperty("x-api-key", API_KEY);
      http.setRequestProperty("content-type", "application/json");

      // Workflow Search - {"filters":[{"field":"name","exclusive":false,"operator":"IN","orWithPrevious":false,"implicitFilters":[],"value":"RSTEST01"}],"projection":"internal","sort":[{"field":"created","direction":"DESC"}],"page":0,"size":50}
      OrderedJSONObject filterRequest = new OrderedJSONObject();
      OrderedJSONObject filter = new OrderedJSONObject();
      filter.put("field", "id");
      filter.put("exclusive", new Boolean(false));
      filter.put("operator", "IN");
      filter.put("orWithPrevious", new Boolean(false));
      filter.put("implicitFilters", new JSONArray());
      filter.put("value", id);
      JSONArray filterList = new JSONArray();
      filterList.add(filter);
      filterRequest.put("filters", filterList);
      filterRequest.put("projection", "internal");
      //filterRequest.put("projection", "basic");
      filterRequest.put("page", new Integer(0));
      filterRequest.put("size", new Integer(50));
      OrderedJSONObject sort = new OrderedJSONObject();
      sort.put("field", "created");
      sort.put("direction", "DESC");
      JSONArray sortList = new JSONArray();
      sortList.add(sort);
      filterRequest.put("sort", sortList);

      byte[] out = filterRequest.toString().getBytes(StandardCharsets.UTF_8);
      int length = out.length;
      http.setFixedLengthStreamingMode(length);
      http.connect();
   
      try(OutputStream os = http.getOutputStream()) {
         os.write(out);

         OrderedJSONObject res = null;
         StringBuilder response = new StringBuilder();
         BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
         String responseLine = null;
         while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
         }
         if(response.length() > 0) {
            res = new OrderedJSONObject(response.toString());
         } else {
            res = new OrderedJSONObject();
         }
         System.out.println(res.toString());
         if(res.has("_embedded")) {
            OrderedJSONObject embeddedObj = (OrderedJSONObject)res.get("_embedded");
            JSONArray wfList = (JSONArray)embeddedObj.get("workflowBatches");
            if(wfList.size() > 0) {
               OrderedJSONObject wfObj = (OrderedJSONObject)wfList.get(0);
               workflowId = (String)wfObj.get("uuid");
            }
         }

         http.disconnect();
         conn.commit();
      } catch(Exception ex) {
         ex.printStackTrace();
      }
      //System.out.println("Id:"+workflowId);
      return workflowId;
   }

   private void getClientId() throws Exception {
      String clientUrl = rsurl;
      URL url = new URL(clientUrl);
      HttpURLConnection http = (HttpURLConnection)url.openConnection();
      http = (HttpURLConnection)url.openConnection();
      http.setRequestMethod("GET");
      http.setDoOutput(true);
      http.setRequestProperty("x-api-key", API_KEY);
      http.setRequestProperty("content-type", "application/json");

      OrderedJSONObject filterRequest = new OrderedJSONObject();
      filterRequest.put("page", new Integer(0));
      filterRequest.put("size", new Integer(50));
   
      byte[] out = filterRequest.toString().getBytes(StandardCharsets.UTF_8);
      int length = out.length;
      http.setFixedLengthStreamingMode(length);
      http.connect();
   
      try(OutputStream os = http.getOutputStream()) {
         os.write(out);
         OrderedJSONObject res = null;
         StringBuilder response = new StringBuilder();
         BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
         String responseLine = null;
         while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
         }
         if(response.length() > 0) {
            res = new OrderedJSONObject(response.toString());
         } else {
            res = new OrderedJSONObject();
         }
         System.out.println(res.toString());
         http.disconnect();
      } catch(Exception ex) {
         ex.printStackTrace();
      }
   }

   private String search(String ids, String scanPeriod) throws Exception {
      System.out.println("In search..."+ids+":"+scanPeriod);
      String workflowId = "";
      OrderedJSONObject statusList = new OrderedJSONObject();

      //String searchUrl = rsurl+"/"+clientsId+"/workflowBatch/search";
      String searchUrl = rsurl+"/"+clientId+"/hostFinding/search";
      URL url = new URL(searchUrl);
      HttpURLConnection http = (HttpURLConnection)url.openConnection();
      http = (HttpURLConnection)url.openConnection();
      http.setRequestMethod("POST");
      http.setDoOutput(true);
      http.setRequestProperty("x-api-key", API_KEY);
      http.setRequestProperty("content-type", "application/json");

      // Workflow Search - {"filters":[{"field":"name","exclusive":false,"operator":"IN","orWithPrevious":false,"implicitFilters":[],"value":"RSTEST01"}],"projection":"internal","sort":[{"field":"created","direction":"DESC"}],"page":0,"size":50}
      OrderedJSONObject filterRequest = new OrderedJSONObject();
      OrderedJSONObject filter = new OrderedJSONObject();
      filter.put("field", "id");
      filter.put("exclusive", new Boolean(false));
      filter.put("operator", "IN");
      filter.put("orWithPrevious", new Boolean(false));
      filter.put("implicitFilters", new JSONArray());
      filter.put("value", ids);
      JSONArray filterList = new JSONArray();
      filterList.add(filter);
      filterRequest.put("filters", filterList);
      filterRequest.put("projection", "internal");
      //filterRequest.put("projection", "basic");
      filterRequest.put("page", new Integer(0));
      filterRequest.put("size", new Integer(50));
      OrderedJSONObject sort = new OrderedJSONObject();
      sort.put("field", "created");
      sort.put("direction", "DESC");
      JSONArray sortList = new JSONArray();
      sortList.add(sort);
      filterRequest.put("sort", sortList);

      byte[] out = filterRequest.toString().getBytes(StandardCharsets.UTF_8);
      int length = out.length;
      http.setFixedLengthStreamingMode(length);
      http.connect();
   
      try(OutputStream os = http.getOutputStream()) {
         os.write(out);

         OrderedJSONObject res = null;
         StringBuilder response = new StringBuilder();
         BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
         String responseLine = null;
         while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
         }
         if(response.length() > 0) {
            res = new OrderedJSONObject(response.toString());
         } else {
            res = new OrderedJSONObject();
         }
         System.out.println(res.toString());
         if(res.has("_embedded")) {
            OrderedJSONObject embeddedObj = (OrderedJSONObject)res.get("_embedded");
            if(embeddedObj.has("hostFindings")) {
               JSONArray hostFindings = (JSONArray)embeddedObj.get("hostFindings");
               Iterator iter = hostFindings.iterator();
               while(iter.hasNext()) {
                  OrderedJSONObject hostFinding = (OrderedJSONObject)iter.next();
                  System.out.println(hostFinding);
                  String scanId = ((Integer)hostFinding.get("id")).toString();
                  String status = (String)hostFinding.get("status");
                  double riskRating = (Double)hostFinding.get("riskRating");
                  String firstIngestedOn = (String)hostFinding.get("platformFirstIngestedOn");
                  String lastIngestedOn = (String)hostFinding.get("platformLastIngestedOn");
                  String vrrUpdatedOn = (String)hostFinding.get("vrrUpdatedOn");
                  String resolvedOn = (String)hostFinding.get("resolvedOn");
                  JSONArray wfNames = (JSONArray)hostFinding.get("workflowGeneratedNames");
                  OrderedJSONObject wfList = (OrderedJSONObject)hostFinding.get("workflowDistribution");
                  System.out.println("-----------------");
                  System.out.println(scanId+":"+status);
                  System.out.println(wfNames);
                  System.out.println(wfList);
                  System.out.println("-----------------");
                  if(status.equalsIgnoreCase("Closed")) {
                     JSONArray approvedWf = (JSONArray)wfList.get("approvedWorkflows");
                     if(approvedWf.size() == 1) {
                        OrderedJSONObject wfObj = (OrderedJSONObject)approvedWf.get(0);
                        String type = (String)wfObj.get("type");
                        if(type.equalsIgnoreCase("acceptance")) {
                           status = "Risk Acceptance";
                        } else if(type.equalsIgnoreCase("false_positive")) {
                           status = "False Positive";
                        } else {
                           status = "Complete";
                        }
                     } else {
                        status = "Complete";
                     }
                  } else if(status.equalsIgnoreCase("Open")) {
                     status = "Active";
                  }

                  String priority = "Info";
                  if(riskRating > 0 && riskRating <= 4) {
                     priority = "Low";
                  } else if(riskRating > 4 && riskRating <= 7) {
                     priority = "Medium";
                  } else if(riskRating > 7 && riskRating <= 9) {
                     priority = "High";
                  } else if(riskRating > 9 && riskRating <= 10) {
                     priority = "Critical";
                  } 

                  System.out.println(scanId+":"+status+":"+riskRating+":"+priority);

                  // Update Vulnerability Status
                  updateStatus(scanId, hostFinding, status, priority, firstIngestedOn, lastIngestedOn, vrrUpdatedOn, resolvedOn, wfNames, wfList, scanPeriod);
               }
            }
         } else { // Exists in SmarterD but, not in Vulnerability Tool
            /*
            System.out.println("Updating Status to Complete!!!");
            String updatesql = "update enterprise_vulnerability_log set extension = json_set(extension, '$.THREAT.status', 'Complete') where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = ?";
            PreparedStatement pst = conn.prepareStatement(updatesql);
            pst.setString(1, ids);
            pst.setString(2, scanPeriod);
            pst.executeUpdate();
            pst.close();
            */
            throw new Exception("Exiting: Data not found in RiskSense!!!");
         }
         conn.commit();
         http.disconnect();
      } catch(Exception ex) {
         ex.printStackTrace();
      }
      //System.out.println("Id:"+workflowId);
      return workflowId;
   }

   private void updateStatus(String scanId, OrderedJSONObject hostFinding, String status, String priority, String firstIngestedOn, String lastIngestedOn, String vrrUpdatedOn, String resolvedOn, JSONArray wfNames, OrderedJSONObject wfList, String scanPeriod) throws Exception {
      System.out.println("In updateStatus..."+scanId+":"+status+":"+priority);
      String sql = "select id, priority, vulnerability_id, vulnerability_name, extension from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = ?";
      String updatesql = "update enterprise_vulnerability_log set priority = ?, extension = ? where id = ?";
      String updatevulnrsql = "update enterprise_vulnerability set priority = ? where _id = ?";
      String currentPriority = null, vulnrLogId = null, vulnrId = null, vulnrName = "", ext = null;

      // Format Dates
      firstIngestedOn = formatUKDateFormat(firstIngestedOn);
      lastIngestedOn = formatUKDateFormat(lastIngestedOn);
      vrrUpdatedOn = formatUKDateFormat(vrrUpdatedOn);
      if(resolvedOn != null && resolvedOn.length() > 0) {
         resolvedOn = formatUKDateFormat(resolvedOn);
      } else {
         resolvedOn = "";
      }

      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, scanId);
      pst.setString(2, scanPeriod);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         vulnrLogId = rs.getString("id");
         currentPriority = rs.getString("priority");
         vulnrId = rs.getString("vulnerability_id");
         vulnrName = rs.getString("vulnerability_name");
         ext = rs.getString("extension");
      }
      rs.close();
      pst.close();

      if(vulnrLogId != null) {
         // Update Status and Workflow Details
         OrderedJSONObject extObj = new OrderedJSONObject(ext);
         OrderedJSONObject threatObj = (OrderedJSONObject)extObj.get("THREAT");

         String currentStatus = (String)threatObj.get("status");
         System.out.println(currentPriority+":"+priority+":"+currentStatus+":"+status);
         
         //fw.write(currentPriority+":"+priority+":"+currentStatus+":"+status);
         threatObj.put("First Discovered On", firstIngestedOn);
         threatObj.put("Last Discovered On", lastIngestedOn);
         threatObj.put("Resolved On", resolvedOn);
         threatObj.put("status", status);
         extObj.put("THREAT", threatObj);
         extObj.put("VRR Updated On", vrrUpdatedOn);

         pst = conn.prepareStatement(updatesql);
         pst.setString(1, priority);
         pst.setString(2, extObj.toString());
         pst.setString(3, vulnrLogId);
         pst.executeUpdate();
         pst.close();

         // Update Vulnerability Priority
         pst = conn.prepareStatement(updatevulnrsql);
         pst.setString(1, priority);
         pst.setString(2, vulnrId);
         pst.executeUpdate();
         pst.close();

         // Update workflow table
         if(wfNames.size() > 0) {
            updateWorkflow(scanId, vulnrId, vulnrName, wfNames, wfList, scanPeriod, priority);
         }
      } else { // Exists in Vulnerability Tool but, not in SmarterD
         System.out.println("Not Found...adding...");
         vulnrName = (String)hostFinding.get("title");
         String desc = (String)hostFinding.get("description");
         String source = (String)hostFinding.get("source");
         JSONArray patchList = (JSONArray)hostFinding.get("patches");
         String mitigation = (String)hostFinding.get("solution");
         String riskRating = ((Double)hostFinding.get("riskRating")).toString();
         String notes = (String)hostFinding.get("output");
         String severity = ((Double)hostFinding.get("severity")).toString();
         OrderedJSONObject severityObj = (OrderedJSONObject)hostFinding.get("severityEmbedded");
         String severityLevel = (String)severityObj.get("scanner");
         if(severity == "0") {
            severityLevel = "Info";
         }
         String lastseen = (String)hostFinding.get("lastFoundOn");
         String findingType = (String)hostFinding.get("findingType");
         String port = "";
         if(hostFinding.get("port") != null) {
            port = ((Integer)hostFinding.get("port")).toString();
         }
         OrderedJSONObject hostObj = (OrderedJSONObject)hostFinding.get("host");
         String hostName = (String)hostObj.get("hostName");
         String hostId = ((Integer)hostObj.get("hostId")).toString();
         String ipAddress = (String)hostObj.get("ipAddress");
         JSONArray serviceList = (JSONArray)hostFinding.get("services");
         String scannerOutput = (String)hostFinding.get("output");
         
         JSONArray portList = new JSONArray();
         if(port != null && port.length() > 0) {
            portList.add(port);
         }
         JSONArray cves = new JSONArray();

         if(notes == null) {
            notes = "";
         }

         JSONArray patchUrl = new JSONArray();
         Iterator iter = patchList.iterator();
         while(iter.hasNext()) {
            OrderedJSONObject patchObj = (OrderedJSONObject)iter.next();
            String url = (String)patchObj.get("url");
            patchUrl.add(url);
         }

         // Check if Vulnerability is there
         String checkvulnrsql = "select _id from enterprise_vulnerability where nameId = ?";
         pst = conn.prepareStatement(checkvulnrsql);
         pst.setString(1, vulnrName);
         rs = pst.executeQuery();
         while(rs.next()) {
            vulnrId = rs.getString("_id");
         }
         rs.close();
         pst.close();

         if(vulnrId == null) {   // Add Vulnerability
            vulnrId = generateUUID();
            JSONArray lastseenList = new JSONArray();
            if(lastseen != null && lastseen.length() > 0) {
               lastseenList.add(lastseen);
            }

            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("notes", notes);
            extObj.put("CVSS 2.0", "");
            extObj.put("CVSS 3.0", "");
            extObj.put("severity", severity);
            extObj.put("last_seen", lastseenList);
            extObj.put("lifecycle", "Active");
            extObj.put("CVEs Associated", cves);
            extObj.put("Ticket Reference", new JSONArray());
            extObj.put("Patch Title", "");
            extObj.put("Possible Patches", patchList);
            extObj.put("mitigationStatus", "");
            extObj.put("enterprise_action", mitigation);
            extObj.put("Vulnerability Type", "");
            extObj.put("Vulnerability Risk Rating", riskRating);
            extObj.put("Scanner Reported Severity", severityLevel);
            extObj.put("First Ingested On", firstIngestedOn);
            extObj.put("Last Ingested On", lastIngestedOn);
            extObj.put("Finding Type", findingType);
            extObj.put("Evidence", "");
            extObj.put("Exploits", "");

            String insertsql = "insert into enterprise_vulnerability(_id, nameId, description,priority,source,extension,createdBy,updatedBy) values(?,?,?,?,?,?,?,?)";
            pst = conn.prepareStatement(insertsql);
            pst.setString(1, vulnrId);
            pst.setString(2, vulnrName);
            pst.setString(3, desc);
            pst.setString(4, priority);
            pst.setString(5, source);
            pst.setString(6, extObj.toString());
            pst.setString(7, "support@smarterd.com");
            pst.setString(8, "support@smarterd.com");
            pst.executeUpdate();
            pst.close();
         }

         // Get Asset Detail
         String assetsql = "select id,asset_name,asset_location_name,lifecycle_name,owner,json_unquote(json_extract(extension,'$.\"Type\"')) type,json_unquote(json_extract(extension,'$.\"Sub-Type\"')) subtype,json_unquote(json_extract(extension,'$.\"OS\"')) os,json_unquote(json_extract(extension,'$.\"Environment\"')) env,json_unquote(json_extract(extension,'$.\"OEM Vendor\"')) make,json_unquote(json_extract(extension,'$.\"Model\"')) model,json_extract(extension,'$.\"Application Support Queue\"') app_queue,json_extract(extension,'$.\"Infra Support Queue\"') infra_queue,json_unquote(json_extract(extension,'$.Manager')) manager,json_unquote(json_extract(extension,'$.\"Technical Owner\"')) tech_owner from enterprise_asset where json_contains(json_unquote(json_extract(extension,'$.\"IP Address\"')) , '\"REPLACE_STRING\"') > 0 and lifecycle_name != 'Decommissioned'";
         OrderedJSONObject assetObj = new OrderedJSONObject();
         String assetId = null, assetName = null;
         assetsql = assetsql.replaceAll("REPLACE_STRING", ipAddress);
         PreparedStatement pst1 = conn.prepareStatement(assetsql);
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
            assetId = rs1.getString("id");
            assetName = rs1.getString("asset_name");
            String owner = rs1.getString("owner");
            String manager = rs1.getString("manager");
            String techOwner = rs1.getString("tech_owner");
            String loc = rs1.getString("asset_location_name");
				String lc = rs1.getString("lifecycle_name");
				String type = rs1.getString("type");
				String subtype = rs1.getString("subtype");
				String os = rs1.getString("os");
				String env = rs1.getString("env");
				String vendor = rs1.getString("make");
				String model = rs1.getString("model");
				String appQueue = rs1.getString("app_queue");
				String infraQueue = rs1.getString("infra_queue");

            JSONArray infraQueueList = null;
            if(infraQueue != null && infraQueue.length() > 0) {
                infraQueueList = new JSONArray(infraQueue);
            } else {
                infraQueueList = new JSONArray();
            }
            JSONArray appQueueList = null;
            if(appQueue != null && appQueue.length() > 0) {
                appQueueList = new JSONArray(appQueue);
            } else {
                appQueueList = new JSONArray();
            }

            if(owner == null) {
                owner = "";
            }
            if(manager == null) {
                manager = "";
            }
            JSONArray techOwnerList = null;
            if(techOwner != null && techOwner.length() > 0) {
                techOwnerList = new JSONArray(techOwner);
            } else {
                techOwnerList = new JSONArray();
            }

            assetObj.put("asset_location_name", loc);
				assetObj.put("lifecycle_name", lc);
				assetObj.put("Type", type);
				assetObj.put("Sub-Type", subtype);
				assetObj.put("OS", os);
				assetObj.put("Environment", env);
            assetObj.put("OEM Vendor", vendor);
            assetObj.put("Model", model);
            assetObj.put("Application Support Queue", appQueueList);
            assetObj.put("Infra Support Queue", infraQueueList);
            assetObj.put("Owner", owner);
            assetObj.put("Manager", manager);
            assetObj.put("Technical Owner", techOwner);
			}
			rs1.close();
			pst1.close();

         // Insert into Enterprise Vulnerability Asset
         if(assetId == null) {
            assetId = generateUUID();
            String assetinsertsql = "insert into enterprise_vulnerability_asset(id,rowId,asset_name,asset_category,extension,createdBy) values(?,uuid(),?,?,?,?)";
            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("sync", "N");
            extObj.put("reviewed", "N");
            extObj.put("scan_period", scanPeriod);
            extObj.put("hostname", hostName);
            pst = conn.prepareStatement(assetinsertsql);
            pst.setString(1, assetId);
            pst.setString(2, ipAddress);
            pst.setString(3, "IT ASSET");
            pst.setString(4, extObj.toString());
            pst.setString(5, "support@smarterd.com");
            pst.executeUpdate();
            pst.close();
         }

         // Insert Host
         OrderedJSONObject threatCtx = new OrderedJSONObject();
         threatCtx.put("scan_id", scanId);
         threatCtx.put("status", status);
         threatCtx.put("Ticket Reference", "");
         threatCtx.put("notes", notes);
         threatCtx.put("assignee", new JSONArray());
         threatCtx.put("IP Address", ipAddress);
         threatCtx.put("Port", portList);
         threatCtx.put("Service", serviceList);
         threatCtx.put("Evidence", "");
         threatCtx.put("source", source);
         threatCtx.put("Host Id", hostId);
         threatCtx.put("lifecycle", "Active");
         threatCtx.put("Scanner Output", scannerOutput);
         threatCtx.put("group_name", "");
         threatCtx.put("Vulnerability Type", "");
         threatCtx.put("First Discovered On", firstIngestedOn);
         threatCtx.put("Last Discovered On", lastIngestedOn);
         threatCtx.put("Resolved On", resolvedOn);

         OrderedJSONObject extObj = new OrderedJSONObject();
         extObj.put("asset_status", "");
         extObj.put("scan_period", scanPeriod);
         extObj.put("scan_status", "Open");
         extObj.put("sync_id", "");
         extObj.put("scan_sync", "");
         extObj.put("sync_name", "");
         extObj.put("sync_status", "");
         extObj.put("THREAT", threatCtx);
         extObj.put("ASSET", assetObj);
         extObj.put("VRR Updated On", vrrUpdatedOn);
         DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
         LocalDate ldt = LocalDate.now();
         String dt = ldt.format(df);
         String insertsql = "insert into enterprise_vulnerability_log(id,vulnerability_id,vulnerability_name,asset_id,asset_name,ipaddress,hostname,priority,status,load_date,source,extension,created_timestamp) values(uuid(),?,?,?,?,?,?,?,?,?,?,?,current_timestamp)";	
         pst = conn.prepareStatement(insertsql);
         pst.setString(1, vulnrId);
         pst.setString(2, vulnrName);
         pst.setString(3, assetId);
         pst.setString(4, assetName);
         pst.setString(5, ipAddress);                     
         pst.setString(6, hostName);
         pst.setString(7, priority);
         pst.setString(8, "New");
         pst.setString(9, dt);
         pst.setString(10, source);
         pst.setString(11, extObj.toString());
         pst.executeUpdate();
         pst.close();

         // Update workflow table
         if(wfNames.size() > 0) {
            updateWorkflow(scanId, vulnrId, vulnrName, wfNames, wfList, scanPeriod, priority);
         }

         System.out.println("Successfully added Vulnerability!!!");
      }
   }

   private void updateWorkflow(String scanId, String vulnrId, String vulnrName, JSONArray wfNames, OrderedJSONObject wfList, String scanPeriod, String priority) throws Exception {
      String insertwfsql = "insert into enterprise_vulnerability_workflow(id,scan_id,vulnerability_id,vulnerability_name,workflow_name,workflow_id,workflow_type,acronym,workflow_state,scan_period,priority,created_by,updated_by) values(uuid(),?,?,?,?,?,?,?,?,?,?,?,?)";      
      String wfsql = "select id, workflow_state from enterprise_vulnerability_workflow where scan_id = ? and workflow_id = ?";
      String updatewfsql = "update enterprise_vulnerability_workflow set workflow_state=?,workflow_type=?,scan_period=?,priority=? where id = ?";

      Iterator iter = wfList.keySet().iterator();
      while(iter.hasNext()) {
         String wfAction = (String)iter.next();
         if(wfList.get(wfAction) instanceof JSONArray) {
            JSONArray actionList = (JSONArray)wfList.get(wfAction); 
            if(actionList != null) {
               Iterator actionIter = actionList.iterator();
               while(actionIter.hasNext()) {
                  OrderedJSONObject actionObj = (OrderedJSONObject)actionIter.next();
                  String wfActionId = "", type = "", acronym = "", state = "";
                  if(actionObj.has("generatedId")) {
                     wfActionId = (String)actionObj.get("generatedId");
                  }
                  if(actionObj.has("type")) {
                     type = (String)actionObj.get("type");
                  }
                  if(actionObj.has("acronym")) {
                     acronym = (String)actionObj.get("acronym");
                  }
                  if(actionObj.has("state")) {
                     state = (String)actionObj.get("state");
                  }

                  String wfId = null, wfState = "";
                  PreparedStatement pst = conn.prepareStatement(wfsql);
                  pst.setString(1, scanId);
                  pst.setString(2, wfActionId);
                  ResultSet rs = pst.executeQuery();
                  while(rs.next()) {
                     wfId = rs.getString("id");
                     wfState = rs.getString("workflow_state");
                  }
                  rs.close();
                  pst.close();
                  if(wfState == null) {
                     wfState = "";
                  }

                  if(wfId == null) {
                     pst = conn.prepareStatement(insertwfsql);
                     pst.setString(1, scanId);
                     pst.setString(2, vulnrId);
                     pst.setString(3, vulnrName);
                     pst.setString(4, wfActionId);
                     pst.setString(5, wfActionId);
                     pst.setString(6, type);
                     pst.setString(7, acronym);
                     pst.setString(8, state);
                     pst.setString(9, scanPeriod);
                     pst.setString(10, priority);
                     pst.setString(11, "support@smarter.com");
                     pst.setString(12, "support@smarterd.com");
                     pst.executeUpdate();
                     pst.close();
                  } else if(!state.equalsIgnoreCase(wfState)) {   // Update only if the state has changed
                     pst = conn.prepareStatement(updatewfsql);
                     pst.setString(1, state);
                     pst.setString(2, type);
                     pst.setString(3, scanPeriod);
                     pst.setString(4, priority);
                     pst.setString(5, wfId);
                     pst.executeUpdate();
                     pst.close();
                  }
               }
            }
         }
      }
   }

   private void validate(String filename) throws Exception {
      String[] row;
		int count = 0, rowCount = 0, totalVulnrCount = 0;
      JSONArray vulnrList = new JSONArray();

		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String scanId="", priority="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						scanId = row[i].trim();
                  vulnrList.add(scanId);
                  count++;
					}
				}
            //if(count == 1) {
            String ids = vulnrList.toString().replaceAll("[\\[\\]]", "").replaceAll("\"","");
            search(ids, "NOV-22");
            //count = 0;
            vulnrList.clear();
            //break;
            //}
         }
      }
   }

   private void updateVulnerabilityPriority(String filename) throws Exception {
		String[] row;
		int rowCount = 0, totalVulnrCount = 0;
		CSVReader reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		while ((row = reader.readNext()) != null) {
			rowCount++;
			String scanId="", priority="";
			if (rowCount > 1) {
				for(int i=0; i<row.length; i++) {
					if(i==0) {		
						scanId = row[i].trim();
					} else if(i==1) {
						priority = row[i].trim();;
					} 
				}
				//System.out.println(scanId+":"+priority);
				// Update Log
				String updatesql = "update enterprise_vulnerability_log set priority = ?, extension=json_set(extension,'$.THREAT.status', 'Active') where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
				PreparedStatement pst = conn.prepareStatement(updatesql);
				pst.setString(1, priority);
				pst.setString(2, scanId);
				pst.executeUpdate();
				pst.close();

            /*
            int count = 0;
            String checksql = "select count(*) count from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
            PreparedStatement pst = conn.prepareStatement(checksql);
            pst.setString(1, scanId);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
               count = rs.getInt("count");
            }
            rs.close();
            pst.close();

            if(count == 0) {
               System.out.println(scanId);
            }
            */
            /*
				String updatesql = "update enterprise_vulnerability_log set extension=json_set(extension,'$.matched', true), extension=json_set(extension,'$.THREAT.status', 'Active') where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = 'NOV-22'";
            PreparedStatement pst = conn.prepareStatement(updatesql);
				pst.setString(1, scanId);
				pst.executeUpdate();
				pst.close();
            */
			}
		}
	}

   private String getCurrentScanPeriod() throws Exception {
      String settingsql = "select setting_name,setting_value,json_unquote(json_extract(extension,'$.value_order')) setting_order from enterprise_setting where setting_category = 'Vulnerability Scan Period' and setting_name != '' order by setting_order asc";
      String periodsql = "select distinct json_unquote(json_extract(extension, '$.scan_period')) scan_period,str_to_date(json_unquote(json_extract(extension,'$.THREAT.\"Last Discovered On\"')), '%m/%d/%Y') dt from enterprise_vulnerability_log order by dt desc";
      String currentPeriod = null;
      PreparedStatement pst = conn.prepareStatement(settingsql);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String name = rs.getString("setting_name");
         String scanPeriod = rs.getString("setting_value");
         if(name.equalsIgnoreCase("Current Period")) {
            currentPeriod = scanPeriod;
         }
      }
      rs.close();
      pst.close();

      if(currentPeriod == null) {    // Backup logic, if setting is not configured
         pst = conn.prepareStatement(periodsql);
         rs = pst.executeQuery();
         while(rs.next()) {
            currentPeriod = rs.getString("scan_period");
            break;
         }
         rs.close();
         pst.close();
      }

      return currentPeriod;
   }

   public static void main(String args[]) throws Exception {
      String filename = args[0];
      String companyCode = args[1];
      String env = args[2];

      //SmarteRiskSense rs = (SmarteRiskSense)new SmarteLoader(companyCode);

      /*
      String name = "Terminal Services Encryption Level is Medium or Low";
      String desc = "Terminal Services Encryption Level is Medium or Low";
      String type = "Complete";
      String reason = "Ticket Ref: SCTASK0015359";
      String priority = "Low";
      JSONArray idList = new JSONArray();
      idList.add("313308239");
      idList.add("313308453");
      idList.add("313308545");
      idList.add("313308690");
      OrderedJSONObject res = rs.sync(name, desc, type, reason, idList, priority);
      */
      String name = "SmarterD Test-1";
      String desc = "SmarterD Test-1";
      String type = "Complete";
      String reason = "Ticket Ref: SCTASK0015359";
      String priority = "Medium";
      JSONArray idList = new JSONArray();
      idList.add("666639938");
      //idList.add("313308453");
      //idList.add("313308545");
      //idList.add("313308690");
      //OrderedJSONObject res = rs.sync(name, desc, type, reason, idList, priority);

      //String id = rs.searchWorkflow("68");
      //String id = rs.search("893365907,666637715");
      
      //rs.syncStatus("JUL-23");

      //rs.getClientId();
      //rs.validate();
      //rs.updateVulnerabilityPriority();
   }
}