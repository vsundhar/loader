import java.sql.*;
import java.util.*;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;

public class ApplicationLoader {
    private String updatedBy = null;
    private String companyCode = null;
    private String hostname = "localhost";
    private Connection conn = null;
    private CSVReader reader = null;

    public ApplicationLoader(String filename, String companyCode, String env) throws Exception {
        this.companyCode = companyCode;

        reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);

        if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    private void load() throws Exception {
        System.out.println("Loading Application...");
        String[] line;
        int rowCount=0;
        while ((line = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
                String sla=null, appType=null, appOwner=null, itStakeholder=null, appName=null, vendor=null, ecosystem=null, deploymentModel=null, businessArea=null, notes=null, desc=null;
				System.out.println("Processing row-" + rowCount + "...");
				
				for(int i=0; i<line.length; i++) {
					if (i==0) {
						sla = line[i].trim();
					} else if (i==1) {
						appType = line[i].trim();
					} else if (i==2) {
						
					} else if (i==3) {
						
					} else if (i==4) {
						
					} else if (i==5) {
						
					} else if (i==6) {
						
					} else if (i==7) {
						
					} else if (i==8) {
						
					} else if (i==9) {
						
					} else if (i==10) {
						
					} else if (i==11) {
						
					} else if (i==12) {
						itStakeholder = line[i].trim();
					} else if (i==13) {
						appName = line[i];
					} else if (i==14) {
						vendor = line[i].trim();
					} else if (i==15) {
						ecosystem = line[i].trim();
					} else if (i==16) {
						deploymentModel = line[i].trim();
					} else if (i==17) {
						businessArea = line[i].trim();
					} else if (i==18) {
						notes = line[i].trim();
					} else if (i==19) {
						desc = line[i].trim();
					}
				}

				JSONArray ecoArr = new JSONArray();
				if(ecosystem != null && ecosystem.length() > 0) {
					ecoArr.add(ecosystem);
				} else {
					ecoArr = new JSONArray();
				}
				String criticality = null;
				if(sla.equalsIgnoreCase("Gold")) {
					criticality = "Critical";
				} else if(sla.equalsIgnoreCase("Silver")) {
					criticality = "High";
				} else if(sla.equalsIgnoreCase("Bronze")) {
					criticality = "Medium";
				} else {
					criticality = "Low";
				}

				String name = "";
				if(itStakeholder.indexOf("Prasad") != -1) {
					name = "Prasad";
				} else if(itStakeholder.indexOf("Gopal") != -1) {
					name = "Gopal";
				} else if(itStakeholder.indexOf("Magesh") != -1) {
					name = "Magesh";
				} else if(itStakeholder.indexOf("Bharat") != -1) {
					name = "Bharat";
				} else if(itStakeholder.indexOf("Vaibhav") != -1) {
					name = "Vaibhav";
				} else if(itStakeholder.indexOf("Poornima") != -1) {
					name = "Poornima";
				} else if(itStakeholder.indexOf("Gangadhar") != -1) {
					name = "Gangadhar";
				} else if(itStakeholder.indexOf("Sanya") != -1) {
					name = "Sanya";
				} else if(itStakeholder.indexOf("Harmeet") != -1) {
					name = "Hatmeet";
				} else if(itStakeholder.indexOf("Kaushik") != -1) {
					name = "Kaushik";
				} else {
					name = "";
				}
				
				if(businessArea.indexOf("Finance") != -1) {
					businessArea = "FINANCE";
				} else if(businessArea.indexOf("Human") != -1) {
					businessArea = "HUMAN RESOURCES";
				} else if(businessArea.indexOf("Technical") != -1) {
					businessArea = "INFORMATION TECHNOLOGY";
					appOwner = "Fritz Debrine";
				} else if(businessArea.indexOf("Identity") != -1) {
					businessArea = "IT SECURITY";
					appOwner = "Hans Schwarz";
				} else if(businessArea.indexOf("Security") != -1) {
					businessArea = "IT SECURITY";
					appOwner = "Hans Schwarz";
				} else if(businessArea.indexOf("Supplychain") != -1) {
					businessArea = "SUPPLY CHAIN";
					appOwner = "Myrna Barthel";
				} else if(businessArea.indexOf("Analytics") != -1) {
					businessArea = "ANALYTICS";
					appOwner = "Jon Forsman";
				} else if(businessArea.indexOf("Merchandising") != -1) {
					businessArea = "MERCHANDISING";
					appOwner = "Myrna Barthel";
				} else if(businessArea.indexOf("eComm") != -1) {
					businessArea = "ECOMMERCE";
					appOwner = "Rod Brehm";
				} else if(businessArea.indexOf("Store") != -1) {
					businessArea = "STORE OPERATIONS";
					appOwner = "Myrna Barthel";
				} else if(businessArea.indexOf("Real Estate") != -1) {
					businessArea = "REAL ESTATE";
					appOwner = "Myrna Barthel";
				} else if(businessArea.indexOf("CRM") != -1) {
					businessArea = "MARKETING";
				} else if(businessArea.indexOf("Integration") != -1) {
					businessArea = "INTEGRATION";
				} else {
					businessArea = "";
				}
               
				if(appName.length() > 0) {
					// Select resource detail
					String resourceId = null, resourceName = "";
					String resourcesql = "select id, asset_name from enterprise_asset where asset_name like ?";
					PreparedStatement pst = conn.prepareStatement(resourcesql);
					pst.setString(1, name+"%");
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						resourceId = rs.getString("id");
						resourceName = rs.getString("asset_name");
					}
					rs.close();
					pst.close();

					OrderedJSONObject managerObj = new OrderedJSONObject();
					if(resourceId != null) {
						managerObj.put("id", resourceId);
						managerObj.put("asset_name", resourceName);
						managerObj.put("prev_id", "");
						managerObj.put("prev_asset_name", "");
					}

					// Check Asset
					String assetsql = "select id, extension from enterprise_asset where asset_name = ?";
					String id = null, ext = null;
					pst = conn.prepareStatement(assetsql);
					pst.setString(1, appName);
					rs = pst.executeQuery();
					while(rs.next()) {
						id = rs.getString("id");
						ext = rs.getString("extension");
					}
					rs.close();
					pst.close();

					if(id != null) {
						System.out.println("Updating asset..."+appName);
						String updatesql = "update enterprise_asset set asset_desc = ?, owner=?, extension = ? where id = ?";
						OrderedJSONObject extObj = new OrderedJSONObject(ext);
						extObj.put("SLA Level", sla);
						extObj.put("Criticality", criticality);
						extObj.put("Application Deployment Model", deploymentModel);
						extObj.put("Business Unit", businessArea);
						extObj.put("OEM Vendor", vendor);
						extObj.put("ecosystem", ecoArr);
						extObj.put("notes", notes);
						extObj.put("Manager", managerObj.toString());
						pst = conn.prepareStatement(updatesql);
						pst.setString(1, desc);
						pst.setString(2, appOwner);
						pst.setString(3, extObj.toString());
						pst.setString(4, id);
						pst.executeUpdate();
						pst.close();
					} else {
						System.out.println("Inserting asset..."+appName);
						String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,owner,extension,created_by,created_timestamp,updated_by,company_code) values(uuid(),?,?,?,?,'support@smarterd.com',current_timestamp,'support@smarterd.com',?)";
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj.put("SLA Level", sla);
						extObj.put("Criticality", criticality);
						extObj.put("Application Deployment Model", deploymentModel);
						extObj.put("Business Unit", businessArea);
						extObj.put("OEM Vendor", vendor);
						extObj.put("ecosystem", ecoArr);
						extObj.put("notes", notes);
						extObj.put("Manager", managerObj.toString());
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, appName);
						pst.setString(2, desc);
						pst.setString(3, appOwner);
						pst.setString(4, extObj.toString());
						pst.setString(5, companyCode);
						pst.executeUpdate();
						pst.close();
					}
				}
            }
        }
		conn.commit();
    }

	private String checkApp(String appName) throws Exception {
		String sql = "select id from enterprise_asset where asset_name = ?";
		String id = null;
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, appName);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();

		return id;
	}

    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String filename = args[0];
		String companyCode = args[1];
        String env = args[2];
        
        ApplicationLoader loader = new ApplicationLoader(filename, companyCode, env);
        loader.load();
    } 
}