import java.util.*;

public class GenerateUUID {
	public GenerateUUID() {}

	public static void main(String[] args) throws Exception {
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid.toString());
	}
}