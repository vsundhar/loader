import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class DHADatabaseLoader {
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public DHADatabaseLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = CSVParser.parse(new FileReader(new File(filename+"_data.csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String appName=null, dbString=null;
		for(CSVRecord dataRow : dataReader) {
			if(dataRow.isConsistent()) {
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						appName = dataRow.get(i).trim();
					} else if(i==1) {
						dbString = dataRow.get(i).trim();
					}
				}
			}
			// Parse dbName 
			String serverName="", instanceName="", dbName="";
			if(dbString.indexOf("[") != -1) {
				String str = dbString.substring(1, dbString.indexOf("]"));
				dbName = dbString.substring(dbString.lastIndexOf("\\")+1);
				if(str.indexOf("\\") != -1) {
					String[] arr = str.split("\\\\");
					serverName = arr[0];
					instanceName = arr[1]; 
				} else {
					serverName = str;
				}
			} else if(dbString.indexOf("\\") != -1) {
				if(dbString.indexOf("\\") != -1) {
					String[] arr = dbString.split("\\\\");
					serverName = arr[0];
					dbName = arr[1]; 
				} 
			} else {
				dbName = dbString;
			}
			System.out.println(serverName+":"+instanceName+":"+dbName);

			// Get Asset Id
			String appId = null;
			String checksql = "select id from enterprise_asset where asset_name = ?";
			PreparedStatement pst = conn.prepareStatement(checksql);
			pst.setString(1, appName);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				appId = rs.getString("id");
			}
			rs.close();
			pst.close();
			System.out.println(appId);
			
			if(appId == null) {
				String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
				String desc = "Application added as part of database loading 9_Custom_EA_DatabasesList - Review and Update.";
				appId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj = initializeApplicationExtension(extObj);
				extObj.put("custom", new OrderedJSONObject());
				pst = conn.prepareStatement(insertsql);
				pst.setString(1, appId);
				pst.setString(2, appName);
				pst.setString(3, desc);
				pst.setString(4, extObj.toString());
				pst.setString(5, companyCode);
				pst.executeUpdate();
				pst.close();
			}

			String assetId = null;
			if(serverName.length() > 0) {
				pst = conn.prepareStatement(checksql);
				pst.setString(1, serverName);
				rs = pst.executeQuery();
				while(rs.next()) {
					assetId = rs.getString("id");
				}
				rs.close();
				pst.close();
				System.out.println(assetId);

				if(assetId == null) {
					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
					String desc = "IT Asset added as part of database loading 9_Custom_EA_DatabasesList - Review and Update.";
					assetId = generateUUID();
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj = initializeITAssetExtension(extObj);
					extObj.put("custom", new OrderedJSONObject());
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetId);
					pst.setString(2, serverName);
					pst.setString(3, desc);
					pst.setString(4, extObj.toString());
					pst.setString(5, companyCode);
					pst.executeUpdate();
					pst.close();
				}

				OrderedJSONObject ctx = new OrderedJSONObject();
				createRel(appId, assetId, "ASSET", "ASSET", ctx, "support@smarterd.com");
			}

			int id=0;
			String comment=null, ext=null;
			String checkfeaturesql = "select id, comment, extension from enterprise_comment where comp_id=? and comment_type='Recommendation' and TOPIC_NAME='DATABASE'";
			pst = conn.prepareStatement(checkfeaturesql);
			pst.setString(1, appId);
			rs = pst.executeQuery();
			while(rs.next()) {
				id = rs.getInt("id");
				comment = rs.getString("comment"); 
				ext = rs.getString("extension");
			}
			rs.close();
			pst.close();

			if(id == 0) {
				String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,company_code,created_by,updated_by,comment_address,status,comment_type,extension) values(null,?,'DATABASE',?,?,'support@smarterd.com','support@smarterd.com','',?,'Recommendation',?)";
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj.put("component_type", "APPLICATION");
				OrderedJSONObject statusObj = new OrderedJSONObject();
				statusObj.put("userGenerated", new Boolean(true));
				statusObj.put("applicable", "");
				statusObj.put("topiclink", "");
				statusObj.put("maturity", "");
				dbString = "<li><p>"+dbString+"</p></li>";
				pst = conn.prepareStatement(insertsql);
				pst.setString(1, appId);
				pst.setString(2, dbString);
				pst.setString(3, companyCode);
				pst.setString(4, statusObj.toString());
				pst.setString(5, extObj.toString());
				pst.executeUpdate();
				pst.close();
			} else {
				String updatesql = "update enterprise_comment set comment_type = 'Recommendation',comment = ?,extension=? where id = ?";
				comment = comment+"<li><p>"+dbString+"</p></li>";
				OrderedJSONObject extObj = new OrderedJSONObject(ext);
				extObj.put("component_type", "APPLICATION");
				pst = conn.prepareStatement(updatesql);
				pst.setString(1, comment);
				pst.setString(2, extObj.toString());
				pst.setInt(3, id);
				pst.executeUpdate();
				pst.close();
			}

			id = 0;
			comment = null;
			ext = null;
			if(assetId != null) {
				pst = conn.prepareStatement(checkfeaturesql);
				pst.setString(1, assetId);
				rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getInt("id");
					comment = rs.getString("comment"); 
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(id == 0) {
					String insertsql = "insert into enterprise_comment(id,comp_id,TOPIC_NAME,comment,company_code,created_by,updated_by,comment_address,status,comment_type,extension) values(null,?,'DATABASE',?,?,'support@smarterd.com','support@smarterd.com','',?,'Recommendation',?)";
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("component_type", "IT ASSET");
					OrderedJSONObject statusObj = new OrderedJSONObject();
					statusObj.put("userGenerated", new Boolean(true));
					statusObj.put("applicable", "");
					statusObj.put("topiclink", "");
					statusObj.put("maturity", "");
					dbString = "<li><p>"+dbString+"</p></li>";
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetId);
					pst.setString(2, dbString);
					pst.setString(3, companyCode);
					pst.setString(4, statusObj.toString());
					pst.setString(5, extObj.toString());
					pst.executeUpdate();
					pst.close();
				} else {
					String updatesql = "update enterprise_comment set comment_type = 'Recommendation',comment = ?,extension=? where id = ?";
					comment = comment+"<li><p>"+dbString+"</p></li>";
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("component_type", "IT ASSET");
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, comment);
					pst.setString(2, extObj.toString());
					pst.setInt(3, id);
					pst.executeUpdate();
					pst.close();
				}
			}
		}

		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private void createRel(String componentId, String assetId, String componentType, String assetType, OrderedJSONObject ctx, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}
    }


	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		DHADatabaseLoader loader = new DHADatabaseLoader(filename, companyCode, env);
		loader.load();
	}
}