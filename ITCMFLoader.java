import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class ITCMFLoader {
	private String domain = "IT-CMF";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public ITCMFLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		String previousParent = null;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String id=null, parentCapability=null, parentDesc=null, childCapability=null, desc=null, internalId=null;
				String assetClass="", priority="0";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						internalId = row[i].trim();
					} else if(i==1) {
						parentCapability = row[i].trim();
					} else if(i==2) {
						childCapability = row[i].trim();
					} else if(i==3) {
						desc = row[i].trim();
					}
				}
					
				System.out.println(internalId+":"+parentCapability+":"+childCapability+":"+desc);
				
				// Check Capability
				String parentId = null, childId=null;
				String capabilityId = checkCapability(parentCapability, childCapability);		
				if(capabilityId == null || capabilityId.length() == 0) {			
					if((previousParent == null || previousParent.equals(parentCapability)) && capabilityList.has(parentCapability)) {
						parentId = (String)capabilityList.get(parentCapability);
						childId = generateUUID();
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, assetClass, priority, "2", levelTwo);
						levelTwo++;
						capabilityList.put(childCapability, childId);
						previousParent = parentCapability;
					} else if(!previousParent.equals(parentCapability)) {
						levelTwo = 0;
						parentId = (String)capabilityList.get(parentCapability);
						childId = generateUUID();
						
						//insertCapability(parentId, rootId, id, "IT-CMF", parentCapability, parentDesc, "", "", "1", levelOne);
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, assetClass, priority, "2", levelTwo);
						//capabilityList.put(parentCapability, parentId);
						capabilityList.put(childCapability, childId);
						levelOne++;
						levelTwo++;
						previousParent = parentCapability;
					}	
				}
				
				System.out.println(parentId+":"+parentCapability+":"+childId+":"+childCapability);
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability("IT-CMF", "IT-CMF");
		
		if(rootId == null || rootId.length() == 0) {
			//rootId = generateUUID();
			rootId = "IT-CMF";
			insertCapability(rootId, rootId, "", "IT-CMF", "IT-CMF", "IT-CMF Controls", "", "", "0", 0);
			capabilityList.put("IT-CMF", rootId);
		}
		
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String assetClass, String priority, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "2";
   		String createdBy = "support@smarterd.com";
   		String owner = "", status = "Y";
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", parent);
		extObject.put("assetClass", assetClass);
		extObject.put("priority", priority);
		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		ITCMFLoader loader = new ITCMFLoader(filename, companyCode, env);
		loader.process();
	}
}