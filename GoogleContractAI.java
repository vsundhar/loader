import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
/*
import com.google.cloud.documentai.v1.Document;
import com.google.cloud.documentai.v1.DocumentProcessorServiceClient;
import com.google.cloud.documentai.v1.ProcessRequest;
import com.google.cloud.documentai.v1.ProcessResponse;
import com.google.cloud.documentai.v1.RawDocument;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
*/

public class GoogleContractAI {
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";

	String projectId = "664392006974";
    String location = "us"; // Format is "us" or "eu".
    String processerId = "38291933b171743";
    String filePath = "";
   	
	public GoogleContractAI(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		filePath = filename;
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		/*
		try (DocumentProcessorServiceClient client = DocumentProcessorServiceClient.create()) {
			// The full resource name of the processor, e.g.:
			// projects/project-id/locations/location/processor/processor-id
			// You must create new processors in the Cloud Console first
			String name =
				String.format("projects/%s/locations/%s/processors/%s", projectId, location, processorId);
	  
			// Read the file.
			byte[] imageFileData = Files.readAllBytes(Paths.get(filePath));
	  
			// Convert the image data to a Buffer and base64 encode it.
			ByteString content = ByteString.copyFrom(imageFileData);
	  
			RawDocument document =
				RawDocument.newBuilder().setContent(content).setMimeType("application/pdf").build();
	  
			// Configure the process request.
			ProcessRequest request =
				ProcessRequest.newBuilder().setName(name).setRawDocument(document).build();
	  
			// Recognizes text entities in the PDF document
			ProcessResponse result = client.processDocument(request);
			Document documentResponse = result.getDocument();
	  
			// Get all of the document text as one big string
			String text = documentResponse.getText();
	  
			// Read the text recognition output from the processor
			System.out.println("The document contains the following paragraphs:");
			Document.Page firstPage = documentResponse.getPages(0);
			List<Document.Page.Paragraph> paragraphs = firstPage.getParagraphsList();
	  
			for (Document.Page.Paragraph paragraph : paragraphs) {
			  String paragraphText = getText(paragraph.getLayout().getTextAnchor(), text);
			  System.out.printf("Paragraph text:\n%s\n", paragraphText);
			}
	  
			// Form parsing provides additional output about
			// form-formatted PDFs. You  must create a form
			// processor in the Cloud Console to see full field details.
			System.out.println("The following form key/value pairs were detected:");
	  
			for (Document.Page.FormField field : firstPage.getFormFieldsList()) {
			  String fieldName = getText(field.getFieldName().getTextAnchor(), text);
			  String fieldValue = getText(field.getFieldValue().getTextAnchor(), text);
	  
			  System.out.println("Extracted form fields pair:");
			  System.out.printf("\t(%s, %s))\n", fieldName, fieldValue);
			}
		}
		*/
		
		conn.commit();
		conn.close();
	}

	// Extract shards from the text field
	/*
	private static String getText(Document.TextAnchor textAnchor, String text) {
		if (textAnchor.getTextSegmentsList().size() > 0) {
			int startIdx = (int) textAnchor.getTextSegments(0).getStartIndex();
			int endIdx = (int) textAnchor.getTextSegments(0).getEndIndex();
			return text.substring(startIdx, endIdx);
		}
		return "[NO TEXT]";
	}
	*/
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		GoogleContractAI loader = new GoogleContractAI(filename, companyCode, env);
		loader.process();
	}
}