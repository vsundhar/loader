import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;
import java.text.DecimalFormat;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;

public class MADSVCMPlanTemplateLoader {
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public MADSVCMPlanTemplateLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = CSVParser.parse(new FileReader(new File(filename)), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		OrderedJSONObject planList = new OrderedJSONObject();
		OrderedJSONObject rDetailList = new OrderedJSONObject();
		DateTimeFormatter indf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter outdf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		DecimalFormat df = new DecimalFormat("0.00");
		String planinsertsql = "insert into SE.enterprise_threat_plan(_id,name,planType,category,startDate,endDate,completion,status,implementation,extension,createdBy,updatedBy) values(?,?,?,'',?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com')";
		String subplaninsertsql = "insert into SE.enterprise_threat_subplan(_id,name,description,planType,category,threatPlan,root,subplan,startDate,endDate,completion,status,implementation,extension,createdBy,updatedBy) values(?,?,?,?,'',?,?,?,?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com')";
		OrderedJSONObject subplanList = new OrderedJSONObject();

		OrderedJSONObject extObj = new OrderedJSONObject();
		extObj.put("custom", new OrderedJSONObject());
		extObj.put("fiscal_year", "");
		extObj.put("plannedStartDate", "");
		extObj.put("plannedEndDate", "");
		extObj.put("size", "");
		extObj.put("roadmap", "N");
		extObj.put("auto", "Y"); // To track roadmaps that was auto generated
		extObj.put("system_risk", new Integer(0));
		extObj.put("system_priority", "");
		extObj.put("task_id", "");
		extObj.put("internal_id", "");
		extObj.put("priority", "");
		extObj.put("dependency", new JSONArray());
		extObj.put("predecessor", "");
		extObj.put("assignee", "");
		extObj.put("template", "");
		extObj.put("level", new Integer(1));
		extObj.put("leaf", new Boolean(false));
		extObj.put("crossFunctionalTeam", new JSONArray());

		String planId = generateUUID();
		int taskId = generateId(planId);
		extObj.put("task_id", new Integer(taskId));
		String planName = "CMMC L2 POAM Plan Template";
		String planType = "PLAN";
		String impl = "New";
		
		PreparedStatement pst = conn.prepareStatement(planinsertsql);
		pst.setString(1, planId);
		pst.setString(2, planName);
		pst.setString(3, "CS");
		pst.setString(4, "");
		pst.setString(5, "");
		pst.setString(6, "0");
		pst.setString(7, "GREEN");
		pst.setString(8, impl);
		pst.setString(9, extObj.toString());
		pst.executeUpdate();
		pst.close();

		for(CSVRecord dataRow : dataReader) {
			String subplanName=null, practiceId=null, shortName=null, internalId=null, mode=null, taskName=null, desc=null, taskType=null, duration="", sd="", ed="", completion="0", totalHrs=null, resources=null, budget=null;
			if(dataRow.isConsistent()) {
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						subplanName = dataRow.get(i).trim();
					} else if(i==1) {
						internalId = dataRow.get(i).trim();
					} else if(i==2) {
						practiceId = dataRow.get(i).trim();
					} else if(i==3) {
						shortName = dataRow.get(i).trim();
					} else if(i==4) {
						taskName = dataRow.get(i).trim();
					} else if(i==5) {
						desc = dataRow.get(i).trim();
					} else if(i==6) {
						taskType = dataRow.get(i).trim();
					}
				}
			}
			System.out.println(subplanName+":"+taskName+":"+practiceId);

			String subplanId = null;
			if(!subplanList.has(subplanName)) {
				String subplanInternalId = internalId.substring(0,internalId.lastIndexOf("."));
				extObj.put("internal_id", subplanInternalId);
				planType = "SUBPLAN";
				subplanId = generateUUID();
				taskId = generateId(subplanId);
				extObj.put("level", new Integer(2));
				extObj.put("task_id", new Integer(taskId));
				pst = conn.prepareStatement(subplaninsertsql);
				pst.setString(1, subplanId);
				pst.setString(2, subplanName);
				pst.setString(3, "");
				pst.setString(4, "IT");
				pst.setString(5, planId);
				pst.setBoolean(6, true);
				pst.setString(7, "");
				pst.setString(8, sd);
				pst.setString(9, ed);
				pst.setString(10, completion);
				pst.setString(11, "GREEN");
				pst.setString(12, impl);
				pst.setString(13, extObj.toString());
				pst.executeUpdate();
				pst.close();
				subplanList.put(subplanName, subplanId);
			} else {
				subplanId = (String)subplanList.get(subplanName);
			}

			planType = "ACTIVITY";
			String activityId = generateUUID();
			taskId = generateId(activityId);
			extObj.put("level", new Integer(3));
			extObj.put("internal_id", internalId);
			extObj.put("task_id", new Integer(taskId));
			extObj.put("control_id", practiceId);
			extObj.put("leaf", new Boolean(true));
			pst = conn.prepareStatement(subplaninsertsql);
			pst.setString(1, activityId);
			pst.setString(2, taskName);
			pst.setString(3, desc);
			pst.setString(4, "IT");
			pst.setString(5, planId);
			pst.setBoolean(6, false);
			pst.setString(7, subplanId);
			pst.setString(8, sd);
			pst.setString(9, ed);
			pst.setString(10, completion);
			pst.setString(11, "GREEN");
			pst.setString(12, impl);
			pst.setString(13, extObj.toString());
			pst.executeUpdate();
			pst.close();
		}

		conn.commit();
		conn.close();
	}

	private int generateId(String str) throws Exception {
        String base64encodedString = Base64.getEncoder().encodeToString(str.getBytes("utf-8"));
		BigInteger i = new BigInteger(base64encodedString.getBytes());
		int j = i.intValue();
        return j;
    }

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	//String userName = "admin", password = "smarterD2018!";
		String userName = "peapi", password = "smartEuser2022!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		MADSVCMPlanTemplateLoader loader = new MADSVCMPlanTemplateLoader(filename, companyCode, env);
		loader.load();
	}
}