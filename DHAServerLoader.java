import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class DHAServerLoader {
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public DHAServerLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = CSVParser.parse(new FileReader(new File(filename+"_data.csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String appName=null, serverName=null;
		for(CSVRecord dataRow : dataReader) {
			if(dataRow.isConsistent()) {
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						appName = dataRow.get(i).trim();
					} else if(i==1) {
						serverName = dataRow.get(i).trim();
					}
				}
			}
			System.out.println(appName+":"+serverName);

			// Get Asset Id
			String appId = null;
			String checksql = "select id from enterprise_asset where asset_name = ?";
			PreparedStatement pst = conn.prepareStatement(checksql);
			pst.setString(1, appName);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				appId = rs.getString("id");
			}
			rs.close();
			pst.close();
			System.out.println(appId);
			
			if(appId == null) {
				String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
				String desc = "Application added as part of database loading 9_Custom_EA_DatabasesList - Review and Update.";
				appId = generateUUID();
				OrderedJSONObject extObj = new OrderedJSONObject();
				extObj = initializeApplicationExtension(extObj);
				extObj.put("custom", new OrderedJSONObject());
				pst = conn.prepareStatement(insertsql);
				pst.setString(1, appId);
				pst.setString(2, appName);
				pst.setString(3, desc);
				pst.setString(4, extObj.toString());
				pst.setString(5, companyCode);
				pst.executeUpdate();
				pst.close();
			}

			String assetId = null;
			if(serverName.length() > 0) {
				pst = conn.prepareStatement(checksql);
				pst.setString(1, serverName);
				rs = pst.executeQuery();
				while(rs.next()) {
					assetId = rs.getString("id");
				}
				rs.close();
				pst.close();
				System.out.println(assetId);

				if(assetId == null) {
					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
					String desc = "Server added as part of Application to Server loading 8_Custom_EA_ServersList - Review and Update.";
					assetId = generateUUID();
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj = initializeITAssetExtension(extObj);
					extObj.put("custom", new OrderedJSONObject());
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, assetId);
					pst.setString(2, serverName);
					pst.setString(3, desc);
					pst.setString(4, extObj.toString());
					pst.setString(5, companyCode);
					pst.executeUpdate();
					pst.close();
				}

				OrderedJSONObject ctx = new OrderedJSONObject();
				createRel(appId, assetId, "ASSET", "ASSET", ctx, "support@smarterd.com");
			}
		}

		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private void createRel(String componentId, String assetId, String componentType, String assetType, OrderedJSONObject ctx, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		}
    }


	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		DHAServerLoader loader = new DHAServerLoader(filename, companyCode, env);
		loader.load();
	}
}