import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  

public class UCMFlatLoader {
	private String domain = "UCM";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
	OrderedJSONObject controlNFList = new OrderedJSONObject();
   	
	public UCMFlatLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		Map rowCountList = new HashMap();
		int rowCount = 0;

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String rInternalId=null, pInternalId=null, cInternalId=null, aInternalId=null;
				String rName=null, pName=null, cName=null, aName=null;
				String rDesc=null, pDesc=null, cDesc=null, aDesc=null;
				String isoismsRef="", isoannexRef="", socccRef="", socconfRef="", socavailRef="", soxRef="", iso22301Ref="";
				String owner="", department="", status=null, targetDate="", state="", watchers="", notes="";
				String assetClass="", priority="0";
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						rInternalId = row[i].trim();
					} else if(i==1) {
						rName = row[i].trim();
					} else if(i==2) {
						rDesc = row[i].trim();
					} else if(i==3) {
						pInternalId = row[i].trim();
					} else if(i==4) {
						pName = row[i].trim();
					} else if(i==5) {
						pDesc = row[i].trim();
					} else if(i==6) {
						cInternalId = row[i].trim();
					} else if(i==7) {
						cName = row[i].trim();
					} else if(i==8) {
						cDesc = row[i].trim();
					} else if(i==9) {
						aInternalId = row[i].trim();
					} else if(i==10) {
						aName = row[i].trim();
					} else if(i==11) {
						aDesc = row[i].trim();
					} else if(i==12) {
						status = row[i].trim();
					} else if(i==13) {
						department = row[i].trim();
					} else if(i==14) {
						watchers = row[i].trim();
					} else if(i==15) {
						targetDate = row[i].trim();
					} else if(i==16) {
						state = row[i].trim();
					} else if(i==17) {
						isoismsRef = row[i].trim();
					} else if(i==18) {
						isoannexRef = row[i].trim();
					} else if(i==19) {
						socccRef = row[i].trim();
					} else if(i==20) {
						socavailRef = row[i].trim();
					} else if(i==21) {
						socconfRef = row[i].trim();
					} else if(i==22) {
						notes = row[i].trim();
					} else if(i==23) {
						owner = row[i].trim();
						if(owner.equals("?")) {
							owner = "";
						}
					}
				}
				
				JSONArray sf = new JSONArray();

				JSONArray isoismsArr = null;
				if(isoismsRef != null && isoismsRef.length() > 0 && !isoismsRef.equalsIgnoreCase("N/A")) {
					String[] isoRefArr = isoismsRef.split("\n");
					List isoRefList = Arrays.asList(isoRefArr);
					isoismsArr = new JSONArray(isoRefList);
				} else {
					isoismsArr = new JSONArray();
				}
				JSONArray isoannexArr = null;
				if(isoannexRef != null && isoannexRef.length() > 0 && !isoannexRef.equalsIgnoreCase("N/A")) {
					String[] isoRefArr = isoannexRef.split("\n");
					List isoRefList = Arrays.asList(isoRefArr);
					isoannexArr = new JSONArray(isoRefList);
				} else {
					isoannexArr = new JSONArray();
				}
				JSONArray socccArr = null;
				if(socccRef != null && socccRef.length() > 0 && !socccRef.equalsIgnoreCase("N/A")) {
					String[] socRefArr = socccRef.split("\n");
					List socRefList = Arrays.asList(socRefArr);
					socccArr = new JSONArray(socRefList);
				} else {
					socccArr = new JSONArray();
				}
				JSONArray socavailArr = null;
				if(socavailRef != null && socavailRef.length() > 0 && !socavailRef.equalsIgnoreCase("N/A")) {
					String[] socRefArr = socavailRef.split("\n");
					List socRefList = Arrays.asList(socRefArr);
					socavailArr = new JSONArray(socRefList);
				} else {
					socavailArr = new JSONArray();
				}
				JSONArray socconfArr = null;
				if(socconfRef != null && socconfRef.length() > 0 && !socconfRef.equalsIgnoreCase("N/A")) {
					String[] socRefArr = socconfRef.split("\n");
					List socRefList = Arrays.asList(socRefArr);
					socconfArr = new JSONArray(socRefList);
				} else {
					socconfArr = new JSONArray();
				}
				JSONArray soxArr = null;
				if(soxRef != null && soxRef.length() > 0 && !soxRef.equalsIgnoreCase("N/A")) {
					String[] soxRefArr = soxRef.split("\n");
					List soxRefList = Arrays.asList(soxRefArr);
					soxArr = new JSONArray(soxRefList);
				} else {
					soxArr = new JSONArray();
				}
				JSONArray iso22301Arr = null;
				if(iso22301Ref != null && iso22301Ref.length() > 0 && !iso22301Ref.equalsIgnoreCase("N/A")) {
					String[] iso22301RefArr = iso22301Ref.split("\n");
					List iso22301RefList = Arrays.asList(iso22301RefArr);
					iso22301Arr = new JSONArray(iso22301RefList);
				} else {
					iso22301Arr = new JSONArray();
				}

				JSONArray commentList = new JSONArray();
				if(notes != null && notes.length() > 0) {
					LocalDateTime now = LocalDateTime.now();  
					DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");  
					String timestamp = now.format(format);  
					OrderedJSONObject commentObj = new OrderedJSONObject();
					commentObj.put("comment", notes);
					commentObj.put("user", "Internal Audit");
					commentObj.put("timestamp", timestamp);
					commentList.add(commentObj);
				}

				OrderedJSONObject cObj = new OrderedJSONObject();
				OrderedJSONObject extObject = new OrderedJSONObject();
				extObject.put("family", rName);
				extObject.put("securityFunction", sf);
				extObject.put("compliance", cObj);
				extObject.put("evidence", "");
				extObject.put("population", "");
				extObject.put("control_status", "");
				extObject.put("department", "");
				extObject.put("watchers", new JSONArray());

				System.out.println(rName+":"+pName+":"+cName+":"+aName);
				
				// Check Level 1 Control
				String rId = null;			
				if(!capabilityList.containsKey(rName)) {
					rId = generateUUID();
					OrderedJSONObject customField = new OrderedJSONObject();
					extObject.put("custom", customField);
					String ext = extObject.toString();
					insertCapability(rId, rootId, rInternalId, rootId, rName, rDesc, ext, new JSONArray().toString(), "", "1", 0);
					capabilityList.put(rName, rId);
				} else {
					rId = (String)capabilityList.get(rName);
				}

				String cId = null;
				cId = generateUUID();
				OrderedJSONObject customField = new OrderedJSONObject();
				customField.put("Control Sub-Family", pName);
				customField.put("Control Short Name", cName);
				extObject.put("custom", customField);
				String ext = extObject.toString();
				insertCapability(cId, rId, cInternalId, rName, cDesc, "", ext, commentList.toString(), owner, "2", 0);

				/*
				if(!capabilityList.containsKey(rName+":"+cDesc)) {
					cId = generateUUID();
					
					int cCount = 0;
					if(rowCountList.containsKey(rName)) {
						cCount = ((Integer)rowCountList.get(rName)).intValue();
					}
					cCount++;
					cInternalId = rInternalId +"."+String.valueOf(cCount);
					OrderedJSONObject customField = new OrderedJSONObject();
					customField.put("Control Sub-Family", pName);
					customField.put("Control Short Name", cName);
					extObject.put("custom", customField);
					String ext = extObject.toString();
					insertCapability(cId, rId, cInternalId, rName, cDesc, "", ext, "", "2", 0);
					capabilityList.put(rName+":"+cDesc, cId);
					rowCountList.put(rName, new Integer(cCount));
				} else {
					cId = (String)capabilityList.get(rName+":"+cDesc);
					int cCount = ((Integer)rowCountList.get(rName)).intValue();
					cInternalId = rInternalId +"."+String.valueOf(cCount);
				}
				*/
				
				// Check Level 2 Control
				//String aId = checkCapability(cName, aName, "4");
				//if(aId == null || aId.length() == 0) {
				String aId = null;
				if(aName != null && aName.length() > 0) {	
					aId = generateUUID();
					int aCount = 0;
					if(rowCountList.containsKey(cDesc)) {
						aCount = ((Integer)rowCountList.get(cDesc)).intValue();
					}
					aCount++;
					aInternalId = cInternalId +"."+String.valueOf(aCount);
					if(!capabilityList.containsKey(cDesc+":"+aName)) {
						customField.put("Target Date", targetDate);
						customField.put("State", state);
						extObject.put("custom", customField);
						extObject.put("control_status", status);
						extObject.put("department", department);
						ext = extObject.toString();
						insertCapability(aId, cId, aInternalId, cDesc, aName, aDesc, ext, new JSONArray().toString(), owner, "3", 0);
						capabilityList.put(cDesc+":"+aName, aId);
						rowCountList.put(cDesc, new Integer(aCount));
					} else {
						aId = (String)capabilityList.get(cDesc+":"+aName);
					}
				}

				// Establish Compliance Relationship
				if(isoismsArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "ISO 27001 ISMS", isoismsArr);
				}
				if(isoannexArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "ISO 27001 ANNEX A", isoannexArr);
				}
				if(socccArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "SOC 2", socccArr);
				}
				if(socconfArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "SOC 2", socconfArr);
				}
				if(socavailArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "SOC 2", socavailArr);
				}
				if(soxArr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "SOX", soxArr);
				}
				if(iso22301Arr.size() > 0) {
					createComplianceRelationship(rId, null, cId, aId, "ISO 22301", iso22301Arr);
				}
			}
		}
		
		conn.commit();
		conn.close();

		System.out.println("Compliance Not Found List:"+controlNFList.toString());
	}
	
	private void checkRootCapability() throws Exception {
		rootId = checkCapability("UCM");
		
		if(rootId == null || rootId.length() == 0) {
			OrderedJSONObject extObject = new OrderedJSONObject();
			String ext = extObject.toString();

			//rootId = generateUUID();
			rootId = "UCM";
			insertCapability(rootId, rootId, "", "UCM", "UCM", "UCM Controls", ext, new JSONArray().toString(),"", "0", 0);
		}
		capabilityList.put(rootId, rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String internalId) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where internal_id = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}

	private String checkCapability(String parent, String child, String level) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id from enterprise_business_capability where parent_capability_name = ? and child_capability_name=? and business_capability_level = ?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		pst.setString(3, level);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String ext, String comment, String owner, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,business_capability_comment,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String status = "Y";
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		factorObject.put("currentMaturity", "0.0");
		factorObject.put("targetMaturity", "0.0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();
   		
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, comment);
		pst.setString(17, createdBy);
		pst.setString(18, createdBy);
		pst.setString(19, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void createComplianceRelationship(String rId, String pId, String cId, String aId, String type, JSONArray compArr) throws Exception {
		System.out.println("createComplianceRelationship:"+compArr+":"+type);
		String checkrelsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
        String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		String compsql = "select id from enterprise_business_capability where internal_id = ? and business_capability_domain like ?";
		PreparedStatement pst = conn.prepareStatement(insertrelsql);
		String updatedBy = "support@smarterd.com";

		if(compArr.size() > 0) {
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPLIANCE", type);
			Iterator iter = compArr.iterator();
			while(iter.hasNext()) {
				String str = (String)iter.next();

				String compId = null;
				PreparedStatement pst1 = conn.prepareStatement(compsql);
				pst1.setString(1, str);
				pst1.setString(2, type);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					compId = rs1.getString("id");
				}
				rs1.close();
				pst1.close();

				if(compId != null) {
					int count = 0;
					if(aId != null) {
						pst1 = conn.prepareStatement(checkrelsql);
						pst1.setString(1, aId);
						pst1.setString(2, compId);
						rs1 = pst1.executeQuery();
						while(rs1.next()) {
							count = rs1.getInt("count");
						}
						rs1.close();
						pst1.close();

						if(count == 0) {
							if(aId != null) {
								ctx.put("LINKED", "Y");
								ctx.put("COMPONENT TYPE", "CAPABILITY");
								pst.setString(1, compId);
								pst.setString(2, aId);
								pst.setString(3, "COMPLIANCE");
								pst.setString(4, ctx.toString());
								pst.setString(5, updatedBy);
								pst.setString(6, updatedBy);
								pst.setString(7, companyCode);
								pst.addBatch();
								ctx.put("COMPONENT TYPE", "COMPLIANCE");
								pst.setString(1, aId);
								pst.setString(2, compId);
								pst.setString(3, "CAPABILITY");
								pst.setString(4, ctx.toString());
								pst.setString(5, updatedBy);
								pst.setString(6, updatedBy);
								pst.setString(7, companyCode);
								pst.addBatch();
							}
						}
					}

					count = 0;
					pst1 = conn.prepareStatement(checkrelsql);
					pst1.setString(1, cId);
					pst1.setString(2, compId);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						// Rollup 
						/*
						if(aId != null) {
							ctx.put("LINKED", "Y");
						} else {
							ctx.put("LINKED", "N");
						}
						*/
						ctx.put("LINKED", "N");
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, compId);
						pst.setString(2, cId);
						pst.setString(3, "COMPLIANCE");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "COMPLIANCE");
						pst.setString(1, cId);
						pst.setString(2, compId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
					}

					if(pId != null) {
						count = 0;
						pst1 = conn.prepareStatement(checkrelsql);
						pst1.setString(1, pId);
						pst1.setString(2, compId);
						rs1 = pst1.executeQuery();
						while(rs1.next()) {
							count = rs1.getInt("count");
						}
						rs1.close();
						pst1.close();

						if(count == 0) {
							ctx.put("COMPONENT TYPE", "CAPABILITY");
							ctx.put("LINKED", "Y");
							pst.setString(1, compId);
							pst.setString(2, pId);
							pst.setString(3, "COMPLIANCE");
							pst.setString(4, ctx.toString());
							pst.setString(5, updatedBy);
							pst.setString(6, updatedBy);
							pst.setString(7, companyCode);
							pst.addBatch();
							ctx.put("COMPONENT TYPE", "COMPLIANCE");
							pst.setString(1, pId);
							pst.setString(2, compId);
							pst.setString(3, "CAPABILITY");
							pst.setString(4, ctx.toString());
							pst.setString(5, updatedBy);
							pst.setString(6, updatedBy);
							pst.setString(7, companyCode);
							pst.addBatch();
						}
					}

					if(rId != null) {
						count = 0;
						pst1 = conn.prepareStatement(checkrelsql);
						pst1.setString(1, rId);
						pst1.setString(2, compId);
						rs1 = pst1.executeQuery();
						while(rs1.next()) {
							count = rs1.getInt("count");
						}
						rs1.close();
						pst1.close();

						if(count == 0) {
							ctx.put("COMPONENT TYPE", "CAPABILITY");
							ctx.put("LINKED", "Y");
							pst.setString(1, compId);
							pst.setString(2, rId);
							pst.setString(3, "COMPLIANCE");
							pst.setString(4, ctx.toString());
							pst.setString(5, updatedBy);
							pst.setString(6, updatedBy);
							pst.setString(7, companyCode);
							pst.addBatch();
							ctx.put("COMPONENT TYPE", "COMPLIANCE");
							pst.setString(1, rId);
							pst.setString(2, compId);
							pst.setString(3, "CAPABILITY");
							pst.setString(4, ctx.toString());
							pst.setString(5, updatedBy);
							pst.setString(6, updatedBy);
							pst.setString(7, companyCode);
							pst.addBatch();
						}
					}
				} else {
					if(!controlNFList.containsKey(str)) {
						controlNFList.put(str, type);
					}
				}
			}
		}
		pst.executeBatch();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		UCMFlatLoader loader = new UCMFlatLoader(filename, companyCode, env);
		loader.process();
	}
}