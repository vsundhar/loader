import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class DHAIntegrationLoader {
	private CSVReader dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public DHAIntegrationLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = new CSVReader(new FileReader(new File(filename+"_data.csv")), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void load() throws Exception {
		String[] dataRow;
		int rowCount=0;
		while ((dataRow = dataReader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				OrderedJSONObject ctx = new OrderedJSONObject();
				OrderedJSONObject custom = new OrderedJSONObject();
				String assetName=null, eaLink=null, connectionType=null, remoteAssetName=null, systemName=null, remoteAppOrg=null, direction=null, integrationType=null, connectionInfo=null, comment=null, endpoint=null, frequency=null;
				for(int i=0; i<dataRow.length; i++) {
					if(i==0) {			
						assetName = dataRow[i].trim();
					} else if(i==1) {
						
					} else if(i==2) {
						connectionType = dataRow[i].trim();
						ctx.put("connection_type", connectionType);
					} else if(i==3) {
						systemName = dataRow[i].trim();
						custom.put("System Name", systemName);
					} else if(i==4) {
						remoteAppOrg = dataRow[i].trim();
					} else if(i==5) {
						remoteAssetName = dataRow[i].trim();
					} else if(i==6) {
						custom.put("Entity Category", dataRow[i].trim());
					} else if(i==7) {
						direction = dataRow[i].trim();
					} else if(i==8) {
						integrationType = dataRow[i].trim();
					} else if(i==9) {
						connectionInfo = dataRow[i].trim();
					} else if(i==10) {
						comment = dataRow[i].trim();
						comment = comment.replaceAll("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]", "");
						ctx.put("comment", comment);
					} else if(i==11) {
						custom.put("Through Rhapsody", dataRow[i].trim());
					} else if(i==12) {
						endpoint = dataRow[i].trim();
						ctx.put("endpoint", endpoint);
					} else if(i==13) {
						frequency = dataRow[i].trim();
						ctx.put("frequency", frequency);
					} else if(i==14) {
						String medicalData = dataRow[i].trim();
						if(medicalData.length() > 0) {
							custom.put("Medical Data", new Boolean(dataRow[i].trim().toLowerCase()));
						} else {
							custom.put("Medical Data", new Boolean(false));
						}
					} else if(i==15) {	
						String imported = dataRow[i].trim();
						if(imported.length() > 0) {
							custom.put("Imported", new Boolean(dataRow[i].trim().toLowerCase()));
						} else {
							custom.put("Imported", new Boolean(false));
						}
					}
                }	

				String type = null;
				if(remoteAssetName == null || remoteAssetName.length() == 0) {
					if(systemName != null && systemName.length() > 0) {
						remoteAssetName = systemName;
						type = "Entity";
					}
				}

				if(assetName != null && assetName.length() > 0 && remoteAssetName != null && remoteAssetName.length() > 0) {
					ctx.put("direction", direction);
					ctx.put("integration_type", integrationType);
					ctx.put("connection_info", connectionInfo);
					ctx.put("custom", custom);

					// Get Asset Id
					String assetId = null;
					String checksql = "select id from enterprise_asset where asset_name = ?";
					PreparedStatement pst = conn.prepareStatement(checksql);
					pst.setString(1, assetName);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						assetId = rs.getString("id");
					}
					rs.close();
					pst.close();

					String insertsql = "insert into enterprise_asset(id,asset_name,lifecycle_name,asset_lifecycle_list,extension,created_by,updated_by,company_code) values(?,?,'',json_array(),?,'support@smarterd.com','support@smarterd.com',?)";
					if(assetId == null) {
						assetId = generateUUID();
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj = initializeApplicationExtension(extObj);
						extObj.put("custom", new OrderedJSONObject());
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, assetId);
						pst.setString(2, assetName);
						pst.setString(3, extObj.toString());
						pst.setString(4, companyCode);
						pst.executeUpdate();
						pst.close();
					}

					// Get Remote Asset Id
					String remoteAssetId = null;
					pst = conn.prepareStatement(checksql);
					pst.setString(1, remoteAssetName);
					rs = pst.executeQuery();
					while(rs.next()) {
						remoteAssetId = rs.getString("id");
					}
					rs.close();
					pst.close();

					if(remoteAssetId == null) {
						remoteAssetId = generateUUID();
						OrderedJSONObject extObj = new OrderedJSONObject();
						extObj = initializeApplicationExtension(extObj);
						if(type != null) {
							extObj.put("Type", type);
						}
						extObj.put("custom", new OrderedJSONObject());
						pst = conn.prepareStatement(insertsql);
						pst.setString(1, remoteAssetId);
						pst.setString(2, remoteAssetName);
						pst.setString(3, extObj.toString());
						pst.setString(4, companyCode);
						pst.executeUpdate();
						pst.close();
					}

					createRel(assetId, remoteAssetId, assetName, remoteAssetName, "ASSET", "ASSET", direction, integrationType, connectionInfo, ctx, "support@smarterd.com");
				}
			}	
		}

		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	private void createRel(String componentId, String assetId, String assetName, String remoteAssetName, String componentType, String assetType, String direction, String integrationType, String connectionInfo, OrderedJSONObject ctx, String updatedBy) throws Exception {
        System.out.println("In createRel:"+componentId+":"+assetId);
		String checksql = "select count(*) count from enterprise_rel where component_id = ? and asset_id = ? and json_extract(asset_rel_context, '$.\"direction\"') = ? and json_extract(asset_rel_context, '$.\"integration_type\"') = ? and json_extract(asset_rel_context, '$.\"conenction_info\"') = ?";
        String enterpriseRelInsertSQL = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";
		String enterpriseRelUpdateSQL = "update enterprise_rel set asset_rel_context = ? where component_id = ? and asset_id = ?";
		int count = 0;
		PreparedStatement pst = conn.prepareStatement(checksql);
		pst.setString(1, componentId);
		pst.setString(2, assetId);
		pst.setString(3, direction);
		pst.setString(4, integrationType);
		pst.setString(5, connectionInfo);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count == 0) {
			pst = conn.prepareStatement(enterpriseRelInsertSQL);
			ctx.put("asset_name", remoteAssetName);
			ctx.put("LINKED", "N");
			ctx.put("COMPONENT TYPE", componentType);
			pst.setString(1, assetId);
			pst.setString(2, componentId);
			pst.setString(3, assetType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			ctx.put("COMPONENT TYPE", assetType);
			ctx.put("asset_name", assetName);
			ctx.put("integration_type", "Remote "+integrationType);
			pst.setString(1, componentId);
			pst.setString(2, assetId);
			pst.setString(3, componentType);
			pst.setString(4, ctx.toString());
			pst.setString(5, updatedBy);
			pst.setString(6, updatedBy);
			pst.setString(7, companyCode);
			pst.addBatch();
			pst.executeBatch();
			pst.close();
		} else {
			pst = conn.prepareStatement(enterpriseRelUpdateSQL);
			ctx.put("asset_name", remoteAssetName);
			pst.setString(1, ctx.toString());
			pst.setString(2, componentId);
			pst.setString(3, assetId);
			pst.addBatch();
			ctx.put("asset_name", assetName);
			ctx.put("integration_type", "Remote "+integrationType);
			pst.setString(1, ctx.toString());
			pst.setString(2, assetId);
			pst.setString(3, componentId);
			pst.executeBatch();
			pst.close();
		}
    }


	private int generateId() throws Exception {
        System.out.println("Generating ID...");
		String nextIdSQL = "insert into SE.id values(null)";
		String nextIdSelectSQL = "SELECT LAST_INSERT_ID() FROM SE.id";
        int newId = 0;
        PreparedStatement pst = conn.prepareStatement(nextIdSQL);
        pst.executeUpdate();
        pst.close();
        conn.commit();
        System.out.println("ID Updated!!!");
        pst = null;
        pst = conn.prepareStatement(nextIdSelectSQL);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            newId = rs.getInt(1);
        }
        rs.close();
        pst.close();
        System.out.println("Generated ID:" + newId);

        return newId;
    }

	private OrderedJSONObject initializeApplicationExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Business app/IT app", "");
        extObj.put("Organization", "");
        extObj.put("Business Unit", "");
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Manager", "");
        extObj.put("Compliance", new JSONArray());
        extObj.put("Critcality", "");

        return extObj;
    }

	private OrderedJSONObject initializeSoftwareExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("OEM Vendor", "");
        extObj.put("Version", "");
        extObj.put("asset_version_enddate", "");
        extObj.put("asset_version_ext_support", "");

        return extObj;
    }

	private OrderedJSONObject initializeITAssetExtension(OrderedJSONObject extObj) throws Exception {
        extObj.put("Type", "");
        extObj.put("Sub-Type", "");
        extObj.put("OS", "");
        extObj.put("Model", "");
        extObj.put("OEM Vendor", "");
        extObj.put("IP Address", new JSONArray());
        extObj.put("Ecosystem", new JSONArray());
        extObj.put("Environment", "");
        extObj.put("end_of_life", "N");
        extObj.put("Serial Number", new JSONArray());

        return extObj;
    }

	// Fix JSON String for any Special Chars
	public static class FixedJson {
        private final String target;
        private final Pattern pattern;

        public FixedJson(String target) {
            this(target,Pattern.compile("\"(.+?)\"[^\\w\"]"));
        }

        public FixedJson(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll(
                matchResult -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(
                        matchResult.group(),
                        0,
                        matchResult.start(1) - matchResult.start(0)
                    );
                    sb.append(
                        new Escaped(
                            new Escaped(matchResult.group(1)).value()
                        ).value()
                    );
                    sb.append(
                        matchResult.group().substring(
                            matchResult.group().length() - (matchResult.end(0) - matchResult.end(1))
                        )
                    );
                    return sb.toString();
                }
            );
        }
    }

    public static class Escaped {
        private final String target;
        private final Pattern pattern;

        public Escaped(String target) {
            this(target,Pattern.compile("[\\\\]"));
        }

        public Escaped(String target, Pattern pattern) {
            this.target = target;
            this.pattern = pattern;
        }

        public String value() {
            return this.pattern.matcher(this.target).replaceAll("\\\\$0");
        }
    }
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		DHAIntegrationLoader loader = new DHAIntegrationLoader(filename, companyCode, env);
		loader.load();
	}
}