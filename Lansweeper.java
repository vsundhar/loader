
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;
import java.io.*;
import java.nio.charset.*;

public class Lansweeper {
	private String companyCode = null;
	private String hostname = "localhost";
    private Connection conn = null;
	private static String authUrl = "https://api.lansweeper.com/api/integrations/oauth/token";
	private static String queryUrl = "https://api.lansweeper.com/api/integrations/graphql";
	private static String clientId = "f1300302bd3f753ab6b96e1e8f064444";
	private static String clientSecret = "QYbnmJCLi4OkAUPfmH7w1GQJ9q_q3QnY-O6Wry0R6jLdLHH6YeNQIehsvcybPHtg";
	private static String appredirectUrl = "https://www.smarterd.com";
	private static String appauthCode = "dc2af82597d73a69"; // Once only use code - have to be generated everytime for access
	private String refreshToken = "edr5f9aR9tcW8zVaEVO7ekXzeTjW7fwvaKtm8eOV7cnZG";
   	
	public Lansweeper(String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}

	private void load() throws Exception {
		String updateassetstatussql = "update enterprise_asset_discovery set last_scan = current_scan, current_scan = 'N'";
		String updatesoftwarestatussql = "update enterprise_software_discovery set last_scan = current_scan, current_scan = 'N'";
		String checkassetsql = "select id, next_scan_include, extension from enterprise_asset_discovery where asset_name like ?";
		String checksoftwaresql = "select id, next_scan_include, extension from enterprise_software_discovery where asset_key = ? and software_name like ?";
		String insertassetsql = "insert into enterprise_asset_discovery(id,site_id,site_name,asset_key,asset_name,asset_type,extension,next_scan_include,current_scan) values(uuid(),?,?,?,?,?,?,'N','Y')";
		String insertsoftwaresql = "insert into enterprise_software_discovery(id,asset_key,software_id,software_name,software_vendor,software_os,software_version,software_patch_version,software_last_changed,extension,next_scan_include,current_scan) values(uuid(),?,?,?,?,?,?,?,?,?,'N','Y')";
		String updateassetsql = "update enterprise_asset_discovery set site_id=?,site_name=?,extension=?,current_scan=? where id=?";
		String updatesoftwaresql = "update enterprise_software_discovery set software_vendor=?,software_os=?,software_version=?,software_patch_version=?,software_last_changed=?,extension=?,current_scan=? where id=?";
		int insertedCount = 0, updatedCount = 0, rowCount = 0;
		String accessToken = getToken();
		JSONArray siteList = getStiteId(accessToken);
		Iterator iter = siteList.iterator();
		while(iter.hasNext()) {
			OrderedJSONObject siteDetail = (OrderedJSONObject)iter.next();
			Iterator siteIter = siteDetail.keySet().iterator();
			while(siteIter.hasNext()) {
				String siteId = (String)siteIter.next();
				String siteName = (String)siteDetail.get(siteId);
				System.out.println(siteId+":"+siteName);

				if(siteName.indexOf(companyCode.toLowerCase()) != -1) {
					// Update Asset Scan Status
					PreparedStatement pst = conn.prepareStatement(updateassetstatussql);
					pst.executeUpdate();
					pst.close();

					// Update Software Scan Status
					pst = conn.prepareStatement(updatesoftwarestatussql);
					pst.executeUpdate();
					pst.close();

					JSONArray assetList = getAssetList(siteId, accessToken);
					Iterator assetIter = assetList.iterator();
					while(assetIter.hasNext()) {
						OrderedJSONObject assetObj = (OrderedJSONObject)assetIter.next();
						String assetKey = (String)assetObj.get("assetKey");
						String assetName = (String)assetObj.get("assetName");
						String assetType = (String)assetObj.get("assetTypeName");

						// Check Asset
						String id = null, nextScanInclude = null, ext = null;;
						pst = conn.prepareStatement(checkassetsql);
						pst.setString(1, assetName);
						ResultSet rs = pst.executeQuery();
						while(rs.next()) {
							id = rs.getString("id");
							nextScanInclude = rs.getString("next_scan_include");
							ext = rs.getString("extension");
						}
						rs.close();
						pst.close();

						if(nextScanInclude == null || nextScanInclude.length() == 0 || nextScanInclude.equals("Y")) {
							if(id == null) {
								OrderedJSONObject extObj = new OrderedJSONObject();
								pst = conn.prepareStatement(insertassetsql);
								pst.setString(1, siteId);
								pst.setString(2, siteName);
								pst.setString(3, assetKey);
								pst.setString(4, assetName);
								pst.setString(5, assetType);
								pst.setString(6, extObj.toString());
								insertedCount++;
							} else {
								OrderedJSONObject extObj = null;
								if(ext != null && ext.length() > 0) {
									extObj = new OrderedJSONObject(ext);
								} else {
									extObj = new OrderedJSONObject();
								}
								pst = conn.prepareStatement(updateassetsql);
								pst.setString(1, siteId);
								pst.setString(2, siteName);
								pst.setString(3, extObj.toString());
								pst.setString(4, "Y");
								pst.setString(5, id);
								updatedCount++;
							}
							System.out.println(pst.toString());
							pst.executeUpdate();
							pst.close();
						}

						JSONArray softwareList = getSoftwareList(siteId, assetKey, accessToken);
						Iterator softwareIter = softwareList.iterator();
						while(softwareIter.hasNext()) {
							OrderedJSONObject softwareObj = (OrderedJSONObject)softwareIter.next();
							System.out.println(softwareObj.toString());
							String softwareId = (String)softwareObj.get("softwareId");
							String softwareName = (String)softwareObj.get("softwareName");
							String softwareVendor = (String)softwareObj.get("softwarePublisher");
							String softwareOS = (String)softwareObj.get("operatingSystem");
							String softwareVersion = (String)softwareObj.get("version");
							String softwarePatchVersion = "";
							if(softwareObj.has("patch") && softwareObj.get("patch") != null) {
								softwarePatchVersion = ((Integer)softwareObj.get("patch")).toString();
							}
							String lastChanged = (String)softwareObj.get("lastChanged");
							if(lastChanged.length() > 0) {
								lastChanged = lastChanged.substring(0, lastChanged.indexOf("T"));
							}
							System.out.println(softwareName+":"+softwareVendor+":"+softwareOS);

							// Check Asset
							id = null;
							nextScanInclude = null;
							ext = null;;
							pst = conn.prepareStatement(checksoftwaresql);
							pst.setString(1, assetKey);
							pst.setString(2, softwareName);
							rs = pst.executeQuery();
							while(rs.next()) {
								id = rs.getString("id");
								nextScanInclude = rs.getString("next_scan_include");
								ext = rs.getString("extension");
							}
							rs.close();
							pst.close();

							// Update Asset Table
							if(nextScanInclude == null || nextScanInclude.length() == 0 || nextScanInclude.equals("Y")) {
								if(id == null) {
									OrderedJSONObject extObj = new OrderedJSONObject();
									pst = conn.prepareStatement(insertsoftwaresql);
									pst.setString(1, assetKey);
									pst.setString(2, softwareId);
									pst.setString(3, softwareName);
									pst.setString(4, softwareVendor);
									pst.setString(5, softwareOS);
									pst.setString(6, softwareVersion);
									pst.setString(7, softwarePatchVersion);
									pst.setString(8, lastChanged);
									pst.setString(9, extObj.toString());
									insertedCount++;
								} else {
									OrderedJSONObject extObj = null;
									if(ext != null && ext.length() > 0) {
										extObj = new OrderedJSONObject(ext);
									} else {
										extObj = new OrderedJSONObject();
									}
									pst = conn.prepareStatement(updatesoftwaresql);
									pst.setString(1, softwareVendor);
									pst.setString(2, softwareOS);
									pst.setString(3, softwareVersion);
									pst.setString(4, softwarePatchVersion);
									pst.setString(5, lastChanged);
									pst.setString(6, extObj.toString());
									pst.setString(7, "Y");
									pst.setString(8, id);
									updatedCount++;
								}
								System.out.println(pst.toString());
								pst.executeUpdate();
								pst.close();
							}
							rowCount++;
						}
					}

					break;
				}
			}
		}
		conn.commit();
		System.out.println("Sucessfully loaded discovered assets[Inserted rows:"+insertedCount+" Updated rows:"+updatedCount+" Total rows:"+rowCount);
	}

	public String getToken() throws Exception {
		String accessToken = null;
		if(refreshToken == null || refreshToken.length() == 0) {
			accessToken = generateToken();
		} else {
			accessToken = refreshToken();
		}

		return accessToken;
	}
	
	public String generateToken() throws Exception {
		String grantType = "authorization_code";
		String accessToken = null;
		URL url = new URL(authUrl);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		OrderedJSONObject bodyObj = new OrderedJSONObject();
		bodyObj.put("client_id", clientId);
		bodyObj.put("client_secret", clientSecret);
		bodyObj.put("grant_type", grantType);
		bodyObj.put("code", appauthCode);
		bodyObj.put("redirect_uri", appredirectUrl);

		byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
			os.write(out);

			BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
    		StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			if(response.length() > 0) {
				OrderedJSONObject res = new OrderedJSONObject(response.toString());
				accessToken = (String)res.get("access_token");
				refreshToken = (String)res.get("refresh_token");
			}
		}
		return accessToken;
	}

	public String refreshToken() throws Exception {
		String grantType = "refresh_token";
		String accessToken = null;
		URL url = new URL(authUrl);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		OrderedJSONObject bodyObj = new OrderedJSONObject();
		bodyObj.put("client_id", clientId);
		bodyObj.put("client_secret", clientSecret);
		bodyObj.put("grant_type", grantType);
		bodyObj.put("refresh_token", refreshToken);

		byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
			os.write(out);

			BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
    		StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}

			if(response.length() > 0) {
				OrderedJSONObject res = new OrderedJSONObject(response.toString());
				accessToken = (String)res.get("access_token");
			}
		}

		return accessToken;
	}

	private String getRefreshToken() {
		return refreshToken;
	}

	private JSONArray getStiteId(String accessToken) throws Exception {
		String query = "{me {username, profiles {site {id,name}}}}";
		JSONArray siteList = new JSONArray();
		URL url = new URL(queryUrl);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		OrderedJSONObject bodyObj = new OrderedJSONObject();
		bodyObj.put("query", query);

		byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Authorization", "Bearer "+accessToken);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
			os.write(out);

			StringBuilder response = new StringBuilder();
			BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			if(response.length() > 0) {
				OrderedJSONObject res = new OrderedJSONObject(response.toString());
				if(res.has("data")) {
					OrderedJSONObject dataObj = (OrderedJSONObject)res.get("data");
					if(dataObj.has("me")) {
						OrderedJSONObject meObj = (OrderedJSONObject)dataObj.get("me");
						if(meObj.has("profiles")) {
							JSONArray profiles = (JSONArray)meObj.get("profiles");
							Iterator iter = profiles.iterator();
							while(iter.hasNext()) {
								OrderedJSONObject siteObj = (OrderedJSONObject)iter.next();
								if(siteObj.has("site")) {
									OrderedJSONObject site = (OrderedJSONObject)siteObj.get("site");
									String siteId = (String)site.get("id");
									String siteName = (String)site.get("name");
									OrderedJSONObject siteDetail = new OrderedJSONObject();
									siteDetail.put(siteId, siteName);
									siteList.add(siteDetail);
								}
							}
						}
					}
				}
			}
		}

		return siteList;
	}

	private JSONArray getAssetList(String siteId, String accessToken) throws Exception {
		String query = "query getAssetResources {site(id: \""+siteId+"\") {assetResources(fields: [\"asset.assetName\",\"asset.assetTypeName\",\"asset.assetKey\"]) {total,items}}}";
		JSONArray assetList = new JSONArray();
		URL url = new URL(queryUrl);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		OrderedJSONObject bodyObj = new OrderedJSONObject();
		bodyObj.put("query", query);

		byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Authorization", "Bearer "+accessToken);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
			os.write(out);

			StringBuilder response = new StringBuilder();
			BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			if(response.length() > 0) {
				OrderedJSONObject res = new OrderedJSONObject(response.toString());
				if(res.has("data")) {
					OrderedJSONObject dataObj = (OrderedJSONObject)res.get("data");
					if(dataObj.has("site")) {
						OrderedJSONObject siteObj = (OrderedJSONObject)dataObj.get("site");
						if(siteObj.has("assetResources")) {
							OrderedJSONObject resourcesObj = (OrderedJSONObject)siteObj.get("assetResources");
							if(resourcesObj.has("items"))  {
								JSONArray items = (JSONArray)resourcesObj.get("items");
								Iterator iter = items.iterator();
								while(iter.hasNext()) {
									OrderedJSONObject itemObj = (OrderedJSONObject)iter.next();
									if(itemObj.has("asset")) {
										OrderedJSONObject assetObj = (OrderedJSONObject)itemObj.get("asset");
										assetList.add(assetObj);
									}
								}
							}
						}
					}
				}
			}
		}

		return assetList;
	}

	private JSONArray getSoftwareList(String siteId, String assetKey, String accessToken) throws Exception {
		String query = "query getSoftwaresByAssetKey {site(id: \""+siteId+"\") {softwares(assetKey: \""+assetKey+"\"orderDirection: DESC orderBy: \"softwares.softwareName\" fields: [\"softwares.softwareId\",\"softwares.operatingSystem\",\"softwares.softwarePublisher\",\"softwares.softwareName\",\"softwares.version\",\"softwares.patch\",\"softwares.lastChanged\"]) {total,items}}}";
		JSONArray softwareList = null;
		URL url = new URL(queryUrl);
		HttpURLConnection http = (HttpURLConnection)url.openConnection();
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		OrderedJSONObject bodyObj = new OrderedJSONObject();
		bodyObj.put("query", query);

		byte[] out = bodyObj.toString().getBytes(StandardCharsets.UTF_8);
		int length = out.length;
		http.setFixedLengthStreamingMode(length);
		http.setRequestProperty("Authorization", "Bearer "+accessToken);
		http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		http.connect();
		try(OutputStream os = http.getOutputStream()) {
			os.write(out);

			StringBuilder response = new StringBuilder();
			BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			if(response.length() > 0) {
				OrderedJSONObject res = new OrderedJSONObject(response.toString());
				if(res.has("data")) {
					OrderedJSONObject dataObj = (OrderedJSONObject)res.get("data");
					if(dataObj.has("site")) {
						OrderedJSONObject siteObj = (OrderedJSONObject)dataObj.get("site");
						if(siteObj.has("softwares")) {
							OrderedJSONObject softwares = (OrderedJSONObject)siteObj.get("softwares");
							if(softwares.has("items"))  {
								softwareList = (JSONArray)softwares.get("items");
							}
						}
					}
				}
			}
		}

		return softwareList;
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		Lansweeper loader = new Lansweeper(companyCode, env);
		loader.load();
	}
}

