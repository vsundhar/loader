import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class CSFLoader {
	private String domain = "NIST CSF";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
	OrderedJSONObject controlNFList = new OrderedJSONObject();
   	
	public CSFLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String parentInternalId=null, parentCapability=null, parentDesc=null, childCapability=null, desc=null, internalId=null;
				String inscopeAssetClass=null, enablingAssetClass=null, securityFunction=null,priority=null, pciRef=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						parentInternalId = row[i].trim();
					} else if(i==1) {
						parentCapability = row[i].trim();
					} else if(i==2) {
						parentDesc = row[i].trim();
					} else if(i==3) {
						internalId = row[i].trim();
					} else if(i==4) {
						inscopeAssetClass = row[i].trim();
					} else if(i==5) {
						enablingAssetClass = row[i].trim();
					} else if(i==6) {
						securityFunction = row[i].trim();
					} else if(i==7) {
						childCapability = row[i].trim();
					} else if(i==8) {
						desc = row[i].trim();
					} else if(i==9) {
						priority = row[i].trim();
					} else if(i==10) {
						pciRef = row[i].trim();
					}
				}

				parentDesc = parentDesc.replaceAll("'","");
				desc = desc.replaceAll("'","");

				JSONArray pciArr = null;
				if(pciRef != null && pciRef.length() > 0 && !pciRef.equalsIgnoreCase("N/A")) {
					String[] pciRefArr = pciRef.split(",");
					List pciRefList = Arrays.asList(pciRefArr);
					pciArr = new JSONArray(pciRefList);
				} else {
					pciArr = new JSONArray();
				}

				System.out.println(parentInternalId+":"+parentCapability+":"+childCapability+":"+desc+":"+internalId);
		
				// Check Capability
				String parentId = checkCapability(rootId, parentCapability);	
				if(parentId == null) {
					levelOne++;
					parentId = generateUUID();
					insertCapability(parentId, rootId, parentInternalId, rootId, parentCapability, parentDesc, "", "", "", "", "1", levelOne);
					levelTwo = 0;
				} else {
					levelTwo++;
				}
				String childId = checkCapability(parentCapability, childCapability);		
				if(childId == null) {		
					levelTwo++;	
					childId = generateUUID();
					insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, inscopeAssetClass, enablingAssetClass, securityFunction, priority, "2", levelTwo);
					System.out.println("Successfully inserted:"+parentId+":"+parentCapability+":"+childId+":"+childCapability);
				}

				if(pciArr.size() > 0) {
					createComplianceRelationship(parentId, childId, "NIST CSF", pciArr);
					System.out.println("Compliance Nout Found List:"+controlNFList.toString());
				}
			}
		}
		
		conn.commit();
		conn.close();
	}

	public void updateIds() throws Exception {
		String sql = "update "+companyCode+".enterprise_business_capability set id = ?, parent_capability_id=? where child_capability_name=? and parent_capability_name=?";
		PreparedStatement pst = conn.prepareStatement(sql);

		String[] row;
		int rowCount=0;
		int level = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String id=null, parentCapability=null, parentDesc=null, childCapability=null, desc=null, internalId=null;
				String assetClass=null, securityFunction=null,priority=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						id = row[i].trim();
					} else if(i==1) {
						parentCapability = row[i].trim();
					} else if(i==2) {
						parentDesc = row[i].trim();
					} else if(i==3) {
						internalId = row[i].trim();
					} else if(i==4) {
						assetClass = row[i].trim();
					} else if(i==5) {
						securityFunction = row[i].trim();
					} else if(i==6) {
						childCapability = row[i].trim();
					} else if(i==7) {
						desc = row[i].trim();
					} else if(i==8) {
						priority = row[i].trim();
					}
				}
				/*
				String childId = checkCapability(parentCapability, childCapability);
				pst.setString(1, id);
				pst.setString(2, parentId);
				pst.setString(3, childCapability);
				pst.setString(4, parentCapability);
				pst.addBatch();
				*/

				
			}
		}
		pst.executeBatch();
		conn.commit();
	}
	
	private void checkRootCapability() throws Exception {
		String id = checkCapability("NIST CSF", "NIST CSF");
		//rootId = "d121ee96-b83d-4e3d-8b7d-3d90ba7c8c88";
		rootId = "NIST CSF";

		if(id == null) {
			insertCapability(rootId, rootId, "", "NIST CSF", "NIST CSF", "NIST CSF Controls", "", "", "", "", "0", 0);
		}
		capabilityList.put("NIST CSF", rootId);
		System.out.println("Root Id:"+rootId);
	}
	
	private String checkCapability(String parent, String child) throws Exception {
		String id = null;
		String capabilityCheckSQL = "select id,parent_capability_id,business_capability_ext from enterprise_business_capability where parent_capability_name = ? and child_capability_name = ? and business_capability_status='Y' and business_capability_domain=?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		pst.setString(3, domain);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			id = rs.getString("id");
		}
		rs.close();
		pst.close();
		
		return id;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String inscopeAssetClass, String enablingAssetClass, String securityFunction, String priority, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
		String owner = "", status = "Y";
		JSONArray inscopeclassArr = new JSONArray();
		String [] str = inscopeAssetClass.split(",");
		for(int i=0; i<str.length; i++) {
			if(str[i].trim().length() > 0) {
				inscopeclassArr.add(str[i].trim());
			}
		}
		JSONArray enablingclassArr = new JSONArray();
		String [] str1 = enablingAssetClass.split(",");
		for(int i=0; i<str1.length; i++) {
			if(str1[i].trim().length() > 0) {
				enablingclassArr.add(str1[i].trim());
			}
		}
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("currentMaturity", "0.0");
		factorObject.put("targetMaturity", "0.0");
		factorObject.put("baselineMaturity", "0.0");
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");

		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", parent);
		extObject.put("assetClass", inscopeclassArr);
		extObject.put("ecosystem", enablingclassArr);
		extObject.put("securityFunction", securityFunction);
		extObject.put("criticality", priority);
		extObject.put("frequency", new JSONArray());
		extObject.put("evidence", "");
		extObject.put("system_risk", new Integer(0));
		extObject.put("user_risk", new Integer(0));

		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2,parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void updateCapability(String id, String parentId, String childName, String parentName, String ext, String assetClass) throws Exception {
		String sql = "update enterprise_business_capability set id=?, parent_capability_id=?, business_capability_ext=? where child_capability_name = ? and parent_capability_name=?";
		JSONArray classArr = null;
		OrderedJSONObject extObj = new OrderedJSONObject(ext);
		if(extObj.has("assetClass")) {
			if(extObj.get("assetClass") instanceof String) {
				String capabilityAssetClassList = (String)extObj.get("assetClass");
				if(capabilityAssetClassList.length() > 0) {
					classArr = new JSONArray(capabilityAssetClassList);
				} else {
					classArr = new JSONArray();
				}
			} else if(extObj.get("assetClass") instanceof JSONArray) {
				classArr = (JSONArray)extObj.get("assetClass");
			} else {
				classArr = new JSONArray();
			}
			ArrayList list = new ArrayList();
			Iterator iter = classArr.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				list.add(s.trim());
				// Remove old assetclass with space in front
				iter.remove();
			}
			iter = list.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				classArr.add(s);
			}
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					if(!classArr.contains(str[i].trim())) {
						classArr.add(str[i].trim());
					}
				}
			}
		} else {
			classArr = new JSONArray();
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					classArr.add(str[i].trim());
				}
			}
		}
		extObj.put("assetClass", classArr);
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, id);
		pst.setString(2, parentId);
		pst.setString(3, extObj.toString());
		pst.setString(4, childName);
		pst.setString(5, parentName);
		System.out.println(pst.toString());
		pst.executeUpdate();
	}

	private void createComplianceRelationship(String rId, String aId, String type, JSONArray compArr) throws Exception {
		System.out.println("createComplianceRelationship:"+compArr+":"+type);
		String checkrelsql = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
        String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		String compsql = "select id from enterprise_business_capability where internal_id = ? and business_capability_domain like ?";
		PreparedStatement pst = conn.prepareStatement(insertrelsql);
		String updatedBy = "support@smarterd.com";

		if(compArr.size() > 0) {
			OrderedJSONObject ctx = new OrderedJSONObject();
			ctx.put("COMPLIANCE", type);
			Iterator iter = compArr.iterator();
			while(iter.hasNext()) {
				String str = (String)iter.next();

				String compId = null;
				PreparedStatement pst1 = conn.prepareStatement(compsql);
				pst1.setString(1, str);
				pst1.setString(2, type);
				ResultSet rs1 = pst1.executeQuery();
				while(rs1.next()) {
					compId = rs1.getString("id");
				}
				rs1.close();
				pst1.close();

				if(compId != null) {
					int count = 0;
					pst1 = conn.prepareStatement(checkrelsql);
					pst1.setString(1, aId);
					pst1.setString(2, compId);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						ctx.put("LINKED", "");
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						pst.setString(1, compId);
						pst.setString(2, aId);
						pst.setString(3, "COMPLIANCE");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "COMPLIANCE");
						pst.setString(1, aId);
						pst.setString(2, compId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
					}

					count = 0;
					pst1 = conn.prepareStatement(checkrelsql);
					pst1.setString(1, rId);
					pst1.setString(2, compId);
					rs1 = pst1.executeQuery();
					while(rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0) {
						ctx.put("COMPONENT TYPE", "CAPABILITY");
						ctx.put("LINKED", "Y");
						pst.setString(1, compId);
						pst.setString(2, rId);
						pst.setString(3, "COMPLIANCE");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
						ctx.put("COMPONENT TYPE", "COMPLIANCE");
						pst.setString(1, rId);
						pst.setString(2, compId);
						pst.setString(3, "CAPABILITY");
						pst.setString(4, ctx.toString());
						pst.setString(5, updatedBy);
						pst.setString(6, updatedBy);
						pst.setString(7, companyCode);
						pst.addBatch();
					}
				} else {
					if(!controlNFList.containsKey(str)) {
						controlNFList.put(str, type);
					}
				}
			}
		}
		pst.executeBatch();
		pst.close();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		String command = args[3];

		CSFLoader loader = new CSFLoader(filename, companyCode, env);
		if(command.equals("load")) {
			loader.process();
		} else if(command.equals("update")) {
			loader.updateIds();
		}
	}
}