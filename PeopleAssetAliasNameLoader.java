import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class PeopleAssetAliasNameLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public PeopleAssetAliasNameLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String resourceName=null, aliasName=null;
				String category=null, level=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						resourceName = row[i].trim();
					} else if(i==1) {
						aliasName = row[i].trim();
					}
				}
				System.out.println(resourceName+":"+aliasName);

				String id=null, extension=null;
				String peopleCheckSQL = "select id,extension from enterprise_asset where asset_name = ? and asset_category='PEOPLE'";
				String peopleUpdateSQL = "update enterprise_asset set extension = ? where asset_name = ?";
				PreparedStatement pst = conn.prepareStatement(peopleCheckSQL);
				pst.setString(1, resourceName);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
					extension = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(id != null) {
					System.out.println("Found People:"+resourceName);
					OrderedJSONObject extObj=null;
					if(extension != null) {
						extObj = new OrderedJSONObject(extension);
						if(extObj.has("Alias Names")) {
							JSONArray arr = (JSONArray)extObj.get("Alias Names");
							if(!arr.contains(aliasName)) {
								arr.add(aliasName);
								extObj.put("Alias Names", arr);
							}
						} else {
							JSONArray arr = new JSONArray();
							arr.add(aliasName);
							extObj.put("Alias Names", arr);
						}
					}

					pst = conn.prepareStatement(peopleUpdateSQL);
					pst.setString(1, extObj.toString());
					pst.setString(2, resourceName);
					pst.executeUpdate();
					pst.close();
				}

				resourceName = null;
				aliasName = null;
			}
		}
		
		conn.commit();
		conn.close();
	}
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		PeopleAssetAliasNameLoader loader = new PeopleAssetAliasNameLoader(filename, companyCode, env);
		loader.process();
	}
}