import java.sql.*;
import java.util.*;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class AssetClassLoader {
    private String updatedBy = null;
    private String companyCode = null;
    private String hostname = "localhost";
    private Connection conn = null;

    public AssetClassLoader(String updatedBy, String companyCode, String env) throws Exception {
        this.updatedBy = updatedBy;
        this.companyCode = companyCode;

        if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
    }

    private OrderedJSONObject initializeAssetClass() throws Exception {
        System.out.println("Loading Asset Class...");
        OrderedJSONObject res = new OrderedJSONObject();
        String insertSQL = "insert into enterprise_setting(id,setting_name,setting_category,setting_type,updated_by,updated_timestamp,company_code) value(null,?,'ASSET CLASS','ASSET',?,CURRENT_TIMESTAMP,?)";
        String checkSQL = "select count(*) count from enterprise_setting where setting_name like ? and setting_category = 'ASSET CLASS' and setting_type = 'ASSET'";

        LinkedHashSet classList = new LinkedHashSet<>();

        // Get Class List - This can be an expensive operation
        // Need to figure out a mechanism to prepopulate this information in a seperate table
        String sql = "select asset_category,replace(json_extract(extension, '$.\"Type\"'), '\"', '') assetclass1,replace(json_extract(extension, '$.\"Sub-Type\"'), '\"', '') assetclass2,json_extract(extension, '$.\"Ecosystem\"') assetclass3,replace(json_extract(extension, '$.\"Business app/IT app\"'), '\"', '') assetclass4 from enterprise_asset";
        PreparedStatement pst = conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            String assetCategory = rs.getString("asset_category");
            String assetClass1 = rs.getString("assetclass1");
            String assetClass2 = rs.getString("assetclass2");
            String assetClass3 = rs.getString("assetclass3");
            String assetClass4 = rs.getString("assetclass4");
            if(assetClass1 != null && assetClass1.length() > 0) {
                classList.add(assetClass1);
            }
            if(assetClass2 != null && assetClass2.length() > 0) {
                classList.add(assetClass2);
            }
            System.out.println(assetClass3);
            if(assetClass3 != null && assetClass3.length() > 0 && assetClass3.indexOf("[") != -1) {
                JSONArray arr = new JSONArray(assetClass3);
                Iterator iter = arr.iterator();
                while(iter.hasNext()) {
                    String str = (String)iter.next();
                    if(str != null && str.length() > 0) {
                        classList.add(str);
                    }
                }
            } else if(assetClass3 != null && assetClass3.length() > 0) {
                assetClass3 = assetClass3.replaceAll("\"", "");
                if(assetClass3.length() > 0) {
                    classList.add(assetClass3);
                }
            }
            if(assetClass4 != null && assetClass4.length() > 0 && assetCategory.equals("APPLICATION")) {
                classList.add(assetClass4);
            } 
            //System.out.println("Asset Id:"+assetId+":"+assetClass1+":"+assetClass2+":"+assetClass3+":"+assetClass4);
        }
        rs.close();
        pst.close();

        pst = conn.prepareStatement(insertSQL);
        Iterator iter = classList.iterator();
        while(iter.hasNext()) {
            String assetClass = (String)iter.next();

            // Check if present
            int count = 0;
            PreparedStatement pst1 = conn.prepareStatement(checkSQL);
            pst1.setString(1, assetClass);
            rs = pst1.executeQuery();
            while(rs.next()) {
                count = rs.getInt("count");
            }
            rs.close();
            pst1.close();

            // Insert into enterprise_setting
            if(count == 0) {
                pst.setString(1, assetClass);
                pst.setString(2, updatedBy);
                pst.setString(3, companyCode);
                pst.addBatch();
            }            
        }

        pst.executeBatch();
        conn.commit();

        res.put("STATUS", "Successfully loaded Asset Class!!!");
        System.out.println("Successfully loaded Asset Class!!!");
        return res;
    }

    private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        String updatedBy = args[0];
		String companyCode = args[1];
        String env = args[2];
        
        AssetClassLoader loader = new AssetClassLoader(updatedBy, companyCode, env);
        loader.initializeAssetClass();
    } 

}