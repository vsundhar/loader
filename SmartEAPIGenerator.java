import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmartEAPIGenerator {
	public static void main(String[] args)throws Exception {
		CSVPrinter writer = new CSVPrinter(new FileWriter("./SmartEAPI.csv"), CSVFormat.EXCEL);
		File[] files = new File("../pe-api/src/main/java/com/smarterd/processor").listFiles();
		try {
			List list = new ArrayList();
			String line1[] = {"File Name","Access Type","Return Type","Method Name","Arguments"};
			list.add(line1);
			String line= " ";
			int count = 0;
			for(File file : files) {
				if(file.isFile()) {
					count++;
					if(file.getName().indexOf("SmarteProcessor") == -1) {
						FileReader fr = new FileReader(file);
						BufferedReader br = new BufferedReader(fr);
						ArrayList<String> array = new ArrayList<String>();
						while((line = br.readLine()) != null) {
							String filterLine = filterLines(line);
							if(filterLine != "") {
								String m1 = filterLine.substring(0, filterLine.indexOf("(")-1);
								String m2 = filterLine.substring(filterLine.indexOf("("));
								m2 = m2.replaceAll("[\\(\\)]", "");
								String[] arr = m1.split(" ");
								System.out.println(filterLine);
								if(arr[0].length() > 0) {
									String line2[] = {file.getName(),arr[0],arr[1],arr[2],m2};
									list.add(line2);
								}
							}
						}
					}
				}
			}
			writer.printRecords(list);
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	static String filterLines(String line) {
		String out = "";
		List<String> method = new ArrayList<>();
		Pattern reMethod= Pattern.compile("(public|private)(.*\\)).*\\{");
		Matcher matchMethod = reMethod.matcher(line);
		while (matchMethod.find()) {
			method.add(matchMethod.group());
		}
		if(method.size() > 0) {
			if(line.indexOf("void main") == -1) {
				out = line.replaceAll("(\\w+)\\((.*)\\)", "$1 ($2)");
				if(out.indexOf("throws") != -1) {
					out = out.substring(0, out.indexOf("throws")).trim();
				}
			}
		}
		return out;
	}
}


