import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Base64;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.ContentType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;

public class SmarteIvantiHttp {
   private String companyCode = null;
   private Connection conn = null;
   static String apiKey = "C99C9507344742E3A545BAC7716575B5";
   static String ivantiUrl = "http://lhdismv01/HEAT/api/odata/businessobject/";
   
   String hostname = "localhost";
   //String hostname = "dhamysqldb01";
   public SmarteIvantiHttp(String companyCode, String env) throws Exception {
      this.companyCode = companyCode;
        
      // Connect Mysql
      mysqlConnect(companyCode);
   }

   private void clean() throws Exception {
      conn.commit();
      conn.close();
   }

   private void sync(int batchsize, int skipCount) throws Exception {
      System.out.println("In sync...");

      String unit = "days";
      int interval = 1, batchCount=25;
      DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      LocalDate ld = LocalDate.now();
      LocalDate sdt = null;
      if(unit.equalsIgnoreCase("mts")) {
         sdt = ld.minus(interval, ChronoUnit.MINUTES);
      } else if(unit.equalsIgnoreCase("hrs")) {
         sdt = ld.minus(interval, ChronoUnit.HOURS);
      } else if(unit.equalsIgnoreCase("days")) {
         sdt = ld.minus(interval, ChronoUnit.DAYS);
      } else if(unit.equalsIgnoreCase("months")) {
         sdt = ld.minus(interval, ChronoUnit.MONTHS);
      }
      String currentDate = ld.format(df);
      String modifiedDate = sdt.format(df);
      String sd = modifiedDate+"T20:00:00Z";
      String ed = currentDate+"T20:00:00Z";

      String url = ivantiUrl+"incidents";
      String dateFilter = "LastModDateTime ge "+sd+" and LastModDateTime lt "+ed;
      String limit = "$top="+batchCount+"&$skip="+skipCount;
      String columnList = "RecId,Subject,Symptom,IncidentNumber,ResolutionSymptom,Priority,Status,Owner,Source,ReportedDAte,ResolvedDateTime,ReportedBy,ResolvedBy,CustomerDepartment,AssetID_Category,CauseCode,Impact,DHA_Incident_Category,DHA_Incident_SubCategory,OwnerTeam,FirstCallResolution";
      String orderBy = "LastModDateTime asc";
      String filter = "$select="+columnList+"&$filter="+dateFilter+"&"+limit+"&$orderby="+orderBy;
      System.out.println(url+"?"+filter);
      filter = URLEncoder.encode(filter).toString();
      //url = url+"?"+filter;

      //url = url+"&$select="+columnList+"&$filter="+filter+"&$orderby=LastModDateTime asc";
      //url = url+"&$select="+columnList+"&$orderby=LastModDateTime asc";
      //url = url+"&$orderby=LastModDateTime asc";
     
		// Add Options
      /*
      CUrl curl = new CUrl();
		curl = curl.opt("-X", "GET");
		curl = curl.opt("-H", "Authorization:rest_api_key="+apiKey);
		curl = curl.opt("-H", "Content-Type:application/json");
      curl = curl.opt("-G", url);
      curl = curl.opt("-d", URLEncoder.encode("$top=25"));
      curl = curl.opt("-d", URLEncoder.encode("$skip="+skipCount));
      curl = curl.opt("-d", URLEncoder.encode("$select="+columnList));
      curl = curl.opt("-d", URLEncoder.encode("$filter="+dateFilter));
      curl = curl.opt("-d", URLEncoder.encode("$orderby="+orderBy));
      
		String res = curl.exec(null);
      */

      //System.out.println(res);
      URL httpUrl = new URL(url);
      HttpURLConnection http = (HttpURLConnection)httpUrl.openConnection();
      http = (HttpURLConnection)httpUrl.openConnection();
      http.setRequestMethod("GET");
      http.setDoOutput(true);
      http.setRequestProperty("Authorization", "rest_api_key="+apiKey);
      http.setRequestProperty("Content-Type", "application/json");
      //http.setRequestProperty("Connection", "keep-alive");
      http.connect();

      // Send Request
      OutputStream os = http.getOutputStream();
      OrderedJSONObject res = null;
      StringBuilder response = new StringBuilder();
      BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
      String responseLine = null;
      while ((responseLine = br.readLine()) != null) {
         response.append(responseLine.trim());
      }
      if(response.length() > 0) {
         res = new OrderedJSONObject(response.toString());
      } else {
         res = new OrderedJSONObject();
      }
      http.disconnect();

      OrderedJSONObject outputList = new OrderedJSONObject(res);

       // Process Response
      processIVantiIncidentResponse(outputList);

      if(skipCount == 0) {
         int count = ((Integer)outputList.get("@odata.count")).intValue();
         batchsize = (int)Math.ceil((double)count/(double)25);
         System.out.println(count+":"+batchsize);
      }
        
      // Decrement batch count 
      batchsize = batchsize-1;

      if(batchsize > 0) {
         skipCount = skipCount + 25;
         sync(batchsize, skipCount);
      }
   }

   private void processIVantiIncidentResponse(OrderedJSONObject outputList) throws Exception {
      System.out.println("In processiVantiIncidentResponse...");
      JSONArray list = (JSONArray)outputList.get("value");
      System.out.println(list.size());
      Iterator listIter = list.iterator();
      while(listIter.hasNext()) {
         JSONObject obj = (JSONObject)listIter.next();
         //System.out.println(obj.toString());
         String name=null, desc=null, internalId=null, ticketRef=null, impactDetails=null, resolution=null, resolutionDetails=null;
         String priority=null, lifecycle=null,incidentTime=null,resolutionTime=null, source=null, owner=null, dept=null, ecosystem=null;
         String assignmentGroup=null, reportedBy=null, resolvedBy=null, causeCode=null;
         boolean firstcallResolution=false;
         String assetRecId=null, incidentLink=null, problemRecId=null, impact=null, category=null, subcategory=null;
         Iterator iter = obj.keySet().iterator();
         while(iter.hasNext()) {
            String key = (String)iter.next();
            if(key.equalsIgnoreCase("RecId")) {
               internalId = (String)obj.get("RecId");
            } else if(key.equalsIgnoreCase("Subject")) {
               name = (String)obj.get("Subject");
            } else if(key.equalsIgnoreCase("Symptom")) {
               desc = (String)obj.get("Symptom");
            } else if(key.equalsIgnoreCase("IncidentNumber")) {
               ticketRef = ((Integer)obj.get("IncidentNumber")).toString();
            } else if(key.equalsIgnoreCase("ResolutionSymptom")) {
               resolutionDetails = (String)obj.get("ResolutionSymptom");
            } else if(key.equalsIgnoreCase("Priority")) {
               priority = (String)obj.get("Priority");
            } else if(key.equalsIgnoreCase("Status")) {
               lifecycle = (String)obj.get("Status");
            } else if(key.equalsIgnoreCase("Owner")) {
               owner = (String)obj.get("Owner");
            } else if(key.equalsIgnoreCase("Source")) {
               source = (String)obj.get("Source");
            } else if(key.equalsIgnoreCase("ReportedDAte")) {
               incidentTime = (String)obj.get("ReportedDAte");
            } else if(key.equalsIgnoreCase("ResolvedDateTime")) {
               resolutionTime = (String)obj.get("ResolvedDateTime");
            } else if(key.equalsIgnoreCase("ReportedBy")) {
               reportedBy = (String)obj.get("ReportedBy");
            } else if(key.equalsIgnoreCase("ResolvedBy")) {
               resolvedBy = (String)obj.get("ResolvedBy");
            } else if(key.equalsIgnoreCase("CustomerDepartment")) {
               dept = (String)obj.get("CustomerDepartment");
            } else if(key.equalsIgnoreCase("AssetID_Category")) {
               ecosystem = (String)obj.get("AssetID_Category");
            } else if(key.equalsIgnoreCase("CauseCode")) {
               causeCode = (String)obj.get("CauseCode");
            } else if(key.equalsIgnoreCase("Impact")) {
               impact = (String)obj.get("Impact");
            } else if(key.equalsIgnoreCase("DHA_Incident_Category")) {
               category = (String)obj.get("DHA_Incident_Category");
            } else if(key.equalsIgnoreCase("DHA_Incident_SubCategory")) {
               subcategory = (String)obj.get("DHA_Incident_SubCategory");
            } else if(key.equalsIgnoreCase("OwnerTeam")) {
               assignmentGroup = (String)obj.get("OwnerTeam");
            } else if(key.equalsIgnoreCase("FirstCallResolution")) {
               firstcallResolution = ((Boolean)obj.get("FirstCallResolution")).booleanValue();
            }
         }

         if(resolutionDetails == null) {
            resolutionDetails = "";
         }
         if(resolutionTime == null) {
            resolutionTime = "";
         }
         JSONArray ecoArr = new JSONArray();
         if(ecosystem != null && ecosystem.length() > 0) {
               ecoArr.add(ecosystem);
         }

         if(incidentTime.indexOf("+") != -1) {
            incidentTime = incidentTime.substring(0,incidentTime.indexOf("+"));
            incidentTime = formatIncidentTime(incidentTime);
         }
         if(resolutionTime.length() > 0 && resolutionTime.indexOf("+") != -1) {
            resolutionTime = resolutionTime.substring(0,resolutionTime.indexOf("+"));
            resolutionTime = formatIncidentTime(resolutionTime);
         }

         // Get CI Details
         String location="", configName="", assetType="",assetSubType="";
         /*
         OrderedJSONObject ciDetails = getCIDetails(internalId);
         if(ciDetails.has("location")) {
               location = (String)ciDetails.get("location");
         }
         if(ciDetails.has("config_name")) {
               configName = (String)ciDetails.get("config_name");
         }
         if(ciDetails.has("asset_type")) {
               assetType = (String)ciDetails.get("asset_type");
         }
         if(ciDetails.has("asset_subtype")) {
               assetSubType = (String)ciDetails.get("asset_subtype");
         }
         */

         String checksql = "select _id, extension from enterprise_incident_test where ticketRef = ?";
         String id = null, ext = null;
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, ticketRef);
         ResultSet rs = pst.executeQuery();
         while (rs.next()) {
               id = rs.getString("_id");
               ext = rs.getString("extension");
         }
         rs.close();
         pst.close();

         if (id == null) {
               System.out.println("Inserting Incident...");
               id = generateUUID();
               OrderedJSONObject extObj = new OrderedJSONObject();
               extObj.put("RecId", internalId);
               extObj.put("Category", category);
               extObj.put("Sub Category", subcategory);
               extObj.put("Created By", reportedBy);
               extObj.put("service_type", "INCIDENT");
               extObj.put("Assignment Group", assignmentGroup);
               extObj.put("Problem", "");
               extObj.put("Assignee", owner);
               extObj.put("Resolved By", resolvedBy);
               extObj.put("Root Cause", causeCode);
               //extObj.put("On-Hold Reason", onHoldReason);
               //extObj.put("Resolution Code", resolutionCode);
               extObj.put("Department", dept);
               extObj.put("Location", location);
               extObj.put("config_id", configName);
               extObj.put("ci_type", assetType);
               extObj.put("ci_subtype", assetSubType);
               //extObj.put("Additional Comment", addlComments);
               extObj.put("custom", new OrderedJSONObject());

               String insertsql = "insert into enterprise_incident_test(_id,ecosystem,name,ticketRef,description,impactDetails,resolutionDetails,source,priority,lifecycle,incidentTime,resolutionTime,businessstakeholder,itstakeholder,extension,createdBy,updatedBy) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
               pst = conn.prepareStatement(insertsql);
               pst.setString(1, id);
               pst.setString(2, ecoArr.toString());
               pst.setString(3, name);
               pst.setString(4, ticketRef);
               pst.setString(5, desc);
               pst.setString(6, impact);
               pst.setString(7, resolutionDetails);
               pst.setString(8, source);
               pst.setString(9, priority);
               pst.setString(10, lifecycle);
               pst.setString(11, incidentTime);
               pst.setString(12, resolutionTime);
               pst.setString(13, "");
               pst.setString(14, owner);
               pst.setString(15, extObj.toString());
               pst.setString(16, "support@smarterd.com");
               pst.setString(17, "support@smarterd.com");
               pst.executeUpdate();
               pst.close();
         } else {
               System.out.println("Updating Incident...");
               OrderedJSONObject extObj = new OrderedJSONObject(ext);
               extObj.put("RecId", internalId);
               extObj.put("Category", category);
               extObj.put("Sub Category", subcategory);
               extObj.put("Created By", reportedBy);
               extObj.put("service_type", "INCIDENT");
               extObj.put("Assignment Group", assignmentGroup);
               extObj.put("Problem", "");
               extObj.put("Assignee", owner);
               extObj.put("Resolved By", resolvedBy);
               extObj.put("Root Cause", causeCode);
               //extObj.put("On-Hold Reason", onHoldReason);
               //extObj.put("Resolution Code", resolutionCode);
               extObj.put("Department", dept);
               extObj.put("Location", location);
               extObj.put("config_id", configName);
               extObj.put("ci_type", assetType);
               extObj.put("ci_subtype", assetSubType);
               extObj.put("custom", new OrderedJSONObject());
            
               String updatesql = "update enterprise_incident_test set ecosystem=?,name=?,description=?,priority=?,lifecycle=?,resolutionTime=?,extension=? where _id=?";
               pst = conn.prepareStatement(updatesql);
               pst.setString(1, ecoArr.toString());
               pst.setString(2, name);
               pst.setString(3, desc);
               pst.setString(4, priority);
               pst.setString(5, lifecycle);
               pst.setString(6, resolutionTime);
               pst.setString(7, extObj.toString());
               pst.setString(8, id);
               pst.executeUpdate();
               pst.close();
         }
      }

      System.out.println("Successfully processed iVanti Incident Response!!!");
   }

   private void updateSyncStatus(String name, String command, String action, String status, String msg, Connection dbConn) {
      System.out.println("In updateLog:"+name+":"+command+":"+action+":"+status);
      String insertsql = "insert into enterprise_job (id, name, command, action, status, status_msg,job_order,active,company_code, updated_by) values(null,?,?,?,?,?,0,'N',?,?)";
      String updatesql = "update enterprise_job set status = ?,status_msg=?,updated_by=? where command=? and action=?";
      String checksql = "select status from enterprise_job where command=? and action=?";

      try {
         String currentstatus = null;
         PreparedStatement pst = conn.prepareStatement(checksql);
         pst.setString(1, command);
         pst.setString(2, action);
         ResultSet rs = pst.executeQuery();
         while(rs.next()) {
               currentstatus = rs.getString("status");
         }
         rs.close();
         pst.close();
   
         if(currentstatus == null) {
               pst = dbConn.prepareStatement(insertsql);
               pst.setString(1, name);
               pst.setString(2, command);
               pst.setString(3, action);
               pst.setString(4, status);
               pst.setString(5, msg);
               pst.setString(6, companyCode);
               pst.setString(7, "support@smarterd.com");
         } else {
               pst = dbConn.prepareStatement(updatesql);
               pst.setString(1, status);
               pst.setString(2, msg);
               pst.setString(3, "support@smarterd.com");
               pst.setString(4, command);
               pst.setString(5, action);
         }
         pst.executeUpdate();
         System.out.println("Successfully updated Log!!!");
      } catch(Exception ex) {
         System.out.println("Error Updating Log Status!!!!");
         ex.printStackTrace();
      }
   }

   // Get Location, Site and Asset CI Details
   private OrderedJSONObject getCIDetails(String internalId) throws Exception {
      System.out.println("In getCIDetails..."+internalId);
      String ciUrl = ivantiUrl+"/CIs?";
      String columnList="Name,Site,ivnt_AssetFullType";
      String filter = "$select="+columnList+"&$filter=RecID eq '"+internalId+"'";
      filter = URLEncoder.encode(filter).toString();
      ciUrl = ciUrl+filter;
      // Add Options
      CUrl curl = new CUrl();
		curl = curl.opt("-X", "GET");
		curl = curl.opt("-H", "Authorization:rest_api_key="+apiKey);
		curl = curl.opt("-H", "Content-Type:application/json");
      curl = curl.opt("-G", ciUrl);
      //curl = curl.opt("-d", "filter=Status%20eq%20Closed");
		String output = curl.exec(null);
      System.out.println(output);
      /*
      URL httpUrl = new URL(ciUrl);
      HttpURLConnection http = (HttpURLConnection)httpUrl.openConnection();
      http = (HttpURLConnection)httpUrl.openConnection();
      http.setRequestMethod("GET");
      http.setDoOutput(true);
      http.setRequestProperty("Authorization", "rest_api_key="+apiKey);
      http.setRequestProperty("Content-Type", "application/json");
      //http.setRequestProperty("Connection", "keep-alive");
      http.connect();

      // Send Request
      OutputStream os = http.getOutputStream();
      OrderedJSONObject res = null;
      StringBuilder response = new StringBuilder();
      BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
      String responseLine = null;
      while ((responseLine = br.readLine()) != null) {
         response.append(responseLine.trim());
      }
      if(response.length() > 0) {
         res = new OrderedJSONObject(response.toString());
      } else {
         res = new OrderedJSONObject();
      }
      http.disconnect();
      */
      OrderedJSONObject res = new OrderedJSONObject(output);
      System.out.println("Successfully executed CI API:"+res.toString());
      return res;
   }

   private synchronized String generateUUID() throws Exception {
      UUID uuid = UUID.randomUUID();
      return(uuid.toString());
   }

   // Format Incident and Resolution Time
   private String formatIncidentTime(String incidentTime) throws Exception {
      incidentTime = formatUSIncidentTime(incidentTime);
      return incidentTime;
	}

   private String formatUSIncidentTime(String incidentTime) throws Exception {
      //System.out.println("In formatUSIncidentTime..."+incidentTime);
      DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
      DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      DateTimeFormatter df2 = DateTimeFormatter.ofPattern("MM/dd/yy HH:mm");
      DateTimeFormatter df3 = DateTimeFormatter.ofPattern("M/dd/yy HH:mm");
      DateTimeFormatter df4 = DateTimeFormatter.ofPattern("M/dd/yy H:mm");
      DateTimeFormatter df5 = DateTimeFormatter.ofPattern("MM/d/yy HH:mm");
      DateTimeFormatter df6 = DateTimeFormatter.ofPattern("M/d/yy HH:mm");
      DateTimeFormatter df7 = DateTimeFormatter.ofPattern("M/d/yy H:mm");
      DateTimeFormatter df8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

      if(incidentTime.indexOf("T") != -1) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df8);
         incidentTime = df.format(dt);
      } else if(incidentTime.indexOf("-") != -1) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df1);
         incidentTime = df.format(dt);
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 14) {
			LocalDateTime dt = LocalDateTime.parse(incidentTime, df2);
			incidentTime = df.format(dt);
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 13) {
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df3);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df5);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 12) {
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df4);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
			try {
				LocalDateTime dt = LocalDateTime.parse(incidentTime, df6);
				incidentTime = df.format(dt);
			} catch(Exception ex) {

			}
		} else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 11) {
			LocalDateTime dt = LocalDateTime.parse(incidentTime, df7);
			incidentTime = df.format(dt);
		}

		return incidentTime;
    }

   // Format Incident and Resolution Time
   private String formatUKIncidentTime(String incidentTime) throws Exception {
      DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
      DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      DateTimeFormatter df2 = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");
      DateTimeFormatter df3 = DateTimeFormatter.ofPattern("dd/M/yy HH:mm");
      DateTimeFormatter df4 = DateTimeFormatter.ofPattern("dd/M/yy H:mm");
      DateTimeFormatter df5 = DateTimeFormatter.ofPattern("d/MM/yy HH:mm");
      DateTimeFormatter df6 = DateTimeFormatter.ofPattern("d/M/yy HH:mm");
      DateTimeFormatter df7 = DateTimeFormatter.ofPattern("d/M/yy H:mm");
      DateTimeFormatter df8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

      if(incidentTime.indexOf("T") != -1) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df8);
         incidentTime = df.format(dt);
      } else if(incidentTime.indexOf("-") != -1) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df1);
         incidentTime = df.format(dt);
      } else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 14) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df2);
         incidentTime = df.format(dt);
      } else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 13) {
         try {
            LocalDateTime dt = LocalDateTime.parse(incidentTime, df3);
            incidentTime = df.format(dt);
         } catch(Exception ex) {

         }
         try {
            LocalDateTime dt = LocalDateTime.parse(incidentTime, df5);
            incidentTime = df.format(dt);
         } catch(Exception ex) {

         }
      } else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 12) {
         try {
            LocalDateTime dt = LocalDateTime.parse(incidentTime, df4);
            incidentTime = df.format(dt);
         } catch(Exception ex) {

         }
         try {
            LocalDateTime dt = LocalDateTime.parse(incidentTime, df6);
            incidentTime = df.format(dt);
         } catch(Exception ex) {

         }
      } else if(incidentTime.indexOf("/") != -1 && incidentTime.length() == 11) {
         LocalDateTime dt = LocalDateTime.parse(incidentTime, df7);
         incidentTime = df.format(dt);
      }

      return incidentTime;
   }


   private void mysqlConnect(String dbname) throws Exception {
      String userName = "root", password = "smarterD2018!";
      //String userName = "smarterd", password = "uXrPwz64Dg21DgYTMF";
      //String port = "3801";
      String port = "3306";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":"+port+"/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }

   public static void main(String args[]) throws Exception {
      String companyCode = args[0];
      String env = args[1];

      SmarteIvantiHttp ivanti = new SmarteIvantiHttp(companyCode, env);
      ivanti.sync(1, 0);
      //ivanti.getCIDetails(ivantiUrl, apiKey, "C55B3E84284492C91AE87F57D16988B");
      //ivanti.getIncidentDetail("00018B7320004B22ABF2AFEF3F12DA0C");

      ivanti.clean();
   }
}