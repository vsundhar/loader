import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.text.SimpleDateFormat;
import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.CellStyle; 
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class MADSPOAMGenerator {
   private String companyCode = null;
   private Connection conn = null;
  
   public MADSPOAMGenerator(String companyCode) throws Exception {
      this.companyCode = companyCode;

      // Connect Mysql
      mysqlConnect(companyCode);
   }

   public void generateReport(String reportName) throws Exception {
      XSSFWorkbook poamBook = new XSSFWorkbook(new FileInputStream(reportName)); 
      XSSFSheet poamSheet = poamBook.getSheet("Open POA&Ms"); 
      XSSFRow row = poamSheet.createRow(4);
      Cell uniqueId = row.createCell(0);
      uniqueId.setCellValue("Vijay");
      CellStyle style = uniqueId.getCellStyle();
      style.setAlignment(HorizontalAlignment.CENTER);
      style.setVerticalAlignment(VerticalAlignment.CENTER);
      
      String file = "vijay.xlsx";
      poamBook.write(new FileOutputStream(file));
      poamBook.close();
   }

   public JSONArray getAssessmentDetail(String id, String internalId) throws Exception {
      JSONArray detailList = new JSONArray();
      String sql = "select a.id id,internal_id,a.control_id control_id,a. parent_control_id,a. control_family,a.control_level control_level,json_unquote(json_extract(a.extension, '$.\"artifacts_referenced\"')) artifacts_referenced, child_capability_name,parent_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) control_family,json_unquote(json_extract(business_capability_ext, '$.custom.\"NIST Mapping\"')) nist_mapping,json_unquote(json_extract(business_capability_factor, '$.\"currentMaturity\"')) current_maturity,b.internal_id internal_id,a.gaps gaps,a.control_in_place control_in_place,a.status status,a.severity severity,a.effort effort,a.artifacts artifacts,a.notes notes,a.recommendation recommendation,a.extension extension,a.updated_timestamp updated_timestamp,a.created_by created_by,a.updated_by updated_by from enterprise_assessment_detail a, enterprise_business_capability b where a.control_id=b.id and a.assessment_id = ? and b.internal_id like ? and a.status like ? and a.control_level = 2";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, id);
      pst.setString(2, internalId);
      if(internalId.equals("%")) {    // For POA&M
          pst.setString(3, "Not Implemented");
      } else {    // For Technical Report
          pst.setString(3, "%");
      }
      System.out.println(pst.toString());
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String controlId = rs.getString("control_id");
         internalId = rs.getString("internal_id");
         String assessmentDetailId = rs.getString("id");
         String practiceName = rs.getString("child_capability_name");
         String status = rs.getString("status");
         String severity = rs.getString("severity");
         String effort = rs.getString("effort");
         String controlInplace = rs.getString("control_in_place");
         String gaps = rs.getString("gaps");
         String recommendation = rs.getString("recommendation");
         String artifacts = rs.getString("artifacts");
         String artifactsRef = rs.getString("artifacts_referenced");
         String nistMapping = rs.getString("nist_mapping");
         String currentMaturity = rs.getString("current_maturity");
         if(status == null) {
            status = "";
         }
         if(severity == null) {
            severity = "";
         }
         if(effort == null) {
            effort = "";
         }
         if(controlInplace == null) {
            controlInplace = "";
         }
         if(gaps == null) {
            gaps = "";
         }
         if(recommendation == null) {
            recommendation = "";
         }
         if(artifacts == null) {
            artifacts = "";
         }
         if(artifactsRef == null) {
            artifactsRef = "";
         }
         OrderedJSONObject obj = new OrderedJSONObject();
         obj.put("control_id", controlId);
         obj.put("internal_id", internalId);
         obj.put("assessment_detail_id", assessmentDetailId);
         obj.put("practice_name", practiceName);
         obj.put("status", status);
         obj.put("severity", severity);
         obj.put("effort", effort);
         obj.put("control_in_place", controlInplace);
         obj.put("gaps", gaps);
         obj.put("recommendation", recommendation);
         obj.put("artifacts", artifactsRef);
         obj.put("nist_mapping", nistMapping);
         obj.put("control_maturity", currentMaturity);
         detailList.add(obj);
      }
      rs.close();
      pst.close();

      return detailList;
   }

   private void createRisk(String assessmentId) throws Exception {
      String insertsql = "insert into enterprise_collaboration(id,component_id,component_name,component_type,name,message,type,status,extension,created_timestamp,created_by,updated_timestamp,updated_by,company_code) values(?,?,?,?,?,?,'RISK','OPEN',?,current_timestamp,?,current_timestamp,?,?)";
      String insertrelsql = "insert into enterprise_rel(id,asset_id,component_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)"; 
		PreparedStatement pst = conn.prepareStatement(insertsql);
      PreparedStatement pst3 = conn.prepareStatement(insertrelsql);
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yy");
      LocalDate dt = LocalDate.now();
      String month = Integer.toString(dt.getMonthValue());
      if(month.length() == 1) {
            month = "0"+month;
      }
      String year = Integer.toString(dt.getYear()).substring(2);
      int count = 1;
      JSONArray detailList = getAssessmentDetail(assessmentId, "%");
      Iterator iter = detailList.iterator();
      while(iter.hasNext()) {
         OrderedJSONObject detailObj = (OrderedJSONObject)iter.next();
         String uniqueId = "";   // risk_register
         if(count < 10) {
            uniqueId = "MS-"+month+year+"-00"+count;
         } else if(count < 100) {
            uniqueId = "MS-"+month+year+"-0"+count;
         } else {
            uniqueId = "MS-"+month+year+"-"+count;
         }
         String nistMapping = "";
         if(detailObj.has("nist_mapping")) {
            nistMapping = (String)detailObj.get("nist_mapping");
         }
         String controlId = (String)detailObj.get("control_id");
         String internalId = (String)detailObj.get("internal_id");   // selected_component
         String assessmentDetailId = (String)detailObj.get("assessment_detail_id");
         String practiceName = (String)detailObj.get("practice_name");  // threat
         String controlInPlace = (String)detailObj.get("control_in_place");   // Notes
         String gaps = (String)detailObj.get("gaps"); // Vulnerability
         String severity = (String)detailObj.get("severity");  // current_risk
         String recommendation = (String)detailObj.get("recommendation");  // mitigation
         String controlMaturity = (String)detailObj.get("control_maturity"); 

         OrderedJSONObject message = new OrderedJSONObject();
         JSONArray cList = new JSONArray();
         cList.add(internalId);
         JSONArray acArr = new JSONArray();
         OrderedJSONObject rrObj = new OrderedJSONObject();
         OrderedJSONObject crObj = new OrderedJSONObject();
         if(severity.equalsIgnoreCase("High")) {
            crObj.put("harm", new Integer(4));
            crObj.put("probability", new Integer(3));
            crObj.put("risk", new Integer(12));
         } else if(severity.equalsIgnoreCase("Medium")) {
            crObj.put("harm", new Integer(3));
            crObj.put("probability", new Integer(2));
            crObj.put("risk", new Integer(6));
         } else if(severity.equalsIgnoreCase("Low")) {
            crObj.put("harm", new Integer(2));
            crObj.put("probability", new Integer(1));
            crObj.put("risk", new Integer(2));
         }
         OrderedJSONObject erObj = new OrderedJSONObject();
         OrderedJSONObject ciaObj = new OrderedJSONObject();
         OrderedJSONObject extObj = new OrderedJSONObject();
         extObj.put("source", "gap");
         extObj.put("assessment_id", assessmentId);
         extObj.put("assessment_detail_id", assessmentDetailId);
         extObj.put("risk_register", uniqueId);
         extObj.put("control_maturity", controlMaturity);
         extObj.put("threat", practiceName);
         extObj.put("vulnerability", gaps);
         extObj.put("priority", severity);
         extObj.put("current_risk", crObj);
         extObj.put("estimated_risk", erObj);
         extObj.put("cia", ciaObj);
         extObj.put("remediation_option", "Mitigate Risk");
         extObj.put("mitigation", recommendation);
         extObj.put("notes", controlInPlace);
         extObj.put("control_effectiveness", "");
         extObj.put("new_control_effectiveness", "");

         // Add Collaboration Entry
         String collId = generateUUID();
         pst.setString(1, collId);
         pst.setString(2, controlId);
         pst.setString(3, practiceName);
         pst.setString(4, "CAPABILITY");
         pst.setString(5, practiceName);
         pst.setString(6, message.toString());
         pst.setString(7, extObj.toString());
         pst.setString(8, "support@smarterd.com");
         pst.setString(9, "support@smarterd.com");
         pst.setString(10, companyCode);
         pst.addBatch();

         OrderedJSONObject ctx = new OrderedJSONObject();
         ctx.put("LINKED", "N");
         ctx.put("COMPONENT TYPE", "CAPABILITY");
         pst3.setString(1, collId);
         pst3.setString(2, controlId);
         pst3.setString(3, "RISK");
         pst3.setString(4, ctx.toString());
         pst3.setString(5, "support@smarterd.com");
         pst3.setString(6, "support@smarterd.com");
         pst3.setString(7, companyCode);
         pst3.addBatch();
         ctx.put("COMPONENT TYPE", "RISK");
         pst3.setString(1, controlId);
         pst3.setString(2, collId);
         pst3.setString(3, "CAPABILITY");
         pst3.setString(4, ctx.toString());
         pst3.setString(5, "support@smarterd.com");
         pst3.setString(6, "support@smarterd.com");
         pst3.setString(7, companyCode);
         pst3.addBatch();
      }

      pst.executeBatch();
      pst.close();
      pst3.executeBatch();
      pst3.close();
      conn.commit();
   }
   
   private synchronized String generateUUID() throws Exception {
      UUID uuid = UUID.randomUUID();
      return(uuid.toString());
   }

   private void mysqlConnect(String dbname) throws Exception {
      //String hostname = "35.227.181.195";
      String hostname = "35.197.106.183";
      String userName = "root", password = "smarterD2018!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }
   
   public static void main(String args[]) throws Exception {
      String reportName = "POAM Template CMMC Level 3.xlsx";
      String companyCode = args[0];
      
      MADSPOAMGenerator gen = new MADSPOAMGenerator(companyCode);
      //gen.generateReport(reportName);

      String assessmentId = "58ba3d96-ff72-11eb-8ed2-42010a8a005f";
      gen.createRisk(assessmentId);
   }
}