import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.FileWriter;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.commons.csv.CSVFormat;    // SmarteSchemaLoader uses this - Moving forward approach
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.wink.json4j.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.UUID;
import java.util.Iterator;
import java.time.Duration;
import java.time.Instant;
import org.apache.log4j.Logger;

public abstract class SmarteLoader {
   protected Connection conn = null;
   protected String companyCode = null;
   protected static Logger log = Logger.getLogger(SmarteLoader.class.getName());

   public SmarteLoader() {
   }

   public void initialize(String companyCode) throws Exception {
      this.companyCode = companyCode;

      // Initialize Connection
      initializeConnection(companyCode);
   }

   public void cleanup() {
      try {
         conn.commit();
         conn.close();
      } catch(Exception ex) {
         ex.printStackTrace();
      }
   }

   protected synchronized String generateUUID() throws Exception {
      UUID uuid = UUID.randomUUID();
      return(uuid.toString());
   }

   protected void initializeConnection(String dbname) throws Exception {
      //String hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
      String hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
      String userName = "peapi", password = "smartEuser2022!";
      //String userName = "smarterd", password = "uXrPwz64Dg21DgYTMF";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&characterEncoding=utf8", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }

   // Format UK Format to US Format
   protected String formatUKDateFormat(String date) throws Exception {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		DateTimeFormatter df1 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		DateTimeFormatter df3 = DateTimeFormatter.ofPattern("dd/MM/yy");
		DateTimeFormatter df4 = DateTimeFormatter.ofPattern("dd/M/yy");
		DateTimeFormatter df5 = DateTimeFormatter.ofPattern("dd/M/yy");
		DateTimeFormatter df6 = DateTimeFormatter.ofPattern("d/MM/yy");
		DateTimeFormatter df7 = DateTimeFormatter.ofPattern("d/M/yy");

		if(date.indexOf("-") != -1) {
            try {
                LocalDate dt = LocalDate.parse(date, df1);
                date = df.format(dt);
            } catch(Exception ex) {
			}
            try {
				LocalDate dt = LocalDate.parse(date, df2);
				date = df.format(dt);
			} catch(Exception ex) {
			}
		} else if(date.indexOf("/") != -1 && date.length() == 8) {
			LocalDate dt = LocalDate.parse(date, df3);
			date = df.format(dt);
		} else if(date.indexOf("/") != -1 && date.length() == 7) {
			try {
				LocalDate dt = LocalDate.parse(date, df4);
				date = df.format(dt);
			} catch(Exception ex) {
			}
            try {
				LocalDate dt = LocalDate.parse(date, df5);
				date = df.format(dt);
			} catch(Exception ex) {
			}
			try {
				LocalDate dt = LocalDate.parse(date, df6);
				date = df.format(dt);
			} catch(Exception ex) {
			}
		} else if(date.indexOf("/") != -1 && date.length() == 6) {
			try {
				LocalDate dt = LocalDate.parse(date, df7);
				date = df.format(dt);
			} catch(Exception ex) {

			}
		}

		return date;
	}

   protected abstract void load() throws Exception;
}
