import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.commons.csv.CSVFormat;    // SmarteSchemaLoader uses this - Moving forward approach
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.wink.json4j.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.UUID;
import java.util.Iterator;
import java.time.Duration;
import java.time.Instant;

/* Sample Queries
Request detections last tested before July 15, 2018:
curl -H "X-Requested-With:curl demo2" -u "user:password" -d "action=list&network_ids=1000&status=Active,Fixed,New,Re-Opened&ips=10.10.10.10&detection_last_tested_before=2018-07-15" "https://qualysapi.qualys.com/api/2.0/fo/asset/host/vm/detection/"

Request detections last tested in the past 185 days:
curl -H "X-Requested-With:curl demo2" -u "user:password" -d "action=list&network_ids=1000&status=Active,Fixed,New,Re-Opened&ips=10.10.10.7&detection_last_tested_since_days=185" "https://qualysapi.qualys.com/api/2.0/fo/asset/host/vm/detection/"

Request detections last tested on or after August 1, 2018:
curl -H "X-Requested-With:curl demo2" -u "user:password" -d "action=list&network_ids=1000&status=Active,Fixed,New,Re-Opened&ips=10.10.10.7&detection_last_tested_since=2018-08-01" "https://qualysapi.qualys.com/api/2.0/fo/asset/host/vm/detection/"

Request detections last tested between August 1st and October 1st:
curl -H "X-Requested-With:curl demo2" -u "user:password" -d "action=list&network_ids=1000&status=Active,Fixed,New,Re-Opened&ips=10.10.10.7&detection_last_tested_since=2018-08-01&detection_last_tested_before=2018-10-01" "https://qualysapi.qualys.com/api/2.0/fo/asset/host/vm/detection/

Request detections last tested between 15 days ago through 30 days ago:
curl -H "X-Requested-With:curl demo2" -u "user:password" -d "action=list&network_ids=1000&status=Active,Fixed,New,Re-Opened&ips=10.10.10.7&detection_last_tested_since_days=30&detection_last_tested_before_days=15" "https://qualysapi.qualys.com/api/2.0/fo/asset/host/vm/detection/
*/

/* Body Format
0:Host ID
1:IP Address
2:Tracking Method
3:Network ID
4:Operating System
5:DNS Name
6:Netbios Name
7:QG HostID
8:Last Scan Datetime
9:OS CPE
10:Last VM Scanned Date
11:Last VM Scanned Duration
12:Last VM Auth Scanned Date
13:Last VM Auth Scanned Duration
14:QID
15:Type
16:Port
17:Protocol
18:FQDN
19:SSL
20:Instance
21:Status
22:Severity
23:First Found Datetime
24:Last Found Datetime
25:Last Test Datetime
26:Last Update Datetime
27:Last Fixed Datetime
28:Results
29:Ignored
30:Disabled
31:Times Found
32:Service
33:Last Processed Datetime
*/


public class SmarteQualys extends SmarteLoader {
  
   public SmarteQualys() {
   }

   public void load() throws Exception {
      process("");
   }

   private void process(String url) throws Exception {
      Instant startTime = Instant.now();
      String id="madse3vs", pw="RahuPri95!";
      //String filter = "action=list&use_tags=1&tag_set_by=name&tag_set_include=ROUSH&show_tags=1&status=Active,Fixed,New,Re-Opened&detection_last_tested_since_days=1&output_format=CSV&truncation_limit=2&suppress_duplicated_data_from_csv=1";
      String filter = "action=list&use_tags=1&tag_set_by=id&tag_include_selector=any&tag_set_include=35329024,36896371,38088832,36750996,38088831,36750997,36750995,38088830,35084921,34599135,37443593,37443592&show_tags=1&status=Active,Fixed,New,Re-Opened&detection_last_tested_since_days=2&output_format=CSV&truncation_limit=1&suppress_duplicated_data_from_csv=1";
      //String filter = "action=list&use_tags=1&tag_set_by=name&tag_set_include=ROUSH&show_tags=1&status=Active,Fixed,New,Re-Opened&detection_last_tested_since_days=1&output_format=json&truncation_limit=2";
      if(url.length() == 0) {
         url = "https://qualysguard.qg3.apps.qualys.com/api/2.0/fo/asset/host/vm/detection/?"+filter;
      }
      String sql = "select cve_list,title,subcategory,category,reference,cvebase,cve3base from SE.enterprise_qualys_cve_mapping where qid = ?";
   
		// Add Options
      CUrl curl = new CUrl();
		curl = curl.opt("-X", "GET");
		curl = curl.opt("-H", "X-Requested-With:SmarterD Qualys Adaptor");
		curl = curl.opt("-u", "madse3vs:RahuPri95!");
      curl = curl.opt("-G", url);
		String res = curl.exec(null);
      if(res.length() > 0 && res.indexOf("SIMPLE_RETURN") == -1) {
         try {
            int rowCount = 0;
            boolean body = false, footer = false, row = false;;
            String nextUrl = "";
            String hostId = "",ipAddress="",os="",hostName="", qid="", type="", port="", protocol="", status="", severity="", service="", firstIngestedOn="", lastIngestedOn="",vrrUpdatedOn="",resolvedOn="";
            CSVParser dataReader = CSVParser.parse(res, CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            for(CSVRecord dataRow : dataReader) {
               for(int i=0; i<dataRow.size(); i++) {
                  String str = dataRow.get(i).trim();
                  if(str.equals("'-")) {
                     str = "";
                  }
                  if(i == 0) {
                     if(str.indexOf("HEADER") != -1) {
                        break;
                     } else if(str.indexOf("BEGIN_RESPONSE_BODY_CSV") != -1) {
                        body = true;
                        break;
                     } else if(str.indexOf("END_RESPONSE_BODY_CSV") != -1) {
                        body = false;
                        break;
                     } else if(str.indexOf("BEGIN_RESPONSE_FOOTER_CSV") != -1) {
                        footer = true;
                        break;
                     } else if(str.indexOf("END_RESPONSE_FOOTER_CSV") != -1) {
                        footer = false;
                        break;
                     } else if(body && !str.equalsIgnoreCase("Host ID")) {
                        row = true;
                        if(str.length() > 0 ) {
                           hostId = str;
                           hostName="";
                           ipAddress="";
                           os="";
                        }
                     }
                  } else if(i == 1) {
                     if(row && str.length() > 0) {
                        ipAddress = str;
                     }
                  } else if(i == 2) {
                     if(footer && str.indexOf("Next URL") == -1) {
                        nextUrl = str;
                     }
                  } else if(i == 3) {
                  } else if(i == 4) {
                     if(row && str.length() > 0) {
                        os = str;
                     }
                  } else if(i == 5) {
                     if(row && str.length() > 0) {
                        hostName = str;
                     }
                  } else if(i == 14) {
                     if(row && str.length() == 0) {
                        break;
                     } else if(row && str.length() > 0) {
                        qid = str;
                     }
                  } else if(i == 15) {
                     if(row && str.length() > 0) {
                        type = str;
                     }
                  } else if(i == 16) {
                     if(row && str.length() > 0) {
                        port = str;
                     }
                  } else if(i == 17) {
                     if(row && str.length() > 0) {
                        protocol = str;
                     }
                  } else if(i == 18) {
                  } else if(i == 19) {
                  } else if(i == 20) {
                  } else if(i == 21) {
                     if(row && str.length() > 0) {
                        status = str;
                     }
                  } else if(i == 22) {
                     if(row && str.length() > 0) {
                        severity = str;
                     }
                  } else if(i == 23) {
                     if(row && str.length() > 0) {
                        firstIngestedOn = str;
                     }
                  } else if(i == 24) {
                     if(row && str.length() > 0) {
                        lastIngestedOn = str;
                     }
                  } else if(i == 26) {
                     if(row && str.length() > 0) {
                        vrrUpdatedOn = str;
                     }
                  } else if(i == 27) {
                     if(row && str.length() > 0) {
                        resolvedOn = str;
                     }
                  } else if(i == 32) {
                     if(row && str.length() > 0) {
                        service = str;
                     }
                  }
               }

               if(row && qid.length() > 0 && !qid.equalsIgnoreCase("QID")) {
                  String title="", cves = null, category="", subcategory="",reference="",cvebase="",cve3base="";
                  PreparedStatement pst = conn.prepareStatement(sql);
                  pst.setString(1, qid);
                  ResultSet rs = pst.executeQuery();
                  while(rs.next()) {
                     title = rs.getString("title");
                     category = rs.getString("category");
                     subcategory = rs.getString("subcategory");
                     cves = rs.getString("cve_list");
                     reference = rs.getString("reference");
                     cvebase = rs.getString("cvebase");
                     cve3base = rs.getString("cve3base");
                  }
                  rs.close();
                  pst.close();

                  JSONArray cveList = new JSONArray();
                  if(cves != null && cves.length() > 0) {
                     String [] list = cves.split(",");
                     for(int i=0; i<list.length; i++) {
                        String str = list[i].trim();
                        cveList.add(str);
                     }
			         }

                  firstIngestedOn = formatDate(firstIngestedOn);
                  lastIngestedOn = formatDate(lastIngestedOn);
                  vrrUpdatedOn = formatDate(vrrUpdatedOn);
                  resolvedOn = formatDate(resolvedOn);

                  String patchable = "No";
                  if(subcategory != null && subcategory.indexOf("Patch Available") != -1) {
                     patchable = "Yes";
                  }

                  String severityLevel = "";
                  if(severity.equals("1")) {
                     severityLevel = "Info";
                  } else if(severity.equals("2")) {
                     severityLevel = "Low";
                  } else if(severity.equals("3")) {
                     severityLevel = "Medium";
                  } else if(severity.equals("4")) {
                     severityLevel = "High";
                  } else if(severity.equals("5")) {
                     severityLevel = "Critical";
                  }

                  // Create Unique ScanId
                  String scanId = qid+hostId+port;

                  // Create Scan Period
                  String scanPeriod = initializeScanPeriod(lastIngestedOn);

                  // Initialize Host Finding
                  JSONArray patchList = new JSONArray();
                  JSONArray serviceList = new JSONArray();
                  if(service.length() > 0) {
                     serviceList.add(service);
                  }
                  OrderedJSONObject hostObj = new OrderedJSONObject();
                  hostObj.put("hostId", hostId);
                  hostObj.put("hostName", hostName);
                  hostObj.put("ipAddress", ipAddress);

                  OrderedJSONObject hostFinding = new OrderedJSONObject();
                  hostFinding.put("title", title);
                  hostFinding.put("description", "");
                  hostFinding.put("source", "Qualys");
                  hostFinding.put("patches", patchList);
                  hostFinding.put("solution", reference);
                  hostFinding.put("riskRating", "");
                  hostFinding.put("severity", severity);
                  hostFinding.put("severityLevel", severityLevel);
                  hostFinding.put("output", "");
                  hostFinding.put("services", serviceList);
                  hostFinding.put("port", port);
                  hostFinding.put("lastFoundOn", lastIngestedOn);
                  hostFinding.put("findingType", type);
                  hostFinding.put("cvebase", cvebase);
                  hostFinding.put("cve3", cve3base);
                  hostFinding.put("host", hostObj);

                  System.out.println(qid+":"+title+":"+hostId+":"+port+":"+ipAddress+":"+hostName+":"+status+":"+severityLevel+":"+firstIngestedOn+":"+lastIngestedOn);

                  // Update Vulnerability
                  updateVulnerability(scanId, hostFinding, status, severityLevel, firstIngestedOn, lastIngestedOn, vrrUpdatedOn, resolvedOn, scanPeriod);
                 
                  qid="";
                  type="";
                  port="";
                  protocol="";
                  status="";
                  severity="";
                  service="";
                  firstIngestedOn="";
                  lastIngestedOn="";
                  vrrUpdatedOn="";
                  resolvedOn="";
                  row = false;
               } else if(footer && nextUrl.length() > 0) {
                  Instant endTime = Instant.now();
                  long duration = Duration.between(startTime, endTime).toMinutes();
		            System.out.println("Successfully loaded Qualys Data["+duration+" mts]");
                  System.out.println(nextUrl);
                  process(nextUrl);
               }
               /*
               if(rowCount == 6) {
                  break;
               }
               */
            }
         } catch(Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
         }
      } else {
         System.out.println(res.substring(res.indexOf("<SIMPLE_RETURN>"),res.indexOf("</SIMPLE_RETURN>")).replaceAll("<SIMPLE_RETURN>", ""));
      }
      conn.commit();
      conn.close();
   }

   private void updateVulnerability(String scanId, OrderedJSONObject hostFinding, String status, String priority, String firstIngestedOn, String lastIngestedOn, String vrrUpdatedOn, String resolvedOn, String scanPeriod) throws Exception {
      System.out.println("In updateVulnerability..."+scanId+":"+status+":"+priority);
      String sql = "select id, priority, vulnerability_id, vulnerability_name, extension from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.scan_id') = ? and json_extract(extension, '$.scan_period') = ?";
      String updatesql = "update enterprise_vulnerability_log set priority = ?, extension = ? where id = ?";
      String updatevulnrsql = "update enterprise_vulnerability set priority = ? where _id = ?";
      String currentPriority = null, vulnrLogId = null, vulnrId = null, vulnrName = "", ext = null;

      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, scanId);
      pst.setString(2, scanPeriod);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         vulnrLogId = rs.getString("id");
         currentPriority = rs.getString("priority");
         vulnrId = rs.getString("vulnerability_id");
         vulnrName = rs.getString("vulnerability_name");
         ext = rs.getString("extension");
      }
      rs.close();
      pst.close();

      if(vulnrLogId != null) {
         // Update Status and Workflow Details
         OrderedJSONObject extObj = new OrderedJSONObject(ext);
         OrderedJSONObject threatObj = (OrderedJSONObject)extObj.get("THREAT");

         String currentStatus = (String)threatObj.get("status");
         System.out.println(currentPriority+":"+priority+":"+currentStatus+":"+status);
         
         //fw.write(currentPriority+":"+priority+":"+currentStatus+":"+status);
         threatObj.put("First Discovered On", firstIngestedOn);
         threatObj.put("Last Discovered On", lastIngestedOn);
         threatObj.put("Resolved On", resolvedOn);
         threatObj.put("status", status);
         extObj.put("THREAT", threatObj);
         extObj.put("VRR Updated On", vrrUpdatedOn);

         pst = conn.prepareStatement(updatesql);
         pst.setString(1, priority);
         pst.setString(2, extObj.toString());
         pst.setString(3, vulnrLogId);
         //pst.executeUpdate();
         pst.close();

         // Update Vulnerability Priority
         pst = conn.prepareStatement(updatevulnrsql);
         pst.setString(1, priority);
         pst.setString(2, vulnrId);
         //pst.executeUpdate();
         pst.close();
      } else { // Exists in Vulnerability Tool but, not in SmarterD
         System.out.println("Not Found...adding...");
         vulnrName = (String)hostFinding.get("title");
         String desc = (String)hostFinding.get("description");
         String source = (String)hostFinding.get("source");
         JSONArray patchList = (JSONArray)hostFinding.get("patches");
         String mitigation = (String)hostFinding.get("solution");
         String riskRating = (String)hostFinding.get("riskRating");
         String notes = (String)hostFinding.get("output");
         String severity = (String)hostFinding.get("severity");
         String severityLevel = (String)hostFinding.get("severityLevel");
         String lastseen = (String)hostFinding.get("lastFoundOn");
         String findingType = (String)hostFinding.get("findingType");
         String cvebase = (String)hostFinding.get("cvebase");
         String cve3 = (String)hostFinding.get("cve3");
         String port = "";
         if(hostFinding.get("port") != null) {
            port = (String)hostFinding.get("port");
         }
         JSONArray serviceList = (JSONArray)hostFinding.get("services");
         String scannerOutput = (String)hostFinding.get("output");

         OrderedJSONObject hostObj = (OrderedJSONObject)hostFinding.get("host");
         String hostName = (String)hostObj.get("hostName");
         String hostId = (String)hostObj.get("hostId");
         String ipAddress = (String)hostObj.get("ipAddress");
        
         JSONArray portList = new JSONArray();
         if(port != null && port.length() > 0) {
            portList.add(port);
         }
         JSONArray cves = new JSONArray();

         if(notes == null) {
            notes = "";
         }

         JSONArray patchUrl = new JSONArray();
         Iterator iter = patchList.iterator();
         while(iter.hasNext()) {
            OrderedJSONObject patchObj = (OrderedJSONObject)iter.next();
            String url = (String)patchObj.get("url");
            patchUrl.add(url);
         }

         // Check if Vulnerability is there
         String checkvulnrsql = "select _id from enterprise_vulnerability where nameId = ?";
         pst = conn.prepareStatement(checkvulnrsql);
         pst.setString(1, vulnrName);
         rs = pst.executeQuery();
         while(rs.next()) {
            vulnrId = rs.getString("_id");
         }
         rs.close();
         pst.close();

         if(vulnrId == null) {   // Add Vulnerability
            vulnrId = generateUUID();
            JSONArray lastseenList = new JSONArray();
            if(lastseen != null && lastseen.length() > 0) {
               lastseenList.add(lastseen);
            }

            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("notes", notes);
            extObj.put("CVSS 2.0", cvebase);
            extObj.put("CVSS 3.0", cve3);
            extObj.put("severity", severity);
            extObj.put("last_seen", lastseenList);
            extObj.put("lifecycle", "Active");
            extObj.put("CVEs Associated", cves);
            extObj.put("Ticket Reference", new JSONArray());
            extObj.put("Patch Title", "");
            extObj.put("Possible Patches", patchList);
            extObj.put("mitigationStatus", "");
            extObj.put("enterprise_action", mitigation);
            extObj.put("Vulnerability Type", "");
            extObj.put("Vulnerability Risk Rating", riskRating);
            extObj.put("Scanner Reported Severity", severityLevel);
            extObj.put("First Ingested On", firstIngestedOn);
            extObj.put("Last Ingested On", lastIngestedOn);
            extObj.put("Finding Type", findingType);
            extObj.put("Evidence", "");
            extObj.put("Exploits", "");

            String insertsql = "insert into enterprise_vulnerability(_id, nameId, description,priority,source,extension,createdBy,updatedBy) values(?,?,?,?,?,?,?,?)";
            pst = conn.prepareStatement(insertsql);
            pst.setString(1, vulnrId);
            pst.setString(2, vulnrName);
            pst.setString(3, desc);
            pst.setString(4, priority);
            pst.setString(5, source);
            pst.setString(6, extObj.toString());
            pst.setString(7, "support@smarterd.com");
            pst.setString(8, "support@smarterd.com");
            //pst.executeUpdate();
            pst.close();
         }

         // Get Asset Detail
         String assetsql = "select id,asset_name,asset_location_name,lifecycle_name,owner,json_unquote(json_extract(extension,'$.\"Type\"')) type,json_unquote(json_extract(extension,'$.\"Sub-Type\"')) subtype,json_unquote(json_extract(extension,'$.\"OS\"')) os,json_unquote(json_extract(extension,'$.\"Environment\"')) env,json_unquote(json_extract(extension,'$.\"OEM Vendor\"')) make,json_unquote(json_extract(extension,'$.\"Model\"')) model,json_extract(extension,'$.\"Application Support Queue\"') app_queue,json_extract(extension,'$.\"Infra Support Queue\"') infra_queue,json_unquote(json_extract(extension,'$.Manager')) manager,json_unquote(json_extract(extension,'$.\"Technical Owner\"')) tech_owner from enterprise_asset where json_contains(json_unquote(json_extract(extension,'$.\"IP Address\"')) , '\"REPLACE_STRING\"') > 0 and lifecycle_name != 'Decommissioned'";
         OrderedJSONObject assetObj = new OrderedJSONObject();
         String assetId = null, assetName = null;
         assetsql = assetsql.replaceAll("REPLACE_STRING", ipAddress);
         PreparedStatement pst1 = conn.prepareStatement(assetsql);
         ResultSet rs1 = pst1.executeQuery();
         while(rs1.next()) {
            assetId = rs1.getString("id");
            assetName = rs1.getString("asset_name");
            String owner = rs1.getString("owner");
            String manager = rs1.getString("manager");
            String techOwner = rs1.getString("tech_owner");
            String loc = rs1.getString("asset_location_name");
            String lc = rs1.getString("lifecycle_name");
            String type = rs1.getString("type");
            String subtype = rs1.getString("subtype");
            String os = rs1.getString("os");
            String env = rs1.getString("env");
            String vendor = rs1.getString("make");
            String model = rs1.getString("model");
            String appQueue = rs1.getString("app_queue");
            String infraQueue = rs1.getString("infra_queue");

            JSONArray infraQueueList = null;
            if(infraQueue != null && infraQueue.length() > 0) {
               infraQueueList = new JSONArray(infraQueue);
            } else {
               infraQueueList = new JSONArray();
            }
            JSONArray appQueueList = null;
            if(appQueue != null && appQueue.length() > 0) {
               appQueueList = new JSONArray(appQueue);
            } else {
               appQueueList = new JSONArray();
            }

            if(owner == null) {
               owner = "";
            }
            if(manager == null) {
               manager = "";
            }
            JSONArray techOwnerList = null;
            if(techOwner != null && techOwner.length() > 0) {
               techOwnerList = new JSONArray(techOwner);
            } else {
               techOwnerList = new JSONArray();
            }

            assetObj.put("asset_location_name", loc);
            assetObj.put("lifecycle_name", lc);
            assetObj.put("Type", type);
            assetObj.put("Sub-Type", subtype);
            assetObj.put("OS", os);
            assetObj.put("Environment", env);
            assetObj.put("OEM Vendor", vendor);
            assetObj.put("Model", model);
            assetObj.put("Application Support Queue", appQueueList);
            assetObj.put("Infra Support Queue", infraQueueList);
            assetObj.put("Owner", owner);
            assetObj.put("Manager", manager);
            assetObj.put("Technical Owner", techOwner);
         }
         rs1.close();
         pst1.close();

         // Insert into Enterprise Vulnerability Asset
         if(assetId == null) {
            assetId = generateUUID();
            String assetinsertsql = "insert into enterprise_vulnerability_asset(id,rowId,asset_name,asset_category,extension,createdBy) values(?,uuid(),?,?,?,?)";
            OrderedJSONObject extObj = new OrderedJSONObject();
            extObj.put("sync", "N");
            extObj.put("reviewed", "N");
            extObj.put("scan_period", scanPeriod);
            extObj.put("hostname", hostName);
            pst = conn.prepareStatement(assetinsertsql);
            pst.setString(1, assetId);
            pst.setString(2, ipAddress);
            pst.setString(3, "IT ASSET");
            pst.setString(4, extObj.toString());
            pst.setString(5, "support@smarterd.com");
            //pst.executeUpdate();
            pst.close();
         }

         // Insert Host
         OrderedJSONObject threatCtx = new OrderedJSONObject();
         threatCtx.put("scan_id", scanId);
         threatCtx.put("status", status);
         threatCtx.put("Ticket Reference", "");
         threatCtx.put("notes", notes);
         threatCtx.put("assignee", new JSONArray());
         threatCtx.put("IP Address", ipAddress);
         threatCtx.put("Port", portList);
         threatCtx.put("Service", serviceList);
         threatCtx.put("Evidence", "");
         threatCtx.put("source", source);
         threatCtx.put("Host Id", hostId);
         threatCtx.put("lifecycle", "Active");
         threatCtx.put("Scanner Output", scannerOutput);
         threatCtx.put("group_name", "");
         threatCtx.put("Vulnerability Type", "");
         threatCtx.put("First Discovered On", firstIngestedOn);
         threatCtx.put("Last Discovered On", lastIngestedOn);
         threatCtx.put("Resolved On", resolvedOn);

         OrderedJSONObject extObj = new OrderedJSONObject();
         extObj.put("asset_status", "");
         extObj.put("scan_period", scanPeriod);
         extObj.put("scan_status", "Open");
         extObj.put("sync_id", "");
         extObj.put("scan_sync", "");
         extObj.put("sync_name", "");
         extObj.put("sync_status", "");
         extObj.put("THREAT", threatCtx);
         extObj.put("ASSET", assetObj);
         extObj.put("VRR Updated On", vrrUpdatedOn);
         DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
         LocalDate ldt = LocalDate.now();
         String dt = ldt.format(df);
         String insertsql = "insert into enterprise_vulnerability_log(id,vulnerability_id,vulnerability_name,asset_id,asset_name,ipaddress,hostname,priority,status,load_date,source,extension,created_timestamp) values(uuid(),?,?,?,?,?,?,?,?,?,?,?,current_timestamp)";	
         pst = conn.prepareStatement(insertsql);
         pst.setString(1, vulnrId);
         pst.setString(2, vulnrName);
         pst.setString(3, assetId);
         pst.setString(4, assetName);
         pst.setString(5, ipAddress);                     
         pst.setString(6, hostName);
         pst.setString(7, priority);
         pst.setString(8, "New");
         pst.setString(9, dt);
         pst.setString(10, source);
         pst.setString(11, extObj.toString());
         //pst.executeUpdate();
         pst.close();
         System.out.println("Successfully added Vulnerability!!!");
      }
   }
   
   private String formatDate(String d) throws Exception {
      DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");   // 10
      DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");     // ISO
      LocalDate dt = null;
      String formattedDate = "";
      if(d != null && d.length() > 0) {
         if(d.indexOf("T") != -1) {
            dt = LocalDate.parse(d, df1);
            formattedDate = df.format(dt);
         } else {
            formattedDate = d;
         }
      }

      return formattedDate;
   }

   private String initializeScanPeriod(String lastIngestedDate) throws Exception {
      String scanPeriod = "";
      LocalDate ld = null;
      DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      if(lastIngestedDate != null && lastIngestedDate.length() > 0) {
         ld = LocalDate.parse(lastIngestedDate, df);
      } else {
         ld = LocalDate.now();
      }
      String m = ld.getMonth().toString().substring(0,3);
      String year = Integer.toString(ld.getYear()).substring(2);
      scanPeriod = m.toUpperCase()+"-"+year;

      return scanPeriod;
   }

   public static void main(String[] args) {
      /*
      try {
         String companyCode = args[0];
         String env = args[1];
         SmarteLoader api = (SmarteQualys)new SmarteLoader(companyCode);
         api.process("");
      } catch (Exception e) {
         e.printStackTrace();
      }
      */
   }
}
