import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.text.SimpleDateFormat;
import java.io.File;
import java.sql.*;

import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.IBody;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.IRunBody;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import  org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtContentRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtPr;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import javax.xml.namespace.QName;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class MADSTechnicalAssessmentGenerator {
   private String companyCode = null;
   private XWPFTable templateTable = null;
   private XWPFParagraph templateParagraph = null;
   private Connection conn = null;
  
   public MADSTechnicalAssessmentGenerator(String companyCode) throws Exception {
      this.companyCode = companyCode;

      // Connect Mysql
      mysqlConnect(companyCode);
   }

   public void generateReport(String reportName) throws Exception {
      try (XWPFDocument document = new XWPFDocument(Files.newInputStream(Paths.get("./"+reportName+".docx")))) {
         XWPFTable legendTbl = null;
         List<XWPFTable> tables = document.getTables();
         for (int x=0; x<tables.size(); x++) {
            List<XWPFTableRow> tableRows = tables.get(x).getRows();
            int rowCount = tableRows.size();
            System.out.println(rowCount);
            if(rowCount == 9 && x==1) {
               legendTbl = tables.get(x);
               System.out.println("Legend Found!!!");
               break;
            }
         }

         if(legendTbl != null) {
            List<XWPFParagraph> paraList = document.getParagraphs();
            for(int i=0;i<paraList.size();i++){
               XWPFParagraph para = paraList.get(i);
               String text = para.getText();
               String id = "a9714816-142b-11ed-b8ae-42010a8a0006";

               if(text.equals("${mark_newTable}")) {
                  XmlCursor cursor = para.getCTP().newCursor();

                  String controlsql = "select id, child_capability_name from enterprise_business_capability where business_capability_level = 1 and business_capability_domain = 'CIS 8' order by child_capability_name";
                  String childsql = "select id, internal_id, child_capability_name, json_extract(business_capability_ext, '$.custom.\"Implementation Group\"') impl_group from enterprise_business_capability where parent_capability_id = ?";

                  PreparedStatement pst = conn.prepareStatement(controlsql);
                  ResultSet rs = pst.executeQuery();
                  while(rs.next()) {
                     String controlId = rs.getString("id");
                     String controlName = rs.getString("child_capability_name");
                  
                     XWPFParagraph newpara  = document.insertNewParagraph(cursor);
                     XWPFRun run = newpara.createRun();
                     run.setText(controlName, 0);
                     run.setFontSize(10);
                     run.setBold(true);
                     run.setColor("BD2E32");

                     PreparedStatement pst1 = conn.prepareStatement(childsql);
                     pst1.setString(1, controlId);
                     ResultSet rs1 = pst1.executeQuery();
                     while(rs1.next()) {
                        String sgId = rs1.getString("id");
                        String sgName = rs1.getString("child_capability_name");
                        String internalId = rs1.getString("internal_id");

                        JSONArray details = getAssessmentDetail(id, internalId.trim());


                        // Add Table
                        cursor.toNextToken();
                        XWPFTable newTbl = document.insertNewTbl(cursor);
                        copyTable(legendTbl, newTbl);
                     }
                     rs1.close();
                     pst1.close();

                     cursor.toNextToken();
                     newpara  = document.insertNewParagraph(cursor);
                    
                     cursor.toNextToken();
                     XWPFTable newTbl = document.insertNewTbl(cursor);
                     copyTable(legendTbl, newTbl);

                     document.removeBodyElement(document.getPosOfParagraph(para));

                     break;
                  }
               }
            }

            //document.createParagraph();
            //document.createTable();// Create a empty table in the document
            //document.setTable(2, newTbl);  

            //CTTbl inserted = document.getDocument().getBody().insertNewTbl(2);    
            //XWPFTable newTbl = new XWPFTable(inserted, document);
            //XWPFParagraph paragraph = document.createParagraph();
            //XmlCursor cursor = paragraph.getCTP().newCursor();
            //XWPFTable newTbl = document.insertNewTbl(cursor);
            //copyTable(legend, newTbl);

            // Populate tables
            //String level1sql = "select id, child_capability_name from enterprise_business_capability where business_capability_level = 1 and business_capability_domain = 'CIS 8' order by child_capability_name";
            //updateTable(tableRows);

            // save the docs
            try (FileOutputStream out = new FileOutputStream("./vs_"+reportName+".docx")) {
               document.write(out);
               document.close();
               document.close();
            }
         }
      }
   }

   private void copyTable(XWPFTable source, XWPFTable target) {
      target.getCTTbl().setTblPr(source.getCTTbl().getTblPr());
      target.getCTTbl().setTblGrid(source.getCTTbl().getTblGrid());
      for (int r = 0; r<source.getRows().size(); r++) {
         XWPFTableRow targetRow = target.createRow();
         XWPFTableRow row = source.getRows().get(r);
         targetRow.getCtRow().setTrPr(row.getCtRow().getTrPr());
         for (int c=0; c<row.getTableCells().size(); c++) {
            //newly created row has 1 cell
            XWPFTableCell targetCell = c==0 ? targetRow.getTableCells().get(0) : targetRow.createCell();
            XWPFTableCell cell = row.getTableCells().get(c);
            targetCell.getCTTc().setTcPr(cell.getCTTc().getTcPr());
            XmlCursor cursor = targetCell.getParagraphArray(0).getCTP().newCursor();
            for (int p = 0; p < cell.getBodyElements().size(); p++) {
               IBodyElement elem = cell.getBodyElements().get(p);
               if (elem instanceof XWPFParagraph) {
                  XWPFParagraph targetPar = targetCell.insertNewParagraph(cursor);
                  cursor.toNextToken();
                  XWPFParagraph par = (XWPFParagraph) elem;
                  copyParagraph(par, targetPar);
               } else if (elem instanceof XWPFTable) {
                  XWPFTable targetTable = targetCell.insertNewTbl(cursor);
                  XWPFTable table = (XWPFTable) elem;
                  copyTable(table, targetTable);
                  cursor.toNextToken();
               }
            }
            //newly created cell has one default paragraph we need to remove
            targetCell.removeParagraph(targetCell.getParagraphs().size()-1);
        }
      }
      //newly created table has one row by default. we need to remove the default row.
      target.removeRow(0);
   }

   private void copyParagraph(XWPFParagraph source, XWPFParagraph target) {
      target.getCTP().setPPr(source.getCTP().getPPr());
      for (int i=0; i<source.getRuns().size(); i++ ) {
         XWPFRun run = source.getRuns().get(i);
         XWPFRun targetRun = target.createRun();
         //copy formatting
         targetRun.getCTR().setRPr(run.getCTR().getRPr());
         //no images just copy text
         targetRun.setText(run.getText(0));
      }
   }

   private JSONArray getAssessmentDetail(String id, String internalId) throws Exception {
        JSONArray detailList = new JSONArray();
        String sql = "select a.id id,json_unquote(json_extract(extension, '$.\"internal_id\"')) internal_id,a.control_id control_id,a.parent_control_id,a.control_family,a.control_level control_level,json_unquote(json_extract(a.extension, '$.\"artifacts_referenced\"')) artifacts_referenced,json_unquote(json_extract(extension,'$.\"control_name\"')) child_capability_name,json_unquote(json_extract(extension,'$.\"parent_control_name\"')) parent_capability_name,control_family,json_unquote(json_extract(extension, '$.\"nist_mapping\"')) nist_mapping,a.gaps gaps,a.control_in_place control_in_place,a.status status,a.severity severity,a.effort effort,a.artifacts artifacts,a.notes notes,a.recommendation recommendation,a.extension extension,json_unquote(json_extract(extension, '$.\"services_offered\"')) services_offered,a.updated_timestamp updated_timestamp,a.created_by created_by,a.updated_by updated_by from enterprise_assessment_detail a where a.assessment_id = ? and json_unquote(json_extract(extension, '$.\"internal_id\"')) like ? and a.status like '%' and a.control_level = 2";
        String poamsql = "select a.id id,json_unquote(json_extract(extension, '$.\"internal_id\"')) internal_id,a.control_id control_id,a.parent_control_id,a.control_family,a.control_level control_level,json_unquote(json_extract(a.extension, '$.\"artifacts_referenced\"')) artifacts_referenced,json_unquote(json_extract(extension,'$.\"control_name\"')) child_capability_name,json_unquote(json_extract(extension,'$.\"parent_control_name\"')) parent_capability_name,control_family,json_unquote(json_extract(extension, '$.\"nist_mapping\"')) nist_mapping,a.gaps gaps,a.control_in_place control_in_place,a.status status,a.severity severity,a.effort effort,a.artifacts artifacts,a.notes notes,a.recommendation recommendation,a.extension extension,json_unquote(json_extract(extension, '$.\"services_offered\"')) services_offered,a.updated_timestamp updated_timestamp,a.created_by created_by,a.updated_by updated_by from enterprise_assessment_detail a where a.assessment_id = ? and json_unquote(json_extract(extension, '$.\"internal_id\"')) like ? and a.status in ('Not Implemented', 'Partially Implemented', 'Fully Implemented', 'N/A') and a.control_level = 2";
        PreparedStatement pst = null;
        if(internalId.equals("%")) {  // For POA&M
            pst = conn.prepareStatement(poamsql);
        } else {    // For Technical Report
            pst = conn.prepareStatement(sql);
        }
        pst.setString(1, id);
        pst.setString(2, internalId);
        System.out.println(pst.toString());
        ResultSet rs = pst.executeQuery();
        while(rs.next()) {
            internalId = rs.getString("internal_id");
            String practiceName = rs.getString("child_capability_name");
            String status = rs.getString("status");
            String severity = rs.getString("severity");
            String effort = rs.getString("effort");
            String controlInplace = rs.getString("control_in_place");
            String gaps = rs.getString("gaps");
            String recommendation = rs.getString("recommendation");
            String artifacts = rs.getString("artifacts");
            String artifactsRef = rs.getString("artifacts_referenced");
            String nistMapping = rs.getString("nist_mapping");
            String services = rs.getString("services_offered");

            if(status == null) {
                status = "";
            }
            if(severity == null) {
                severity = "";
            }
            if(effort == null) {
                effort = "";
            }
            if(controlInplace == null) {
                controlInplace = "";
            }
            if(gaps == null) {
                gaps = "";
            }
            if(recommendation == null) {
                recommendation = "";
            }
            if(artifacts == null) {
                artifacts = "";
            }
            if(artifactsRef == null) {
                artifactsRef = "";
            }
            JSONArray servicesList = null;
            if(services != null && services.length() > 0) {
                servicesList = new JSONArray(services);
            } else {
                servicesList = new JSONArray();
            }
            OrderedJSONObject obj = new OrderedJSONObject();
            obj.put("internal_id", internalId);
            obj.put("practice_name", practiceName);
            obj.put("status", status);
            obj.put("severity", severity);
            obj.put("effort", effort);
            obj.put("control_in_place", controlInplace);
            obj.put("gaps", gaps);
            obj.put("recommendation", recommendation);
            obj.put("artifacts_ref", artifactsRef);
            obj.put("nist_mapping", nistMapping);
            obj.put("services_offered", servicesList);
            detailList.add(obj);
        }
        rs.close();
        pst.close();

        return detailList;
    }


   public void generateSSPReport(String reportName) throws Exception {
      try (XWPFDocument document = new XWPFDocument(Files.newInputStream(Paths.get("./"+reportName+".docx")))) {

         List paragraphList = document.getParagraphs();

         Iterator iter = paragraphList.iterator();
         while(iter.hasNext()) {
            XWPFParagraph paragraph = (XWPFParagraph)iter.next();
            String text = paragraph.getText();
            
            if(text.indexOf(":") != -1) {
               String[] str = text.split("\\.");
               if(str.length == 3) {
                  text = text.replaceAll(":", "");
                  //System.out.println(text);
               }
            } else if(text.indexOf("Implemented") != -1) {
               int count = 0;
               for (CTSdtRun sdtRun : paragraph.getCTP().getSdtList()) {
                   System.out.println(text);
                  if (W14Checkbox.isW14Checkbox(sdtRun)) {
                     if(count == 0) {
                        W14Checkbox w14Checkbox = new W14Checkbox(sdtRun);
                        //System.out.println(w14Checkbox.getChecked());
                        if (w14Checkbox.getChecked()) {
                           w14Checkbox.setChecked(false); 
                        } else {
                           w14Checkbox.setChecked(true);
                        }
                     }
                     //System.out.println(w14Checkbox.getChecked());
                     count++;
                  }
               }
            }
         }	

         OrderedJSONObject obj = new OrderedJSONObject();
         obj.put("COMPANY_NAME", "VIJAY");
         replaceTextInDocument(document, obj);

         replaceTextInTable(document);
            
         // save the docs
         /*
         try (FileOutputStream out = new FileOutputStream("./vs_"+reportName+".docx")) {
            document.write(out);
            document.close();
            document.close();
         }
         */
      }
   }

   static class W14Checkbox {
      CTSdtRun sdtRun = null;
      CTSdtContentRun sdtContentRun = null;
      XmlObject w14CheckboxChecked = null;
    
      W14Checkbox(CTSdtRun sdtRun) {
         this.sdtRun = sdtRun;
         this.sdtContentRun = sdtRun.getSdtContent();
         String declareNameSpaces = "declare namespace w14='http://schemas.microsoft.com/office/word/2010/wordml'";
         XmlObject[] selectedObjects = sdtRun.getSdtPr().selectPath(declareNameSpaces + ".//w14:checkbox/w14:checked");
         if (selectedObjects.length > 0) {  
            this.w14CheckboxChecked  = selectedObjects[0];
         }
      }

      CTSdtContentRun getContent() {
         return this.sdtContentRun;
      }

      XmlObject getW14CheckboxChecked() {
         return this.w14CheckboxChecked;
      }
      boolean getChecked() {
         XmlCursor cursor = this.w14CheckboxChecked.newCursor();
         String val = cursor.getAttributeText(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "val", "w14"));
         return "1".equals(val) || "true".equals(val);
      }
      void setChecked(boolean checked) {
         XmlCursor cursor = this.w14CheckboxChecked.newCursor();
         String val = (checked)?"1":"0";
         cursor.setAttributeText(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "val", "w14"), val);
         cursor.dispose();
         CTText t = this.sdtContentRun.getRArray(0).getTArray(0);
         String content = (checked)?"\u2612":"\u2610";
         t.setStringValue(content);
      }
    
      static boolean isW14Checkbox(CTSdtRun sdtRun) {
         CTSdtPr sdtPr = sdtRun.getSdtPr();
         String declareNameSpaces = "declare namespace w14='http://schemas.microsoft.com/office/word/2010/wordml'";
         XmlObject[] selectedObjects = sdtPr.selectPath(declareNameSpaces + ".//w14:checkbox");
         if (selectedObjects.length > 0) return true;  
         return false;
      }
   }

   protected void replaceTextinDocumentTextBox(XWPFDocument doc, String findText, String replaceText) throws Exception {
      for (XWPFParagraph paragraph : doc.getParagraphs()) {
          XmlCursor cursor = paragraph.getCTP().newCursor();
          cursor.selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r");
       
          List<XmlObject> ctrsintxtbx = new ArrayList<XmlObject>();
       
          while(cursor.hasNextSelection()) {
              cursor.toNextSelection();
              XmlObject obj = cursor.getObject();
              ctrsintxtbx.add(obj);
          }
          for (XmlObject obj : ctrsintxtbx) {
              CTR ctr = CTR.Factory.parse(obj.xmlText());
              XWPFRun bufferrun = new XWPFRun(ctr, (IRunBody)paragraph);
              String text = bufferrun.getText(0);
              System.out.println(text);
              if (text != null && text.contains(findText)) {
                  text = text.replace(findText, replaceText);
                  bufferrun.setText(text, 0);
              }
              obj.set(bufferrun.getCTR());
          }
      }
   }

   private void replaceTextInDocument(XWPFDocument doc, OrderedJSONObject replaceTextObj) throws Exception {
      for(XWPFParagraph p : doc.getParagraphs()) {
         String pText = p.getText();
         Iterator iter = replaceTextObj.keySet().iterator();
         while(iter.hasNext()) {
            String findText = (String)iter.next();
            String replaceText = (String)replaceTextObj.get(findText);
            if(pText.indexOf(findText) != -1) {
               List<XWPFRun> runs = p.getRuns();
               if(runs.size() > 0) {
                  for(XWPFRun r : runs) {
                     String text = r.getText(0);
                     if(text != null && text.contains(findText)) {
                        System.out.println(text);
                        text = text.replace(findText, replaceText);
                        r.setText(text, 0);
                     }
                  }
               } else {
                  System.out.println(findText+":"+pText);
               }
            }
         }
      }
   }

   private void replaceTextInTable(XWPFDocument doc) throws Exception {
      List<XWPFTable> tables = doc.getTables();
      for (int x=0; x<tables.size(); x++) {
         List<XWPFTableRow> tableRows = tables.get(x).getRows();
         for (int r=0; r<tableRows.size();r++) {
            XWPFTableRow tableRow = tableRows.get(r);
            List<XWPFTableCell> tableCells = tableRow.getTableCells();
            //System.out.println("Row Cells:"+r+":"+tableCells.size());
            for (int c=0; c<tableCells.size();c++) {
               XWPFTableCell tableCell = tableCells.get(c);
               if(r == 2) {
                  if(c == 1) {
                     XWPFParagraph p = tableCell.getParagraphs().get(0);
                     List<XWPFRun> runList = p.getRuns();
                     Iterator iter = runList.iterator();
                     while(iter.hasNext()) {
                        XWPFRun run = (XWPFRun)iter.next();
                        String text = run.getText(0);
                        text = text.replace("COMPANY_NAME", "VIJAY");
                        run.setText(text, 0);
                     }
                  }
               }
            }
         }
      }
   }

   private void replace(XWPFParagraph paragraph, String searchValue, String replacement) {
      if (hasReplaceableItem(paragraph.getText(), searchValue)) {
         String replacedText = paragraph.getText().replaceAll(searchValue, replacement);

         removeAllRuns(paragraph);

         insertReplacementRuns(paragraph, replacedText);
      }
  }
  
  private void insertReplacementRuns(XWPFParagraph paragraph, String replacedText) {
      String[] replacementTextSplitOnCarriageReturn = replacedText.split("\n");
  
      for (int j = 0; j < replacementTextSplitOnCarriageReturn.length; j++) {
          String part = replacementTextSplitOnCarriageReturn[j];
  
          XWPFRun newRun = paragraph.insertNewRun(j);
          newRun.setText(part);
  
          if (j+1 < replacementTextSplitOnCarriageReturn.length) {
              newRun.addCarriageReturn();
          }
      }       
  }
  
  private void removeAllRuns(XWPFParagraph paragraph) {
      int size = paragraph.getRuns().size();
      for (int i = 0; i < size; i++) {
          paragraph.removeRun(0);
      }
  }
  
   private boolean hasReplaceableItem(String runText, String searchValue) {
      return runText.contains(searchValue);
   }

   private void updateTable(String internalId, String implGroup, String sf, List<XWPFTableRow> tableRows) throws Exception {

      // Get Data from DB
      OrderedJSONObject res = loadData(internalId);
      System.out.println(res.toString());

      for (int r=0; r<tableRows.size();r++) {
         XWPFTableRow tableRow = tableRows.get(r);
         List<XWPFTableCell> tableCells = tableRow.getTableCells();
         System.out.println("Row Cells:"+r+":"+tableCells.size());
         for (int c=0; c<tableCells.size();c++) {
            XWPFTableCell tableCell = tableCells.get(c);
            if(r == 0) {
               if(c == 0) {
                  String practiceName = tableCell.getText();
                  System.out.println("Practice Name:"+practiceName);
               }
            } else if(r == 1) {
               if(c == 1) { // Status
                  String status = (String)res.get("status");
                  updateTableCell(status, tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               } else if(c == 2) { // Severity
                  String severity = (String)res.get("severity");
                  updateTableCell(severity, tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               } else if(c == 3) { // Effort
                  String effort = (String)res.get("effort");
                  updateTableCell(effort, tableCell);
                  tableCell.getParagraphs().get(0).setAlignment(ParagraphAlignment.CENTER);
               }
            } else if(r == 2) {
            } else if(r == 3) {
               if(c > 0) {
                  updateTableCell(implGroup, tableCell);
               }
            } else if(r == 4) {
               if(c > 0) {
                  updateTableCell(sf, tableCell);
               }
            } else if(r == 5) {
               if(c > 0) {
                  String controlInPlace = (String)res.get("control_in_place");
                  updateTableCell(controlInPlace, tableCell);
               }
            } else if(r == 6) {
               if(c > 0) {
                  String gaps = (String)res.get("gaps");
                  updateTableCell(gaps, tableCell);
               }
            } else if(r == 7) {
               if(c > 0) {
                  String recommendation = (String)res.get("recommendation");
                  updateTableCell(recommendation, tableCell);
               }
            } else if(r == 8) {
               if(c > 0) {
                  String artifacts = (String)res.get("artifacts");
                  updateTableCell(artifacts, tableCell);
               }
            }
         }
      }
   }

   private void updateTableCell(String content, XWPFTableCell tableCell) throws Exception {
      XWPFParagraph paragraph = tableCell.getParagraphs().get(0);

      // Clear Content
      List<XWPFRun> runList = paragraph.getRuns();
      Iterator iter = runList.iterator();
      while(iter.hasNext()) {
         XWPFRun run = (XWPFRun)iter.next();
         String runText = run.getText(run.getTextPosition());
         String replaced = runText.replaceFirst(".*", "");
         run.setText(replaced, 0);
      }

      // Update Content
      if(runList.size() > 0) {
         XWPFRun run = paragraph.getRuns().get(0);
         run.setText(content, 0);
      } else {
         String str = tableCell.getText();
         String replaced = str.replaceFirst(".*", content);
         tableCell.setText(replaced);
      }
   }

   private void updateParagraph(String content, XWPFParagraph paragraph) throws Exception {
      // Clear Content
      List<XWPFRun> runList = paragraph.getRuns();
      Iterator iter = runList.iterator();
      while(iter.hasNext()) {
         XWPFRun run = (XWPFRun)iter.next();
         String runText = run.getText(run.getTextPosition());
         String replaced = runText.replaceFirst(".*", "");
         run.setText(replaced, 0);
      }

      // Update Content
      if(runList.size() > 0) {
         XWPFRun run = paragraph.getRuns().get(0);
         run.setText(content, 0);
      }
   }

   private static void removeParagraphs(XWPFTableCell tableCell) {
      int count = tableCell.getParagraphs().size();
      for(int i = 0; i < count; i++){
         tableCell.removeParagraph(i);
      }
   }

   public OrderedJSONObject loadData(String internalId) throws Exception {
      OrderedJSONObject res = new OrderedJSONObject();
      String sql = "select a.id id,internal_id,a.control_id control_id,a. parent_control_id,a. control_family,a.control_level control_level,child_capability_name,parent_capability_name,json_unquote(json_extract(business_capability_ext, '$.\"family\"')) control_family,a.gaps gaps,a.control_in_place control_in_place,a.status status,a.severity severity,a.effort effort,a.artifacts artifacts,a.notes notes,a.recommendation recommendation,a.extension extension,a.updated_timestamp updated_timestamp,a.created_by created_by,a.updated_by updated_by, count(c.control_id) count from enterprise_assessment_detail a left join enterprise_assessment_detail c on a.control_id = c.parent_control_id, enterprise_business_capability b where a.control_id=b.id and a.assessment_id = ? and b.internal_id = ? and a.control_level = 2";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, "db5b62dd-f0ca-11eb-9a0c-42010a8a0003");
      pst.setString(2, internalId);
      ResultSet rs = pst.executeQuery();
      while(rs.next()) {
         String status = rs.getString("status");
         String severity = rs.getString("severity");
         String effort = rs.getString("effort");
         String controlInplace = rs.getString("control_in_place");
         String gaps = rs.getString("gaps");
         String recommendation = rs.getString("recommendation");
         String artifacts = rs.getString("artifacts");
         if(status == null) {
            status = "";
         }
         if(severity == null) {
            severity = "";
         }
         if(effort == null) {
            effort = "";
         }
         if(controlInplace == null) {
            controlInplace = "";
         }
         if(gaps == null) {
            gaps = "";
         }
         if(recommendation == null) {
            recommendation = "";
         }
         if(artifacts == null) {
            artifacts = "";
         }
         res.put("status", status);
         res.put("severity", severity);
         res.put("effort", effort);
         res.put("control_in_place", controlInplace);
         res.put("gaps", gaps);
         res.put("recommendation", recommendation);
         res.put("artifacts", artifacts);
      }
      rs.close();
      pst.close();

      return res;
   }

   /*
   //Iterate through the list and check for table element type
   Iterator<IBodyElement> docElementsIterator = doc.getBodyElementsIterator();
   while (docElementsIterator.hasNext()) {
         IBodyElement docElement = docElementsIterator.next();
         if ("TABLE".equalsIgnoreCase(docElement.getElementType().name())) {
            //Get List of table and iterate it
            List<XWPFTable> xwpfTableList = docElement.getBody().getTables();
            for (XWPFTable xwpfTable : xwpfTableList) {
               int rowCount = xwpfTable.getNumberOfRows();
               if(rowCount == 7) {
                  for (int i = 0; i < xwpfTable.getRows().size(); i++) {
                     if(i > 2) {
                        for (int j = 0; j < xwpfTable.getRow(i).getTableCells().size(); j++) {
                           //System.out.println(xwpfTable.getRow(i).getCell(j).getText());
                           if(j > 0) {
                              XWPFTableCell tableCell = xwpfTable.getRow(i).getCell(j);
                              removeParagraphs(tableCell);
                              tableCell.setText("Vijay Sundhar");
                           }
                        }
                     }
                  }
                  break;
               }
            }
         }
   }
   */

   private void mysqlConnect(String dbname) throws Exception {
      String hostname = "35.227.181.195";
      String userName = "root", password = "smarterD2018!";
      Properties connectionProps = new Properties();
      connectionProps.put("user", userName);
      connectionProps.put("password", password);    	
      conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
      conn.setAutoCommit(false);
      System.out.println("Successfully connected to Mysql DB:"+dbname);
   }
   
   public static void main(String args[]) throws Exception {
      //String reportName = "Technical Report Template CMMC Level 3 Short";
      String reportName = "CIS18 Technical Report Template";
      String companyCode = args[0];
      
      MADSTechnicalAssessmentGenerator gen = new MADSTechnicalAssessmentGenerator(companyCode);
      gen.generateReport(reportName);
      //gen.generateSSPReport(reportName);
   }
}