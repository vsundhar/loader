import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.util.*;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.UUID;

public class SlackWebHook {
    private Connection conn = null;
    private String preprodurl = "https://hooks.slack.com/services/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";
    private String demosecurityAction = "https://hooks.slack.com/services/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";
    private String testsecurityaction = "https://hooks.slack.com/services/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";
    private String prodex = "https://hooks.slack.com/services/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";

    String controlFamily = "Malware", control="Enable DNS Query Logging", controlId = "abd08512-60ab-4dbf-b0e9-150df3dd853b";
    public SlackWebHook() throws Exception {
        mysqlConnect();
    }

    public void post() throws Exception {
		URL obj = new URL(prodex);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        //OrderedJSONObject text = new OrderedJSONObject();
        //text.put("text", "Hello World");
        /*
        String str = "Control Family:"+familyName;
        str += System.lineSeparator();
        str += "Control:"+controlName;
        str += System.lineSeparator();
        str += "Previous Risk:"+currentRisk+" "+currentRiskOutput.toString();
        str += System.lineSeparator();
        str += "Current Risk:"+risk+" "+riskOutput.toString();
        OrderedJSONObject text = new OrderedJSONObject();
        text.put("text", str);
        */
        String str = "*Risk Identified:*\n";
        str += "Control Family: *"+controlFamily+"*\n";
        //str += System.lineSeparator();
        str += "Control: *"+control+"*\n";
        //str += System.lineSeparator();
        str += "Previous Risk: *0* \n";
        str += "Current Risk: *16*";
        //String text = generateRoadmapMsgTemplate("0", "0", "01/01/2020", str);
        String text = generateDialogMsgTemplate(str, controlId);
        System.out.println(text);;
        
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = text.getBytes("utf-8");
            os.write(input, 0, input.length);           
        }

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Successfully posted message!!!");
    }

    public OrderedJSONObject createTextSection(String t) throws Exception {
        OrderedJSONObject section = new OrderedJSONObject();
        OrderedJSONObject text = new OrderedJSONObject();
        text.put("type", "mrkdwn");
        //text.put("emoji", new Boolean(true));
        text.put("text", t);

        section.put("type", "section");
        section.put("text", text);
        
        return section;
    }

    private OrderedJSONObject createMrkdwnSection(String title, Map values, String type) throws Exception {
        OrderedJSONObject section = new OrderedJSONObject();
        section.put("type", "section");

        OrderedJSONObject text = new OrderedJSONObject();
        text.put("type", "mrkdwn");
        text.put("text", title);
        
        OrderedJSONObject accessory = new OrderedJSONObject();   
        if(type.equals("text")) {
            accessory.put("type", "static_select");
        } else if(type.equals("date")) {
            accessory.put("type", "datepicker");
        }
        OrderedJSONObject placeholder = new OrderedJSONObject();
        placeholder.put("type", "plain_text");
        placeholder.put("emoji", new Boolean(true));
        if(type.equals("text")) {
            placeholder.put("text", "Select "+title);
        } else if(type.equals("date")) {
            placeholder.put("text", "Select a date");
        }
        accessory.put("placeholder", placeholder);
        if(type.equals("text")) {
            JSONArray options = new JSONArray();
            Iterator iter = values.keySet().iterator();
            while(iter.hasNext()) {
                String key = (String)iter.next();
                String value = (String)values.get(key);
                OrderedJSONObject mrkdwnOption = new OrderedJSONObject();
                OrderedJSONObject t = new OrderedJSONObject();
                t.put("type", "plain_text");
                t.put("emoji", new Boolean(true));
                t.put("text", key);
                mrkdwnOption.put("text", t);
                mrkdwnOption.put("value", value);
                options.add(mrkdwnOption);
            }
            accessory.put("options", options);
        }

        section.put("text", text);
        section.put("accessory", accessory);

        return section;
    }

    private OrderedJSONObject createDialogMrkdwnSection(String title, LinkedHashMap values, String currentValue) throws Exception {
        OrderedJSONObject mrkdwn = new OrderedJSONObject();
        mrkdwn.put("name", title);
        mrkdwn.put("type", "select");
        mrkdwn.put("label", title);
        mrkdwn.put("value", currentValue);
        JSONArray options = new JSONArray();
        Iterator iter = values.keySet().iterator();
        while(iter.hasNext()) {
            String key = (String)iter.next();
            String value = (String)values.get(key);
            OrderedJSONObject mrkdwnOption = new OrderedJSONObject();
            mrkdwnOption.put("label", key);
            mrkdwnOption.put("value", value);
            options.add(mrkdwnOption);
        }
        mrkdwn.put("options", options);

        return mrkdwn;
    }

    private OrderedJSONObject createActionSection(Map actionList) throws Exception { 
        OrderedJSONObject section = new OrderedJSONObject();
        section.put("type", "actions");

        JSONArray elementList = new JSONArray();
        Iterator iter = actionList.keySet().iterator();
        while(iter.hasNext()) {
            String name = (String)iter.next();
            String value = (String)actionList.get(name);
            OrderedJSONObject element = new OrderedJSONObject();
            element.put("type", "button");
            element.put("value", value+":DEMO") ;

            OrderedJSONObject text = new OrderedJSONObject();
            text.put("type", "plain_text");
            text.put("emoji", new Boolean(false));
            text.put("text", name);
            element.put("text", text);
            elementList.add(element);
        }
        section.put("elements", elementList);

        return section;
    }

    private OrderedJSONObject createDivider() throws Exception {
        OrderedJSONObject divider = new OrderedJSONObject();
        divider.put("type", "divider");

        return divider;
    }

    private String generateRoadmapMsgTemplate(String currentMaturity, String targetMaturity, String targetDate, String msg) throws Exception {
        JSONArray slackMsg = new JSONArray();
        Map maturityValues = new HashMap();
        maturityValues.put("Not Tracked", "0");
        maturityValues.put("Initial", "1");
        maturityValues.put("Repeatable", "2");
        maturityValues.put("Defined", "3");
        maturityValues.put("Managed", "4");
        maturityValues.put("Matured", "5");
        
        Map dateValues = new HashMap();

        Map actionList = new HashMap();
        actionList.put("Roadmap", "roadmap");
        actionList.put("Backlog", "backlog");

        String value = "";
        OrderedJSONObject textMsg = createTextSection(msg);
        OrderedJSONObject divider = createDivider();
        OrderedJSONObject currentMaturitySection = createMrkdwnSection("Current Maturity", maturityValues, "text");
        OrderedJSONObject targetMaturitySection = createMrkdwnSection("Target Maturity", maturityValues, "text");
        OrderedJSONObject targetDateSection = createMrkdwnSection("Target Date", dateValues, "date");
        OrderedJSONObject actionSection = createActionSection(actionList);
        
        slackMsg.add(textMsg);
        slackMsg.add(divider);
        //JSONArray mrkdwnArr = new JSONArray();
        slackMsg.add(currentMaturitySection);
        slackMsg.add(targetMaturitySection);
        slackMsg.add(targetDateSection);
        //OrderedJSONObject optionGroup = new OrderedJSONObject();
        //optionGroup.put("option_groups", mrkdwnArr);
        //slackMsg.add(optionGroup);
        slackMsg.add(divider);
        slackMsg.add(actionSection);

        OrderedJSONObject blocks = new OrderedJSONObject();
        blocks.put("blocks", slackMsg);

        return blocks.toString();
    }

    private String generateTextMsgTemplate(String msg) throws Exception {
        OrderedJSONObject text = new OrderedJSONObject();
        text.put("type", "plain_text");
        text.put("emoji", new Boolean(true));
        text.put("text", msg);

        return text.toString();
    }

    private String generateDialogMsgTemplate(String msg, String controlId) throws Exception {
        String token = "xoxp-748065782663-748065783031-752078630149-4f279f589bc66a1c08284512be6c68ba";
        JSONArray slackMsg = new JSONArray();
        UUID uuid = UUID.randomUUID();
        Map actionList = new HashMap();
        actionList.put("Roadmap", uuid.toString());
        actionList.put("Backlog", controlId);
        
        OrderedJSONObject textMsg = createTextSection(msg);
        OrderedJSONObject divider = createDivider();
        OrderedJSONObject actionSection = createActionSection(actionList);
        
        slackMsg.add(textMsg);
        slackMsg.add(actionSection);
        
        OrderedJSONObject blocks = new OrderedJSONObject();
        blocks.put("blocks", slackMsg);

        initializeDialog(uuid.toString(), controlId);

        return blocks.toString();
    }

    private String generateRoadmapDialogTemplate(String uuid, String controlId, String currentMaturity, String targetMaturity, String targetDate) throws Exception {
        JSONArray elements = new JSONArray();
        LinkedHashMap maturityValues = new LinkedHashMap<>();
        maturityValues.put("Not Tracked", "0");
        maturityValues.put("Initial", "1");
        maturityValues.put("Repeatable", "2");
        maturityValues.put("Defined", "3");
        maturityValues.put("Managed", "4");
        maturityValues.put("Matured", "5");
        
        LinkedHashMap dateValues = new LinkedHashMap<>();
        dateValues.put("Q3-2019", "Q32019");
        dateValues.put("Q4-2019", "Q42019");
        dateValues.put("Q1-2020", "Q12020");
        dateValues.put("Q2-2020", "Q22020");
        dateValues.put("Q3-2020", "Q32020");
        dateValues.put("Q4-2020", "Q42020");
    
        OrderedJSONObject currentMaturitySection = createDialogMrkdwnSection("Current Maturity", maturityValues, currentMaturity);
        OrderedJSONObject targetMaturitySection = createDialogMrkdwnSection("Target Maturity", maturityValues, targetMaturity);
        OrderedJSONObject targetDateSection = createDialogMrkdwnSection("Target Date", dateValues, targetDate);
    
        elements.add(currentMaturitySection);
        elements.add(targetMaturitySection);
        elements.add(targetDateSection);
        //OrderedJSONObject optionGroup = new OrderedJSONObject();
        //optionGroup.put("option_groups", elements);

        String state = uuid+":roadmap:DEMO";

        OrderedJSONObject dialog = new OrderedJSONObject();
        dialog.put("title", "Add to Roadmap");
        dialog.put("callback_id", controlId);
        dialog.put("elements", elements);
        dialog.put("state", state);

        return dialog.toString();
    }

    private void initializeDialog(String uuid, String controlId) throws Exception {
        String token = "xoxp-743454286467-754454369364-743483929987-9857528d5e3f71aac77db246a4ccb8f2";
        //String url = "https://slack.com/api/dialog.open?";
        String t = generateRoadmapDialogTemplate(uuid, controlId, "0", "0", "01/01/2020");
        //url += "token=xoxp-748065782663-748065783031-752078630149-4f279f589bc66a1c08284512be6c68ba";
        //url += "&trigger_id=13345224609.738474920.8088930838d88f008e0";
        //url += "&dialog="+URLEncoder.encode(t, "UTF-8");
        System.out.println("Dialog:"+t);
        t = URLEncoder.encode(t, "UTF-8");
        OrderedJSONObject ext = new OrderedJSONObject();
        ext.put("dialog", t);
        ext.put("token", token);
        ext.put("component_id", controlId);
        String sql = "insert into enterprise_collaboration(id,extension,created_timestamp) values(?, ?, current_timestamp)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, uuid);
        pst.setString(2, ext.toString());
        pst.executeUpdate();
        conn.commit();
    }

    private void mysqlConnect() throws Exception {
        String preprod = "35.227.181.195";
        String prod = "35.197.106.183";
        String userName = "root", password = "smarterD2018!", dbname = "DEMO", hostname=preprod;
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

    public static void main(String[] args) throws Exception {
        SlackWebHook slack = new SlackWebHook();
        slack.post();
    }
}