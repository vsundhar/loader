import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONObject;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

import java.util.*;
import java.text.*;
import java.sql.*;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class VulnrValidation {
	private CSVParser dataReader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public VulnrValidation(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		dataReader = CSVParser.parse(new FileReader(new File(filename+".csv")), CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void validateScanCount() throws Exception {
		int rowCount = 0, missingCount=0, duplicateCount=0;
		for(CSVRecord dataRow : dataReader) {
			String scanId=null, hostName=null;
			rowCount++;
			if(dataRow.isConsistent()) {
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						scanId = dataRow.get(i).trim();
					} else if(i==1) {
						hostName = dataRow.get(i).trim();
					}
				}
			}
			
			// Get Asset Id
			int count = 0;
			String checksql = "select count(*) count from enterprise_vulnerability_log where json_extract(extension, '$.\"scan_id\"') = ?";
			PreparedStatement pst = conn.prepareStatement(checksql);
			pst.setString(1, scanId);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();
			
			if(count==0) {
				System.out.println(rowCount+":"+scanId+":"+hostName+":Missing");
				missingCount++;
			} else if(count>1) {
				System.out.println(rowCount+":"+scanId+":"+hostName+":Duplicate");
				duplicateCount++;
			}
		}
		System.out.println("Total:"+rowCount+" Missing:"+missingCount+" Duplicate:"+duplicateCount);
		conn.close();
	}

	private void validateRelCount(String scanPeriod) throws Exception {
		String logsql = "select vulnerability_id, asset_id, json_unquote(json_extract(extension, '$.\"Port\"[0]')) port from enterprise_vulnerability_log where json_extract(extension, '$.\"scan_period\"') = ?";
		String relsql = "select id, json_unquote(json_extract(asset_rel_context, '$.THREAT.\"status\"')) status from enterprise_rel where component_id=? and asset_id=? and json_extract(asset_rel_context, '$.THREAT.\"Port\"[0]') = ?";
		String delsql = "delete from enterprise_rel where component_id = ? and asset_id = ? and json_extract(asset_rel_context, '$.THREAT.\"Port\"[0]') = ?";
		String diffsql = "select id from enterprise_rel where component_id = ? and asset_id = ? and json_extract(asset_rel_context, '$.THREAT.\"Port\"[0]') = ? and id not in (select max(id) from enterprise_rel where component_id = ? and asset_id = ? and json_extract(asset_rel_context, '$.THREAT.\"Port\"[0]') = ?)";
		PreparedStatement pst2 = conn.prepareStatement(delsql);
		JSONArray diffList = new JSONArray();

		int duplicateVulnrCount = 0, duplicateDiffVulnrCount = 0, missingRelCount = 0;
		PreparedStatement pst = conn.prepareStatement(logsql);
		pst.setString(1, scanPeriod);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String vulnrId = rs.getString("vulnerability_id");
			String assetId = rs.getString("asset_id");
			String port = rs.getString("port");
			//System.out.println(vulnrId+":"+assetId+":"+port);
			if(port == null) {
				port = "";
			}
			int count = 0, diffStatusCount = 0;
			String diffStatus = "";
			PreparedStatement pst1 = conn.prepareStatement(relsql);
			pst1.setString(1, vulnrId);
			pst1.setString(2, assetId);
			pst1.setString(3, port);
			//System.out.println(pst1.toString());
			ResultSet rs1 = pst1.executeQuery();
			while(rs1.next()) {
				int id = rs1.getInt("id");
				String status = rs1.getString("status");
				/*
				if(count == 0) {
					diffStatus = status;
				} else if(!status.equals(diffStatus)) {
					System.out.println(id+":"+status);
					diffStatusCount++;
					diffStatus = status;
					duplicateDiffVulnrCount++;
				} else {
					System.out.println("Duplicate Found. deleting...");
					duplicateVulnrCount++;
					pst2.setString(1, vulnrId);
					pst2.setString(2, assetId);
					pst2.setString(3, port);
					pst2.addBatch();
					pst2.setString(1, assetId);
					pst2.setString(2, vulnrId);
					pst2.setString(3, port);
					pst2.addBatch();
				}
				*/
				count++;
			}
			rs1.close();
			pst1.close();
			System.out.println("Count:"+count);
			if(count == 0) {
				missingRelCount++;
				System.out.println("Missing Rel:"+vulnrId+":"+assetId+":"+port);
			} else if(count > 1) {
				System.out.println("Duplicate Rows found with different status. Adding to Delete List...");
			 	pst1 = conn.prepareStatement(diffsql);
				pst1.setString(1, vulnrId);
				pst1.setString(2, assetId);
				pst1.setString(3, port);
				pst1.setString(4, vulnrId);
				pst1.setString(5, assetId);
				pst1.setString(6, port);
				rs1 = pst1.executeQuery();
				while(rs1.next()) {
					int id = rs1.getInt("id");
					diffList.add(Integer.toString(id));
				}
				rs1.close();
				pst1.close();

				pst1 = conn.prepareStatement(diffsql);
				pst1.setString(1, assetId);
				pst1.setString(2, vulnrId);
				pst1.setString(3, port);
				pst1.setString(4, assetId);
				pst1.setString(5, vulnrId);
				pst1.setString(6, port);
				rs1 = pst1.executeQuery();
				while(rs1.next()) {
					int id = rs1.getInt("id");
					diffList.add(Integer.toString(id));
				}
				rs1.close();
				pst1.close();
			}
		}
		rs.close();
		pst.close();

		pst2.executeBatch();
		pst2.close();
		conn.commit();

		System.out.println("Total Duplicate Host Found Removed:"+duplicateVulnrCount);
		System.out.println("Total Duplicate Host with Different Status List:"+diffList.size());
		System.out.println("Total Missing Rel Host:"+missingRelCount);
		System.out.println(diffList.toString());
	}

	private void addMissingRel(String vulnrId, String assetId, String port, String priority) throws Exception {
		/*
		String vulnrName = "Unix Operating System Unsupported Version Detection";
		String vulnrId = "a83f1c65-8128-4eec-b55e-262ef8524afa";
		String assetName = "10.98.64.41";
		String ipAddress = "";
		String assetId = "1974af83-5dd2-44c9-baff-d5e7e70607e3";
		String priority = "Medium";
		String hostId = "7355478";
		String osName = "AIX 5.3";
		String groupName = "Default Group";
		String port = "";
		String assetStatus = "Active";
		String source = "NESSUS";
		String scannerOutput = "";
		String lastseenOn = "1/3/22";
		JSONArray vulnrType = new JSONArray();
		JSONArray serviceList = new JSONArray();

		OrderedJSONObject threatCtx = new OrderedJSONObject();
		threatCtx.put("id", vulnrId);
		threatCtx.put("threatName", vulnrName);
		threatCtx.put("status", assetStatus);
		threatCtx.put("priority", priority);
		threatCtx.put("type", "");
		threatCtx.put("OS", osName);
		threatCtx.put("IP Address", "10.98.64.41");
		threatCtx.put("Scanner Output", scannerOutput);
		threatCtx.put("Host Id", hostId);
		threatCtx.put("group_name", groupName);
		threatCtx.put("source", source);

		JSONArray dateList = new JSONArray();
		dateList.add(lastseenOn);
		threatCtx.put("last_seen", dateList);
		threatCtx.put("Vulnerability Type", assetVulnrType);

		JSONArray assetPortList = new JSONArray();
		assetPortList.add(port);
		threatCtx.put("Port", assetPortList);
		threatCtx.put("Service", serviceList);
		threatCtx.put("status", "Active");
		
		OrderedJSONObject ctxObj = new OrderedJSONObject();
		ctxObj.put("COMPONENT TYPE", "THREAT ASSET");
		ctxObj.put("LINKED", "N");
		ctxObj.put("THREAT", threatCtx);
		String relinsertsql = "insert into enterprise_rel(id, component_id, asset_id, asset_rel_type, asset_rel_context, created_by, created_timestamp, updated_by, updated_timestamp, company_code) values(null,?,?,?,?,?,current_timestamp,?,current_timestamp,?)";		
		pst = conn.prepareStatement(relinsertsql);
		pst.setString(1, vulnrId);
		pst.setString(2, assetId);
		pst.setString(3, "ASSET");
		pst.setString(4, ctxObj.toString());
		pst.setString(5, email);
		pst.setString(6, email);
		pst.setString(7, companyCode);
		pst.addBatch();
		ctxObj.put("COMPONENT TYPE", "ASSET");
		pst.setString(1, assetId);
		pst.setString(2, vulnrId);
		pst.setString(3, "THREAT ASSET");
		pst.setString(4, ctxObj.toString());
		pst.setString(5, email);
		pst.setString(6, email);
		pst.setString(7, companyCode);
		pst.addBatch();
		pst.executeBatch();
		pst.close();
		*/
	}

	// Update First Discovered Date, Last Discovered Date, Workflow Id and State
	private void updateVulnrLog() throws Exception {
		int rowCount = 0, missingCount=0, duplicateCount=0;
		for(CSVRecord dataRow : dataReader) {
			String scanId=null, firstdate=null, lastdate=null, wfId=null, wfstate=null;
			rowCount++;
			if(dataRow.isConsistent()) {
				for(int i=0; i<dataRow.size(); i++) {
					if(i==0) {
						scanId = dataRow.get(i).trim();
					} else if(i==1) {
						firstdate = dataRow.get(i).trim();
					} else if(i==2) {
						lastdate = dataRow.get(i).trim();
					} else if(i==3) {
						wfId = dataRow.get(i).trim();
					} else if(i==4) {
						wfstate = dataRow.get(i).trim();
					}
				}
				if(firstdate == null) {
					firstdate = "";
				} else {
					firstdate = formatDate(firstdate);
				}
				if(lastdate == null) {
					lastdate = "";
				} else{
					lastdate = formatDate(lastdate);
				}
				if(wfId == null) {
					wfId = "";
				}
				if(wfstate == null) {
					wfstate = "";
				}
				System.out.println(scanId+":"+firstdate+":"+lastdate+":"+wfId+":"+wfstate);
			
				// Get Asset Id
				String id=null, ext=null;
				String checksql = "select id, extension from enterprise_vulnerability_log where json_extract(extension, '$.THREAT.\"scan_id\"') = ? and json_extract(extension, '$.\"scan_period\"') = 'NOV-22'";
				PreparedStatement pst = conn.prepareStatement(checksql);
				pst.setString(1, scanId);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					id = rs.getString("id");
					ext = rs.getString("extension");
				}
				rs.close();
				pst.close();

				if(id != null) {
					System.out.println("Found!!!");
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					OrderedJSONObject threatCtx = (OrderedJSONObject)extObj.get("THREAT");
					threatCtx.put("First Discovered On", firstdate);
            		threatCtx.put("Last Discovered On", lastdate);
					extObj.put("Workflow Id", wfId);
            		extObj.put("Workflow State", wfstate);
					extObj.put("THREAT", threatCtx);

					// Update
					String updatesql = "update enterprise_vulnerability_log set extension=? where id = ?";
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, extObj.toString());
					pst.setString(2, id);
					pst.executeUpdate();
					pst.close();
				} else {
					System.out.println("Not Found:"+scanId);
				}
				/*
				if(rowCount == 1) {
					break;
				}
				*/
			}
		}
		conn.commit();
		conn.close();
	}

	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }

	private String formatDate(String d) throws Exception {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");   // 10
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("MM/d/yyyy");   // 9
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("M/dd/yyyy");   // 9
        DateTimeFormatter df3 = DateTimeFormatter.ofPattern("MM/dd/yy");    // 8
        DateTimeFormatter df4 = DateTimeFormatter.ofPattern("M/d/yyyy");    // 8
        DateTimeFormatter df5 = DateTimeFormatter.ofPattern("MM/d/yy");     // 7
        DateTimeFormatter df6 = DateTimeFormatter.ofPattern("M/dd/yy");     // 7
        DateTimeFormatter df7 = DateTimeFormatter.ofPattern("M/d/yy");      // 6
        DateTimeFormatter df8 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.000'Z'");     // ISO
        DateTimeFormatter df9 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        LocalDate dt = null;
        String formattedDate = "";
        if(d != null && d.length() > 0) {
            d = d.trim();
            if(d.length() == 6 && d.indexOf("/") != -1) {
                dt = LocalDate.parse(d, df7);
                formattedDate = df.format(dt);
			} else if (d.length() == 7 && d.indexOf("/") != -1) {    //1/01/07
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df6);
                } else {
                    dt = LocalDate.parse(d, df5);
                }
                formattedDate = df.format(dt);
            } else if (d.length() == 8 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df4);
                } else {
                    dt = LocalDate.parse(d, df3);
                }
                formattedDate = df.format(dt);
			} else if (d.length() == 9 && d.indexOf("/") != -1) {
                String m = d.substring(0,d.indexOf("/"));
                if(m.length() == 1) {
                    dt = LocalDate.parse(d, df2);
                } else {
                    dt = LocalDate.parse(d, df1);
                }
                formattedDate = df.format(dt);
			} else if(d.indexOf("T") != -1) {
                dt = LocalDate.parse(d, df8);
                formattedDate = df.format(dt);
			} else if(d.indexOf(" ") != -1) {
                dt = LocalDate.parse(d, df9);
                formattedDate = df.format(dt);
			} else {
                formattedDate = d;
            }
        }

        return formattedDate;
    }
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "peapi", password = "smartEuser2022!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&serverTimezone=UTC&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=UTF-8", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}

	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		VulnrValidation loader = new VulnrValidation(filename, companyCode, env);
		//loader.validateRelCount("MAR-22");
		loader.updateVulnrLog();
	}
}