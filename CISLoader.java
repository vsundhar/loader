import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class CISLoader {
	private String domain = "CIS 8";
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    private OrderedJSONObject capabilityList = new OrderedJSONObject();
    String rootId = null, hostname="localhost";
    int levelOne = 0, levelTwo = 0;
   	
	public CISLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			//hostname = "35.197.106.183";
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			//hostname = "35.227.181.195";
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
        
        // Check if Root capability exists - NIST 800-171
        checkRootCapability();
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String id=null, parentCapability=null, parentDesc=null, childCapability=null, desc=null, internalId=null;
				String assetClass=null, securityFunction=null,priority=null,ecosystem=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						id = row[i].trim();
					} else if(i==1) {
						parentCapability = row[i].trim();
					} else if(i==2) {
						parentDesc = row[i].trim();
					} else if(i==3) {
						internalId = row[i].trim();
					} else if(i==4) {
						assetClass = row[i].trim();
					} else if(i==5) {
						securityFunction = row[i].trim();
					} else if(i==6) {
						childCapability = row[i].trim();
					} else if(i==7) {
						desc = row[i].trim();
					} else if(i==8) {
						priority = row[i].trim();
					} else if(i==9) {
						ecosystem = row[i].trim();
					}
				}
					
				System.out.println(id+":"+parentCapability+":"+childCapability+":"+desc+":"+internalId);
				
				// Check Capability
				String parentId = null, childId=null;
				OrderedJSONObject res = checkCapability(parentCapability, childCapability, companyCode);		
				if(res.size() == 0) {				
					if(capabilityList.has(parentCapability)) {
						parentId = (String)capabilityList.get(parentCapability);
						childId = generateUUID();
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, assetClass, securityFunction, priority, ecosystem, "2", levelTwo);
						levelTwo++;
					} else {
						levelTwo = 0;
						parentId = generateUUID();
						childId = generateUUID();
						
						insertCapability(parentId, rootId, id, "CIS 8", parentCapability, parentDesc, "", securityFunction, "", "", "1", levelOne);
						insertCapability(childId, parentId, internalId, parentCapability, childCapability, desc, assetClass, securityFunction, priority, ecosystem, "2", levelTwo);
						capabilityList.put(parentCapability, parentId);
						
						levelOne++;
						levelTwo++;
					}	
				} else {
					String capabilityId = null, ext = null;
					capabilityId = (String)res.get("id");
					parentId = (String)res.get("parent_id");
					ext = (String)res.get("ext");
					System.out.println(capabilityId+":"+parentId);
					updateCapability(capabilityId, parentId, childCapability, parentCapability, ext, assetClass, ecosystem, priority);
					capabilityList.put(parentCapability, parentId);
				}
				
				System.out.println(parentId+":"+parentCapability+":"+childId+":"+childCapability);
			}
		}
		
		conn.commit();
		conn.close();
	}
	
	private void checkRootCapability() throws Exception {
		OrderedJSONObject res = checkCapability(domain, domain, companyCode);
		rootId = domain;
		System.out.println(res.toString());
		if(res.size() == 0) {
			insertCapability(rootId, rootId, "", domain, domain, "CIS 8 Controls", "", "", "", "", "0", 0);
		}
		capabilityList.put(domain, rootId);
		
		System.out.println("Root Id:"+rootId);
	}
	
	private OrderedJSONObject checkCapability(String parent, String child, String ccode) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		String capabilityCheckSQL = "select id,parent_capability_id,business_capability_ext from "+ccode+".enterprise_business_capability where parent_capability_name = ? and child_capability_name = ? and business_capability_status='Y' and business_capability_domain=?";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, parent);
		pst.setString(2, child);
		pst.setString(3, domain);
		//System.out.println(pst.toString());
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String parentId = rs.getString("parent_capability_id");
			String ext = rs.getString("business_capability_ext");
			res.put("id", id);
			res.put("parent_id", parentId);
			res.put("ext", ext);
		}
		rs.close();
		pst.close();
		
		return res;
	}

	private OrderedJSONObject checkCapability(String internalId) throws Exception {
		OrderedJSONObject res = new OrderedJSONObject();
		String capabilityCheckSQL = "select id,business_capability_ext from enterprise_business_capability where internal_id = ? and business_capability_domain='CIS 8'";
		PreparedStatement pst = conn.prepareStatement(capabilityCheckSQL);
		pst.setString(1, internalId);
		//System.out.println(pst.toString());
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String ext = rs.getString("business_capability_ext");
			res.put("id", id);
			res.put("ext", ext);
		}
		rs.close();
		pst.close();
		
		return res;
	}
	
	private void insertCapability(String id, String parentId, String internalId, String parent, String child, String desc, 
								  String assetClass, String securityFunction, String priority, String ecosystem, String level, int order) throws Exception {
		String capabilityInsertSQL = "insert into enterprise_business_capability(id,parent_capability_id, internal_id,parent_capability_name,child_capability_name,business_capability_desc,business_capability_category,owner,business_capability_status,business_capability_order,business_capability_level, business_capability_factor,business_capability_asset_factor,business_capability_domain,business_capability_ext,created_by,updated_by,company_code,created_timestamp) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())"; 
   		String category = "3";
   		String createdBy = "support@smarterd.com";
   		String owner = "", status = "Y";
   		
   		OrderedJSONObject factorObject = new OrderedJSONObject();
		factorObject.put("currentMaturity", "");
		factorObject.put("targetMaturity", "");
		factorObject.put("baselineMaturity", "");
		factorObject.put("targetDate", "");
		factorObject.put("current_risk", "NA");
		factorObject.put("current_risk_value", "0");
		String factor = factorObject.toString();
		
		OrderedJSONObject assetfactorObject = new OrderedJSONObject();
		String assetFactor = assetfactorObject.toString();

		OrderedJSONObject planfactorObject = new OrderedJSONObject();
		String planFactor = planfactorObject.toString();

		JSONArray classArr = new JSONArray();
		String [] str = assetClass.split(",");
		for(int i=0; i<str.length; i++) {
			if(str[i].trim().length() > 0) {
				classArr.add(str[i].trim());
			}
		}

		JSONArray arr = new JSONArray();
		if(ecosystem != null && ecosystem.length() > 0) {
			str = ecosystem.split(",");
			for(int i=0; i<str.length; i++) {
				arr.add(str[i].trim());
			}
		}

		JSONArray sfArr = new JSONArray();
		if(securityFunction != null && securityFunction.length() > 0) {
			sfArr.add(securityFunction);
		}

		OrderedJSONObject extObject = new OrderedJSONObject();
		extObject.put("family", parent);
		extObject.put("compliance", new OrderedJSONObject());
		extObject.put("assetClass", classArr);
		extObject.put("ecosystem", arr);
		extObject.put("securityFunction", sfArr);
		extObject.put("criticality", priority);
		extObject.put("evidence", "");
		extObject.put("population", "");
		String ext = extObject.toString();
	
		PreparedStatement pst = conn.prepareStatement(capabilityInsertSQL);
		pst.setString(1, id);
		pst.setString(2, parentId);
		pst.setString(3, internalId);
		pst.setString(4, parent);
		pst.setString(5, child);
		pst.setString(6, desc);
		pst.setString(7, category);
		pst.setString(8, owner);
		pst.setString(9, status);
		pst.setInt(10, order);
		pst.setString(11, level);
		pst.setString(12, factor);
		pst.setString(13, assetFactor);
		pst.setString(14, domain);
		pst.setString(15, ext);
		pst.setString(16, createdBy);
		pst.setString(17, createdBy);
		pst.setString(18, companyCode);
		System.out.println(pst.toString());
		pst.executeUpdate();
		pst.close();
	}

	private void updateCapability(String id, String parentId, String childName, String parentName, String ext, String assetClass, String ecosystem, String priority) throws Exception {
		String sql = "update enterprise_business_capability set id=?, parent_capability_id=?, business_capability_ext=? where child_capability_name = ? and parent_capability_name=?";
		JSONArray classArr = null;
		OrderedJSONObject extObj = new OrderedJSONObject(ext);
		if(extObj.has("assetClass")) {
			if(extObj.get("assetClass") instanceof String) {
				String capabilityAssetClassList = (String)extObj.get("assetClass");
				if(capabilityAssetClassList.length() > 0) {
					classArr = new JSONArray(capabilityAssetClassList);
				} else {
					classArr = new JSONArray();
				}
			} else if(extObj.get("assetClass") instanceof JSONArray) {
				classArr = (JSONArray)extObj.get("assetClass");
			} else {
				classArr = new JSONArray();
			}
			ArrayList list = new ArrayList();
			Iterator iter = classArr.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				list.add(s.trim());
				// Remove old assetclass with space in front
				iter.remove();
			}
			iter = list.iterator();
			while(iter.hasNext()) {
				String s = (String)iter.next();
				classArr.add(s);
			}
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					if(!classArr.contains(str[i].trim())) {
						classArr.add(str[i].trim());
					}
				}
			}
		} else {
			classArr = new JSONArray();
			if(assetClass != null) {
				String [] str = assetClass.split(",");
				for(int i=0; i<str.length; i++) {
					classArr.add(str[i].trim());
				}
			}
		}

		JSONArray arr = new JSONArray();
		if(ecosystem != null && ecosystem.length() > 0) {
			String[] str = ecosystem.split(",");
			for(int i=0; i<str.length; i++) {
				arr.add(str[i].trim());
			}
		}

		extObj.put("assetClass", classArr);
		extObj.put("ecosystem", arr);
		extObj.put("criticality", priority);
		PreparedStatement pst = conn.prepareStatement(sql);
		pst.setString(1, id);
		pst.setString(2, parentId);
		pst.setString(3, extObj.toString());
		pst.setString(4, childName);
		pst.setString(5, parentName);
		System.out.println(pst.toString());
		pst.executeUpdate();
	}

	private void updateImplementationGroup() throws Exception {
		String[] row;
		int rowCount=0;
		int level = 0;
		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				System.out.println("Processing row-" + rowCount + "...");
				
				String internalId=null, ig1=null, ig2=null, ig3=null;
				String assetClass=null, securityFunction=null,priority=null,ecosystem=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {			
						internalId = row[i].trim();
					} else if(i==1) {
						ig1 = row[i].trim();
					} else if(i==2) {
						ig2 = row[i].trim();
					} else if(i==3) {
						ig3 = row[i].trim();
					}
				}

				OrderedJSONObject res = checkCapability(internalId);

				if(res.size() > 0) {
					String updatesql = "update enterprise_business_capability set business_capability_ext=? where id = ?";
					String id = (String)res.get("id");
					String ext = (String)res.get("ext");

					JSONArray implArr = new JSONArray();
					if(ig1 != null && ig1.equalsIgnoreCase("x")) {
						implArr.add("IG1");
					}
					if(ig2 != null && ig2.equalsIgnoreCase("x")) {
						implArr.add("IG2");
					}
					if(ig3 != null && ig3.equalsIgnoreCase("x")) {
						implArr.add("IG3");
					}

					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					if(extObj.has("custom")) {
						OrderedJSONObject customObj = (OrderedJSONObject)extObj.get("custom");
						customObj.put("Implementation Group", implArr);
						extObj.put("custom", customObj);
					} else {
						OrderedJSONObject customObj = new OrderedJSONObject();
						customObj.put("Implementation Group", implArr);
						extObj.put("custom", customObj);
					}
					PreparedStatement pst = conn.prepareStatement(updatesql);
					pst.setString(1, extObj.toString());
					pst.setString(2, id);
					pst.executeUpdate();
					pst.close();

					System.out.println("Updated Control:"+internalId);
				}
			}
		}
		conn.commit();
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "admin", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		CISLoader loader = new CISLoader(filename, companyCode, env);
		//loader.process();
		loader.updateImplementationGroup();
	}
}