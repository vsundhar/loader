import java.util.ArrayList;

public abstract class Animal {
	// Container to hold all Animals
	protected List animalList = new ArrayList();

	// Return Animal List
	public List getAnimalList() {
		return animalList;
	}

	// To be Implemented by Inherited Class
	protected abstract void speak();
}