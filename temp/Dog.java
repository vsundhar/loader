import java.util.ArrayList;

public class Dog implements Animal {
	private String name = null;
	
	// Add this instance to Animal List
	public Dog(String name) {
		this.name = name;
		animalList.add(this);
	}

	// Implement Super Class Method
	public void speak() {
		System.out.println("This is "+name+"...Hoof Hoof!!!");
	}
}