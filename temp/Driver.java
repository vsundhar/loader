import java.util.*;

public class Driver {
	private String name = null;
	
	// Add this instance to Animal List
	public Driver() {}

	public static void main(String[] args) throws Exception {
		Dog jasmine = new Dog("Jasmine");

		// Cat to Animal to get Animal List
		List animalList = ((Animal)jasmine).getAnimalList();
		Iterator iter = animalList.iterator();
		while(iter.hasNext()) {
			Dog dog = (Dog)iter.next();
			dog.speak();
		}
	}
}