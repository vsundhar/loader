import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.File;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.text.*;
import java.sql.*;

public class MauricesAssetLoader {
	private CSVReader reader = null;
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public MauricesAssetLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		reader = new CSVReader(new FileReader(new File(filename)), ',', '"', 0);
		System.out.println("Successfully read data file:" + filename);
		
		if(env.equals("prod")) {
			hostname = "35.197.106.183";
		} else if(env.equals("preprod")) {
			hostname = "35.227.181.195";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String[] row;
		int rowCount=0;
		String assetSQL = "select id,asset_name, extension from enterprise_asset";
		int notFoundCount=0;
		Map assetList = new HashMap();
		PreparedStatement pst = conn.prepareStatement(assetSQL);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			String id = rs.getString("id");
			String name = rs.getString("asset_name");
			String ext = rs.getString("extension");
			assetList.put(id+":"+name.trim(), ext);
		}
		rs.close();
		pst.close();

		while ((row = reader.readNext()) != null) {
			rowCount++;
			if (rowCount > 1) {
				//System.out.println("Processing row-" + rowCount + "...");
				
				String name=null, desc=null, version=null, owner=null,itOwner=null, ba=null;
				String latestVersion=null,upgradeComplexity=null,moveComplexity=null,industryPosition=null;
				String primaryRecommendation=null, secondaryRecommendation=null,risk=null,topAlternatives=null;
				String tcsList=null, potentialSaving=null,projectCode=null, notes=null, type=null;
				for(int i=0; i<row.length; i++) {
					if(i==0) {
						type = row[i].trim();
					} else if(i==1) {
						name = row[i].trim();
					} else if(i==2) {			
						desc = row[i];
					} else if(i==3) {
						version = row[i];
					} else if(i==4) {
						owner = row[i];
					} else if(i==5) {
						itOwner = row[i].trim();
					} else if(i==6) {
						ba = row[i];
					} else if(i==7) {
						notes = row[i];
					} else if(i==8) {
						latestVersion = row[i];
					} else if(i==9) {
						upgradeComplexity = row[i];
					} else if(i==10) {
						moveComplexity = row[i];
					} else if(i==11) {
						industryPosition = row[i];
					} else if(i==12) {
						primaryRecommendation = row[i];
					} else if(i==13) {
						secondaryRecommendation = row[i];
					} else if(i==14) {
						risk = row[i];
					} else if(i==15) {
						topAlternatives = row[i];
					} else if(i==16) {
						tcsList = row[i];
					} else if(i==17) {
						potentialSaving = row[i];
					} else if(i==18) {
						projectCode = row[i];
					}
				}

				String id = null, assetName=null;
				boolean found = false;
				Iterator iter = assetList.keySet().iterator();
				while(iter.hasNext()) {
					String key = (String)iter.next();
					id = key.substring(0, key.indexOf(":"));
					assetName = key.substring(key.indexOf(":")+1);
					String ext = (String)assetList.get(key);
					System.out.println(key);
					OrderedJSONObject extObj = new OrderedJSONObject(ext);

					if(assetName.equalsIgnoreCase(name)) {
						System.out.println(name);
						found = true;
					} else if(extObj.has("Alias Names")) {
						JSONArray alias = (JSONArray)extObj.get("Alias Names");
						Iterator aliasIter = alias.iterator();
						while(aliasIter.hasNext()) {
							String aliasName = (String)aliasIter.next();
							if(aliasName.equalsIgnoreCase(name)) {
								System.out.println("Alias Match:"+name);
								found=true;
								break;
							}
						}
					}
					if(found) {
						break;
					}
				}
				System.out.println(found+":"+id+":"+name);
				System.out.println(version);

				if(!found) {
					String insertsql = "insert into enterprise_asset(id,asset_name,asset_desc,asset_category,asset_version,owner,extension,created_by,updated_by,company_code) values(uuid(),?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com','MAUR')";
					OrderedJSONObject extObj = new OrderedJSONObject();
					extObj.put("Notes", notes);
					extObj.put("Latest Version", latestVersion);
					extObj.put("Upgrade Complexity", upgradeComplexity);
					extObj.put("Move Complexity", moveComplexity);
					extObj.put("Position in Industry", industryPosition);
					extObj.put("Primary Recommendation", primaryRecommendation);
					extObj.put("Secondary Recommendation", secondaryRecommendation);
					extObj.put("Risk/Callouts", risk);
					extObj.put("Top Alternatives", topAlternatives);
					extObj.put("In TCS List", tcsList);
					extObj.put("Potential for Cost Saving", potentialSaving);
					extObj.put("Project", projectCode);
					extObj.put("Business app/IT app", type);
					extObj.put("Business Unit", ba);
					extObj.put("Manager", itOwner);
					pst = conn.prepareStatement(insertsql);
					pst.setString(1, name);
					pst.setString(2, desc);
					pst.setString(3, "APPLICATION");
					pst.setString(4, version);
					pst.setString(5, owner);
					pst.setString(6, extObj.toString());
					pst.executeUpdate();
				} else {
					String updatesql = "update enterprise_asset set asset_desc=?,asset_version=?,owner=?,extension=? where id=?";
					String ext = (String)assetList.get(id+":"+assetName);
					OrderedJSONObject extObj = new OrderedJSONObject(ext);
					extObj.put("Notes", notes);
					extObj.put("Latest Version", latestVersion);
					extObj.put("Upgrade Complexity", upgradeComplexity);
					extObj.put("Move Complexity", moveComplexity);
					extObj.put("Position in Industry", industryPosition);
					extObj.put("Primary Recommendation", primaryRecommendation);
					extObj.put("Secondary Recommendation", secondaryRecommendation);
					extObj.put("Risk/Callouts", risk);
					extObj.put("Top Alternatives", topAlternatives);
					extObj.put("In TCS List", tcsList);
					extObj.put("Potential for Cost Saving", potentialSaving);
					extObj.put("Project", projectCode);
					extObj.put("Business app/IT app", type);
					extObj.put("Business Unit", ba);
					extObj.put("Manager", itOwner);
					pst = conn.prepareStatement(updatesql);
					pst.setString(1, desc);
					pst.setString(2, version);
					pst.setString(3, owner);
					pst.setString(4, extObj.toString());
					pst.setString(5, id);
					pst.executeUpdate();
				}
				pst.close();
			}
		}
		System.out.println("Total Assets:"+rowCount+ " Not found:"+notFoundCount);
		conn.commit();
		conn.close();
	}
		
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "root", password = "smarterD2018!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		MauricesAssetLoader loader = new MauricesAssetLoader(filename, companyCode, env);
		loader.process();
	}
}