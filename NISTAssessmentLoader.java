import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.Optional;
import java.io.File;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFCellStyle; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 
import org.apache.poi.xssf.usermodel.XSSFFont; 
import org.apache.poi.xssf.usermodel.XSSFColor; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.CellStyle; 
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.BorderStyle; 
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.net.URLEncoder;

import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONArray;

public class NISTAssessmentLoader {
	private String domain = "UCM";
	private String companyCode = null;
    private Connection conn = null;
    String hostname="localhost";
   	
	public NISTAssessmentLoader(String filename, String companyCode, String env) throws Exception {
		this.companyCode = companyCode;
		
		if(env.equals("prod")) {
			hostname = "sd-prod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		} else if(env.equals("preprod")) {
			hostname = "sd-preprod-aws.cxr0i92wo9k7.us-east-1.rds.amazonaws.com";
		}
        
        // Connect Mysql
        mysqlConnect(companyCode);
	}
	
	public void process() throws Exception {
		String assessmentId = "f074f36f-8d28-11ed-8898-0ae85b2c5055";

		File folder = new File("./MAUR_Phase_2_Assessment");
		File[] listOfFiles = folder.listFiles();
		for(File file : listOfFiles) {
			if(file.isFile()) {
				System.out.println(file.getName());
				FileInputStream fis = new FileInputStream(file);
				XSSFWorkbook workBook = new XSSFWorkbook (fis); 

				OrderedJSONObject issueObj = new OrderedJSONObject();
				String controlEffectiveness="", riskRating = "", controlInPlace="", status="", recommendation="";
				for (int i = 0; i < workBook.getNumberOfSheets(); i++) {
					XSSFSheet workSheet = workBook.getSheetAt(i); 
					String wsName = workSheet.getSheetName();
					System.out.println(i+":"+wsName);
					if(i == 1) { // Control Area
						int maxIssueRowCount = 0;
						// Get Issues and Recommendation - Row 39 Until Empty - Cell 2 and 3
						// Get iterator to all the rows in current sheet 
						Iterator<Row> rowIterator = workSheet.iterator(); 
						while (rowIterator.hasNext()) { 
							Row row = rowIterator.next(); 
							int rowNo = row.getRowNum();
							if(rowNo >= 38 && maxIssueRowCount == 0) {	// Start of Issues and read until you hit empty row
								int cellNo = 0;
								String key="", value="No";
								Iterator<Cell> cellIterator = row.cellIterator(); 
								while (cellIterator.hasNext()) { 
									Cell cell = cellIterator.next(); 
									//System.out.println(cellNo+":"+cell.getStringCellValue().trim());
									switch(cell.getCellType()) {
										case STRING:
											if(cellNo == 0) {
												key = cell.getStringCellValue().trim();
											} else if(cellNo == 5) {
												value = cell.getStringCellValue().trim();
											}
											break;
										case BLANK:
											if(cellNo == 0) {
												maxIssueRowCount = rowNo;
											}
											break;
										default:
											break;
									}
									cellNo++;
								}
								if(key.length() > 0 && maxIssueRowCount == 0) {
									issueObj.put(key, value);
									//System.out.println(key+":"+value);
								}
							} else if(rowNo >= 38 && maxIssueRowCount > 0) {
								int cellNo = 0;
								String key="", value="No";
								Iterator<Cell> cellIterator = row.cellIterator(); 
								while (cellIterator.hasNext()) { 
									Cell cell = cellIterator.next(); 
									//System.out.println(cellNo+":"+cell.getStringCellValue().trim());
									switch(cell.getCellType()) {
										case STRING:
											if(cellNo == 1 || cellNo == 3) {
												key = cell.getStringCellValue().trim();
											} else if(cellNo == 4) {
												value = cell.getStringCellValue().trim();
											}
											break;
										case BLANK:
											if(cellNo == 0) {
												maxIssueRowCount = rowNo;
											}
											break;
										default:
											break;
									}
									cellNo++;
								}

								if(key.equalsIgnoreCase("Based on the interim testing performed, the control is deemed to be:")) {
									controlEffectiveness = value;
								} else if(key.equalsIgnoreCase("Risk Rating:")) {
									riskRating = value;
								}
							}
						}
					} else if(i >= 2 && wsName.indexOf("-") != -1) {
						String familyId = wsName.substring(0, wsName.indexOf("."));
						String internalId = wsName.substring(0, wsName.lastIndexOf("-")).trim(); 

						OrderedJSONObject headerObj = new OrderedJSONObject();
						JSONArray assessmentQList = new JSONArray();
						JSONArray assessmentQResponseList = new JSONArray();
						OrderedJSONObject qIdMap = new OrderedJSONObject();
						String controlId = null, controlName = null, parentId = null, parentName=null, familyName="", questions=null, ext=null, currentMaturity="", level=""; 
						int maxCellNo = 0, maxRow = 0;
						boolean isAsset = false;

						// Get iterator to all the rows in current sheet 
						Iterator<Row> rowIterator = workSheet.iterator(); 
						while (rowIterator.hasNext()) { 
							Row row = rowIterator.next(); 
							int rowNo = row.getRowNum();	// Row Index starts with 0
							if(rowNo == 11) {
								//System.out.println(rowNo);
								int cellNo = 0;
								Iterator<Cell> cellIterator = row.cellIterator(); 
								while (cellIterator.hasNext()) { 
									Cell cell = cellIterator.next(); 
									if(cellNo > 2) {	// Some sheets have Asset Description and Some don't and hence start with 3rd Column
										//System.out.println(cell.getCellType());
										switch(cell.getCellType()) {
											case STRING:
												String str = cell.getStringCellValue().trim();
												if(str.length() > 0 && str.equalsIgnoreCase("Asset Description")) {
													isAsset = true;
												} else if(str.length() > 0) {
													//System.out.println(rowNo+":"+cellNo+":"+str);
													headerObj.put(Integer.toString(cellNo), str);
												}
												break;
											case BLANK:
												maxCellNo = cellNo-1;
												break;
											default:
												break;
										}
									}
									if(maxCellNo > 0) {
										break;
									} else {
										cellNo++;
									}
								}

								if(cellNo > 0 && maxCellNo == 0) { // If in case we don't hit BLANK column in Header
									maxCellNo = cellNo-1;
								}

								System.out.println(familyId+":"+internalId);

								// Get Control Detail
								String controlsql = "select id, parent_capability_id,child_capability_name,parent_capability_name,business_capability_ext,json_unquote(json_extract(business_capability_factor, '$.currentMaturity')) current_maturity, internal_id,business_capability_level from enterprise_business_capability where internal_id = ?";
								String updatecontrolsql = "update enterprise_business_capability set extension=? where id = ?";
								PreparedStatement pst = conn.prepareStatement(controlsql);
								pst.setString(1, internalId);
								ResultSet rs = pst.executeQuery();
								while(rs.next()) {
									controlId = rs.getString("id");
									parentId = rs.getString("parent_capability_id");
									controlName = rs.getString("child_capability_name");
									parentName = rs.getString("parent_capability_name");
									ext = rs.getString("business_capability_ext");
									currentMaturity = rs.getString("current_maturity");
									level = rs.getString("business_capability_level");
								}
								rs.close();
								pst.close();
								
								OrderedJSONObject extObj = new OrderedJSONObject(ext);
								if(extObj.has("family")) {
									familyName = (String)extObj.get("family");
								} else {
									familyName = "";
								}

								JSONArray qList = null;
								if(extObj.has("questions")) {
									qList = (JSONArray)extObj.get("questions");
								} else {
									qList = new JSONArray();
								}

								// Reconcile with Control QList
								Iterator iter = headerObj.keySet().iterator();
								while(iter.hasNext()) {
									String key = (String)iter.next();
									String question = (String)headerObj.get(key);

									// Check if this question is in control question list
									boolean found = false;
									OrderedJSONObject qObj = null;
									Iterator qIter = qList.iterator();
									while(qIter.hasNext()) {
										qObj = (OrderedJSONObject)qIter.next();
										String q = (String)qObj.get("question");
										if(q.equalsIgnoreCase(question)) {
											found = true;
											break;
										}
									}
									String qId = null;
									if(!found) {
										qObj = new OrderedJSONObject();
										qId = generateUUID();
										qObj.put("id", qId);
										qObj.put("question", question);
										qObj.put("possible_response", "");
										qList.add(qObj);
									} else {
										qId = (String)qObj.get("id");
									}
									assessmentQList.add(qObj);
									qIdMap.put(question, qId);
								}

								// Update Control QList

							} else if(rowNo >= 12 && maxRow == 0) {
								System.out.println(maxCellNo+":"+maxRow);
								OrderedJSONObject rowObj = new OrderedJSONObject();
								int cellNo = 0;
								String assetName = "", appName = "";
								Iterator<Cell> cellIterator = row.cellIterator(); 
								while (cellIterator.hasNext()) { 
									Cell cell = cellIterator.next(); 
									if(cellNo == 2) {
										switch (cell.getCellType()) { 
											case STRING: 
												assetName = cell.getStringCellValue().trim();
												break; 
											case BLANK:
												maxRow = rowNo;
												break;
										}
									} else if(cellNo == 3 && isAsset) {
										appName = cell.getStringCellValue().trim();
									} else if(cellNo > 3 && isAsset && cellNo <= maxCellNo && maxRow == 0) {
										String str = "";
										switch (cell.getCellType()) { 
											case STRING: 
												str = cell.getStringCellValue().trim();
												break; 
											case BOOLEAN: 
												boolean b = cell.getBooleanCellValue(); 
												str = new Boolean(b).toString();
												break;  
											case BLANK:
												str = "";
												break;
											default : 
												break;
										} 
										//System.out.println(rowNo+":"+cellNo+":"+str);
										String header = (String)headerObj.get(Integer.toString(cellNo));
										rowObj.put(header, str);
									} else if(cellNo > 2 && !isAsset && cellNo <= maxCellNo && maxRow == 0) {
										String str = "";
										switch (cell.getCellType()) { 
											case STRING: 
												str = cell.getStringCellValue().trim();
												break; 
											case BOOLEAN: 
												boolean b = cell.getBooleanCellValue(); 
												str = new Boolean(b).toString();
												break;  
											case BLANK:
												str = "";
												break;
											default : 
												break;
										} 
										//System.out.println(rowNo+":"+cellNo+":"+str);
										String header = (String)headerObj.get(Integer.toString(cellNo));
										rowObj.put(header, str);
									}
									cellNo++;
								} 

								// Get AssetId
								if(assetName.length() > 0 && maxRow == 0) {
									//System.out.println(assetName+":"+appName);
									String assetId = "";
									String assetsql = "select id from enterprise_asset where asset_name = ?";
									PreparedStatement pst = conn.prepareStatement(assetsql);
									pst.setString(1, assetName);
									ResultSet rs = pst.executeQuery();
									while(rs.next()) {
										assetId = rs.getString("id");
									}
									rs.close();
									pst.close();

									if(assetId == null) {
										assetId = "";
									}

									// Create Asset Object
									OrderedJSONObject assetObj = new OrderedJSONObject();
									assetObj.put("id", assetId);
									assetObj.put("name", assetName);
									assetObj.put("app_name", appName);

									// Create Response Object
									OrderedJSONObject resObj = new OrderedJSONObject();
									resObj.put("asset", assetObj);
									resObj.put("artifacts_referenced", "");
									resObj.put("comment", "");
									resObj.put("audit_status", "");

									System.out.println(rowObj);
									Iterator iter = rowObj.keySet().iterator();
									while(iter.hasNext()) {
										String q = (String)iter.next();
										String value = ((String)rowObj.get(q)).trim();
										String qId = (String)qIdMap.get(q);
										JSONArray linkArr = new JSONArray();
										if(value == null) {
											OrderedJSONObject linkObj = new OrderedJSONObject();
											linkObj.put("title", "");
											linkObj.put("link", "");
											linkArr.add(linkObj);
										} else { // Check if value has url link
											int count = getSubStrCount(value, "https");
											System.out.println(value);
											if(count == 1) {
												String title = "", link = "";
												if(value.startsWith("https")) {
													link = value;
													value = "";
												} else if(value.indexOf("(https") != -1) {
													link = value.substring(value.indexOf("(https")+1, value.lastIndexOf(")")-1);
													value = value.substring(0, value.indexOf("(https")).trim();
													if(value.lastIndexOf("(") != -1) {
														value = value.substring(0, value.lastIndexOf("(")-1);
													} 
												} else if(value.indexOf("https") != -1) {
													if(value.lastIndexOf(")") != -1) {
														link = value.substring(value.indexOf("https"), value.lastIndexOf(")")-1);
													} else {
														link = value.substring(value.indexOf("https"));
													}
													value = value.substring(0, value.indexOf("https")).trim();
													if(value.lastIndexOf("(") != -1) {
														value = value.substring(0, value.lastIndexOf("(")-1).trim();
													} else if(value.lastIndexOf("-") != -1) {
														value = value.substring(0, value.lastIndexOf("-")-1).trim();
													}
												}
												System.out.println(value+":"+link);
												OrderedJSONObject linkObj = new OrderedJSONObject();
												linkObj.put("title","");
												linkObj.put("link", link.trim());
												linkArr.add(linkObj);
											} else if(count > 1) {
												String[] list = value.split("- ");
												String title="", link=""; 
												for(int j=0; j<list.length; j++) {
													if(j==0) {
														value = list[j].trim();
													} else if(j==1) {
														title = list[j].trim();
													} else if(j==2) {
														link = list[j].trim();
														System.out.println(title+":"+link);
														OrderedJSONObject linkObj = new OrderedJSONObject();
														linkObj.put("title", title);
														linkObj.put("link", link);
														linkArr.add(linkObj);
													} else if(j==3) {
														String str = list[j].trim();
														title = str.substring(0, str.indexOf(" (")-1);
														link = str.substring(str.indexOf(" (")+1, str.lastIndexOf(")")-1);
														System.out.println(title+":"+link);
														OrderedJSONObject linkObj = new OrderedJSONObject();
														linkObj.put("title", title);
														linkObj.put("link", link);
														linkArr.add(linkObj);
													} 
												}
											} 
										}
										OrderedJSONObject valueObj = new OrderedJSONObject();
										valueObj.put("value", value);
										valueObj.put("link", linkArr);
										resObj.put(qId, valueObj);
									}
									assessmentQResponseList.add(resObj);
								}
							}
						}

						System.out.println("Control Effectiveness:"+controlEffectiveness+" Risk Rating:"+riskRating);
						System.out.println(internalId+":"+controlId+":"+controlName+":"+parentId+":"+parentName+":"+familyName);
						System.out.println("Question:"+assessmentQList.size()+" Responses:"+assessmentQResponseList.size());
						System.out.println("-----------------------");

						// Insert into Assessment Detail Object
						String insertsql = "insert into enterprise_assessment_detail values(uuid(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,current_timestamp,'support@smarterd.com','support@smarterd.com')";
						String updatesql = "update enterprise_assessment_detail set gaps=?, extension=? where id = ?";
						OrderedJSONObject detailExtObj = new OrderedJSONObject();
						detailExtObj.put("internal_id", internalId);
						detailExtObj.put("weighted_score", "");
						detailExtObj.put("parent_control_name", parentName);
						detailExtObj.put("control_name", controlName);
						detailExtObj.put("nist_mapping", "");
						detailExtObj.put("current_maturity", currentMaturity);
						detailExtObj.put("standard", "");
						detailExtObj.put("questions", assessmentQList);
						detailExtObj.put("question_responses", assessmentQResponseList);

						// Check if control exists
						String detailId = null;
						String checksql = "select id from enterprise_assessment_detail where assessment_id = ? and control_id = ?";
						PreparedStatement pst = conn.prepareStatement(checksql);
						pst.setString(1, assessmentId);
						pst.setString(2, controlId);
						ResultSet rs = pst.executeQuery();
						while(rs.next()) {
							detailId = rs.getString("id");
						}
						rs.close();
						pst.close();

						// Stringfy IssueObj
						int count = 0;
						String issues = "";
						Iterator iter = issueObj.keySet().iterator();
						while(iter.hasNext()) {
							String issue = (String)iter.next();
							if(count == 0) {
								issues = issue;
							} else {
								issues = issues+"\n"+issue;
							}
							count++;
						}

						// Set Status based on Control Effectiveness
						if(controlEffectiveness.equalsIgnoreCase("Effective")) {
							status = "Fully Implemented";
						} else if(controlEffectiveness.equalsIgnoreCase("Ineffective")) {
							status = "Partially Implemented";
						} else {
							status = "Not Implemented";
						}

						if(detailId == null) {
							pst = conn.prepareStatement(insertsql);
							pst.setString(1, assessmentId);
							pst.setString(2, controlId);
							pst.setString(3, parentId);
							pst.setString(4, familyName);
							pst.setString(5, level);
							pst.setString(6, issues); //gaps
							pst.setString(7, controlInPlace);
							pst.setString(8, status);
							pst.setString(9, "");	// severity
							pst.setString(10, ""); // effort
							pst.setString(11, ""); // artifacts
							pst.setString(12, ""); // notes
							pst.setString(13, recommendation);
							pst.setString(14, detailExtObj.toString());
							pst.executeUpdate();
							pst.close();
						} else {
							pst = conn.prepareStatement(updatesql);
							pst.setString(1, issues);
							pst.setString(2, detailExtObj.toString());
							pst.setString(3, detailId);
							pst.executeUpdate();
							pst.close();
						}
					}
				}	
			}
		}

		conn.commit();
		conn.close();

		System.out.println("Assessment loaded Successfully!!!");
	}
	
	private synchronized String generateUUID() throws Exception {
        UUID uuid = UUID.randomUUID();
        return(uuid.toString());
    }

	private int getSubStrCount(String str, String substr) throws Exception {
		Pattern p = Pattern.compile(substr);
		Matcher m = p.matcher(str);
		int count = 0;
		while (m.find()){
			count +=1;
		}

		return count;
	}
	
	private void mysqlConnect(String dbname) throws Exception {
    	String userName = "peapi", password = "smartEuser2022!";
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+hostname+":3306/"+dbname+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		conn.setAutoCommit(false);
		System.out.println("Successfully connected to Mysql DB:"+dbname);
	}
	
	public static void main(String[] args) throws Exception {
		String filename = args[0];
		String companyCode = args[1];
		String env = args[2];
		NISTAssessmentLoader loader = new NISTAssessmentLoader(filename, companyCode, env);
		loader.process();
	}
}